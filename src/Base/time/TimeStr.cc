/* -*- mode: c++; c-basic-offset: 3; -*- */
//
//    Time associated functions
//
#include "PConfig.h"
#include <time.h>
#ifndef HAVE_CLOCK_GETTIME
#include <sys/time.h>
#endif
#include <cstdlib>
#include <cstring>
#include <iostream>
#include "Time.hh"
#include "Interval.hh"

using namespace std;

//--------------------------------------  difference between GPS and ITC
static Time::ulong_t
GPS2UTC( const Time& t0 )
{
    // Number of seconds in a day
    const Time::ulong_t secpd = 24 * 60 * 60;
    // offset from Unix UTC to GPS Jan 1,1970-Jan 6,1980 = 10y + 5d + 2 leap-d
    const Time::ulong_t offset0 = ( 10 * 365 + 7 ) * secpd;

    return offset0 - LeapS( t0 );
}

Time::ulong_t
LeapS( const Time& t0 )
{
    // Number of seconds in a day
    const Time::ulong_t secpd = 24 * 60 * 60;

    // Offset in days from the GPS t=0
    const int           nLeaps = 18;
    const Time::ulong_t leapd[ nLeaps ] = {
        // 1980 + Std years + Leap years + Jan-Jul
        361 + 181, // Jul 1, 1981
        361 + 365 + 181, // Jul 1, 1982
        361 + 2 * 365 + 181, // Jul 1, 1983
        361 + 3 * 365 + 366 + 181, // Jul 1, 1985
        361 + 6 * 365 + 366, // Jan 1, 1988
        361 + 7 * 365 + 2 * 366, // Jan 1, 1990
        361 + 8 * 365 + 2 * 366, // Jan 1, 1991
        361 + 8 * 365 + 3 * 366 + 181, // Jul 1, 1992
        361 + 9 * 365 + 3 * 366 + 181, // Jul 1, 1993
        361 + 10 * 365 + 3 * 366 + 181, // Jul 1, 1994
        361 + 12 * 365 + 3 * 366, // Jan 1, 1996
        361 + 12 * 365 + 4 * 366 + 181, // Jul 1, 1997
        361 + 14 * 365 + 4 * 366, // Jan 1, 1999
        361 + 19 * 365 + 6 * 366, // Jan 1, 2006
        361 + 21 * 365 + 7 * 366, // Jan 1, 2009
        361 + 23 * 365 + 8 * 366 + 181, // Jul 1, 2012
        361 + 26 * 365 + 8 * 366 + 181, // Jul 1, 2015
        361 + 27 * 365 + 9 * 366 // Jan 1, 2017
    };

    //----------------------------------  Count the leap seconds in effect
    Time::ulong_t r( 0 );
    Time::ulong_t sec = t0.getS( );
    for ( int i = 0; i < nLeaps; i++ )
    {
        if ( sec >= leapd[ i ] * secpd + r )
            r++;
    }
    return r;
}

//-------------------------------------- << operator for output
ostream&
operator<<( ostream& out, const Time& t )
{
    char string[ 40 ];
    TimeStr( t, string, "%s:%n" );
    out << string;
    return out;
}

//--------------------------------------  Convert to a Unix time_t
Time::ulong_t
getUTC( const Time& t )
{
    return t.getS( ) + GPS2UTC( t );
}

//--------------------------------------  Convert from a Unix time_t
Time
fromUTC( Time::ulong_t t )
{
    Time::ulong_t t0 = GPS2UTC( Time( 0 ) );
    if ( t <= t0 )
        return Time( 0, 0 );
    Time::ulong_t t1 = GPS2UTC( Time( t - t0 ) );
    return Time( t - t1, 0 );
}

//--------------------------------------  Get the approximate current time
Time
Now( void )
{
#if !defined( HAVE_CLOCK_GETTIME )
    struct timeval now;
    if ( gettimeofday( &now, 0 ) != 0 )
    {
        return Time( 0, 0 );
    }
    return fromUTC( now.tv_sec ) + Interval( now.tv_usec / 1E6 );
#else
    struct timespec now;
    if ( clock_gettime( CLOCK_REALTIME, &now ) != 0 )
    {
        return Time( 0, 0 );
    }
    return fromUTC( now.tv_sec ) + Interval( now.tv_nsec / 1E9 );
#endif
}

//--------------------------------------  String conversion function
static char*       puti( char* s, unsigned int i, int w = 0, char pad = ' ' );
static const char* Mon[ 12 ] = {
    "January", "February", "March",     "April",   "May",      "June",
    "July",    "August",   "September", "October", "November", "December"
};
static const char* Day[ 7 ] = { "Sunday",   "Monday", "Tuesday", "Wednesday",
                                "Thursday", "Friday", "Saturday" };

inline void
scopy( const char* in, char*& out, int max = 0 )
{
    int num = max;
    for ( const char* p = in; *p && ( !max || num ); --num )
        *out++ = *p++;
}

static std::string
FmtString( const Time& t, const char* fmt, bool local )
{
    static bool is_tzset = false;
    if ( local && !is_tzset )
    {
        tzset( );
        is_tzset = true;
    }
    time_t     tSec = getUTC( t );
    struct tm* date = local ? localtime( &tSec ) : gmtime( &tSec );

    std::string str;
    const char* form = "%s:%n";
    if ( fmt )
        form = fmt;
    const char* fstart = form;

    for ( ; *form; )
    {
        char* pstr;
        if ( *form == '%' )
        {
            if ( form != fstart )
                str += std::string( fstart, form - fstart );
            form++;

            //--------------------------  Select ad character
            char pad = ' ';
            if ( *form == '0' )
            {
                form++;
                pad = '0';
            }

            //--------------------------  Get field width
            int width = 0;
            while ( *form >= '0' && *form <= '9' )
            {
                width *= 10;
                width += *form++ - '0';
            }

            //--------------------------  Interpret the format field.
            char number[ 32 ];
            if ( !*form )
                continue;
            switch ( *form )
            {
            case 'y':
                if ( !width )
                    width = 2;
                pstr = puti( number, date->tm_year % 100, width, '0' );
                break;
            case 'Y':
                pstr = puti( number, date->tm_year + 1900, width, pad );
                break;
            case 'm':
                pstr = puti( number, date->tm_mon + 1, width, pad );
                break;
            case 'M':
                pstr = number;
                scopy( Mon[ date->tm_mon ], pstr, width );
                break;
            case 'd':
                pstr = puti( number, date->tm_mday, width, pad );
                break;
            case 'D':
                pstr = puti( number, date->tm_yday, width, pad );
                break;
            case 'H':
                pstr = puti( number, date->tm_hour, width, pad );
                break;
            case 'N':
                if ( !width )
                    width = 2;
                pstr = puti( number, date->tm_min, width, '0' );
                break;
            case 'n':
                if ( !width )
                    width = 9;
                pstr = puti( number, t.getN( ), width, '0' );
                break;
            case 'S':
                if ( !width )
                    width = 2;
                pstr = puti( number, date->tm_sec, width, '0' );
                break;
            case 's':
                pstr = puti( number, t.getS( ), width, pad );
                break;
            case 'w':
                pstr = puti( number, date->tm_wday, width, pad );
                break;
            case 'W':
                pstr = number;
                scopy( Day[ date->tm_wday ], pstr, width );
                break;
            case 'Z':
                pstr = number;
                if ( !local )
                {
                    scopy( "UTC", pstr );
                }
                else if ( date->tm_isdst > 0 && daylight )
                {
                    scopy( tzname[ 1 ], pstr );
                }
                else
                {
                    scopy( tzname[ 0 ], pstr );
                }
                break;
            case 'z':
                pstr = number;
                if ( !local )
                {
                    scopy( " 0000", pstr );
                }
                else
                {
                    long abstz = timezone;
                    if ( abstz > 0 )
                    {
                        scopy( "-", pstr );
                    }
                    else
                    {
                        abstz = -abstz;
                        scopy( " ", pstr );
                    }
                    pstr = puti( pstr, abstz / 3600, 2, '0' );
                    pstr = puti( pstr, ( abstz / 60 ) % 60, 2, '0' );
                }
                break;
            default:
                pstr = number;
                *pstr++ = *form;
            }
            str += string( number, pstr - number );
            form++;
            fstart = form;
        }
        else if ( *form == '\\' )
        {
            if ( form != fstart )
                str += std::string( fstart, form - fstart );
            form++;
            if ( *form == '\\' )
            {
                str += '\\';
                form++;
            }
            else if ( *form == 'n' )
            {
                str += '\n';
                form++;
            }
            else if ( *form >= '0' && *form <= '9' )
            {
                str += strtol( form, &pstr, 0 );
                form = pstr;
            }
            else
            {
                str += '\\';
                str += *form++;
            }
            fstart = form;
        }
        else
        {
            form++;
        }
    }
    if ( form != fstart )
        str += std::string( fstart, form - fstart );
    return str;
}

static char*
FmtTime( const Time& t, char* s, const char* fmt, bool local )
{
    std::string str = FmtString( t, fmt, local );
    return strcpy( s, str.c_str( ) );
}

char*
LocalStr( const Time& t, char* str, const char* fmt )
{
    return FmtTime( t, str, fmt, true );
}

std::string
LocalString( const Time& t, const char* fmt )
{
    return FmtString( t, fmt, true );
}

char*
TimeStr( const Time& t, char* str, const char* fmt )
{
    return FmtTime( t, str, fmt, false );
}

std::string
TimeString( const Time& t, const char* fmt )
{
    return FmtString( t, fmt, false );
}

static char*
puti( char* s, unsigned int n, int w, char pad )
{
    if ( n / 10 )
        s = puti( s, n / 10, w - 1, pad );
    else
        for ( ; w > 1; w-- )
            *s++ = pad;
    *s++ = '0' + ( n % 10 );
    return s;
}
