/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "EggTimer.hh"
#include "Interval.hh"

//======================================  Construct a timer... Remember time
EggTimer::EggTimer( void )
{
    mStartTime = Now( );
}

//======================================  Destroy a timer
EggTimer::~EggTimer( void )
{
}

//======================================  Get the elapsed time.
Interval
EggTimer::elapsed( void ) const
{
    return Now( ) - mStartTime;
}
