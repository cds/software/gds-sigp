/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef TIMELINE_HH
#define TIMELINE_HH

#include "Time.hh"
#include "Interval.hh"
#include <vector>

/** The timeline class keeps a list of active (on) segments. 
 */
class timeline
{
public:
    /** %Time segment class represents a significant epoch in time.
    */
    struct time_seg
    {
        /** Construct a time segment from the start time and duration.
       *  \brief constructor
       *  \param t0 Segment start time
       *  \param dt Segment duration
       */
        time_seg( const Time& t0 = Time( 0 ), const Interval dt = 0.0 );

        /** Construct a time segment from the start and end times.
       *  \brief constructor
       *  \param t0 Segment start time
       *  \param t1 Segment end time
       */
        time_seg( const Time& t0, const Time& t1 );

        /**  Destroy a time segment.
       */
        ~time_seg( void ) = default;

        /**  Test whether two time segments share a boundary time.
       */
        bool adjacent( const time_seg& ts ) const;

        /**  Combine two overlapping or adjacent time segments to contain all 
        *  times contained by either segment.
	*/
        void combine( const time_seg& t0 );

        /**  Segment duration
       */
        Interval duration( void ) const;

        /** calculate segment end time.
       */
        Time end( void ) const;

        /** Calculate the intersection of two time segments to contain all times 
        * contained by both segments.
	*/
        void intersect( const time_seg& t0 );

        /** Empty test operator.
       */
        operator bool( void ) const;

        /** Calculate the segment overlap, \e i.e. the time contain by both this
       *  this segment and the argument segment.
       *  \brief Calculate the segment overlap.
       *  \param x Overlapping time segment.
       *  \return Segment containing overlapping times.
       */
        time_seg operator&( const time_seg& x ) const;

        /** Test whether the specified time comes before the start time of 
       *  this segment.
       *  \brief test whether time is before the segment.
       *  \param t time to be compared.
       *  \return true if the spcified time comes before this segment.
       */
        bool operator<( const Time& t ) const;

        /** test whether two segments overlap (non-adjacent).
       */
        bool overlap( const time_seg& ts ) const;

        /** start time of a segment.
       */
        Time start( void ) const;

        Time     _t0;
        Interval _dt;
    };

    /**  Static segment encompassing all times (gps 0-1999999999)
    */
    static const time_seg           all_times;
    typedef std::vector< time_seg > segvec;
    typedef segvec::const_iterator  const_timeline_iterator;
    typedef segvec::iterator        timeline_iterator;

public:
    /** construct an empty time-line.
    */
    timeline( void ) = default;

    /** Construct a copy of the argument time-line.
    *  \brief Copy contructor
    *  \param tl time-line to be copied
    */
    timeline( const timeline& tl ) = default;

    /** Move the argument timeline to this instance.
    *  \brief Move contructor
    *  \param tl time-line to be moved.
    */
    timeline( timeline&& tl ) = default;

    /** Destroy atime-line instance.
    *  \brief destructor
    */
    ~timeline( void ) = default;

    /** add a segment with the specified start time and duration to
    *  the time-line.
    *  \brief Add a segment to the time-line.
    *  \param t0 start time of segment.
    *  \param dt durationof segment.
    */
    void add_seg( const Time& t0, Interval dt );

    /** add a soecified segment to the time-line.
    *  \brief Add a segment to the time-line.
    *  \param t0 time segment to be added.
    */
    void add_seg( const time_seg& t0 );

    /** Return a reference to the last segment.
    */
    const time_seg& back( void ) const;

    /** Begin iterator of timeline list.
    */
    const_timeline_iterator begin( void ) const;

    /**  Scan the time line for empty or uncoalesced segments. A message is 
    *   printed if one or more such segments are found.
    */
    bool   check( void ) const;
    size_t count( const Time& t0, Interval dt ) const;
    bool   empty( void ) const;

    /** End point iterator of timeline list.
    */
    const_timeline_iterator end( void ) const;
    void                    erase_until( const Time& t0 );

    /** Return an iterator pointing to the %timeline first segment containing 
    *  a time greater than or equal to the argument time. If the specified 
    *  time is greater than the end time of all segment, the method returns 
    *  the end() iterator.
    *  \brief Find the segment containing the argument time.
    *  \param t Time closest to the segment to be found.
    */
    const_timeline_iterator find( const Time& t ) const;

    timeline        operator~( void ) const;
    timeline        operator&( const timeline& tl ) const;
    timeline        operator&( const time_seg& tl ) const;
    timeline        operator|( const timeline& tl ) const;
    const time_seg& operator[]( Time t ) const;

    Interval on_time( const time_seg& epoch = all_times ) const;
    Interval on_time( const Time& t0, Interval dt ) const;
    Interval off_time( const Time& t0, Interval dt ) const;
    size_t   size( void ) const;

private:
    segvec _timevect;
};

//======================================  Inline methods
inline timeline::time_seg::time_seg( const Time& t0, const Interval dt )
    : _t0( t0 ), _dt( dt )
{
}

inline timeline::time_seg::time_seg( const Time& t0, const Time& t1 )
    : _t0( t0 ), _dt( 0.0 )
{
    if ( t0 < t1 )
        _dt = t1 - t0;
}

inline bool
timeline::time_seg::adjacent( const time_seg& ts ) const
{
    return end( ) == ts.start( ) || start( ) == ts.end( );
}

inline void
timeline::time_seg::combine( const time_seg& ts )
{
    Time tEnd = end( );
    if ( ts.end( ) > tEnd )
        tEnd = ts.end( );
    if ( ts.start( ) < _t0 )
        _t0 = ts.start( );
    _dt = tEnd - _t0;
}

inline Interval
timeline::time_seg::duration( void ) const
{
    return _dt;
}

inline void
timeline::time_seg::intersect( const time_seg& ts )
{
    Time tEnd = end( );
    if ( ts.end( ) < end( ) )
        tEnd = ts.end( );
    if ( ts.start( ) > start( ) )
        _t0 = ts.start( );
    _dt = tEnd - start( );
}

inline Time
timeline::time_seg::end( void ) const
{
    return _t0 + _dt;
}

inline timeline::time_seg::operator bool( void ) const
{
    return _dt > Interval( 0.0 );
}

inline timeline::time_seg
timeline::time_seg::operator&( const time_seg& x ) const
{
    time_seg r( *this );
    r.intersect( x );
    return r;
}

inline bool
timeline::time_seg::operator<( const Time& t ) const
{
    return t < _t0;
}

inline bool
timeline::time_seg::overlap( const time_seg& ts ) const
{
    return end( ) > ts.start( ) && start( ) < ts.end( );
}

inline Time
timeline::time_seg::start( void ) const
{
    return _t0;
}

inline const timeline::time_seg&
timeline::back( void ) const
{
    return _timevect.back( );
}

inline timeline::const_timeline_iterator
timeline::begin( void ) const
{
    return _timevect.begin( );
}

inline bool
timeline::empty( void ) const
{
    return _timevect.empty( );
}

inline timeline::const_timeline_iterator
timeline::end( void ) const
{
    return _timevect.end( );
}

inline Interval
timeline::on_time( const time_seg& epoch ) const
{
    return on_time( epoch._t0, epoch._dt );
}

inline size_t
timeline::size( void ) const
{
    return _timevect.size( );
}

#endif // !defined(TIMELINE_HH)
