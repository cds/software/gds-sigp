/*  -*- mode: C++; c-basic-offset: 4; -*-   */
#ifndef INTERVAL_HH
#define INTERVAL_HH

#ifndef __CINT__
#define TICKS
#endif
#include <iosfwd>

#define DNSTOS 0.000000001
#define DSTONS 1000000000.

/** The %Interval class represents time intervals. This class complements
  * and is compatible with the Time class which represents absolute GPS 
  * times. At present, intervals are represented as double-precision 
  * floating point numbers. This gives about 53-bits to the representation 
  * which corresponds to a precision of 1 in $9x10^15$ or <1ns for 
  * intervals of up to ~100 days.
  * @memo Time interval class.
  * @author John. G. Zweizig
  * @version 1.1; Last Modified: June 15, 1999
  */
class Interval
{
public:
    ///  Data type used for integer seconds.
    typedef unsigned long ulong_t;

    /**  Construct an interval instance and initialize it to zero.
      *  \brief Default constructor.
      */
    Interval( void )
    {
        mSec = 0.0;
    }

    /**  Construct an %Interval from seconds and nanoseconds.
      *  \brief Second and nanosecond interval constructor.
      *  \param sec  Integer seconds in %Interval
      *  \param nsec Fractional nanoseconds in %Interval.
      */
    Interval( long sec, ulong_t nsec )
    {
        mSec = double( nsec ) * DNSTOS + double( sec );
    }

    /**  Construct an %interval and initialize it from a double (in seconds).
      *  \brief Construct an %Interval from double.
      *  \param sec %Interval value in seconds.
      */
    Interval( double sec )
    {
        mSec = sec;
    }

    //----------------------------------  Overload various operators.

    /**  Cast the %Interval to a double.
      *  \brief Convert to double.
      *  \return Double precision value of the %Interval in seconds.
      */
    operator double( void ) const
    {
        return mSec;
    }

    /**  Add an %Interval to this instance. The result replaces this
      *  instance and is returned in by the method.
      *  \brief Add an interval
      *  \param dt %Interval to be added.
      *  \returns Resulting sum interval.
      */
    Interval
    operator+=( const Interval& dt )
    {
        mSec += dt.mSec;
        return ( *this );
    }

    /**  Subtract an %Interval from this instance. The result replaces this
      *  instance and is returned in by the method.
      *  \brief Subtract an interval
      *  \param dt %Interval to be subtracted.
      *  \returns Resulting difference interval.
      */
    Interval
    operator-=( const Interval& dt )
    {
        mSec -= dt.mSec;
        return ( *this );
    }

    /**  Multiply the %Interval by a double constant. The result replaces 
      *  this instance and is returned from the method.
      *  \brief Multiply interval by a constant.
      *  \param c Constant by which the %Interval is to be multiplies.
      *  \return Product of the Interval times the constant.
      */
    Interval
    operator*=( double c )
    {
        mSec *= c;
        return ( *this );
    }

    /**  Divide an %Interval by an %Interval.
      *  \brief Ratio of two intervals.
      *  \param div Divisor interval.
      *  \returns Dimensionless result.
      */
    double
    operator/( const Interval& div ) const
    {
        return mSec / div.mSec;
    }

    /**  Divide an %Interval by a double constant
      *  \brief Divide an interval by a constant.
      *  \param dt Contant
      *  \returns Result interval.
      */
    Interval
    operator/=( double dt )
    {
        mSec /= dt;
        return ( *this );
    }

    /**  Compare the %Interval to zero and return true if so.
      *  \brief Test for zero.
      *  \return True if the %Interval is zero.
      */
    bool
    operator!( void ) const
    {
        return ( ticks( ) == 0 );
    }

    /**  Compare Intervals (equality)
      *  \brief %Interval comparison.
      *  \param rhs %Interval to be compared.
      *  \return True if this instance is equal to \a rhs.
      */
    bool
    operator==( const Interval& rhs ) const
    {
        return ( ticks( ) == rhs.ticks( ) );
    }

    /**  Compare Intervals (inequality)
      *  \brief %Interval comparison.
      *  \param rhs %Interval to be compared.
      *  \return True if this instance is not equal to \a rhs.
      */
    bool
    operator!=( const Interval& rhs ) const
    {
        return ( ticks( ) != rhs.ticks( ) );
    }

    /**  Compare Intervals (Greater or Equal)
      *  \brief %Interval comparison.
      *  \param rhs %Interval to be compared.
      *  \return True if this instance is greater than or equal to \a rhs.
      */
    bool
    operator>=( const Interval& rhs ) const
    {
        return ( ticks( ) >= rhs.ticks( ) );
    }

    /**  Compare Intervals (Less or equal)
      *  \brief %Interval comparison.
      *  \param rhs %Interval to be compared.
      *  \return True if this instance is less than or equal to \a rhs.
      */
    bool
    operator<=( const Interval& rhs ) const
    {
        return ( ticks( ) <= rhs.ticks( ) );
    }

    /**  Compare Intervals (Greater)
      *  \brief %Interval comparison.
      *  \param rhs %Interval to be compared.
      *  \return True if this instance is greater than \a rhs.
      */
    bool
    operator>( const Interval& rhs ) const
    {
        return ( ticks( ) > rhs.ticks( ) );
    }

    /**  Compare Intervals (Less than)
      *  \brief %Interval comparison.
      *  \param rhs %Interval to be compared.
      *  \return True if this instance is less than \a rhs.
      */
    bool
    operator<( const Interval& rhs ) const
    {
        return ( ticks( ) < rhs.ticks( ) );
    }

    //------------------------------------  Accessors.
    /** Returns the floor of the number of seconds in the interval. Note that
      * if the number is negative, the absolute value of the seconds field is
      * GREATER than the absolute value of the interval.
      * @memo Get seconds field.
      * @return The integer floor of the interval in seconds.
      */
    long
    GetS( ) const
    {
        if ( mSec >= 0 )
            return long( mSec );
        return long( mSec ) - 1;
    }

    /** Offset in nanoseconds from the seconds field defined by GetS().
      * Note that the nanosecond field is always positive.
      * @memo Get nano-second.
      * @return Number of nanoseconds offset from the seconds field.
      */
    ulong_t
    GetN( void ) const
    {
        return ulong_t( ( mSec - double( GetS( ) ) ) * DSTONS + 0.5 );
    }

    /**  Return the %Interval time in seconds.
      *  \brief Get the %Interval in seconds.
      *  \return number of seconds in seconds. 
      */
    double
    GetSecs( void ) const
    {
        return mSec;
    }

    /**  The interval is replaced by the specified number of seconds.
      *  \brief %Interval in seconds.
      *  \param s Number of seconds.
      */
    void
    SetS( ulong_t s )
    {
        mSec = s;
    }

    /**  Set nano-seconds fiels. The specified number of nanoseconds is added
      *  to the integer number of seconds in the %Interval.
      *  \brief Set the number of nanoseconds.
      *  \param n Number of nanoseconds.
      */
    void
    SetN( ulong_t n )
    {
        mSec = double( GetS( ) ) + ( DSTONS * double( n ) );
    }

    //------------------------------------  Data members.
private:
    /** Get interval in an easily manipulable representation.
     */
#ifdef TICKS
    long long
    ticks( void ) const
    {
        return (long long)( mSec * DSTONS + 0.5 );
    }
#else
    double
    ticks( void ) const
    {
        return mSec;
    }
#endif
    double mSec; // seconds
};

/**  @name %Interval Functions
  *  The non-member interval functions allow further manipulation of 
  *  time interval data.
  *  @memo Manipulation of time intervals.
  *  @author John G Zweizig
  *  @version 1.1; Last modified June 15, 1999
  */
//@{

/**  Two intervals are added together producing an %Interval.
  *  @memo Add two intervals.
  */
inline Interval
operator+( const Interval& t, const Interval& dt )
{
    Interval r = t;
    r += dt;
    return r;
}

/**  Difference of two intervals.
 */
inline Interval
operator-( const Interval& t, const Interval& dt )
{
    Interval r = t;
    r -= dt;
    return r;
}

/**  Multiply an %Interval by a double.
  *  The time interval is multiplied by an arbitrary number. Both the
  *  %Interval*double and double*%Interval operators exist.
  */
inline Interval
operator*( const Interval& t, double x )
{
    Interval r = t;
    r *= x;
    return r;
}

inline Interval
operator*( double x, const Interval& t )
{
    Interval r = t;
    r *= x;
    return r;
}

/**  Divide a time %Interval by a double.
  *  \brief Divine an %Interval by a number.
  */
inline Interval
operator/( const Interval& t, const double x )
{
    Interval r = t;
    r /= x;
    return r;
}

/** Compare interval to double
 */
inline bool
operator==( Interval a1, double a2 )
{
    return a1 == Interval( a2 );
}

inline bool
operator<( Interval a1, double a2 )
{
    return a1 < Interval( a2 );
}

inline bool
operator>( Interval a1, double a2 )
{
    return a1 > Interval( a2 );
}

inline bool
operator!=( Interval a1, double a2 )
{
    return !( a1 == a2 );
}

inline bool
operator>=( Interval a1, double a2 )
{
    return !( a1 < a2 );
}

inline bool
operator<=( Interval a1, double a2 )
{
    return !( a1 > a2 );
}

/**  Operator function to print an %Interval to an STL output stream.
  *  \brief Output an %Interval value.
  *  \param out Output stream
  *  \param t   %Interval to be written to stream
  *  \return  Reference to the output stream.
  */
std::ostream& operator<<( std::ostream& out, const Interval& t );

//@}

#endif // INTERVAL_HH
