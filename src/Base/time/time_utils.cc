/*  -*- mode: C++; c-basic-offset: 3; -*-   */
#include "time_utils.hh"
#include "Interval.hh"
#include <cerrno>
#include <time.h>

//=====================================  sleep_until
bool
sleep_until( const Time& t, bool allow )
{
    bool done = true;
    Time now = Now( );
    if ( now < t )
    {
        double          dt_left = double( t - now );
        struct timespec dt, left;
        dt.tv_sec = long( dt_left );
        dt.tv_nsec = ( dt_left - dt.tv_sec ) * 1.0e9;

        //--------------------------------  Sleep as appropriate.
        done = false;
        while ( !done )
        {
            if ( nanosleep( &dt, &left ) )
            {
                if ( errno != EINTR || allow )
                    break;
                dt = left;
            }
            else
            {
                done = true;
            }
        }
    }
    return done;
}
