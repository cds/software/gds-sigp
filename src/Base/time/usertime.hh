/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef USER_TIMER_HH
#define USER_TIMER_HH

/**  Measure user CPU time elapsed since the timer was constructed.
  */
class user_timer
{
public:
    /**  Construct a timer and record the current user time.
      *  @memo Constructor
      */
    user_timer( void );

    /**  Destroy the timer.
      *  @memo Destructor
      */
    ~user_timer( void );

    /**  Get the total user CPU time used by this process.
      *  @memo Get total user CPU time.
      *  @return Total user time.
      */
    double get_raw_time( void ) const;

    /**  Get the user CPU time used by this process since the timer
      *  was constructed or reset.
      *  @memo Get elapsed user CPU time.
      *  @return User CPU time in seconds.
      */
    double get_time( void ) const;

    /**  Set a new start time for measureing elapsed time.
      */
    void set_start( void );

private:
    double start;
};

#endif // !defined(USER_TIMER_HH)
