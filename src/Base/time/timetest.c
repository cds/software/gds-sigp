#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <time.h>
#ifndef HAVE_CLOCK_GETTIME
#include <sys/time.h>
#endif
#include "tconv.h"
#include <string.h>
/*#include "Time.hh"*/

#define SECS_PER_HOUR ( 60 * 60 )
#define SECS_PER_DAY ( SECS_PER_HOUR * 24 )
#define OFFS_TAI ( ( ( ( 72 - 58 ) * 365 + 3 ) * SECS_PER_DAY + 10 ) )

struct DIstruct
{
    int high, low;
};
typedef union
{
    struct DIstruct s;
    tainsec_t       ll;
} DIunion;

void
printLL( tainsec_t x )
{
    DIunion w;

    w.ll = x;
    printf( "result = %x | %x\n", w.s.high, w.s.low );
}

/* main program */
int
#ifdef OS_VXWORKS
timetest( void )
#else
main( int argc, char* argv[] )
#endif
{
    int       i;
    utc_t     d1, d2;
    time_t    t1, t2;
    taisec_t  tai0;
    tainsec_t tai1;
    leap_t    leap;
    char      buf[ 256 ] = "";
    char      buf2[ 256 ] = "";

    d1.tm_sec = 12;
    d1.tm_min = 35;
    d1.tm_hour = 16;
    d1.tm_mday = 29;
    d1.tm_mon = 3;
    d1.tm_year = 98;
    d1.tm_isdst = 1;
    d2 = d1;
    d2.tm_year = 72;

    htonTAI( 165036987364, buf );

    printf( "size of short: %i, int: %i and long: %i\n",
            (int)sizeof( short ),
            (int)sizeof( taisec_t ),
            (int)sizeof( tainsec_t ) );

    t1 = mktime( &d1 );
    t2 = mktime( &d2 );

    printf( "first = %li; second = %li\n", t1, t2 );

    printf( "TAI at %s = %li\n", asctime( &d1 ), UTCtoTAI( &d1 ) );
    printf( "TAI at Jan. 1, 1972 = %i\n", OFFS_TAI );

    tai0 = OFFS_TAI + 1 - TAIatGPSzero;
    if ( TAItoUTC( tai0, &d2 ) != NULL )
    {
        printf( "date/time = %s\n", asctime( TAItoUTC( tai0, &d2 ) ) );
    }

    tai0 += ( 34 * 365 + 9 ) * SECS_PER_DAY + 20;
    /*   tai = OFFS_TAI + 182 * SECS_PER_DAY - 1; */
    for ( i = 0; i < 5; ++i, ++tai0 )
    {
        strftime( buf, 100, "%b %d %X %Y", TAItoUTC( tai0, &d2 ) );
        printf( "tai = %li -> %s -> %li\n", tai0, buf, UTCtoTAI( &d2 ) );
        /*printf ("tai = %li -> %s -> %li (%i) -> %s\n", tai0, buf, UTCtoTAI (&d2), 
	         LeapS(Time(tai0)), TimeStr(Time(tai0), buf2, "%H:%N:%S %d %M %Y"));*/
    }

    tai1 = 1000000000 * (tainsec_t)tai0 + 500000000;
    memset( &d2, 0, sizeof( d2 ) );
    printf( "date/time = %s\n", asctime( TAIntoUTC( tai1, &d2 ) ) );

    /* test leap table */
    while ( 1 )
    {
        /*struct timespec wait = {10, 0};*/

        tai0 = 0;
        while ( getNextLeap( tai0, &leap ) != NULL )
        {
            strftime(
                buf, 100, "%b %d %X %Y", TAItoUTC( leap.transition, &d2 ) );
            printf( "leap at GPS = %10li, total correction = %2i - %s\n",
                    leap.transition,
                    leap.change,
                    buf );
            tai0 = leap.transition;
        }
        if ( 1 )
            break;
        /* nanosleep (&wait, NULL); */
    }

    /* check GPS zero */
    d1.tm_sec = 0;
    d1.tm_min = 0;
    d1.tm_hour = 00;
    d1.tm_mday = 6;
    d1.tm_mon = 0;
    d1.tm_year = 80;
    d1.tm_isdst = 0;
    tai0 = UTCtoTAI( &d1 );
    printf( "\n" );
    if ( tai0 != 0 )
        printf( "wrong GPS offset: difference = %li\n", tai0 - 0 );
    strftime( buf, 100, "%b %d %X %Y", TAItoUTC( 0, &d2 ) );
    printf( "GPS zero is at: TAI = %i which is %s\n", 0, buf );

#if 0
      {
         struct tm 		utc;
         tai_t			tai;

#if !defined( HAVE_CLOCK_GETTIME )
         struct timeval now;
         if (gettimeofday (&now, 0) != 0) {
            printf ("gettimeofday failed\n");
         }
         else {
            printf ("gettimeofday successful\n");
            printf ("sec = %li, nsec = %li\n", now.tv_sec, now.tv_usec*1E3);
         }
#else
         struct timespec 	now;    
         if (clock_gettime (CLOCK_REALTIME, &now) != 0) {
            printf ("clock_gettime failed\n");
         }
         else {
            printf ("clock_gettime successful\n");
            printf ("sec = %li, nsec = %li\n", now.tv_sec, now.tv_nsec);
         }
#endif      
         if (gmtime_r (&now.tv_sec, &utc) != 0) {
            printf ("gmtime failed\n");
         }
         tai.tai = UTCtoTAI (&utc);
         if (tai.tai == 0) {
            tai.tai = now.tv_sec;
         };
#if !defined( HAVE_CLOCK_GETTIME )
        tai.nsec = now.tv_usec*1E3;
#else
        tai.nsec = now.tv_nsec;
#endif     
        printLL (*((tainsec_t*) &tai));
      
         tai1 = TAInow();
         printLL (tai1);
      
         printf ("time now is %li\n", time(NULL));
      }
#endif

    printf( "TAI now = " );
    printLL( TAInow( ) );
    printf( "date/time = %s", asctime( TAIntoUTC( TAInow( ), &d2 ) ) );
    printf( "GPS sec = %ld\n\n", (unsigned long)( TAInow( ) / _ONESEC ) );

#ifndef OS_VXWORKS
    if ( argc > 1 )
    {
        FILE* fp = fopen( argv[ 1 ], "w" );
        if ( fp != NULL )
        {
            /* write leap table file */
            fprintf( fp,
                     "# LEAP SECOND INFORMATION\n"
                     "# GPS (sec)\tleap\tdate\n" );
            tai0 = 0;
            while ( getNextLeap( tai0, &leap ) != NULL )
            {
                strftime(
                    buf, 100, "%b %d %X %Y", TAItoUTC( leap.transition, &d2 ) );
                fprintf( fp,
                         "%11li\t%2i\t# %s\n",
                         leap.transition,
                         leap.change,
                         buf );
                tai0 = leap.transition;
            }
            fclose( fp );
        }
    }
#endif

    exit( EXIT_SUCCESS );
    return 0;
}
