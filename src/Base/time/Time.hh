/*  -*- mode: C++; c-basic-offset: 4; -*-   */
#ifndef TIME_HH
#define TIME_HH

#include <iosfwd>

class Interval;

/**  The time class contains a GPS time in seconds and nanoseconds since 
  *  the start of the GPS epoch (midnight, January 6, 1980). The %Time class 
  *  is closely coupled with the Interval class which represents time 
  *  intervals. All basic arithmetic operations are defined between the two
  *  classes. Ancillary (non-member) functions convert the GPS time to other
  *  representations
  *  @memo GPS time container class.
  *  @author John G. Zweizig
  *  @version 1.1; Modified: August 23, 1999
  */
class Time
{
public:
    /// Data type for integer seconds and nanoseconds.
    typedef unsigned long ulong_t;

    /** A %Time instance is created and initialized to %Time(0)
      * @memo Default constructor
      */
    Time( void );

    /** GPS time is constructed from seconds and nanoseconds. If the
      * nanosecond field isn't specified, it is assumed to be zero.
      * @memo Constructor from seconds and nanoseconds.
      * @param sec Number of GPS seconds since midnight, Jan 6, 1980.
      * @param nsec Number of nanoseconds since last GPS second.
      */
    explicit Time( ulong_t sec, ulong_t nsec = 0 );

    //----------------------------------  Overload various operators.
    /**  Test if the time is non-zero.
      *  \brief bool conversion operator
      *  \return true if the time is valid (non-zero).
      */
    bool ne_0( void ) const;

    /**  Assign the value of the argument %Time to the current %Time instance.
      *  \brief memo Assignment operator
      *  \param t %Time to be assigned to this instance.
      *  \return reference to this instance.
      */
    Time& operator=( const Time& t );

    /**  Add an Interval to a %Time.
      *  \brief Add an interval.
      *  \param dt Interval to be added.
      *  \return Reference to result (this instance).
      */
    Time& operator+=( const Interval& dt );

    ///  Subtract a %Time rhs.
    Interval operator-( const Time& dt ) const;

    ///  Subtract a n interval rhs
    Time& operator-=( const Interval& dt );

    ///  Compare to zero
    bool operator!( void ) const;

    ///  Compare times (equality)
    bool operator==( const Time& rhs ) const;

    ///  Compare times (inequality)
    bool operator!=( const Time& rhs ) const;

    ///  Compare times (Greater or Equal)
    bool operator>=( const Time& rhs ) const;

    ///  Compare times (Less or equal)
    bool operator<=( const Time& rhs ) const;

    ///  Compare times (Greater)
    bool operator>( const Time& rhs ) const;

    ///  Compare times (Less than)
    bool operator<( const Time& rhs ) const;

    /** Boolean cast operator.
     */
    operator bool( void ) const;

    //------------------------------------  Accessors.
    /**  Get the GPS second boundary.
     *  @memo Number of seconds.
     *  @return The number of seconds since a GPS epoch.
     */
    ulong_t getS( void ) const;

    /**  Get the number of nanoseconds since a GPS second boundary.
     *  @memo Number of nanoseconds.
     *  @return The number of nanoseconds since a GPS second.
     */
    ulong_t getN( void ) const;

    /**  Get the GPS time in seconds since the start of the GPS epoch.
     *  @memo Seconds since GPS 0.
     *  @return The number of GPS seconds.
     */
    double totalS( void ) const;

    /**  Get the GPS time in nanoseconds since t0.
     *  @memo Nanoseconds since GPS 0.
     *  @return The number of GPS nanoseconds.
     */
    double totalNS( void ) const;

    /**  Get the fraction of a second after an integer number of GPS seconds.
      *  \brief Get the fractional seconds.
      *  \return Fractional seconds.
      */
    double fracS( void ) const;

    /**  Set the number of seconds past the start of the second GPS epoch.
      *  \brief Set seconds field.
      *  \param s Number of seconds since start of GPS epoch.
      */
    void
    setS( ulong_t s )
    {
        mSec = s;
    }

    /**  Set the number of nanoseconds offset from the seconds field.
      *  \brief Set nanosecond field.
      *  \param n Number of nanoseconds.
      */
    void setN( ulong_t n );

    //------------------------------------  Data members.
private:
    ulong_t mSec; // seconds
    ulong_t mNsec; // nano-seconds
};

//----------------------------------------  Non-member operators & functions
/** \file
  * Functions for the translation and manipulation of %Time objects.
  * \brief %Time translation and manipulation functions.
  * \author John G. Zweizig <john.zweizig@ligo.org>
  * \version $Id$
  */
//@{

/**  An interval is added to a GPS time resulting in a GPS time.
  *  @memo Add an interval to a GPS time.
  *  @return Starting time offset by the time increment.
  *  @param t  Starting time.
  *  @param dt Signed time increment.
  */
Time operator+( const Time& t, const Interval& dt );

/**  An interval is added to a GPS time resulting in a GPS time.
  *  @memo Add an interval to a GPS time.
  *  @return Starting time offset by the time increment.
  *  @param t  Starting time.
  *  @param dt Signed time increment.
  */
Time operator+( const Interval& dt, const Time& t );

/**  An Interval is subtracted from a GPS time resulting in a GPS time.
  *  \brief Subtract an interval from a GPS time.
  *  \param  t %Time from which the interval is to be subtracted.
  *  \param dt %Interval to be subtracted.
  *  \return Resulting time.
  */
Time operator-( const Time& t, const Interval& dt );

/**  The date and time (to the nearest second) are printed to the 
  *  output stream.
  *  \brief Output formatted time to ostream.
  *  \param out Reference to output strem
  *  \param   t %Time to be written to the output steram.
  *  \return Reference to the output stream.
  */
std::ostream& operator<<( std::ostream& out, const Time& t );

/**  Two times are compared for equality within dT nanoseconds.
 *  @memo Compare two times for almost equal.
 *  @return true if the two times are equal within the specified tolerance.
 *  @param t1 First time.
 *  @param t2 Second time.
 *  @param dT Maximum time difference in namoseconds.
 */
bool Almost( const Time& t1, const Time& t2, Time::ulong_t dT = 1 );

/**  The current system time (UTC) is converted to a GPS time and returned 
 *  a time object. Note that since the system clock is used, the accuracy
 *  is highly system dependent and the the time returned is at best an 
 *  approximate current time.
 *  @memo Approximate GPS time.
 *  @return The approximate system time as a %Time object.
 */
Time Now( void );

/**  Define time format strings:
  *
  *  * TIMESTR_FORMAT_DATE    is the default format for the shell date command,
  *    \e e.g. "Sun Jan  6  0:00:01 UTC 1980"
  *  * TIMESTR_FORMAT_RFC2822 format to produce rfc-2822 (UTC) date strings,
  *    \e e.g. "Sun, 06 Jan 1980  0:00:01  0000"
  */
#define TIMESTR_FORMAT_DATE "%3W %3M %2d %2H:%02N:%02S %Z %Y"
#define TIMESTR_FORMAT_RFC2822 "%3W, %02d %3M %Y %02H:%02N:%02S %z"

/**  The time argument is converted to a UTC date and time string and stored
  *  in 'str'. The optional format string describes how the string is to be 
  *  converted. The format contains arbitrary characters interspersed with 
  *  escaped format specifiers that cause the appropriate date information 
  *  to be inserted. Each escaped format specifier is of the form 
  *  "%[0][n]<fmt>". If a leading "0" is specified, numeric fields are padded 
  *  with zeroes if necessary. If a numeric field width is specified, the 
  *  field is padded on the left with blanks or truncated as appropriate to 
  *  give the correct field length. The following table lists the format 
  *  codes and their meaning:
  *
  *  <table>
  *  <tr><th>Format</th><th>Meaning</th>
  *      <th>Format</th><th>Meaning</th>
  *      <th>Format</th><th>Meaning</th></tr>
  *  <tr><td> \%d  </td><td>Day of month</td>
  *      <td> \%D  </td><td>Day of year</td>
  *      <td> \%H  </td><td>Hour of day</td></tr>
  *  <tr><td> \%m  </td><td>Month number</td>
  *      <td> \%M  </td><td>Month name</td>
  *      <td> \%n  </td><td>Nanoseconds</td></tr>
  *  <tr><td> \%N  </td><td>Minute in hour</td>
  *      <td> \%S  </td><td>Seconds</td>
  *      <td> \%s  </td><td>GPS seconds</td></tr>
  *  <tr><td> \%w  </td><td>Day number in week</td>
  *      <td> \%W  </td><td>Week day</td>
  *      <td> \%y  </td><td>Year in century</td></tr>
  *  <tr><td> \%Y  </td><td>Year</td>
  *      <td> \%z  </td><td>%Time zone offset from UTC ([-]hhmm)</td>
  *      <td> \%Z  </td><td>%Time zone</td></tr>
  *  </table>
  *
  *  For example, a format like fmt="Today's date is %02d %3M %Y" would
  *  produce a string like "Today's date is 09 Dec 2003". If no format is 
  *  specified, "%s:%n" is used.
  *  @memo Convert the specified time to a date string.
  *  @return Returns the output string pointer or Null upon failure.
  *  @param t %Time to be converted
  *  @param str Character array into which the string will be stored.
  *  @param format Format string (defaults to "%s:%n")
  */
char* TimeStr( const Time& t, char* str, const char* format = 0 );

/**  Convert time to a string according to a specified format using the same
  *  format string meaning as TimeStr.
  *  \see TimeStr()
  *  \brief Convert time to a string.
  *  \param t      %Time to be converted.
  *  \param format Conversion format
  *  \return String containing time and other formatted text
  */
std::string TimeString( const Time& t, const char* format = 0 );

/**  The time argument is converted to a local date and time string and 
  *  stored in 'str'. The format is specified as for TimeStr().
  *  @memo Convert the specified time to a local time string.
  *  @return Returns the output string pointer or Null upon failure.
  *  @param t %Time to be converted
  *  @param str Character array into which the string will be stored.
  *  @param format Format string.
  */
char* LocalStr( const Time& t, char* str, const char* format );

/**  Convert the specified time to a local time string according to a format 
  *  using the same format string meaning as TimeStr.
  *  \see TimeStr()
  *  \brief Convert time to a local time string.
  *  \param t      %Time to be converted.
  *  \param format Conversion format
  *  \return String containing time and other formatted text
  */
std::string LocalString( const Time& t, const char* format = 0 );

/**  Convert GPS Time to a UTC (e.g. Unix time_t) time
  *  @memo convert to UTC time.
  *  @param t Time to be converted to UTC
  *  @return UTC seconds in Unix time_t format.
  */
Time::ulong_t getUTC( const Time& t );

/**  Get the number of leap seconds since the start of the GPS epoch
 *  (Jan 6, 1980) for a given GPS time. This differs from the total 
 *  number of leap seconds by 19 - 10 were defined into TAI on Jan 1, 1970
 *  and 9 more were added between Jan 1, 1970 and Jan 6, 1980.
 *  @memo Number of leap seconds since the start of the GPS epoch.
 *  @return Number of leap seconds in effect at the specified time.
 *  @param t %Time at which leap seconds are evaluated.
 */
Time::ulong_t LeapS( const Time& t );

/**  Number of leap seconds defined before the start of the GPS epoch.
  *  10 were defined into TAI on Jan 1, 1970 and 9 more were added between 
  *  Jan 1, 1970 and Jan 6, 1980.
  */
static const Time::ulong_t Leap0( 19 );

/**  Get a %Time from a UTC (\e e.g. Unix time_t) time.
 *  @memo UTC time from Unix time.
 *  @param t  %Time to convert from UTC.
 *  @return GPS time.
 */
Time fromUTC( Time::ulong_t t );

//@}

//----------------------------------------  Compare to zero
inline bool
Time::operator!( void ) const
{
    return !mSec && !mNsec;
}

//----------------------------------------  Compare (equal, greater, less)
inline bool
Time::operator==( const Time& rhs ) const
{
    return ( mSec == rhs.mSec && mNsec == rhs.mNsec );
}

inline bool
Time::operator>( const Time& rhs ) const
{
    return ( mSec > rhs.mSec || ( mSec == rhs.mSec && mNsec > rhs.mNsec ) );
}

inline bool
Time::operator<( const Time& rhs ) const
{
    return ( mSec < rhs.mSec || ( mSec == rhs.mSec && mNsec < rhs.mNsec ) );
}

//----------------------------------------  Compare (derived from above)
inline bool
Time::operator!=( const Time& rhs ) const
{
    return !operator==( rhs );
}

inline bool
Time::operator>=( const Time& rhs ) const
{
    return !( operator<( rhs ) );
}

inline bool
Time::operator<=( const Time& rhs ) const
{
    return !( operator>( rhs ) );
}

//----------------------------------------  bool conversion
inline Time::operator bool( void ) const
{
    return mSec || mNsec;
}

inline bool
Time::ne_0( void ) const
{
    return bool( *this );
}

//----------------------------------------  Accessors.
inline Time::ulong_t
Time::getS( void ) const
{
    return mSec;
}

inline Time::ulong_t
Time::getN( void ) const
{
    return mNsec;
}

#endif //  TIME_HH
