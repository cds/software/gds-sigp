#include "TimeStamp.hh"

//======================================  Default constructor.
TimeStamp::TimeStamp( void )
{
    set( );
}

//======================================  Destructor
TimeStamp::~TimeStamp( void )
{
}

//======================================  Set the time.
TimeStamp&
TimeStamp::set( void )
{
    return set( Now( ) );
}

//======================================  Set the time.
TimeStamp&
TimeStamp::set( const Time& t )
{
    mT0 = t;
    mValid = false;
    return *this;
}

//======================================  Get a formatted string.
const char*
TimeStamp::getString( ) const
{
    if ( !mValid )
    {
        LocalStr( mT0, mStamp, "%Y.%02m.%02d-%02H:%02N:%02S" );
        mValid = true;
    }
    return mStamp;
}
