/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef EGG_TIMER_HH
#define EGG_TIMER_HH

#include "Time.hh"

/**  The EggTimer class measures the elapsed wall-clock interval from when 
  *  it is created. The current time is save on creation and the user may
  *  fetch the elapsed time at various pionts after.
  *  @memo Measure elapsed real-time from timer creation.
  *  @author J. Zweiizg
  *  @version 1.1; Last updated  July 6, 2009
  */
class EggTimer
{
public:
    /**  Construct a timer.
      *  @memo Constructor.
      */
    EggTimer( void );

    /**  Destroy a timer.
      *  @memo Destructor
      */
    ~EggTimer( void );

    /**  Return elapsed time since the timer was constructed.
      *  @memo Elapsed time
      *  @return Elapsed time Interval.
      */
    Interval elapsed( void ) const;

private:
    Time mStartTime;
};

#endif // !defined(EGG_TIMER_HH)
