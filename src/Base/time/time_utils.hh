/*  -*- mode: C++; c-basic-offset: 4; -*-   */
#ifndef GDS_TIME_UTILS
#define GDS_TIME_UTILS

#include "Time.hh"

/**  \file
  *  Timing utility functions;
  *  \brief %Time utility functions.
  *  \author John G. Zweizig <john.zweizig@ligo.org>
  */
//@{

/**  Sleep until the specified time. This function allow an arbitrarily long
  *  sleep time and can optionally be interrupted by a signal. The function 
  *  returns true if the sleep has successfully completed. If the return
  *  value is false, errno should be set to EINTR indicating that the sleep
  *  was interrupted by a signal. Any other error code wouild indicate a
  *  logic error.
  *  \brief Sleep until the specified time
  *  \param t     %Time to wake up
  *  \param allow Allow sleep to be interupted by a signal.
  *  \return True if time  is complete.
  */
bool sleep_until( const Time& t, bool allow = false );

#endif // !defined(GDS_TIME_UTILS)
