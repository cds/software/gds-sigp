/* -*- mode: c++; c-basic-offset: 3; -*- */

#include "timeline.hh"
#include <stdexcept>
#include <iostream>
#include <algorithm>

using namespace std;

const timeline::time_seg timeline::all_times( Time( 0 ),
                                              Interval( 1999999999.0 ) );

//======================================  Add a segment to a timeline method
void
timeline::add_seg( const Time& t0, Interval dt )
{
    add_seg( time_seg( t0, dt ) );
}

//=====================================  Add a segment to a timeline method
void
timeline::add_seg( const time_seg& seg )
{
    if ( !seg )
        return;
    if ( _timevect.empty( ) )
    {
        _timevect.push_back( seg );
    }
    time_seg& last( _timevect.back( ) );
    if ( seg.start( ) < last.start( ) )
    {
        throw runtime_error( "timeline: Segment added out of order." );
    }
    if ( last.adjacent( seg ) || last.overlap( seg ) )
        last.combine( seg );
    else
        _timevect.push_back( seg );
}

//======================================  check the timeline
bool
timeline::check( void ) const
{
    if ( empty( ) )
        return false;
    size_t mtseg( 0 ), uncseg( 0 );
    size_t N = size( );
    for ( size_t i = 0; i < N; i++ )
    {
        if ( !_timevect[ i ] )
            mtseg++;
        if ( i + 1 < N && _timevect[ i ].end( ) >= _timevect[ i + 1 ].start( ) )
            uncseg++;
    }
    if ( mtseg || uncseg )
    {
        cerr << "timeline: Check found " << mtseg << " empty and " << uncseg
             << " uncoalesced segments." << endl;
        return true;
    }
    return false;
}

//======================================  count number of entries operator
size_t
timeline::count( const Time& t0, Interval dt ) const
{
    size_t             rcnt = 0;
    timeline::time_seg limits( t0, dt );
    for ( auto i = _timevect.begin( ); i != _timevect.end( ); i++ )
    {
        if ( i->overlap( limits ) )
            rcnt++;
    }
    return rcnt;
}

//======================================  Inversion operator
timeline
timeline::operator~( void ) const
{
    timeline r;
    if ( empty( ) )
    {
        r.add_seg( all_times );
        return r;
    }
    time_seg last;
    for ( auto i = _timevect.begin( ); i != _timevect.end( ); i++ )
    {
        if ( i->start( ) != last.end( ) )
        {
            r.add_seg( last.end( ), i->start( ) - last.end( ) );
        }
        last = *i;
    }
    if ( last.end( ) != all_times.end( ) )
    {
        r.add_seg( last.end( ), all_times.end( ) - last.end( ) );
    }
    return r;
}

//======================================  Logical and operator
timeline
timeline::operator&( const timeline& tl ) const
{
    timeline r;
    if ( empty( ) || tl.empty( ) )
        return r;
    size_t i = 0, j = 0;
    size_t sz_i = size( ), sz_j = tl.size( );
    while ( i < sz_i && j < sz_j )
    {
        if ( _timevect[ i ].overlap( tl._timevect[ j ] ) )
        {
            time_seg ts = _timevect[ i ];
            ts.intersect( tl._timevect[ j ] );
            r.add_seg( ts );
        }
        if ( _timevect[ i ].end( ) < tl._timevect[ j ].end( ) )
            i++;
        else
            j++;
    }
    return r;
}

//======================================  Logical and operator
timeline
timeline::operator&( const time_seg& ts ) const
{
    timeline r;
    if ( empty( ) || !ts )
        return r;
    for ( auto tv : _timevect )
    {
        r.add_seg( tv & ts ); // Adding a null segment is a null operation
    }
    return r;
}

//======================================  Logical or operator
timeline
timeline::operator|( const timeline& tl ) const
{
    if ( empty( ) )
        return tl;
    if ( tl.empty( ) )
        return *this;
    timeline r;
    size_t   i = 0, j = 0;
    size_t   sz_i = size( ), sz_j = tl.size( );
    time_seg rsg;
    while ( i < sz_i && j < sz_j )
    {
        const time_seg& tsi( _timevect[ i ] );
        const time_seg& tsj( tl._timevect[ j ] );
        if ( tsi.start( ) < tsj.start( ) )
        {
            if ( !rsg )
            {
                rsg = tsi;
            }
            else if ( rsg.overlap( tsi ) || rsg.adjacent( tsi ) )
            {
                rsg.combine( tsi );
            }
            else
            {
                r.add_seg( rsg );
                rsg = tsi;
            }
            i++;
            if ( rsg.overlap( tsj ) || rsg.adjacent( tsj ) )
            {
                rsg.combine( tsj );
                j++;
            }
        }
        else
        {
            if ( !rsg )
            {
                rsg = tsj;
            }
            else if ( rsg.overlap( tsj ) || rsg.adjacent( tsj ) )
            {
                rsg.combine( tsj );
            }
            else
            {
                r.add_seg( rsg );
                rsg = tsj;
            }
            j++;
            if ( rsg.overlap( tsi ) || rsg.adjacent( tsi ) )
            {
                rsg.combine( tsi );
                i++;
            }
        }
    }
    for ( ; i < sz_i; i++ )
        r.add_seg( _timevect[ i ] );
    for ( ; j < sz_j; j++ )
        r.add_seg( tl._timevect[ j ] );
    return r;
}

//======================================  Find closest segment
const timeline::time_seg&
timeline::operator[]( const Time t ) const
{
    static time_seg null;
    if ( empty( ) )
        return null;
    auto p = upper_bound(
        begin( ), end( ), t, []( Time a, const time_seg& b ) -> bool {
            return a < b.end( );
        } );
    if ( p == end( ) )
        return null;
    return *p;
}

//======================================  Inversion operator
void
timeline::erase_until( const Time& t0 )
{
    auto j = _timevect.begin( );
    for ( auto i = j; i != _timevect.end( ); i++ )
    {
        Time tEnd = i->end( );
        if ( tEnd <= t0 )
        {
            continue;
        }
        else if ( i->_t0 < t0 )
        {
            j->_t0 = t0;
            j->_dt = tEnd - t0;
        }
        else if ( j != i )
        {
            *j = *i;
        }
        j++;
    }
}

//======================================  Inversion operator
Interval
timeline::on_time( const Time& t0, Interval dt ) const
{
    Time     tEnd = t0 + dt;
    Interval dtot = 0.0;
    for ( auto tv : _timevect )
    {
        Time beg_i = tv.start( );
        Time end_i = tv.end( );
        if ( end_i <= t0 || beg_i >= tEnd )
            continue;
        if ( beg_i < t0 )
            beg_i = t0;
        if ( end_i > tEnd )
            end_i = tEnd;
        dtot += end_i - beg_i;
    }
    return dtot;
}

//======================================  measure the off time
Interval
timeline::off_time( const Time& t0, Interval dt ) const
{
    return ( ~*this ).on_time( t0, dt );
}
