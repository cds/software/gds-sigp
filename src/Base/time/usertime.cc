/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "usertime.hh"
#include <sys/times.h>
#include <unistd.h>
#include <limits.h>

user_timer::user_timer( void )
{
    set_start( );
}

user_timer::~user_timer( void )
{
}

void
user_timer::set_start( void )
{
    start = get_raw_time( );
}

double
user_timer::get_time( void ) const
{
    return get_raw_time( ) - start;
}

double
user_timer::get_raw_time( void ) const
{
    struct tms t;
    times( &t );
    return t.tms_utime / double( sysconf( _SC_CLK_TCK ) );
}
