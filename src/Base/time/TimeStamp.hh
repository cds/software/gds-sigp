#ifndef TIMESTAMP_HH
#define TIMESTAMP_HH

#include "Time.hh"
#include <iostream>

//======================================  TimeStamp Class
/**  Maintain a printable time-stamp
  *  @author John Zweizig
  *  @version $Revision$; Modified $Date$
  */
class TimeStamp
{
public:
    /**  Construct a %TimeStamp instance and initialize it to the current
    *  time.
    *  \brief Default constructor
    */
    TimeStamp( void );

    /**  Destroy a timeStamp object.
    *  \brief Destructor
    */
    ~TimeStamp( void );

    /**  Set the timestamp time to the current wall time.
    *  \brief Set the time to now.
    *  \returns Reference to modified instance.
    */
    TimeStamp& set( void );

    /**  Set the timestamp to the specified time.
    *  \brief Set the time.
    *  \param t Time to which the time stamp is to be set.
    *  \returns Reference to modified instance.
    */
    TimeStamp& set( const Time& t );

    /**  Get the time as a printable string in the standard yyyy.m.dd-hh.mm.ss 
    *  form.
    *  \brief Get a printable time string.
    *  \return Pointer to ta current time string.
    */
    const char* getString( ) const;

    /**  Get most recent set time.
    *  \brief Get the most recent time
    *  \returns Most recent set time.
    */
    Time getTime( void ) const;

private:
    Time         mT0;
    mutable bool mValid;
    mutable char mStamp[ 32 ];
};

inline Time
TimeStamp::getTime( void ) const
{
    return mT0;
}

inline std::ostream&
operator<<( std::ostream& out, const TimeStamp& t )
{
    return out << t.getString( );
}

#endif // TIMESTAMP_HH
