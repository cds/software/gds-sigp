
#define _LIGO_MMAP_CC
#include "mmap.hh"
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

namespace gdsbase
{

    //______________________________________________________________________________
    mmap::mmap( ) : fFileMap( false ), fData( 0 ), fLength( 0 )
    {
    }
    //______________________________________________________________________________
    mmap::mmap( const char* filename, std::ios_base::openmode which )
        : fFileMap( false ), fData( 0 ), fLength( 0 )
    {
        if ( map_file( filename, fData, fLength, which ) )
        {
            fFileMap = true;
        }
    }

    //______________________________________________________________________________
    mmap::mmap( pointer_type p, size_type len )
        : fFileMap( false ), fData( 0 ), fLength( 0 )
    {
        fData = p;
        fLength = len;
    }

    //______________________________________________________________________________
    mmap::~mmap( )
    {
        if ( filemap( ) )
        {
            unmap_file( fData, fLength );
        }
    }

    //______________________________________________________________________________
    bool
    mmap::map_file( const char*             filename,
                    pointer_type&           addr,
                    size_type&              len,
                    std::ios_base::openmode which )
    {
        // open mode
        int prot = 0;
        if ( which & std::ios_base::in )
            prot |= PROT_READ;
        if ( which & std::ios_base::out )
            prot |= PROT_WRITE;
        // open file
        int fd = ::open( filename,
                         ( which & std::ios_base::out ) ? O_RDWR : O_RDONLY );
        if ( fd == -1 )
        {
            return false;
        }
        // get its size
        struct stat info;
        int         ret = ::fstat( fd, &info );
        if ( ret )
        {
            return false;
        }
        // map file into memory
        void* data = ::mmap( 0, info.st_size, prot, MAP_SHARED, fd, 0 );
        // QFS requires the exec flag to be set, so try again
        if ( data == MAP_FAILED )
        {
            data =
                ::mmap( 0, info.st_size, prot | PROT_EXEC, MAP_SHARED, fd, 0 );
        }
        ::close( fd );
        if ( data == MAP_FAILED )
        {
            return false;
        }
        // set values and return
        addr = data;
        len = info.st_size;
        return true;
    }

    //______________________________________________________________________________
    bool
    mmap::unmap_file( pointer_type addr, size_type len )
    {
        return ( ::munmap( addr, len ) == 0 );
    }

} // namespace gdsbase
