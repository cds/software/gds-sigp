#ifndef GDS_CONSTANT_HH
#define GDS_CONSTANT_HH
static const double pi = 3.14159265358979323846;
static const double twopi = 6.28318530717958647692;
static const double piovr2 = 1.57079632679489661923;
#endif // !defined(GDS_CONSTANT_HH)
