/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "thread_base.hh"
#include <signal.h>
#include <stdexcept>
#include <iostream>

using namespace thread;
using namespace std;

//======================================  Construct a thread base
void*
thread_base::launch( void* p )
{
    return reinterpret_cast< thread_base* >( p )->thread_stub( );
}

//======================================  Construct a thread base
thread_base::thread_base( void ) : mRun( false ), mBusy( false ), mThreadID( 0 )
{
    if ( pthread_attr_init( &mThreadAttr ) != 0 )
    {
        throw runtime_error( "Error initializing thread attributes" );
    }
}

//======================================  Destroy a thread
thread_base::~thread_base( void )
{
    if ( mBusy )
        kill_thread( SIGKILL );
    pthread_attr_destroy( &mThreadAttr );
}

//======================================  Wait for thread to complete
int
thread_base::join( void** retval ) const
{
    return pthread_join( mThreadID, retval );
}

//======================================  Issue a signal to the specified thread
int
thread_base::kill_thread( int sig )
{
    return pthread_kill( mThreadID, sig );
}

//======================================  Run the thread
void
thread_base::set_detached( bool d )
{
    if ( d )
        pthread_attr_setdetachstate( &mThreadAttr, PTHREAD_CREATE_DETACHED );
    else
        pthread_attr_setdetachstate( &mThreadAttr, PTHREAD_CREATE_JOINABLE );
}

//======================================  Run the thread
int
thread_base::start_thread( void )
{

    //----------------------------------  Create the thread
    int err = pthread_create( &mThreadID, &mThreadAttr, &launch, this );
    return err;
}

//======================================  Run the thread
int
thread_base::stop_thread( void )
{
    mRun = false;
    return 0;
}

//======================================  Stub function
void*
thread_base::thread_stub( void )
{
    mRun = true;
    mBusy = true;
    void* err = 0;
    try
    {
        err = thread_entry( );
    }
    catch ( exception& e )
    {
        cerr << "thread_base: Caught exception (" << e.what( )
             << ") in thread_entry." << endl;
    }
    mBusy = false;
    return err;
}
