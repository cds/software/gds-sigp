/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef THREAD_GATEWAY_HH
#define THREAD_GATEWAY_HH

#include "gmutex.hh"

namespace thread
{

    /**  The %gateway class controls thread access to allow serial use
      *  of a shared resource. A thread that wishes to use a controlled 
      *  resource must first request sole access with either the wait() 
      *  or wait_timed() methods. The resource must be released by the
      *  thread when it has finished by invoking the allow() method.
      *  \brief Thread resource %gateway
      *  \author John Zweizig
      *  \version 1.0; Last modified May 8, 2008
      */
    class gateway : public abstractsemaphore
    {
    public:
        ///  Size and count data type
        typedef unsigned long counter_type;

    public:
        /**  Construct an idle %gateway.
	  *  \brief Constructor
	  */
        gateway( void );

        /**  Destroy a %gateway.
	  *  \brief Destructor.
	  */
        virtual ~gateway( void );

        /**  Allow at least one thread to stop waiting. No thread ordering 
	  *  enforced.
	  */
        virtual void allow( void );

        /**  Lock the queue information.
	  *  \brief Lock the queue.
	  */
        void lock( void );

        /**  Lock the queue information for read access.
	  *  \brief Lock the queue.
	  */
        void
        readlock( void )
        {
            lock( );
        }

        /**  Set the shut status flag and release all threads blocked waiting 
	  *  for data in this queue. All released threads will exit pop() with 
	  *  a runtime_error exception.
	  *  \brief Open the gate (release all threads).
	  */
        virtual void release( void );

        /**  Test to see whether the mutex is locked.
	  *  \brief Try the lock.
	  *  \param lck Type of lock (read/write) to test.
	  *  \return true if mutex is locked.
	  */
        virtual bool trylock( locktype lck = rdlock );

        /**  Unlock the mutex.
	  *  \brief Unlock the mutex.
	  */
        void unlock( void );

        /**  Wait for this thread to be allowed to access the resource. The
	  *  %gateway must be locked by the current thread when wait is called.
	  *  Although this is not enforced, the results will not be predictable
	  *  if it is unlocked. The %gateway will be locked when control is 
	  *  returned.
	  *  \brief Wait for access.
	  */
        virtual void wait( void );

        /**  Wait for this thread to be allowed to access the resource or 
	  *  until the specified time has elapsed. The %gateway must be locked 
	  *  by the current thread when \c wait_timed is called. The %gateway 
	  *  will be locked when control is returned.
	  *  \brief Wait a maximum time for access.
	  *  \param dt Maximum wait time in seconds.
	  *  \return True if the wait was successful (no timeout).
	  */
        virtual bool wait_timed( double dt );

        /**  Get the number of threads currently waiting for the resource.
	  *  \brief Number of waiting threads.
	  *  \return Number of threads waiting for the resource.
	  */
        counter_type wait_count( void ) const;

        /**  Lock the queue information for write access.
	  *  \brief Lock the queue.
	  */
        void
        writelock( void )
        {
            lock( );
        }

    private:
        pthread_mutex_t _lock;
        pthread_cond_t  _cond;
        counter_type    _wait_count;
    };

    //==================================  Inline methods
    inline gateway::counter_type
    gateway::wait_count( void ) const
    {
        return _wait_count;
    }

} // namespace thread
#endif
