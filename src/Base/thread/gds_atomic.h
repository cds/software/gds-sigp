/* -*- mode: c; c-basic-offset: 4; -*- */
#ifndef GDS_ATOMIC_H
#define GDS_ATOMIC_H

#ifdef __GNUC__
#define GNU_GCC_VERSION ( __GNUC__ * 100 + __GNUC_MINOR__ )
#else
#define GNU_GCC_VERSION 0
#endif

#if ( GNU_GCC_VERSION >= 401 ) &&                                              \
    ( !defined( __i386 ) || defined( __i486 ) || defined( __i686 ) )
#define USE_BUILTIN_ATOMICS 1
#endif
#if ( GNU_GCC_VERSION == 401 ) && defined( __sparc )
#undef USE_BUILTIN_ATOMICS
#endif
#ifdef USE_BUILTIN_ATOMICS

/** Atomic pre-add a to x (\e i.e. x+a)
 */
#define ATOMIC_PREADD( x, a ) __sync_add_and_fetch( &( x ), a )

/** Atomic post-increment x (\e i.e. x++)
 */
#define ATOMIC_POSTINC( x ) __sync_fetch_and_add( &( x ), 1 )

/** Atomic post-decrement x (\e i.e. x--)
 */
#define ATOMIC_POSTDEC( x ) __sync_fetch_and_sub( &( x ), 1 )

/** Atomic pre-increment x (\e i.e. ++x)
 */
#define ATOMIC_PREINC( x ) __sync_add_and_fetch( &( x ), 1 )

/** Atomic pre-decrement x (\e i.e. --x)
 */
#define ATOMIC_PREDEC( x ) __sync_sub_and_fetch( &( x ), 1 )

/** Atomic compare and swap: \verbatim 
    if (x != y) return false;
    x = z; 
    return true; \endverbatim
 */
#define ATOMIC_CMP_SWAP( x, y, z ) __sync_bool_compare_and_swap( &( x ), y, z )
#endif

#endif /* !defined(GDS_ATOMIC_H) */
