/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "gateway.hh"
#include <time.h>
#ifndef HAVE_CLOCK_GETTIME
#include <sys/time.h>
#endif

using namespace thread;

//======================================  Constructor
gateway::gateway( void ) : _wait_count( 0 )
{
    pthread_mutex_init( &_lock, 0 );
    pthread_cond_init( &_cond, 0 );
}

//======================================  Destructor
gateway::~gateway( void )
{
    pthread_mutex_destroy( &_lock );
    pthread_cond_destroy( &_cond );
}

//======================================  Release a waiting thread
void
gateway::allow( void )
{
    pthread_cond_signal( &_cond );
}

//======================================  Lock mutex
void
gateway::lock( void )
{
    pthread_mutex_lock( &_lock );
}

//======================================  Release all waiting thread
void
gateway::release( void )
{
    pthread_cond_broadcast( &_cond );
}

//======================================  Unlock queue data
bool
gateway::trylock( abstractsemaphore::locktype lck )
{
    return ( pthread_mutex_trylock( &_lock ) == 0 );
}

//======================================  Unlock queue data
inline void
gateway::unlock( void )
{
    pthread_mutex_unlock( &_lock );
}

//======================================  Wait for the resource
void
gateway::wait( void )
{
    _wait_count++;
    pthread_cond_wait( &_cond, &_lock );
    _wait_count--;
}

static const long k_1B( 1000000000 );

//======================================  Wait for the resource
bool
gateway::wait_timed( double dt )
{

    //----------------------------------  Get the timeout time
    struct timespec t;
#if !defined( HAVE_CLOCK_GETTIME )
    struct timeval tv;
    gettimeofday( &tv, NULL );
    t.tv_sec = tv.tv_sec;
    t.tv_nsec = tv.tv_usec * 1000;
#else
    clock_gettime( CLOCK_REALTIME, &t );
#endif

    long dt_s = long( dt );
    long dt_n = long( ( dt - dt_s ) * 1e9 + 0.5 );
    t.tv_sec += dt_s;
    t.tv_nsec += dt_n;
    if ( t.tv_nsec >= k_1B )
    {
        t.tv_nsec -= k_1B;
        t.tv_sec++;
    }

    //----------------------------------  Do the tied wait
    _wait_count++;
    bool rc = ( pthread_cond_timedwait( &_cond, &_lock, &t ) == 0 );
    _wait_count--;
    return rc;
}
