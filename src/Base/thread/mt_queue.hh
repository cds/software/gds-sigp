/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef MT_QUEUE_HH
#define MT_QUEUE_HH

#include "gateway.hh"
#include <exception>

namespace thread
{

    /// @ingroup sends_bits

    /**  mt_queue template class implements an MT-safe first-in first-out 
      *  circular buffer (queue) with thread locking. If the used attempts 
      *  to pop data from an empty queue, the callers thread will be blocked
      *  until a data item is available. The number of slots remains constant 
      *  without external action, i.e. calling method \c resize(). The api 
      *  syntax conforms as much as possible to the STL standards.
      *  \brief MT queue template
      *  \author John Zweizig
      *  \version 1.0; Last modified May 7, 2008
      */
    template < class T >
    class mt_queue
    {
    public:
        /** Size and count data type
	  */
        typedef unsigned long size_type;

    public:
        /**  mt_queue constructor. The queue is constructed with nRead empty 
	  *  slots.
	  *  \brief Constructor
	  *  \param nRead Number of slots allocated in queue.
	  */
        mt_queue( int nRead = 0 );

        /**  Destroy an mt_queue.
	  *  \brief Destructor.
	  */
        virtual ~mt_queue( void );

        /**  Get the number of slot in the queue. This number remains constant
	  *  unless the queue is expanded or shrunk by using the \c resize() 
	  *  method.
	  *  \brief Get the number of slots
	  *  \return number of slots in the queue.
	  *  \exception std::runtime_error is thrown if a shutdown is issued.
	  */
        size_type capacity( void ) const;

        /**  Empty the queue.
	  *  \brief Clear the queue.
	  */
        void clear( void );

        /**  Test whether the queue is empty.
	  *  \brief Test for empty.
	  *  \return True if empty.
	  */
        bool empty( void ) const;

        /**  Test whether the queue is full.
	  *  \brief Test for full.
	  *  \return True if full.
	  */
        bool full( void ) const;

        /**  Return the ith entry of the queue. Throw an exception if \a i
	  *  goes past the end of the queue;
	  *  \param i index of entry in queue
	  *  \return ith entry value.
	  */
        T peek( size_type i ) const;

        /**  Remove an entry from the queue if the queue. If no data are 
	  *  available the thread will block until a data item is pushed into 
	  *  the queue or a queue shutown sequence is initiated.
	  *  \brief Pop an element from the queue.
	  *  \param x target location for object removed from the queue.
	  *  \exception \c mt_queue_shutdown is thrown if the thread is 
	  *  waiting when the queue is shut down.
	  */
        void pop( T& x );

        /**  Remove an entry from the queue if the queue is not empty.
	  *  \brief Pop an element from the queue if available.
	  *  \param x target location for object removed from the queue.
	  *  \return True if the data item was removed from the queue.
	  */
        bool pop_if_present( T& x );

        /**  Remove an entry from the queue. If the queue is empty, pop_wait 
	  *  will wait a maximum of \c dt seconds for data. 
	  *  \brief Pop an element from the queue. Wait a defined time if not
	  *         available.
	  *  \param x  Target location for object removed from the queue.
	  *  \param dt maximum wait time in seconds.
	  *  \return True if the data item was removed from the queue.
	  */
        bool pop_wait( T& x, double dt );

        /**  Push specified data into the queue. The method throws an exception
	  *  if there is no free space for the data item.
	  *  \brief Push data onto the tail of the queue.
	  *  \param x Data to be pushed into the queue.
	  *  \exception std::runtime_error thrown if no space available.
	  */
        void push( const T& x );

        /**  Push specified data if the queue is not full. The method returns 
	  *  -1 (true) on success and 0 (false) if the queue is full.
	  *  \brief Push data onto the tail of the queue if a slot is available.
	  *  \param x Data to be pushed into the queue.
	  *  \return True if the data item was sucessfully added to the queue.
	  */
        bool push_if_not_full( const T& x );

        /**  The number of slots in the queue is set to the argument value. The
	  *  data in the queue are copied if necessary.
	  *  \brief Reset the number of queue slots.
	  *  \param n New number of slots.
	  */
        void resize( size_type n );

        /**  Set the shut status flag and release all threads blocked waiting 
	  *  for data in this queue. All released threads will exit pop() with 
	  *  a runtime_error exception.
	  */
        void shut( void );

        /**  Get the number of full slots in the queue.
	  *  \brief Get number of data items.
	  *  \return Number of slots with data.
	  */
        size_type size( void ) const;

        /**  Get the number of threads waiting for pop.
	  *  \brief Get number of waiting threads.
	  *  \return Number of threads waiting for data.
	  */
        size_type wait_count( void ) const;

    private:
        /**  Internal push method. The queue data must be locked by the calling
	  *  thread before invoking push_internal.
	  *  \brief Internal push method.
	  *  \param  Data item to push into queue.
	  *  \return True if push successful, otherwise false.
	  */
        bool push_internal( const T& x );

        /**  Internal pop function. The queue data must be locked by the calling
	  *  thread before invoking pop_internal.
	  *  \brief Internal pop method.
	  *  \param x Receives data popped off of the stack.
	  *  \return True if pop successful, otherwise false.
	  */
        bool pop_internal( T& x );

    private:
        gateway   _gate;
        T*        _data;
        size_type _allocation;
        size_type _capacity;
        size_type _read;
        size_type _write;
        bool      _shut;
    };

    /**  mt_queue_shutdown defines an exception thrown by the mt_queue 
      *  for all waiting threads when the queue is shut down.
      *  \brief Shudown exception class.
      *  \author John Zweizig (john.zweizig@ligo.org)
      *  \version 1.0; Last modified May 14, 2010
      */
    class mt_queue_shutdown : public std::exception
    {
    public:
        /**  Default constructor 
	 */
        mt_queue_shutdown( void )
        {
        }

        /**  Get the exception decription text.
	  *  \brief Exception description.
	  *  \return Constant character strign pointer to decscrition text.
	  */
        const char*
        what( void ) const throw( )
        {
            return "mt_queue shut down";
        }
    };

} // namespace thread

#include "mt_queue.icc"

#endif
