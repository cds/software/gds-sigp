/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef SENDS_THREAD_BASE
#define SENDS_THREAD_BASE
#include <pthread.h>

namespace thread
{

    /**  Thread_base is a base class for threads. There is a single
      *  entry point for the thread.
      *  \brief Threaded asynchronous execution base class
      *  \ingroup sends_bits
      *  \author John Zweizig (john.zweizig@ligo.org)
      *  \version 1.1; Last modified May 16, 2010
      */
    class thread_base
    {
    public:
        /**  Construct the thread base class.
	  *  \brief Thread base constructor.
	  */
        thread_base( void );

        /**  Destroy the thread_base class.
	  *  \brief thread_bas destructor
	  */
        virtual ~thread_base( void );

        /**  Test whether the thread is currently executing.
	  *  \brief Test busy
	  *  \return True if the asynchronous thread is running.
	  */
        bool busy( void ) const;

        /**  Wait for the thread to complete and store the return value
	  *  pointer in the specified location.
	  *  \brief Join the current thread.
	  *  \param retcd Pointer to a location to receive the exit value.
	  *  \return Zero if the join completed.
	  */
        int join( void** retcd ) const;

        /**  Send a signal to this thread.
	  *  \brief signal a thread
	  *  \param sig Signal number.
	  *  \return Kill() return code.
	  */
        virtual int kill_thread( int sig );

        /**  Test the thread run-enable flag.
	  *  \brief Test run enable.
	  *  \return True if thread execution is enabled.
	  */
        bool run( void ) const;

        /**  Set the detached flag. This causes the detached flag in the
	  *  thread attributes structure to be set. The next time a thread
	  *  is spawned it will be run as a detached thread.
	  *  \brief Enable detached exeution
	  *  \param d If tru, enable detached running, else disable.
	  */
        void set_detached( bool d );

        /**  Create a thread and execute thread_entry(). The run flag and 
	  *  busy flag are set if the thread is successfully started.
	  *  \brief Start a thread.
	  *  \return return code from pthread_create.
	  */
        virtual int start_thread( void );

        /**  Clear the run-enable flag. The thread will stop only if it 
	  *  checks the run-enable flag (see run()).
	  *  \brief Diable thread.
	  */
        virtual int stop_thread( void );

        /**  Method to be executed when the thread is started.
	  *  \brief thread entry method.
	  *  \return User defined return value.
	  */
        virtual void* thread_entry( void ) = 0;

    private:
        /**  thread_base stub method to set-up for thread execution. The
	 *  thread_stub sets the run and busy flags and then calls the 
	 *  thread_entry method. Exceptions in thread_entry are caught
	 *  and the busy flag is clear after thread entry is returned.
	 *  \brief Set up thread environment.
	 *  \return User defined return value from thread_entry.
	 */
        void* thread_stub( void );

        /**  Private thread launch function used by start_thread() to cast the
	 *  argument to the thread_base class and invoke the thread_stub method.
	 *  \brief launch a thread
	 *  \param p thread_base pointer to the class object.
	 *  \return User defined return code from thread_stub/thread_entry.
	 */
        static void* launch( void* p );

    private:
        volatile bool  mRun;
        volatile bool  mBusy;
        pthread_t      mThreadID;
        pthread_attr_t mThreadAttr;
    };

    //==================================  Inline methods
    inline bool
    thread_base::busy( void ) const
    {
        return mBusy;
    }

    inline bool
    thread_base::run( void ) const
    {
        return mRun;
    }
} // namespace thread

#endif // !defined(SENDS_THREAD_BASE)
