/* -*- mode: c++; c-basic-offset: 3; -*- */
#include <time.h>
#include <cstdlib>
#include <unistd.h>
#include <iostream>
#include <cstring>
#include "goption.hh"
#include "gmutex.hh"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// externals                                                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
extern "C" char* optarg;
extern "C" int   optind;

//-----------------------------------------------------------------------------
// getopt changed its tune. It used to define EOF for the end-of-option-list
// value. Posix says this value must be -1. For the time being I'll make a
// local definition of EOF = -1, to maintain backward compatibility, but this
// could cause problems in the future.
#ifndef EOF
#define EOF int( -1 )
#endif
//-----------------------------------------------------------------------------

namespace gdsbase
{

    using namespace std;
    using namespace thread;

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // Global mutex to make parser MT safe                                  //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    ::thread::mutex fMux;

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // option_string                                                        //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    option_string::option_string( const char* id,
                                  const char* arg,
                                  const char* optstr )
        : fargc( 0 ), fError( false ), fHelp( false )
    {
        memset( fargv, 0, sizeof( fargv ) );
        for ( int i = 0; i < maxarg; i++ )
        {
            fargv_ptr[ i ] = fargv[ i ];
        }
        if ( id )
        {
            strncpy( fargv[ 0 ], id, maxarglen - 1 );
            fargc = 1;
        }
        else
        {
            strcpy( fargv[ 0 ], "" );
            fargc = 1;
        }
        char* opt = arg ? new ( nothrow ) char[ strlen( arg ) + 10 ] : 0;
        if ( opt )
        {
            strcpy( opt, arg );
            char* last;
            char* p = strtok_r( opt, " \f\n\r\t\v", &last );
            while ( p && ( fargc < maxarg ) )
            {
                strncpy( fargv[ fargc ], p, maxarglen - 1 );
                fargc++;
                p = strtok_r( 0, " \f\n\r\t\v", &last );
            }
            delete[] opt;
        }
        if ( optstr )
            parse( optstr );
    }

    //______________________________________________________________________________
    option_string::option_string( int _argc, char** _argv, const char* optstr )
        : fargc( 0 ), fError( false ), fHelp( false )
    {
        memset( fargv, 0, sizeof( fargv ) );
        for ( int i = 0; i < maxarg; i++ )
        {
            fargv_ptr[ i ] = fargv[ i ];
        }
        for ( int i = 0; ( i < _argc ) && ( i < maxarg ); i++ )
        {
            strncpy( fargv[ fargc ], _argv[ i ], maxarglen - 1 );
            fargc++;
        }
        if ( optstr )
            parse( optstr );
    }

    //______________________________________________________________________________
    option_string::~option_string( )
    {
    }

    //______________________________________________________________________________
    void
    option_string::parse( const char* optstr )
    {
        int c;
        fError = false;
        fHelp = false;
        fArgs.clear( );
        fOpts.clear( );
        // optional arguments
        semlock lockit( fMux );
        optind = 1;
        while ( ( c = getopt( argc( ), argv( ), optstr ) ) != EOF )
        {
            switch ( c )
            {
            case 'h': {
                fHelp = true;
                break;
            }
            case '?': {
                fError = true;
                break;
            }
            default: {
                // callback
                addopt( c, optarg );
                break;
            }
            }
        }
        // remaining arguments
        for ( int i = optind; i < fargc; i++ )
        {
            if ( fargv[ i ] && *fargv[ i ] )
                addarg( fargv[ i ] );
        }
    }

    //______________________________________________________________________________
    std::string
    option_string::str( ) const
    {
        string s;
        for ( opt_list::const_iterator i = fOpts.begin( ); i != fOpts.end( );
              ++i )
        {
            s += " -";
            s += i->first;
            if ( !i->second.empty( ) )
            {
                s += " ";
                s += i->second;
            }
        }
        for ( arg_list::const_iterator i = fArgs.begin( ); i != fArgs.end( );
              ++i )
        {
            s += " ";
            s += *i;
        }
        if ( !s.empty( ) )
        {
            s.erase( 0, 1 );
        }
        return s;
    }

    //______________________________________________________________________________
    int
    option_string::argnum( ) const
    {
        int i = 0;
        for ( arg_list::const_iterator iter = argbegin( ); iter != argend( );
              ++iter, ++i )
            ;
        return i;
    }

    //______________________________________________________________________________
    bool
    option_string::opt( char c ) const
    {
        opt_list::const_iterator i = fOpts.find( c );
        return ( i != fOpts.end( ) );
    }

    //______________________________________________________________________________
    bool
    option_string::getOpt( char c, string& value ) const
    {
        opt_list::const_iterator i = fOpts.find( c );
        if ( i != fOpts.end( ) )
        {
            value = i->second;
            return true;
        }
        else
        {
            return false;
        }
    }

    //______________________________________________________________________________
    bool
    option_string::getOpt( char c, int& value ) const
    {
        string s;
        if ( !getOpt( c, s ) )
        {
            return false;
        }
        value = atoi( s.c_str( ) );
        return true;
    }

    //______________________________________________________________________________
    bool
    option_string::getOpt( char c, double& value ) const
    {
        string s;
        if ( !getOpt( c, s ) )
        {
            return false;
        }
        value = atof( s.c_str( ) );
        return true;
    }

    //______________________________________________________________________________
    void
    option_string::addarg( const char* arg )
    {
        if ( arg )
            fArgs.push_back( arg );
    }

    //______________________________________________________________________________
    void
    option_string::addopt( char c, const char* opt )
    {
        fOpts.insert( opt_list::value_type( c, string( opt ? opt : "" ) ) );
    }

} // namespace gdsbase
