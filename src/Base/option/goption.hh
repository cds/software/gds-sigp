/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: foption							*/
/*                                                         		*/
/* Module Description: option processing				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 26Nov00  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: foption.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_GOPTION_H
#define _LIGO_GOPTION_H

#include <string>
#include <vector>
#include <map>

namespace gdsbase
{

    /** @name Option processing
    This header defines a support class for option processing.
   
    @memo Option processing
    @author Written November 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

    //@{

    /// Maximum number of arguments
    const int maxarg = 256;
    /// Maximum lengthof arguments
    const int maxarglen = 256;

    /** Option processing class. This class supports argument processing in 
    form of list and string. Arguments are parsed and stored in a map
    of pair<char,string> (optional arguments) and a list of strings 
    (additional arguments). This class is MT safe as long as the getopt
    function is not used by any other thread directly.
    
    @memo Option processing class.
 ************************************************************************/
    class option_string
    {
    public:
        /// List of remainig arguments
        typedef std::vector< std::string > arg_list;
        /// List of optional arguments
        typedef std::map< char, std::string > opt_list;

        /** Constructor. If optstr is non zero, the options are
          parsed.
          @param id Program name -> argv[0]
          @param arg Option string (as on command line)
          @param optstr Option list (as for getopt)
         */
        option_string( const char* id,
                       const char* arg,
                       const char* optstr = 0 );
        /** Constructor. If optstr is non zero, the options are
          parsed.
          @param argc Number of arguments
          @param argv Option list
          @param optstr Option list (as for getopt)
         */
        option_string( int argc, char** argv, const char* optstr = 0 );
        /// Destructor
        ~option_string( );

        /** Parse.
          @param optstr Option list (as for getopt)
         */
        void parse( const char* optstr );

        /// Number of arguments
        int
        argc( ) const
        {
            return fargc;
        }
        /// List of arguments
        char* const*
        argv( ) const
        {
            return fargv_ptr;
        }
        /// return argument string
        std::string str( ) const;

        /// begin of remaining arguments
        arg_list::const_iterator
        argbegin( ) const
        {
            return fArgs.begin( );
        }
        /// end of remaining arguments
        arg_list::const_iterator
        argend( ) const
        {
            return fArgs.end( );
        }
        /// Number of remaining arguments
        int argnum( ) const;

        /// Returns true if option was defined
        bool opt( char c ) const;
        /** get option
          @param c option letter
          @param value option value (return): string, int or double
          @return true if option exists
        */
        bool getOpt( char c, std::string& value ) const;
        bool getOpt( char c, int& value ) const;
        bool getOpt( char c, double& value ) const;

        /// help needed?
        bool
        help( ) const
        {
            return fHelp | fError;
        }

        bool
        error( ) const
        {
            return fError;
        }

    protected:
        /// option length
        int fargc;
        /// option array
        char fargv[ maxarg ][ maxarglen ];
        /// optio arra pointers
        char* fargv_ptr[ maxarg ];
        /// Error flag
        bool fError;
        /// Help flag
        bool fHelp;
        /// Remaining arguments
        arg_list fArgs;
        /// Optional arguments
        opt_list fOpts;

        /// Add a remaining argument
        void addarg( const char* arg );
        /// Add a optional argument
        void addopt( char c, const char* opt );
    };

    //@}
} // namespace gdsbase
#endif // _LIGO_GOPTION_H
