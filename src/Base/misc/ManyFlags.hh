/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef MANYFLAGS_HH
#define MANYFLAGS_HH

#include "gds_atomic.h"

/**  ManyFlags is a template for a generalized flag-word class. The flags
  *  are organized internally as NW words of NB bits, but are accessed by a
  *  single bit index. Bits may be set or cleared individually, or treated
  *  as constituents of a single word. Some operations (tset, tclr) are 
  *  performed atomically if the appropriate builtin functions are available.
  *  
  *  @memo Generalized flag word class.
  *  @author John Zweizig
  *  @version 1.2; Last modified May 22, 2009
  */
template < int NW, int NB >
class ManyFlags
{
public:
    /**  Data type used for bit masks.
     */
    typedef unsigned int flag_t;

    /**  Data type used for counters.
     */
    typedef unsigned int count_t;

private:
    flag_t mFlag[ NW ];

    count_t
    word( count_t ib ) const
    {
        return ib / NB;
    }

    flag_t
    mask( count_t ib ) const
    {
        return 1 << ( ib % NB );
    }

    flag_t
    range( ) const
    {
        return ( 1 << NB ) - 1;
    }

public:
    /**  Construct a flag object and zero all flags.
      *  @memo Default constructor
      */
    ManyFlags( )
    {
        zero( );
    }

    /**  Construct a flag object and initalize it to the specified 
      *  existing flag value.
      *  @memo Copy constructor
      *  @param x Manyflags object to copy.
      */
    ManyFlags( const ManyFlags& x )
    {
        for ( count_t i = 0; i < NW; i++ )
            mFlag[ i ] = x.mFlag[ i ];
    }

    /**  Returns true if any flag is set in any word.
      *  @memo Test for any flag set.
      *  @return true if one or more flags are set.
      */
    bool
    any( void ) const
    {
        for ( count_t i = 0; i < NW; i++ )
            if ( mFlag[ i ] != 0 )
                return true;
        return false;
    }

    /**  The specified bit is set to zero (false). No action is taken when 
      *  an invalid bit is specified.
      *  @memo Clear a bit.
      *  @param ib umber of bit to be cleared.
      */
    void
    clear( count_t ib )
    {
        if ( ib < NB * NW )
            mFlag[ word( ib ) ] &= ~mask( ib );
    }

    /**  The first nBits, where nBits is the precision of a unsigned long, 
      *  bits in a ManyBits object are concatinated together and returned.
      *  @memo Collect bits into a single word.
      *  @return Unsigned long word with the highest order bits.
      */
    unsigned long collect( void ) const;

    /**  Count the number of set bits in the object.
      *  @memo count set bits.
      *  @return Number of set bits in flag set.
      */
    count_t count( void ) const;

    /**  Test whether all bits are zero.
      *  @memo Test for zero.
      *  @return true if all bits are zero (false).
      */
    bool
    operator!( void ) const
    {
        return !any( );
    }

    /**  Returns the present value of the specified bit.
      *  @memo Return bit value.
      *  @return true if specified bit is non-zero.
      *  @param ib Number of bit to be tested.
      */
    bool
    operator[]( count_t ib ) const
    {
        return test( ib );
    }

    /**  Returns the complement of the specified bit.
      *  @memo Return bit complement.
      *  @return true if specified bit is zero.
      *  @param ib Number of bit to be complemented.
      */
    bool
    Not( count_t ib ) const
    {
        return !test( ib );
    }

    /**  Calculate the bit-wise logical or of two flag objects.
      *  @memo Logical or.
      *  @param rhs Flags to be ORed into this flag mask.
      *  @return Reference to this flag mask.
      */
    ManyFlags&
    operator|=( const ManyFlags& rhs )
    {
        for ( count_t i = 0; i < NW; i++ )
            mFlag[ i ] |= rhs.mFlag[ i ];
        return *this;
    }

    /**  Calculate the bit-wise logical and of two flag objects.
      *  @memo Logical and.
      *  @param rhs Flags to be ANDed into this flag mask.
      *  @return Reference to this flag mask.
      */
    ManyFlags&
    operator&=( const ManyFlags& rhs )
    {
        for ( count_t i = 0; i < NW; i++ )
            mFlag[ i ] &= rhs.mFlag[ i ];
        return *this;
    }

    /**  A ManyFlags object is returned with all 1 bits are set to 0 and 
      *  visa-versa.
      *  @memo Toggle all bits.
      *  @return Manyflags object with complemented bits.
      */
    ManyFlags operator~( void ) const;

    /**  The specified bit is set to one (true). No action is taken when
      *  an invalid bit is specified.
      *  @memo Set a bit.
      *  @param ib Number of bit to be set.
      */
    void
    set( count_t ib )
    {
        if ( ib < NB * NW )
            mFlag[ word( ib ) ] |= mask( ib );
    }

    /**  The bit specified by ib is tested and cleared. The original value
      *  of bit ib is returned. This operation is performed atomically 
      *  only if the builtin atomic operators are available (testable with 
      *  USE_BUILTIN_ATOMICS).
      *  @memo Test a specified bit and clear it.
      *  @return Original value of specified bit.
      *  @param  ib bit number to be tested/cleared.
      */
    bool tclr( count_t ib );

    /**  The value of bit ib is returned.
      *  @memo Test a bit.
      *  @param  ib bit number to be tested.
      *  @return Value of specified bit.
      */
    bool test( count_t ib ) const;

    /**  The bit specified by ib is tested and set to true. The original 
      *  value of bit ib is returned. This operation is performed atomically 
      *  only if the builtin atomic operators are available (testable with 
      *  USE_BUILTIN_ATOMICS).
      *  @memo Test a specified bit and set it.
      *  @return original value of the specified bit.
      */
    bool tset( count_t ib );

    /**  All the bits in an object are set to zero (false).
      *  @memo Clear all bits.
      */
    void zero( void );
};

//======================================  Non Member operators
template < int NW, int NB >
inline ManyFlags< NW, NB >
operator&( const ManyFlags< NW, NB >& lhs, const ManyFlags< NW, NB >& rhs )
{
    ManyFlags< NW, NB > r( lhs );
    r &= rhs;
    return r;
}

template < int NW, int NB >
inline ManyFlags< NW, NB >
operator|( const ManyFlags< NW, NB >& lhs, const ManyFlags< NW, NB >& rhs )
{
    ManyFlags< NW, NB > r( lhs );
    r |= rhs;
    return r;
}

//======================================  Collect
template < int NW, int NB >
inline unsigned long
ManyFlags< NW, NB >::collect( ) const
{
    unsigned long r( 0 );
    flag_t        m = range( );
    for ( count_t i = NW; i-- != 0; )
    {
        r <<= NB;
        r |= ( mFlag[ i ] & m );
    }
    return r;
}

//======================================  Count set bits
template < int NW, int NB >
inline typename ManyFlags< NW, NB >::count_t
ManyFlags< NW, NB >::count( void ) const
{
    count_t r( 0 );
    flag_t  m = 1;
    for ( count_t i = 0; i < NB; i++ )
    {
        for ( count_t j = 0; j < NW; j++ )
            if ( ( mFlag[ j ] & m ) != 0 )
                r++;
        m <<= 1;
    }
    return r;
}

//======================================  return complement
template < int NW, int NB >
inline ManyFlags< NW, NB >
ManyFlags< NW, NB >::operator~( void ) const
{
    ManyFlags r;
    flag_t    m = range( );
    for ( count_t i = 0; i < NW; i++ )
        r.mFlag[ i ] = ~mFlag[ i ] & m;
    return r;
}

//======================================  Test and clear
template < int NW, int NB >
inline bool
ManyFlags< NW, NB >::tclr( count_t ib )
{
    if ( ib >= NB * NW )
        return false;
    count_t wd = word( ib );
    flag_t  m = mask( ib );
#ifdef USE_BUILTIN_ATOMICS
    return ( __sync_fetch_and_and( mFlag + wd, ~m ) & m ) != 0;
#else
    if ( ( mFlag[ wd ] & m ) == 0 )
        return false;
    mFlag[ wd ] &= ~m;
    return true;
#endif
}

//======================================  Test a bit
template < int NW, int NB >
inline bool
ManyFlags< NW, NB >::test( count_t ib ) const
{
    if ( ib >= NB * NW )
        return false;
    if ( ( mFlag[ word( ib ) ] & mask( ib ) ) != 0 )
        return true;
    return false;
}

//======================================  Test and set
template < int NW, int NB >
inline bool
ManyFlags< NW, NB >::tset( count_t ib )
{
    if ( ib >= NB * NW )
        return false;
    count_t wd = word( ib );
    flag_t  m = mask( ib );
#ifdef USE_BUILTIN_ATOMICS
    return ( __sync_fetch_and_or( mFlag + wd, m ) & m ) != 0;
#else
    if ( ( mFlag[ wd ] & m ) != 0 )
        return true;
    mFlag[ wd ] |= m;
    return false;
#endif
}

//======================================  Test and set
template < int NW, int NB >
inline void
ManyFlags< NW, NB >::zero( void )
{
    for ( count_t i = 0; i < NW; i++ )
        mFlag[ i ] = 0;
}

#endif // MANYFLAGS_HH
