/* -*- mode: c++; c-basic-offset: 4; -*- */
//
//    System error class
//
#include <stdexcept>
#include <string>
#include <errno.h>
#include <string.h>

/**  The SysError class is used for exceptions that are caused by a system 
  *  error. The description string contains an optional location string 
  *  followed by the system error descrptions (strerror) test.
  *  @memo System error exception class.
  *  @author John Zweizig (john.zweizig@ligo.org)
  *  @version 1.1; Last modified May 14, 2010
  */
class SysError : public std::runtime_error
{
public:
    /**  Constructor with system error text string only
      *  @memo System error constructor
      */
    SysError( void ) : std::runtime_error( strerror( errno ) )
    {
    }

    /**  Constructor with location string and system error message.
      *  @memo Constructor with location.
      *  @param msg Location text string.
      */
    SysError( const std::string& msg )
        : std::runtime_error( msg + ": " + strerror( errno ) )
    {
    }
};
