/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef _LIGO_CCSTREAMBUF_H
#define _LIGO_CCSTREAMBUF_H

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: ccstreambuf						*/
/*                                                         		*/
/* Module Description: Stream buffer to calculate checksum	        */
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 17Sep13  J. Zweizig   	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: pipe_exec.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* John Zweizig  (626) 395-2485  ---         john.zweizig@ligo.org	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 2013.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include <iosfwd>
#include <ios>
#include <streambuf>
#include "checksum_crc32.hh"

#define CCBUF_TEMP_SIZE 1024

namespace std
{

    /** @name Null output stream buffer that calculates crc32
   
       \brief CRC pseudo-stream buffer
       @author John Zweizig (john.zweizig@ligo.org)
       @version 1.0
   ************************************************************************/

    //@{

    /** Basic stream buffer for memory mapped files.
    
       @memo Basic checksum calculation stream buffer
   ************************************************************************/
    template < class charT, class traits = char_traits< charT > >
    class basic_ccbuf : public basic_streambuf< charT, traits >
    {
    public:
        typedef typename basic_streambuf< charT, traits >::int_type  int_type;
        typedef typename basic_streambuf< charT, traits >::off_type  off_type;
        typedef typename basic_streambuf< charT, traits >::pos_type  pos_type;
        typedef typename basic_streambuf< charT, traits >::char_type char_type;
        typedef traits   traits_type;
        typedef uint32_t crc32_type;

        /**  Create a default streambuf.
        *  \brief Default constructor
      */
        basic_ccbuf( ios_base::openmode which = ios_base::in | ios_base::out );

        /**  Destroy a streambuf and release temporary storage.
        *  \brief Destructor.
	*/
        virtual ~basic_ccbuf( );

        /// Return the data length
        streamsize
        get_length( ) const
        {
            return _temp_offset + this->pptr( ) - this->pbase( );
        }

        /// Return the calculated CRC32 checksum.
        crc32_type
        get_crc32( ) const
        {
            return _checker.result( );
        }

        /// Sync the temporary buffer
        int_type sync( void );

    private:
        char_type*     _temp_buf;
        pos_type       _temp_offset;
        checksum_crc32 _checker;

    protected:
        /// Read one character
        virtual int_type underflow( );
        /// Write one character
        virtual int_type overflow( int_type c = traits::eof( ) );
        /// Read multiple characters
        virtual streamsize xsgetn( char_type* s, streamsize n );
        /// Write multiple characters
        virtual streamsize xsputn( const char_type* s, streamsize num );
        /// Seek with offset
        virtual pos_type seekoff( off_type           off,
                                  ios_base::seekdir  way,
                                  ios_base::openmode which = ios_base::in |
                                      ios_base::out );
        /// Seek with position
        virtual pos_type seekpos( pos_type           sp,
                                  ios_base::openmode which = ios_base::in |
                                      ios_base::out );
    };

    /** Stream buffer with memory mapped file (char).
    
       @memo Stream buffer with memory mapped file (char)
   ************************************************************************/
    typedef basic_ccbuf< char > cc_streambuf;

    /** Stream buffer with memory mapped file (wchar_t).
    
       @memo Stream buffer with memory mapped file (wchar_t)
   ************************************************************************/
    typedef basic_ccbuf< wchar_t > cc_wstreambuf;

    /** Basic checksum input stream (NULL).
    
       @memo Basic input stream buffer for checksum
   ************************************************************************/
    template < class charT, class traits = char_traits< charT > >
    class basic_ccistream : public basic_istream< charT, traits >
    {
    public:
        typedef charT                            char_type;
        typedef basic_istream< charT, traits >   istream_type;
        typedef basic_ccbuf< charT, traits >     buffer_type;
        typedef basic_ccistream< charT, traits > ccstream_type;

        /** Default constructor.
       */
        basic_ccistream( ) : istream_type( &fBuf ), fBuf( ios_base::in )
        {
            this->init( &fBuf );
        }

        /// Buffer
        buffer_type*
        rdbuf( )
        {
            return &fBuf;
        }
        /// Buffer
        const buffer_type*
        rdbuf( ) const
        {
            return &fBuf;
        }
        /// Is open?

    private:
        /// Stream buffer
        buffer_type fBuf;
    };

    /** Basic output stream for memory mapped files.
    
       @memo Basic output stream buffer for memory mapped files
   ************************************************************************/
    template < class charT, class traits = char_traits< charT > >
    class basic_ccostream : public basic_ostream< charT, traits >
    {
    public:
        typedef charT                            char_type;
        typedef basic_ostream< charT, traits >   ostream_type;
        typedef basic_ccbuf< charT, traits >     buffer_type;
        typedef basic_ccostream< charT, traits > ccstream_type;
        typedef typename buffer_type::crc32_type crc32_type;

        /** Default constructor.
       */
        basic_ccostream( ) : ostream_type( &fBuf ), fBuf( ios_base::out )
        {
        }

        /// Buffer
        buffer_type*
        rdbuf( )
        {
            return &fBuf;
        }
        /// Buffer
        const buffer_type*
        rdbuf( ) const
        {
            return &fBuf;
        }

        crc32_type
        get_crc32( ) const
        {
            return fBuf.get_crc32( );
        }

    private:
        /// Stream buffer
        buffer_type fBuf;
    };

    /** Basic IO stream for memory mapped files.
    
       @memo Basic IO stream buffer for memory mapped files
   ************************************************************************/
    template < class charT, class traits = char_traits< charT > >
    class basic_ccstream : public basic_iostream< charT, traits >
    {
    public:
        typedef charT                           char_type;
        typedef basic_iostream< charT, traits > stream_type;
        typedef basic_ccbuf< charT, traits >    buffer_type;
        typedef basic_ccstream< charT, traits > ccstream_type;

        /** Default constructor.
       */
        basic_ccstream( )
            : stream_type( &fBuf ), fBuf( ios_base::in | ios_base::out )
        {
            this->init( &fBuf );
        }

        /// Buffer
        buffer_type*
        rdbuf( )
        {
            return &fBuf;
        }
        /// Buffer
        const buffer_type*
        rdbuf( ) const
        {
            return &fBuf;
        }

    private:
        /// Stream buffer
        buffer_type fBuf;
    };

    /** Input stream with file descriptors (char).
    
       @memo Input stream buffer with file descriptors (char)
   ************************************************************************/
    typedef basic_ccistream< char > icrcstream;

    /** Input stream with file descriptors (wchar_t).
    
       @memo Input stream buffer with file descriptors (wchar_t)
   ************************************************************************/
    typedef basic_ccistream< wchar_t > wicrcstream;

    /** Output stream with file descriptors (char).
    
       @memo Output stream buffer with file descriptors (char)
   ************************************************************************/
    typedef basic_ccostream< char > ocrcstream;

    /** Output stream with file descriptors (wchar_t).
    
       @memo Output stream buffer with file descriptors (wchar_t)
   ************************************************************************/
    typedef basic_ccostream< wchar_t > wocrcstream;

    /** IO stream with file descriptors (char).
    
       @memo IO stream buffer with file descriptors (char)
   ************************************************************************/
    typedef basic_ccstream< char > crcstream;

    /** IO stream with file descriptors (wchar_t).
    
       @memo IO stream buffer with file descriptors (wchar_t)
   ************************************************************************/
    typedef basic_ccstream< wchar_t > wcrcstream;
    //@}

    //___________________________________________________________________________
    template < class charT, class traits >
    basic_ccbuf< charT, traits >::basic_ccbuf( ios_base::openmode which )
        : _temp_buf( 0 ), _temp_offset( 0 )
    {
        this->setp( 0, 0 );
        this->setg( 0, 0, 0 );
    }

    //___________________________________________________________________________
    template < class charT, class traits >
    basic_ccbuf< charT, traits >::~basic_ccbuf( )
    {
        if ( _temp_buf )
            delete[] _temp_buf;
        _temp_buf = 0;
    }

    //___________________________________________________________________________
    template < class charT, class traits >
    typename basic_ccbuf< charT, traits >::int_type
    basic_ccbuf< charT, traits >::overflow( int_type c )
    {
        if ( !_temp_buf )
        {
            _temp_buf = new char_type[ CCBUF_TEMP_SIZE ];
            this->setp( _temp_buf, _temp_buf + CCBUF_TEMP_SIZE );
        }
        size_t len = this->pptr( ) - this->pbase( );
        if ( len )
        {
            _checker.add( this->pbase( ), len );
            _temp_offset += len;
            this->setp( _temp_buf, _temp_buf + CCBUF_TEMP_SIZE );
        }
        _temp_buf[ 0 ] = c;
        return traits_type::to_int_type( c );
    }

    //___________________________________________________________________________
    template < class charT, class traits >
    typename basic_ccbuf< charT, traits >::int_type
    basic_ccbuf< charT, traits >::sync( void )
    {
        return overflow( 0 ); // flush the buffer
    }

    //___________________________________________________________________________
    template < class charT, class traits >
    typename basic_ccbuf< charT, traits >::int_type
    basic_ccbuf< charT, traits >::underflow( )
    {
        return traits::eof( ); // not a readable iostream
    }

    //___________________________________________________________________________
    template < class charT, class traits >
    streamsize
    basic_ccbuf< charT, traits >::xsgetn( char_type* s, streamsize num )
    {
        return 0;
    }

    //___________________________________________________________________________
    template < class charT, class traits >
    streamsize
    basic_ccbuf< charT, traits >::xsputn( const char_type* s, streamsize num )
    {
        streamsize N = 0;
        while ( N < num )
        {
            streamsize left = this->epptr( ) - this->pptr( );
            if ( !left )
            {
                int_type c = traits_type::to_int_type( s[ N ] );
                if ( overflow( c ) != c )
                    return traits::eof( );
                left = this->epptr( ) - this->pptr( );
                if ( !left )
                    return traits::eof( );
            }
            streamsize tbd = num - N;
            if ( left < tbd )
                tbd = left;
            traits::copy( this->pptr( ), s + N, tbd );
            this->pbump( tbd );
            N += tbd;
        }
        return N;
    }

    //___________________________________________________________________________
    template < class charT, class traits >
    typename basic_ccbuf< charT, traits >::pos_type
    basic_ccbuf< charT, traits >::seekoff( off_type           off,
                                           ios_base::seekdir  way,
                                           ios_base::openmode which )
    {
        streamsize newoff = 0;
        if ( which & ios_base::out )
        {
            if ( way == ios_base::beg )
                newoff = 0;
            else
                newoff = _temp_offset + ( this->pptr( ) - this->pbase( ) );
        }
        else
        {
            return pos_type( off_type( -1 ) );
        }
        return seekpos( newoff, which );
    }

    //___________________________________________________________________________
    template < class charT, class traits >
    typename basic_ccbuf< charT, traits >::pos_type
    basic_ccbuf< charT, traits >::seekpos( pos_type           sp,
                                           ios_base::openmode which )
    {
        if ( ( sp < _temp_offset ) )
        {
            return pos_type( off_type( -1 ) );
        }

        if ( which & ios_base::out )
        {
            if ( sp > ( this->epptr( ) - this->pbase( ) ) )
            {
                return pos_type( off_type( -1 ) );
            }
            this->pbump( (int)sp - int( this->pptr( ) - this->pbase( ) ) );
        }
        return sp;
    }
} // namespace std

#endif // _LIGO_CCSTREAMBUF_H
