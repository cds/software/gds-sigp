/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef REDIRECT_STD_HH
#define REDIRECT_STD_HH

#include <cstdio>
#include <unistd.h>
#include <fcntl.h>
#include <string>

/**  Redirect the standard output streams to the specified file.
  *  Either stdout or stderr (or both) may be redirected to the file.  
  *  \brief Redirect std output stream(s)
  *  \param file  File name string
  *  \param flags Select stream: 1=stdout, 2=stderr, 3=both
  *  \return 0 on success, -1 on failure.
  */
inline int
redirect_std( const std::string& file, int flags = 3 )
{
    if ( !( flags & 3 ) )
        return 0;
    int fd = open( file.c_str( ), O_WRONLY | O_CREAT, 0644 );
    if ( fd < 0 )
        return -1;
    if ( ( flags & 1 ) != 0 && dup2( fd, fileno( stdout ) ) < 0 )
        return -1;
    if ( ( flags & 2 ) != 0 && dup2( fd, fileno( stderr ) ) < 0 )
        return -1;
    return 0;
}

#endif // !defined(REDIRECT_STD_HH)
