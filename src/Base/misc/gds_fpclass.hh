/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef GDS_FPCLASS_HH
#define GDS_FPCLASS_HH
#include <inttypes.h>

//========================================= floating point type templates
#ifdef __sun
#include <ieeefp.h>
template < typename T >
inline int
is_finite( T x )
{
    switch ( fpclass( x ) )
    {
    case FP_SNAN:
    case FP_QNAN:
    case FP_PINF:
    case FP_NINF:
        return 0;
    default:
        return 1;
    }
    return 1;
}

template < typename T >
inline int
is_normal( T x )
{
    switch ( fpclass( x ) )
    {
    case FP_PZERO:
    case FP_NZERO:
    case FP_PNORM:
    case FP_NNORM:
        return 1;
    default:
        return 0;
    }
    return 0;
}
#else
#include <cmath>

/**  Template finite test function for float or double. A value is considered
  *  finite if it is not inf, -inf  or NaN.
  *  \brief Test finite
  *  \param x Float or double to test. 
  *  \returns True if the argument is finite.
  */
template < typename T >
inline int
is_finite( T x )
{
    switch ( std::fpclassify( x ) )
    {
    case FP_NAN:
    case FP_INFINITE:
        return 0;
    default:
        return 1;
    }
    return 0;
}

/**  Template normalized test function for float or double. Unnormalized
  *  values occur if the result of an arithemetic operation is less than
  *  The lowest normalizable value, \e i.e. 
  *  \f$2^{-128} ~ 3 \times 10^{-37}\f$ for floats and 
  *  \f$2^{-1024} ~ 5.6 \times 10^{-309}\f$ for double precision.
  *  \brief Test finite
  *  \param x Float or double to test. 
  *  \returns True if the value is normalized.
  */
template < typename T >
inline int
is_normal( T x )
{
    switch ( std::fpclassify( x ) )
    {
    case FP_ZERO:
    case FP_NORMAL:
        return 1;
    default:
        return 0;
    }
    return 0;
}
#endif

/**  Test all elements are finite as defined for is_finite(). 
  *  finite_vect returns false if any element is not finite.
  *  \brief Test whether vector has finite elements.
  *  \param p Pointer to the first element of the vector.
  *  \param N Number of elements in the vector.
  *  \returns True if all elements are finite.
  */
inline bool
finite_vect( const float* p, size_t N )
{
    union pfi
    {
        const float*    f;
        const uint32_t* i;
    } px;
    const uint32_t msk( 0x7f800000 );
    px.f = p;
    const uint32_t* pi = px.i;
    for ( size_t i = 0; i < N; ++i )
    {
        if ( ( *pi++ & msk ) == msk )
            return false;
    }
    return true;
}

/**  Test all elements are finite as defined for is_finite().  
  *  finite_vect returns false if any element is not finite.
  *  \brief Test whether vector has finite elements.
  *  \param p Pointer to the first element of the vector.
  *  \param N Number of elements in the vector.
  *  \returns True if all elements are finite.
  */
inline bool
finite_vect( const double* p, size_t N )
{
    union pdi
    {
        const double*   d;
        const uint64_t* i;
    } px;
    const uint64_t msk( 0x7ff0000000000000ULL );
    px.d = p;
    const uint64_t* pi = px.i;
    for ( size_t i = 0; i < N; ++i )
    {
        if ( ( *pi++ & msk ) == msk )
            return false;
    }
    return true;
}

#ifndef isfinite
#define isfinite( x ) is_finite( x )
#endif

#ifndef isnormal
#define isnormal( x ) is_normal( x )
#endif

#endif // !defined(GDS_FPCLASS_HH)
