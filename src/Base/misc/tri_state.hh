/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef GDS_TRISTATE_HH
#define GDS_TRISTATE_HH

/**  tri_state class defines a 30state object that may have the values 
  *  {unset, off or on}. By default the tri_state is unset. If it is tested 
  *  (with bool(tri_state) ), an unset boolean is assumed to be off.
  */
class tri_state
{
public:
    /**  Construct an unset tri_state.
     *  \brief Default constructor.
     */
    tri_state( void );

    /**  Construct a tri_state and set it to a boolean value.
     *  \brief Boolean value constructor.
     */
    tri_state( bool on );

    tri_state( const tri_state& from ) = default;

    tri_state( tri_state&& from ) = default;

    tri_state& operator=( const tri_state& from ) = default;

    /**  Test whether the tri_state is set.
     *  \brief Test if set.
     *  \return True if the tri_state has been sst to a boolean value {on, off}
     */
    bool is_set( void ) const;

    /**  test the boolean state. An unset tri_state converts to false.
     *  \brief convert tri_state to bool.
     *  \return true if tri_state is on.
     */
    operator bool( void ) const;

    const char* state( void ) const;

    /**  Set the tri_state to a boolean value.
     *  \brief assignment operator.
     *  \param on New set state.
     *  \return set value.
     */
    bool operator=( bool on );

    /**  Set the tri_state to a boolean value.
     *  \brief Set the value.
     *  \param on New set state.
     */
    void set( bool on );

    /**  Set the tri_state to a value of "unset".
     *  \brief Unset the  tri_state
     */
    void unset( void );

private:
    int val;
};

//======================================  inline methods
inline tri_state::tri_state( void )
{
    unset( );
}

inline tri_state::tri_state( bool on )
{
    set( on );
}

inline bool
tri_state::is_set( void ) const
{
    return val != -1;
}

inline tri_state::operator bool( void ) const
{
    return val == 1;
}

inline bool
tri_state::operator=( bool on )
{
    set( on );
    return on;
}

inline void
tri_state::set( bool on )
{
    val = on ? 1 : 0;
}

inline const char*
tri_state::state( void ) const
{
    if ( !is_set( ) )
        return "unset";
    if ( !val )
        return "false";
    return "true";
}

inline void
tri_state::unset( void )
{
    val = -1;
}

#endif // !defined(GDS_TRISTATE_HH)
