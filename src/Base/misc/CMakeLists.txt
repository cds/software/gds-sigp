
add_library(misc OBJECT
    checksum_crc32.cc
    SigFlag.cc
)

target_include_directories(misc PUBLIC
    ${CMAKE_BINARY_DIR}/src/config
)

install(FILES
    ccstream.hh
    checksum_crc32.hh
    gds_fpclass.hh
    lcl_array.hh
    ManyFlags.hh
    redirect_std.hh
    SigFlag.hh
    SysError.hh
    test_endian.hh
    tri_state.hh
    DESTINATION include/gds-sigp
)
