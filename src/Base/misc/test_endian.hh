/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef TEST_ENDIAN_HH
#define TEST_ENDIAN_HH

/**  Class to test the endianness of the current platform. An endianness 
  *  flag is initialized in the constructor and subsequent calls to little_end
  *  or big_end are used to test the endianness of the platform.
  *  @memo Test platform endianness.
  *  @author John Zweizig (john.zweizig@ligo.org)
  *  @version 1.0; Last modified May 14, 2010
  */
class test_endian
{
    /// Overlap an int with a char array.
    union test_u
    {
        int  Int;
        char Char[ sizeof( int ) ];
    } test_data;

public:
    /**  Construct a test_endian object. The integer is set to zero and 
      *  then the first byte is set to 1.
      *  @memo Construct an endianness tester.
      */
    test_endian( void );

    /**  Test whether the corrent platform uses little endian byte ordering.
      *  @memo Test if little endian.
      *  @return true if little endian platform.
      */
    bool little_end( void ) const;

    /**  Test whether the corrent platform uses big endian byte ordering.
      *  @memo Test if big endian.
      *  @return true if big endian platform.
      */
    bool big_end( void ) const;
};

//======================================  Inline methods.
inline test_endian::test_endian( void )
{
    test_data.Int = 0;
    test_data.Char[ 0 ] = 1;
}

inline bool
test_endian::little_end( void ) const
{
    return test_data.Int == 1;
}

inline bool
test_endian::big_end( void ) const
{
    return test_data.Int != 1;
}

#endif // TEST_ENDIAN_HH
