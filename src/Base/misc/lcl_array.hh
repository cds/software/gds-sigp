/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef STD_LCL_ARRAY_HH
#define STD_LCL_ARRAY_HH
#include <cstring>
#include <cstdlib>

/**  \brief Local array template.
  *
  *  The lcl_array class can be used for heap-based local data storage.
  *  The array is allocated on 64-byte boundaries to align the data with
  *  the cache lines of most intel processors. The local arry auto-destructs 
  *  when the program leaves the scope un which the object was defined. This 
  *  is especially useful if exceptions are used to return control from a 
  *  point other than the end of the function.
  *  \author John G Zweizig
  *  \version 1.2; October 24, 2017
  */
template < class T >
class lcl_array
{
public:
    /** Array size type.
      */
    typedef unsigned long size_type;

private:
    /** Local storage data array.
     */
    T*        _data;
    size_type _size;

public:
    /** \brief Constructor
      *
      * Allocate a temporary array of length n.
      * @param _n number of element to allocate.
      */
    lcl_array( size_type _n );

    /** \brief Initializing constructor
      *
      *  Allocate a temporary array of length n and initialize it to values 
      *  at the specified location.
      *  @param _n number of element to allocate.
      */
    lcl_array( size_type _n, const T* ival );

    /** \brief Destructor
      *
      * Delete the local array.
      */
    ~lcl_array( void );

    /** \brief Get the array pointer
      *
      * Return a writable array pointer.
      * \return Writable pointer to the first element of the temporary array.
      */
    operator T*( void )
    {
        return _data;
    }

    /** \brief Get the array pointer
      *
      * Return a constant array pointer.
      * \return Constant pointer to the first element of the temporary array.
      */
    operator const T*( void ) const
    {
        return _data;
    }

    /**  \brief Get the array pointer
      *
      *  Get a writeable pointer.
      *  \return Writable pointer to the first element of the temporary array.
      */
    T*
    get( void )
    {
        return _data;
    }

    /** \brief Get the array pointer
      *
      * Return a constant array pointer.
      * \return Constant pointer to the first element of the temporary array.
      */
    const T*
    get( void ) const
    {
        return _data;
    }

    /**  Return number of allocated elements in the array.
      *  \brief Get array size.
      *  \return Number of elements.
      */
    size_type
    size( void ) const
    {
        return _size;
    }

    /**  Swap data between two lco_array instancces of the same type.
      *  \brief swap data
      *  \param lcla lcl_array to be swapped wwith this
      */
    void swap( lcl_array< T >& lcla );

private:
    /**  Dummy copy constructor to inhibit copying.
      *  @param x dummy argument
      */
    lcl_array( const lcl_array& x );

    /**  Dummy assignment operator to inhibit copying.
      *  @param x dummy argument
      */
    lcl_array& operator=( const lcl_array& x );
};

//====================================== inline in implementation
template < class T >
lcl_array< T >::lcl_array( size_type _n ) : _data( 0 ), _size( 0 )
{
    // _data = new T[_n];
    if ( !posix_memalign(
             reinterpret_cast< void** >( &_data ), 64, sizeof( T ) * _n ) )
    {
        _size = _n;
    }
}

template < class T >
lcl_array< T >::lcl_array( size_type _n, const T* ival )
    : _data( 0 ), _size( 0 )
{
    // _data = new T[_n];
    if ( !posix_memalign(
             reinterpret_cast< void** >( &_data ), 64, sizeof( T ) * _n ) )
    {
        _size = _n;
        if ( ival )
            memcpy( _data, ival, _n * sizeof( T ) );
    }
}

template < class T >
lcl_array< T >::~lcl_array( void )
{
    free( _data );
    // delete [] _data;
}

template < class T >
void
lcl_array< T >::swap( lcl_array< T >& lcla )
{
    size_type _n = _size;
    T*        _p = _data;
    _size = lcla._size;
    lcla._size = _n;
    _data = lcla._data;
    lcla._data = _p;
}

#endif // !defined(STD_LCL_ARRAY_HH)
