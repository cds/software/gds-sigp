/* -*- mode: c++; c-basic-offset: 4; -*- */
//
//    Signal access object
//
#ifndef __EXTENSIONS__
#define __EXTENSIONS__
#endif
#include "PConfig.h"
#include "SigFlag.hh"
#include <cstdio>
#include <signal.h>
#include <iostream>
#include <stdexcept>

using namespace std;

SigFlag*        SigFlag::sRoot = (SigFlag*)0;
SigFlag::mask_t SigFlag::sEnable = 0;

//======================================  Interrupt routine
//
//  Note that this routine should be made thread-safe... This may be
//  complicated by the fact that it is run as a signal handler. Replace
//  linked list with pointer vector?
void
SigFlag::SigFlagInt( int sig )
{
    for ( SigFlag* point = SigFlag::sRoot; point; point = point->mLink )
    {
        if ( point->isMember( sig ) )
        {
            point->signal_handler( sig );
        }
    }
}

extern "C" {
void
SigFlagIntC( int sig )
{
    SigFlag::SigFlagInt( sig );
}
}
//
//    Private disable method
//
void
SigFlag::disable( void )
{
    struct sigaction action;

    //-----------------------------------  Build active signal bit-mask
    mask_t tmask = 0;
    for ( SigFlag* p = sRoot; p; p = p->mLink )
        tmask |= p->mSigMask;

    //-----------------------------------  Loop over enabled signals, disable
    //                                     those no longer in use.
    for ( int sig = 0; sig < MAXSIG; sig++ )
    {
        mask_t imask = 1 << sig;
        if ( ( sEnable & ~tmask & imask ) != 0 )
        {
            action.sa_flags = 0;
            sigemptyset( &action.sa_mask );
            action.sa_handler = SIG_DFL;
            int rc = sigaction( sig, &action, NULL );
            if ( rc )
                perror( "Error in SigFlag::disable call to sigaction" );
            sEnable &= ~imask;
        }
    }
}

//--------------------------------------  Constructors
SigFlag::SigFlag( void )
    : mLink( 0 ), mSigMask( 0 ), mValue( 0 ), mFlags( kNull )
{
    chain( );
}

void
SigFlag::chain( void )
{
    SigFlag* last = before( (SigFlag*)0 );
    if ( last == (SigFlag*)1 )
    {
        throw std::logic_error( "No end to signal chain" );
    }
    else if ( last )
    {
        last->mLink = this;
    }
    else
    {
        sRoot = this;
    }
}

SigFlag::SigFlag( int signo, sigFunc flags )
    : mLink( 0 ), mSigMask( 0 ), mValue( 0 ), mFlags( flags )
{
    chain( );
    add( signo );
}

//---------------------------------------  Set the mode
void
SigFlag::setMode( sigFunc flags )
{
    mFlags |= mask_t( flags );
    for ( int sig = 0; sig < MAXSIG; sig++ )
    {
        if ( isMember( sig ) )
        {
            if ( ( flags & kBlock ) != 0 )
                siginterrupt( sig, 0 );
        }
    }
}

void
SigFlag::clrMode( sigFunc flags )
{
    mFlags &= ~mask_t( flags );
    for ( int sig = 0; sig < MAXSIG; sig++ )
    {
        if ( isMember( sig ) )
        {
            if ( ( flags & kBlock ) != 0 )
                siginterrupt( sig, 1 );
        }
    }
}

//---------------------------------------  Destructor
SigFlag::~SigFlag( )
{
    zero( );

    SigFlag* last = before( this );
    if ( !last )
    {
        sRoot = mLink;
    }
    else if ( last == (SigFlag*)1 )
    {
        cerr << "This SigFlag is not in chain" << endl;
    }
    else
    {
        last->mLink = mLink;
    }
}

//======================================  Zero flag.
void
SigFlag::zero( void )
{
    mSigMask = 0;
    disable( );
}

//======================================  Add a signal to a flag object
void
SigFlag::add( int sig )
{
    if ( ( sig <= 0 ) || ( sig >= MAXSIG ) )
    {
        std::cerr << "Invalid signal number: " << sig << std::endl;
        return;
    }
    mask_t imask = ( 1 << sig );
    mSigMask |= imask;
    if ( !( sEnable & imask ) )
    {
        struct sigaction action;
        sEnable |= imask;
        action.sa_flags = 0;
        if ( fOnce( ) )
            action.sa_flags = SA_RESETHAND;
        if ( fBlock( ) )
            action.sa_flags |= SA_RESTART;
        sigemptyset( &action.sa_mask );
        action.sa_handler = SigFlagIntC;
        int rc = sigaction( sig, &action, NULL );
        if ( rc )
            perror( "Error in SigFlag::add call to sigaction" );
    }
}

//======================================  Remove a signal from a SigFlag
void
SigFlag::remove( int sig )
{
    if ( ( sig <= 0 ) || ( sig >= MAXSIG ) )
        return;
    mSigMask &= ~( 1 << sig );
    disable( );
}

//======================================  Signal handler
void
SigFlag::signal_handler( int sig )
{
    mValue |= mask_t( 1 << sig );
}
