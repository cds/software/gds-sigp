/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef SIGFLAG_HH
#define SIGFLAG_HH

//
//    Signal access object
//
#include <signal.h>

#ifndef MAXSIG
#define MAXSIG 32
#endif

/**  The %SigFlag class establishes a signal handler for one or more specified 
  *  signals. When a signal occurs, the handler remembers the signal, and
  *  optionally rearms. By default the signal will interrupt any blocked
  *  system functions. At present the handling of a signal by several 
  *  SigFlags will give poorly defined results.
  *  @memo Signal handler.
  *  @author J. Zweizig
  *  @version $Id$
  */
class SigFlag
{
public:
    /**  Create an unattached %SigFlag instance.
      *  @memo Default constructor.
      */
    SigFlag( void );

    /**  Enumerate the bits for the signal function modifier flags. 
      *  The valid bit values are: 
      *  <ul>
      *    <li> kOnce: the handler catches a signal once and disarms
      *    <li> kBlock: blocking system calls are not interrupted. 
      *  </ul>
      *  @memo Function modifier flags.
      */
    enum sigFunc
    {
        kNull = 0, ///< No options
        kOnce = 1, ///< Deliver the signal only once.
        kBlock = 2 ///< Don't interrupt system calls
    };

    /// Signal mask data type.
    typedef unsigned int mask_t;

    /**  A %SigFlag instance is constructed and attached to the specified 
      *  signal.
      *  \brief Construct a %SigFlag and attach it to a signal.
      *  \param sig   Signal number to attach.
      *  \param flags Signal option flags.
      */
    explicit SigFlag( int sig, sigFunc flags = kNull );

    /**  Detach signals and destroy the %SigFlag instance.
      *  \brief destructor.
      */
    virtual ~SigFlag( void );

    /**  Get the kBlock bit of the flag word. The kBlock flag indicates 
      *  that the signal(s) specified by the object should not interrupt
      *  processing of the interruptable system calls.
      *  @memo Test kBlock flag.
      *  @return True if kBlock mode flag is set.
      */
    bool fBlock( void ) const;

    /**  Get the kOnce bit of the flag word. The kOnce flag indicates 
      *  that the signal(s) specified by the object should be allowed to
      *  fire only once.
      *  @memo Test kOnce flag.
      *  @return True if kOnce mode flag is set.
      */
    bool fOnce( void ) const;

    /**  Get a mask containing one bit per signal caught.
      *  @memo Get signal mask.
      *  @return Mask with a bit set for each caught signal.
      */
    mask_t getSigFlags( void ) const;

    /**  Test if the specified signal is a member of this flag group.
      *  @memo Test if signal is in this flag group
      *  @return True if the specified signal is a member of this flag.
      *  @param sig Signal to be tested for membership.
      */
    bool isMember( int sig ) const;

    /**  The %SigFlag is converted to true if any signal has been caught 
      *  and not cleared.
      *  @memo Conversion to bool data type.
      *  @return True if one or more signal flags are set.
      */
    operator bool( void ) const;

    /**  Test returns true if any of the signals handled by the object has
      *  been caught.
      *  @memo Test if any signal has been caught.
      *  @return True if any signal has been caught.
      */
    bool test( void ) const;

    /**  Test returns true if the specified signals handled by the object 
      *  has been caught.
      *  @memo Test if a specific signal has been caught.
      *  @return True if specified signal has been caught.
      *  @param sig Signal number to be tested.
      */
    bool test( int sig ) const;

    //----------------------------------  Mutators
    /**  Add a signal to the list of signals handled by this instance.
      *  @memo Add a signal.
      *  @param sig signal number to be added to member list.
      */
    void add( int sig );

    /**  Clear the flags for all signals. Clear returns true if one or
      *  more flags were set.
      *  @memo Clear all signal flags.
      *  @return true if one or more signals were set.
      */
    bool clear( void );

    /**  Clear the specified signal flag. Clear returns true if the 
      *  specified flag was set.
      *  \brief Clear a specified flag.
      *  \param sig Signal number to be removed from the mask
      *  \return True if specified signal was set.
      */
    bool clear( int sig );

    /**  The specified signal flags are cleared.
      *  @memo Clear the specified signal flags.
      *  @param flags Bit mask of signals to clear.
      */
    void clearFlags( mask_t flags );

    /**  The specified processing mode flags are cleared.
      *  @memo Clear the optional processing modes.
      *  @param flags Bit mask of mode flags to clear.
      */
    void clrMode( sigFunc flags = kNull );

    /**  Remove a signal from the list of signals handled by this object.
      *  No action is taken if the specified signal is not a member of 
      *  the flag. 
      *  @memo Remove a signal from flag member list.
      *  @param sig Number of signal to be removed from the ember list.
      */
    void remove( int sig );

    /**  Set an unmaskable signal flag. This function gives the user a 
      *  means to set a flag without sending a signal. 
      *  @memo Set the unmaskable signal 0 flag.
      */
    void setSig0( void );

    /**  The %SigFlag mode flags are set as specified.
      *  @memo Set the optional processing modes.
      *  @param flags Bit mask of mode flags to set.
      */
    void setMode( sigFunc flags = kNull );

    /**  All signals are removed from the flag.
      *  @memo Remove all signals.
      */
    void zero( void );

    /**
     *  Function that receives control when a signal is caught.
     *  \brief Signal handler
     *  \param sig Caught signal number.
     */
    virtual void signal_handler( int sig );

    /**  Static signal interrupt dispatcher function. SigFlagInt scans the
      *  currently defined handlers for the one that attached to the 
      *  specified signal.
      *  \brief Interrupt handler.
      *  \param sig Signal number.
      */
    static void SigFlagInt( int sig );

    //------------------------------------  Private methods
private:
    /**  Internal function to disable a signal.
      *  @memo Disable a signal.
      */
    void disable( );

    /**  Point to the object previous to the argument in the %SigFlag list.
      */
    void chain( void );

    /**  Point to the object previous to the argument in the %SigFlag list.
      */
    SigFlag* before( const SigFlag* id ) const;

    //------------------------------------  Data members (private)
private:
    /**  sRoot points to the first %SigFlag in a singly-linked list. The 
      *  signal handler follows this list to see which flag(s) is (are)
      *  to be set.
      *  @memo Pointer to a linked list of %SigFlag descriptors.
      */
    static SigFlag* sRoot;

    /**  sEnable has a corresponding bit set for each signal enabled.
      *  @memo Global enabled signal mask.
      */
    static mask_t sEnable;

    /**  Link field in the singly linked list.
      *  @memo Pointer to the next %SigFlag.
      */
    SigFlag* mLink;

    /**  Mask of signals to be handled by this flag object.
      *  @memo Signal Mask.
      */
    mask_t mSigMask;

    /**  Each time a signal is caught the associated mask bit is set 
      *  in mValue.
      *  @memo Caught signal mask.
      */
    volatile mask_t mValue;

    /**  mFlags define any optional processing required.
      *  @memo Function flags.
      */
    mask_t mFlags;
};

//--------------------------------------  Test if signal is a member
inline bool
SigFlag::isMember( int sig ) const
{
    return ( mSigMask & ( 1 << sig ) ) != 0;
}

//--------------------------------------  Conversion to bool data type.
inline SigFlag::operator bool( ) const
{
    return ( mValue != 0 );
}

//--------------------------------------  Test all signal flags
inline bool
SigFlag::test( void ) const
{
    return bool( *this );
}

//--------------------------------------  Test a specified signal flag
inline bool
SigFlag::test( int sig ) const
{
    return ( ( mValue & ( 1 << sig ) ) != 0 );
}

//--------------------------------------  Clear all Signal flags
inline bool
SigFlag::clear( void )
{
    if ( !mValue )
        return false;
    mValue = 0;
    return true;
}

//--------------------------------------  Clear a specified flag.
inline bool
SigFlag::clear( int sig )
{
    mask_t tmask = 1 << sig;
    if ( !( mValue & tmask ) )
        return false;
    mValue &= ~tmask;
    return true;
}

//--------------------------------------  Clear a specified flag.
inline void
SigFlag::clearFlags( mask_t flags )
{
    mValue &= ~flags;
}

//--------------------------------------  Set the sig0 flag
inline void
SigFlag::setSig0( void )
{
    mValue |= 1;
}

//--------------------------------------  Test Block flag.
inline bool
SigFlag::fBlock( ) const
{
    return ( mFlags & kBlock ) != 0;
}

//--------------------------------------  Test Once flag.
inline bool
SigFlag::fOnce( ) const
{
    return ( mFlags & kOnce ) != 0;
}

//--------------------------------------  Get signal mask
inline SigFlag::mask_t
SigFlag::getSigFlags( void ) const
{
    return mValue;
}

//--------------------------------------  Find predecessor function in list
inline SigFlag*
SigFlag::before( const SigFlag* id ) const
{
    if ( sRoot == id )
        return (SigFlag*)0;
    for ( SigFlag* p = sRoot; p; p = p->mLink )
        if ( p->mLink == id )
            return p;
    return (SigFlag*)1;
}

#endif
