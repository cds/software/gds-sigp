#ifndef COMPLEX_HH
#define COMPLEX_HH

#include <complex>

typedef std::complex< float >  fComplex;
typedef std::complex< double > dComplex;


// differs from stdlib
double inline abs( const std::complex< double >& z )
{
  return ::sqrt(z.real()*z.real() + z.imag()*z.imag());
}

// differs from stdlib
inline std::complex< double >
operator/( const std::complex< double >& lhs, const std::complex< double >& rhs )
{
    return ( lhs * conj( rhs ) ) / norm( rhs );
}


// not defined in stdlib
template < class T >
std::complex< T >
xcc( std::complex< T >& lhs, std::complex< T > rhs )
{
    lhs = lhs * conj( rhs );
    return lhs;
}

// not defined in stdlib
template < class T >
std::complex< T >
operator~( std::complex< T > z )
{
    return conj( z );
}

// not defined in stdlib
template < class T >
bool
operator!( std::complex< T > z )
{
    return ( !z.real( ) && !z.imag( ) );
}

// not defined in stdlib
inline std::complex< double >
operator*( float lhs, const std::complex< double >& rhs )
{
    return double(lhs) * rhs;
}

// not defined in stdlib
inline std::complex< double >
operator*( const std::complex< double >& lhs, float rhs )
{
    return lhs * double(rhs);
}

// not defined in stdlib
inline std::complex< double >
operator*( const std::complex< float >& lhs, double rhs )
{
    return std::complex< double >(lhs) * rhs;
}

// not defined in stdlib
inline std::complex< double >
operator/( const std::complex< double >& lhs, int rhs )
{
    return lhs / double(rhs);
}

// not defined in stdlib
inline std::complex< double >
operator+( const std::complex< float >& lhs, const std::complex< double >& rhs )
{
    return std::complex< double >(lhs) + rhs;
}

#endif
