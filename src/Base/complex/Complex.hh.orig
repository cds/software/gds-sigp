#ifndef COMPLEX_HH
#define COMPLEX_HH
#include <time.h>
#include <cmath>
#include <iosfwd>

/**  basicplx implements complex data type by means of the template mechanism.
  *  All functions are in-line to maximize efficiency.
  *  @memo Complex number template class.
  *  @author John Zweizig
  *  @version 1.3; Modified 5/30/00
  */
template<class T>
class basicplx {
public:
  /// Real data type used for all math operations.
  typedef double math_type;

  /// Define the type name for use by functions using a basicplx.
  typedef T data_type;

  /**  Create a complex number. The data are left uninitialized to increase
    *  efficiency.
    *  @memo Default constructor.
    */
  basicplx() {}

  /**  Destroy a complex number.
    *  @memo Destructor.
    */
  ~basicplx() {}

  /**  Create a complex number and initialize it from another complex 
    *  type number.
    *  @memo Copy constructor.
    *  @param rhs Complex number to be copied or converted to this.
    */ 
#ifndef __CINT__
  template<class U>
  basicplx(const basicplx<U>& rhs);
#endif
  //basicplx(const basicplx& rhs);

  /**  Create a complex and initialize it from its real and (optional) 
    *  imaginary components.
    *  @memo Initializing constructor.
    *  @param Re Real part of the complex number.
    *  @param Im Imaginary part of the complex number.
    */
  basicplx(T Re, T Im=0.0);

  //  Note that the cast operator was formerly defined. This tends to be
  //  non-intuitive so it was removed on 23-Jul-1999 JGZ. Specifically,
  //  "fComplex * double" is evaluated as "float(fComplex) * double"!
  // /**  Cast to float.
  //   *  Casting a complex to a float projects out the real component.
  //   */
  // operator T(void) const;

  /**  The rhs is copied to the lhs variable.
    *  @memo Assignment operator.
    *  @param rhs Complex value to be assigned to this.
    *  @return Reference to the modified complex variable.
    */
#ifndef __CINT__
  template<class U>
  basicplx& operator=(const basicplx<U>& rhs);
#endif
  //basicplx& operator=(const basicplx<T> &rhs);

  /**  The complex numbers are added and left in the lhs argument.
    *  The result is also returned by reference.
    *  @memo Add a complex number.
    *  @param rhs Complex number to be added to this.
    *  @return Reference to the sum variable.
    */
  basicplx& operator+=(const basicplx &rhs);

  /**  The rhs complex number is subtracted  from and the result is left 
    *  in the lhs argument. The result is also returned by reference.
    *  @memo Subtract a complex number.
    *  @param rhs Complex number to be subtracted from this.
    *  @return Reference to the difference variable.
    */
  basicplx& operator-=(const basicplx &rhs);

  /**  The lhs complex variable is multiplied by the rhs argument. The 
    *  result replaces the lhs argument and is returned by reference.
    *  @memo Multiply a complex number by a complex.
    *  @param rhs Complex number to multiply this by.
    *  @return Reference to the product variable.
    */
  basicplx& operator*=(const basicplx &rhs);

  /**  The lhs complex variable is multiplied by the rhs float. The 
    *  result replaces the lhs complex argument and is returned
    *  by reference.
    *  @memo Multiply complex by real math value.
    *  @param rhs Real number to multiply this by.
    *  @return Reference to the product variable.
    */
  basicplx& operator*=(math_type rhs);

  /**  The lhs complex variable is divided by the rhs argument. The 
    *  result replaces the lhs argument and is also returned.
    *  @memo Complex / Complex.
    *  @param rhs Complex number to divide this by.
    *  @return Reference to the dividend variable.
    */
  basicplx& operator/=(const basicplx &rhs);

  /**  The lhs complex variable is divided by the rhs argument. The 
    *  result replaces the lhs argument and is also returned.
    *  @memo Complex / float.
    *  @param rhs Real number to divide this by.
    *  @return Reference to the dividend variable.
    */
  basicplx& operator/=(math_type rhs);


  //--------------------------------------  Accessors
  /**  Evaluates to true if the lhs is exactly equal to the rhs.
    *  @memo Compare two complex numbers.
    *  @param y Comparison value.
    *  @return True if equal.
    */
  bool operator==(const basicplx& y) const;

  /**  Evaluates to true if the lhs is unequal to the rhs.
    *  @memo Compare two complex numbers.
    *  @param y Comparison value.
    *  @return True if not equal.
    */  
  bool operator!=(const basicplx& y) const {
     return !(*this == y); }

  /**  Evaluates to true if the complex object is zero.
    *  @memo Test for complex variable equal to zero.
    *  @return True if equal to zero.
    */
  bool operator!(void) const;

  /**  The complex conjugate of the argument is returned.
    *  @memo Complex conjugation.
    *  @return Complex conjugate of this.
    */
  basicplx operator~(void) const;

  /**  The negative of the argument is returned.
    *  @memo Unary negate.
    *  @return Negative of this.
    */
  basicplx operator-(void) const;

  /**  The complex number is set to 
    *  \f$ mag \times (\cos{arg} + i \sin{arg} ) \f$.
    *  @memo Set a complex from magnitude and argument.
    *  @param mag Magnitude of complex to assign.
    *  @param arg Argument of of the complex in radians.
    *  @return Reference to the modified complex.
    */
  basicplx& setMArg(math_type mag, math_type arg);

  /**  The complex object is multiplied by the conjugate of the complex 
    *  argument.
    *  @memo Multiply by ~arg.
    *  @param rhs Complex number to be conjugated and multiplied
    *  @return Reference to the product variable.
    */
  basicplx& xcc(const basicplx& rhs);

  //This doesn't seem very useful....
  //basicplx& xcc(math_type rhs);

  /**  The square of the magnitude, \e i.e \f$ ||x|| = (x \times x^{*} ) \f$, 
    *  is returned.
    *  @memo Magnitude Squared.
    *  @return Magnitude squared of this.
    */
  math_type MagSq(void) const;

  /**  The magnitude \e i.e. \f$ |x| = \sqrt{ x \times x^{*} } \f$ is returned.
    *  @memo Magnitude.
    *  @return Magnitude of this.
    */
  math_type Mag(void) const;

  /**  Return the argument in the range \f$ -\pi \le x < \pi \f$.
    *  @memo Argument.
    *  @return Complex argument.
    */
  math_type Arg(void) const;

  /**  the real part of the complex number is returned.
    *  @memo Real Part.
    *  @return Real part of this.
    */
  T Real() const;

  /**  The imaginary part of the complex number is returned.
    *  @memo Imaginary Part.
    *  @return Imaginary part of this.
    */
  T Imag() const;

  /**  The argument and the object are swapped.
    *  @memo Swap two complex numbers.
    *  @param x Complex variable to be swapped with this.
    */
  void swap(basicplx& x);

private:
  /**  Variable containing the real part of the complex number.
    *  @memo Real Part.
    */
  T mRe;

  /**  Imaginary part.
    *  Variable containing the imaginary part of the complex number.
    */
  T mIm;
};

#ifndef __CINT__

// template<class T>
// inline
// basicplx<T>::basicplx(const basicplx<T>& rhs) {
//    mRe = rhs.mRe;
//    mIm = rhs.mIm;}

template<class T> template<class U>
inline 
basicplx<T>::basicplx(const basicplx<U>& rhs) {
   mRe = rhs.Real();
   mIm = rhs.Imag();
}

template<class T>
inline
basicplx<T>::basicplx(T Re, T Im) {
    mRe = Re;
    mIm = Im;}

// Removed 23-Jul-1999 - See class definition for discussion.
// template<class T>
// inline
// basicplx<T>::operator T(void) const {
//     return mRe;}

// template<class T>
// inline basicplx<T>&
// basicplx<T>::operator=(const basicplx<T> &rhs) {
//     mRe = rhs.mRe;
//    mIm = rhs.mIm;
//    return (*this);}

template<class T> template<class U>
basicplx<T>& basicplx<T>::operator=(const basicplx<U>& rhs) {
   mRe = rhs.Real();
   mIm = rhs.Imag();
   return (*this);}

template<class T>
inline basicplx<T>&
basicplx<T>::operator+=(const basicplx<T> &rhs) {
    mRe += rhs.mRe;
    mIm += rhs.mIm;
    return (*this);}

template<class T>
inline basicplx<T>&
basicplx<T>::operator-=(const basicplx<T> &rhs) {
    mRe -= rhs.mRe;
    mIm -= rhs.mIm;
    return (*this);}

template<class T>
inline basicplx<T>&
basicplx<T>::operator*=(const basicplx<T> &rhs) {
    T Re = mRe*rhs.mRe - mIm*rhs.mIm;  // Note: this IS safe for x*=x;
    T Im = mRe*rhs.mIm + mIm*rhs.mRe;
    mRe  = Re;
    mIm  = Im;
    return (*this);}

template<class T>
inline basicplx<T>&
basicplx<T>::xcc(const basicplx<T> &rhs) {
    T Re = mRe*rhs.mRe + mIm*rhs.mIm;    // Note: This should be safe.
    mIm  = mIm*rhs.mRe - mRe*rhs.mIm;
    mRe  = Re;
    return (*this);}

template<class T>
inline basicplx<T>&
basicplx<T>::operator*=(math_type rhs) {
    mRe *= rhs;
    mIm *= rhs;
    return (*this);}

//template<class T>
//inline basicplx<T>&
//basicplx<T>::xcc(math_type rhs) {
//    return (*this *= rhs);
//}

template<class T>
inline basicplx<T>&
basicplx<T>::operator/=(const basicplx<T> &rhs) {
    xcc(rhs);
    (*this) /= rhs.MagSq();
    return (*this);}

template<class T>
inline basicplx<T>&
basicplx<T>::operator/=(math_type rhs) {
    if (rhs) {
        mRe /= rhs;
	mIm /= rhs;}
    return (*this);
}

template<class T>
inline bool
basicplx<T>::operator==(const basicplx<T>& x) const {
    return (mRe == x.mRe) && (mIm == x.mIm);
}

template<class T>
inline bool
basicplx<T>::operator!(void) const {
    return !mRe && !mIm;}

template<class T>
inline basicplx<T>
basicplx<T>::operator~(void) const {
    basicplx<T> r(mRe, -mIm);
    return r;}

template<class T>
inline basicplx<T>
basicplx<T>::operator-(void) const {
    basicplx<T> r(-mRe, -mIm);
    return r;}

template<class T>
inline typename basicplx<T>::math_type 
basicplx<T>::MagSq(void) const {
    math_type re = mRe;
    math_type im = mIm;
    return re*re + im*im;
}

template<class T>
inline typename basicplx<T>::math_type 
basicplx<T>::Mag(void) const {
    math_type re = mRe;
    math_type im = mIm;
    return ::sqrt(re*re + im*im);
}

template<class T>
inline typename basicplx<T>::math_type 
basicplx<T>::Arg(void) const {
    return atan2(math_type(mIm), math_type(mRe));
}

template<class T>
inline T 
basicplx<T>::Real() const {
    return mRe;}

template<class T>
inline T 
basicplx<T>::Imag() const {
    return mIm;}

template<class T>
inline void
basicplx<T>::swap(basicplx<T>& x) {
    T t   = mRe;
    mRe   = x.mRe;
    x.mRe = t;
    t     = mIm;
    mIm   = x.mIm;
    x.mIm = t;}

//-------------------------------  Set a complex from (magnitude, argument)
template<class T>
inline basicplx<T>&
basicplx<T>::setMArg(math_type mag, math_type arg) {
    mRe = mag * ::cos(arg);
    mIm = mag * ::sin(arg);
    return *this;
}
#endif // !defined(__CINT__)

//______________________________________
//______________________________________   NON-MEMBER FUNCTIONS

/**@name Non-member complex functions
 */
//@{

/**  The sum of two complex numbers is returned.
  *  @memo basicplx + basicplx.
  */
template<class T>
inline basicplx<T> 
operator+(const basicplx<T>& lhs, const basicplx<T>& rhs) {
    basicplx<T> r = lhs;
    r += rhs;
    return r;}

/**  The difference of two complex numbers is returned.
  *  @memo basicplx - basicplx.
  */
template<class T>
inline basicplx<T> 
operator-(const basicplx<T>& lhs, const basicplx<T>& rhs) {
    basicplx<T> r = lhs;
    r -= rhs;
    return r;}

/**  The product of two complex numbers is returned.
  *  @memo basicplx * basicplx.
  *  @return The complex product of the two arguments
  */
template<class T>
inline basicplx<T> 
operator*(const basicplx<T>& lhs, const basicplx<T>& rhs) {
    basicplx<T> r = lhs;
    r *= rhs;
    return r;}

/**  basicplx * float.
  *  The product of a complex number and a float is returned.
  */
template<class T>
inline basicplx<T> 
operator*(typename basicplx<T>::math_type lhs, const basicplx<T>& rhs) {
    basicplx<T> r(rhs);
    r *= lhs;
    return r;}

/**  The product of a complex number and a float is returned.
  *  @memo float * Complex
  */
template<class T>
inline basicplx<T> 
operator*(const basicplx<T>& lhs, typename basicplx<T>::math_type rhs) {
    basicplx<T> r(lhs);
    r *= rhs;
    return r;}

/**  basicplx / basicplx.
  *  The ratio of two complex numbers is returned.
  */
template<class T>
inline basicplx<T> 
operator/(const basicplx<T>& lhs, const basicplx<T>& rhs) {
    basicplx<T> r(lhs);
    r /= rhs;
    return r;}

/**  basicplx / float.
  *  The ratio of two complex numbers is returned.
  */
template<class T>
inline basicplx<T> 
operator/(const basicplx<T>& lhs, typename basicplx<T>::math_type rhs) {
    basicplx<T> r(lhs);
    r /= rhs;
    return r;}

/**  Print a complex number to an output stream.
  *  The complex number is printed <real>+<imag>i.
  */
template<class T>
inline std::ostream& 
operator<<(std::ostream& out, const basicplx<T>& rhs) 
{
    if (rhs.Imag() < 0) return out << rhs.Real() << rhs.Imag() << "i";
    else                return out << rhs.Real() << "+" << rhs.Imag() << "i";
}

/** complex conjugate
 */
template<class T>
inline basicplx<T>
cc(const basicplx<T>& x) {
  basicplx<T> r(Real(x),(-Imag(x)));
  return r;
}

/**  e to a complex power.
  *  Raise e to a complex power.
  */
template<class T>
inline basicplx<T> 
exp(const basicplx<T>& x) {
    basicplx<T> r;
    r.setMArg(::exp(Real(x)), Imag(x));
    return r;
}

/**  Natural logarithm of a complex number.
  *  Logarithm base e is taken.
  */
template<class T>
inline basicplx<T> 
log(const basicplx<T>& x) {
    return basicplx<T>(0.5 * ::log(x.MagSq()), x.Arg());
}

/**  Argument of a complex number.
  *  Extracts the argument of a complex number
  */
template <class T>
inline typename basicplx<T>::math_type 
Arg(const basicplx<T>& x) {
    return x.Arg();
}

/**  Argument of a complex number.
  *  Extracts the argument of a complex number
  */
template <class T>
inline typename basicplx<T>::math_type 
arg(const basicplx<T>& x) {
    return x.Arg();
}

/**  Real part of a complex number.
  *  Extracts the real part of a complex number
  */
template <class T>
inline T 
Real(const basicplx<T>& x) {
    return x.Real();
}

/**  Imaginary part of a complex number.
  *  Extracts the imaginary part of a complex number
  */
template <class T>
inline T 
Imag(const basicplx<T>& x) {
    return x.Imag();
}

/**  Absolute value of a complex number.
  *  the absolute value of a complex number is returned.
  */
template <class T>
inline typename basicplx<T>::math_type
abs(const basicplx<T>& x) {
    return x.Mag();
}

/**  Square root of a complex number.
  *  The square root of a complex number is returned. The root with a
  *  positive real part is returned.
  */
template <class T>
inline basicplx<T> 
sqrt(const basicplx<T>& rhs) {
    typename basicplx<T>::math_type det = (abs(rhs) + Real(rhs))*0.5;
    if (Imag(rhs) >= 0) {
        return basicplx<T>(::sqrt(det),  ::sqrt(det-Real(rhs)));
    } else {
        return basicplx<T>(::sqrt(det), -::sqrt(det-Real(rhs)));
    }
}
//@}

/// Single precision complex data type
typedef basicplx<float>  fComplex;

/// Double precision complex data type
typedef basicplx<double> dComplex;

#endif  //  COMPLEX_HH






