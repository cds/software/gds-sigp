#include <random>
#include <iostream>
#include <memory>
#include <string>
#include <stdexcept>
#include <complex>
#include <time.h>

#include "Complex.hh"

// https://stackoverflow.com/a/26221725
template < typename... Args >
std::string
string_format( const std::string& format, Args... args )
{
    int size_s = std::snprintf( nullptr, 0, format.c_str( ), args... ) +
        1; // Extra space for '\0'
    if ( size_s <= 0 )
    {
        throw std::runtime_error( "Error during formatting." );
    }
    auto                      size = static_cast< size_t >( size_s );
    std::unique_ptr< char[] > buf( new char[ size ] );
    std::snprintf( buf.get( ), size, format.c_str( ), args... );
    return std::string(
        buf.get( ), buf.get( ) + size - 1 ); // We don't want the '\0' inside
}

std::complex< double >
cust_to_std( dComplex z )
{
    return std::complex< double >( z.Real( ), z.Imag( ) );
}

dComplex
std_to_cust( std::complex< double > z )
{
    return dComplex( z.real( ), z.imag( ) );
}

std::string
complex_to_string( std::complex< double > z )
{
    return string_format( "%-20.17g + i%-20.17g", z.real( ), z.imag( ) );
}

std::string
complex_to_string( dComplex z )
{
    return string_format( "%-20.17g + i%-20.17g", z.Real( ), z.Imag( ) );
}

std::string
double_to_string( double d )
{
    return string_format( "%-20.17g", d );
}

int
compare_complex( std::complex< double > lhs, dComplex rhs )
{
    return lhs == cust_to_std( rhs ) && std_to_cust( lhs ) == rhs;
}

int
compare_complex( dComplex lhs, std::complex< double > rhs )
{
    return lhs == std_to_cust( rhs ) && cust_to_std( lhs ) == rhs;
}

int
main( )
{
    double lower_bound = -10000;
    double upper_bound = 10000;

    std::uniform_real_distribution< double > unif( lower_bound, upper_bound );
    std::default_random_engine               engine;

    engine.seed( (unsigned)time( NULL ) );

    double re_1 = unif( engine );
    double im_1 = unif( engine );

    double re_2 = unif( engine );
    double im_2 = unif( engine );

    double d = unif( engine );

    double r = unif( engine );
    double theta = unif( engine );

    dComplex cust_1 = dComplex( re_1, im_1 );
    dComplex cust_2 = dComplex( re_2, im_2 );

    std::complex< double > std_1 = std::complex< double >( re_1, im_1 );
    std::complex< double > std_2 = std::complex< double >( re_2, im_2 );

    std::cout << cust_1 << std::endl;
    std::cout << std_1 << std::endl;

    std::cout << cust_2 << std::endl;
    std::cout << std_2 << std::endl;

    std::cout << "1. " << ( std_to_cust( std_1 ) == cust_1 ) << std::endl;
    std::cout << "2. " << ( std_to_cust( std_2 ) == cust_2 ) << std::endl;

    std::cout << "3. " << ( cust_to_std( cust_1 ) == std_1 ) << std::endl;
    std::cout << "4. " << ( cust_to_std( cust_2 ) == std_2 ) << std::endl;

    std::cout << "5. " << ( std_to_cust( cust_to_std( cust_1 ) ) == cust_1 )
              << std::endl;
    std::cout << "6. " << ( cust_to_std( std_to_cust( std_1 ) ) == std_1 )
              << std::endl;

    std::cout << "19. " << compare_complex( log( cust_1 ), log( std_1 ) )
              << std::endl;
    std::cout << complex_to_string( log( cust_1 ) ) << std::endl;
    std::cout << complex_to_string(
                     dComplex( 0.5 * ::log( cust_1.MagSq( ) ), cust_1.Arg( ) ) )
              << std::endl;
    std::cout << complex_to_string( log( std_1 ) ) << std::endl;
    std::cout << complex_to_string( std::complex< double >(
                     0.5 * ::log( std::norm( std_1 ) ), arg( std_1 ) ) )
              << std::endl;
    std::cout << cust_1.MagSq( ) << " = " << std::norm( std_1 ) << std::endl;

    std::cout << "7. " << compare_complex( cust_1 + cust_2, std_1 + std_2 )
              << std::endl;
    std::cout << "8. " << compare_complex( cust_1 - cust_2, std_1 - std_2 )
              << std::endl;
    std::cout << "9. " << compare_complex( cust_1 * cust_2, std_1 * std_2 )
              << std::endl;
    std::cout << "10. " << compare_complex( -cust_1, -std_1 ) << std::endl;

    std::cout << "11." << std::endl;
    std::cout << complex_to_string( cust_1 / cust_2 ) << std::endl
              << complex_to_string( std_1 / std_2 ) << std::endl
              << complex_to_string( ( std_1 * conj( std_2 ) ) / norm( std_2 ) )
              << std::endl;

    std::cout << "12." << std::endl;
    std::cout << ( cust_1.Mag( ) == abs( std_1 ) ) << std::endl;
    std::cout << double_to_string( cust_1.Mag( ) ) << std::endl
              << double_to_string( abs( std_1 ) ) << std::endl;

    std::cout << "13. " << ( cust_1.MagSq( ) == std::norm( std_1 ) )
              << std::endl;
    std::cout << "14. " << ( cust_1.Arg( ) == arg( std_1 ) ) << std::endl;

    std::cout << "15. " << compare_complex( cc( cust_1 ), conj( std_1 ) )
              << std::endl;
    std::cout << "16. " << compare_complex( ~cust_1, conj( std_1 ) )
              << std::endl;

    std::cout << "17. "
              << compare_complex( cust_1.xcc( cust_2 ), std_1 * conj( std_2 ) )
              << std::endl;

    std::cout << "18. "
              << compare_complex( cust_1.setMArg( r, theta ),
                                  std::polar( r, theta ) )
              << std::endl;

    return 0;
}
