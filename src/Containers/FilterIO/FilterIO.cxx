//
// FilterIO
//

// $Id$
// $Log$
// Revision 1.1  1999/10/18 17:29:36  jzweizig
// New filter I/O container and associated classes
//

#include "FilterIO.hh"

#include <stdexcept>
#include <string>
#include <map>

#include "Key.hh"
#include "KeyChain.hh"
#include "TSeries.hh"

FilterIO::~FilterIO(void) {}

// For assignment
TSeries& FilterIO::operator[] (const Key& key) 
{
  return iomap[key];
}

void FilterIO::erase(const Key& key)
{
  iomap.erase(key);
}

void FilterIO::clear(void) 
{
  iomap.clear();
}

// Inquiries

bool FilterIO::isFull(void) const
{
  return iomap.size() == chain.size();
}

bool FilterIO::isEmpty(void) const
{
  return iomap.empty();
}

KeyChain
FilterIO::keyChain(void) const 
{
  return chain;
}

FilterIO::FilterIO(const KeyChain& c) : chain(c) {};

