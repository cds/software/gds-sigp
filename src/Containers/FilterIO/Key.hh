#ifndef Key_HH
#define Key_HH

#ifndef __CINT__
#include <time.h>
#include <stdexcept>
#else
namespace std
{
    class invalid_argument;
}
#endif

#include <string>

class Key
{
public:
    Key( void );

    // Constructor is the only way properties are set
    Key( const int& v, const std::string& d, const std::string& f );

    ~Key( );

    std::string describe( void ) const;

    bool operator<( const Key& ) const;
    bool operator>( const Key& ) const;
    bool operator==( const Key& ) const;
    bool operator!=( const Key& ) const;

    int value( void ) const;

private:
    std::string descr;
    std::string filter;
    int         val;
};

#endif // Key_HH
