#include "Key.hh"
#include <stdexcept>
#include <typeinfo>
#include <string>

Key::Key( void ) : descr( "" ), val( 0 ){ };

Key::Key( const int& v, const std::string& d, const std::string& f )
    : descr( d ), filter( f ), val( v )
{
}

Key::~Key( void ){ };

std::string
Key::describe( void ) const
{
    return descr;
}

bool
Key::operator<( const Key& k ) const
{
    bool tvalue;

    if ( typeid( k ) != typeid( *this ) )
        tvalue = false;
    else if ( k.filter != filter )
        tvalue = false;
    else
        tvalue = ( this->value( ) < k.value( ) );

    return tvalue;
}

bool
Key::operator>( const Key& k ) const
{
    return ( k < *this );
}

bool
Key::operator==( const Key& k ) const
{
    bool tvalue;
    if ( typeid( k ) != typeid( *this ) )
        tvalue = false;
    if ( k.filter != filter )
        tvalue = false;
    else
        tvalue = ( this->value( ) == k.value( ) );

    return tvalue;
}

bool
Key::operator!=( const Key& k ) const
{
    bool tvalue;

    if ( typeid( k ) != typeid( *this ) )
        tvalue = true;
    else if ( k.filter != filter )
        tvalue = false;
    else
        tvalue = ( this->value( ) != k.value( ) );

    return tvalue;
}

int
Key::value( void ) const
{
    return val;
}
