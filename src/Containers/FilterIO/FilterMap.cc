//
// FilterMap
//

// $Id: FilterMap.cxx 23 2001-09-28 23:15:07Z john.zweizig $
// $Log$
// Revision 1.1  1999/10/18 17:29:36  jzweizig
// New filter I/O container and associated classes
//

#include "FilterMap.hh"

FilterMap::FilterMap( const KeyChain& c ) : chain( c )
{
}

FilterMap::~FilterMap( void )
{
}

// For assignment
TSeries&
FilterMap::operator[]( const Key& key )
{
    return iomap[ key ];
}

// For assignment
const TSeries&
FilterMap::operator[]( const Key& key ) const
{
    return iomap.find( key )->second;
}

void
FilterMap::erase( const Key& key )
{
    iomap.erase( key );
}

void
FilterMap::clear( void )
{
    iomap.clear( );
}

// Inquiries

bool
FilterMap::isFull( void ) const
{
    return iomap.size( ) == chain.size( );
}

const KeyChain&
FilterMap::keyChain( void ) const
{
    return chain;
}
