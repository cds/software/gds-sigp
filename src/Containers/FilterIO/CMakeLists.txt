
add_library(FilterIO OBJECT
    FilterMap.cc
    Key.cc
    KeyChain.cc
)

target_include_directories(FilterIO PRIVATE
    ${CMAKE_SOURCE_DIR}/src/Containers/TSeries

    ${CMAKE_SOURCE_DIR}/src/Base/complex
    ${CMAKE_SOURCE_DIR}/src/Base/time
)

install(FILES
    FilterIO.hh
    FilterMap.hh
    Key.hh
    KeyChain.hh
    DESTINATION include/gds-sigp
)
