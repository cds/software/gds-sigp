#include "KeyChain.hh"
#include "Key.hh"

KeyChain::KeyChain( const std::list< Key >& keys ) : chain( keys )
{
}

int
KeyChain::size( void ) const
{
    return chain.size( );
}

bool
KeyChain::isEmpty( void ) const
{
    return chain.empty( );
}

// Element access

const Key&
KeyChain::front( void ) const
{
    return chain.front( );
}

const Key&
KeyChain::back( void ) const
{
    return chain.back( );
}

// Iterator functions

KeyChain::const_iterator
KeyChain::begin( void ) const
{
    return chain.begin( );
}

KeyChain::const_iterator
KeyChain::end( void ) const
{
    return chain.end( );
}

//  KeyChain::const_iterator KeyChain::rbegin (void) const
//  {return chain.rbegin(); }

//  KeyChain::const_iterator KeyChain::rend (void) const
//  { return chain.rend(); }
