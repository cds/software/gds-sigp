#ifndef KeyChain_HH
#define KeyChain_HH

#include <list>
#include "Key.hh"

// Key chain has a (not is a) list since a list is an implementation.

class KeyChain
{
public:
    // Typedefs

    // Provide an iterator for accessing in read-only mode
    typedef std::list< Key >::const_iterator const_iterator;

    // The constructor is the only way to set the keys in the KeyChain
    KeyChain( const std::list< Key >& keys );

    // Inquiries

    // Number of keys in chain
    int size( void ) const;

    // Are there any keys in chain?
    bool isEmpty( void ) const;

    // Element access

    // Reference to first key
    const Key& front( void ) const;

    // Reference to last key
    const Key& back( void ) const;

    // Iterator functions

    const_iterator begin( void ) const;

    const_iterator end( void ) const;

    //  const_iterator rbegin (void) const;

    //  const_iterator rend (void) const;

private:
    std::list< Key > chain;
};

#endif // KeyChain_HH
