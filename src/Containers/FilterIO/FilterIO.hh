#ifndef FilterIO_HH
#define FilterIO_HH

#include <stdexcept>

class TSeries;
class Key;

/// General container for TSeries.

class FilterIO
{
public:
    FilterIO( void )
    {
    }

    virtual ~FilterIO( )
    {
    }

    /// Add TSeries associated with Key k to the container
    virtual TSeries& operator[]( const Key& k ) = 0;

    /// Fetch TSeries associated with Key k from the container
    virtual const TSeries& operator[]( const Key& k ) const = 0;

    /// Delete TSeries associated with Key k from the container
    virtual void erase( const Key& ) = 0;

    /// Clear all the TSeries from the container
    virtual void clear( void ) = 0;

    // Inquiries

    /// Is there a TSeries for allowed Key?
    virtual bool isFull( void ) const = 0;

    /// Are there no TSeries in the container?
    virtual bool isEmpty( void ) const = 0;
};

#endif // FilterIO_HH
