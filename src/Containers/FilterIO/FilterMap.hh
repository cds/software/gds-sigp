#ifndef FilterMap_HH
#define FilterMap_HH

#include "FilterIO.hh"
#include "KeyChain.hh"
#include "TSeries.hh"
#include <map>

/// General container for TSeries.

class FilterMap : public FilterIO
{
public:
    FilterMap( const KeyChain& c );

    virtual ~FilterMap( );

    /// Add TSeries associated with Key k to the container
    virtual TSeries& operator[]( const Key& k );

    /// Fetch TSeries associated with Key k from the container
    virtual const TSeries& operator[]( const Key& k ) const;

    /// Delete TSeries associated with Key k from the container
    virtual void erase( const Key& );

    /// Clear all the TSeries from the container
    virtual void clear( void );

    // Inquiries

    /// Is there a TSeries for allowed Key?
    virtual bool isFull( void ) const;

    /// Are there no TSeries in the container?
    virtual bool isEmpty( void ) const;

    const KeyChain& keyChain( void ) const;

private:
    typedef std::map< Key, TSeries >   iomap_type;
    typedef iomap_type::const_iterator const_iomap_iter;
    typedef iomap_type::iterator       iomap_iter;

private:
    KeyChain   chain;
    iomap_type iomap;
};

//======================================  Inline methods

inline bool
FilterMap::isEmpty( void ) const
{
    return iomap.empty( );
}

#endif // FilterMap_HH
