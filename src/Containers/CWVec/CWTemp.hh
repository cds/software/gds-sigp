//
//   Copy on write vector class definition.
//
#ifndef CWTEMP_HH
#define CWTEMP_HH

#include <utility>

/**  The CWTemp template classes allow data in a specified container to be 
  *  shared by more than one Data vector without forcing a copy. This is 
  *  especially useful for reducing overheads when passing ownership of 
  *  long data vectors. A data vector can be copied using the assignment (=)
  *  operator, and the data container is referenced with the getRef method.
  *  @brief Copy on write data vector class template.
  *  @author J. Zweizig.
  *  @version 1.1; Modified February 23, 2000.
  */
template < class C >
class CWTemp
{
public:
    ///  General purpose data length type.
    typedef unsigned long size_type;

    ///  Underlying container type.
    typedef C container_type;

    ///  Node containing the use count and a data container.
    typedef std::pair< size_type, container_type > node_type;

    ///  Iterator borrowed from the container.
    typedef typename C::iterator iterator;

    ///  Constant iterator borrowed from container.
    typedef typename C::const_iterator const_iterator;

    ///  Data type borrowed from container.
    typedef typename C::value_type value_type;

    /**  Default constructor.
   */
    CWTemp( void );

    /**  Construct a CW vector.
    *  @brief Copy constructor.
    *  @param N Number of words to allocate.
    */
    explicit CWTemp( size_type N );

    /**  Copy constructor
    *  @brief Copy constructor.
    *  @param x Vector to be copied
    */
    CWTemp( const CWTemp& x );

    /**  Delete the vector.
    *  @brief Destructor.
    */
    ~CWTemp( void );

    /**  Get a read-only copy of the pointer.
    *  @brief Create a new read instance.
    *  @param x CW vetor with data to be copied..
    *  @return A read-Only pointer.
    */
    CWTemp& operator=( const CWTemp& x );

    /** Get the number of words allocated by the CW vector.
    * @brief Get the CW vector size.
    * @return Number of data word in the vector.
    */
    size_type getSize( void ) const;

    /** Get the number of words containing data.
    * @brief Get the CW vector size.
    * @return Number of data word in the vector.
    */
    size_type getLength( void ) const;

    /** Get an iterator to the first element.
    *  @brief Get constant iterator for start of container.
    *  @return Constant iterator for start of container.
   */
    const_iterator begin( void ) const;

    /**  Get an iterator to the first element.
    *  @brief Get iterator for start of container.
    *  @return Iterator for start of container.
    */
    iterator begin( void );

    /**  Get an iterator to the last element.
    *  @brief Get constant iterator for end of container.
    *  @return Constant iterator for end of container.
    */
    const_iterator end( void ) const;

    /**  Get an iterator to the last element.
    *  @brief Get iterator for end of container.
    *  @return Iterator for end of container.
    */
    iterator end( void );

    /**  Get a constant reference to the CW data.
    *  @brief Get a constant reference to the data vector.
    *  @return Constant reference to the container used to hold the data.
    */
    const container_type& getRef( void ) const;

    /**  Get a writable reference to the CW data.
    *  @brief Get a writable reference to the data vector.
    *  @return Reference to the container used to hold the data.
    */
    container_type& getRef( void );

    /**  Get a constant reference to a data word.
    *  @brief Get a constant reference to a data word.
    *  @param i Index of word to fetch.
    *  @return Constant reference to the specified data word.
    */
    const value_type& operator[]( size_type i ) const;

    /**  Get a writable reference to a data word.
    *  @brief Get a writable reference to a data word.
    *  @param i Index of word to fetch.
    *  @return Reference to the specified data word.
    */
    value_type& operator[]( size_type i );

private:
    /**  Get a pointer to a writeable copy of the vector. This function
    *  results in copying the vector if there is more than one user
    *  of this data.
    *  @brief Request a writeable copy of the data vector.
    *  @return Pointer to a writeable CWTemp with the same contents.
    */
    node_type* Access( void );

    /**  Replace a read instance. If the use count is zero the node is deleted.
    *  Otherwise, the use count is decremented, and the pointer is set to 
    *  the new node pointer. The new pointer (x) is assumed to have already 
    *  been reserved (with Use).
    *  @brief Release a node.
    */
    void Replace( node_type* x );

    /**  Get a pointer to a read-only copy of the container. The Use count
    *  of the node is incremented.
    *  @brief Get a read-only pointer.
    *  @return pointer to the allcated node.
    */
    node_type* Use( void ) const;

private:
    /**  Number of times the vector has been referenced.
    */
    node_type* mPtr;
};

//--------------------------------------  Constant access methods
template < class C >
inline const typename CWTemp< C >::container_type&
CWTemp< C >::getRef( void ) const
{
    return mPtr->second;
}

template < class C >
inline typename CWTemp< C >::const_iterator
CWTemp< C >::begin( ) const
{
    return getRef( ).begin( );
}

template < class C >
inline typename CWTemp< C >::const_iterator
CWTemp< C >::end( ) const
{
    return getRef( ).end( );
}

template < class C >
inline typename CWTemp< C >::node_type*
CWTemp< C >::Use( void ) const
{
    ( mPtr->first )++;
    return mPtr;
}

template < class C >
inline const typename CWTemp< C >::value_type&
CWTemp< C >::operator[]( size_type i ) const
{
    return getRef( )[ i ];
}

template < class C >
inline typename CWTemp< C >::size_type
CWTemp< C >::getSize( void ) const
{
    return getRef( ).capacity( );
}

template < class C >
inline typename CWTemp< C >::size_type
CWTemp< C >::getLength( void ) const
{
    return getRef( ).size( );
}

//======================================  Constructors and destructors
template < class C >
inline CWTemp< C >::CWTemp( void )
{
    mPtr = new node_type;
    mPtr->first = 0;
}

template < class C >
inline CWTemp< C >::CWTemp( size_type N )
{
    // This doesn't work: mPtr = new node_type(0, C(N));
    mPtr = new node_type;
    mPtr->first = 0;
    mPtr->second.insert( mPtr->second.end( ), N, value_type( ) );
}

template < class C >
inline CWTemp< C >::CWTemp( const CWTemp< C >& x )
{
    mPtr = x.Use( );
}

//======================================  Non-constant methods
template < class C >
inline void
CWTemp< C >::Replace( node_type* newptr )
{
    if ( !mPtr->first-- )
        delete mPtr;
    mPtr = newptr;
}

template < class C >
inline CWTemp< C >::~CWTemp( )
{
    Replace( 0 );
}

template < class C >
inline typename CWTemp< C >::node_type*
CWTemp< C >::Access( void )
{
    if ( mPtr->first )
        Replace( new node_type( 0, mPtr->second ) );
    return mPtr;
}

template < class C >
inline typename CWTemp< C >::container_type&
CWTemp< C >::getRef( void )
{
    Access( );
    return mPtr->second;
}

template < class C >
inline typename CWTemp< C >::iterator
CWTemp< C >::begin( )
{
    return getRef( ).begin( );
}

template < class C >
inline typename CWTemp< C >::iterator
CWTemp< C >::end( )
{
    return getRef( ).end( );
}

template < class C >
inline CWTemp< C >&
CWTemp< C >::operator=( const CWTemp& x )
{
    Replace( x.Use( ) );
    return *this;
}

template < class C >
inline typename CWTemp< C >::value_type&
CWTemp< C >::operator[]( size_type i )
{
    return getRef( )[ i ];
}

#endif
