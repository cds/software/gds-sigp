/* -*- mode: c++; c-basic-offset: 4; -*- */
//
//   Copy on write vector class definition.
//
#ifndef CWVECT_HH
#define CWVECT_HH

#include <cstring>
#include <cstdlib>
#include <stdexcept>
#include "gds_atomic.h"

#define CWVEC_STATS

#ifndef USE_BUILTIN_ATOMICS
#define PRE_INC( a ) ( ++a )
#define PRE_DEC( a ) ( --a )
#define POST_INC( a ) ( a++ )
#define POST_DEC( a ) ( a-- )
#else
#define PRE_INC( a ) __sync_add_and_fetch( &a, 1 )
#define PRE_DEC( a ) __sync_sub_and_fetch( &a, 1 )
#define POST_INC( a ) __sync_fetch_and_add( &a, 1 )
#define POST_DEC( a ) __sync_fetch_and_sub( &a, 1 )
#endif

#ifdef CWVEC_STATS
/**  @brief Copy-on-Write vector statistics container
  *
  *  Copy-on write vector statistics container. This class is used to count 
  *  the number of allocations, frees and deep copies of CWVec objects.
  *  and external structure is allocated at start-up time and the 
  *  statistics are printed out when it is deleted.
  *
  *  @author John G. Zweizig
  *  @version 1.1; Last modified December 4, 2007
  */
class CWStat_struct
{
public:
    /** Constructor
      */
    CWStat_struct( void );

    /** Destructor
      */
    ~CWStat_struct( void );

    /// Number of vectors allocated.
    long alloc_count;

    /// Number of vectors deleted.
    long delete_count;

    /// Number of shallow (pointer) copies performed.
    long copy_count;

    /// Number of deep (full data) copies performed.
    long deep_count;
};

extern CWStat_struct CWStats;
#endif

/**  The CWVec classes allow data to be shared by more than one Data vector 
  *  without forcing a copy. This is especially useful for reducing overhead 
  *  when passing ownership of long data vectors. A data vector can be copied 
  *  using the copy constructor and when write access is needed, access() is 
  *  used.
  *  @brief Copy on write data vector class template.
  */
template < class T >
class CWVec
{
public:
    /// Data type for CWVec word counts and indices
    typedef unsigned long size_type;

    /// CWVec vector data type
    typedef T value_type;

    /**  The vector data node contains a data vector, an ownership flag and a 
      *  use count. The use counter (_ref_count) is initialized to zero and 
      *  incremented each time an object "Uses" the data and decremented each 
      *  time an object relases the data. The vector may not be written or 
      *  deleted unless the use count is 0 (single user). The ownership flag 
      *  (_mine) is true if the data are owned by the vector.
      *  @brief Copy-on-write vector data node.
      */
    class vec_node
    {
    public:
        /**  Construct a vec_node object. If cpy is true, a vector of the 
	  *  specified length is allocated and the ownership flag is true.
	  *  If dval is non-zero the data are copied to the new vector. If
	  *  cpy is false, the ownership flag is reset and the "dval" pointer
	  *  is used as a vector address.
	  *  @param N    Length of data vector to be created.
	  *  @param dval initial value of the data vector.
	  *  @param cpy  Use specified const data if false.
	  *  @brief vec_node Constructor
	  */
        vec_node( size_type N, const T* dval = 0, bool cpy = true );

        /**  Destroy the %vec_node and delete the data vector if it is owned.
	  *  @brief %vec_node destructor.
	  */
        ~vec_node( void );

        /**  Return a pointer to the data vector.
	  *  @return Pointer to the first data word.
	  */
        T*
        ref( void )
        {
            return _data;
        }

        /**  Return a constant pointer to the data vector.
	  *  @return Constant pointer to the first data word.
	  */
        const T*
        ref( void ) const
        {
            return _data;
        }

        /**  Copy the specified data string from the vec_node argument (v) to 
	  *  the first N entries of the current data string.
	  *  @brief Copy data from another vec_node.
	  *  @param v vec_node containing the data to be copied.
	  *  @param inx Index of the first word in v to be copied.
	  *  @param N Number of words to be copied.
	  */
        void copy( const vec_node& v, size_type inx, size_type N );

        /**  The reference count is decremented to indicate that the vec_node
	  *  is no longed in use by the calling object.
	  *  @brief Release the vec_node.
	  *  @return true if the vec_node has been released by all objects.
	  */
        bool
        release( void ) const
        {
            return !POST_DEC( _ref_count );
        }

        /**  Get the data length of the vec_node array.
	  *  @brief Get the vector length.
	  *  @return Number of data words in the array.
	  */
        size_type
        size( void ) const
        {
            return _size;
        }

        /**  Test whether the vector is shared and therefore write protected. 
	  *  @brief test if the vec_node is shared.
	  *  @return True if the data vector is owned by the calling object.
	  */
        bool
        shared( void ) const
        {
            return _ref_count || !_mine;
        }

        /**  Reserve vector for use by another user.
	  *  @brief Increment the use count.
	  */
        void
        use( void ) const
        {
            PRE_INC( _ref_count );
        }

    private:
        mutable size_type _ref_count;
        bool              _mine;
        size_type         _size;
        T*                _data;
    };

    /**  Construct a data vector of Length L. If the data pointer is non-null,
    *  the indicated data are copied into the vector. Otherwise the vector
    *  is left uninitialized.
    *  @brief Basic constructor.
    *  @param L    Length of the vector to be created in data words.
    *  @param data Optional initial data values.
    *  @param cpy  Copy pointer instead of data if false.
    */
    explicit CWVec( size_type L = 0, const T* data = 0, bool cpy = true );

    /**  Constructor a new CWVec identical to the first argument, but 
    *  copy only the specified number of data words.
    *  @brief Limited copy constructor.
    *  @param x CWVec to be copied.
    */
    explicit CWVec( const CWVec& x );

    /**  Delete the vector.
    *  @brief Destructor.
    */
    ~CWVec( void );

    /**  Get a pointer to a writeable copy of the vector. This function
    *  results in copying the vector if there is more than one user
    *  of this data.
    *  @brief Request a writeable copy of the data vector.
    *  @return Pointer to a writeable CWVec with the same contents.
    */
    void access( void );

    /**  Get the capacity of the internal vector.
    *  @brief Internal vector capacity
    *  @return Vector capacity.
    */
    size_type capacity( void ) const;

    /**  Clear the vector by zeroing the data length. Note that the data vector 
    *  is released if the it is shared with another CWVec.
    *  @brief Zero the data length.
    */
    void clear( void );

    /**  The current vec_node is released and deleted if its use count falls to
    *  zero. The vec_node is address is then set to the specified address. 
    *  Note that the argument vec_node must have its instance count set 
    *  appropriately before it is passed to replace().
    *  @brief Replace the vector node.
    *  @param v pointer to the new vector node.
    */
    void replace( vec_node* v );

    /**  Release a read instance. Delete the vector if it is unused.
    *  @brief Release a read instance.
    */
    void release( void );

    /**  Get the number of words allocated by the CW vector.
    *  @brief Get the CW vector size.
    *  @return current length of the data vector.
    */
    size_type size( void ) const;

    /**  Get a constant reference to the CW data.
    *  @brief Get a constant reference to the data vector.
    *  @return Constant pointer to the start of the ddata vector.
    */
    const T* ref( void ) const;

    /**  Get a writable reference to the CW data.
    *  @brief Get a writable reference to the data vector.
    *  @return Pointer to the start of the data vector.
    */
    T* ref( void );

    /**  Insure that the specified number of data words are available in the
    *  data vector. If the requested data length is less than the current 
    *  data length, no action is taken. If the requested data length is 
    *  greater than the current length, a new vector is allocated and the 
    *  existing data are copied.
    *  @brief Insure that a vector of the specified size is available.
    *  @param N New length to allocate.
    */
    void reserve( size_type N );

    /**  Extend the vector to the specified length. Existing data are copied 
    *  if necessary, but added words are not initialized. If the requested 
    *  data length is less than the current length, no action is taken other
    *  than updating the current length. If the requested length is zero,
    *  the index is also set to zero. Resize does not request write access.
    *  @brief Shrink or extend the vector with uninitialized data.
    *  @param N New vector length.
    */
    void resize( size_type N );

    /**  Return true if internal data vector is shared with another CWVec.
    *  @brief Test whether vector is shared.
    *  @return True if the vector is in use by more than one data vector.
    */
    bool shared( void ) const;

    /**  Modify %CWVec limits to contain a substring of the current vector.
    *  If len is zero or inx+len is greater than the current string length, 
    *  the substring extends to the end of the current string.
    *  @brief take a substring of the current vector.
    *  @param inx Index of the first word of the substring.
    *  @param len Number of words in the substring.
    */
    void substr( size_type inx = 0, size_type len = 0 );

    /**  Perform a shallow copy (\e i.e. copy the pointer) of a constant 
    *  %CWVec of the same type. Note that the copy is safe for copies of 
    *  a CWVec to itself. 
    *  @brief Copy the contents of a %CWVec.
    *  @param v Vector to be copied.
    *  @return Reference to the current %CWVec.
    */
    CWVec& operator=( const CWVec& v );

    /**  Get a constant reference to a data word.
    *  @brief Get a constant reference to a data word.
    *  @param i Index of word to be accessed.
    *  @return Constant reference to data word.
    */
    const T& operator[]( size_type i ) const;

    /**  Get a writable reference to a data word.
    *  @brief Get a writable reference to a data word.
    *  @param i Index of word to be accessed.
    *  @return Reference to data word.
    */
    T& operator[]( size_type i );

private:
    /**  Number of data words used.
   */
    size_type mLength;

    /**  First data word used.
   */
    size_type mOffset;

    /**  Pointer to the data vector.
   */
    vec_node* mData;
};

//======================================  Inline implementation templates
template < class T >
inline CWVec< T >::vec_node::vec_node( size_type N, const T* dat, bool cpy )
    : _ref_count( 0 ), _mine( cpy ), _size( N ), _data( 0 )
{
    if ( _mine )
    {
        //_data = new T[N];
        if ( N * sizeof( T ) > 2000000000 )
            throw std::runtime_error( "aligned malloc >2GB" );
        if ( posix_memalign(
                 reinterpret_cast< void** >( &_data ), 128, N * sizeof( T ) ) )
        {
            throw std::runtime_error( "aligned malloc error" );
        }
        if ( dat )
        {
            memcpy( _data, dat, N * sizeof( T ) );
#ifdef CWVEC_STATS
            PRE_INC( CWStats.deep_count );
#endif
        }
    }
    else
    {
        _data = const_cast< T* >( dat );
#ifdef CWVEC_STATS
        PRE_INC( CWStats.copy_count );
#endif
    }
#ifdef CWVEC_STATS
    PRE_INC( CWStats.alloc_count );
#endif
}

template < class T >
inline CWVec< T >::vec_node::~vec_node( void )
{
    if ( _mine )
    {
        free( _data );
        //delete[] _data;
    }
#ifdef CWVEC_STATS
    PRE_INC( CWStats.delete_count );
#endif
}

template < class T >
inline void
CWVec< T >::vec_node::copy( const vec_node& v, size_type inx, size_type N )
{
    memmove( _data, v._data + inx, N * sizeof( T ) );
#ifdef CWVEC_STATS
    PRE_INC( CWStats.deep_count );
#endif
}

template < class T >
inline CWVec< T >::CWVec( size_type L, const T* dat, bool cpy ) : mOffset( 0 )
{
    mData = new vec_node( L, dat, cpy );
    mLength = L; // used to be mData->size()
}

template < class T >
inline void
CWVec< T >::substr( size_type inx, size_type len )
{
    if ( inx > mLength )
        inx = mLength;
    mOffset += inx;
    if ( !len || inx + len > mLength )
        mLength -= inx;
    else
        mLength = len;
}

template < class T >
inline CWVec< T >::CWVec( const CWVec< T >& x ) : mData( 0 )
{
    *this = x;
}

template < class T >
inline void
CWVec< T >::replace( vec_node* v )
{
    if ( mData && mData->release( ) )
        delete mData;
    mData = v;
}

template < class T >
inline void
CWVec< T >::release( void )
{
    replace( 0 );
}

template < class T >
inline CWVec< T >&
CWVec< T >::operator=( const CWVec& v )
{
    if ( v.mData )
        v.mData->use( );
    replace( v.mData );
#ifdef CWVEC_STATS
    PRE_INC( CWStats.copy_count );
#endif
    mLength = v.mLength;
    mOffset = v.mOffset;
    return *this;
}

template < class T >
inline CWVec< T >::~CWVec( )
{
    replace( 0 );
}

template < class T >
inline void
CWVec< T >::access( void )
{
    if ( mData->shared( ) )
    {
        replace( new vec_node( mLength, mData->ref( ) + mOffset ) );
        mOffset = 0;
    }
}

template < class T >
inline void
CWVec< T >::clear( void )
{
    mLength = 0;
    mOffset = 0;
    if ( shared( ) )
        replace( 0 );
}

template < class T >
inline typename CWVec< T >::size_type
CWVec< T >::capacity( void ) const
{
    if ( !mData )
        return 0;
    return mData->size( );
}

template < class T >
inline typename CWVec< T >::size_type
CWVec< T >::size( void ) const
{
    return mLength;
}

template < class T >
inline const T*
CWVec< T >::ref( void ) const
{
    return mData->ref( ) + mOffset;
}

template < class T >
inline T*
CWVec< T >::ref( void )
{
    access( );
    return mData->ref( ) + mOffset;
}

template < class T >
inline void
CWVec< T >::reserve( size_type N )
{
    if ( shared( ) || N > mData->size( ) )
    {
        size_type L = ( N < mLength ) ? N : mLength;
        vec_node* v = new vec_node( N );
        if ( L )
            v->copy( *mData, mOffset, L );
        replace( v );
        mOffset = 0;
    }
    else if ( mOffset + N > mData->size( ) )
    {
        mData->copy( *mData, mOffset, mLength );
        mOffset = 0;
    }
}

template < class T >
inline void
CWVec< T >::resize( size_type N )
{
    if ( !N )
    {
        clear( );
    }
    else
    {
        reserve( N );
        mLength = N;
    }
}

template < class T >
inline bool
CWVec< T >::shared( void ) const
{
    return !mData || mData->shared( );
}

template < class T >
inline const T&
CWVec< T >::operator[]( size_type i ) const
{
    return ref( )[ i ];
}

template < class T >
inline T&
CWVec< T >::operator[]( size_type i )
{
    return ref( )[ i ];
}
#endif
