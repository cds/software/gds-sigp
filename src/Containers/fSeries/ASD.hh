/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef CONTAINER_ASD_HH
#define CONTAINER_ASD_HH

#include "fSeries.hh"

namespace containers
{

    class PSD;
    class DFT;

    /**  The ASD class is based on the fSeries class and contains a 
      *  amplitude-spectral-density. The data are the positive 
      *  square-root of the power spectral density (PSD) class.
      *  @brief Amplitude Spectral Density container class.
      *  @author John G. Zweizig
      *  @version 1.0; Last modified June 16, 2009
      */
    class ASD : public fSeries
    {
    public:
        /**  The default ASD constructor creates an empty ASD object
	  *  with no allocated data vector and invalid frequency step.
	  *  @brief Default ASD constructor.
	  */
        ASD( void );

        /**  Construct a ASD from the square-root of a PSD. The
	  *  fSeries data are filled as described under SetData.
	  *  @brief Construct an ASD from a PSD.
	  *  @param fs PSD from which the ASD is derived.
	  */
        ASD( const PSD& fs );

        /**  Construct a ASD from the magnitude of a DFT. The
	  *  fSeries data are filled as described under SetData.
	  *  @brief Construct an ASD from a DFT.
	  *  @param fs DFT from which the ASD is derived.
	  */
        ASD( const DFT& fs );

        /**  Destroy an ASD and release any allocated storage.
	  *  @brief Destroy an ASD.
	  */
        ~ASD( void );

        /**  Create an identical copy of an ASD object.
	  *  @brief Clone an ASD
	  *  @return Pointer to the new ASD object.
	  */
        ASD* clone( void ) const;

        /**  Get the ASD series type (kASD).
	  *  @return The ASD type code (fSeries::kASD)
	  */
        fSeries::FSType getFSType( void ) const;
    };

    //==================================  Inline methods
    inline fSeries::FSType
    ASD::getFSType( void ) const
    {
        return fSeries::kASD;
    }

} // namespace containers

#endif // !defined(CONTAINER_ASD_HH)
