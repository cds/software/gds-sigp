/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "PSD.hh"
#include "DFT.hh"
#include "DVecType.hh"
#include "lcl_array.hh"

using namespace containers;

//======================================  PSD default constructor
PSD::PSD( void )
{
}

//======================================  PSD data constructor
//
//  Note on normalization:
//  The conventions document (LIGO-T010095) says that PSD should be
//       PSD(f) = Delta/(N*sigma_w^2) (||h~(f)|| + ||h~(-f)||)
//   and
//       DFT(f) = (Delta/sigma_w) h~(f)
//   where Delta is the time series step size, N is the length of the time
//   series and sigma_w is the window RMS. The PSD is therefore:
//       PSD(f) = 1/(Delta*N) (||DFT(f)|| + ||DFT(-f)||)
//   but
//       1/(Delta*N) = dF
//
PSD::PSD( const DFT& fs )
{
    fSeries::operator=( fs.modsq( ) );
    *this *= getFStep( );
    size_type N = fs.size( );
    if ( fs.single_sided( ) && N > 2 )
        refDVect( ).scale( 1, 2.0, N - 2 );
}

//======================================  PSD fSeries constructor
PSD::PSD( const fSeries& fs ) : fSeries( fs )
{
}

//======================================  PSD destructor
PSD::~PSD( void )
{
}

//======================================  Clone PSD
PSD*
PSD::clone( void ) const
{
    return new PSD( *this );
}

//======================================  Extract a sub-string
PSD
PSD::extract_psd( freq_type f0, freq_type dF ) const
{
    return PSD( fSeries::extract( f0, dF ) );
}

//======================================  Fold PSD
void
PSD::fold( void )
{
    if ( double_sided( ) )
    {
        size_type N = ( size( ) - 1 ) / 2;
        if ( complex( ) )
        {
            lcl_array< dComplex > c( N );
            refDVect( ).getData( 1, N, c );
            DVecType< dComplex > dv( N );
            dv.reverse( 0, c, N );
            fSeries::fold( );
            refDVect( ).add( 1, dv, 0, N );
        }
        else
        {
            lcl_array< double > d( N );
            refDVect( ).getData( 1, N, d );
            DVecType< double > dv( N );
            dv.reverse( 0, d, N );
            fSeries::fold( );
            refDVect( ).add( 1, dv, 0, N );
        }
    }
}
