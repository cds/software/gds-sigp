/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef CONTAINER_PSD_HH
#define CONTAINER_PSD_HH

#include "fSeries.hh"

namespace containers
{

    class DFT;

    /**  The PSD class is based on the fSeries class and contains a 
      *  power-spectral-density. The data are normalized as specified in 
      *  "Conventions for data and software products of LIGO and the LSC" 
      *  (<a href="http://www.ligo.caltech.edu/docs/T/T010095-00.pdf">
      *  LIGO-T010095-00-Z</a>).
      *  \note The normalizaton of PSDs and DFTs as defined by the 
      *        conventions document is:
      *  \f[ PSD(f) = \frac{\Delta}{N \sigma_{w}^2 } 
                      ( |\tilde{h}(f) |^2 + | \tilde{h}(-f) |^2) \f]
      *  and the DFT frequency series is 
      *  \f[ DFT(f) = \frac{\Delta}{\sigma_{w} } \tilde{h}(f) \f]
      *  where \f$ \Delta \f$ is the time series time step, N is the
      *  length of the time series (in samples) and \f$ \sigma_w \f$ is the 
      *  window RMS. For symmetric (single_sided) series and for frequency 
      *  bins \f$ 0 < f < f_{NY} \f$, \f$ \tilde{h}(-f) = \tilde{h}^*(f) \f$
      *  and the PSD becomes:
      *  \f[ PSD(f) = \frac{2 \Delta}{N \sigma_{w}^2 } |\tilde{h}(f) |^2 \f]
      *  The PSD is calculated from the DFT as:
      *  \f[ PSD(f) = \frac{1}{N \Delta} (|DFT(f)|^2 + |DFT(-f)|^2 ) \f]
      *  where \f$ \frac{1}{N \Delta} = dF \f$ is the frequency step. 
      *
      *  \brief Power Spectral Density container class.
      *  \author John G. Zweizig
      *  \version $Id$
      */
    class PSD : public fSeries
    {
    public:
        /**  The default PSD constructor creates an empty PSD object
	  *  with no allocated data vector and invalid frequency step.
	  *  @brief Default PSD constructor.
	  */
        PSD( void );

        /**  Construct a PSD from the square of a DFT. The
	  *  fSeries data are filled as described under SetData.
	  *  @brief Construct a PSD from a DFT.
	  *  @param fs DFT from which the PSD is derived.
	  */
        PSD( const DFT& fs );

        /**  Destroy a PSD and release any allocated storage.
	  *  @brief Destroy a PSD.
	  */
        ~PSD( void );

        /**  Create an identical copy of a PSD object.
	  *  @brief Clone a PSD
	  *  @return A pointer to the new PSD object.
	  */
        PSD* clone( void ) const;

        /**  Returns an fSeries containing a subset of the parent fSeries.
	  *  @brief  Get a substring of the fSeries.
	  *  @param f0    Lowest frequency to be extracted from the PSD (in Hz)
	  *  @param dF    Frequency interval to be extracted (in Hz) 
	  *  @return The specified sub-series
	  */
        PSD extract_psd( freq_type f0, freq_type dF ) const;

        /**  Get the DFT series type (kPSD).
	  *  @return The DFT type code (fSeries::kPSD)
	  */
        fSeries::FSType getFSType( void ) const;

        /**  Fold the PSD at \c f=0.
	  */
        void fold( void );

    private:
        /**  Construct a PSD from the square of a DFT. The
	  *  fSeries data are filled as described under SetData.
	  *  @brief Construct a PSD from a DFT.
	  *  @param fs fSeries from which the PSD is derived.
	  */
        PSD( const fSeries& fs );
    };

    //==================================  Inline methods
    inline fSeries::FSType
    PSD::getFSType( void ) const
    {
        return fSeries::kPSD;
    }

} // namespace containers

#endif // !defined(CONTAINER_PSD_HH)
