/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef CONTAINER_DFT_HH
#define CONTAINER_DFT_HH

#include "fSeries.hh"
class TSeries;

namespace containers
{

    /**  The DFT class is derived from an fSeries class and contains the
      *  discrete Fourier transform of a time series. The data are normalized 
      *  as specified in "Conventions for data and software products of 
      *  LIGO and the LSC" 
      *  (<a href="http://www.ligo.caltech.edu/docs/T/T010095-00.pdf">
      *  LIGO-T010095-00-Z</a>) \e i.e. it is multiplied by the time step 
      *  divided by the window r.m.s. (both values are store in the 
      *  Time series.
      *  - v1.0: Original version
      *  - v1.1: Add iFFT() method, fix full series frequency reordering, 
      *          make all DFTs dComplex.
      *  @brief Discrete Fourier Transform class.
      *  @author John G. Zweizig
      *  @version 1.1; Last modified April 7, 2008
      */
    class DFT : public fSeries
    {
    public:
        /**  The default DFT constructor creates an empty DFT object
	  *  with no allocated data vector and invalid frequency step.
	  *  @brief Default DFT constructor.
	  */
        DFT( void );

        /**  Construct a DFT from the fourier transform of a TSeries. The
	  *  fSeries data are filled as described under SetData.
	  *  @brief Construct a DFT from a TSeries.
	  *  @param ts Time series to be fourier transformed.
	  */
        DFT( const TSeries& ts );

        /**  Destroy a DFT and release any allocated storage.
	  *  @brief Destroy a DFT.
	  */
        ~DFT( void ) = default;

        /**  Create an identical copy of a DFT object.
	  *  @brief Clone a DFT
	  *  @return A pointer to the new DFT object.
	  */
        DFT* clone( void ) const;

        /**  Return a DFT that has been shifted in time by the specified
	  *  interval (dt). Each bin of the returned DFT (y[j]) is calculated 
	  *  from original (x[j]) by <tt>y[j] = x[j] * exp(-i*f[j]*dt)</tt>.
	  *  @brief evolve the DFT in time.
	  *  @param dt Time interval to evolve the DFT.
	  *  @return DFT evolved as described above.
	  */
        DFT evolve( Interval dt ) const;

        /**  Return a DFT extracted from this.
	  *  @brief Extract a DFT.
	  *  @param f0 Start frequency of extracted DFT.
	  *  @param dF Frequency bandwidth of extracted series.
	  *  @return DFT extracted as described above.
	  */
        DFT extract_dft( freq_type f0, freq_type dF ) const;

        /**  Get the DFT series type (kDFT).
	  *  @return The DFT type code (fSeries::kDFT)
	  */
        fSeries::FSType getFSType( void ) const;

        /**  Use an inverse FFT to calculate the a time series.
	  *  @brief Transform the DFT to a time series.
	  *  @return Time series calculated from the DFT.
	  */
        TSeries iFFT( void ) const;

        /**  Use an inverse FFT to calculate a time series.
	  *  @brief Transform the DFT to a time series.
	  *  @param out Time series calculated from the DFT.
	  */
        void iFFT( TSeries& out ) const;

        /** Return complex conjugate of a DFT.
	 *  \brief Complex conjugate.
	 *  \param dft Discrete Fourier transform object to be conjugated
	 *  \return Conjugated series of argument DFT
	 */
        DFT operator~( void ) const;

        using fSeries::setData;

        /**  Set the fSeries data from a discrete Fourier transform of the
	  *  specified time series. A double-sided complex fSeries is 
	  *  produced with minimum frequency of <tt>-1/dT</tt> and frequency 
	  *  step of <tt>1/(N*dT)</tt> where <tt>dT</tt> is the time increment
	  *  (inverse of the sample rate) and <tt>N</tt> is the number of
	  *  samples in the input time series.
	  *  @brief Set the DFT data from the fourier transform of a TSeries.
	  *  @param ts TSeries to be transformed.
	  */
        void setData( const TSeries& ts );

        /**  The length of the time series used to create this DFT. This 
	  *  number is needed for normalization of e.g. PSDs and iFFTs. For
	  *  a complex (double_sided) DFT, the original time series length 
	  *  is the same as the length of the DFT. For a real DFT, the length
	  *  is inferred from the time-step and frequency step 
	  *  (\f$ N = (\delta t \delta f)^{-1} \f$), or, if the original 
	  *  time-step is not recorded, from the fSeries length (assumes
	  *  an even series length, i.e. \c N=2*size()-2 ).
	  *  @brief Time series length.
	  *  @return Length of original time series.
	  */
        size_type series_length( void ) const;
    };

    //======================================  Inline methods
    inline fSeries::FSType
    DFT::getFSType( void ) const
    {
        return fSeries::kDFT;
    }

} //  namespace containers

#endif // !defined(CONTAINER_DFT_HH)
