/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "ASD.hh"
#include "PSD.hh"
#include "DFT.hh"
#include "DVecType.hh"
#include "lcl_array.hh"
#include <cmath>

using namespace containers;

//======================================  ASD default constructor
ASD::ASD( void )
{
}

//======================================  ASD data constructor
ASD::ASD( const PSD& fs )
{
    if ( fs.empty( ) )
        return;
    size_type N = fs.size( );
    if ( fs.refDVect( ).D_data( ) )
    {
        fSeries::operator=( fs );
        double*  p = dynamic_cast< DVectD& >( refDVect( ) ).refTData( );
        for ( size_type i = 0; i < N; ++i )
            p[ i ] = sqrt( p[ i ] );
    }
    else if ( fs.refDVect( ).F_data( ) )
    {
        fSeries::operator=( fs );
        float*   p = dynamic_cast< DVectF& >( refDVect( ) ).refTData( );
        for ( size_type i = 0; i < N; ++i )
            p[ i ] = sqrt( p[ i ] );
    }
    else
    {
        DVectD  dv( fs.refDVect( ) );
        double* p = dv.refTData( );
        for ( size_type i = 0; i < N; ++i )
            p[ i ] = sqrt( p[ i ] );
        setTimeSpan( fs.getStartTime( ), fs.getDt( ) );
        setData( fs.getLowFreq( ), fs.getFStep( ), dv );
        setDSMode( );
    }
}

//======================================  DFT data constructor
ASD::ASD( const DFT& fs )
{
    fSeries:: operator=( fs.modulus( ) );
    size_type N = size( );
    if ( single_sided( ) && N > 2 )
        refDVect( ).scale( 1, sqrt( 2.0 ), N - 2 );
}

//======================================  ASD destructor
ASD::~ASD( void )
{
}

//======================================  Clone ASD
ASD*
ASD::clone( void ) const
{
    return new ASD( *this );
}
