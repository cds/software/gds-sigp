/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "fSeries.hh"
#include "DVecType.hh"
#include <stdexcept>
#include <iostream>
#include "lcl_array.hh"
#include "dv_function.hh"

using namespace std;
using namespace containers;

//======================================  Constructor
fSeries::fSeries( void )
    : mF0( 0 ), mDf( 0 ), mT0( 0 ), mDt( 0 ), mDelta( 0 ), mDSMode( kEmpty )
{
}

//======================================  Copy constructor
fSeries::fSeries( const fSeries& x )
{
    *this = x;
}

//======================================  Data constructor
fSeries::fSeries(
    freq_type f0, freq_type dF, const Time& t0, Interval dT, const DVector& dV )
    : mF0( f0 ), mDf( dF ), mT0( t0 ), mDt( dT ), mDelta( 0 ),
      mData( dV.clone( ) )
{
    setDSMode( );
}

//======================================  Constructor
fSeries::fSeries(
    freq_type f0, freq_type dF, const Time& t0, Interval dT, DVector* dV )
    : mF0( f0 ), mDf( dF ), mT0( t0 ), mDt( dT ), mDelta( 0 ), mData( dV )
{
    setDSMode( );
}

//======================================  Clone an fSeries
void
fSeries::appName( const string& txt )
{
    mName += txt;
}

//======================================  Clone an fSeries
fSeries*
fSeries::clone( void ) const
{
    return new fSeries( *this );
}

//======================================  Extend an fSeries
void
fSeries::extend( freq_type fmax )
{
    if ( mDf <= 0 || null( ) )
        throw runtime_error( "fSeries: Attempted to extend empty series" );
    if ( fmax >= mF0 )
    {
        size_type nNew = size_type( ( fmax - mF0 ) / mDf + 0.5 );
        if ( single_sided( ) )
            nNew++;
        size_type end = size( );
        if ( nNew > end )
            mData->replace_with_zeros( end, 0, nNew - end );
    }
    else if ( double_sided( ) )
    {
        size_type nNew = size_type( ( mF0 - fmax ) / mDf + 0.5 );
        if ( nNew )
        {
            mData->replace_with_zeros( 0, 0, nNew );
            mF0 -= double( nNew ) * mDf;
        }
    }
    mDelta = 0;
}

//======================================  Extract a subseries.
fSeries
fSeries::extract( freq_type f0, freq_type dF ) const
{
    freq_type fmax = f0 + dF;
    if ( f0 < mF0 )
        f0 = mF0;
    size_type inx0 = getBin( f0 );
    size_type inx1 = getBin( fmax );
    if ( single_sided( ) )
        inx1++;
    if ( empty( ) || inx1 <= inx0 )
        return fSeries( f0, mDf, mT0, mDt );
    size_type len = inx1 - inx0;
    fSeries   r( getBinF( inx0 ), mDf, mT0, mDt, mData->Extract( inx0, len ) );
    r.setSampleTime( mDelta );

    //----------------------------------  Force fNy point to be real.
    if ( single_sided( ) )
    {
        if ( mData->W_data( ) )
        {
            dynamic_cast< DVectW& >( *( r.mData ) )[ len - 1 ] =
                mData->getDouble( inx1 - 1 );
        }
        else if ( mData->C_data( ) )
        {
            dynamic_cast< DVectC& >( *( r.mData ) )[ len - 1 ] =
                mData->getFloat( inx1 - 1 );
        }
    }
    return r;
}

//======================================  get FSType
fSeries::FSType
fSeries::getFSType( void ) const
{
    return kSeries;
}

//======================================  Resample series
fSeries
fSeries::interpolate( freq_type fmin,
                      freq_type fmax,
                      freq_type df,
                      bool      l ) const
{
    if ( l )
        return interpolate( fmin, fmax, df, kLog );
    else
        return interpolate( fmin, fmax, df, kLinear );
}

//======================================  Resample series
fSeries
fSeries::interpolate( freq_type fmin,
                      freq_type fmax,
                      freq_type df,
                      FSinterp  ityp ) const
{

    //----------------------------------  Set the return series.
    fSeries r( fmin, df, mT0, mDt );
    if ( empty( ) || fmax < fmin )
        return r;

    //----------------------------------  See if extract will work...
    size_type inc = size_type( df / mDf + 0.5 );
    if ( double( inc ) * mDf == df && mF0 == 0 )
    {
        size_type iMax = getBin( fmax ) + inc;
        if ( inc == 1 )
            r.mData.reset( mData->Extract( 0, iMax ) );
        else
            r.mData.reset( mData->Extract( 0, iMax / inc, inc ) );
        size_type iMin = r.getBin( fmin );
        if ( iMin )
            r.mData->sub( 0, *r.mData, 0, iMin );
        r.mDSMode = kFolded;

        //----------------------------------  Do the full interpolation.
    }
    else
    {
        size_type nBin = size_type( ( fmax - fmin ) / df + 1e-4 ) + 1;

        //------------------------------  Get the input series packing
        size_type nF = getNStep( );
        freq_type fHi = getHighFreq( );

        //------------------------------  Interpolate (slowly but surely).
        if ( refDVect( ).C_data( ) )
        {
            DVecType< dComplex > dv( nBin );
            const fComplex*      p =
                dynamic_cast< const DVectC& >( *mData ).refTData( );

            switch ( ityp )
            {
            case kLinear:
                for ( size_type i = 0; i < nBin; ++i )
                {
                    freq_type f = fmin + df * double( i );
                    if ( f < mF0 || f > fHi )
                    {
                        dv[ i ] = dComplex( 0 );
                    }
                    else
                    {
                        size_type inx = size_type( ( f - mF0 ) / mDf );
                        freq_type f0 = getBinF( inx );
                        if ( inx == nF )
                        {
                            inx--;
                            f0 -= mDf;
                        }
                        double a = ( f - f0 ) / mDf;
                        dv[ i ] = p[ inx ] * ( 1.0 - a ) + p[ inx + 1 ] * a;
                    }
                }
                break;
            case kLog:
                for ( size_type i = 0; i < nBin; ++i )
                {
                    freq_type f = fmin + df * double( i );
                    if ( f < mF0 || f > fHi )
                    {
                        dv[ i ] = dComplex( 0 );
                    }
                    else
                    {
                        size_type inx = size_type( ( f - mF0 ) / mDf );
                        freq_type f0 = getBinF( inx );
                        if ( inx == nF )
                        {
                            inx--;
                            f0 -= mDf;
                        }
                        double a = ( f - f0 ) / mDf;
                        dv[ i ] = exp( log( p[ inx ] ) * ( 1 - a ) +
                                       log( p[ inx + 1 ] ) * a );
                    }
                }
                break;
            default:
                throw runtime_error( "interpolation method not implemented" );
            }
            r.setData( fmin, df, dv );
        }
        else if ( refDVect( ).W_data( ) )
        {
            DVecType< dComplex > dv( nBin );
            const dComplex*      p =
                dynamic_cast< const DVectW& >( *mData ).refTData( );
            switch ( ityp )
            {
            case kLinear:
                for ( size_type i = 0; i < nBin; ++i )
                {
                    freq_type f = fmin + df * double( i );
                    if ( f < mF0 || f > fHi )
                    {
                        dv[ i ] = dComplex( 0 );
                    }
                    else
                    {
                        size_type inx = size_type( ( f - mF0 ) / mDf );
                        freq_type f0 = getBinF( inx );
                        if ( inx == nF )
                        {
                            inx--;
                            f0 -= mDf;
                        }
                        double a = ( f - f0 ) / mDf;
                        dv[ i ] = p[ inx ] * ( 1.0 - a ) + p[ inx + 1 ] * a;
                    }
                }
                break;
            case kLog:
                for ( size_type i = 0; i < nBin; ++i )
                {
                    freq_type f = fmin + df * double( i );
                    if ( f < mF0 || f > fHi )
                    {
                        dv[ i ] = dComplex( 0 );
                    }
                    else
                    {
                        size_type inx = size_type( ( f - mF0 ) / mDf );
                        freq_type f0 = getBinF( inx );
                        if ( inx == nF )
                        {
                            inx--;
                            f0 -= mDf;
                        }
                        double a = ( f - f0 ) / mDf;
                        dv[ i ] = exp( log( p[ inx ] ) * ( 1 - a ) +
                                       log( p[ inx + 1 ] ) * a );
                    }
                }
                break;
            default:
                throw runtime_error( "interpolation method not implemented" );
            }
            r.setData( fmin, df, dv );
        }
        else if ( refDVect( ).D_data( ) )
        {
            DVectD        dv( nBin );
            double*       dvp = dv.refTData( );
            const double* p =
                dynamic_cast< const DVectD& >( *mData ).refTData( );
            switch ( ityp )
            {
            case kLinear:
                for ( size_type i = 0; i < nBin; ++i )
                {
                    freq_type f = fmin + df * double( i );
                    if ( f < mF0 || f > fHi )
                    {
                        dvp[ i ] = 0.0;
                    }
                    else
                    {
                        size_type inx = size_type( ( f - mF0 ) / mDf );
                        freq_type f0 = getBinF( inx );
                        if ( inx == nF )
                        {
                            inx--;
                            f0 -= mDf;
                        }
                        double a = ( f - f0 ) / mDf;
                        dvp[ i ] = p[ inx ] * ( 1.0 - a ) + p[ inx + 1 ] * a;
                    }
                }
                break;
            case kLog:
                for ( size_type i = 0; i < nBin; ++i )
                {
                    freq_type f = fmin + df * double( i );
                    if ( f < mF0 || f > fHi )
                    {
                        dvp[ i ] = 0.0;
                    }
                    else
                    {
                        size_type inx = size_type( ( f - mF0 ) / mDf );
                        freq_type f0 = getBinF( inx );
                        if ( inx == nF )
                        {
                            inx--;
                            f0 -= mDf;
                        }
                        double a = ( f - f0 ) / mDf;
                        dvp[ i ] = exp( log( p[ inx ] ) * ( 1 - a ) +
                                        log( p[ inx + 1 ] ) * a );
                    }
                }
                break;
            default:
                throw runtime_error( "interpolation method not implemented" );
            }
            r.setData( fmin, df, dv );
        }
        else
        {
            DVectD              dv( nBin );
            lcl_array< double > p( nF );
            getData( nF, p.get( ) );
            switch ( ityp )
            {
            case kLinear:
                for ( size_type i = 0; i < nBin; ++i )
                {
                    freq_type f = fmin + df * double( i );
                    if ( f < mF0 || f > fHi )
                    {
                        dv[ i ] = 0.0;
                    }
                    else
                    {
                        size_type inx = size_type( ( f - mF0 ) / mDf );
                        freq_type f0 = getBinF( inx );
                        if ( inx == nF )
                        {
                            inx--;
                            f0 -= mDf;
                        }
                        double a = ( f - f0 ) / mDf;
                        dv[ i ] = p[ inx ] * ( 1.0 - a ) + p[ inx + 1 ] * a;
                    }
                }
                break;
            case kLog:
                for ( size_type i = 0; i < nBin; ++i )
                {
                    freq_type f = fmin + df * double( i );
                    if ( f < mF0 || f > fHi )
                    {
                        dv[ i ] = 0.0;
                    }
                    else
                    {
                        size_type inx = size_type( ( f - mF0 ) / mDf );
                        freq_type f0 = getBinF( inx );
                        if ( inx == nF )
                        {
                            inx--;
                            f0 -= mDf;
                        }
                        double a = ( f - f0 ) / mDf;
                        dv[ i ] = exp( log( p[ inx ] ) * ( 1 - a ) +
                                       log( p[ inx + 1 ] ) * a );
                    }
                }
                break;
            default:
                throw runtime_error( "interpolation method not implemented" );
            }
            r.setData( fmin, df, dv );
        }
    }
    r.setSampleTime( mDelta );
    return r;
}

//======================================  Calculate complex argument
fSeries
fSeries::argument( void ) const
{
    //----------------------------------  Get DVectD with the argument vs. f
    DVectD dv( dv_argument( refDVect( ) ) );

    //----------------------------------  Construct an fSeries.
    fSeries r( mF0, mDf, mT0, mDt, dv );
    r.setSampleTime( mDelta );
    if ( !mName.empty( ) )
    {
        r.mName = "argument(";
        r.mName += mName;
        r.mName += ")";
    }
    return r;
}

//======================================  Calculate modulus-squared
fSeries
fSeries::modsq( void ) const
{
    if ( empty( ) )
        return fSeries( );
    DVectD  dv( dv_modsq( refDVect( ) ) );
    fSeries r( mF0, mDf, mT0, mDt, dv );
    r.setSampleTime( mDelta );
    if ( !mName.empty( ) )
    {
        r.mName = "modsq(";
        r.mName += mName;
        r.mName += ")";
    }
    return r;
}

//======================================  Calculate modulus
fSeries
fSeries::modulus( void ) const
{
    DVectD    dv( dv_modsq( refDVect( ) ) );
    size_type N = size( );
    for ( size_type i = 0; i < N; ++i )
    {
        dv[ i ] = sqrt( dv[ i ] );
    }

    fSeries r( mF0, mDf, mT0, mDt, dv );
    r.setSampleTime( mDelta );
    if ( !mName.empty( ) )
    {
        r.mName = "modulus(";
        r.mName += mName;
        r.mName += ")";
    }
    return r;
}

//======================================  Print out the fSeries header
std::ostream&
fSeries::dump_header( std::ostream& out ) const
{
    if ( !this )
    {
        out << "fSeries::dump_header: Request is dereferened null pointer"
            << endl;
        return out;
    }
    out << "fSeries " << mName << ": Start time " << getStartTime( )
        << " End Time " << getEndTime( ) << endl;
    out << "Series type: ";
    switch ( getFSType( ) )
    {
    case kSeries:
        out << "fSeries";
        break;
    case kDFT:
        out << "DFT";
        break;
    case kPSD:
        out << "PSD";
        break;
    case kCSD:
        out << "CSD";
        break;
    case kASD:
        out << "ASD";
        break;
    }
    out << "  Storage type: ";
    switch ( mDSMode )
    {
    case kEmpty:
        out << "None" << endl;
        break;
    case kFolded:
        out << "Folded" << endl;
        break;
    case kFoldedOdd:
        out << "Folded (odd length)" << endl;
        break;
    case kFull:
        out << "Full" << endl;
        break;
    case kFullReal:
        out << "Full (real dft)" << endl;
        break;
    }
    out << "Low Frequency " << getLowFreq( ) << " High Frequency "
        << getHighFreq( ) << " No. frequency steps " << getNStep( ) << endl;
    out << "Time Series start: " << mT0 << " Interval: " << mDt
        << " Step: " << mDelta << endl;
    return out;
}

//======================================  Print out the fSeries contents
std::ostream&
fSeries::Dump( std::ostream& out ) const
{
    dump_header( out );
    if ( !this || null( ) )
        return out;
    else
        return mData->Dump( out );
}

//======================================  Erase all data from series
void
fSeries::clear( void )
{
    if ( !null( ) )
        mData->Clear( );
    mDSMode = kEmpty;
}

//======================================  Remove negative frequencies
void
fSeries::fold( void )
{
    if ( empty( ) || single_sided( ) )
        return;
    size_type N = size( );
    size_type ibin0 = N / 2;
    mF0 = getBinF( ibin0 );
    unique_ptr< DVector > save( mData.release( ) );
    mData.reset( save->Extract( ibin0, N - ibin0 ) );
    if ( 2 * ibin0 == N )
    {
        unique_ptr< DVector > temp( save->Extract( 0, 1 ) );
        temp->Conjugate( );
        mData->Append( *temp );
        mDSMode = kFolded;
    }
    else
    {
        mDSMode = kFoldedOdd;
    }
}

//======================================  Replace negative frequencies
void
fSeries::unfold( void )
{
    if ( empty( ) || double_sided( ) )
        return;
    size_type N = size( );
    long      nTot = ( mDSMode == kFoldedOdd ) ? 2 * N - 1 : 2 * ( N - 1 );
    //if (mDelta != Interval(0.0)) nTot = long(1.0 / (mDelta * mDf) + 0.5);
    //else                         nTot = 2 * (N - 1);
    unique_ptr< DVector > negs( mData->clone( ) );
    negs->reverse( );
    negs->Conjugate( );
    // note tricky code...
    // the zero bin is the Nth bin of the folded series.
    if ( ( nTot % 2 ) == 0 )
        negs->replace( N - 1, 1, *mData, 0, N - 1 );
    else
        negs->replace( N - 1, 1, *mData, 0, N );
    mData.swap( negs );
    mF0 -= mDf * ( N - 1 );
    mDSMode = kFullReal;
}

//======================================  Set data from DVector
void
fSeries::setData( freq_type f0, freq_type df, const DVector& data )
{
    setData( f0, df, data.clone( ) );
}

//======================================  setData
void
fSeries::setData( freq_type f0, freq_type df, DVector* data )
{
    mF0 = f0;
    mDf = df;
    if ( mData.get( ) != data )
        mData.reset( data );
    setDSMode( );
}

//======================================  Set the data storage mode
void
fSeries::setDSMode( void )
{
    if ( mDf <= 0.0 )
        throw runtime_error( "fSeries: Invalid frequency step." );
    if ( !size( ) )
        mDSMode = kEmpty;
    else if ( mF0 < 0 )
        mDSMode = kFull;
    else
        mDSMode = kFolded;
}

//======================================  Set the name string
void
fSeries::setName( const std::string& name )
{
    mName = name;
}

//======================================  Set the source sample time
void
fSeries::setSampleTime( Interval dT )
{
    mDelta = dT;
}

//======================================  Set the time span
void
fSeries::setTimeSpan( const Time& t0, Interval dT )
{
    mT0 = t0;
    mDt = dT;
}

//======================================  Reserve space in the data vector
void
fSeries::reserve( size_type len )
{
    if ( !null( ) )
        refDVect( ).reserve( len );
}

//======================================  Assignment operators.
fSeries&
fSeries::operator=( const fSeries& x )
{
    mName = x.mName;
    mF0 = x.mF0;
    mDf = x.mDf;
    mT0 = x.mT0;
    mDt = x.mDt;
    mDSMode = x.mDSMode;
    mDelta = x.mDelta;
    if ( !x.empty( ) )
        mData.reset( x.refDVect( ).clone( ) );
    else
        mData.reset( 0 );
    return *this;
}

//======================================  Bias operator
fSeries&
fSeries::operator+=( double bias )
{
    if ( !empty( ) )
        refDVect( ) += bias;
    return *this;
}

//======================================  Addition operator
fSeries&
fSeries::operator+=( const fSeries& rhs )
{
    if ( empty( ) || size( ) != rhs.size( ) || mDSMode != rhs.mDSMode )
    {
        cerr << "fSeries sizes: this=" << size( ) << " that=" << rhs.size( )
             << endl;
        throw runtime_error( "fSeries: Add an unequal length fSeries" );
    }
    refDVect( ) += rhs.refDVect( );
    return *this;
}

//======================================  Subtract operator
fSeries&
fSeries::operator-=( const fSeries& rhs )
{
    if ( empty( ) || size( ) != rhs.size( ) || mDSMode != rhs.mDSMode )
        throw runtime_error( "fSeries: Subtract an unequal length fSeries" );
    refDVect( ) -= rhs.refDVect( );
    return *this;
}

//======================================  Scale operator
fSeries&
fSeries::operator*=( double scale )
{
    if ( !empty( ) )
        refDVect( ) *= scale;
    return *this;
}

//======================================  Scale operator
fSeries&
fSeries::operator*=( dComplex scale )
{
    if ( !empty( ) )
    {
        if ( complex( ) )
            refDVect( ).scale( 0, scale, size( ) );
        else
            refDVect( ) *= scale.real( );
    }
    return *this;
}

//======================================  Multiply operator
fSeries&
fSeries::operator*=( const fSeries& fs )
{
    if ( empty( ) || size( ) != fs.size( ) || mDSMode != fs.mDSMode )
        throw runtime_error( "fSeries: Multiply by an unequal length fSeries" );
    refDVect( ) *= fs.refDVect( );
    return *this;
}

//======================================  Divide operator
fSeries&
fSeries::operator/=( const fSeries& fs )
{
    if ( empty( ) || size( ) != fs.size( ) || mDSMode != fs.mDSMode )
        throw runtime_error( "fSeries: Divide by an unequal length series" );
    refDVect( ) /= fs.refDVect( );
    return *this;
}

//======================================  Parenthesis operator.
dComplex
fSeries::operator( )( freq_type freq ) const
{
    if ( null( ) )
        throw runtime_error( "Dereferencing null fSeries" );
    return refDVect( ).getCplx( getBin( freq ) );
}

//======================================  Set data template.
template < class T >
void
fSeries::setData( freq_type f0, freq_type df, size_type len, const T* data )
{
    mF0 = f0;
    mDf = df;
    mData.reset( new DVecType< T >( len, data ) );
    setDSMode( );
}

//======================================  Template instanciation
#if !defined( __SUNPRO_CC )
template void fSeries::setData< float >( freq_type    f0,
                                         freq_type    df,
                                         size_type    len,
                                         const float* data );

template void fSeries::setData< double >( freq_type     f0,
                                          freq_type     df,
                                          size_type     len,
                                          const double* data );

template void fSeries::setData< fComplex >( freq_type       f0,
                                            freq_type       df,
                                            size_type       len,
                                            const fComplex* data );

template void fSeries::setData< dComplex >( freq_type       f0,
                                            freq_type       df,
                                            size_type       len,
                                            const dComplex* data );
#endif

//======================================  Type template Constructor
template < class T >
fSeries::fSeries( freq_type   f0,
                  freq_type   dF,
                  const Time& t0,
                  Interval    dT,
                  size_type   NData,
                  const T*    Data )
    : mF0( f0 ), mDf( dF ), mT0( t0 ), mDt( dT ), mDelta( 0 )
{
    if ( NData )
        mData.reset( new DVecType< T >( NData, Data ) );
    setDSMode( );
}

#if !defined( __SUNPRO_CC )
template fSeries::fSeries( freq_type    f0,
                           freq_type    dF,
                           const Time&  t0,
                           Interval     dT,
                           size_type    len,
                           const float* data );

template fSeries::fSeries( freq_type     f0,
                           freq_type     dF,
                           const Time&   t0,
                           Interval      dT,
                           size_type     len,
                           const double* data );

template fSeries::fSeries( freq_type       f0,
                           freq_type       dF,
                           const Time&     t0,
                           Interval        dT,
                           size_type       len,
                           const fComplex* data );

template fSeries::fSeries( freq_type       f0,
                           freq_type       dF,
                           const Time&     t0,
                           Interval        dT,
                           size_type       len,
                           const dComplex* data );
#endif
