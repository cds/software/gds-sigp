/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "CSD.hh"
#include "DFT.hh"
#include "TSeries.hh"
#include <stdexcept>

using namespace std;
using namespace containers;

//====================================== CSD default constructor
CSD::CSD( void )
{
}

//====================================== CSD data constructor
CSD::CSD( const DFT& fs1, const DFT& fs2 )
{
    if ( fs1.empty( ) )
        return;
    if ( fs1.size( ) != fs2.size( ) )
    {
        throw runtime_error( "Can't construct CSD from different size DFTs" );
    }
    fSeries::operator=( fs1 );
    refDVect( ).cmpy( 0, fs2.refDVect( ), 0, size( ) );
    refDVect( ) *= getFStep( );

    //----------------------------------  Single sided CSDs have a factor
    //                                    of 2 for +/- freqs in bins (1...N-1)
    if ( single_sided( ) )
        refDVect( ).scale( 1, 2.0, size( ) - 2 );
}

//====================================== CSD destructor
CSD::~CSD( void )
{
}

//====================================== CSD clone method
CSD*
CSD::clone( void ) const
{
    return new CSD( *this );
}
