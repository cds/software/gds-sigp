/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "PConfig.h"
#include "DFT.hh"
#include "TSeries.hh"
#include "DVecType.hh"
#include "constant.hh"
#include "fft.hh"
#include "gds_functions.hh"
#include <cmath>

using namespace containers;

#include <iostream>
using namespace std;

//======================================  DFT default constructor
DFT::DFT( void )
{
}

//======================================  DFT data constructor
DFT::DFT( const TSeries& ts )
{
    setData( ts );
}

//======================================  DFT clone method
DFT*
DFT::clone( void ) const
{
    return new DFT( *this );
}

//======================================  DFT clone method
DFT
DFT::evolve( Interval dt ) const
{

    //----------------------------------  Get fSeries Parameters
    size_type N = size( );
    double    dF = getFStep( );
    double    f0 = getBinF( 0 );
    double    delta = fmod( dF * dt, 1.0 );
    if ( !delta )
        return *this;

    //----------------------------------  Calculate the repetition length
    long   num, denom;
    double dx = rat( delta, num, denom, 0.001 / double( N ) );
    long   repeatLen = ( denom < N ) ? denom : N;

    //----------------------------------  Fill one repetition length
    double Phi0 = f0 * twopi * dt;
    double dPhi = delta * twopi;
    DVectW dv( N );
    for ( size_type i = 0; i < repeatLen; ++i )
    {
        double arg = Phi0 + dPhi * i;
#ifdef USE_SINCOS
        double sinphi, cosphi;
        sincos( arg, &sinphi, &cosphi );
        dv[ i ] = dComplex( cosphi, sinphi );
#else
        dv[ i ] = dComplex( cos( arg ), sin( arg ) );
#endif
    }

    //----------------------------------  Fill in the multiplier vector
    for ( long j = repeatLen; j < N; j += repeatLen )
    {
        long ncopy = ( j + repeatLen > N ) ? N - j : repeatLen;
        dv.replace( j, repeatLen, dv, 0, repeatLen );
    }

    //----------------------------------  Build the DFT
    DFT r;
    r.setData( f0, dF, dv );
    r.setTimeSpan( getStartTime( ), getDt( ) );
    r *= *this;
    return r;
}

//======================================  Extract a DFT
DFT
DFT::extract_dft( freq_type f0, freq_type dF ) const
{
    DFT r;
    dynamic_cast< fSeries& >( r ) = fSeries::extract( f0, dF );
    return r;
}

//======================================  Transform the DFT to a time series.
TSeries
DFT::iFFT( void ) const
{
    TSeries out;
    iFFT( out );
    return out;
}

//======================================  Transform the DFT to a time series.
void
DFT::iFFT( TSeries& out ) const
{
    //----------------------------------  Get TSeries length
    size_type N = series_length( );
    double    dF = getFStep( );
    if ( !( N > 0 && dF > 0 ) )
    {
        out.Clear( );
        cerr << "DFT::iFFT failed, N=" << N << " dF=" << dF << endl;
        return;
    }
    Interval tStep = 1. / ( N * dF );

    //----------------------------------  Pointer to input (Complex) series.
    DVector* dOut = out.refDVect( );
    double   f0 = 0;
    if ( single_sided( ) )
    {
        if ( !dOut || dOut->getType( ) != DVector::t_double )
        {
            dOut = new DVecType< double >( N );
        }
        else
        {
            dOut->ReSize( N );
        }
        DVecType< dComplex > dvd( refDVect( ) );
        wfft(
            dvd.refTData( ), dynamic_cast< DVectD* >( dOut )->refTData( ), N );
        f0 = getLowFreq( );
    }
    else
    {
        int nNeg = N / 2;
        int nPos = N - nNeg;
        if ( !dOut || dOut->getType( ) != DVector::t_dcomplex )
        {
            dOut = new DVecType< dComplex >( N );
        }
        else
        {
            dOut->ReSize( N );
        }

        //--------------------------------------------------------------
        //
        //  Note that the vector reordering is done here. It is probably
        //  Safe to leave it like this. If it is to be changed, code has
        //  to be added to take care of type detection and conversion.
        //
        //--------------------------------------------------------------
        DVecType< dComplex >* dv = dynamic_cast< DVectW* >( dOut );
        dv->replace( 0, nPos, refDVect( ), nNeg, nPos );
        dv->replace( nPos, nNeg, refDVect( ), 0, nNeg );
        wfft( dv->refTData( ), N, 0 );
        f0 = ( getLowFreq( ) + getHighFreq( ) ) * 0.5;
    }
    *dOut *= dF;
    out.setData( getStartTime( ), tStep, dOut );
    out.setF0( f0 );
}
//======================================  Conjugate the DFT data
DFT
DFT::operator~( void ) const
{
    if ( !complex( ) )
        return *this;
    DFT rdft( *this );
    rdft.refDVect( ).Conjugate( );
    return rdft;
}

//======================================  Return the series length.
DFT::size_type
DFT::series_length( void ) const
{
    size_t N = size( );
    if ( !single_sided( ) )
        return N;
    return ( getDSMode( ) == kFoldedOdd ) ? 2 * N - 1 : 2 * ( N - 1 );
    //  ***check***
}

//======================================  Set the data to the FFT of a TSeries
void
DFT::setData( const TSeries& ts )
{
    clear( );
    size_type nData = ts.getNSample( );
    if ( !nData )
        return;

    freq_type f0 = ts.getF0( );
    Interval  delta = ts.getTStep( );

    //----------------------------------  Get a dComplex output DVector
    DVectW* dv;

    //----------------------------------  Fill from a complex TSeries.
    const DVector* tsVect = ts.refDVect( );
    if ( tsVect->C_data( ) || tsVect->W_data( ) )
    {
        dv = new DVectW( *tsVect );
        wfft( dv->refTData( ), nData, 1 );
        wfft_reorder( dv->refTData( ), dv->refTData( ), nData, false );
        f0 -= 0.5 / delta;

        //----------------------------------  Fill from a real TSeries.
    }
    else
    {
        const DVecType< double > dvd( *tsVect );
        dv = new DVectW( nData / 2 + 1 );
        wfft( dvd.refTData( ), dv->refTData( ), nData );
    }

    //-------------------------------  Normalize the data
    *dv *= delta / ts.getSigmaW( );
    Interval dT = ts.getInterval( );
    fSeries::setData( f0, 1.0 / dT, dv );
    setTimeSpan( ts.getStartTime( ), dT );
    fSeries::setSampleTime( delta );
}
