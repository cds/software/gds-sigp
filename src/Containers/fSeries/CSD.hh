/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef CONTAINER_CSD_HH
#define CONTAINER_CSD_HH

#include "fSeries.hh"

namespace containers
{

    class DFT;

    /**  The CSD class is an fSeries-based container for cross spectral 
      *  density \em i.e. \f$ C_{XY} = \tilde{X} \tilde{Y}^* \f$.
      *  \brief Cross spectral density container.
      *  \author John Zweizig (john.zweizig@ligo.org)
      */
    class CSD : public fSeries
    {
    public:
        /**  Default empty condtructor.
	  *  \brief Empty constructor.
	  */
        CSD( void );

        /**  Cross spectral density data constructor.
	  *  \brief Data constructor.
	  *  \param fs1 DFT of first (X) signal
	  *  \param fs2 DFT of second (Y) signal
	  */
        CSD( const DFT& fs1, const DFT& fs2 );

        /**  Destroy a CSD object and release its allocated memory.
	  *  \brief Destructor.
	  */
        ~CSD( void );

        /**  Construct an identical CDS object.
	  *  \brief Clone a CSD
	  *  \return Pointer to the cloned object.
	  */
        CSD* clone( void ) const;

        /**  Get the fSeries sub-type. This will always return \c kCSD .
	  *  \brief Get fSeries sub-type.
	  *  \return fSeries sub-type code.
	  */
        fSeries::FSType getFSType( void ) const;
    };

    //==================================  Inline methods
    inline fSeries::FSType
    CSD::getFSType( void ) const
    {
        return fSeries::kCSD;
    }

} // namespace containers

#endif // !defined(CONTAINER_CSD_HH)
