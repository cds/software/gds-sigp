/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef DV_FUNCTION_HH
#define DV_FUNCTION_HH

#include "DVecType.hh"

/** \file
  * Functions on data vectors
  * \brief Functions of data vectors
  * \author John G. Zweizig <john.zweizig@ligo.org>
  * \version $Id: $
  */
//@{

/**  Calculate the modulus-squared of a specified vector.
  *  \brief Modulus-squared
  *  \param dv input vector.
  *  \returns Double precision data vector containing modulus squared.
  */
DVectD dv_modsq( const DVector& dv );

/**  Calculate the argument of a specified data vector. If the input vector 
  *  is not complex, the argument is zero.
  *  \brief Complex argument
  *  \param dv input vector.
  *  \returns Double precision data vector containing argument values.
  */
DVectD dv_argument( const DVector& dv );

/**  Average specified number of successive bins of a data vector.
  *  \brief average bins of a data vector.
  *  \param dv   Input data vector.
  *  \param nAvg number of successive bins to average.
  *  \return  Double precision data vector containing averaged values.
  */
DVectD dv_average( const DVector& dv, size_t nAvg );

/**  Average specified number of successive bins of a complex data vector.
  *  \brief average bins of a data vector.
  *  \param dv   Input data vector.
  *  \param nAvg number of successive bins to average.
  *  \return  Double comples data vector containing averaged values.
  */
DVectW dv_cplx_avg( const DVector& dv, size_t nAvg );

//@}

#endif // !defined(DV_FUNCTION_HH)
