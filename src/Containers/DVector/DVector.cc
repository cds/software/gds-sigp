/* -*- mode: c++; c-basic-offset: 4; -*- */
//
//    DVector class implementation.
//
#include <time.h>
#include <iostream>
//
#include "PConfig.h"
#include "DVecType.hh"

// NOT CLEAR WHY THIS IS NEEDED BUT AVOIDS STRANGE LINK ERRORS ON CYGWIN/GCC
#ifdef P__WIN32
template class DVecType< short >;
template class DVecType< int >;
template class DVecType< uint32_t >;
template class DVecType< float >;
template class DVecType< double >;
template class DVecType< fComplex >;
template class DVecType< dComplex >;
#endif

using namespace std;

//======================================  Define the data type names.
const char*
DVector::dvtype_name( DVType typ )
{
    switch ( typ )
    {
    case t_short:
        return "short";
    case t_int:
        return "int";
    case t_long:
        return "long";
    case t_float:
        return "float";
    case t_double:
        return "double";
    case t_complex:
        return "fComplex";
    case t_dcomplex:
        return "dComplex";
    case t_uint:
        return "uint";
    default:
        return "Unknown";
    }
}
//======================================  Convert the data type names.
DVector::DVType
DVector::dvtype_code( const std::string& name )
{
    if ( name == "short" )
        return t_short;
    if ( name == "int" )
        return t_int;
    if ( name == "long" )
        return t_long;
    if ( name == "float" )
        return t_float;
    if ( name == "double" )
        return t_double;
    if ( name == "fComplex" )
        return t_complex;
    if ( name == "dComplex" )
        return t_dcomplex;
    if ( name == "uint" )
        return t_uint;
    throw std::runtime_error( "DVector::dvtype_code: Unknown data type name" );
}

//======================================  Get the vector data type name
const char*
DVector::getTypeName( void ) const
{
    return dvtype_name( getType( ) );
}

//======================================  Vector append
void
DVector::Append( size_type N, const short* data )
{
    const CWVec< short > cv( N, data, false );
    replace( getLength( ), 0, DVecType< short >( cv ), 0, N );
}

void
DVector::Append( size_type N, const int* data )
{
    const CWVec< int > cv( N, data, false );
    replace( getLength( ), 0, DVecType< int >( cv ), 0, N );
}

void
DVector::Append( size_type N, const uint32_t* data )
{
    const CWVec< uint32_t > cv( N, data, false );
    replace( getLength( ), 0, DVecType< uint32_t >( cv ), 0, N );
}

void
DVector::Append( size_type N, const float* data )
{
    const CWVec< float > cv( N, data, false );
    replace( getLength( ), 0, DVecType< float >( cv ), 0, N );
}

void
DVector::Append( size_type N, const double* data )
{
    const CWVec< double > cv( N, data, false );
    replace( getLength( ), 0, DVecType< double >( cv ), 0, N );
}

void
DVector::Append( size_type N, const fComplex* data )
{
    const CWVec< fComplex > cv( N, data, false );
    replace( getLength( ), 0, DVecType< fComplex >( cv ), 0, N );
}

void
DVector::Append( size_type N, const dComplex* data )
{
    const CWVec< dComplex > cv( N, data, false );
    replace( getLength( ), 0, DVecType< dComplex >( cv ), 0, N );
}

//======================================  Construct a converted vector
DVector*
DVector::convert( DVType type ) const
{
    switch ( type )
    {
    case t_short:
        return new DVectS( *this );
    case t_int:
        return new DVectI( *this );
    case t_float:
        return new DVectF( *this );
    case t_double:
        return new DVectD( *this );
    case t_complex:
        return new DVectC( *this );
    case t_dcomplex:
        return new DVectW( *this );
    case t_uint:
        return new DVectU( *this );
    default:
        throw runtime_error( "DVector::convert: Invalid type specified" );
    }
}

//======================================  Extend vector with zeros
void
DVector::Extend( size_type newlen )
{
    size_type end = size( );
    if ( newlen > end )
        replace_with_zeros( end, 0, newlen - end );
}
