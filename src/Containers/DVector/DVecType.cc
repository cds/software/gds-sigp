/* -*- mode: c++; c-basic-offset: 3; -*- */
//
//    DVector class implementation.
//
#include "DVecType.hh"
#include <iostream>
#include "gds_fpclass.hh"
#include "gds_veclib.hh"
#include <algorithm>
#include <stdexcept>

using namespace std;

#ifdef CWVEC_STATS
CWStat_struct CWStats;

CWStat_struct::CWStat_struct( void )
    : alloc_count( 0 ), delete_count( 0 ), copy_count( 0 ), deep_count( 0 )
{
}

CWStat_struct::~CWStat_struct( void )
{
    if ( alloc_count != delete_count )
    {
        cerr << "WARNING! allocation count (" << alloc_count
             << ") differs from the delete count (" << delete_count << ")."
             << endl;
    }
    if ( getenv( "CWVEC_STATS" ) )
    {
        cerr << "CWVec statistics:" << endl;
        cerr << "  Number of vectors allocated:  " << alloc_count << endl;
        cerr << "  Number of vectors deleted:    " << delete_count << endl;
        cerr << "  Number of shallow copies:     " << copy_count << endl;
        cerr << "  Number of deep (data) copies: " << deep_count << endl;
    }
}
#endif

//======================================  Inline substring check function
inline void
check_substr( DVector::size_type& inx,
              DVector::size_type& N,
              DVector::size_type  len )
{
    if ( inx + N > len )
    {
        if ( inx > len )
            inx = len;
        N = len - inx;
    }
}

//======================================  Templates
//======================================  Constructors and destructors
template < class T >
DVecType< T >::DVecType( const DVector& dv )
{
    *this = dv;
}

template < class T >
DVecType< T >::DVecType( const DVecType& dv, size_type len ) : mData( dv.mData )
{
    mData.substr( 0, len );
}

template < class T >
DVecType< T >::DVecType( size_type len, const T* data ) : mData( len, data )
{
}

template < class T >
DVecType< T >::~DVecType( void )
{
}

template < class T >
DVecType< T >*
DVecType< T >::clone( void ) const
{
    return new DVecType( *this );
}

template <>
DVector::DVType
DVecType< short >::getDataType( void )
{
    return DVector::t_short;
}

template <>
DVector::DVType
DVecType< int >::getDataType( void )
{
    return DVector::t_int;
}

template <>
DVector::DVType
DVecType< float >::getDataType( void )
{
    return DVector::t_float;
}

template <>
DVector::DVType
DVecType< double >::getDataType( void )
{
    return DVector::t_double;
}

template <>
DVector::DVType
DVecType< fComplex >::getDataType( void )
{
    return DVector::t_complex;
}

template <>
DVector::DVType
DVecType< dComplex >::getDataType( void )
{
    return DVector::t_dcomplex;
}

template <>
DVector::DVType
DVecType< uint32_t >::getDataType( void )
{
    return DVector::t_uint;
}

//======================================  Test for finite data.
template <>
bool
DVectF::finite( void ) const
{
    return finite_vect( refTData( ), mData.size( ) );
    //size_type len = mData.size();
    //if (!len) return true;
    //const float* myData = refTData();
    //bool rc = isfinite(*myData);
    //for (size_type i=1; i<len; ++i) if (!isfinite(myData[i])) rc = false;
    //return rc;
}

template <>
bool
DVectD::finite( void ) const
{
    return finite_vect( refTData( ), mData.size( ) );
    //size_type len = mData.size();
    //if (!len) return true;
    //const double* myData = refTData();
    //bool rc = isfinite(*myData);
    //for (size_type i=1; i<len; ++i) if (!isfinite(myData[i])) rc = false;
    //return rc;
}

template <>
bool
DVectC::finite( void ) const
{
    const float* myData = reinterpret_cast< const float* >( refTData( ) );
    return finite_vect( myData, 2 * mData.size( ) );
    //size_type len = mData.size();
    //if (!len) return true;
    //len *= 2;
    //bool rc = isfinite(*myData);
    //for (size_type i=1; i<len; ++i) if (!isfinite(myData[i])) rc = false;
    //return rc;
}

template <>
bool
DVectW::finite( void ) const
{
    const double* myData = reinterpret_cast< const double* >( refTData( ) );
    return finite_vect( myData, 2 * mData.size( ) );
    //size_type len = mData.size();
    //if (!len) return true;
    //len *= 2;
    //bool rc = isfinite(*myData);
    //for (size_type i=1; i<len; ++i) if (!isfinite(myData[i])) rc = false;
    //return rc;
}

template < class T >
bool
DVecType< T >::finite( void ) const
{
    return true;
}

//======================================  Test for finite data.
template <>
bool
DVectF::normal( void ) const
{
    size_type len = mData.size( );
    if ( !len )
        return true;
    const float* myData = refTData( );
    bool         rc = isfinite( *myData ) && isnormal( *myData );
    for ( size_type i = 1; i < len; ++i )
    {
        float x = myData[ i ];
        if ( x == 0 )
            continue;
        if ( !isfinite( x ) || !isnormal( x ) )
            rc = false;
    }
    return rc;
}

template <>
bool
DVectD::normal( void ) const
{
    size_type len = mData.size( );
    if ( !len )
        return true;
    const double* myData = refTData( );
    bool          rc = isfinite( *myData ) && isnormal( *myData );
    for ( size_type i = 1; i < len; ++i )
    {
        double x = myData[ i ];
        if ( x == 0 )
            continue;
        if ( !isfinite( x ) || !isnormal( x ) )
            rc = false;
    }
    return rc;
}

template <>
bool
DVectC::normal( void ) const
{
    size_type len = mData.size( );
    if ( !len )
        return true;
    const float* myData = reinterpret_cast< const float* >( refTData( ) );
    len *= 2;
    bool rc = isfinite( *myData ) && isnormal( *myData );
    for ( size_type i = 1; i < len; ++i )
    {
        float x = myData[ i ];
        if ( x == 0 )
            continue;
        if ( !isfinite( x ) || !isnormal( x ) )
            rc = false;
    }
    return rc;
}

template <>
bool
DVectW::normal( void ) const
{
    size_type len = mData.size( );
    if ( !len )
        return true;
    const double* myData = reinterpret_cast< const double* >( refTData( ) );
    len *= 2;
    bool rc = isfinite( *myData ) && isnormal( *myData );
    for ( size_type i = 1; i < len; ++i )
    {
        double x = myData[ i ];
        if ( x == 0 )
            continue;
        if ( !isfinite( x ) || !isnormal( x ) )
            rc = false;
    }
    return rc;
}

template < class T >
bool
DVecType< T >::normal( void ) const
{
    return true;
}

//======================================  Force copy
template < class T >
void
DVecType< T >::force_copy( void )
{
    mData.access( );
}

//======================================  get argument Vector data
template < class T >
std::unique_ptr< T[] >
DVecType< T >::arg_data( const DVector& v, size_type inx, size_type N ) const
{
    std::unique_ptr< T[] > p( new T[ N ] );
    v.getData( inx, N, p.get( ) );
    return p;
}

//======================================  GetUInt() templates
template <>
uint32_t
DVecType< fComplex >::getUInt( size_type inx ) const
{
    float d = mData[ inx ].real( );
    if ( d < 0 )
        throw std::runtime_error( "Can't convert negative value to uint" );
    return uint32_t( d );
}

template <>
uint32_t
DVecType< dComplex >::getUInt( size_type inx ) const
{
    double d = mData[ inx ].real( );
    if ( d < 0 )
        throw std::runtime_error( "Can't convert negative value to uint" );
    return uint32_t( d );
}

template <>
uint32_t
DVecType< uint32_t >::getUInt( size_type inx ) const
{
    return mData[ inx ];
}

template < class T >
uint32_t
DVecType< T >::getUInt( size_type inx ) const
{
    T d = mData[ inx ];
    if ( d < 0 )
        throw std::runtime_error( "Can't convert negative value to uint" );
    return uint32_t( d );
}

//======================================  GetData() templates
template <>
DVector::size_type
DVectS::getData( size_type inx, size_type N, short* d ) const
{
    size_type len = mData.size( );
    if ( inx >= len )
        return 0;
    size_type nw = ( len < ( N + inx ) ) ? len - inx : N;
    memcpy( d, refTData( ) + inx, nw * sizeof( short ) );
    return nw;
}

template < class T >
DVector::size_type
DVecType< T >::getData( size_type inx, size_type N, short* d ) const
{
    size_type nw = N;
    check_substr( inx, nw, mData.size( ) );
    for ( size_type i = 0; i < nw; ++i )
        d[ i ] = getShort( i + inx );
    return nw;
}

template <>
DVector::size_type
DVectF::getData( size_type inx, size_type N, int* d ) const
{
    size_type nw = N;
    check_substr( inx, nw, mData.size( ) );
    vcvtfi( refTData( ) + inx, d, nw );
    return nw;
}

template < class T >
DVector::size_type
DVecType< T >::getData( size_type inx, size_type N, int* d ) const
{
    size_type nw = N;
    check_substr( inx, nw, mData.size( ) );
    for ( size_type i = 0; i < nw; ++i )
        d[ i ] = getInt( i + inx );
    return nw;
}

template <>
DVector::size_type
DVectU::getData( size_type inx, size_type N, uint32_t* d ) const
{
    size_type nw = N;
    check_substr( inx, nw, mData.size( ) );
    memcpy( d, refTData( ) + inx, nw * sizeof( uint32_t ) );
    return nw;
}

template < class T >
DVector::size_type
DVecType< T >::getData( size_type inx, size_type N, uint32_t* d ) const
{
    size_type nw = N;
    check_substr( inx, nw, mData.size( ) );
    for ( size_type i = 0; i < nw; ++i )
        d[ i ] = getUInt( i + inx );
    return nw;
}

template <>
DVector::size_type
DVectF::getData( size_type inx, size_type N, float* d ) const
{
    size_type nw = N;
    check_substr( inx, nw, mData.size( ) );
    memcpy( d, refTData( ) + inx, nw * sizeof( float ) );
    return nw;
}

template <>
DVector::size_type
DVectI::getData( size_type inx, size_type N, float* d ) const
{
    size_type nw = N;
    check_substr( inx, nw, mData.size( ) );
    vcvtif( refTData( ) + inx, d, nw );
    return nw;
}

template <>
DVector::size_type
DVectC::getData( size_type inx, size_type N, float* d ) const
{
    size_type nw = N;
    check_substr( inx, nw, mData.size( ) );
    const fComplex* rp = refTData( ) + inx;
    for ( size_type i = 0; i < nw; ++i )
        d[ i ] = ( rp++ )->real( );
    return nw;
}

template <>
DVector::size_type
DVectW::getData( size_type inx, size_type N, float* d ) const
{
    size_type nw = N;
    check_substr( inx, nw, mData.size( ) );
    const dComplex* rp = refTData( ) + inx;
    for ( size_type i = 0; i < nw; ++i )
        d[ i ] = ( rp++ )->real( );
    return nw;
}

template < class T >
DVector::size_type
DVecType< T >::getData( size_type inx, size_type N, float* d ) const
{
    size_type nw = N;
    check_substr( inx, nw, mData.size( ) );
    const T* rp = refTData( ) + inx;
    for ( size_type i = 0; i < nw; ++i )
        d[ i ] = float( *rp++ );
    return nw;
}

template <>
DVector::size_type
DVectC::getData( size_type inx, size_type N, double* d ) const
{
    size_type nw = N;
    check_substr( inx, nw, mData.size( ) );
    const fComplex* rp = refTData( ) + inx;
    for ( size_type i = 0; i < nw; ++i )
        d[ i ] = ( rp++ )->real( );
    return nw;
}

template <>
DVector::size_type
DVectW::getData( size_type inx, size_type N, double* d ) const
{
    size_type nw = N;
    check_substr( inx, nw, mData.size( ) );
    const dComplex* rp = refTData( ) + inx;
    for ( size_type i = 0; i < nw; ++i )
        d[ i ] = ( rp++ )->real( );
    return nw;
}

template <>
DVector::size_type
DVectD::getData( size_type inx, size_type N, double* d ) const
{
    size_type nw = N;
    check_substr( inx, nw, mData.size( ) );
    memcpy( d, refTData( ) + inx, nw * sizeof( double ) );
    return nw;
}

template < class T >
DVector::size_type
DVecType< T >::getData( size_type inx, size_type N, double* d ) const
{
    size_type nw = N;
    check_substr( inx, nw, mData.size( ) );
    const T* rp = refTData( ) + inx;
    for ( size_type i = 0; i < nw; ++i )
        d[ i ] = double( *rp++ );
    return nw;
}

template < class T >
DVector::size_type
DVecType< T >::getData( size_type inx, size_type N, fComplex* d ) const
{
    size_type nw = N;
    check_substr( inx, nw, mData.size( ) );
    for ( size_type i = 0; i < nw; ++i )
        d[ i ] = getCplx( i + inx );
    return nw;
}

template <>
DVector::size_type
DVectW::getData( size_type inx, size_type N, dComplex* d ) const
{
    size_type nw = N;
    check_substr( inx, nw, mData.size( ) );
    memcpy( d, refTData( ) + inx, nw * sizeof( dComplex ) );
    return nw;
}

template < class T >
DVector::size_type
DVecType< T >::getData( size_type inx, size_type N, dComplex* d ) const
{
    size_type len = mData.size( );
    if ( inx >= len )
        return 0;
    size_type nw = ( len < ( N + inx ) ) ? len - inx : N;
    for ( size_type i = 0; i < nw; ++i )
        d[ i ] = getDCplx( i + inx );
    return nw;
}

//======================================  Vector minimum, maximum
template < class T >
double
DVecType< T >::getMaximum( void ) const
{
    size_type len = mData.size( );
    if ( !len )
        return 0.0;
    const T* myData = refTData( );
    T        Vmax( *myData );
    for ( size_type i = 1; i < len; ++i )
        if ( myData[ i ] > Vmax )
            Vmax = myData[ i ];
    return double( Vmax );
}

template < class T >
double
DVecType< T >::getMinimum( void ) const
{
    size_type len = mData.size( );
    if ( !len )
        return 0.0;
    const T* myData = refTData( );
    T        Vmin( *myData );
    for ( size_type i = 1; i < len; ++i )
        if ( myData[ i ] < Vmin )
            Vmin = myData[ i ];
    return double( Vmin );
}

template <>
double
DVecType< fComplex >::getMaximum( void ) const
{
    size_type len = mData.size( );
    if ( !len )
        return 0.0;
    const fComplex* myData = refTData( );
    double          Vmax( ( *myData ).real( ) );
    for ( size_type i = 1; i < len; ++i )
    {
        if ( myData[ i ].real( ) > Vmax )
            Vmax = myData[ i ].real( );
    }
    return Vmax;
}

template <>
double
DVecType< dComplex >::getMaximum( void ) const
{
    size_type len = mData.size( );
    if ( !len )
        return 0.0;
    const dComplex* myData = refTData( );
    double          Vmax( ( *myData ).real( ) );
    for ( size_type i = 1; i < len; ++i )
    {
        if ( myData[ i ].real( ) > Vmax )
            Vmax = myData[ i ].real( );
    }
    return Vmax;
}

template <>
double
DVecType< fComplex >::getMinimum( void ) const
{
    size_type len = mData.size( );
    if ( !len )
        return 0.0;
    const fComplex* myData = refTData( );
    double          Vmin( ( *myData ).real( ) );
    for ( size_type i = 1; i < len; ++i )
    {
        if ( myData[ i ].real( ) < Vmin )
            Vmin = myData[ i ].real( );
    }
    return Vmin;
}
template <>
double
DVecType< dComplex >::getMinimum( void ) const
{
    size_type len = mData.size( );
    if ( !len )
        return 0.0;
    const dComplex* myData = refTData( );
    double          Vmin( ( *myData ).real( ) );
    for ( size_type i = 1; i < len; ++i )
    {
        if ( myData[ i ].real( ) < Vmin )
            Vmin = myData[ i ].real( );
    }
    return Vmin;
}

//====================================== Number of entry in limits
template < class T >
DVector::size_type
DVecType< T >::getNBetween( double low, double high ) const
{
    size_type N( 0 );
    size_type len = mData.size( );
    if ( len )
    {
        const T* myData = refTData( );
        T        Vmin = T( low );
        T        Vmax = T( high );
        for ( size_type i = 0; i < len; ++i )
        {
            if ( myData[ i ] >= Vmin && myData[ i ] < Vmax )
                N++;
        }
    }
    return N;
}

template < class T >
DVector::size_type
DVecType< T >::getNGreater( double limit ) const
{
    size_type len = mData.size( );
    size_type N( 0 );
    if ( len )
    {
        const T* myData = refTData( );
        T        Vmax = T( limit );
        while ( len-- )
            if ( *myData++ > Vmax )
                N++;
    }
    return N;
}

template < class T >
DVector::size_type
DVecType< T >::getNLess( double limit ) const
{
    size_type len = mData.size( );
    size_type N( 0 );
    if ( len )
    {
        const T* myData = refTData( );
        T        Vmin = T( limit );
        while ( len-- )
            if ( *myData++ < Vmin )
                N++;
    }
    return N;
}

template <>
DVector::size_type
DVecType< fComplex >::getNBetween( double low, double high ) const
{
    size_type len = mData.size( );
    size_type N( 0 );
    if ( len )
    {
        const fComplex* myData = refTData( );
        while ( len-- )
        {
            double d = ( *myData++ ).real( );
            if ( d >= low && d < high )
                N++;
        }
    }
    return N;
}

template <>
DVector::size_type
DVecType< fComplex >::getNGreater( double Vmax ) const
{
    size_type len = mData.size( );
    size_type N( 0 );
    if ( len )
    {
        const fComplex* myData = refTData( );
        while ( len-- )
            if ( ( *myData++ ).real( ) > Vmax )
                N++;
    }
    return N;
}

template <>
DVector::size_type
DVecType< fComplex >::getNLess( double Vmin ) const
{
    size_type len = mData.size( );
    size_type N( 0 );
    if ( len )
    {
        const fComplex* myData = refTData( );
        while ( len-- )
            if ( ( *myData++ ).real( ) < Vmin )
                N++;
    }
    return N;
}

template <>
DVector::size_type
DVecType< dComplex >::getNBetween( double low, double high ) const
{
    size_type N( 0 );
    size_type len = mData.size( );
    if ( len )
    {
        const dComplex* myData = refTData( );
        while ( len-- )
        {
            double d = ( *myData++ ).real( );
            if ( d >= low && d < high )
                N++;
        }
    }
    return N;
}

template <>
DVector::size_type
DVecType< dComplex >::getNGreater( double Vmax ) const
{
    size_type len = mData.size( );
    size_type N( 0 );
    if ( len )
    {
        const dComplex* myData = refTData( );
        while ( len-- )
            if ( ( *myData++ ).real( ) > Vmax )
                N++;
    }
    return N;
}

template <>
DVector::size_type
DVecType< dComplex >::getNLess( double Vmin ) const
{
    size_type len = mData.size( );
    size_type N( 0 );
    if ( len )
    {
        const dComplex* myData = refTData( );
        while ( len-- )
            if ( ( *myData++ ).real( ) < Vmin )
                N++;
    }
    return N;
}

//======================================  Replace a substring
template < class T >
DVecType< T >&
DVecType< T >::replace(
    size_type inx, size_type M, const DVector& v, size_type inx2, size_type N )
{
    //----------------------------------  Make sure replaced string is valid
    size_type len = mData.size( );
    check_substr( inx, M, len );

    if ( N != M )
    {
        size_type newLen = len + N - M;
        size_type nMov = newLen - ( inx + N );
        if ( N > M )
            mData.resize( newLen );
        if ( nMov )
        {
            T* wp = mData.ref( ) + inx;
            memmove( wp + N, wp + M, nMov * sizeof( T ) );
        }
        if ( N < M )
            mData.resize( newLen );
    }
    if ( N )
        v.getData( inx2, N, mData.ref( ) + inx );
    return *this;
}

template < class T >
DVecType< T >&
DVecType< T >::replace( size_type inx, size_type M, T v, size_type N )
{
    size_type len = mData.size( );
    check_substr( inx, M, len );

    if ( N != M )
    {
        size_type newLen = len + N - M;
        size_type nMov = newLen - ( inx + N );
        if ( N > M )
            mData.resize( newLen );
        if ( nMov )
        {
            T* wp = mData.ref( ) + inx;
            memmove( wp + N, wp + M, nMov * sizeof( T ) );
        }
        if ( N < M )
            mData.resize( newLen );
    }

    //----------------------------------  Fill the gap.
    if ( N )
    {
        T* wp = mData.ref( ) + inx;
        if ( !v )
        {
            memset( wp, 0, N * sizeof( T ) );
        }
        else
        {
            for ( size_type i = 0; i < N; ++i )
                *wp++ = v;
        }
    }
    return *this;
}

//======================================  Insert data backwards
template < class T >
DVecType< T >&
DVecType< T >::reverse( size_type inx, const T* v, size_type N )
{
    if ( inx + N > mData.size( ) )
        mData.resize( inx + N );
    T* pOut = mData.ref( ) + inx;
    if ( v < pOut + N && pOut < v + N )
    {
        if ( v != pOut )
            memmove( pOut, v, N );
        for ( T* pEnd = pOut + N - 1; pEnd > pOut; --pEnd )
        {
            T t = *pEnd;
            *pEnd = *pOut;
            *pOut++ = t;
        }
    }
    else
    {
        for ( const T* pIn = v + N; pIn > v; )
            *pOut++ = *( --pIn );
    }
    return *this;
}

//======================================  Reverse data in vector.
template < class T >
DVecType< T >&
DVecType< T >::reverse( void )
{
    size_type N = mData.size( );
    if ( !N )
        return *this;
    if ( mData.shared( ) )
    {
        CWVec< T > temp( N );
        const T*   pIn = const_cast< const DVecType& >( *this ).refTData( );
        T*         pOut = temp.ref( ) + N;
        for ( size_type i = 0; i < N; ++i )
            *( --pOut ) = *pIn++;
        mData = temp;
    }
    else
    {
        T* pOut = mData.ref( );
        T* pEnd = mData.ref( ) + N;
        while ( pOut < --pEnd )
        {
            T temp = *pEnd;
            *pEnd = *pOut;
            *pOut++ = temp;
        }
    }
    return *this;
}

//======================================  Add two vectors
template < class T >
DVecType< T >&
DVecType< T >::add( size_type      inx,
                    const DVector& v,
                    size_type      inx2,
                    size_type      N )
{
    if ( !N )
        N = mData.size( );
    check_substr( inx, N, mData.size( ) );
    check_substr( inx2, N, v.getLength( ) );
    if ( !N )
        return *this;

    T* myData = refTData( ) + inx;
    if ( v.getType( ) != getType( ) )
    {
        auto data = arg_data( v, inx2, N );
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] += data[ i ];
    }
    else
    {
        const T* data = reinterpret_cast< const T* >( v.refData( ) ) + inx2;
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] += data[ i ];
    }
    return *this;
}

//======================================  Subtract two vectors
template < class T >
DVecType< T >&
DVecType< T >::sub( size_type      inx,
                    const DVector& v,
                    size_type      inx2,
                    size_type      N )
{
    if ( !N )
        N = mData.size( );
    check_substr( inx, N, mData.size( ) );
    check_substr( inx2, N, v.getLength( ) );
    if ( !N )
        return *this;

    T* myData = refTData( ) + inx;
    if ( v.getType( ) != getType( ) )
    {
        auto data = arg_data( v, inx2, N );
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] -= data[ i ];
    }
    else
    {
        const T* data = reinterpret_cast< const T* >( v.refData( ) ) + inx2;
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] -= data[ i ];
    }
    return *this;
}

//======================================  Complex sub-vector sum
template < class T >
DVector::complex_type
DVecType< T >::CSum( size_type inx, size_type N ) const
{
    double    d( 0.0 );
    size_type nw = N;
    check_substr( inx, nw, mData.size( ) );

    const T* myData = refTData( ) + inx;
    for ( size_type i = 0; i < nw; ++i )
        d += double( *myData++ );
    return dComplex( d );
}

template <>
DVector::complex_type
DVectC::CSum( size_type inx, size_type N ) const
{
    complex_type s( 0.0, 0.0 );
    size_type    nw = N;
    check_substr( inx, nw, mData.size( ) );

    const fComplex* myData = refTData( ) + inx;
    for ( size_type i = 0; i < nw; ++i )
        s += myData[ i ];
    return s;
}

template <>
DVector::complex_type
DVectW::CSum( size_type inx, size_type N ) const
{
    complex_type s( 0.0, 0.0 );
    size_type    nw = N;
    check_substr( inx, nw, mData.size( ) );

    const dComplex* myData = refTData( ) + inx;
    for ( size_type i = 0; i < nw; ++i )
        s += myData[ i ];
    return s;
}

//======================================  Sub-vector sum
template < class T >
DVector::math_type
DVecType< T >::VSum( size_type inx, size_type N ) const
{
    math_type d( 0.0 );
    size_type nw = N;
    check_substr( inx, nw, mData.size( ) );
    const T* myData = refTData( ) + inx;
    for ( size_type i = 0; i < nw; ++i )
        d += double( *myData++ );
    return d;
}

template <>
DVector::math_type
DVectC::VSum( size_type inx, size_type N ) const
{
    math_type d( 0.0 );
    size_type nw = N;
    check_substr( inx, nw, mData.size( ) );
    const fComplex* myData = refTData( ) + inx;
    for ( size_type i = 0; i < nw; ++i )
        d += double( myData[ i ].real( ) );
    return d;
}

template <>
DVector::math_type
DVectW::VSum( size_type inx, size_type N ) const
{
    double    d( 0.0 );
    size_type nw = N;
    check_substr( inx, nw, mData.size( ) );
    const dComplex* myData = refTData( ) + inx;
    for ( size_type i = 0; i < nw; ++i )
        d += myData[ i ].real( );
    return d;
}

//======================================  Erase a sub-vector
template < class T >
void
DVecType< T >::Erase( size_type inx, size_type len )
{
    size_type curl = mData.size( );
    if ( inx >= curl || !len )
        return;
    size_type in2 = inx + len;
    if ( !inx )
    {
        mData.substr( in2, 0 );
    }
    else if ( in2 >= curl )
    {
        mData.substr( 0, inx );
    }
    else if ( inx != in2 )
    {
        curl -= in2;
        T*       po = refTData( ) + inx;
        const T* pi = refTData( ) + in2;
        for ( size_type i = 0; i < curl; ++i )
            *po++ = *pi++;
        mData.resize( inx + curl );
    }
}

//======================================  Extract sub-vectors
template < class T >
DVecType< T >*
DVecType< T >::Extract( size_type inx, size_type len ) const
{
    return new DVecType< T >( mData, inx, len );
}

template < class T >
DVecType< T >*
DVecType< T >::Extract( size_type inx, size_type len, size_type inc ) const
{
    size_type curl = mData.size( );
    if ( inx >= curl || !len || !inc )
    {
        len = 0;
    }
    else if ( inx + inc * ( len - 1 ) + 1 > curl )
    {
        len = ( curl - 1 - inx ) / inc + 1;
    }
    DVecType* r = new DVecType( len );
    if ( len )
    {
        T*       pout = r->refTData( );
        const T* pin = refTData( ) + inx;
        for ( size_type i = 0; i < len; ++i )
        {
            pout[ i ] = *pin;
            pin += inc;
        }
    }
    return r;
}

//======================================  Resize a data vector.
template < class T >
void
DVecType< T >::ReSize( size_type len )
{
    mData.resize( len );
}

//======================================  Assignment operator.
template < class T >
DVecType< T >&
DVecType< T >::operator=( const DVector& rhs )
{
    size_type len = rhs.getLength( );
    mData.clear( );
    if ( len )
    {
        if ( rhs.getType( ) == getType( ) )
        {
            mData = dynamic_cast< const DVecType& >( rhs ).mData;
        }
        else
        {
            mData.resize( len ); //  Note 'mData = CWVec(len)' might be faster
            rhs.getData( 0, len, mData.ref( ) );
        }
    }
    return *this;
}

//======================================  Complex conjugate operator.
template <>
void
DVectC::Conjugate( void )
{
    size_type nw = mData.size( );
    if ( !nw )
        return;
    fComplex* cdata = refTData( );
    for ( size_type i = 0; i < nw; ++i )
        cdata[ i ] = ~cdata[ i ];
}

template <>
void
DVectW::Conjugate( void )
{
    size_type nw = mData.size( );
    if ( !nw )
        return;
    dComplex* cdata = refTData( );
    for ( size_type i = 0; i < nw; ++i )
        cdata[ i ] = ~cdata[ i ];
}

template < class T >
void
DVecType< T >::Conjugate( void )
{
    return;
}

//======================================  Comparison operator.
template < class T >
bool
DVecType< T >::operator==( const DVector& rhs ) const
{
    bool      rc = true;
    size_type len = rhs.getLength( );
    if ( !len || len != mData.size( ) )
    {
        rc = false;
    }
    else if ( rhs.getType( ) == getType( ) )
    {
        const T* p = (const T*)rhs.refData( );
        const T* myData = refTData( );
        for ( size_type i = 0; i < len; ++i )
        {
            if ( myData[ i ] != p[ i ] )
            {
                rc = false;
                break;
            }
        }
    }
    else
    {
        auto     v = arg_data( rhs, 0, len );
        const T* myData = refTData( );
        for ( size_type i = 0; i < len; ++i )
        {
            if ( myData[ i ] != v[ i ] )
            {
                rc = false;
                break;
            }
        }
    }
    return rc;
}

//======================================  Complex dot product
template <>
double
DVectC::dot( size_type      inx,
             const DVector& rhs,
             size_type      in2,
             size_type      N ) const
{
    check_substr( inx, N, mData.size( ) );
    check_substr( in2, N, rhs.getLength( ) );
    if ( !N )
        return 0.0;

    double       sum = 0.0;
    const float* myData = reinterpret_cast< const float* >( refTData( ) + inx );
    if ( rhs.C_data( ) )
    {
        const float* cdata =
            reinterpret_cast< const float* >( rhs.refData( ) ) + 2 * in2;
        for ( size_type i = 0; i < N; ++i )
        {
            sum += double( *myData++ ) * double( *cdata++ );
            sum -= double( *myData++ ) * double( *cdata++ );
        }
    }
    else if ( rhs.W_data( ) )
    {
        const double* cdata =
            reinterpret_cast< const double* >( rhs.refData( ) ) + 2 * in2;
        for ( size_type i = 0; i < N; ++i )
        {
            sum += double( *myData++ ) * *cdata++;
            sum -= double( *myData++ ) * *cdata++;
        }
    }
    else if ( rhs.D_data( ) )
    {
        const double* fdata =
            reinterpret_cast< const double* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
        {
            sum += double( *myData ) * fdata[ i ];
            myData += 2;
        }
    }
    else
    {
        double* rdata = new double[ N ];
        rhs.getData( in2, N, rdata );
        for ( size_type i = 0; i < N; ++i )
        {
            sum += double( *myData ) * rdata[ i ];
            myData += 2;
        }
        delete[] rdata;
    }
    return sum;
}

template <>
double
DVectW::dot( size_type      inx,
             const DVector& rhs,
             size_type      in2,
             size_type      N ) const
{
    check_substr( inx, N, mData.size( ) );
    check_substr( in2, N, rhs.getLength( ) );
    if ( !N )
        return 0.0;

    double        sum = 0.0;
    const double* myData =
        reinterpret_cast< const double* >( refTData( ) + inx );
    if ( rhs.C_data( ) )
    {
        const float* cdata =
            reinterpret_cast< const float* >( rhs.refData( ) ) + 2 * in2;
        for ( size_type i = 0; i < N; ++i )
        {
            sum += *myData++ * double( *cdata++ );
            sum -= *myData++ * double( *cdata++ );
        }
    }
    else if ( rhs.W_data( ) )
    {
        const double* cdata =
            reinterpret_cast< const double* >( rhs.refData( ) ) + 2 * in2;
        for ( size_type i = 0; i < N; ++i )
        {
            sum += *myData++ * *cdata++;
            sum -= *myData++ * *cdata++;
        }
    }
    else if ( rhs.D_data( ) )
    {
        const double* fdata =
            reinterpret_cast< const double* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
        {
            sum += *myData * fdata[ i ];
            myData += 2;
        }
    }
    else
    {
        double* rdata = new double[ N ];
        rhs.getData( in2, N, rdata );
        for ( size_type i = 0; i < N; ++i )
        {
            sum += *myData * rdata[ i ];
            myData += 2;
        }
        delete[] rdata;
    }
    return sum;
}

//======================================  Dot product.
template < class T >
double
DVecType< T >::dot( size_type      inx,
                    const DVector& rhs,
                    size_type      in2,
                    size_type      N ) const
{
    check_substr( inx, N, mData.size( ) );
    check_substr( in2, N, rhs.getLength( ) );
    if ( !N )
        return 0.0;

    double   sum = 0.0;
    const T* myData = refTData( ) + inx;
    if ( rhs.D_data( ) )
    {
        const double* fdata =
            reinterpret_cast< const double* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
            sum += double( *myData++ ) * fdata[ i ];
    }
    else
    {
        double* rdata = new double[ N ];
        rhs.getData( in2, N, rdata );
        for ( size_type i = 0; i < N; ++i )
            sum += double( *myData++ ) * rdata[ i ];
        delete[] rdata;
    }
    return sum;
}

//======================================  Complex dot product
template <>
DVectC::complex_type
DVectC::cdot( size_type      inx,
              const DVector& rhs,
              size_type      in2,
              size_type      N ) const
{
    check_substr( inx, N, mData.size( ) );
    check_substr( in2, N, rhs.getLength( ) );
    if ( !N )
        return 0.0;

    complex_type    sum = 0.0;
    const fComplex* myData = refTData( ) + inx;
    if ( rhs.C_data( ) )
    {
        const fComplex* cdata =
            reinterpret_cast< const fComplex* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
            sum += dComplex( myData[ i ] * ~cdata[ i ] );
    }
    else if ( rhs.W_data( ) )
    {
        const dComplex* cdata =
            reinterpret_cast< const dComplex* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
            sum += dComplex( myData[ i ] ) * ~cdata[ i ];
    }
    else if ( rhs.D_data( ) )
    {
        const double* cdata =
            reinterpret_cast< const double* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
            sum += dComplex( myData[ i ] ) * cdata[ i ];
    }
    else
    {
        double* rdata = new double[ N ];
        rhs.getData( in2, N, rdata );
        for ( size_type i = 0; i < N; ++i )
            sum += dComplex( myData[ i ] ) * rdata[ i ];
        delete[] rdata;
    }
    return sum;
}

template <>
DVectW::complex_type
DVectW::cdot( size_type      inx,
              const DVector& rhs,
              size_type      in2,
              size_type      N ) const
{
    check_substr( inx, N, mData.size( ) );
    check_substr( in2, N, rhs.getLength( ) );
    if ( !N )
        return 0.0;

    dComplex        sum = 0.0;
    const dComplex* myData = refTData( ) + inx;
    if ( rhs.C_data( ) )
    {
        const fComplex* cdata =
            reinterpret_cast< const fComplex* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
            sum += myData[ i ] * dComplex( ~cdata[ i ] );
    }
    else if ( rhs.W_data( ) )
    {
        const dComplex* cdata =
            reinterpret_cast< const dComplex* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
            sum += myData[ i ] * ~cdata[ i ];
    }
    else if ( rhs.D_data( ) )
    {
        const double* rdata =
            reinterpret_cast< const double* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
            sum += myData[ i ] * rdata[ i ];
    }
    else
    {
        double* rdata = new double[ N ];
        rhs.getData( in2, N, rdata );
        for ( size_type i = 0; i < N; ++i )
            sum += myData[ i ] * rdata[ i ];
        delete[] rdata;
    }
    return sum;
}

//======================================  Dot product.
template < class T >
DVector::complex_type
DVecType< T >::cdot( size_type      inx,
                     const DVector& rhs,
                     size_type      in2,
                     size_type      N ) const
{
    check_substr( inx, N, mData.size( ) );
    check_substr( in2, N, rhs.getLength( ) );
    if ( !N )
        return 0.0;

    dComplex sum = 0.0;
    const T* myData = refTData( ) + inx;
    if ( rhs.C_data( ) )
    {
        const fComplex* cdata =
            reinterpret_cast< const fComplex* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
            sum += float( *myData++ ) * ~cdata[ i ];
    }
    else if ( rhs.W_data( ) )
    {
        const dComplex* cdata =
            reinterpret_cast< const dComplex* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
            sum += float( *myData++ ) * ~cdata[ i ];
    }
    else
    {
        dComplex* rdata = new dComplex[ N ];
        rhs.getData( in2, N, rdata );
        for ( size_type i = 0; i < N; ++i )
            sum += float( *myData++ ) * ~rdata[ i ];
        delete[] rdata;
    }
    return sum;
}

//======================================  mpy (DVectC).
template <>
DVectC&
DVectC::mpy( size_type inx, const DVector& rhs, size_type in2, size_type N )
{
    check_substr( inx, N, mData.size( ) );
    check_substr( in2, N, rhs.getLength( ) );
    if ( !N )
        return *this;

    fComplex* myData = refTData( ) + inx;
    if ( rhs.getType( ) == t_double )
    {
        const double* p;
        p = reinterpret_cast< const double* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] *= *p++;
    }
    else if ( rhs.getType( ) == t_complex )
    {
        const fComplex* data =
            reinterpret_cast< const fComplex* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] *= data[ i ];
    }
    else if ( rhs.getType( ) == t_dcomplex )
    {
        const dComplex* data =
            reinterpret_cast< const dComplex* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] *= fComplex( data[ i ] );
    }
    else
    {
        double* data = new double[ N ];
        rhs.getData( in2, N, data );
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] *= data[ i ];
        delete[] data;
    }
    return *this;
}

//======================================  mpy (DVectW).
template <>
DVectW&
DVectW::mpy( size_type inx, const DVector& rhs, size_type in2, size_type N )
{
    check_substr( inx, N, mData.size( ) );
    check_substr( in2, N, rhs.getLength( ) );
    if ( !N )
        return *this;

    dComplex* myData = refTData( ) + inx;
    if ( rhs.getType( ) == t_double )
    {
        //const double* p;
        //p = reinterpret_cast<const double*>(rhs.refData()) + in2;
        //for (size_type i=0; i<N; ++i) myData[i] *= *p++;
        const double* data =
            reinterpret_cast< const double* >( rhs.refData( ) ) + in2;
        vcmuld( data, myData, N );
    }
    else if ( rhs.getType( ) == t_complex )
    {
        const fComplex* data =
            reinterpret_cast< const fComplex* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] *= dComplex( data[ i ] );
    }
    else if ( rhs.getType( ) == t_dcomplex )
    {
        const dComplex* data =
            reinterpret_cast< const dComplex* >( rhs.refData( ) ) + in2;
        vmul( data, myData, N );
    }
    else
    {
        double* data = new double[ N ];
        rhs.getData( in2, N, data );
        //for (size_type i=0; i<N; ++i) myData[i] *= data[i];
        vcmuld( data, myData, N );
        delete[] data;
    }
    return *this;
}

//======================================  mpy(DVector).
template < class T >
DVecType< T >&
DVecType< T >::mpy( size_type      inx,
                    const DVector& rhs,
                    size_type      in2,
                    size_type      N )
{
    size_type len = mData.size( );
    if ( inx >= len )
        return *this;
    if ( inx + N > len )
        N = len - inx;

    len = rhs.getLength( );
    if ( in2 >= len )
        return *this;
    if ( in2 + N > len )
        N = len - in2;

    if ( rhs.getType( ) != getType( ) )
    {
        auto data = arg_data( rhs, in2, N );
        T*   myData = refTData( ) + inx;
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] *= data[ i ];
    }
    else
    {
        T*       myData = refTData( ) + inx;
        const T* data = reinterpret_cast< const T* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] *= data[ i ];
    }
    return *this;
}

//======================================  cmpy (DVectC).
template <>
DVectC&
DVectC::cmpy( size_type inx, const DVector& rhs, size_type in2, size_type N )
{
    check_substr( inx, N, mData.size( ) );
    check_substr( in2, N, rhs.getLength( ) );
    if ( !N )
        return *this;

    fComplex* myData = refTData( ) + inx;
    if ( rhs.getType( ) == t_double )
    {
        const double* p;
        p = reinterpret_cast< const double* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] *= *p++;
    }
    else if ( rhs.getType( ) == t_complex )
    {
        const fComplex* data =
            reinterpret_cast< const fComplex* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] *= ~data[ i ];
    }
    else if ( rhs.getType( ) == t_dcomplex )
    {
        const dComplex* data =
            reinterpret_cast< const dComplex* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] *= ~fComplex( data[ i ] );
    }
    else
    {
        double* data = new double[ N ];
        rhs.getData( in2, N, data );
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] *= data[ i ];
        delete[] data;
    }
    return *this;
}

//======================================  cmpy (DVectW).
template <>
DVectW&
DVectW::cmpy( size_type inx, const DVector& rhs, size_type in2, size_type N )
{
    check_substr( inx, N, mData.size( ) );
    check_substr( in2, N, rhs.getLength( ) );
    if ( !N )
        return *this;

    dComplex* myData = refTData( ) + inx;
    if ( rhs.getType( ) == t_double )
    {
        const double* p =
            reinterpret_cast< const double* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] *= *p++;
    }
    else if ( rhs.getType( ) == t_complex )
    {
        const fComplex* data =
            reinterpret_cast< const fComplex* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] *= ~dComplex( data[ i ] );
    }
    else if ( rhs.getType( ) == t_dcomplex )
    {
        const dComplex* data =
            reinterpret_cast< const dComplex* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] *= ~data[ i ];
    }
    else
    {
        double* data = new double[ N ];
        rhs.getData( in2, N, data );
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] *= data[ i ];
        delete[] data;
    }
    return *this;
}

//======================================  cmpy(DVector).
template < class T >
DVecType< T >&
DVecType< T >::cmpy( size_type      inx,
                     const DVector& rhs,
                     size_type      in2,
                     size_type      N )
{
    check_substr( inx, N, mData.size( ) );
    check_substr( in2, N, rhs.getLength( ) );
    if ( !N )
        return *this;

    T* myData = refTData( ) + inx;
    if ( rhs.getType( ) != getType( ) )
    {
        auto data = arg_data( rhs, in2, N );
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] *= data[ i ];
    }
    else
    {
        const T* data = reinterpret_cast< const T* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] *= data[ i ];
    }
    return *this;
}

//======================================  operator/=(DVector).
template < class T >
DVecType< T >&
DVecType< T >::div( size_type      inx,
                    const DVector& rhs,
                    size_type      in2,
                    size_type      N )
{
    check_substr( inx, N, mData.size( ) );
    check_substr( in2, N, rhs.getLength( ) );
    if ( !N )
        return *this;

    T* myData = refTData( ) + inx;
    if ( rhs.getType( ) != getType( ) )
    {
        auto data = arg_data( rhs, in2, N );
        for ( size_type i = 0; i < N; ++i )
        {
            if ( !data[ i ] )
                myData[ i ] = T( 0 );
            else
                myData[ i ] /= data[ i ];
        }
    }
    else
    {
        const T* data = reinterpret_cast< const T* >( rhs.refData( ) ) + in2;
        for ( size_type i = 0; i < N; ++i )
        {
            if ( !data[ i ] )
                myData[ i ] = T( 0 );
            else
                myData[ i ] /= data[ i ];
        }
    }
    return *this;
}

//======================================  operator*=(double)  (scale factor)
template <>
DVectS&
DVectS::scale( size_type inx, math_type rhs, size_type N )
{
    if ( rhs != 1.0 )
    {
        check_substr( inx, N, mData.size( ) );
        if ( !N )
            return *this;

        short* myData = refTData( ) + inx;
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] = short( myData[ i ] * rhs );
    }
    return *this;
}

template <>
DVectI&
DVectI::scale( size_type inx, math_type rhs, size_type N )
{
    if ( rhs != 1.0 )
    {
        check_substr( inx, N, mData.size( ) );
        if ( !N )
            return *this;

        int* myData = refTData( ) + inx;
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] = int( myData[ i ] * rhs );
    }
    return *this;
}

template <>
DVectU&
DVectU::scale( size_type inx, math_type rhs, size_type N )
{
    if ( rhs < 0.0 )
    {
        throw runtime_error( "unsigned vector scaled by a negative constant" );
    }
    else if ( rhs != 1.0 )
    {
        check_substr( inx, N, mData.size( ) );
        if ( !N )
            return *this;

        uint32_t* myData = refTData( ) + inx;
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] = uint32_t( myData[ i ] * rhs );
    }
    return *this;
}

template <>
DVectD&
DVectD::scale( size_type inx, math_type rhs, size_type N )
{
    if ( rhs != 1.0 )
    {
        check_substr( inx, N, mData.size( ) );
        if ( N )
            vscale( refTData( ) + inx, rhs, N );
    }
    return *this;
}

template <>
DVectW&
DVectW::scale( size_type inx, math_type rhs, size_type N )
{
    if ( rhs != 1.0 )
    {
        check_substr( inx, N, mData.size( ) );
        if ( N )
            vscale(
                reinterpret_cast< double* >( refTData( ) + inx ), rhs, N + N );
    }
    return *this;
}

template < class T >
DVecType< T >&
DVecType< T >::scale( size_type inx, math_type rhs, size_type N )
{
    if ( rhs != 1.0 )
    {
        check_substr( inx, N, mData.size( ) );
        if ( !N )
            return *this;

        T* myData = refTData( ) + inx;
        for ( size_type i = 0; i < N; ++i )
            *myData++ *= rhs;
    }
    return *this;
}

template <>
DVectC&
DVectC::scale( size_type inx, complex_type rhs, size_type N )
{
    if ( rhs != complex_type( 1.0, 0.0 ) )
    {
        check_substr( inx, N, mData.size( ) );
        if ( !N )
            return *this;

        fComplex  fac( rhs );
        fComplex* myData = refTData( ) + inx;
        for ( size_type i = 0; i < N; ++i )
            *myData++ *= fac;
    }
    return *this;
}

template <>
DVectW&
DVectW::scale( size_type inx, complex_type rhs, size_type N )
{
    if ( rhs != complex_type( 1.0, 0.0 ) )
    {
        check_substr( inx, N, mData.size( ) );
        if ( !N )
            return *this;

        dComplex* myData = refTData( ) + inx;
        for ( size_type i = 0; i < N; ++i )
            *myData++ *= rhs;
    }
    return *this;
}

template < class T >
DVecType< T >&
DVecType< T >::scale( size_type inx, complex_type rhs, size_type N )
{
    return scale( inx, rhs.real( ), N );
}

//======================================  operator+=(double)  add bias constant
template < class T >
DVecType< T >&
DVecType< T >::bias( size_type inx, math_type rhs, size_type N )
{

    T off = T( rhs );
    if ( off != T( 0 ) )
    {
        check_substr( inx, N, mData.size( ) );
        if ( !N )
            return *this;

        T* myData = refTData( ) + inx;
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] += off;
    }
    return *this;
}

//======================================  operator+=(double)  add bias constant
template <>
DVectC&
DVectC::bias( size_type inx, complex_type rhs, size_type N )
{

    fComplex off( rhs );
    if ( off != fComplex( 0 ) )
    {
        check_substr( inx, N, mData.size( ) );
        if ( !N )
            return *this;

        fComplex* myData = refTData( ) + inx;
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] += off;
    }
    return *this;
}

//======================================  add a complex bias constant
template <>
DVectW&
DVectW::bias( size_type inx, complex_type rhs, size_type N )
{
    if ( rhs != complex_type( 0 ) )
    {
        check_substr( inx, N, mData.size( ) );
        if ( !N )
            return *this;

        dComplex* myData = refTData( ) + inx;
        for ( size_type i = 0; i < N; ++i )
            myData[ i ] += rhs;
    }
    return *this;
}

//======================================  add a complex bias constant
template < class T >
DVecType< T >&
DVecType< T >::bias( size_type inx, complex_type rhs, size_type N )
{
    return bias( inx, rhs.real( ), N );
}

//======================================  Dump a single line
template < class T >
void
dumpLine( std::ostream& out, T data[], size_t i, size_t N )
{
    out << "data[" << i << "] = ";
    for ( size_t j = 0; j < N; j++ )
        out << data[ i + j ] << "  ";
    out << std::endl;
}
template <>

void
dumpLine< uint32_t >( std::ostream& out, uint32_t data[], size_t i, size_t N )
{
    out << "data[" << i << "] = " << hex;
    for ( size_t j = 0; j < N; j++ )
        out << data[ i + j ] << "  ";
    out << dec << std::endl;
}

//======================================  Dump the vector
template < class T >
std::ostream&
DVecType< T >::Dump( std::ostream& out ) const
{
    const int ncol( 8 );
    size_type MaxLen = mData.capacity( );
    out << "DVector of type " << getTypeName( )
        << ", length = " << mData.size( ) << " (" << MaxLen
        << " words allocated)." << std::endl;
    if ( !MaxLen )
        return out;
    size_type skipline = 0;
    size_type N = mData.size( );
    const T*  myData = refTData( );
    for ( size_type i = 0; i < N; i += ncol )
    {
        size_type llen = N - i;
        if ( llen > ncol )
            llen = ncol;
        if ( i > skipline )
        {
            bool same = true;
            for ( size_t j = 0; j < llen; j++ )
            {
                if ( myData[ i + j ] != myData[ skipline + j ] )
                {
                    same = false;
                    break;
                }
            }
            if ( same && i < N - ncol )
                continue;
            //--------------------------  If more than one line skipped
            if ( i > skipline + 2 * ncol )
            {
                out << "    --- lines " << skipline + ncol << " - " << i - ncol
                    << " are the same ---" << endl;
            }
            else if ( i == skipline + ncol * 2 )
            {
                dumpLine( out, myData, skipline + ncol, ncol );
            }
            skipline = i;
        }
        dumpLine( out, myData, i, llen );
    }
    if ( skipline < N - ncol )
    {
        out << "    --- lines " << skipline + ncol << " - " << N - ncol
            << " are the same ---" << endl;
    }
    return out;
}

template < class T >
DVecType< T >*
DVecType< T >::interpolate( size_type inx, size_type N, size_type delta ) const
{
    if ( delta <= 1 )
        return Extract( inx, N );
    check_substr( inx, N, mData.size( ) );
    DVecType< T >* dv = new DVecType< T >( delta * N );
    size_t         oinx = 0;
    for ( size_t i = 0; i < N; i++ )
    {
        ( *dv )[ oinx++ ] = ( *this )[ inx + i ];
        for ( size_t j = 1; j < delta; j++ )
            ( *dv )[ oinx++ ] = T( 0 );
    }
    return dv;
}

//======================================  Explicit instantiations
template class DVecType< short >;
template class DVecType< int >;
template class DVecType< uint32_t >;
template class DVecType< float >;
template class DVecType< double >;
template class DVecType< fComplex >;
template class DVecType< dComplex >;
