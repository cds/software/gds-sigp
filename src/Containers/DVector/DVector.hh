/* -*- mode: c++; c-basic-offset: 3; -*- */
//
//   DVector class definition.  Copy on write version.
//
#ifndef DVECTOR_HH
#define DVECTOR_HH

#include <iosfwd>
#include "Complex.hh"
#ifndef __CINT__
#include <cstdint>
#else
typedef unsigned int uint32_t;
#endif

/**  The data vector holds a variable length data vector of arbitrary 
  *  type. Methods are provided to convert data to a requested type 
  *  independent of the internal representation of the data. Data are
  *  stored in a copy-on-write vector (CWVec) for efficient copying and
  *  passing data vectors between functions.
  *  %DVector are primarily used for time and frequency series.
  *  @brief Data Vector class.
  *  @author John G. Zweizig
  *  @version 2.1; Last modified: May 19, 2004
  */
class DVector
{
public:
    /**  %DVType enumerates the data types that may be held by a %DVector.
     *  Some of the data vector types may not be implemented. 
     *  @brief %DVector data type enumeration.
     */
    enum DVType
    {
        t_short,
        t_int,
        t_long,
        t_float,
        t_double,
        t_complex,
        t_dcomplex,
        t_uint
    };

    ///  Data type used internally for size and index variables.
    typedef unsigned long size_type;
    typedef double        math_type;
    typedef dComplex      complex_type;

public:
    /**  Initialize the base class element, \e i.e. the data length.
     *  @brief Default constructor.
     */
    DVector( void )
    {
    }

    /**  Null base class destructor.
     *  @brief Destructor.
     */
    virtual ~DVector( )
    {
    }

    //-------------------------------------  Accessors

    /**  An instance of a class derived from %DVector is duplicated and 
     *  a pointer to the duplicate is returned. All data are copied.
     *  @brief   Duplicate a %DVector.
     *  @return a pointer to the %DVector clone.
     */
    virtual DVector* clone( void ) const = 0;

    /**  Test whether the vector is empty.
     *  @brief Test for empty vector.
     *  @return true if the vector length is zero.
     */
    virtual bool
    empty( void ) const
    {
        return !size( );
    }

    /**  An instance of a class derived from %DVector with the specified 
     *  data type is constructed and all data are copied.
     *  @brief Construct a %DVector with the same data of specified type.
     *  @return Pointer to the resulting %DVector.
     */
    DVector* convert( DVType t ) const;

    /**  Erase a sub-string from the vector.
     *  @brief  Erase a subset of a vector.
     *  @param  inx    Index of first element to be erased.
     *  @param  length Number of data elements to be erased.
     */
    virtual void Erase( size_type inx, size_type length ) = 0;

    /**  A sub-string starting at the first element is extracted from the 
     *   %DVector. The sub-string length is forced to be valid.
     *  @brief   Extract a subset of a vector.
     *  @param  length number of data elements to be extracted.
     *  @return a pointer to a new %DVector containing the extracted data.
     */
    virtual DVector*
    Extract( size_type length ) const
    {
        return Extract( 0, length );
    }

    /**  A sub-string is extracted from the %DVector. The sub-string
     *  length is forced to be valid.
     *  @brief   Extract a subset of a vector.
     *  @param  inx    index of first element to be extracted
     *  @param  length number of data elements to be extracted.
     *  @return a pointer to a new %DVector containing the extracted data.
     */
    virtual DVector* Extract( size_type inx, size_type length ) const = 0;

    /**  A sub-string is extracted and decimated from the %DVector. The 
     *  extracted vector contains the elements defined by \c DVector[N*inc]
     *  where N is all the integers \c 0\ \<=\ N\ \<\ length
     *  @brief   Extract a subset of a vector.
     *  @param  inx    Starting index of the sub-string to be extracted
     *  @param  length Number of elements to be extracted
     *  @param  inc    Index increment between successive extracted elements.
     *  @return pointer to the extracted data vector.
     */
    virtual DVector*
    Extract( size_type inx, size_type length, size_type inc ) const = 0;

    /**  Force unshared ownership of a data vector. If the data vector is 
     *  not shared with any other data vector, no further processing is 
     *  performed. Of the data vector is shared, a copy of the vector is
     *  made and the original is released by.
     *  \brief Force unshared ownership.
     */
    virtual void force_copy( void ) = 0;

    /**  Return true if all elements of the vector are finite, i.e. not inf
     *  or NaN.
     *  @brief Test for finite data.
     *  @return true if all elements are finite.
     */
    virtual bool finite( void ) const = 0;

    /**  Return true if all elements of the vector are normalized, i.e. not 
     *  subnormal, inf or NaN.
     *  @brief Test for normalized data.
     *  @return true if all elements are normalized.
     */
    virtual bool normal( void ) const = 0;

    /**  Return the data vector type as a DVector::DVType code.
     *  @brief   Get the data type
     *  @return a data vector type code.
     */
    virtual DVType getType( void ) const = 0;

    /**    Get the data type name as a constant character string.
     *  @brief   Get the data type name.
     *  @return pointer to a constant character array containing the data 
     *          type name.
     */
    const char* getTypeName( void ) const;

    /**    Test if the vector data type is t_short.
     *  @brief   Test for short int data.
     *  @return true if the vector data contains short integers.
     */
    bool
    S_data( void ) const
    {
        return getType( ) == t_short;
    }

    /**  Test if the vector data type is t_float.
     *  @brief Test for float data.
     *  @return true if the vector data contains floats.
     */
    bool
    I_data( void ) const
    {
        return getType( ) == t_int;
    }

    /**  Test if the vector data type is t_float.
     *  @brief Test for float data.
     *  @return true if the vector data contains floats.
     */
    bool
    F_data( void ) const
    {
        return getType( ) == t_float;
    }

    /**  Test if the vector data type is t_float.
     *  @brief Test for float data.
     *  @return true if the vector data contains floats.
     */
    bool
    D_data( void ) const
    {
        return getType( ) == t_double;
    }

    /**  Test if the vector data type is t_complex.
     *  @brief  Test for fComplex data.
     *  @return true if the vector data type is t_complex.
     */
    bool
    C_data( void ) const
    {
        return getType( ) == t_complex;
    }

    /**  Test whether the vector contains double precision complex data.
     *  @brief  Test for dComplex data.
     *  @return true if the vector data type is t_dcomplex.
     */
    bool
    W_data( void ) const
    {
        return getType( ) == t_dcomplex;
    }

    /**  Get the number of bytes in a data word.
     *  @brief Get the size of a data word.
     *  @return The number of bytes in a data word.
     */
    virtual size_type getSize( void ) const = 0;

    /**  Get the number of data elements in the vector.
     *  @brief Get the data length.
     *  @deprecated Equivalent to size()
     *  @return The data vector length.
     */
    virtual size_type getLength( void ) const = 0;

    /**  Get the number of data elements in the vector.
     *  @brief Get the data length.
     *  @return The data vector length.
     */
    size_type
    size( void ) const
    {
        return getLength( );
    }

    /**  Return the capacity of the data vector in data words.
     *  @brief Get the vector capacity
     *  @return Number of words that can fit in the vector.
     */
    virtual size_type capacity( void ) const = 0;

    /**  \c ReSize insures that there is sufficient storage allocated 
     *  for \a len data items. No memory is release if the current 
     *  allocation is greater than the requested size. The vector
     *  data are copied if the vector storage must be reallocated.
     *  The current data length is set to the specified length on return
     *  but any new data remain uninitialized.
     *  @brief Expand the vector storage.
     *  @param len Minimum number of words that must be available,
     */
    virtual void ReSize( size_type len ) = 0;

    /**  A data vector of the specified length is allocated. The current
     *  data length remains the same. If the present data vector is already
     *  greater than or equal to the specified length the data vector is not
     *  reallocated. If reallocation occurs, the current data may be copied
     *  to the front of the new vector. The vector is not reserved for write 
     *  access.
     *  @brief  Insure a minimum number of words.
     *  @param len Number of words to reserve.
     */
    virtual void reserve( size_type len ) = 0;

    /**  Get a pointer to the data array. Note that because the %DVector uses
     *  internal copy on write data storage, the data can be moved by any 
     *  manipulation of the data.
     *  @brief Get a pointer to the data.
     *  @return void pointer to the data array.
     */
    virtual void*
    refData( void )
    {
        return 0;
    }

    /**  Get a constant pointer to the data array. Note that because the %DVector
     *  uses internal copy-on-write data storage, the data can be moved by any 
     *  manipulation of the data.
     *  @brief Get a constant pointer to the data.
     *  @return constant void pointer to the data array.
     */
    virtual const void*
    refData( void ) const
    {
        return 0;
    }

    /**  Copy data into a short integer output array. If the %DVector has
     *        a different type than the output array, the data are 
     *        converted to the output type.
     *  @brief Copy/Convert data into a short integer array.
     *  @param inx   Starting index of data to be copied.
     *  @param Ndata Maximum number of data words to be copied.
     *  @param Data  Output short integer array.
     *  @return number of data words actually copied.
     */
    virtual size_type
    getData( size_type inx, size_type Ndata, short Data[] ) const = 0;

    /**  Copy data into an integer output array. If the %DVector has
     *        a different type than the output array, the data are 
     *        converted to the output type.
     *  @brief Copy/Convert data into a float array.
     *  @param inx   Starting index of data to be copied.
     *  @param Ndata Maximum number of data words to be copied.
     *  @param Data  Output integer array.
     *  @return number of data words actually copied.
     */
    virtual size_type
    getData( size_type inx, size_type Ndata, int Data[] ) const = 0;

    /**  Copy data into an integer output array. If the %DVector has
     *        a different type than the output array, the data are 
     *        converted to the output type.
     *  @brief Copy/Convert data into a float array.
     *  @param inx   Starting index of data to be copied.
     *  @param Ndata Maximum number of data words to be copied.
     *  @param Data  Output integer array.
     *  @return number of data words actually copied.
     */
    virtual size_type
    getData( size_type inx, size_type Ndata, uint32_t Data[] ) const = 0;

    /**  Copy data into a float output array. If the %DVector has
     *        a different type than the output array, the data are 
     *        converted to the output type.
     *  @brief Copy/Convert data into a float array.
     *  @param inx   Starting index of data to be copied.
     *  @param Ndata Maximum number of data words to be copied.
     *  @param Data  Output float array.
     *  @return number of data words actually copied.
     */
    virtual size_type
    getData( size_type inx, size_type Ndata, float Data[] ) const = 0;

    /**  Copy data into a double float output array. If the %DVector has
     *        a different type than the output array, the data are 
     *        converted to the output type.
     *  @brief Copy/Convert data into a float array.
     *  @param inx   Starting index of data to be copied.
     *  @param Ndata Maximum number of data words to be copied.
     *  @param Data  Output double float array.
     *  @return number of data words actually copied.
     */
    virtual size_type
    getData( size_type inx, size_type Ndata, double Data[] ) const = 0;

    /**  Copy data into a complex output array. If the %DVector has
     *        a different type than the output array, the data are 
     *        converted to the output type.
     *  @brief Copy/Convert data into a complex array.
     *  @param inx   Starting index of data to be copied.
     *  @param Ndata Maximum number of data words to be copied.
     *  @param Data  Output complex array.
     *  @return number of data words actually copied.
     */
    virtual size_type
    getData( size_type inx, size_type Ndata, fComplex Data[] ) const = 0;

    /**  Copy data into a complex output array. If the %DVector has
     *        a different type than the output array, the data are 
     *        converted to the output type.
     *  @brief Copy/Convert data into a complex array.
     *  @param inx   Starting index of data to be copied.
     *  @param Ndata Maximum number of data words to be copied.
     *  @param Data  Output complex array.
     *  @return number of data words actually copied.
     */
    virtual size_type
    getData( size_type inx, size_type Ndata, dComplex Data[] ) const = 0;

    /**    A specified data word is picked up from the %DVector, converted 
     *          to a short integer and returned. No validity checking is made 
     *          on the index.
     *  @brief   Get one data word.
     *  @param  inx Index into the data vector of the word to be returned.
     *  @return value of selected word converted if necessary to short integer.
     */
    virtual short getShort( size_type inx ) const = 0;

    /**    A specified data word is picked up from the %DVector, converted 
     *          to an integer and returned. No validity checking is made 
     *          on the index.
     *  @brief   Get one data word.
     *  @param  inx Index into the data vector of the word to be returned.
     *  @return value of selected word converted if necessary to integer.
     */
    virtual int getInt( size_type inx ) const = 0;

    /**    A specified data word is picked up from the %DVector, converted 
     *          to a long integer and returned. No validity checking is made 
     *          on the index.
     *  @brief   Get one data word.
     *  @param  inx Index into the data vector of the word to be returned.
     *  @return value of selected word converted if necessary to long integer.
     */
    virtual long getLong( size_type inx ) const = 0;

    /**    A specified data word is picked up from the %DVector, converted 
     *          to a float and returned. No validity checking is made 
     *          on the index.
     *  @brief   Get one data word.
     *  @param  inx Index into the data vector of the word to be returned.
     *  @return value of selected work converted if necessary to float.
     */
    virtual float getFloat( size_type inx ) const = 0;

    /**    A specified data word is picked up from the %DVector, converted 
     *          to a double float and returned. No validity checking is made 
     *          on the index.
     *  @brief   Get one data word.
     *  @param  inx Index into the data vector of the word to be returned.
     *  @return value of selected word converted if necessary to double.
     */
    virtual double getDouble( size_type inx ) const = 0;

    /**    A specified data word is picked up from the %DVector, converted 
     *          to a complex and returned. No validity checking is made 
     *          on the index.
     *  @brief   Get one data word.
     *  @param  inx Index into the data vector of the word to be returned.
     *  @return value of selected work converted if necessary to complex.
     */
    virtual fComplex getCplx( size_type inx ) const = 0;

    /**    A specified data word is picked up from the %DVector, converted 
     *          to a complex and returned. No validity checking is made 
     *          on the index.
     *  @brief   Get one data word.
     *  @param  inx Index into the data vector of the word to be returned.
     *  @return value of selected work converted if necessary to complex.
     */
    virtual dComplex getDCplx( size_type inx ) const = 0;

    /**  Return the largest entry. If the vector is complex, the largest 
     *  real part is returned.
     *  @brief   Maximum data value.
     *  @return Largest data value.
     */
    virtual double getMaximum( void ) const = 0;

    /**  Return the smallest entry. If the vector is complex, the smallest 
     *  real part is returned.
     *  @brief   Minimum data value.
     *  @return Smallest data value.
     */
    virtual double getMinimum( void ) const = 0;

    /**  Count the number of entries between two specified limits. If the 
     *  vector is complex, the real part is compared.
     *  @brief   Number of entries greater than limit.
     *  @param  low  Lower limit of entries to be counted.
     *  @param  high Upper limit of entries to be counted.
     *  @return Number of entries in range <TT>low <= x < high</tt>.
     */
    virtual size_type getNBetween( double low, double high ) const = 0;

    /**  Count the number of entries with value greater than the specified 
     *          limit. If the vector is complex, the real part is compared.
     *  @brief  Number of entries greater than limit.
     *  @param  limit Lower limit of entries to be counted.
     *  @return Number of entries greater than limit.
     */
    virtual size_type getNGreater( double limit ) const = 0;

    /**  Count the number of entries with value less than the specified 
     *          limit. If the vector is complex, the real part is compared.
     *  @brief  Number of entries less than limit.
     *  @param  limit Upper limit of entries to be counted.
     *  @return Number of entries less than limit.
     */
    virtual size_type getNLess( double limit ) const = 0;

    /**  Insert \a N values starting at \a inx2 of the argument 
     *  vector into the current vector starting at index \a inx.
     *  @brief Inset data from a vector into the current vector.
     *  @param inx  Index of the insertion point of the current vector.
     *  @param v    Vector containing data to be inserted.
     *  @param inx2 index in v of the data to be inserted.
     *  @param N    Number of words to be inserted
     *  @return Reference to the result vector.
     */
    virtual DVector& insert( size_type      inx,
                             const DVector& v,
                             size_type      inx2 = 0,
                             size_type      N = 0 );

    /**  Replace \a N values starting at \a inx of the current vector 
     *  with \a nV values starting at index \a inv of the argument 
     *  vector. Note that the sub-strings must be within the range of the
     *  vectors, \e i.e. <tt>0 <= inx+N < len</tt> and 
     *  <tt>0 <= inv+Nv < v.len</tt>.
     *  @brief Replace data from a sub-vector into the current vector.
     *  @param inx  Index of the insertion point of the current vector.
     *  @param N    Number of words to be replaced
     *  @param v    Vector containing data to be inserted.
     *  @param inv  index in v of the data to be inserted.
     *  @param Nv   Number of words to be inserted
     *  @return Reference to the result vector.
     */
    virtual DVector& replace( size_type      inx,
                              size_type      N,
                              const DVector& v,
                              size_type      inv,
                              size_type      Nv ) = 0;

    /**  Replace \a N values starting at \a inx of the current vector 
     *  with \a N2 values starting at index \a inx2 of the argument 
     *  vector.
     *  @brief Replace data from a vector into the current vector.
     *  @param inx  Index of the insertion point of the current vector.
     *  @param N    Number of words to be replaced
     *  @param v    Vector containing data to be inserted.
     *  @return Reference to the result vector.
     */
    DVector& replace( size_type inx, size_type N, const DVector& v );

    /**  Replace the specified data sub-string with a specified number of zero
     *  elements. If the sub-string length (\a len) is zero,  \a nzer zero
     *  elements are innserted before the position specified by \s inx.
     *  @brief Replace a sub-string or insert zeros.
     *  @param inx First word to replace
     *  @param len Number of words to replace.
     *  @param nzer Number of zeroes to insert.
     */
    virtual DVector&
    replace_with_zeros( size_type inx, size_type len, size_type nzer ) = 0;

    /**  Add \a N values in the addend vector to the current vector 
     *  starting at index \a inx of the current vector and 
     *  \a inx2 of the addend vector.
     *  @brief Add two vectors
     *  @param inx  Index of the first element in this vector to be added.
     *  @param v    Vector to be added
     *  @param inx2 index in \a v of the first word to be added. 
     *  @param N    Number of words to be added. If \a N is not specified or
     *              set to zero, it defaults to the length of \a v.
     *  @return Reference to the result vector.
     */
    virtual DVector& add( size_type      inx,
                          const DVector& v,
                          size_type      inx2 = 0,
                          size_type      N = 0 ) = 0;

    /**  Add a constant to \a N values of the current vector starting at
     *  index \a inx .
     *  @brief Add a constant to a vector
     *  @param inx  Index of the first element in this vector to be added.
     *  @param x    Constant to be added
     *  @param N    Number of words to be added
     *  @return Reference to the result vector.
     */
    virtual DVector& bias( size_type inx, math_type x, size_type N = 0 ) = 0;

    /**  Add a constant to \a N values of the current vector starting at
     *  index \a inx .
     *  @brief Add a constant to a vector
     *  @param inx  Index of the first element in this vector to be added.
     *  @param x    Constant to be added
     *  @param N    Number of words to be added
     *  @return Reference to the result vector.
     */
    virtual DVector& bias( size_type inx, complex_type x, size_type N = 0 ) = 0;

    /**  Subtract \a N values in the subtrahend vector from the current 
     *  vector starting at index \a inx of the current vector and 
     *  \a inx2 of the subtrahend vector.
     *  @brief Subtract two vectors
     *  @param inx  Index of the first element in this vector to be added.
     *  @param v    Vector to be added
     *  @param inx2 index in the 
     *  @param N    Number of words to be added
     *  @return Reference to the result vector.
     */
    virtual DVector& sub( size_type      inx,
                          const DVector& v,
                          size_type      inx2 = 0,
                          size_type      N = 0 ) = 0;

    /**  Multiply \a N values in the current vector starting at index 
     *  \a inx by the values starting at \a inx2 of the multiplier 
     *  vector.
     *  @brief Multiply two vectors
     *  @param inx  Index of the first element in this vector to be added.
     *  @param v    Vector to be added
     *  @param inx2 index in the 
     *  @param N    Number of words to be added
     *  @return Reference to the result vector.
     */
    virtual DVector& mpy( size_type      inx,
                          const DVector& v,
                          size_type      inx2 = 0,
                          size_type      N = 0 ) = 0;

    /**  Multiply \a N values in the current vector starting at index 
     *  \a inx by the complex conjugate of values starting at 
     *  \a inx2 of the multiplier vector.
     *  @brief Multiply vector by the complex conjugate of another vector
     *  @param inx  Index of the first element in this vector to be added.
     *  @param v    Vector to be added
     *  @param inx2 index in the 
     *  @param N    Number of words to be added
     *  @return Reference to the result vector.
     */
    virtual DVector& cmpy( size_type      inx,
                           const DVector& v,
                           size_type      inx2 = 0,
                           size_type      N = 0 ) = 0;

    /**  Multiply \a N values of the current vector starting at index 
     *  \a inx by a constant.
     *  @brief Multiply a vector by a constant
     *  @param inx  Index of the first element in this vector to be added.
     *  @param x    Constant to be added
     *  @param N    Number of words to be added
     *  @return Reference to the result vector.
     */
    virtual DVector& scale( size_type inx, math_type x, size_type N = 0 ) = 0;

    /**  Multiply \a N values of the current vector starting at index 
     *  \a inx by a constant.
     *  @brief Multiply a vector by a constant
     *  @param inx  Index of the first element in this vector to be added.
     *  @param x    Constant to be added
     *  @param N    Number of words to be added
     *  @return Reference to the result vector.
     */
    virtual DVector&
    scale( size_type inx, complex_type x, size_type N = 0 ) = 0;

    /**  Divide \a N values starting at index \a inx of the current 
     *  vector by the values starting at \a inx2 of the divisor vector.
     *  @brief Divide two vectors
     *  @param inx  Index of the first element in this vector to be added.
     *  @param v    Vector to be added
     *  @param inx2 index in the 
     *  @param N    Number of words to be added
     *  @return Reference to the result vector.
     */
    virtual DVector& div( size_type      inx,
                          const DVector& v,
                          size_type      inx2 = 0,
                          size_type      N = 0 ) = 0;

    /**  Inner product between two sub-strings.
     *  @brief Inner product
     *  @param inx  Index of the first element in this vector to be added.
     *  @param v    Vector to be added
     *  @param inx2 index in the 
     *  @param N    Number of words to be added
     *  @return Reference to the result vector.
     */
    virtual double dot( size_type      inx,
                        const DVector& v,
                        size_type      inx2 = 0,
                        size_type      N = 0 ) const = 0;

    /**  Multiply two complex sub-vectors and sum the results.
     *  @brief Complex inner product.
     *  @param inx  Index of the first element in this vector to be multiplied.
     *  @param v    Vector to be multiplied.
     *  @param inx2 index into the argument vector
     *  @param N    Number of words to be multiplied/added.
     *  @return Reference to the result vector.
     */
    virtual dComplex cdot( size_type      inx,
                           const DVector& v,
                           size_type      inx2 = 0,
                           size_type      N = 0 ) const = 0;

    /**  All words in a specified range are converted to double and summed.
     *  @brief   Sum N data words.
     *  @param  inx Index into the data vector of first word to be summed.
     *  @param  N   Number of words to be summed.
     *  @return Sum of specified data vector.
     */
    virtual math_type VSum( size_type inx = 0, size_type N = 0 ) const = 0;

    /**  All words in a specified range are converted to complex and summed.
     *  @brief   Sum N data words.
     *  @param  inx Index into the data vector of first word to be summed.
     *  @param  N   Number of words to be summed.
     *  @return Sum of specified data vector.
     */
    virtual complex_type CSum( size_type inx = 0, size_type N = 0 ) const = 0;

    /**  Calculate the inner (dot) product of two DVectors. 
     *        The inner product is defined as the sum of the products of
     *        the corresponding elements, \e i.e. 
     *        \f$ X \cdot Y = \Sigma X_i \cdot Y_i \f$.
     *        If the right hand side operand is complex, it is conjugated 
     *        before the multiplication.
     *  @brief Inner product.
     *  @param  rhs DVector to be multiplied by the other.
     *  @return The dot product of two %DVectors.
     */
    virtual double operator*( const DVector& rhs ) const;

    /**  The data from the rhs %DVector is converted if necessary and copied 
     *  to the lhs %DVector. The original data in the lhs %DVector are lost.
     *  Note that shallow copies are used for data vectors of the same type
     *  and are safe for copying a %DVector to itself.
     *  @brief  Assignment operator.
     *  @param rhs Constant %DVector whose value is copied.
     *  @return Reference to the modified %DVector.
     */
    virtual DVector& operator=( const DVector& rhs ) = 0;

    /**  The data from the rhs %DVector is converted and compared to the
     *  lhs %DVector. The original data are untouched.
     *  @brief  Comparison operator.
     *  @param rhs Constant %DVector whose value is copied.
     *  @return reference to the modified %DVector.
     */
    virtual bool operator==( const DVector& rhs ) const = 0;

    /**  The data in the rhs %DVector are added to the data of the lhs 
     *        %DVector on an element by element basis.
     *  @brief Add a %DVector.
     *  @param rhs %DVector whose data are added.
     *  @return reference to the modified %DVector.
     */
    virtual DVector& operator+=( const DVector& rhs );

    /**  The data in the rhs %DVector are subtracted from the data of 
     *        the lhs %DVector on an element by element basis.
     *  @brief Subtract a %DVector.
     *  @param rhs %DVector whose data are subtracted.
     *  @return reference to the modified %DVector.
     */
    virtual DVector& operator-=( const DVector& rhs );

    /**  The right hand side (rhs) constant is added to each element of 
     *   the left hand side %DVector.
     *  @brief Bias a %DVector.
     *  @param rhs Scalar constant to be added to each element.
     *  @return  reference to the resulting %DVector.
     */
    virtual DVector& operator+=( math_type rhs );

    /**  The right hand side constant (rhs) is subtracted to each element 
     *   of the left hand side %DVector.
     *  @brief Bias a %DVector (negative).
     *  @param rhs Scalar constant to be subtracted from each element.
     *  @return  reference to the resulting %DVector.
     */
    virtual DVector& operator-=( math_type rhs );

    /**  The complex right hand side (rhs) constant is added to each element of 
     *  the left hand side %DVector. The real-part of the constant is added
     *  if the vector is real.
     *  @brief Bias a %DVector.
     *  @param rhs Scalar constant to be added to each element.
     *  @return  reference to the resulting %DVector.
     */
    virtual DVector& operator+=( complex_type rhs );

    /**  Each element of the lhs %DVector is multiplied by the rhs constant.
     *  @brief Scale a %DVector.
     *  @param rhs Scalar constant each element is multiplied by.
     *  @return reference to the resulting %DVector.
     */
    virtual DVector& operator*=( math_type rhs );

    /**  Each element of the lhs %DVector is multiplied by a complex constant.
     *  The data type of the DVector remains the same. If a real vector is
     *  scaled by a complex number, only the real part of the scale factor 
     *  is used.
     *  \brief Scale a %DVector.
     *  \param rhs Scalar constant each element is multiplied by.
     *  \return reference to the resulting %DVector.
     */
    virtual DVector& operator*=( complex_type rhs );

    /**  Each element of the lhs %DVector is divided by the rhs constant.
     *  @brief Scale a %DVector (divide).
     *  @param rhs Scalar constant each element is divided by.
     *  @return reference to the resulting %DVector.
     */
    virtual DVector& operator/=( math_type rhs );

    /**  Each element of the lhs %DVector is multiplied by the corresponding 
     *  element of rhs.
     *  @brief Element by element multiplication of %DVector.
     *  @param rhs Multiplier %DVector.
     *  @return Reference to the resulting %DVector.
     */
    virtual DVector& operator*=( const DVector& rhs );

    /**  Each element of the lhs DVector is divided by the corresponding 
     *  element of rhs.
     *  @brief Divide by a %DVector.
     *  @param rhs Denominator %DVector.
     *  @return Reference to the resulting %DVector.
     */
    virtual DVector& operator/=( const DVector& rhs );

    /**  A formatted dump of the %DVector contents is written to a 
     *        specified stream.
     *  @brief Dump the vector contents.
     *  @param out Output stream that is to receive the formatted dump.
     *  @return Reference to the output stream.
     */
    virtual std::ostream& Dump( std::ostream& out ) const = 0;

    //---------------------------------------  Mutators
    /**  Data from a short integer array are converted to the %DVector 
     *       type and appended to the data vector.
     *  @brief  Append short integer  data to a vector.
     *  @param N    Number of data words to append to the %DVector.
     *  @param data short integer data array.
     */
    void Append( size_type N, const short* data );

    /**  Data from an integer array are converted to the %DVector 
     *         type and appended to the data vector.
     *  @brief  Append integer data to a vector.
     *  @param N    Number of data words to append to the %DVector.
     *  @param data integer data to be appended to the %DVector.
     */
    void Append( size_type N, const int* data );

    /**  Data from an unsigned integer array are converted to the %DVector 
     *         type and appended to the data vector.
     *  @brief  Append unsigned integer data to a vector.
     *  @param N    Number of data words to append to the %DVector.
     *  @param data integer data to be appended to the %DVector.
     */
    void Append( size_type N, const uint32_t* data );

    /**  Data from a float array are converted to the %DVector 
     *         type and appended to the data vector.
     *  @brief  Append float data to a vector.
     *  @param N    Number of data words to append to the %DVector.
     *  @param data float data to be appended to the %DVector.
     */
    void Append( size_type N, const float* data );

    /**  Data from a double float array are converted to the %DVector 
     *         type and appended to the data vector.
     *  @brief  Append double float data to a vector.
     *  @param N    Number of data words to append to the %DVector.
     *  @param data double float data to be appended to the %DVector.
     */
    void Append( size_type N, const double* data );

    /**  Data from a complex array are converted to the %DVector 
     *         type and appended to the data vector.
     *  @brief  Append complex data to a vector.
     *  @param N    Number of data words to append to the %DVector.
     *  @param data complex data to be appended to the %DVector.
     */
    void Append( size_type N, const fComplex* data );

    /**  Data from a complex array are converted to the %DVector 
     *         type and appended to the data vector.
     *  @brief  Append complex data to a vector.
     *  @param N    Number of data words to append to the %DVector.
     *  @param data complex data to be appended to the %DVector.
     */
    void Append( size_type N, const dComplex* data );

    /**    Data from the specified %DVector are appended to the object 
     *          vector. The appended data will be converted to the object 
     *          type if necessary.
     *  @brief   Append a %DVector to a %DVector.
     *  @param  data %DVector containing data to be appended.
     */
    void Append( const DVector& data );

    /**  The current data length is set to zero. No allocated memory is 
     *        released.
     *  @brief Clear a %DVector.
     */
    void
    Clear( void )
    {
        Erase( 0, getLength( ) );
    }

    /**  Replace each element of the vector with it's complex conjugate.
     *  @brief Conjugate a %DVector.
     */
    virtual void Conjugate( void ) = 0;

    /**  The data vector is extended to the specified length. New data words
     *  are filled with zeros. If the current data length is greater than the
     *  requested length, no action is taken (i.e. the vector is not shortened).
     *  Use ReSize to reduce the vector length or to extend the vector with
     *  uninitialized data.
     *  @brief Extend a %DVector.
     *  @param N New size for the data vector.
     */
    virtual void Extend( size_type N );

    /** reverse the data.
     */
    virtual DVector& reverse( void ) = 0;

    /** interpolate
     */
    virtual DVector*
    interpolate( size_type pos, size_type N, size_type delta ) const = 0;

public:
    /**  Translate DVType code to a name string.
     *  \param typ A data type code.
     *  \return Constant pointer to the data type name string.
     */
    static const char* dvtype_name( DVType typ );

    /**  Translate DVType code to a name string.
     *  \param typ A data type code.
     *  \return Constant pointer to the data type name string.
     */
    static DVType dvtype_code( const std::string& name );
};

//======================================  inline DVector methods
inline DVector&
DVector::replace( size_type inx, size_type N, const DVector& v )
{
    return replace( inx, N, v, 0, v.getLength( ) );
}

inline void
DVector::Append( const DVector& data )
{
    replace( getLength( ), 0, data );
}

inline DVector&
DVector::insert( size_type inx, const DVector& v, size_type inx2, size_type N )
{
    return replace( inx, 0, v, inx2, N );
}

inline double
DVector::operator*( const DVector& rhs ) const
{
    return dot( 0, rhs, 0, getLength( ) );
}

inline DVector&
DVector::operator+=( const DVector& rhs )
{
    return add( 0, rhs, 0, getLength( ) );
}

inline DVector&
DVector::operator-=( const DVector& rhs )
{
    return sub( 0, rhs, 0, getLength( ) );
}

inline DVector&
DVector::operator+=( math_type rhs )
{
    return bias( 0, rhs, getLength( ) );
}

inline DVector&
DVector::operator-=( math_type rhs )
{
    return operator+=( -rhs );
}

inline DVector&
DVector::operator+=( complex_type rhs )
{
    return bias( 0, rhs, getLength( ) );
}

inline DVector&
DVector::operator*=( math_type rhs )
{
    return scale( 0, rhs, getLength( ) );
}

inline DVector&
DVector::operator*=( complex_type rhs )
{
    return scale( 0, rhs, getLength( ) );
}

inline DVector&
DVector::operator/=( math_type rhs )
{
    return operator*=( 1.0 / rhs );
}

inline DVector&
DVector::operator*=( const DVector& rhs )
{
    return mpy( 0, rhs, 0, getLength( ) );
}

inline DVector&
DVector::operator/=( const DVector& rhs )
{
    return div( 0, rhs, 0, getLength( ) );
}

#endif // !def(DVECTOR_HH)
