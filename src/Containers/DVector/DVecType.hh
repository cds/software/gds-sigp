/* -*- mode: c++; c-basic-offset: 3; -*- */
//
//   DVector class definition.  Copy on write version.
//
#ifndef DVECTYPE_HH
#define DVECTYPE_HH

#include "DVector.hh"
#include "CWVec.hh"
#include <memory>

/**  The typed data vector template describes the classes based on 
  *  DVector which contain vectors of variously typed data.
  *  @brief Typed data vector class template.
  *  @author John Zweizig (john.zweizig@ligo.org)
  *  @version $Id$
  */
template < class T >
class DVecType : public DVector
{
public:
    using DVector::Extract;

    ///  Data type used internally for size and index variables.
    typedef DVector::size_type size_type;

    ///  math data type
    typedef DVector::math_type math_type;

    ///  complex data type
    typedef DVector::complex_type complex_type;

public:
    /**  Empty vector constructor.
    */
    DVecType( void );

    /**  Create a DVector with the same length and data as the 
    *  argument.
    *  @brief Copy constructor.
    *  @param dv %DVector with data to translate or copy into the new vector.
    */
    explicit DVecType( const DVector& dv );

    /**  Create a typed data vector with the same data as the argument, but 
    *  a different length. This constructor translates data if the argument
    *  DVector is a different type, or performs a shallow copy if the input 
    *  data are the same type.
    *  @brief Construct a vector with specified data. 
    *  @param dv  %DVector from which data are to be copied.
    *  @param len Number of words of data to be copied to the new vector
    */
    DVecType( const DVecType& dv, size_type len );

    /**  A data vector is constructed with the specified size. If an 
    *  initialization array is specified, the data are copied to the 
    *  vector. The current length is set to 'len'. If \a data is NULL the
    *  vector is allocated but not initialized.
    *  @brief Data constructor.
    *  @param len  Vector length in data words.
    *  @param data Optional pointer to initial data.
    */
    explicit DVecType( size_type len, const T* data = 0 );

    /**  A data vector is constructed from a specified sub-string of a 
    *  CWVec.
    *  @param cv CWVec containing data to be extracted.
    *  @param i  Start position of data.
    *  @param l  Length of data to be inserted.
    */
    explicit DVecType( const CWVec< T >& cv, size_type i = 0, size_type l = 0 );

    /**  Delete a typed DVector.
    *  @brief Destructor.
    */
    ~DVecType( );

    /**  Clone a typed DVector.
    */
    DVecType* clone( void ) const;

    /**  Allocate or expand the vector to make room for 'len' words.
    *  Existing data are preserved, but no post padding is performed. 
    *  The current data length is set to 'len'. No write access is 
    *  provided to the data. Use Extend() to pad the vector to the 
    *  specified length. 
    *  @brief  Resize the data vector.
    *  @param len Minimum data vector size.
    */
    void ReSize( size_type len );

    /**  Insure that at least the specified number of data words are available.
    *  @brief  Reserve the specified number of data words.
    *  @param len Number of valid data words.
    */
    void reserve( size_type len );

    /**  Return the capacity of the data vector in data words.
    *  @brief Get the vector capacity
    *  @return Number of words that can fit in the vector.
    */
    size_type capacity( void ) const;

    /**  Test whether the underlying copy-on-write data vector is shared with
    *  another DVecType object.
    *  @brief Test for shared data vector.
    *  @return true if data vector is shared.
    */
    bool shared( void ) const;

    /**  Replace each element with its complex conjugate.
    *  @brief  Complex conjugate.
    */
    void Conjugate( void );

    /**  A sub-string is removed from the vector.
    *  @brief Erase a sub-string from a vector.
    *  @param inx Index of first element to be removed.
    *  @param len Number of elements to be removed.
    */
    void Erase( size_type inx, size_type len );

    /**  A new vector is generated from a sub-string of the vector data.
    *  @note The new vector must be deleted with \c delete.
    *  @brief Extract a sub-string from a vector.
    *  @param inx Index of first element to be extracted.
    *  @param len Number of elements to be extracted.
    *  @return pointer to the new vector.
    */
    DVecType* Extract( size_type inx, size_type len ) const;

    /**  A new vector is generated from a decimated sub-string of the vector 
    *  data. If no data are contained in the specified sub-string (\e e.g. 
    *  if \a inx is not less than the current length or the increment is 
    *  zero) an empty vector is returned.
    *  @note The new vector must be deleted with \c delete.
    *  @brief Extract and decimate a sub-string from a vector.
    *  @param inx Index of the first element to be extracted.
    *  @param len Number of words to extract.
    *  @param inc Increment between successive elements to extract.
    *  @return Pointer to the new vector.
    */
    DVecType* Extract( size_type inx, size_type len, size_type inc ) const;

    /**  Force unshared ownership of a data vector. If the data vector is 
    *  not shared with any other data vector, no further processing is 
    *  performed. Of the data vector is shared, a copy of the vector is
    *  made and the original is released by.
    *  \brief Force unshared ownership.
    */
    virtual void force_copy( void );

    /**  Return true if all elements of the vector are finite, i.e. not inf
    *  or NaN.
    *  @brief Test for finite data.
    *  @return true if all elements are finite.
    */
    bool finite( void ) const;

    /**  Return true if all elements of the vector are normalized, i.e. not
    *  subnormal, inf or NaN.
    *  @brief Test for normalized data.
    *  @return true if all elements are normalized.
    */
    bool normal( void ) const;

    /**  Returns the number of valid data words in the Data Vector.
    *  @brief Get the data length.
    *  @return The number of valid data words.
    */
    size_type getLength( void ) const;

    /**  getSize return the size of a single data word in bytes.
    *  @brief Get data word size.
    *  @return Length of each data element in bytes.
    */
    DVector::size_type
    getSize( void ) const
    {
        return sizeof( T );
    }

    /**  Returns the data type as represented by the DVect::DVType 
    *  enumerator.
    *  @brief Get the data type.
    *  @return Data type of this data vector.
    */
    inline DVType getType( void ) const;

    /**  Returns the data type of the DVecType class as represented by the 
    *  DVect::DVType enumerator.
    *  @brief Get the data type.
    *  @return Data type contained by this %DVecType.
    */
    static DVType getDataType( void );

    /**  Returns a (void*) pointer to the start of the internal array.
    *  @note The returned pointer may be invalidated by any non-constant
    *  reference to the %DVecType data vector.
    *  @brief Get a pointer to the internal data array.
    *  @return Untyped pointer to the start of the internal data array.
    */
    void*
    refData( void )
    {
        return (void*)refTData( );
    }

    /**  Returns a <tt>const void*</tt> pointer to the start of the internal 
    *  data array storage.
    *  @brief Get a constant pointer to the internal data array.
    *  @note The returned pointer may be invalidated by any non-constant
    *  reference to the %DVecType data vector.
    *  @return Untyped constant pointer to the data array.
    */
    const void*
    refData( void ) const
    {
        return (const void*)refTData( );
    }

    /**  Returns a writable pointer to the start of internal array.
    *  @brief Get a write-able data pointer.
    *  @note The returned pointer may be invalidated by any non-constant
    *  reference to the %DVecType data vector.
    *  @return Write-able pointer to the start of the data array.
    */
    T* refTData( void );

    /**  Returns a writable pointer to the start of internal array.
    *  @brief Get a constant data pointer.
    *  @note The returned pointer may be invalidated by any non-constant
    *  reference to the %DVecType data vector.
    *  @return Constant pointer to the start of the data array.
    */
    const T* refTData( void ) const;

    /**  The specified sub-string of the data vector are copied to a short 
    *  integer array (\a data ) and the number of words copied is returned. 
    *  The data will be converted to short integers if the %DVector data are 
    *  of a different type. If \a len is greater then the vector length 
    *  the copy terminates at the vector end.
    *  @brief Copy data vector to a \c short integer array.
    *  @param inx  Index of first word to be copied.
    *  @param len  Number of words to be copied.
    *  @param data Array of short integers to receive data.
    *  @return Number of data words copied.
    */
    size_type getData( size_type inx, size_type len, short data[] ) const;

    /**  The specified sub-string of the data vector are copied to an 
    *  integer array (\a data ) and the number of words copied is returned. 
    *  The data will be converted to integers if the %DVector data are 
    *  of a different type. If \a len is greater then the vector length 
    *  the copy terminates at the vector end.
    *  @brief Copy data vector to an \c int array.
    *  @param inx  Index of first word to be copied.
    *  @param len  Number of words to be copied.
    *  @param data Array of integers to receive data.
    *  @return Number of data words copied.
    */
    size_type getData( size_type inx, size_type len, int data[] ) const;

    /**  The specified sub-string of the data vector are copied to an unsigned
    *  integer array (\a data ) and the number of words copied is returned. 
    *  The data will be converted to integers if the %DVector data are 
    *  of a different type. If \a len is greater then the vector length 
    *  the copy terminates at the vector end.
    *  @brief Copy data vector to an \c int array.
    *  @param inx  Index of first word to be copied.
    *  @param len  Number of words to be copied.
    *  @param data Array of integers to receive data.
    *  @return Number of data words copied.
    */
    size_type getData( size_type inx, size_type len, uint32_t data[] ) const;

    /**  The specified sub-string of the data vector are copied to a float
    *  array (\a data ) and the number of words copied is returned. 
    *  The data will be converted to floats if the %DVector data are 
    *  of a different type. If \a len is greater then the vector length 
    *  the copy terminates at the vector end.
    *  @brief Copy data vector to a \c float array.
    *  @param inx  Index of first word to be copied.
    *  @param len  Number of words to be copied.
    *  @param data Array of floats to receive data.
    *  @return Number of data words copied.
    */
    size_type getData( size_type inx, size_type len, float data[] ) const;

    /**  The specified sub-string of the data vector are copied to a double 
    *  array (\a data ) and the number of words copied is returned. 
    *  The data will be converted to doubles if the %DVector data are 
    *  of a different type. If \a len is greater then the vector length 
    *  the copy terminates at the vector end.
    *  @brief Copy data vector to a \c double array.
    *  @param inx  Index of first word to be copied.
    *  @param len  Number of words to be copied.
    *  @param data Array of doubles to receive data.
    *  @return Number of data words copied.
    */
    size_type getData( size_type inx, size_type len, double data[] ) const;

    /**  The specified sub-string of the data vector are copied to a complex
    *  float array (\a data ) and the number of words copied is returned. 
    *  The data will be converted to complex float if the %DVector data are 
    *  of a different type. If \a len is greater then the vector length 
    *  the copy terminates at the vector end.
    *  @brief Copy data vector to a \c fComplex array.
    *  @param inx  Index of first word to be copied.
    *  @param len  Number of words to be copied.
    *  @param data Array of complex floats to receive data.
    *  @return Number of data words copied.
    */
    size_type getData( size_type inx, size_type len, fComplex data[] ) const;

    /**  The specified sub-string of the data vector are copied to a complex
    *  double array (\a data ) and the number of words copied is returned. 
    *  The data will be converted to complex doubles if the %DVector data  
    *  are of a different type. If \a len is greater then the vector length 
    *  the copy terminates at the vector end.
    *  @brief Copy data vector to a \c dComplex array.
    *  @param inx  Index of first word to be copied.
    *  @param len  Number of words to be copied.
    *  @param data Array of complex doubles to receive data.
    *  @return Number of data words copied.
    */
    size_type getData( size_type inx, size_type len, dComplex data[] ) const;

    /**  A data word is fetched from the %DVector and converted to a short
    *  integer.
    *  @brief Get one data word.
    *  @param inx Index of the word to be fetched.
    *  @return Indexed data word converted to a short integer.
    */
    short getShort( size_type inx ) const;

    /**  A data word is fetched from the %DVector and converted to an integer.
    *  @brief Get one data word.
    *  @param inx Index of the word to be fetched.
    *  @return Indexed data word converted to an integer.
    */
    int getInt( size_type inx ) const;

    /**  A data word is fetched from the %DVector and converted to a long integer.
    *  @brief Get one data word.
    *  @param inx Index of the word to be fetched.
    *  @return Indexed data word converted to a long integer.
    */
    long getLong( size_type inx ) const;

    /**  A data word is fetched from the %DVector and converted to an unsigned
    *  integer. An exception is thrown if the number to be converted is 
    *  negative. The real part of complex numbers is converted. 
    *  @brief Get one data word.
    *  @param inx Index of the word to be fetched.
    *  @return Indexed data word converted to an unsigned integer.
    */
    uint32_t getUInt( size_type inx ) const;

    /**  A data word is fetched from the %DVector and converted to a float.
    *  @brief Get one data word.
    *  @param inx Index of the word to be fetched.
    *  @return Indexed data word converted to a float.
    */
    float getFloat( size_type inx ) const;

    /**  A data word is fetched from the %DVector and converted to a double.
    *  @brief Get one data word.
    *  @param inx Index of the word to be fetched.
    *  @return Indexed data word converted to a double.
    */
    double getDouble( size_type inx ) const;

    /**  A data word is fetched from the %DVector and converted to a complex
    *   float.
    *  @brief Get one data word.
    *  @param inx Index of the word to be fetched.
    *  @return Indexed data word converted to a \c fComplex.
    */
    fComplex getCplx( size_type inx ) const;

    /**  A data word is fetched from the %DVector and converted to a double
    *  complex data type.
    *  @brief Get one data word.
    *  @param inx Index of the word to be fetched.
    *  @return Indexed data word converted to a \c dComplex.
    */
    dComplex getDCplx( size_type inx ) const;

    /**  Return the largest entry. If the vector is complex, the largest 
    *  real part is returned.
    *  @brief  Maximum data value.
    *  @return Largest data value.
    */
    double getMaximum( void ) const;

    /**  Return the smallest entry. If the vector is complex, the smallest 
    *  real part is returned.
    *  @brief  Minimum data value.
    *  @return Smallest data value.
    */
    double getMinimum( void ) const;

    /**  Count the number of entries with value greater than the specified 
    *  limit. If the vector is complex, the real part is compared.
    *  @brief   Number of entries in range.
    *  @param  low  Lower limit of entries to be counted.
    *  @param  high Upper limit of entries to be counted.
    *  @return Number of entries greater than limit.
    */
    size_type getNBetween( double low, double high ) const;

    /**  Count the number of entries with value greater than the specified 
    *          limit. If the vector is complex, the real part is compared.
    *  @brief   Number of entries greater than limit.
    *  @param  limit Lower limit of entries to be counted.
    *  @return Number of entries greater than limit.
    */
    size_type getNGreater( double limit ) const;

    /**  Count the number of entries with value less than the specified 
    *          limit. If the vector is complex, the real part is compared.
    *  @brief   Number of entries less than limit.
    *  @param  limit Upper limit of entries to be counted.
    *  @return Number of entries less than limit.
    */
    size_type getNLess( double limit ) const;

    /**  Replace \a N values starting at \a inx of the current 
    *  vector with \a Nv values from the argument vector (\a v)starting at 
    *  index \a inv. 
    *  \note If the substring \c v[inv:inv+nv] does not exist, the 
    *  corresponding data will be left uninitialized
    *  @brief Inset data from a vector into the current vector.
    *  @param inx Index of the insertion point of the current vector.
    *  @param N   Number of words to be inserted
    *  @param v   Vector containing data to be inserted.
    *  @param inv index in v of the data to be inserted.
    *  @param Nv  Number of words to be inserted
    *  @return Reference to the result vector.
    */
    DVecType& replace( size_type      inx,
                       size_type      N,
                       const DVector& v,
                       size_type      inv,
                       size_type      Nv );

    /**  Replace \a N values starting at \a inx of the current 
    *  vector with the first \a Nv words of vector \a v.
    *  @brief Inset data from a vector into the current vector.
    *  @param inx Index of the insertion point of the current vector.
    *  @param N   Number of words to be replaced
    *  @param v   Data vector to be inserted.
    *  @param Nv  Number of words to be inserted
    *  @return Reference to the result vector.
    */
    DVecType& replace( size_type inx, size_type N, T v, size_type Nv );

    /**  Replace a specified field in a vector with a specified number of
    *  zero words.
    *  @brief Replace or insert zeros into a vector.
    *  @param inx Index of first word to replace
    *  @param len Number of words to replace.
    *  @param nzer Number of zeroes to insert.
    */
    DVecType&
    replace_with_zeros( size_type inx, size_type len, size_type nzer );

    using DVector::reverse;

    /**  Replace/insert \a N values starting at \a inx of the 
    *  current vector with the specified data in reverse order.
    *  @brief Inset data from a vector into the current vector.
    *  @param inx Index of the insertion point of the current vector.
    *  @param v   Data to be inserted in reverse order.
    *  @param N   Number of words to be inserted
    *  @return Reference to the result vector.
    */
    DVecType& reverse( size_type inx, const T* v, size_type N );

    /**  Replace all elements of the current vector with the data in reverse 
    *  order.
    *  @brief reverse data order in the current vector.
    *  @return Reference to the result vector.
    */
    DVecType& reverse( void );

    /**  Add \a N values in the addend vector to the current vector 
    *  starting at index \a inx of the current vector and 
    *  \a inx2 of the addend vector.
    *  @brief Add two vectors
    *  @param inx  Index of the first element in this vector to be added.
    *  @param v    Vector contining data to be added
    *  @param inx2 index in v of the first word to be added to this vector.
    *  @param N    Number of words to be added
    *  @return Reference to the result vector.
    */
    DVecType&
    add( size_type inx, const DVector& v, size_type inx2 = 0, size_type N = 0 );

    /**  Add a constant to \a N values of the current vector starting at
    *  index \a inx.
    *  @brief Add a constant to a vector
    *  @param inx  Index of the first element in this vector to be added.
    *  @param x    Constant to be added
    *  @param N    Number of words to be added
    *  @return Reference to the result vector.
    */
    DVecType& bias( size_type inx, math_type x, size_type N = 0 );

    /**  Add a constant to \a N values of the current vector starting at
    *  index \a inx.
    *  @brief Add a constant to a vector
    *  @param inx  Index of the first element in this vector to be added.
    *  @param x    Constant to be added
    *  @param N    Number of words to be added
    *  @return Reference to the result vector.
    */
    DVecType& bias( size_type inx, complex_type x, size_type N = 0 );

    /**  Subtract \a N values in the subtrahend vector from the current 
    *  vector starting at index \a inx of the current vector and 
    *  \a inx2 of the subtrahend vector.
    *  @brief Subtract two vectors
    *  @param inx  Index of the first element in this vector to be subtracted.
    *  @param v    Vector to be subtracted
    *  @param inx2 index in the subtrahend vector
    *  @param N    Number of words to be subtracted
    *  @return Reference to the result vector.
    */
    DVecType&
    sub( size_type inx, const DVector& v, size_type inx2 = 0, size_type N = 0 );

    /**  Multiply \a N values starting at index \a inx of the 
    *  current vector by the values in the multiplier vector starting at 
    *  index \a inx2.
    *  @brief Multiply two vectors
    *  @param inx  Index of the first element in this vector to be multiplied.
    *  @param v    Vector to be multiplied
    *  @param inx2 index of first word in the current vector
    *  @param N    Number of words to be multiplied.
    *  @return Reference to the result vector.
    */
    DVecType&
    mpy( size_type inx, const DVector& v, size_type inx2 = 0, size_type N = 0 );

    /**  Multiply \a N values starting at index \a inx of the 
    *  current vector by the cc values in the multiplier vector starting 
    *  at index \a inx2.
    *  @brief Multiply a vector by the complex conjugate of another.
    *  @param inx  Index of the first element in this vector to be multiplied.
    *  @param v    Vector to be multiplied
    *  @param inx2 index of first word in the current vector
    *  @param N    Number of words to be multiplied.
    *  @return Reference to the result vector.
    */
    DVecType& cmpy( size_type      inx,
                    const DVector& v,
                    size_type      inx2 = 0,
                    size_type      N = 0 );

    /**  Multiply \a N values of the current vector starting at 
    *  index \a inx by a constant.
    *  @brief Multiply a vector by a constant
    *  @param inx  Index of the first element in this vector to be added.
    *  @param x    Constant to be added
    *  @param N    Number of words to be added
    *  @return Reference to the result vector.
    */
    DVecType& scale( size_type inx, math_type x, size_type N = 0 );

    /**  Multiply \a N values of the current vector starting at 
    *  index \a inx by a constant.
    *  @brief Multiply a vector by a complex constant
    *  @param inx  Index of the first element in this vector to be added.
    *  @param x    Constant to be added
    *  @param N    Number of words to be added
    *  @return Reference to the result vector.
    */
    DVecType& scale( size_type inx, complex_type x, size_type N = 0 );

    /**  Calculate the vector inner product between two sub-strings.
    *  @brief Vector inner product
    *  @param inx  Index of the first element in this vector to be multiplied.
    *  @param v    Vector to be multiplied
    *  @param inx2 index of first word in the current vector
    *  @param N    Number of words to be multiplied.
    *  @return Reference to the result vector.
    */
    math_type dot( size_type      inx,
                   const DVector& v,
                   size_type      inx2 = 0,
                   size_type      N = 0 ) const;

    /**  Calculate the complex inner product between two vector sub-strings.
    *  @brief Divide two vectors
    *  @param inx  Index of the first element in this vector to be multiplied.
    *  @param v    Vector to be multiplied
    *  @param inx2 index of first word in the current vector
    *  @param N    Number of words to be multiplied.
    *  @return Reference to the result vector.
    */
    complex_type cdot( size_type      inx,
                       const DVector& v,
                       size_type      inx2 = 0,
                       size_type      N = 0 ) const;

    /**  Divide \a N values starting at index \a inx of the 
    *  current vector by the values in the divisor vector starting at 
    *  index \a inx2.
    *  @brief Divide two vectors
    *  @param inx  Index of the first element in this vector to be multiplied.
    *  @param v    Vector to be multiplied
    *  @param inx2 index of first word in the current vector
    *  @param N    Number of words to be multiplied.
    *  @return Reference to the result vector.
    */
    DVecType&
    div( size_type inx, const DVector& v, size_type inx2 = 0, size_type N = 0 );

    /**  The elements of a specified sub-vector are converted to complex 
    *  and summed.
    *  @brief Get the sum of a complex sub-vector.
    *  @return The sum of the specified elements.
    *  @param inx The first element to be summed.
    *  @param N   The number of elements to be summed.
    */
    complex_type CSum( size_type inx, size_type N ) const;

    /**  The elements of a specified sub-vector are converted to double 
    *  and summed.
    *  @brief Get the sum of a sub-vector.
    *  @return The sum of the specified elements.
    *  @param inx The first element to be summed.
    *  @param N   The number of elements to be summed.
    */
    math_type VSum( size_type inx, size_type N ) const;

    /**  Perform a shallow copy of data from the argument %DVecType of the 
    *  same type. Note that definition of this  method is necessary to 
    *  prevent the compiler from using an automatically generated assignment 
    *  method which will run into trouble with DVector::operator=().
    *  @brief Assignment operator.
    *  @param rhs Constant vector to be copied.
    *  @return Reference to the modified %DVecType
    */
    virtual DVecType& operator=( const DVecType& rhs );

    /**  Data from the rhs %DVector are converted to this %DVector type
    *  and copied to the lhs vector.
    *  @brief Assignment operator.
    *  @param rhs Constant vector to be copied.
    *  @return Reference to the modified %DVecType
    */
    virtual DVecType& operator=( const DVector& rhs );

    /**  Data from the \a rhs %DVector are converted to this %DVector type
    *  and compared to this %DVector.
    *  @brief Assignment operator.
    *  @param rhs Data vector to be converted and compared.
    *  @return True if the data vector contents are equal.
    */
    bool operator==( const DVector& rhs ) const;

    /**  A reference to the specified vector element is returned.
    *  @brief   Reference a Vector element.
    *  @return Reference a Vector element.
    *  @param  inx Index of element to be accessed.
    *  @return Reference to the specified value.
    */
    T& operator[]( size_type inx );

    /**  A constant reference to the vector element is returned.
    *        Constant and non-constant references are implemented.
    *  @brief Reference a Vector element.
    *  @param inx Index of element to be accessed.
    *  @return Constant reference to the specified value.
    */
    const T& operator[]( size_type inx ) const;

    /**  The data in the %DVecType vector are formatted and copied to the 
    *  specified standard output stream.
    *  @brief Print the data.
    *  @param out Standard output stream to which the data will be dumped.
    *  @return Reference to the output stream.
    */
    std::ostream& Dump( std::ostream& out ) const;

    /**  interpolate.
    */
    DVecType* interpolate( size_type pos, size_type N, size_type delta ) const;

private:
    /**  Convert and copy the data substring from an argument DVector to a
     *  local array with the type of this DVector. The local array is deleted 
     *  automatically when it goes out of scope.
     *  \brief Make a local copy of an argument vector.
     *  \param v   Argument vector to be copied.
     *  \param inx index of substring start in v.
     *  \param N   length of substring.
     *  \returns   unique_ptr to the local copy.
     */
    std::unique_ptr< T[] >
    arg_data( const DVector& v, size_type inx, size_type N ) const;

private:
    /**  Pointer to a data array of the appropriate type. The array is 
    *        allocated by and owned by the DVecType object.
    *  @brief Pointer to allocated vector
    */
    CWVec< T > mData;
};

//======================================  inline DVType method templates
#ifndef __CINT__

template < class T >
inline DVecType< T >::DVecType( const CWVec< T >& cv, size_type i, size_type l )
    : mData( cv )
{
    mData.substr( i, l );
}

template < class T >
inline typename DVecType< T >::size_type
DVecType< T >::getLength( void ) const
{
    return mData.size( );
}

template < class T >
inline DVecType< T >::DVecType( void )
{
}

template < class T >
inline DVecType< T >&
DVecType< T >::operator=( const DVecType& dv )
{
    mData = dv.mData;
    return *this;
}

//--------------------------------------  Non constant references.
template < class T >
inline T*
DVecType< T >::refTData( void )
{
    return mData.ref( );
}

template < class T >
inline const T*
DVecType< T >::refTData( void ) const
{
    return mData.ref( );
}

template < class T >
inline bool
DVecType< T >::shared( void ) const
{
    return mData.shared( );
}

//------------------------------------  operator[] templates.
template < class T >
inline T&
DVecType< T >::operator[]( size_type inx )
{
    return mData[ inx ];
}

template < class T >
inline const T&
DVecType< T >::operator[]( size_type inx ) const
{
    return mData[ inx ];
}

template < class T >
inline typename DVecType< T >::size_type
DVecType< T >::capacity( void ) const
{
    return mData.capacity( );
}

template < class T >
inline DVecType< T >&
DVecType< T >::replace_with_zeros( size_type inx,
                                   size_type len,
                                   size_type nzer )
{
    return replace( inx, len, T( 0 ), nzer );
}

//======================================  Reserve a minimum data length.
template < class T >
inline void
DVecType< T >::reserve( size_type len )
{
    mData.reserve( len );
}

//======================================  Typed data retrieval templates
template <>
inline short
DVecType< fComplex >::getShort( size_type inx ) const
{
    return short( mData[ inx ].real( ) );
}

template <>
inline short
DVecType< dComplex >::getShort( size_type inx ) const
{
    return short( mData[ inx ].real( ) );
}

template < class T >
inline short
DVecType< T >::getShort( size_type inx ) const
{
    return short( mData[ inx ] );
}

template <>
inline int
DVecType< fComplex >::getInt( size_type inx ) const
{
    return int( mData[ inx ].real( ) );
}

template <>
inline int
DVecType< dComplex >::getInt( size_type inx ) const
{
    return int( mData[ inx ].real( ) );
}

template < class T >
inline int
DVecType< T >::getInt( size_type inx ) const
{
    return int( mData[ inx ] );
}

template <>
inline long
DVecType< fComplex >::getLong( size_type inx ) const
{
    return long( mData[ inx ].real( ) );
}

template <>
inline long
DVecType< dComplex >::getLong( size_type inx ) const
{
    return long( mData[ inx ].real( ) );
}

template < class T >
inline long
DVecType< T >::getLong( size_type inx ) const
{
    return long( mData[ inx ] );
}

template <>
inline float
DVecType< fComplex >::getFloat( size_type inx ) const
{
    return mData[ inx ].real( );
}

template <>
inline float
DVecType< dComplex >::getFloat( size_type inx ) const
{
    return mData[ inx ].real( );
}

template < class T >
inline float
DVecType< T >::getFloat( size_type inx ) const
{
    return float( mData[ inx ] );
}

template <>
inline double
DVecType< fComplex >::getDouble( size_type inx ) const
{
    return mData[ inx ].real( );
}

template <>
inline double
DVecType< dComplex >::getDouble( size_type inx ) const
{
    return mData[ inx ].real( );
}

template < class T >
inline double
DVecType< T >::getDouble( size_type inx ) const
{
    return double( mData[ inx ] );
}

template < class T >
inline fComplex
DVecType< T >::getCplx( size_type inx ) const
{
    return fComplex( mData[ inx ] );
}

template < class T >
inline dComplex
DVecType< T >::getDCplx( size_type inx ) const
{
    return dComplex( mData[ inx ] );
}

template < class T >
inline DVector::DVType
DVecType< T >::getType( void ) const
{
    return getDataType( );
}

#endif // !def(__CINT__)

typedef DVecType< short >    DVectS;
typedef DVecType< int >      DVectI;
typedef DVecType< uint32_t > DVectU;
typedef DVecType< float >    DVectF;
typedef DVecType< double >   DVectD;
typedef DVecType< fComplex > DVectC;
typedef DVecType< dComplex > DVectW;

#endif // !def(DVECTOR_HH)
