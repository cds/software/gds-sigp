/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "dv_function.hh"
#include "gds_veclib.hh"

//======================================  Get modulus squared of a DVector
DVectD
dv_modsq( const DVector& dv )
{
    DVector::size_type len = dv.getLength( );
    DVectD             dvd( len );

    //----------------------------------  Double complex vectors.
    if ( dv.W_data( ) )
    {
        const dComplex* dvw =
            reinterpret_cast< const dComplex* >( dv.refData( ) );
        //for (DVector::size_type i=0; i<len; ++i) dvd[i] = norm(dvw[i]);
        vcmodsq( dvw, dvd.refTData( ), len );
    }

    //----------------------------------  Single complex vectors.
    else if ( dv.C_data( ) )
    {
        const DVectC& dvc = dynamic_cast< const DVectC& >( dv );
        for ( DVector::size_type i = 0; i < len; ++i )
            dvd[ i ] = norm( dvc[ i ] );
    }

    //----------------------------------  Real data types.
    else
    {
        dvd = dv;
        dvd *= dvd;
    }

    //----------------------------------  Return result
    return dvd;
}

//======================================  Get argument of a complex DVector
DVectD
dv_argument( const DVector& dv )
{
    DVector::size_type len = dv.getLength( );
    DVectD             dvd( len );

    //----------------------------------  Double complex vectors.
    if ( dv.W_data( ) )
    {
        const DVectW& dvw = dynamic_cast< const DVectW& >( dv );
        for ( DVector::size_type i = 0; i < len; ++i )
            dvd[ i ] = arg( dvw[ i ] );
    }

    //----------------------------------  Single complex vectors.
    else if ( dv.C_data( ) )
    {
        const DVectC& dvc = dynamic_cast< const DVectC& >( dv );
        for ( DVector::size_type i = 0; i < len; ++i )
            dvd[ i ] = arg( dvc[ i ] );
    }

    //----------------------------------  Rational data types Arg = 0.
    else
    {
        dvd.replace_with_zeros( 0, len, len );
    }

    //----------------------------------  Return result
    return dvd;
}

//======================================  Average successive real bins
DVectD
dv_average( const DVector& dv, size_t nAvg )
{
    size_t nOut = dv.size( ) / nAvg;
    DVectD out( nOut );
    for ( size_t i = 0; i < nOut; i++ )
    {
        out[ i ] = dv.VSum( i * nAvg, nAvg ) / double( nAvg );
    }
    return out;
}

//======================================  Average successive complex bins
DVectW
dv_cplx_avg( const DVector& dv, size_t nAvg )
{
    size_t nOut = dv.size( ) / nAvg;
    DVectW out( nOut );
    for ( size_t i = 0; i < nOut; i++ )
    {
        out[ i ] = dv.CSum( i * nAvg, nAvg ) / double( nAvg );
    }
    return out;
}
