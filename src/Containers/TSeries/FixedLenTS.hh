/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef FIXEDLENTS_HH
#define FIXEDLENTS_HH

#include "TSeries.hh"

/**  The fixed length time series is defined in
  *
  *  <tt> \#include "FixedLenTS.hh" </tt>
  *
  *  You set the maximum time either in the constructor, e.g.
  *
  *  <tt>FixedLenTS mHistory(7200.0);</tt>
  *
  *  Or explicitly 
  *
     <tt>FixedLenTS mHistory; <br>
     mHistory.setMaxLen(t);</tt>
  *
  *  You then append data like....
  *  
  *  \verbatim
      //----------------------------------  Keep the history
      Time t0 = fs.getStartTime();
      Interval dT = fs.getEndTime()-t0;
      mTimeTot += dT;
      float bandP = mBand;
      mHistory.fixedAppend(t0, dT, &bandP); \endverbatim
  *
  *  When it reaches the maximum it starts to delete data from the start,
  *  rather than increasing the length. The FixedLenTS is based on a TSeries 
  *  so you can reference it as you would a TSeries.
  *  \brief Fixed length time series
  *  \author J. Zweizig
  *  \version 2.0; Last modified June 7, 2020
  */
class FixedLenTS : public TSeries
{
public:
    /**  Default constructor creates a fixed length TSeries with zero length.
    *  A <tt>FixedLenTS</tt> created with this constructor is unusable until
    *  the maximum length has been set with <tt>setMaxLen()</tt>
    *  \brief Default constructor.
    */
    FixedLenTS( void );

    /**  Construct a  <tt>FixedLengthTS</tt> with a specified maximum length.
    *  \brief Data constructor.
    *  \param max maximum length for time series.
    */
    explicit FixedLenTS( Interval max );

    /**  Create a FixedLenTS that is an identical copy of the argument series.
    *  \brief Copy constructor.
    *  \param x %FixedLenTS series to be copied
    */
    FixedLenTS( const FixedLenTS& x );

    /**  Destroy the Fixed length time series.
    *  \brief Destructor.
    */
    inline virtual ~FixedLenTS( )
    {
    }

    /**  Append one or more float data words to the series. If the series 
    *  with the appended data is longer than the specified maximum length,
    *  data are removed from the beginning of the series and the start
    *  time is updated as appropriate.
    *  \brief Append float data.
    *  \param t0 Time of first datum.
    *  \param dT Time step between data.
    *  \param x  Pointer to one or more data.
    *  \param n  Number of data to be appended to the time series.
    *  \return
    *   <table>
    *    <tr><td>  0 </td><td> successful</td></tr>
    *    <tr><td> -1 </td><td> ts start time isn't end+N*dt </td></tr>
    *    <tr><td> -2 </td><td> sample intervals aren't equal.</td></tr>
    *   </table>
    */
    template < typename T >
    int fixedAppend( const Time& t0, Interval dT, const T* x, int n = 1 );

    /**  Append a TSeries to the fixed length series. If the resulting series 
    *  would be longer than the specified maximum length, data are removed 
    *  from the start of the current series and/or the start of the data to 
    *  be appended and the current series start time is updated as appropriate.
    *  \brief Append a TSeries.
    *  \param ts TSeries to be appended.
    *  \return
    *   <table>
    *    <tr><td>  0 </td><td> successful</td></tr>
    *    <tr><td> -1 </td><td> ts start time isn't end+N*dt </td></tr>
    *    <tr><td> -2 </td><td> sample intervals aren't equal.</td></tr>
    *   </table>
    */
    int fixedAppend( const TSeries& ts );

    /**  Append zeros to the series until the specified end-time.
    *  \brief Append zeroes until the specified time.
    *  \param t0 Time of first datum.
    *  \param dT Time step between data.
    */
    void padUntil( const Time& t0, Interval dT );

    /**  Set the maximum length for the fixed length time series.
    *  \brief specify the maximum length.
    *  \param dt Maximum length
    */
    void setMaxLen( Interval dt );

    /**  Get the Maximum length specified.
    *  \brief  Get the Maximum length.
    *  \return Maximum length.
    */
    Interval getMaxLen( void ) const;

private:
    Interval maxLen;
};

//======================================  include inline method definitions
#include "FixedLenTS.icc"

#endif // FIXEDLENTS
