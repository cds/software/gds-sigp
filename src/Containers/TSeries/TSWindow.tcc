/* -*- mode: C++; c-basic-offset: 4; -*- */
// TSWindow template code.
// $Id: TSWindow.hh 4266 2006-02-21 06:10:10Z keith.riles $
#ifndef LOCKLOSS_TSWINDOW_TCC
#define LOCKLOSS_TSWINDOW_TCC

#include <iostream>
#include <fstream>
#include <stdexcept>
#include <iterator>
#include <cstdlib>

//======================================  TSWindow constructor
template <class T>
TSWindow<T>::TSWindow(T dummyval, size_t len, Interval dt, int dbg,
		   const std::string& name, const std::string& savefilename)
    : mDebug(dbg),
      mName(name),
      mStartTime(0),
      mDt(dt.GetS()),
      mData(0),
      mSaveFileName(savefilename),
      mDummyVal(dummyval)
{
    // search for "space" or "tab" in name
    if (mName.find(" ") != std::string::npos ||
	mName.find("\t") != std::string::npos) {
	std::string eObj("TSWindow::TSWindow(): ERROR: name cannot contain whitespace");
	std::cerr << eObj << std::endl;
	throw std::runtime_error(eObj);
    }

    // length must be > 0
    if (len <= 0) {
	std::string eObj("TSWindow::TSWindow(): ERROR: max length must be >=0");
	std::cerr << eObj << std::endl;
	throw std::runtime_error(eObj);
    }

    setMaxLength(len);

    if (mDebug > 2)
	dump();
}
    
//======================================  initialize
template<class T>
inline void 
TSWindow<T>::init(T dummyval, const std::string& savefilename, size_t len,
		  const Time& starttime, Interval dt, int dbg,
		  const std::string& name) {
    mDebug = dbg;
    mName  = name;
    mStartTime = starttime.getS();
    mDt        = dt.GetS();
    mData      = 0;
    mSaveFileName = savefilename;
    mDummyVal  = dummyval;

    if (mName.find(" ") != std::string::npos ||
	mName.find("\t") != std::string::npos) {
	std::string eObj("TSWindow::TSWindow(): ERROR: name cannot contain whitespace");
	std::cerr << eObj << std::endl;
	throw std::runtime_error(eObj);
    }

    // length must be > 0
    if (len <= 0) {
	std::string eObj("TSWindow::TSWindow(): ERROR: maxlength must be >=0");
	std::cerr << eObj << std::endl;
	throw std::runtime_error(eObj);
    }

    setMaxLength(len);
}

//======================================  Print info
template<class T>
inline void 
TSWindow<T>::printInfo(void) const {
    std::cout << "TSWindow: name = " << mName << std::endl;
    std::cout << "            dt = " << mDt << std::endl;
    std::cout << "    max length = " << mMaxLength << std::endl;
    std::cout << "    start time = " << mStartTime << std::endl;
}
   
//======================================  set name
template<class T>
inline void 
TSWindow<T>::setName(const std::string & name) {
    if (mDebug > 0)
	std::cout << "TSWindow: setting name: " << name << std::endl;
    mName = name;
}
   
//======================================  set interval
template<class T>
inline void 
TSWindow<T>::setDt(const Interval & dt) {
    if (mDebug > 0) {
	std::cout << "TSWindow: setting interval: " << dt << std::endl;
    }
    mDt = dt.GetS();
}

//======================================  set start time
template<class T>
inline void 
TSWindow<T>::setStartTime(const Time & t0) {
    if (mDebug > 0) {
	std::cout << "TSWindow: setting start time: " << t0 << std::endl;
    }
    mStartTime = t0.getS();
}

//======================================  set max length -- 
//      side effects: sets mMaxLength, allocates data array
template<class T>
inline void
TSWindow<T>:: setMaxLength(size_t len) {
    if (mDebug > 0) {
	std::cout << "TSWindow: setting max length: " << len << std::endl;
    }
    if (len <= 0) {
	std::string eObj("TSWindow::setMaxLength(): ERROR: max length must be >=0");
	std::cerr << eObj << std::endl;
	throw std::runtime_error(eObj);
    }
    mMaxLength = len;

    // allocate space for data array
    if (mData != (T *)NULL) {
	if (mDebug > 0) std::cout << "TSWindow: mData != NULL" << std::endl;
	delete [] mData;
    }

    mData = new T[mMaxLength];

    if (mDebug > 0) {
	std::cout << "TSWindow::setMaxLength(): data arrray allocated; length = "
		  << length() << "; dt = " << dt() << std::endl;
            }
}

/// set savefile name
template<class T>
inline void 
TSWindow<T>::setSaveFileName(const std::string & savefilename) {
    mSaveFileName = savefilename;
}

/// KR - Added new function 20 Feb 2006
template<class T>
inline T 
TSWindow<T>::getLatest(void) const {
    T val;
    val = -1;
    if (mQueue.size() > 0 ) {
	typename std::deque<T>::const_iterator iter = mQueue.end();
	iter--;
	val = *iter;	
    }
    return val;
}

/// KR - Added new function 23 June 2008 (get mean of latest nval values)
template<class T>
inline T 
TSWindow<T>::getMean(int nval) const {
    T sumval = 0.;
    T meanval = -1.;
    if (mQueue.size() > (unsigned int)(nval+1) ) {
	typename std::deque<T>::const_iterator iter = mQueue.end();
	for (int i=0; i<nval; i++) {
	    iter--;
	    sumval += *iter;
	}
	meanval = sumval / nval;
    }
    return meanval;
}

/// KR - Added new function 23 June 2008 (get mean of latest nval values)
template<class T>
inline T 
TSWindow<T>::getMeanPos(int nval) const {
    T meanval = -1.;
    T sumval = 0.;
    if (mQueue.size() > (unsigned int)(nval+1) ) {
	typename std::deque<T>::const_iterator iter = mQueue.end();
	int i = 0;
	while (i < nval && iter != mQueue.begin()) {
	    iter--;
	    T val = *iter;
	    if (val > -1.e-6) {
		sumval += val;
		i++;
	    }
	}
	if (i==nval) meanval = sumval / nval;
    }
    return meanval;
}

/// Append a value into window
template<class T>
inline void 
TSWindow<T>::append(T val, const Time & t) {
    if (mDebug > 0)
	std::cout << "TSWindow::append: "
		  << val << " at time t = " << t.getS() << std::endl;

    if (mMaxLength == 0) {
	std::cerr << "TSWindow: maxLength has not been set" << std::endl;
	std::string eObj = "maxLength is zero";
	throw std::runtime_error(eObj);
    }

    // check for illegal input
    if (t.getN() != 0) {
	std::string eObj("TSWindow::append(): ERROR: time t has non-zero nanosec field.");
	std::cerr << eObj << std::endl;
	throw std::runtime_error(eObj);
    }

    if (t == Time()) {
	std::string eObj("TSWindow::append(): ERROR: time t is 0.");
	std::cerr << eObj << std::endl;
	throw std::runtime_error(eObj);
    }

    // set start time if it hasn't been set
    if (mStartTime == 0)
	mStartTime = t.getS();

    // pad with dummy data if necessary
    if (mQueue.size() > 0 ) {

	// gap is time diff. between last data in queue and
	// data that's about to be appended
	gap_type gap = computeGap(t);

	if (mDebug > 2) {
	    std::cout << "TSWindow::append(): gap = " << gap
		      << std::endl
		      << "    mQueue.size() = " << mQueue.size()
		      << std::endl
		      << "    mDt = " << mDt
		      << std::endl
		      << "    t.getS()   = " << t.getS()
		      << std::endl;
	    if (gap > 0)
		std::cout << "PADDING WITH DUMMY..." << std::endl;
	}

	if (gap < 0) {
	    std::string eObj("TSWindow::append(): ERROR: given time too early "
			     "-- can only append to end of current data");
	    std::cerr << eObj << std::endl;
	    throw std::runtime_error(eObj);
	}

	while (gap > 0) {
	    smart_push_back(mDummyVal);
	    --gap;
	}
    }

    smart_push_back(val);
}
   
//======================================  Fill the queue
template <class T>
inline size_t 
TSWindow<T>::fill(const Time & startTime, const std::vector<T> & data) {
    if (mDebug > 2) {
	std::cout << "TSWindow: fill():     startTime = "
		  << startTime << std::endl;
	std::cout << "TSWindow: fill(): i) mStartTime = "
		  << mStartTime << std::endl;
    }

    // check for error conditions
    if (startTime == Time()) {
	std::string eObj("TSWindow::fill(): ERROR: given start time is 0.");
	std::cerr << eObj << std::endl;
	throw std::runtime_error(eObj);
    }

    if (startTime.getN() != 0) {
	std::string eObj("TSWindow::fill(): ERROR: startTime has non-zero nanosec field");
	std::cerr << eObj << std::endl;
	throw std::runtime_error(eObj);
    }

    size_t numwritten=0;
    if (data.size() > mMaxLength) {
	std::cerr << "TSWindow::fill(): WARNING: too much data given, truncating" 
		  << std::endl;

	mQueue.clear();
	for (numwritten = 0; numwritten < mMaxLength; ++numwritten)
	    mQueue.push_back(data[numwritten]);
    }

    if (data.size() <= mMaxLength) {
	mQueue.clear();
	size_t ds = data.size();
	for (numwritten = 0; numwritten < ds; ++numwritten) {
	    mQueue.push_back(data[numwritten]);
	}

	if (data.size() < mMaxLength) {
	    std::cerr << "TSWindow::fill(): WARNING: too little data given, "
		      << "padding" << std::endl;
	    for (; numwritten < mMaxLength; ++numwritten)
		mQueue.push_back(mDummyVal);
	}
    }

    mStartTime = startTime.getS();

    if (mDebug > 2)
	std::cout << "TSWindow: fill(): ii) mStartTime = "
		  << mStartTime << std::endl;
    return numwritten;
}

//======================================  Fill a single entry
template<class T>
inline size_t 
TSWindow<T>::fill(const Time& startTime, size_t numval, T val) {
    if (numval == 0) numval = mMaxLength;

    if (val == T()) val = mDummyVal;

    std::vector<T> padding(numval, val);
    return fill(startTime, padding);
}

//=======================================  fill only if startTime is not set 
template<class T>
inline size_t 
TSWindow<T>::fillMaybe(const Time& startTime, size_t numval, T val) {
    if (mStartTime == 0)  {
	if (mDebug > 0) {
	    std::cout << "TSWindow::fillMaybe(): mStartTime == 0" << std::endl;
	}
	return fill(startTime, numval, val);
    } else {
	if (mDebug > 0) {
	    std::cout << "TSWindow::fillMaybe(): mStartTime == " 
		      << mStartTime << std::endl;
	}
	return mMaxLength;
    }
}

//======================================  Fill in given TSeries with data
template<class T>
inline void 
TSWindow<T>::fillTSeries(TSeries &ts) const {
    // if interval is 0
    if (mDt == 0) {
	std::string eObj = "TSWindow: ERROR: Sampling interval is 0";
	std::cerr << eObj << std::endl;
	throw std::runtime_error(eObj);
    }

    // dunno what to do if time is not set right, or dt is
    // not set right.
    if (mStartTime == 0) {
	std::string eObj = "TSWindow: ERROR: Start time is 0";
	std::cerr << eObj << std::endl;
	throw std::runtime_error(eObj);
    }

    typename std::deque<T>::const_iterator iter = mQueue.begin();
    for (size_t i=0; iter != mQueue.end(); ++iter, ++i) {
	mData[i] = *iter;
    }
    ts.setData(Time(mStartTime,0), Interval(mDt,0), mData, length());
}

//====================================== Fill array with num data
template <class T>
inline size_t
TSWindow<T>::fillVector(std::vector<T> &d, size_t num) const {
    size_t i = 0;

    if (num > maxLength()) {
	std::string eObj("TSWindow::fillVector(): ERROR: num > maxLength");
	eObj += "; ";
	eObj += mName;
	std::cerr << eObj << std::endl;
	throw std::runtime_error(eObj);
    } else if (num == 0) {
	// make sure there is enough space in the vector
	if (d.size() < length())
	    d.resize(length());

	typename std::deque<T>::const_iterator iter = mQueue.begin();
	for (; iter != mQueue.end(); ++iter, ++i)
	    d[i] = *iter;    
    } else if (num >= 1) {
	if (d.size() < num)
	    d.resize(num);

	i = num-1;
	typename std::deque<T>::const_reverse_iterator iter = mQueue.rbegin();
	for (; iter != mQueue.rend() && i >= 0; ++iter, --i)
	    d[i] = *iter;
    }

    return i;
}

//======================================  Save data to file. 
template <class T>
inline void 
TSWindow<T>::save(void) {
    if (mSaveFileName.size() == 0) {
	std::string eObj =
	    "TSWindow: ERROR: savefile name has not been set";
	std::cerr << eObj << std::endl;
	throw std::runtime_error(eObj);
    } else {
	if (mDebug > 0) 
	    std::cout << "TSWindow::save(): attempting to "
		      << "save to " << mSaveFileName << std::endl;

	// bleah. the iostream library in gcc-3.3 does not reset
	// status bits on close().
	std::ofstream mSaveFile(mSaveFileName.c_str(), std::ios_base::out);
	if (!mSaveFile.is_open()) {
	    if (mDebug > 0) 
		std::cout << "TSWindow::save(): saveFile not open" <<std::endl;
	    mSaveFile.open(mSaveFileName.c_str(), std::ios_base::out);
	    mSaveFile.clear();  // reset mSaveFile to good state
	    mSaveFile.seekp(std::ios_base::beg);
	}

	if (mSaveFile.fail()) {
	    std::string eObj = "TSWindow: ERROR: savefile could not be opened";
	    std::cerr << eObj << std::endl;
	    throw std::runtime_error(eObj);
	}

	mSaveFile.setf(std::ios_base::scientific);
	mSaveFile.precision(18);         // overkill, prolly
	mSaveFile << mDebug
		  << " " << mName
		  << " " << mDummyVal
		  << " " << mMaxLength
		  << " " << mQueue.size()
		  << " " << mStartTime
		  << " " << mDt << std::endl;

	// Ya know, this will not work except for the most
	// simple of template contents. Or if the class
	// contained has << and >> operators defined. D'oh.
	std::copy(mQueue.begin(), mQueue.end(),
		  std::ostream_iterator<T>(mSaveFile, " "));

	mSaveFile.close();

	if (mDebug > 0) 
	    std::cout << "TSWindow::save(): save to "
		      << mSaveFileName << " completed"
		      << std::endl;
    }
}

//======================================  Restore saved data if available
template<class T>
inline void 
TSWindow<T>::restore(void) {
    if (mDebug > 0) 
	std::cout << "TSWindow::restore(): attempting to "
		  << "restore from " << mSaveFileName << std::endl;

    if (mSaveFileName.size() == 0) {
	std::string eObj("TSWindow::restore(): ERROR: savefile name has not been set");
	std::cerr << eObj << std::endl;
	throw std::runtime_error(eObj);
    } else {
	std::ifstream mSaveFile(mSaveFileName.c_str(), std::ios_base::in);
	if (!mSaveFile.is_open()) {
	    mSaveFile.open(mSaveFileName.c_str(), std::ios_base::in);
	    mSaveFile.seekg(std::ios_base::beg);

	    if (mSaveFile.fail()) {
		std::cerr << "TSWindow::restore(): WARNING: save file "
			  << "does not exist; cannot restore" << std::endl;
		mSaveFile.close();
		return;
	    }
	}

	// tmp vars to store start time, and timestep
	ulong_t number_saved;  // number of data points that were saved
	char tmpname[128];  // trivial error-check
	mSaveFile >> mDebug >>  tmpname >> mDummyVal
		  >> mMaxLength >> number_saved >> mStartTime >> mDt;

	if (mDebug > 2) {
	    std::cout << mDebug << "\t"
		      << mDummyVal << "\t"
		      << tmpname << "\t"
		      << mMaxLength << "\t"
		      << number_saved << "\t"
		      << mStartTime << "\t"
		      << mDt << std::endl;

	    std::cout << "mName = " << mName << std::endl;
	    std::cout << "mName.size() = " << mName.size() << std::endl;
	    std::cout << "tmpname = " << tmpname << std::endl;
	}

	if (mName.size() > 0 && std::string(tmpname) != mName) {
	    std::cerr << "TSWindow::restore(): stored data "
		      << "has different name: expected " << mName
		      << " got " << tmpname << std::endl;
	    std::cerr << "    removing file" << std::endl;

	    mSaveFile.close();

	    std::string rmcmd = "/usr/bin/rm -f ";
	    rmcmd += mSaveFileName;
	    if (system(rmcmd.c_str())) return;
	    return;
	} else {
	    mName = tmpname;
	}

	// make sure we have enough buckets allocated for the restore
	// The time doesn't matter because we're just over-writing
	// everything
	while (mQueue.size() < number_saved) 
	    mQueue.push_back(mDummyVal);

	std::copy(std::istream_iterator<T>(mSaveFile),
		  std::istream_iterator<T>(),
		  mQueue.begin());

	mSaveFile.close();

	if (mDebug > 0) {
	    std::cout << "TSWindow::restore(): completed "
		      << "restore from " << mSaveFileName << std::endl;

	    if (mDebug > 2) dump();
	}
    }
}

//======================================  Restore from named file
template <class T>
inline void
TSWindow<T>::restore(const std::string& savefile) {
    mSaveFileName = savefile;
    restore();
}
   
/// Dump data to stderr
template <class T>
inline void
TSWindow<T>::dump(void) const {
    if (mDebug > 2) {
	std::cout << "TSWindow debug dump:" << std::endl;
	std::cout << "\tmName  = " << name() << std::endl;
	std::cout << "\tmDebug = " << mDebug << std::endl;
	std::cout << "\tmMaxLength = " << maxLength() << std::endl;
	std::cout << "\tmStartTime = " << startTime() << std::endl;
	std::cout << "\tmDt = " << mDt << std::endl;
	std::cout << "\tmSaveFileName = " << saveFileName() << std::endl;
	std::cout << "\tData: " << std::endl;
	std::copy(mQueue.begin(), mQueue.end(),
		  std::ostream_iterator<T>(std::cout, " "));
	std::cout << std::endl;
    }
}

//======================================  Smart push_back
template <class T>
inline void
TSWindow<T>::smart_push_back(T val) {
    // if we aren't full
    if (mQueue.size() < mMaxLength) {
	if (mDebug > 0)
	    std::cout << "TSWindow::smart_push_back(): pushing "
		      << val << std::endl;
	mQueue.push_back(val);  // later values at back
    } else {
	if (mDebug > 0) {
	    std::cout << "TSWindow::smart_push_back(): deque full. "
		      << "Popping front" << std::endl;
	    std::cout << "TSWindow::smart_push_back(): pushing "
		      << val << std::endl;
	}

	mQueue.pop_front();
	mQueue.push_back(val);
	mStartTime += mDt;

	if (mDebug > 0)
	    std::cout << "TSWindow::smart_push_back(): mStartTime = "
		      << mStartTime << std::endl;
    }
}

//======================================  compute gap between the time of the 
//                                        last data in queue and the time given
template <class T>
inline typename TSWindow<T>::gap_type 
TSWindow<T>::computeGap(const Time & t) const {
    if (mDebug > 2) {
	std::cout << "TSWindow::computeGap(): "
		  << "t.getS() = " << t.getS()
		  << "; mStartTime = " << mStartTime
		  << "; mQueue.size() = " << mQueue.size()
		  << std::endl;
    }

    if (mDt == 0) {
	std::string eObj("TSWindow::computeGap(): ERROR: mDt == 0");
	std::cerr << eObj << std::endl;
	throw std::runtime_error(eObj);
    }

    // (given time - (time of last data in deque)) / dt
    return (static_cast<gap_type>(t.getS()) -
	    (static_cast<gap_type>(mStartTime) +
	     static_cast<gap_type>(mQueue.size())*static_cast<gap_type>(mDt))) /
	static_cast<gap_type>(mDt);
}

#endif     //  LOCKLOSS_TSWINDOW_HH
