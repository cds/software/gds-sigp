//  -*- mode: C++; c-basic-offset: 4; -*-
//    File: TSWindow.hh
//   Author: David Chin <dwchin@umich.edu> +1-734-730-1274
// $Id: TSWindow.hh 4266 2006-02-21 06:10:10Z keith.riles $
#ifndef LOCKLOSS_TSWINDOW_HH
#define LOCKLOSS_TSWINDOW_HH

#include <string>
#include <vector>
#include <deque>
#include "Interval.hh"
#include "TSeries.hh"

/**  We would like to serve X hours of N second resolution data to the
  *  DMT viewer.  So, I want a queue-like object with an upper limit on
  *  length which acts like a window on a time series.
  *
  *  All Time and Interval objects have the nanoseconds field dropped.
  */
template < class T >
class TSWindow
{
public:
    /// Unsigned long data type
    typedef unsigned long ulong_t;

    /// Gap data type
    typedef long gap_type;

    /** @memo Default constructor
     *  @param dummyval Value to use as dummy padding
     *  @param len   Maximum length of window
     *  @param dt    Sampling interval. NOTE: nanoseconds part is ignored
     *  @param dbg   Debug level
     *  @param name  Name of this \c TSWindow. NOTE: no whitespace
     *  @param savefilename Name of savefile,
     */
    TSWindow( T                  dummyval,
              size_t             len = 8192,
              Interval           dt = Interval( 1, 0 ),
              int                dbg = 0,
              const std::string& name = std::string( "TSWindow_noname" ),
              const std::string& savefilename = std::string( ) );

    /**  Initialize the TS window.
      *  \brief Initializer
      *  \param dummyval Unused variable to specify template type.
      *  \param savefilename Name of file to be used for saving data.
      *  \param len  Maximum length of window
      *  \param starttime Start time of window
      *  \param dt   Sampling interval. NOTE: nanoseconds part is ignored
      *  \param dbg  Debug level
      *  \param name Name of this \c TSWindow.
      */
    void init( T                  dummyval,
               const std::string& savefilename,
               size_t             len = 8192,
               const Time&        starttime = Time( ),
               Interval           dt = Interval( 1, 0 ),
               int                dbg = 0,
               const std::string& name = std::string( "TSWindow_noname" ) );

    /// Destructor
    ~TSWindow( )
    {
        delete[] mData;
    }

    /// max. length of window
    size_t
    maxLength( ) const
    {
        return mMaxLength;
    }

    /// cur. length of window
    size_t
    length( ) const
    {
        return mQueue.size( );
    }

    /**  Get the window start time.
      *  \brief Start time
      *  \return windo start time
      */
    Time
    startTime( void ) const
    {
        return Time( mStartTime, 0 );
    }

    /**  Get the window start time.
      *  \brief Start time
      *  \return windo start time
      */
    Time
    t0( void ) const
    {
        return Time( mStartTime, 0 );
    }

    /// sampling interval
    Interval
    dt( ) const
    {
        return Interval( mDt );
    }

    /// dummy value
    T
    dummyVal( ) const
    {
        return mDummyVal;
    }

    /// Name of this TSWindow
    std::string
    name( ) const
    {
        return mName;
    }

    /// Savefile name
    std::string
    saveFileName( ) const
    {
        return mSaveFileName;
    }

    /// Print info
    void printInfo( void ) const;

    /// set name
    void setName( const std::string& name );

    /**  Set the %TSeries step time.
      *  \brief Set the time step
      *  \param dt Time step.
      */
    void setDt( const Interval& dt );

    /**  Set the %TSeries step time.
      *  \brief Set the time step
      *  \param dt Time step.
      */
    void
    setInterval( const Interval& dt )
    {
        setDt( dt );
    }

    /// set start time
    void setStartTime( const Time& t0 );

    /// set max length -- side effects: sets mMaxLength, allocates data array
    void setMaxLength( size_t len = 8192 );

    /// set debug
    void
    setDebug( int dbg )
    {
        mDebug = dbg;
    }

    /// set savefile name
    void setSaveFileName( const std::string& savefilename );

    /// KR - Added new function 20 Feb 2006
    T getLatest( void ) const;

    /// KR - Added new function 23 June 2008 (get mean of latest nval values)
    /**  Calculate the mean of the last \a nval elements. If the requested 
      *  number of values are not available, getMean returns -1.
      *  \brief Calculate mean of elements..
      *  \param nval Number of elements to average.
      *  \return Mean of selected values or -1.
      */
    T getMean( int nval ) const;

    // KR - Added new function 24 June 2008 (get mean of latest nval
    //                                       non-negative values)
    /**  Calculate the mean of the last \a nval non-negative values. If the 
      *  requested number of values are not available, getMeanpos returns -1.
      *  \brief Calculate mean of positive elements.
      *  \param nval Number of elements to average.
      *  \return Mean of selected values or -1.
      */
    T getMeanPos( int nval ) const;

    /// Append a value into window
    void append( T val, const Time& t );

    /**  @memo Fill window with given data, starting with time. Just
      *  overwrites current data. Returns no. of data
      *  filled.
      *  @param startTime Starting time for the TSWindow
      *  @param data Vector containing data
      *  @return Number of elements filled.
      */
    size_t fill( const Time& startTime, const std::vector< T >& data );

    /** @memo Fill window with nData times the given val, with starting time
      *    startTime
      *  @param numval Number of data elements to fill
      *  @param val    Value to be used for filling
      *  @param startTime Starting time for the TSWindow
      *  @return Number of elements filled
      */
    size_t fill( const Time& startTime, size_t numval = 0, T val = T( ) );

    /// fill only if startTime has not been set
    size_t fillMaybe( const Time& startTime, size_t numval = 0, T val = T( ) );

    /// Fill in given TSeries with data
    void fillTSeries( TSeries& ts ) const;

    /// Alias for fillTSeries()
    void
    tseries( TSeries& ts ) const
    {
        fillTSeries( ts );
    }

    /**  Fill in the argument vector with num data.
      *  if the num parameter is given, data is filled by most recent
      *  num entries.
      *  \brief Copy data window to a local vector.
      *  \param d  Reference to vector to recieve the data.
      *  \param num Number of data elements to copy.
      *  \return NUmber of elements copied.
      */
    size_t fillVector( std::vector< T >& d, size_t num = 0 ) const;

    /**  Fill in the argument vector with all available data;
      *  if the num parameter is given, data is filled by most recent
      *  num entries.
      *  \brief Copy data window to a local vector.
      *  \param d  Reference to vector to recieve the data.
      *  \return Number of elements copied.
      */
    size_t
    data( std::vector< T >& d ) const
    {
        return fillVector( d );
    }

    /** Save data to file. Format:
      * \verbatim
        startTime
	data[0]
	data[1]
	...
	data[N] \endverbatim
     */
    void save( void );

    /// Restore saved data if available
    void restore( void );

    /// Restore from named file
    void restore( const std::string& savefile );

    /// Dump data to stderr
    void dump( void ) const;

private:
    /// Debug level
    int mDebug;

    /// Name of window
    std::string mName;

    /// Max. length of window
    size_t mMaxLength;

    /** The actual deque object. Time ordered, i.e. data gets
     * pushed onto the back
     */
    std::deque< T > mQueue;

    /// The start time of a window
    ulong_t mStartTime;

    /// The stride (time step) of window
    ulong_t mDt;

    /// array of data for TSeries
    T* mData;

    /// File for saving data
    std::string mSaveFileName;

    /// Value to use as dummy data
    T mDummyVal;

    /// Smart push_back
    void smart_push_back( T val );

    /** compute gap between the time of the last data in queue and the
     * time given 
     */
    gap_type computeGap( const Time& t ) const;
};

#include "TSWindow.tcc"

#endif //  LOCKLOSS_TSWINDOW_HH
