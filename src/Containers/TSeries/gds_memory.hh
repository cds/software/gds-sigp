#ifndef GDS_MEMORY_HH
#define GDS_MEMORY_HH

#ifdef __CINT__
namespace std
{
    template < class T >
    class unique_ptr
    {
        T*    _p;
        void* _d;

    public:
        unique_ptr( void ) : _p( NULL ), _d( NULL )
        {
        }
        ~unique_ptr( void )
        {
            delete _p;
        }
        unique_ptr( T* x ) : _p( x ), _d( NULL )
        {
        }
        T&
        operator*( void )
        {
            return *_p;
        }
        const T&
        operator*( void ) const
        {
            return *_p;
        }
        T*
        operator->( void )
        {
            return _p;
        }
        const T*
        operator->( void ) const
        {
            return _p;
        }
        T*
        get( void )
        {
            return _p;
        }
        const T*
        get( void ) const
        {
            return _p;
        }
        void
        reset( T* x )
        {
            if ( _p && x != _p )
                delete _p;
            _p = x;
        }
    };

} // namespace std
#endif

#endif // !defined(GDS_MEMORY_HH)
