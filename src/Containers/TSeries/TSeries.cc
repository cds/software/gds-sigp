//* -*- mode: c++; c-basic-offset: 4; -*- */
//
//   TSeries class implementation
//
#include "PConfig.h"
#include "DVecType.hh"
#include "Chirp.hh"
#include "Interval.hh"
#include "Time.hh"
#include "TSeries.hh"
#include "FSeries.hh"
#include "constant.hh"
#include "fft.hh"
#include <stdexcept>
#include <iostream>
#include <iomanip>

using namespace std;

const TSeries TSeries::null_tseries;

//======================================  Default Constructor
TSeries::TSeries( void )
    : mDt( 0.0 ), mF0( 0.0 ), mFNyquist( 0.0 ), mStatus( 0 ), mSigmaW( 1.0 )
{
}

//======================================  Copy Constructor
TSeries::TSeries( const TSeries& TS )
{
    *this = TS;
}

//======================================  Move constructor.
TSeries::TSeries( TSeries&& ts )
    : mT0( ts.mT0 ), mDt( ts.mDt ), mF0( ts.mF0 ), mFNyquist( ts.mFNyquist ),
      mStatus( ts.mStatus ), mData( std::move( ts.mData ) ),
      mSigmaW( ts.mSigmaW ), mUnits( ts.mUnits )
{
}

//======================================  Assignment operator
TSeries&
TSeries::operator=( const TSeries& ts )
{
    mT0 = ts.mT0;
    mDt = ts.mDt;
    mF0 = ts.mF0;
    mFNyquist = ts.mFNyquist;
    mStatus = ts.mStatus;
    mSigmaW = ts.mSigmaW;
    if ( mData.get( ) && ts.mData.get( ) )
        *mData = *ts.mData;
    else if ( mData.get( ) )
        mData->Clear( );
    else if ( ts.mData.get( ) )
        mData.reset( ts.mData->clone( ) );
    mUnits = ts.mUnits;
    return *this;
}

//======================================  Assignment operator
TSeries&
TSeries::operator=( TSeries&& ts )
{
    mT0 = ts.mT0;
    mDt = ts.mDt;
    mF0 = ts.mF0;
    mFNyquist = ts.mFNyquist;
    mStatus = ts.mStatus;
    mSigmaW = ts.mSigmaW;
    mUnits = ts.mUnits;
    if ( mData && ts.mData )
        *mData = *ts.mData;
    else if ( mData )
        mData->Clear( );
    else if ( ts.mData )
        mData = std::move( ts.mData );
    return *this;
}

//======================================  Copy a TSeries
TSeries&
TSeries::copy( const TSeries& ts )
{
    mT0 = ts.mT0;
    mDt = ts.mDt;
    mF0 = ts.mF0;
    mFNyquist = ts.mFNyquist;
    mStatus = ts.mStatus;
    mSigmaW = ts.mSigmaW;
    mName = ts.mName;
    mUnits = ts.mUnits;
    if ( mData.get( ) && ts.mData.get( ) )
        mData->replace( 0, mData->size( ), *ts.mData );
    else if ( mData.get( ) )
        mData->Clear( );
    else if ( ts.mData.get( ) )
        mData.reset( ts.mData->clone( ) );
    return *this;
}

//======================================  Short integer data Constructor
TSeries::TSeries( const Time&     t0,
                  const Interval& dt,
                  size_type       NData,
                  const short     dData[] )
{
    setData( t0, dt, new DVectS( NData, dData ) );
}

//======================================  Integer data Constructor
TSeries::TSeries( const Time&     t0,
                  const Interval& dt,
                  size_type       NData,
                  const int       dData[] )
{
    setData( t0, dt, new DVectI( NData, dData ) );
}

//======================================  Unsigned int data Constructor
TSeries::TSeries( const Time&     t0,
                  const Interval& dt,
                  size_type       NData,
                  const uint32_t  dData[] )
{
    setData( t0, dt, new DVectU( NData, dData ) );
}

//======================================  Float data Constructor
TSeries::TSeries( const Time&     t0,
                  const Interval& dt,
                  size_type       NData,
                  const float     dData[] )
{
    setData( t0, dt, new DVectF( NData, dData ) );
}

//======================================  Data Constructor
TSeries::TSeries( const Time&     t0,
                  const Interval& dt,
                  size_type       NData,
                  const double    dData[] )
{
    setData( t0, dt, new DVectD( NData, dData ) );
}

//======================================  Data Constructor
TSeries::TSeries( const Time&     t0,
                  const Interval& dt,
                  size_type       NData,
                  const fComplex  dData[] )
{
    setData( t0, dt, new DVectC( NData, dData ) );
}

//======================================  Constructor from a data vector
TSeries::TSeries( const Time& t0, const Interval& dt, const DVector& data )
{
    setData( t0, dt, data.clone( ) );
}

//======================================  Constructor from a data vector
TSeries::TSeries( const Time& t0, const Interval& dt, DVector* data )
{
    setData( t0, dt, data );
}

//======================================  Constructor from function.
TSeries::TSeries( const Time&     t0,
                  const Interval& dt,
                  size_type       NData,
                  const Chirp&    funct )
    : mT0( t0 ), mDt( dt ), mF0( 0.0 ), mFNyquist( 0.5 / double( dt ) ),
      mStatus( 0 ), mSigmaW( 1.0 )
{
    //----------------------------------  Create the data vector.
    DVectF* DVptr = new DVectF( NData );
    //DVptr->ReSize(NData);
    mData.reset( DVptr );

    //----------------------------------  Loop over time points
    for ( size_type i = 0; i < NData; i++ )
    {
        Time t = mT0 + mDt * double( i );
        ( *DVptr )[ i ] = funct.Tspace( t );
    }
}

//======================================  Destructor
TSeries::~TSeries( void )
{
}

//======================================  Data Constructor
TSeries::TSeries( const FSeries& fs )
    : mT0( 0 ), mDt( 0.0 ), mF0( 0.0 ), mFNyquist( 0 ), mStatus( 0 ),
      mSigmaW( 1.0 )
{
    mF0 = fs.getCenterFreq( );
    mFNyquist = fs.getHighFreq( );
    mT0 = fs.getStartTime( );
    size_type nData = fs.getNStep( );
    math_type norm = sqrt( fs.getFStep( ) ) / sqrt( 2.0 ); // check this
    if ( fs.isSingleSided( ) )
    {
        nData *= 2;
        DVectF*         FVec = new DVectF( nData );
        const fComplex* CVec =
            reinterpret_cast< const fComplex* >( fs.refData( ) );
        mDt = 1. / ( nData * fs.getFStep( ) );
        wfft( CVec, FVec->refTData( ), nData );
        FVec->scale( 0, norm, nData );
        mData.reset( FVec );
    }
    else
    {
        DVectC*   DVec = new DVectC( nData );
        fComplex* CVec = DVec->refTData( );
        fs.getData( nData, CVec );
        mData.reset( DVec );
        mDt = 1. / ( nData * fs.getFStep( ) );
        wfft( CVec, nData, -1 );
        for ( size_type i = 0; i < nData; ++i )
            CVec[ i ] *= norm;
    }
}

//======================================  Accessors
TSeries
TSeries::decimate( size_type dec ) const
{
    TSeries r( mT0, mDt * double( dec ) );
    r.setUnits( mUnits );
    if ( dec == 0 )
        return r;
    size_type len = ( getNSample( ) + dec - 1 ) / dec;
    if ( len )
    {
        r.mData.reset( mData->Extract( 0, len, dec ) );
        r.mF0 = mF0;
        double fNy = mF0 + 2.0 / double( r.mDt );
        if ( fNy > mFNyquist )
            fNy = mFNyquist;
        r.mFNyquist = fNy;
        r.setStatus( mStatus );
    }
    return r;
}

void
TSeries::eraseStart( Interval dT )
{
    if ( empty( ) || double( mDt ) <= 0.0 )
        return;
    size_type nBin = size_type( double( dT ) / mDt + 0.5 );
    if ( nBin )
    {
        mData->Erase( 0, nBin );
        mT0 = getBinT( nBin );
    }
}

void
TSeries::extend( const Time& t0 )
{
    if ( !mData.get( ) || !mDt )
        return;
    Time tEnd = getEndTime( );
    if ( !tEnd || t0 <= tEnd )
        return;
    size_type lBin = size_type( ( t0 - getStartTime( ) ) / mDt + 0.5 );
    mData->Extend( lBin );
}

TSeries
TSeries::extract( const Time& t0, const Interval& dT ) const
{
    TSeries   r;
    size_type inxLo, inxHi;
    inxLo = getBin( t0 );
    if ( dT <= Interval( 0.0 ) )
        inxHi = getNSample( );
    else
        inxHi = getBin( t0 + dT );
    // TSeries r(getBinT(inxLo), mDt);
    r.Clear( getBinT( inxLo ), mDt );
    r.setF0( getF0( ) );
    r.setStatus( getStatus( ) );
    r.mName = mName;
    r.mUnits = mUnits;
    if ( inxHi > inxLo )
        r.mData.reset( mData->Extract( inxLo, inxHi - inxLo ) );
    return r;
}

TSeries::size_type
TSeries::getNSample( void ) const
{
    if ( !mData.get( ) )
        return 0;
    return mData->getLength( );
}

TSeries::size_type
TSeries::getBin( const Time& t ) const
{
    if ( t <= mT0 || !mData.get( ) || !mDt )
        return 0;
    size_type r = size_type( ( double( t - mT0 ) + 5e-10 ) / double( mDt ) );
    size_type N = mData->getLength( );
    if ( r > N )
        return N;
    return r;
}

TSeries::complex_type
TSeries::getComplexAverage( void ) const
{
    size_type len = getNSample( );
    if ( !len )
        return complex_type( 0.0 );
    return mData->CSum( 0, len ) / math_type( len );
}

TSeries::math_type
TSeries::getAverage( void ) const
{
    size_type len = getNSample( );
    if ( !len )
        return 0.0;
    return mData->VSum( 0, len ) / math_type( len );
}

TSeries::math_type
TSeries::getMaximum( void ) const
{
    if ( empty( ) )
        return 0.0;
    return mData->getMaximum( );
}

TSeries::math_type
TSeries::getMinimum( void ) const
{
    if ( empty( ) )
        return 0.0;
    return mData->getMinimum( );
}

TSeries::size_type
TSeries::getNBetween( math_type low, math_type high ) const
{
    if ( empty( ) )
        return 0;
    return mData->getNBetween( low, high );
}

TSeries::size_type
TSeries::getNGreater( math_type limit ) const
{
    if ( empty( ) )
        return 0;
    return mData->getNGreater( limit );
}

TSeries::size_type
TSeries::getNLess( math_type limit ) const
{
    if ( empty( ) )
        return 0;
    return mData->getNLess( limit );
}

TSeries::size_type
TSeries::getData( size_type len, short* data ) const
{
    if ( empty( ) )
        return 0;
    return mData->getData( 0, len, data );
}

TSeries::size_type
TSeries::getData( size_type len, int* data ) const
{
    if ( empty( ) )
        return 0;
    return mData->getData( 0, len, data );
}

TSeries::size_type
TSeries::getData( size_type len, float* data ) const
{
    if ( empty( ) )
        return 0;
    return mData->getData( 0, len, data );
}

TSeries::size_type
TSeries::getData( size_type len, double* data ) const
{
    if ( empty( ) )
        return 0;
    return mData->getData( 0, len, data );
}

TSeries::size_type
TSeries::getData( size_type len, fComplex* data ) const
{
    if ( empty( ) )
        return 0;
    return mData->getData( 0, len, data );
}

TSeries::size_type
TSeries::getData( size_type len, dComplex* data ) const
{
    if ( empty( ) )
        return 0;
    return mData->getData( 0, len, data );
}

TSeries::complex_type
TSeries::getComplex( size_type index ) const
{
    if ( empty( ) )
        return complex_type( 0.0 );
    return mData->getCplx( index );
}

double
TSeries::getDouble( size_type index ) const
{
    if ( empty( ) )
        return 0;
    return mData->getDouble( index );
}

bool
TSeries::isComplex( void ) const
{
    return mData.get( ) && ( mData->C_data( ) || mData->W_data( ) );
}

const void*
TSeries::refData( ) const
{
    if ( !mData.get( ) )
        return 0;
    return mData->refData( );
}

void*
TSeries::refData( )
{
    if ( !mData.get( ) )
        return 0;
    return mData->refData( );
}

//======================================  Mutators
void
TSeries::ReSize( size_type len )
{
    if ( mData.get( ) )
        mData->ReSize( len );
}

void
TSeries::Clear( const Time& t0, const Interval& dt )
{
    mT0 = t0;
    mDt = dt;
    mF0 = 0;
    mStatus = 0;
    if ( mData.get( ) )
        mData->Clear( );
}

void
TSeries::Convert( int type )
{
    if ( !mData.get( ) )
    {
        switch ( type )
        {
        case DVector::t_short:
            mData.reset( new DVectS );
            break;
        case DVector::t_int:
            mData.reset( new DVectI );
            break;
        case DVector::t_float:
            mData.reset( new DVectF );
            break;
        case DVector::t_double:
            mData.reset( new DVectD );
            break;
        case DVector::t_complex:
            mData.reset( new DVectC );
            break;
        case DVector::t_dcomplex:
            mData.reset( new DVectW );
            break;
        case DVector::t_uint:
            mData.reset( new DVectU );
            break;
        default:
            throw runtime_error( "TSeries::Convert: Invalid type specified" );
        }
    }
    else if ( mData->getType( ) != type )
    {
        mData.reset( mData->convert( DVector::DVType( type ) ) );
    }
}

void
TSeries::appName( const char* name )
{
    mName += name;
}

//======================================  Set data to a data vector.
int
TSeries::setData( const Time& t0, const Interval& dt, DVector* data )
{
    mT0 = t0;
    mDt = dt;
    mF0 = 0.0;
    mFNyquist = 0.5 / double( dt );
    mStatus = 0;
    mSigmaW = 1.0;
    if ( data != mData.get( ) )
        mData.reset( data );
    return 0;
}

void
TSeries::setF0( double f0 )
{
    mF0 = f0;
}

void
TSeries::setFNyquist( double fNy )
{
    mFNyquist = fNy;
}

void
TSeries::setName( const char* name )
{
    mName = name;
}

void
TSeries::setStatus( stat_type stat )
{
    mStatus = stat;
}

void
TSeries::setSigmaW( math_type sw )
{
    mSigmaW = sw;
}

void
TSeries::setUnits( const std::string& units )
{
    mUnits = units;
}

void
TSeries::combineStatus( stat_type stat )
{
    mStatus |= stat;
}

//======================================  Append TSeries
int
TSeries::Append( const TSeries& t, size_type decim8 )
{
    int rc;

    //----------------------------------  Leave it alone to append nothing.
    if ( t.empty( ) )
    {
        rc = 0;
    }

    //----------------------------------  Commute appends to assignments
    else if ( empty( ) )
    {
        string svUnits = mUnits;
        string svTitle = mName;
        if ( decim8 <= 1 )
            *this = t;
        else
            *this = t.decimate( decim8 );
        mUnits = svUnits;
        mName = svTitle;
        rc = 0;
    }

    //----------------------------------  Do a real append
    else
    {
        rc = Contig( t.getStartTime( ), t.getTStep( ) * double( decim8 ) );
        if ( !rc )
        {
            if ( decim8 <= 1 )
            {
                mData->Append( *t.mData );
            }
            else if ( t.mData->size( ) >= decim8 )
            {
                size_type             len = t.mData->size( ) / decim8;
                unique_ptr< DVector > ext( t.mData->Extract( 0, len, decim8 ) );
                mData->Append( *ext );
            }
            combineStatus( t.mStatus );
        }
    }
    return rc;
}

//======================================  Append short data
int
TSeries::Append( const Time&     t,
                 const Interval& Step,
                 const short*    data,
                 size_type       N )
{

    //----------------------------------  Check that data is continuous
    int bad = Contig( t, Step );
    if ( bad )
        return bad;

    //----------------------------------  Create or add to a DVector
    if ( !mData.get( ) )
        mData.reset( new DVectS( N, data ) );
    else
        mData->Append( N, data );
    return 0;
}

//======================================  Append integer data
int
TSeries::Append( const Time&     t,
                 const Interval& Step,
                 const int*      data,
                 size_type       N )
{

    //----------------------------------  Check that data is continuous
    int bad = Contig( t, Step );
    if ( bad )
        return bad;

    //----------------------------------  Create or add to a DVector
    if ( !mData.get( ) )
        mData.reset( new DVectI( N, data ) );
    else
        mData->Append( N, data );

    return 0;
}

//=======================================  Append float data
int
TSeries::Append( const Time&     t,
                 const Interval& Step,
                 const float*    data,
                 size_type       N )
{

    //----------------------------------  Check that data is continuous
    int bad = Contig( t, Step );
    if ( bad )
        return bad;

    //----------------------------------  Create or add to a DVector
    if ( !mData.get( ) )
        mData.reset( new DVectF( N, data ) );
    else
        mData->Append( N, data );

    return 0;
}

//======================================  Append double float data
int
TSeries::Append( const Time&     t,
                 const Interval& Step,
                 const double*   data,
                 size_type       N )
{

    //----------------------------------  Check that data is continuous
    int bad = Contig( t, Step );
    if ( bad )
        return bad;

    //----------------------------------  Create or add to a DVector
    if ( !mData.get( ) )
        mData.reset( new DVectD( N, data ) );
    else
        mData->Append( N, data );

    return 0;
}

//======================================  Append Complex data
int
TSeries::Append( const Time&     t,
                 const Interval& Step,
                 const fComplex* data,
                 size_type       N )
{
    //----------------------------------  Check that data is continuous
    int bad = Contig( t, Step );
    if ( bad )
        return bad;

    //----------------------------------  Create or add to a DVector
    if ( !mData.get( ) )
        mData.reset( new DVectC( N, data ) );
    else
        mData->Append( N, data );

    return 0;
}

//======================================  Append Complex data
int
TSeries::Append( const Time&     t,
                 const Interval& Step,
                 const dComplex* data,
                 size_type       N )
{
    //----------------------------------  Check that data is continuous
    int bad = Contig( t, Step );
    if ( bad )
        return bad;

    //----------------------------------  Create or add to a DVector
    if ( !mData.get( ) )
        mData.reset( new DVectW( N, data ) );
    else
        mData->Append( N, data );

    return 0;
}

//======================================  Check that data is continuous
int
TSeries::Contig( const Time& t, const Interval& Step )
{
    if ( empty( ) )
    {
        mT0 = t;
        mDt = Step;
    }
    else if ( !Almost( getEndTime( ), t ) )
    {
        return -1; // throw something?
    }
    else if ( mDt != Step )
    {
        return -2; // throw something?
    }
    return 0;
}

//======================================  Dump the TSeries meta-data
ostream&
TSeries::dump_header( ostream& out ) const
{
    out << "TSeries " << mName << ": Start time " << getStartTime( )
        << " End Time " << getEndTime( ) << " Data Length " << getNSample( );
    if ( double( mDt ) >= 1.0 )
        out << " Sample Time " << mDt << "s";
    else if ( double( mDt ) <= 0.0 )
        out << "Sample time not initialized";
    else
        out << " Sample Rate " << 1.0 / double( mDt ) << "Hz";
    out << " units: " << mUnits << endl;
    return out;
}

//======================================  Dump the contents
ostream&
TSeries::Dump( ostream& out ) const
{
    dump_header( out );
    if ( mData.get( ) )
        return mData->Dump( out );
    return out;
}

//======================================  Shift the frequency.
TSeries
TSeries::fShift( math_type f0, math_type phi0 ) const
{
    size_type nwd = getNSample( );
    if ( !nwd )
        return *this;

    DVectC dv( *mData );

    math_type wdt = twopi * f0 * double( mDt );
#ifdef USE_SINCOS
    math_type cosph, sinph;
    sincos( wdt, &sinph, &cosph );
    dComplex dRot( cosph, sinph );
    sincos( phi0, &sinph, &cosph );
    dComplex Rot( cosph, sinph );
#else
    dComplex dRot( ::cos( wdt ), ::sin( wdt ) );
    dComplex Rot( ::cos( phi0 ), ::sin( phi0 ) );
#endif
    if ( mData->C_data( ) )
    {
        for ( size_type i = 0; i < nwd; i++ )
        {
            dv[ i ] *= Rot;
            Rot *= dRot;
        }
    }
    else
    {
        for ( size_type i = 0; i < nwd; i++ )
        {
            dv[ i ] = Rot * dv[ i ].real( );
            Rot *= dRot;
        }
    }

    TSeries r( mT0, mDt, dv );
    r.mF0 = mF0 - f0;
    if ( f0 > 0.0 )
        r.mFNyquist = mFNyquist - f0;
    r.setStatus( getStatus( ) );
    r.mUnits = mUnits;
    return r;
}

//======================================  Bias a TSeries
TSeries&
TSeries::operator+=( math_type Bias )
{
    if ( !empty( ) )
        *mData += Bias;
    return *this;
}

//======================================  Bias a TSeries
TSeries&
TSeries::operator+=( complex_type Bias )
{
    if ( !empty( ) )
        *mData += Bias;
    return *this;
}

//======================================  Scale a TSeries
TSeries&
TSeries::operator*=( math_type Scale )
{
    if ( !empty( ) )
        *mData *= Scale;
    return *this;
}

//======================================  Scale a complex TSeries
TSeries&
TSeries::operator*=( complex_type Scale )
{
    if ( !empty( ) )
        *mData *= Scale;
    return *this;
}

//======================================  Add two overlapping TSeries
TSeries&
TSeries::add_overlap( const TSeries& rhs )
{
    size_type inx, inx1;
    size_type nw = overlap( rhs, inx, inx1 );
    if ( nw )
    {
        mData->add( inx, *rhs.mData, inx1, nw );
        combineStatus( rhs.getStatus( ) );
    }
    return *this;
}

TSeries&
TSeries::operator+=( const TSeries& rhs )
{
    if ( empty( ) || rhs.empty( ) )
        return *this;
    if ( mDt != rhs.mDt || mT0 != rhs.mT0 ||
         getNSample( ) != rhs.getNSample( ) )
    {
        throw runtime_error( "TSeries::operator+= binning mismatch" );
    }
    *mData += *rhs.mData;
    combineStatus( rhs.getStatus( ) );
    return *this;
}

//======================================  Difference of two overlapping TSeries
TSeries&
TSeries::subtract_overlap( const TSeries& rhs )
{
    size_type inx, inx1;
    size_type nw = overlap( rhs, inx, inx1 );
    if ( nw )
    {
        mData->sub( inx, *rhs.mData, inx1, nw );
        combineStatus( rhs.getStatus( ) );
    }
    return *this;
}

TSeries&
TSeries::operator-=( const TSeries& rhs )
{
    if ( empty( ) || rhs.empty( ) )
        return *this;
    if ( mDt != rhs.mDt || mT0 != rhs.mT0 ||
         getNSample( ) != rhs.getNSample( ) )
    {
        throw runtime_error( "TSeries::operator-= binning mismatch" );
    }
    *mData -= *rhs.mData;
    combineStatus( rhs.getStatus( ) );
    return *this;
}

//======================================  Dot product of two series
TSeries::math_type
TSeries::operator*( const TSeries& rhs ) const
{
    if ( empty( ) || rhs.empty( ) )
        return 0.0;
    return ( *mData ) * ( *rhs.mData );
}

//======================================  Product of two series
TSeries&
TSeries::multiply_overlap( const TSeries& rhs )
{
    //----------------------------------  Get the overlapping fields
    size_type inx, inx1;
    size_type nw = overlap( rhs, inx, inx1 );
    if ( !nw )
        return *this;

    //-----------------------------------  Multiply DVectors
    mData->mpy( inx, *rhs.mData, inx1, nw );
    combineStatus( rhs.getStatus( ) );
    mF0 -= rhs.mF0;
    mSigmaW = mSigmaW * rhs.mSigmaW;
    return *this;
}

TSeries&
TSeries::operator*=( const TSeries& rhs )
{
    if ( empty( ) || rhs.empty( ) )
        return *this;
    if ( mDt != rhs.mDt || mT0 != rhs.mT0 ||
         getNSample( ) != rhs.getNSample( ) )
    {
        throw runtime_error( "TSeries::operator*= binning mismatch" );
    }
    *mData *= *rhs.mData;
    combineStatus( rhs.getStatus( ) );
    return *this;
}

//======================================  Ratio of two series
TSeries&
TSeries::divide_overlap( const TSeries& rhs )
{
    //----------------------------------  Get the overlapping fields
    size_type inx, inx1;
    size_type nw = overlap( rhs, inx, inx1 );
    if ( !nw )
        return *this;

    //----------------------------------  Get numerator substring
    switch ( mData->getType( ) )
    {
    case DVector::t_short:
        Convert( DVector::t_float );
        break;
    case DVector::t_int:
    case DVector::t_long:
        Convert( DVector::t_double );
        break;
    default:
        break;
    }

    //-----------------------------------  Divide DVectors
    mData->div( 0, *rhs.mData, inx1, nw );
    combineStatus( rhs.getStatus( ) );
    mF0 -= rhs.mF0;
    mSigmaW = mSigmaW / rhs.mSigmaW;
    return *this;
}

TSeries&
TSeries::operator/=( const TSeries& rhs )
{
    if ( empty( ) || rhs.empty( ) )
        return *this;
    if ( mDt != rhs.mDt || mT0 != rhs.mT0 ||
         getNSample( ) != rhs.getNSample( ) )
    {
        throw runtime_error( "TSeries::operator/= binning mismatch" );
    }
    *mData /= *rhs.mData;
    combineStatus( rhs.getStatus( ) );
    return *this;
}

//========================================  Find overlapping region
TSeries::size_type
TSeries::overlap( const TSeries& ts, size_type& inx, size_type& inx1 ) const
{
    if ( mDt != ts.mDt )
        throw runtime_error( "TSeries time step mismatch" );
    size_type l = getNSample( );
    if ( !l )
        return 0;
    size_type l1 = ts.getNSample( );
    if ( !l1 )
        return 0;
    if ( mT0 == ts.mT0 )
    {
        inx = inx1 = 0;
    }
    else if ( mT0 < ts.mT0 )
    {
        inx = getBin( ts.mT0 );
        l -= inx;
        if ( !Almost( getBinT( inx ), ts.mT0 ) )
            throw runtime_error( "TSeries times not aligned" );
        inx1 = 0;
    }
    else
    {
        inx1 = ts.getBin( mT0 );
        l1 -= inx1;
        if ( !Almost( mT0, ts.getBinT( inx1 ) ) )
            throw runtime_error( "TSeries times not aligned" );
        inx = 0;
    }
    return ( l <= l1 ) ? l : l1;
}
