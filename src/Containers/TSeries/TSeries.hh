/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef TSERIES_HH
#define TSERIES_HH

#include <string>
#include <iostream>
#include <memory>
#ifdef __CINT__
#include "gds_memory.hh"
typedef unsigned int uint32_t;
#else
#include <cstdint>
#endif
#include "Complex.hh"
#include "Interval.hh"
#include "Time.hh"

class DVector;
class FSeries;
class Chirp;

/** The %TSeries class holds a time series of arbitrarily typed data.
  * @brief %Time series data container.
  * @author John G. Zweizig
  * @version 1.6; Last modified August 12, 2016
  */
class TSeries
{
public:
    /**  Vector length data type.
      *  @brief Length type.
      */
    typedef unsigned long size_type;

    /**  Data Status Type.
      *  @brief Status type.
      */
    typedef unsigned long stat_type;

    /**  Mathematical data type.
      *  @brief Data type used in calculations.
      */
    typedef double math_type;

    /**  Mathematical data type.
      *  @brief Data type used in calculations.
      */
    typedef dComplex complex_type;

    /**  A time series object is initialized without a data vector.
      *  @brief Default constructor.
      */
    TSeries( );

    /**  The argument series is duplicated into the new %TSeries.
      *  @brief  Copy Constructor.
      *  @param TS %Time series to be duplicated.
      */
    TSeries( const TSeries& TS );

    /**  The argument series is moved into the new %TSeries.
      *  @brief    Move syntax constructor.
      *  @param TS %Time series to be moved.
      */
    TSeries( TSeries&& TS );

    /**  A float time series object is constructed and initialized from a 
      *  float data array. If \a data is specified as NULL or unspecified,
      *  The resulting time series will have the specified length but the
      *  data will be left uninitialized.
      *  @brief  Float data constructor.
      *  @param t0    Series start time.
      *  @param dt    %Time interval between two successive samples.
      *  @param NData Number of data words.
      *  @param dData float array containing series data.
      */
    TSeries( const Time&     t0,
             const Interval& dt,
             size_type       NData,
             const float     dData[] = 0 );

    /**  A short time series object is constructed and initialized from a 
      *  short data array. If \a data is specified as NULL, the resulting 
      *  time series will have the specified length but the data will be 
      *  left uninitialized.
      *  @brief  Short integer data constructor.
      *  @param t0    Series start time.
      *  @param dt    %Time interval between two successive samples.
      *  @param NData Number of data words.
      *  @param dData short integer array containing series data.
      */
    TSeries( const Time&     t0,
             const Interval& dt,
             size_type       NData,
             const short     dData[] );

    /**  An integer time series object is constructed and initialized from a 
      *  integer data array. If \a data is specified as NULL, the resulting 
      *  time series will have the specified length but the data will be 
      *  left uninitialized.
      *  @brief  Integer data constructor.
      *  @param t0    Series start time.
      *  @param dt    %Time interval between two successive samples.
      *  @param NData Number of data words.
      *  @param dData integer array containing series data.
      */
    TSeries( const Time&     t0,
             const Interval& dt,
             size_type       NData,
             const int       dData[] );

    /**  An unsigned integer time series object is constructed and initialized 
      *  from an unsigned integer data array. If \a data is specified as NULL, 
      *  the resulting time series will have the specified length but the data 
      *  will be left uninitialized.
      *  @brief  Integer data constructor.
      *  @param t0    Series start time.
      *  @param dt    %Time interval between two successive samples.
      *  @param NData Number of data words.
      *  @param dData unsigned int array containing series data.
      */
    TSeries( const Time&     t0,
             const Interval& dt,
             size_type       NData,
             const uint32_t  dData[] );

    /**  A double float time series object is constructed and initialized from 
      *  a double float data array. If \a data is specified as NULL, the 
      *  resulting time series will have the specified length but the data will 
      *  be left uninitialized.
      *  @brief  Double float data constructor.
      *  @param t0    Series start time.
      *  @param dt    %Time interval between two successive samples.
      *  @param NData Number of data words.
      *  @param dData double array containing series data.
      */
    TSeries( const Time&     t0,
             const Interval& dt,
             size_type       NData,
             const double    dData[] );

    /**  A Complex time series object is constructed and initialized from a 
      *  Complex data array. If \a data is specified as NULL, the resulting 
      *  time series will have the specified length but the data will be 
      *  left uninitialized.
      *  @brief  Complex data constructor.
      *  @param t0    Series start time.
      *  @param dt    %Time interval between two successive samples.
      *  @param NData Number of data words.
      *  @param dData Complex array containing series data.
      */
    TSeries( const Time&     t0,
             const Interval& dt,
             size_type       NData,
             const fComplex  dData[] );

    /**  Construct a time series and initialize it from a specified signal
      *  template function.
      *  @brief  Signal template function constructor.
      *  @param t0    Series start time.
      *  @param dt    %Time interval between two successive samples.
      *  @param NData Number of data words.
      *  @param func  Template object based on a Chirp.
      */
    TSeries( const Time&     t0,
             const Interval& dt,
             size_type       NData,
             const Chirp&    func );

    /**  Construct a time series and initialize it from a specified 
      *  data vector.
      *  @brief  Construct and copy data from a %DVector.
      *  @param t0    Series start time.
      *  @param dt    %Time interval between two successive samples.
      *  @param data  Data vector.
      */
    TSeries( const Time& t0, const Interval& dt, const DVector& data );

    /**  Construct a time series and adopt the specified data vector.
      *  If the data vector pointer is specified  as zero or allowed to 
      *  default, the time series is left empty.
      *  @brief  Construct and adopt a data vector.
      *  @param t0    Series start time.
      *  @param dt    %Time interval between two successive samples.
      *  @param data  Data vector.
      */
    TSeries( const Time& t0, const Interval& dt, DVector* data = 0 );

    /**  Construct a time series from an FSeries by inverse fft.
      *  @brief  FSeries constructor.
      *  @param fs    Frequency Series.
      */
    explicit TSeries( const FSeries& fs );

    /**  Blow away the data vector.
      *  @brief %TSeries Destructor.
      */
    virtual ~TSeries( );

    //------------------------------  Accessors
    /**  Convert and copy the argument %TSeries to this %TSeries using the 
      *  existing %DVector. This method differs from the assignment operator 
      *  in that the resulting %TSeries will never share a DVector with the 
      *  the argument series. 
      *  @brief   Copy %TSeries data.
      *  @param  in  %Time series to be copied.
      *  @return Reference to this time series.
      */
    TSeries& copy( const TSeries& in );

    /**  Write a formatted dump of the series contents to the specified output
      *  stream.
      *  @brief   Formatted dump of the %TSeries data.
      *  @param  out  Output stream to receive the formatted %TSeries dump.
      *  @return Reference to the specified output stream.
      */
    std::ostream& Dump( std::ostream& out = std::cout ) const;

    /**  Write a formatted dump of the series mmeta-data to the specified output
      *  stream.
      *  @brief   Formatted dump of the %TSeries meta-data.
      *  @param  out  Output stream to receive the formatted dump.
      *  @return Reference to the specified output stream.
      */
    std::ostream& dump_header( std::ostream& out = std::cout ) const;

    /**  A %TSeries is decimated by the specified integer factor. The resulting
      *  series is returned. The start time of the returned series is the same 
      *  the start time of the original series and the sample time is increased
      *  by the decimation factor. If the initial series length is not a 
      *  multiple of the decimation factor, the resulting series will be longer
      *  in time than the original series.
      *  @brief Decimate a time series.
      *  @param  dec Decimation factor.
      *  @return The time series decimated by thespecified factor.
      */
    TSeries decimate( size_type dec ) const;

    /**  Remove the data from the time interval <tt>{t0, t0+dT}</tt>. The time 
      *  removed is rounded to the nearest integer number of samples.
      *  @brief Erase data from the start of a %TSeries.
      *  @param  dT %Time duration of the sub-string to be erased.
      */
    void eraseStart( Interval dT );

    /**  A %TSeries is returned containing the data from the time interval
      *  <tt>t0 - t0+dT</tt>. If <tt>dT == 0</tt>, all data after t0 will 
      *  be returned.
      *  @brief Extract a subset of the data from a %TSeries.
      *  @return a %TSeries containing the requested subset of the data.
      *  @param  t0 Start time of the desired subset.
      *  @param  dT %Time duration of the desired subset.
      */
    TSeries extract( const Time&     t0,
                     const Interval& dT = Interval( 0.0 ) ) const;

    /**  The series is extended to the specified time by padding on the right 
      *  with zeroes.
      *  @brief Extend a %TSeries.
      *  @param  t0 Desired series end-time.
      */
    void extend( const Time& t0 );

    /**  Get the bin index corresponding to the specified time. Note that the
      *  Time of the indexed sample is less than or equal to the specified time
      *  with the following exceptions. If the requested time is before the 
      *  start time of the series, bin 0 is returned. If the requested time 
      *  is later than the end of the series, the index of the first sample 
      *  after the end of the series (\e i.e. the current series length) is 
      *  returned. 
      *  \brief Get bin index.
      *  \param t Time for which the bin is to be returned. 
      *  \return Index of the bin with time t.
      */
    size_type getBin( const Time& t ) const;

    /**  Get the time corresponding to the specified sample number.
      *  \brief  Get bin time.
      *  \param inx Sample number for which the time is to be calculated.
      *  \return Time corresponding to the specified sample.
      */
    Time getBinT( size_type inx ) const;

    /**  The specified entry is converted to complex if necessary
      *  and returned.
      *  @brief  Get ith element as a complex number.
      *  @param  index Index of the element to be returned.
      *  @return ith element value.
      */
    complex_type getComplex( size_type index ) const;

    /**  The %TSeries data are copied to a short int data buffer pointed to 
      *  by \a data. The data will be converted to 'short int's if the data 
      *  vector is of a different data type. A maximum of \a len data words 
      *  will be copied to the data buffer. 
      *  @brief   Copy data to a short int output array.
      *  @param  len  Maximum number of words to copy.
      *  @param  data Short integer array into which the data will be copied.
      *  @return The number of words copied.
      */
    size_type getData( size_type len, short* data ) const;

    /**  The %TSeries data are copied to an integer data buffer pointed to by
      *  'data'. The data will be converted to integers if the data vector
      *  is of a different data type. A maximum of \a len data words will 
      *  be copied to the data buffer. getData() returns the number of 
      *  words copied.
      *  @brief   Copy data to an integer array
      *  @param  len  Dimension of output array.
      *  @param  data Array to receive series data.
      *  @return Number of words copied.
      */
    size_type getData( size_type len, int* data ) const;

    /**  The %TSeries data are copied to a float data buffer pointed to by
      *  'data'. The data will be converted to floats if the data vector
      *  is of a different data type. A maximum of \a len data words will 
      *  be copied to the data buffer. getData() returns the number of 
      *  words copied.
      *  @brief   Copy data to a float array
      *  @param  len  Dimension of output array.
      *  @param  data Array to receive series data.
      *  @return Number of words copied.
      */
    size_type getData( size_type len, float* data ) const;

    /**  The %TSeries data are copied to a double data buffer pointed to by
      *  'data'. The data will be converted to floats if the data vector
      *  is of a different data type. A maximum of \a len data words will 
      *  be copied to the data buffer. getData() returns the number of 
      *  words copied.
      *  @brief   Copy data to a double array
      *  @param  len  Dimension of output array.
      *  @param  data Array to receive series data.
      *  @return Number of words copied.
      */
    size_type getData( size_type len, double* data ) const;

    /**  The %TSeries data are copied to an fComplex buffer pointed to by
      *  'data'. The data will be converted to fComplex if the data vector
      *  is of a different data type. A maximum of \a len data words will 
      *  be copied to the data buffer. getData() returns the number of 
      *  words copied.
      *  @brief   Copy data to a complex array.
      *  @param  len  Dimension of output array
      *  @param  data Array to receive series data.
      *  @return Number of words copied.
      */
    size_type getData( size_type len, fComplex* data ) const;

    /**  The %TSeries data are copied to a dComplex buffer pointed to by
      *  'data'. The data will be converted to fComplex if the data vector
      *  is of a different data type. A maximum of \a len data words will 
      *  be copied to the data buffer. getData() returns the number of 
      *  words copied.
      *  @brief   Copy data to a complex array.
      *  @param  len  Dimension of output array
      *  @param  data Array to receive series data.
      *  @return Number of words copied.
      */
    size_type getData( size_type len, dComplex* data ) const;

    /**  The specified entry is converted to double precision if necessary
      *  and returned. If the series is complex, the real part of the 
      *  indicated element is returned.
      *  @brief   Get ith element as a double precision number.
      *  @param  index Index of the element to be returned.
      *  @return ith element value.
      */
    double getDouble( size_type index ) const;

    /**  The GPS time of the last plus one is returned as a Time object.
      *  @brief Get the end time of the series.
      *  @return End time of the series.
      */
    Time getEndTime( void ) const;

    /**  getF0 returns the heterodyne frequency used if the time series
      *  was frequency shifted.
      *  @brief   Get heterodyne frequency.
      *  @return Frequency used to down-convert frequencies.
      */
    double
    getF0( void ) const
    {
        return mF0;
    }

    /**  getFNyquist returns the positive Nyquist frequency of the %TSeries.
      *  @brief   Get the Nyquist frequency.
      *  @return Maximum meaningful frequency.
      */
    double
    getFNyquist( void ) const
    {
        return mFNyquist;
    }

    /**  Returns the total time duration of the time series.
      *  @brief   Get series duration.
      *  @return %Time duration of the series.
      */
    Interval getInterval( void ) const;

    /**  Get a valid pointer to a zero-terminated string containing the series 
      *  name. If the name is undefined a pointer to a null string is returned.
      *  @brief   Get the series name.
      *  @return a constant pointer to the series name string.
      */
    const char* getName( void ) const;

    /**  Returns the number of samples stored in the data vector.
      *  @brief   Get the number of samples
      *  @return the number of samples.
      */
    size_type getNSample( void ) const;

    /**  Get the GPS time of the first sample represented as a Time object.
      *  @brief Get the start time.
      *  @return %Time of first sample.
      */
    Time getStartTime( void ) const;

    /**  The time bin width (the inverse of the sampling frequency) is 
      *  returned as an Interval object.
      *  @brief Get the sample time.
      *  @return the sample time interval.
      */
    Interval getTStep( void ) const;

    /**  Get the TSeries units.
      */
    const char* getUnits( void ) const;

    /**  The average value of the data sequence is calculated. Zero is returned
      *  if the series is empty.
      *  @brief Average value of data.
      *  @return Average data value.
      */
    math_type getAverage( void ) const;

    /**  The average value of the data sequence is calculated. Series elements
      *  are converted to complex if necessary before being averaged.  Zero is 
      *  returned if the series is empty.
      *  @brief Average value of data.
      *  @return Average data value.
      */
    complex_type getComplexAverage( void ) const;

    /**  The maximum value of the data sequence is found. Zero is returned
      *  if the series is empty.
      *  @brief Maximum data value.
      *  @return Maximum data value.
      */
    math_type getMaximum( void ) const;

    /**  The minimum value in the sequence is found. Zero is returned
      *  if the series is empty.
      *  @brief Minimum data value.
      *  @return Minimum data value.
      */
    math_type getMinimum( void ) const;

    /**  Count the number of entries in the range <tt>low <= x < high</tt>.
      *  @brief Number of entries between two limits.
      *  @return Number of entries greater than limit.
      *  @param low  Lower limit.
      *  @param high Upper limit.
      */
    size_type getNBetween( math_type low, math_type high ) const;

    /**  Count the number of entries greater than the specified value.
      *  @brief Entries greater than limit.
      *  @return Number of entries greater than limit.
      *  @param Limit Lower limit.
      */
    size_type getNGreater( math_type Limit ) const;

    /**  Count the number of entries less than the specified value. If the 
      *  current instance is complex, the real part is compared.
      *  \brief Entries less than Limit.
      *  \param Limit Value to which all elements are to be compared.
      *  \return Number of entries less than limit.
      */
    size_type getNLess( math_type Limit ) const;

    /**  Get the effective bandwidth ratio imposed by any windowing.
      *  @brief Window sigma.
      *  @return Window sigma.
      */
    math_type getSigmaW( void ) const;

    /**  Get the status information word.
      *  @brief Data status.
      *  @return Data status word.
      */
    stat_type getStatus( void ) const;

    /**  Test if %TSeries contains either %fComplex or %dComplex type data.
      *  @brief Test for complex data.
      *  @return true if data are complex.
      */
    bool isComplex( void ) const;

    /**  Test if %TSeries is empty.
      *  @brief Test for data.
      *  @return true if no data are available.
      */
    bool empty( void ) const;

    /**  @deprecated Replaced by empty()
      *
      *  Test if %TSeries is empty.
      *  @brief Test for data.
      *  @return true if no data are available.
      */
    bool
    isEmpty( void ) const
    {
        return empty( );
    }

    /**  Test the data status. The data are considered good if the status
      *  word is zero.
      *  @brief Test data status.
      *  @return True if status indicates data are good.
      */
    bool isGood( void ) const;

    /**  Get a constant pointer to the data vector.
      *  @brief Get a reference to the data vector.
      *  @return a constant pointer to the data vector.
      */
    const DVector*
    refDVect( void ) const
    {
        return mData.get( );
    }

    /**  Get a constant pointer to the data.
      *  @brief   Get a constant reference to the data.
      *  @return a pointer to the data.
      */
    const void* refData( void ) const;

    /**  Get a pointer to the data.
      *  @brief   Get a reference to the data.
      *  @return a pointer to the data.
      */
    void* refData( void );

    //------------------------------  Mutators
    /**  The specified character string is appended to the time series name 
      *  string.
      *  @brief   Append a string to the series name.
      *  @param  name Constant pointer to a character array to be appended 
      *               to the series name.
      */
    void appName( const char* name );

    /**  Combine the data status word with the specified value.
      *  @brief Set the data status.
      *  @param stat Status value.
      */
    void combineStatus( stat_type stat );

    /**  The supplied data are converted if necessary to the vector data 
      *  type and copied to the series vector. If the vector is not defined, 
      *  a vector of short integers will be created.
      *  @brief   Set the series data.
      *  @param  t    New series start time.
      *  @param  Step New series sample interval.
      *  @param  data Short integer array containing new sample
      *  @param  N    Number of data words in 'data'
      *  @return 0 if successful.
      */
    int setData( const Time&     t,
                 const Interval& Step,
                 const short*    data,
                 size_type       N );

    /**  The supplied data are converted if necessary to the vector data 
      *  type and copied to the series vector. If the vector is not defined, 
      *  a vector of floats will be created.
      *  @brief   Set the series data.
      *  @param  t    New series start time.
      *  @param  Step New series sample interval.
      *  @param  data Float array containing new sample
      *  @param  N    Number of data words in 'data'
      *  @return 0 if successful.
      */
    int setData( const Time&     t,
                 const Interval& Step,
                 const int*      data,
                 size_type       N );

    /**  The supplied data are converted if necessary to the vector data 
      *  type and copied to the series vector. If the vector is not defined, 
      *  a vector of floats will be created.
      *  @brief   Set the series data.
      *  @param  t    New series start time.
      *  @param  Step New series sample interval.
      *  @param  data Float array containing new sample
      *  @param  N    Number of data words in 'data'
      *  @return 0 if successful.
      */
    int setData( const Time&     t,
                 const Interval& Step,
                 const float*    data,
                 size_type       N );

    /**  The supplied data are converted if necessary to the vector data 
      *  type and copied to the series vector. If the vector is not defined, 
      *  a vector of floats will be created.
      *  @brief   Set the series data.
      *  @param  t    New series start time.
      *  @param  Step New series sample interval.
      *  @param  data Float array containing new sample
      *  @param  N    Number of data words in 'data'
      *  @return 0 if successful.
      */
    int setData( const Time&     t,
                 const Interval& Step,
                 const double*   data,
                 size_type       N );

    /**  The supplied data are converted if necessary to the vector data 
      *  type and copied to the series vector. If the vector is not defined, 
      *  a vector of fComplex will be created.
      *  @brief   Set the series data.
      *  @param  t    New series start time.
      *  @param  Step New series sample interval.
      *  @param  data fComplex array containing new sample
      *  @param  N    Number of data words in 'data'
      *  @return 0 if successful.
      */
    int setData( const Time&     t,
                 const Interval& Step,
                 const fComplex* data,
                 size_type       N );

    /**  The supplied data are converted if necessary to the vector data 
      *  type and copied to the series vector. If the vector is not defined, 
      *  a vector of fComplex will be created.
      *  @brief   Set the series data.
      *  @param  t    New series start time.
      *  @param  Step New series sample interval.
      *  @param  data fComplex array containing new sample
      *  @param  N    Number of data words in 'data'
      *  @return 0 if successful.
      */
    int setData( const Time&     t,
                 const Interval& Step,
                 const dComplex* data,
                 size_type       N );

    /**  The supplied data is adopted by the %TSeries. Any existing data 
      *  vector is deleted. The specified vector may be the same as that 
      *  currently owned by the %TSeries.
      *  @brief   Set the series data.
      *  @param  t    New series start time.
      *  @param  Step New series sample interval.
      *  @param  data Data vector containing new sample
      *  @return 0 if successful.
      */
    int setData( const Time& t, const Interval& Step, DVector* data );

    /**  Store the specified frequency in the %TSeries central frequency field.
      *  The data are left unmodified. The central frequency is defined as the 
      *  true frequency of the bin that is apparently DC.
      *  @brief  Set the heterodyne frequency.
      *  @param f0 Frequency to which the %TSeries heterodyne frequency 
      *            will be set.
      */
    void setF0( double f0 );

    /**  Define the Nyquist frequency of the %TSeries. The nyquist frequency is 
      *  set to 2/dT in the constructors and must be set as appropriate by any
      *  decimation filter.
      *  @brief  Set the Nyquist frequency.
      *  @param fNy Nyquist frequency of the %TSeries.
      */
    void setFNyquist( double fNy );

    /**  The specified name is copied to the series name string.
      *  @brief Name a series.
      *  @param name constant pointer to series name character array.
      */
    void setName( const char* name );

    /**  Set the window effective bin width. This is the RMS of any windowing
      *  function used, i.e. <tt>sw = sqrt( Sum(i=0, N ; w(i)^2)/N)</tt>.
      *  @brief Set the window sigma.
      *  @param sw Window RMS.
      */
    void setSigmaW( math_type sw );

    /**  Set the data status word to the specified value.
      *  @brief Set the data status.
      *  @param stat Status value.
      */
    void setStatus( stat_type stat );

    /**  Set the units for the time series.
      *  \brief Set the unit string.
      *  \param units Units description string.
      */
    void setUnits( const std::string& units );

    /**  The specified data are converted if necessary to the series
      *  data type, and appended to the series. If data already exist in 
      *  the vector, the end time of the existing data must match the start
      *  time of the new data. The step-size of the data to be 
      *  appended must also match that of the existing data.
      *  @brief   Append data to the time series.
      *  @param  t    Start time of data to be appended to the series.
      *  @param  Step sample interval of data to be appended to the series.
      *  @param  data Short integer array containing the data to be appended.
      *  @param  N    Number of data words to be appended to the series.
      *  @return
      *   <table> 
      *    <tr><td>  0 </td><td> successful</td></tr>
      *    <tr><td> -1 </td><td> start time isn't equal to the current 
      *                          end time</td></tr>
      *    <tr><td> -2 </td><td> sample intervals aren't equal.</td></tr>
      *   </table>
      */
    int Append( const Time&     t,
                const Interval& Step,
                const short*    data,
                size_type       N );

    /**  The specified data are converted if necessary to the series
      *  data type, and appended to the series. If data already exist in 
      *  the vector, the end time of the existing data must match the start
      *  time of the new data. The step-size of the data to be 
      *  appended must also match that of the existing data.
      *  @brief Append data to the time series.
      *  @param t    Start time of data to be appended to the series.
      *  @param Step sample interval of data to be appended to the series.
      *  @param data Float array containing the data to be appended.
      *  @param N    Number of data words to be appended to the series.
      *  @return
      *   <table> 
      *    <tr><td>  0 </td><td> successful</td></tr>
      *    <tr><td> -1 </td><td> start time isn't equal to the current end 
      *                          time</td></tr>
      *    <tr><td> -2 </td><td> sample intervals aren't equal.</td></tr>
      *   </table>
      */
    int
    Append( const Time& t, const Interval& Step, const int* data, size_type N );

    /**  The specified data are converted if necessary to the series
      *  data type, and appended to the series. If data already exist in 
      *  the vector, the end time of the existing data must match the start
      *  time of the new data. The step-size of the data to be 
      *  appended must also match that of the existing data.
      *  @brief Append data to the time series.
      *  @param t    Start time of data to be appended to the series.
      *  @param Step sample interval of data to be appended to the series.
      *  @param data Float array containing the data to be appended.
      *  @param N    Number of data words to be appended to the series.
      *  @return
      *   <table> 
      *    <tr><td>  0 </td><td> successful</td></tr>
      *    <tr><td> -1 </td><td> start time isn't equal to the current 
      *                          end time</td></tr>
      *    <tr><td> -2 </td><td> sample intervals aren't equal.</td></tr>
      *   </table>
      */
    int Append( const Time&     t,
                const Interval& Step,
                const float*    data,
                size_type       N );

    /**  The specified data are converted if necessary to the series
      *  data type, and appended to the series. If data already exist in 
      *  the vector, the end time of the existing data must match the start
      *  time of the new data. The step-size of the data to be 
      *  appended must also match that of the existing data.
      *  @brief Append data to the time series.
      *  @param t    Start time of data to be appended to the series.
      *  @param Step sample interval of data to be appended to the series.
      *  @param data Float array containing the data to be appended.
      *  @param N    Number of data words to be appended to the series.
      *  @return
      *   <table> 
      *    <tr><td>  0 </td><td> successful</td></tr>
      *    <tr><td> -1 </td><td> start time isn't equal to the current 
      *                          end time</td></tr>
      *    <tr><td> -2 </td><td> sample intervals aren't equal.</td></tr>
      *   </table>
      */
    int Append( const Time&     t,
                const Interval& Step,
                const double*   data,
                size_type       N );

    /**  The specified data are converted if necessary to the series
      *  data type, and appended to the series. If data already exist in 
      *  the vector, the end time of the existing data must match the start
      *  time of the new data. The step-size of the data to be 
      *  appended must also match that of the existing data.
      *  @brief Append data to the time series.
      *  @param t    Start time of data to be appended to the series.
      *  @param Step sample interval of data to be appended to the series.
      *  @param data Complex array containing the data to be appended.
      *  @param N    Number of data words to be appended to the series.
      *  @return
      *   <table> 
      *    <tr><td>  0 </td><td> successful</td></tr>
      *    <tr><td> -1 </td><td> start time isn't equal to the current 
      *                          end time</td></tr>
      *    <tr><td> -2 </td><td> sample intervals aren't equal.</td></tr>
      *   </table>
      */
    int Append( const Time&     t,
                const Interval& Step,
                const fComplex* data,
                size_type       N );

    /**  The specified data are converted if necessary to the series
      *  data type, and appended to the series. If data already exist in 
      *  the vector, the end time of the existing data must match the start
      *  time of the new data. The step-size of the data to be 
      *  appended must also match that of the existing data.
      *  @brief Append data to the time series.
      *  @param t    Start time of data to be appended to the series.
      *  @param Step sample interval of data to be appended to the series.
      *  @param data Complex array containing the data to be appended.
      *  @param N    Number of data words to be appended to the series.
      *  @return
      *   <table> 
      *    <tr><td>  0 </td><td> successful</td></tr>
      *    <tr><td> -1 </td><td> start time isn't equal to the current 
      *                          end time</td></tr>
      *    <tr><td> -2 </td><td> sample intervals aren't equal.</td></tr>
      *   </table>
      */
    int Append( const Time&     t,
                const Interval& Step,
                const dComplex* data,
                size_type       N );

    /**  The argument %TSeries is optionally decimated and appended to the 
      *  series. If the argument series is empty, Append() returns a success
      *  code with no change to the series. The starting time of the argument
      *  series must be equal to the end time of the existing data and the 
      *  sample rate must be the same as the object sample rate. Decimation 
      *  is not yet implemented.
      *  @brief   Concatenate two %TSeries.
      *  @param  ts       time series to be appended.
      *  @param  decim8	decimation factor.
      *  @return
      *   <table> 
      *    <tr><td>  0 </td><td> successful</td></tr>
      *    <tr><td> -1 </td><td> start time isn't equal to the current 
      *                          end time</td></tr>
      *    <tr><td> -2 </td><td> sample intervals aren't equal.</td></tr>
      *   </table>
      */
    int Append( const TSeries& ts, size_type decim8 = 1 );

    /**  The data vector is converted to the specified type. Valid types
      *  are those enumerated in DVector::DVType.
      *  @brief Convert the data vector type. 
      *  @param type The type to which the series data are to be converted.
      */
    void Convert( int type );

    /**  The data vector is extended to hold the specified number of data 
      *  words. No action is taken if the vector can already hold the 
      *  specified number of data words. The current data are copied if 
      *  vector is reallocated.
      *  @brief Expand the data vector. 
      *  @param len Desired vector capacity in words.
      */
    void ReSize( size_type len );

    /**  The current data vector length is set to zero. The data vector 
      *  remains allocated with the original storage capacity. The start
      *  time and sample interval may be reset or are placed in a "don't
      *  care" state. The series name is left unchanged.
      *  @brief Delete the series data.
      *  @param t0 New start time. By default the time is zeroed.
      *  @param dt New sample interval. By default the interval is zeroed.
      */
    void Clear( const Time&     t0 = Time( 0 ),
                const Interval& dt = Interval( 0.0 ) );

    /**  fShift up-converts (heterodynes) the time series by the specified
      *  frequency shift. The resulting data vector will be of fComplex type.
      *  The sign of the frequency shift is such that the DC bin is converted 
      *  to the shift frequency. The %TSeries f0 value is decreased by an equal 
      *  amount.
      *  @brief   Up-convert (heterodyne) a time series.
      *  @param  f0 heterodyne frequency.
      *  @param  phi0 Phase shift of the first sample (defaults to 0).
      *  @return A complex %TSeries with the heterodyned data.
      */
    TSeries fShift( math_type f0, math_type phi0 = 0 ) const;

    /**  The %TSeries data are copied to the current %TSeries. The current 
      *  %TSeries type and name remain unchanged.
      *  @brief   Assignment operator.
      *  @param  ts Constant %TSeries to be copied.
      *  @return Reference to the updated %TSeries.
      */
    TSeries& operator=( const TSeries& ts );

    /**  The %TSeries data are moved to the current %TSeries. The current 
      *  %TSeries type and name remain unchanged.
      *  @brief   Assignment operator.
      *  @param  ts Constant %TSeries to be copied.
      *  @return Reference to the updated %TSeries.
      */
    TSeries& operator=( TSeries&& ts );

    /**  Take an inner (dot) product of the two data vectors.
      *  \brief Correlate two series.
      *  \param y Series with which the inner product is to be calculated.
      *  @return Correlation coefficient (\c Sum(x[i]*y[i]) ).
      */
    math_type operator*( const TSeries& y ) const;

    /**  The argument value is converted to the %TSeries data type and added 
      *  to each element of the time series.
      *  @brief   Bias a %TSeries.
      *  @param  bias Value added to each element.
      *  @return Reference to the modified %TSeries.
      */
    TSeries& operator+=( math_type bias );

    /**  The argument value is converted to the %TSeries data type and added 
      *  to each element of the time series.
      *  @brief   Bias a %TSeries.
      *  @param  bias Value added to each element.
      *  @return Reference to the modified %TSeries.
      */
    TSeries& operator+=( complex_type bias );

    /**  The argument value is converted to the %TSeries data type and 
      *  subtracted from each element of the time series.
      *  @brief   Bias a %TSeries.
      *  @param  bias Value subtracted from each element.
      *  @return Reference to the modified %TSeries.
      */
    TSeries& operator-=( math_type bias );

    /**  The argument value is converted to the %TSeries data type and added 
      *  to each element of the time series.
      *  @brief   Bias a %TSeries.
      *  @param  bias Value added to each element.
      *  @return Reference to the modified %TSeries.
      */
    TSeries& operator-=( complex_type bias );

    /**  Each element of the time series is multiplied by the rhs value.
      *  The result is converted to the series data type and replaces the
      *  original series.
      *  @brief  Scale a %TSeries.
      *  @param  scale Value used to multiply each element.
      *  @return Reference to the modified %TSeries.
      */
    TSeries& operator*=( math_type scale );

    /**  Each element of the time series is multiplied by the complex rhs 
      *  value. Real series are multiplied by the real part of the rhs. The
      *  resulting series retains the initial data type.
      *  @brief  Scale a %TSeries.
      *  @param  scale Value used to multiply each element.
      *  @return Reference to the modified %TSeries.
      */
    TSeries& operator*=( complex_type scale );

    /**  Each element of the time series is divided by the rhs value.
      *  The result is converted to the series data type and replaces the
      *  original series.
      *  @brief  Scale a %TSeries.
      *  @param  scale Value used to divide each element.
      *  @return Reference to the modified %TSeries.
      */
    TSeries& operator/=( math_type scale );

    /**  Each element of the time series is incremented by the corresponding
      *  element of the argument. The \a rhs argument must have the same 
      *  sample rate and contain a subset of the times in the series being 
      *  added to. If the sample rates differ or the \a rhs time bins 
      *  don't correspond exactly to existing time bins, a 
      *  <tt>std::runtime_error</tt> exception is thrown.
      *  @brief   Add a %TSeries substring.
      *  @exception std::runtime_error if binning doesn't match.
      *  @param  rhs %TSeries to be added to the current %TSeries.
      *  @return Reference to the modified %TSeries.
      */
    TSeries& add_overlap( const TSeries& rhs );

    /**  Add element by element of the \a rhs series to this series.
      *  A runtime_error exception is thrown if the start time, number of 
      *  elements or sammple rate differ between the two series.
      *  \brief  add a %TSeries.
      *  \exception std::runtime_error if bin times aren't identical.
      *  \param  rhs %TSeries to be subtracted from the current %TSeries.
      *  \return Reference to the result series (this).
      */
    TSeries& operator+=( const TSeries& rhs );

    /**  Each element of the time series is decremented by the corresponding
      *  elements of the argument. The argument must have the 
      *  same sampling rate and contain a subset of the times in the series 
      *  being added to. If the sample rates differ or the \a rhs time 
      *  bins don't correspond exactly to existing time bins, a 
      *  <tt>std::runtime_error</tt> exception is thrown.
      *  @brief   Subtract a %TSeries.
      *  @exception std::runtime_error if binning doesn't match.
      *  @param  rhs %TSeries to be subtracted from the current %TSeries.
      *  @return Reference to the difference %TSeries.
      */
    TSeries& subtract_overlap( const TSeries& rhs );

    /**  Subtract element by element of the \a rhs series from this series.
      *  A runtime_error exception is thrown if the start time, number of 
      *  elements or sammple rate differ between the two series.
      *  \brief subtract a %TSeries substring.
      *  \exception std::runtime_error if bin times aren't identical.
      *  \param  rhs %TSeries to be subtracted from the current %TSeries.
      *  \return Reference to the result series (this).
      */
    TSeries& operator-=( const TSeries& rhs );

    /**  Each element of the time series is multiplied by the corresponding
      *  elements of the argument. The argument must have the 
      *  same sampling rate and contain a subset of the times in the series 
      *  being multiplied. If the sample rates differ or the \a rhs time 
      *  bins don't correspond exactly to existing time bins, a 
      *  <tt>std::runtime_error</tt> exception is thrown.
      *  @brief Multiply a %TSeries substring.
      *  @exception std::runtime_error if binning doesn't match.
      *  @param  rhs %TSeries to be multiplied into the current %TSeries.
      *  @return Reference to the product %TSeries.
      */
    TSeries& multiply_overlap( const TSeries& rhs );

    /**  Multiply element by element of this series by the \a rhs series.
      *  A runtime_error exception is thrown if the start time, number of 
      *  elements or sammple rate differ between the two series.
      *  \brief Multiply by a %TSeries.
      *  \exception std::runtime_error if bin times aren't identical.
      *  \param  rhs %TSeries by which to multiply the current %TSeries.
      *  \return Reference to the result series (this).
      */
    TSeries& operator*=( const TSeries& y );

    /**  Each element of the time series is divided by the corresponding
      *  elements of the argument. The argument must have the 
      *  same sampling rate and contain a subset of the times in the series 
      *  being added to. If the sample rates differ or the \a rhs time 
      *  bins don't correspond exactly to existing time bins, a 
      *  <tt>std::runtime_error</tt> exception is thrown. The resulting series
      *  type is promoted to float, double or dCOmplex depending on the input
      *  data types. 
      *  @brief   Subtract a %TSeries.
      *  @exception std::runtime_error if binning doesn't match.
      *  @param  rhs %TSeries to be divided from the current %TSeries.
      *  @return Reference to the ratio %TSeries.
      */
    TSeries& divide_overlap( const TSeries& y );

    /**  Divide each element of this series by the coresponding element of the
      *  \a rhs series. A runtime_error exception is thrown if the start time, 
      *  number of elements or sample rate differ between the two series.
      *  \brief Multiply by a %TSeries.
      *  \exception std::runtime_error if bin times aren't identical.
      *  \param  rhs %TSeries by which to multiply the current %TSeries.
      *  \return Reference to the result series (this).
      */
    TSeries& operator/=( const TSeries& y );

    /**  Get a pointer to the data vector.
      *  @brief   Get a reference to the data vector.
      *  @return A pointer to the data vector.
      */
    DVector*
    refDVect( void )
    {
        return mData.get( );
    }

    /**  Test for consistency with end of series. To be contiguous, the 
      *  specified sample interval (\a Step) must be equal to the series
      *  time-step and the specified time must be equal to the series end
      *  time. An offset of less than one namosecond is allowed. If the series 
      *  is empty, it is assumed to be contiguous and the start time and time 
      *  step are set accordingly.
      *  \brief Test for contiguous time.
      *  \param t    Start time to be tested.
      *  \param Step Sample interval to be tested.
      *  \return <ul>
      *    <li> 0: %Time and sample step are contiguous with the series </li>
      *    <li> -1: %Time is not contiguous with the series </li>
      *    <li> -2: Step is not consistent with the series </li>
      *  </ul>
      */
    int Contig( const Time& t, const Interval& Step );

    /**  Overlap finds the ranges of two time series that overlap in time. The
      *  time series must have equal time steps and their start times must 
      *  differ by an integer number of time steps.
      *  \brief Find range of overlapping samples. 
      *  \exception runtime_error: Time step mismatch.
      *  \exception runtime_error: Times not aligned.
      *  \param ts   %Time series to be tested for overlap with this instance.
      *  \param inx  Reference to variable to receive index into this instance
      *              of the start of overlapping data.
      *  \param inx1 Reference to variable to receive index into the arguemnt
      *              series of the start of overlapping data.
      *  \return Number of overlapping samples.
      */
    size_type
    overlap( const TSeries& ts, size_type& inx, size_type& inx1 ) const;

    /** Null TSeries to be used for null arguments, etc.
     */
    static const TSeries null_tseries;

private:
    /**  Series Name
     */
    std::string mName;

    /**  GPS time of the first sample in the series. If the series represents
     *  a physical quantity integrated over a time bin, the first time bin
     *  is from T0 - T0+dT.
     *  @brief Starting absolute time.
     */
    Time mT0;

    /**  %Time increment between successive samples in the time series.
     *  @brief %Time increment or bin size.
     */
    Interval mDt;

    /**  Frequency shift if the series source was heterodyned.
     *  @brief Frequency shift.
     */
    double mF0;

    /**  Nyquist frequency of %TSeries.
     *  @brief Nyquist requency.
     */
    double mFNyquist;

    /**  The data status word defaults to zero when the %TSeries is constructed
     *  and may be set to a non-zero value to indicate dubious data quality. 
     *  @brief Data status word.
     */
    stat_type mStatus;

    /**  Data vector pointer.
     */
    std::unique_ptr< DVector > mData;

    /**  Effective bin width from windowing.
     */
    math_type mSigmaW;

    /**  Save the series units.
     *  \brief Units
     */
    std::string mUnits;
};

#ifndef __CINT__
//--------------------------------------  Get TSeries status
inline bool
TSeries::empty( void ) const
{
    return ( getNSample( ) == 0 );
}

inline Time
TSeries::getBinT( size_type inx ) const
{
    Time r( mT0 + mDt * double( inx ) );
    return r;
}

inline Time
TSeries::getEndTime( void ) const
{
    return getBinT( getNSample( ) );
}

inline Interval
TSeries::getInterval( void ) const
{
    return mDt * double( getNSample( ) );
}

inline const char*
TSeries::getName( void ) const
{
    return mName.c_str( );
}

inline Time
TSeries::getStartTime( void ) const
{
    return mT0;
}

inline Interval
TSeries::getTStep( void ) const
{
    return mDt;
}

inline TSeries::math_type
TSeries::getSigmaW( void ) const
{
    return mSigmaW;
}

inline TSeries::stat_type
TSeries::getStatus( void ) const
{
    return mStatus;
}

inline const char*
TSeries::getUnits( void ) const
{
    return mUnits.c_str( );
}

inline bool
TSeries::isGood( void ) const
{
    return ( getStatus( ) == 0 );
}

//--------------------------------------  Set the series data
inline int
TSeries::setData( const Time&     t,
                  const Interval& Step,
                  const short*    data,
                  size_type       N )
{
    Clear( );
    return Append( t, Step, data, N );
}

inline int
TSeries::setData( const Time&     t,
                  const Interval& Step,
                  const int*      data,
                  size_type       N )
{
    Clear( );
    return Append( t, Step, data, N );
}

inline int
TSeries::setData( const Time&     t,
                  const Interval& Step,
                  const float*    data,
                  size_type       N )
{
    Clear( );
    return Append( t, Step, data, N );
}

inline int
TSeries::setData( const Time&     t,
                  const Interval& Step,
                  const double*   data,
                  size_type       N )
{
    Clear( );
    return Append( t, Step, data, N );
}

inline int
TSeries::setData( const Time&     t,
                  const Interval& Step,
                  const fComplex* data,
                  size_type       N )
{
    Clear( );
    return Append( t, Step, data, N );
}

inline int
TSeries::setData( const Time&     t,
                  const Interval& Step,
                  const dComplex* data,
                  size_type       N )
{
    Clear( );
    return Append( t, Step, data, N );
}

//--------------------------------------  Bias a series.
inline TSeries&
TSeries::operator-=( math_type bias )
{
    return operator+=( -bias );
}

//--------------------------------------  Bias a series.
inline TSeries&
TSeries::operator-=( complex_type bias )
{
    return operator+=( -bias );
}

//--------------------------------------  Scale a series.
inline TSeries&
TSeries::operator/=( math_type scale )
{
    return operator*=( 1.0 / scale );
}

#endif // __CINT__

/** @name %TSeries functions
 * This section contains functions of %TSeries that are not members of the 
 * %TSeries class. 
 * @brief Non-member functions of %TSeries.
 * @author John G. Zweizig
 * @version 1.1; Last modified October 4, 1999
 */
//@{

/**  Difference of two %TSeries.
 *  @brief Difference of two %TSeries.
 *  @return %TSeries difference between the two arguments.
 *  @param t1 Starting series
 *  @param t2 Series to be subtracted from <tt>t1</tt>.
 */
inline TSeries
operator-( const TSeries& t1, const TSeries& t2 )
{
    TSeries r( t1 );
    r -= t2;
    return r;
}

/**  Sum of two %TSeries.
 *  @brief Sum of two %TSeries.
 *  @return %TSeries sum of the two arguments.
 *  @param t1 Starting series
 *  @param t2 Series to be added to <tt>t1</tt>.
 */
inline TSeries
operator+( const TSeries& t1, const TSeries& t2 )
{
    TSeries r( t1 );
    r += t2;
    return r;
}
//@}
#endif //  TSERIES_HH
