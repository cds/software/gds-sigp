#ifndef FSERIES_HH
#define FSERIES_HH

#include <string>
#include <iosfwd>
#include <memory>
#include "Complex.hh"
#include "Time.hh"
#include "Interval.hh"
#include "gds_memory.hh"

class TSeries;
class DVector;
class Chirp;

/**  The FSeries class is used to represent a series in the frequency
  *  domain, e.g. the Fourier transform coefficients of a given signal.
  *  The coefficient normalization is set such that the magnitude squared
  *  gives a meaningful power spectral density (psd). The Fourier 
  *  coefficients should are normalized so that the measured power of a 
  *  flat spectrum is independent of bin size i.e. the amplitude coefficient 
  *  must be be in units of root-seconds. Two tests for correct normalization:
  *  <ol>
  *    <li> The total power in a fourier series is equal to the $RMS^2$ of the
  *         original time series (Parseval's theorem) i.e. for Y = fft(X)
  *         $Sum(|Yi|^2) = Sum(Xi^2)/N$ </li>
  *    <li> The power density ($psd = |Yi|^2$) of a sinusoid is equal to
  *         $psd = A^2*dT/2$ where A is the amplitude of the sinusoid, and dT 
  *         is the total time covered by the time series (inverse of the 
  *         frequency spacing).</li>
  *  </ol>
  *  Note that the amplitude is defined to give the correct power density 
  *  without adding positive and negative frequencies in double-ended
  *  FSeries.
  *  @brief   Frequency series class.
  *  @author J. Zweizig
  *  @version 1.4; Modified December 5, 2007
  */
class FSeries
{
public:
    /**  Data type used to hold complex fourier coefficients.
    *  @brief Complex data type.
    */
    typedef fComplex Complex;

    /**  Data type used to hold vector lengths.
    *  @brief Length data type.
    */
    typedef unsigned long size_type;

    /**  Enumerate data storage modes. Single sided series are stored linearly
    *  from Fo-Fnyquist in increasing bins. Double sided series are stored
    *  with Fmin -> Fo -> Fmax in bins 0 -> N-1.
    *  @brief Data Storage mode.
    */
    enum DSMode
    {
        kEmpty,
        kFull,
        kFolded
    };

    /**  Construct an empty FSeries.
    *  @brief Default constructor.
    */
    FSeries( void );

    /**  Construct an FSeries and initialize it from another FSeries.
    *  @brief Copy constructor.
    *  @param fs FSeries to be copied.
    */
    FSeries( const FSeries& fs );

    /**  Construct an FSeries with a specified frequency offset, frequency
    *  bin and float input data.
    *  @brief Data constructor.
    *  @param f0   Lowest frequency to be contained by the FSeries (in Hz)
    *  @param dF Frequency step between adjacent elements (in Hz) 
    *  @param t0   Start time of the data represented by the FSeries
    *  @param dT   Time interval of the data represented by the FSeries.
    *  @param NData Number of data words to be loaded into the FSeries.
    *  @param dData Float data to be loaded into the FSeries.
    */
    FSeries( double      f0,
             double      dF,
             Time        t0,
             Interval    dT,
             size_type   NData,
             const float dData[] );

    /**  Construct an FSeries with a specified frequency offset, frequency
    *  bin and Complex input data.
    *  @brief Data constructor.
    *  @param f0   Lowest frequency to be contained by the FSeries (in Hz)
    *  @param dF   Frequency step between adjacent elements (in Hz) 
    *  @param t0   Start time of the data represented by the FSeries
    *  @param dT   Time interval of the data represented by the FSeries.
    *  @param NData Number of data words to be loaded into the FSeries.
    *  @param dData Complex data to be loaded into the FSeries.
    */
    FSeries( double        f0,
             double        dF,
             Time          t0,
             Interval      dT,
             size_type     NData,
             const Complex dData[] );

    /**  Construct an FSeries with a specified frequency offset, frequency
    *  bin and data vector. The data  vector is cloned for the TSeries.
    *  @brief Data constructor.
    *  @param f0  Lowest frequency to be contained by the FSeries (in Hz)
    *  @param dF  Frequency step between adjacent elements (in Hz)
    *  @param t0  Start time of the data represented by the FSeries
    *  @param dT  Time interval of the data represented by the FSeries.
    *  @param dv  DVector to be loaded into the FSeries.
    */
    FSeries( double f0, double dF, Time t0, Interval dT, const DVector& dv );

    /**  Construct an FSeries with a specified frequency offset, frequency
    *  bin and data vector. Ownership of the data vector is passed to the
    *  FSeries.
    *  @brief Data constructor.
    *  @param f0  Lowest frequency to be contained by the FSeries (in Hz)
    *  @param dF  Frequency step between adjacent elements (in Hz)
    *  @param t0  Start time of the data represented by the FSeries
    *  @param dT  Time interval of the data represented by the FSeries.
    *  @param dv  DVector to be loaded into the FSeries.
    */
    FSeries( double   f0,
             double   dF,
             Time     t0 = Time( 0 ),
             Interval dT = 0,
             DVector* dv = 0 );

    /**  Construct an FSeries. Intialized it from the FFT of a time series.
    *  The normalization of the series is as described for setData(TSeries&).
    *  @brief FFT constructor.
    *  @param tData Time series from which the FSeries will be constructed.
    */
    FSeries( const TSeries& tData );

    /**  Construct an FSeries. Intialize it from a Chirp template.
    *  @brief Template constructor.
    */
    FSeries( double f0, double dF, size_type NData, const Chirp& func );

    /**  Destroy a series object.
    *  @brief FSeries Destructor.
    */
    ~FSeries( void );

    //------------------------------  Accessors
    /**  Pad the FSeries to the specified maximum frequency.
    *  @brief   Extend the FSeries.
    *  @param  fmax New maximum frequency (in Hz)
    */
    void extend( double fmax );

    /**  Returns an FSeries containing a subset of the parent FSeries.
    *  @brief  Get a substring of the FSeries.
    *  @param f0    Lowest frequency to be extracted from the FSeries (in Hz)
    *  @param dF    Frequency interval to be extracted (in Hz) 
    *  @return The specified sub-series
    */
    FSeries extract( double f0, double dF ) const;

    /**  Optionally convert and copy the first 'len' entries of a series to 
    *  a complex array.
    *  @brief Get complex series data.
    *  @param len  Maximum number of words to be copied.
    *  @param data Complex buffer into which the data will be copied.
    *  @return The number of entries copied
    */
    size_type getData( size_type len, Complex* data ) const;

    /**  Optionally convert and copy the first 'len' entries of a series to 
    *  a float array. If the FSeries is complex, only the real part of the
    *  data are returned.
    *  @brief Get float series data.
    *  @param len  Maximum number of entries to be copied.
    *  @param data Float buffer into which the data will be copied.
    *  @return The number of entries copied
    */
    size_type getData( size_type len, float* data ) const;

    /**  Returns the time interval as specified in the sourrce data.
    *  @brief Get the time interval.
    *  @return The time interval of the data summarized by this series.
    */
    Interval getDt( void ) const;

    /**  Returns the End time as specified in the source data.
    *  @brief Get the End time.
    *  @return The end time of the data summarized by this series.
    */
    Time getEndTime( void ) const;

    /**  Returns the minimum frequency covered by the data.
    *  @brief Get the minimum frequency.
    *  @return Lowest frequency represented in the FSeries in Hz.
    */
    double getLowFreq( void ) const;

    /**  Get the Number of data words.
    */
    size_type getLength( void ) const;

    /**  Returns the maximum (Nyquist) frequency of the data.
    *  @brief Get the maximum Frequency.
    *  @return Highest frequency represented in the FSeries in Hz.
    */
    double getHighFreq( void ) const;

    /**  Returns the center frequency of the data (the low frequency for 
    *  single sided storage).
    *  @brief Get the maximum Frequency.
    *  @return Highest frequency represented in the FSeries in Hz.
    */
    double getCenterFreq( void ) const;

    /**  Returns the Frequency interval between two adjacent points of the 
   *   FSeries.
    *  @brief Get the frequency step.
    *  @return The frequency interval in Hz.
    */
    double getFStep( void ) const;

    /**  Returns a pointer to the frequency series name.
    *  @brief Get the series name.
    *  @return A constant pointer to the series name.
    */
    const char* getName( void ) const;

    /**  Returns the number of frequency steps. Note that the number of
    *  data points is in fact one greater than the number of steps
    *  because both the f=0 and f=Nyquist entries are included.
    *  @brief Get the number of frequency steps.
    *  @return the number of frequency steps.
    */
    size_type getNStep( void ) const;

    /**  Returns the start time as specified in the source data.
    *  @brief Get the start time.
    *  @return The start time of the data summarized by this series.
    */
    Time getStartTime( void ) const;

    /**  Interpolate the current FSeries in the specified frequency range
    *  (<tt>fMin - fMax</tt>) at points separated by the specified frequency 
    *  spacing (<tt>df</tt>). The returned FSeries is a single sided 
    *  series starting at <tt>f=0</tt> with the specified spacing and points 
    *  up to (but not including) fMax. It is non-zero only in the specified 
    *  range. No interpolation is performed if the requested frequency step 
    *  is an integer multiple of the current step. If interpolation is 
    *  necessary, it may be performed either linearly or logarithmically as 
    *  specified by the <tt>logar</tt> parameter.
    *  @brief Interpolate the FSeries points
    *  @return Interpolated frequency series.
    *  @param fMin  Minimum non-zero frequency of returned series
    *  @param fMax  Maximum frequency of returned series
    *  @param df    Frequency step of returned series.
    *  @param logar If true, logarithmic interpolation.
    */
    FSeries interpolate( double fMin,
                         double fMax,
                         double df,
                         bool   logar = false ) const;

    /**  Returns a constant pointer to the data or NULL if the DVector hasn't 
    *  been defined.
    *  @brief Get a pointer to the series data.
    *  @return Constant pointer to the data storage area.
    */
    const void* refData( void ) const;

    /**  Returns a writeablel pointer to the data or NULL if the DVector hasn't 
    *  been defined.
    *  @brief Get a pointer to the series data.
    *  @return Constant pointer to the data storage area.
    */
    void* refData( void );

    /**  Get a constant pointer to the data vector.
    *  @brief Data Vector reference.
    *  @return A pointer to the series data vector.
    */
    const DVector* refDVect( void ) const;

    /**  Get the data vector pointer
    *  @brief Data Vector reference.
    *  @return A pointer to the series data vector.
    */
    DVector* refDVect( void );

    /**  A formatted dump of the FSeries header and data are written to the
    *  output stream.
    *  @brief Dump the contents of the FSeries to an output stream.
    *  @param out I/O stream to which the formatted dump is to be written.
    *  @return The I/O stream passed to the function.
    */
    std::ostream& Dump( std::ostream& out ) const;

    /**  Test if fourrier coefficients are stored in (full) double-sided
    *  format.
    *  @brief Test for double sided storage.
    *  @return true if double sided.
    */
    bool isDoubleSided( void ) const;

    /**  Test if FSeries is empty.
    *  @brief Test for empty series.
    *  @return true if empty..
    */
    bool empty( void ) const;

    /**  Test if fourrier coefficients are stored in (full) double-sided
    *  format.
    *  @brief Test for double sided storage.
    *  @return true if double sided.
    */
    bool isSingleSided( void ) const;

    /**  Test if FSeries DVector is unassigned.
    *  @brief Test for null series.
    *  @return true if null.
    */
    bool null( void ) const;

    /**  The spectral power is calculated in the band from 'fmin' to 'fmax'. 
    *  Because of the normalization used, Power is only summed over 
    *  positive frequencies. If fmin or fmax is not specified, it is 
    *  replaced by the minimum or maximum frequency of the series, 
    *  respectively. The specified frequency limits are rounded to the 
    *  series frequency bins to avoid interpolation.
    *  @brief Get the power in a band.
    *  @param fmin Minimum frequency over which the Power is summed.
    *  @param fmax Maximum frequency over which the Power is summed.
    */
    float Power( float fmin = 0.0, float fmax = 0.0 ) const;

    //------------------------------  Mutators
    /**  The specified string is appended to the existing series name.
    *  @brief Append a string to the series name.
    *  @param name String to be apended to the series name.
    */
    void appName( const char* name );

    /**  Delete the data vector.
    *  @brief Clear the data vector.
    */
    void clear( void );

    /**  The float data in 'data' are optionally converted to the data vector
    *  type and then used to overwrite the series data.
    *  @brief Overwrite the series with float data.
    *  @param len  Number of data words to be written to the FSeries.
    *  @param data A float array holding data to be written to the FSeries.
    */
    void setData( size_type len, const float* data );

    /**  The Complex data in 'data' are optionally converted to the data 
    *  vector type and then used to overwrite the series data.
    *  @brief Overwrite the series with Complex data.
    *  @param len  Number of data words to be written to the FSeries.
    *  @param data A Complex array with the data to be written to the FSeries.
    */
    void setData( size_type len, const Complex* data );

    /**  The current data are cleared and the DVector 'data' is taken over by 
    *  the FSeries.
    *  @brief Overwrite the series with the specified data vector.
    *  @param data A DVector to be adopted by the FSeries.
    */
    void setData( DVector* data );

    /**  A time series is Fourier transformed and the result is stored in 
    *  the FSeries. The series is normalized to make the measured power 
    *  of a flat distribution independent of the binsize i.e. the 
    *  (unfolded) DFT in the range 0<f<Fny is multiplied by: 2*sqrt(dt*N)/N
    *  where N is the number of TSeries samples and dt is the time between 
    *  samples. The resulting FSeries contains int((N+1)/2) frequency 
    *  coefficients.
    *  @brief Replace the series with an FFT of a TSeries.
    *  @param data Time series to be transformed and stored in the FSeries.
    */
    void setData( const TSeries& data );

    /**  The series name is set to the 'name' string.
    *  @brief Set the series name.
    *  @param name Series name.
    */
    void setName( const char* name );

    /**  The series time span is set.
    *  @brief Set the time span.
    *  @param t0 Start time of the data from which the FSeries is derived.
    *  @param dT Interval over which the data were derived or valid.
    */
    void setTimeSpan( const Time& t0, Interval dT );

    /**  The series start time is set to 't0'. This is obsolescent.
    *  @brief Set the start time.
    *  @param t0 Start time of the data from which the FSeries is derived.
    */
    void setT0( const Time& t0 );

    /**  The data vector length is increased to accomodate at least the 
    *  specified number of entries. If sufficient storage has already
    *  been allocated, no action is taken.
    *  @brief Increase the data vector storage.
    *  @return Desired minimum storage length in words.
    */
    void ReSize( size_type len );

    /**  The frequency series is replaced by its time derivative, calculated 
    *  by multiplying each coefficient by 2*pi*i*f.
    *  @brief Time derivative.
    */
    void tDerivative( void );

    /**  The frequency series is replaced by its indefinite time integral, 
    *  calculated by multiplying each coefficient by 2*pi*i*f.
    *  @brief Time integral.
    */
    void tIntegral( void );

    /**  Sum the product of the frequency series and the complex conjugate
    *  of the argument.
    *  @brief complex convolution.
    */
    Complex cdot( const FSeries& fs ) const;

    //------------------------------  Overloaded Operators
    /**  The frequency information, Time information and data of the lhs 
    *  series are replaced by those of the rhs series. The data are 
    *  converted to the the type of the lhs series if necessary.
    *  @brief Assignment operator.
    *  @param rhs The series to be copied.
    *  @return a reference to the updated lhs FSeries.
    */
    FSeries& operator=( const FSeries& rhs );

    /**  The rhs constant is added to the lhs series on a element by element
    *  basis. The result replaces the original contents of the lhs series.
    *  @brief Bias an FSeries.
    *  @param bias The constant to be added.
    *  @return a reference to the updated lhs FSeries.
    */
    FSeries& operator+=( double bias );

    /**  The rhs series is added to the lhs series on a element by element 
    *  basis. The result replaces the original contents of the lhs series.
    *  @brief Add two FSeries.
    *  @param rhs The series to be added.
    *  @return a reference to the updated lhs FSeries.
    */
    FSeries& operator+=( const FSeries& rhs );

    /**  The rhs series is subtracted from the lhs series on a element by 
    *  element basis.  The result replaces the original contents of the 
    *  lhs series.
    *  @brief Subtract a series.
    *  @param rhs The series to be subtracted.
    *  @return a reference to the updated lhs FSeries.
    */
    FSeries& operator-=( const FSeries& rhs );

    /**  Each element of the lhs series is multiplied by a scale factor.
    *  @brief Scale a series.
    *  @param scale The scale factor.
    *  @return a reference to the updated lhs FSeries.
    */
    FSeries& operator*=( double scale );

    /**  Each element of the lhs series is multiplied by the corresponding
    *  element of the argument series.
    *  @brief Multiply a series by another.
    *  @param fs The multiplier series.
    *  @return a reference to the updated lhs FSeries.
    */
    FSeries& operator*=( const FSeries& fs );

    /**  Each element of the lhs series is divided by the corresponding
    *  element of the argument series.
    *  @brief Divide a series by another.
    *  @param fs The divisor series.
    *  @return a reference to the updated lhs FSeries.
    */
    FSeries& operator/=( const FSeries& fs );

    /**  The complex amplitude at the closest bin is returned.
    *  @brief Get the amplitude at a specified frequency.
    *  @param freq Frequency in Hz at which the series is to be evaluated.
    *  @return Complex value of the series at the specified frequency.
    */
    Complex operator( )( double freq ) const;

    //-----------------------------  Other functions
    /**  The zero time of the series is shifted by 'dT'. This is implemented 
    *  by multiplying each element by exp(i*dT*f). The Start-time field of
    *  the series is left unchanged.
    *  @brief Evolve the Frequency series in time.
    *  @param dT Time interval over which the series is to be evolved.
    */
    void evolve( const Interval& dT );

    /**  Calculate the bin number closest to a given frequency. The returned
    *  bin number is always valid, i.e. 0 <= bin < length. If the 
    *  frequency is less than mF0 (including negative frequencies in a
    *  single sided series) bin 0 is returned.
    */
    size_type getBin( double f ) const;

    /**  Calculate the frequency of a given bin.
   */
    double getBinF( size_type bin ) const;

private:
    /**  Frequency series name string.
    *  @brief Data Name
    */
    std::string mName;

    /**  Not zero if the series source was heterodyned.
    *  @brief Minimum frequency.
    */
    double mF0;

    /**  Frequency bin size
    */
    double mDf;

    /**  GPS time of the first sample in the series. If the series represents
    *  a physical quantity integrated over a time bin, the first time bin
    *  is from T0 - T0+dT.
    *  @brief Starting absolute time.
    */
    Time mT0;

    /**  Time interval over which data was taken.
    */
    Interval mDt;

    /**  Data array
    */
    std::unique_ptr< DVector > mData;

    /**  Data storage order.
    */
    DSMode mDSMode;
};

#ifndef __CINT__
//---------------------------------------  Test if series is empty
inline bool
FSeries::empty( void ) const
{
    return mDSMode == kEmpty;
}

//---------------------------------------  Test for null data vector
inline bool
FSeries::null( void ) const
{
    return !mData.get( );
}

//--------------------------------------  Find bin closest to a frequency.
inline FSeries::size_type
FSeries::getBin( double f ) const
{
    size_type r = 0;
    if ( f > mF0 )
    {
        r = size_type( ( f - mF0 ) / mDf + 0.5 );
        size_type Nmax = getNStep( );
        if ( mDSMode == kFull )
            --Nmax;
        if ( r > Nmax )
            r = Nmax;
    }
    return r;
}

//---------------------------------------  Calculate the frequency of a bin.
inline double
FSeries::getBinF( size_type bin ) const
{
    double r = mF0 + double( bin ) * mDf;
    return r;
}

inline double
FSeries::getCenterFreq( void ) const
{
    if ( mDSMode == kFolded )
        return mF0;
    return mF0 + double( getNStep( ) / 2 ) * mDf;
}

inline Interval
FSeries::getDt( void ) const
{
    return mDt;
}

inline Time
FSeries::getEndTime( void ) const
{
    return mT0 + mDt;
}

inline double
FSeries::getFStep( void ) const
{
    return mDf;
}

inline double
FSeries::getHighFreq( void ) const
{
    return mF0 + getNStep( ) * mDf;
}

inline double
FSeries::getLowFreq( void ) const
{
    if ( mDSMode == kFull )
        return mF0;
    return mF0 - getNStep( ) * mDf;
}

inline const char*
FSeries::getName( void ) const
{
    return mName.c_str( );
}

inline Time
FSeries::getStartTime( void ) const
{
    return mT0;
}

inline bool
FSeries::isSingleSided( void ) const
{
    return mDSMode == kFolded;
}

inline bool
FSeries::isDoubleSided( void ) const
{
    return mDSMode == kFull;
}

inline DVector*
FSeries::refDVect( void )
{
    return mData.get( );
}

inline const DVector*
FSeries::refDVect( void ) const
{
    return mData.get( );
}

/**  Each entry of the lhs series is multiplied by the corresponding rhs
  *  entry (conjugated in complex series), and the products are summed.
  *  @brief Inner product of two series.
  *  @param rhs Multiplier FSeries
  *  @return The inner product value.
  */
inline FSeries::Complex
operator*( const FSeries& lhs, const FSeries& rhs )
{
    return lhs.cdot( rhs );
}
#endif

#endif //  FSERIES_HH
