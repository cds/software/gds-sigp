/* -*- mode: c++; c-basic-offset: 4; -*- */
//
//   FSeries class implementation
//
#include "Chirp.hh"
#include "DVecType.hh"
#include "FSeries.hh"
#include "TSeries.hh"
#include "fft.hh"
#include "constant.hh"
#include "lcl_array.hh"
#include <stdexcept>
#include <iostream>

using namespace std;

//======================================  Default Constructor
FSeries::FSeries( )
    : mF0( 0.0 ), mDf( 1.0 ), mT0( 0 ), mDt( 0.0 ), mDSMode( kEmpty )
{
}

//======================================  Copy Constructor
FSeries::FSeries( const FSeries& fs )
{
    *this = fs;
}

//======================================  Data Constructor
FSeries::FSeries( double      F0,
                  double      dF,
                  Time        T0,
                  Interval    dT,
                  size_type   NData,
                  const float dData[] )
    : mF0( F0 ), mDf( dF ), mT0( T0 ), mDt( dT )
{
    setData( NData, dData );
}

//======================================  Data Constructor
FSeries::FSeries( double        F0,
                  double        dF,
                  Time          T0,
                  Interval      dT,
                  size_type     NData,
                  const Complex dData[] )
    : mF0( F0 ), mDf( dF ), mT0( T0 ), mDt( dT )
{
    setData( NData, dData );
}

//======================================  Data Constructor
FSeries::FSeries(
    double F0, double dF, Time T0, Interval dT, const DVector& dv )
    : mF0( F0 ), mDf( dF ), mT0( T0 ), mDt( dT )
{
    setData( dv.clone( ) );
}

//======================================  Data Constructor
FSeries::FSeries( double F0, double dF, Time T0, Interval dT, DVector* dv )
    : mF0( F0 ), mDf( dF ), mT0( T0 ), mDt( dT )
{
    setData( dv );
}

//======================================  Template Constructor
FSeries::FSeries( double F0, double dF, size_type NData, const Chirp& func )
    : mF0( F0 ), mDf( dF ), mDSMode( kFolded )
{
    mName = "Chirp";
    mT0 = func.getT0( );
    mDt = func.getTEnd( ) - mT0;
    DVectC DVec( NData );
    for ( size_type i = 0; i < NData; i++ )
    {
        DVec[ i ] = func.Fspace( F0 + i * dF );
    }
    mData.reset( DVec.clone( ) );
}

//======================================  Data Constructor
FSeries::FSeries( const TSeries& TS )
    : mF0( 0.0 ), mDf( 0 ), mT0( 0 ), mDt( 0.0 ), mDSMode( kEmpty )
{
    mName = TS.getName( );
    setData( TS );
}

//======================================  Destructor
FSeries::~FSeries( void )
{
}

//======================================  Accessors
FSeries
FSeries::extract( double f0, double dF ) const
{
    FSeries r( f0, mDf, mT0, mDt );
    if ( empty( ) )
        return r;

    size_type iMin = getBin( f0 );
    size_type iMax = getBin( f0 + dF );
    if ( isSingleSided( ) && iMax < mData->getLength( ) )
        iMax++;
    r.mF0 = getBinF( iMin );
    r.mDSMode = mDSMode;
    r.mData.reset( mData->Extract( iMin, iMax - iMin ) );
    return r;
}

//======================================  Get the current data length.
FSeries::size_type
FSeries::getLength( void ) const
{
    if ( null( ) )
        return 0;
    return mData->size( );
}

//======================================  Get the number of data steps.
FSeries::size_type
FSeries::getNStep( void ) const
{
    if ( mDSMode != kFolded )
        return mData->getLength( );
    return mData->getLength( ) - 1;
}

//======================================  Get a constant pointer to the data
const void*
FSeries::refData( void ) const
{
    if ( null( ) )
        return 0;
    return mData->refData( );
}

//======================================  Get a pointer to the data.
void*
FSeries::refData( void )
{
    if ( null( ) )
        return 0;
    return mData->refData( );
}

//======================================  Get float data
FSeries::size_type
FSeries::getData( size_type len, float* data ) const
{
    if ( null( ) )
        return 0;
    return mData->getData( 0, len, data );
}

//======================================  Get complex data
FSeries::size_type
FSeries::getData( size_type len, Complex* data ) const
{
    if ( null( ) )
        return 0;
    return mData->getData( 0, len, data );
}

//======================================  Interpolate a frequency series
FSeries
FSeries::interpolate( double fmin, double fmax, double df, bool l ) const
{

    //----------------------------------  Set the return series.
    FSeries r( 0.0, df, mT0, mDt );
    if ( empty( ) )
        return r;

    //----------------------------------  See if extract will work...
    long inc = long( df / mDf + 0.5 );
    if ( double( inc ) * mDf == df && mF0 == 0 )
    {
        size_type iMax = getBin( fmax ) + inc;
        if ( inc == 1 )
            r.mData.reset( mData->Extract( 0, iMax ) );
        else
            r.mData.reset( mData->Extract( 0, iMax / inc, inc ) );
        size_type iMin = r.getBin( fmin );
        if ( iMin )
            r.mData->sub( 0, *r.mData, 0, iMin );
        r.mDSMode = kFolded;

        //----------------------------------  Do the full interpolation.
    }
    else
    {
        const fComplex c0( 0, 0 );

        //------------------------------  Set up the output series
        r.setData( 1, &c0 );
        r.mDSMode = kFolded;
        r.extend( fmax );
        long      nFill = long( fmax / df ) + 1;
        fComplex* pR = reinterpret_cast< fComplex* >( r.refData( ) );

        //------------------------------  Get the input series packing
        const fComplex* pFd = reinterpret_cast< const fComplex* >( refData( ) );
        long            nF = getNStep( );

        //------------------------------  Interpolate (slowly but surely).
        for ( long i = 0; i < nFill; ++i )
        {
            double f = df * double( i );
            if ( f < fmin )
            {
                pR[ i ] = c0;
            }
            else
            {
                long   inx = getBin( f );
                double f0 = getBinF( inx );
                if ( inx && f < f0 )
                {
                    f0 -= mDf;
                    --inx;
                }
                else if ( inx >= nF )
                {
                    inx = nF - 1;
                    f0 = getBinF( inx );
                }
                double a = ( f - f0 ) / mDf;
                if ( l )
                    pR[ i ] = exp( log( pFd[ inx ] ) * ( 1 - a ) +
                                   log( pFd[ inx + 1 ] ) * a );
                else
                    pR[ i ] = pFd[ inx ] * ( 1.0 - a ) + pFd[ inx + 1 ] * a;
            }
        }
    }
    return r;
}

//======================================  Sum power in a frequency range
float
FSeries::Power( float fmin, float fmax ) const
{
    double sum = 0.0;
    if ( empty( ) )
        return sum;
    size_type N = getNStep( );
    if ( fmax <= fmin )
        fmax = mF0 + N * mDf;
    size_type bMin = getBin( fmin );
    size_type bMax = getBin( fmax );
    switch ( mDSMode )
    {
    case kFolded:
        if ( bMax == N )
            bMax++;
        if ( bMax <= bMin )
            break;
        if ( mData->getType( ) == DVector::t_complex )
        {
            const fComplex* p =
                reinterpret_cast< const fComplex* >( refData( ) );
            for ( size_type i = bMin; i < bMax; ++i )
                sum += norm( p[ i ] );
        }
        else if ( mData->getType( ) == DVector::t_dcomplex )
        {
            const dComplex* p =
                reinterpret_cast< const dComplex* >( refData( ) );
            for ( size_type i = bMin; i < bMax; ++i )
                sum += norm( p[ i ] );
        }
        else if ( mData->getType( ) == DVector::t_float )
        {
            const float* p = reinterpret_cast< const float* >( refData( ) );
            for ( size_type i = bMin; i < bMax; ++i )
                sum += p[ i ] * p[ i ];
        }
        else
        {
            size_type          nSel = bMax - bMin;
            lcl_array< float > p( nSel );
            mData->getData( bMin, nSel, p );
            for ( size_type i = 0; i < nSel; ++i )
                sum += p[ i ] * p[ i ];
        }
        break;
    case kFull:
        if ( mData->getType( ) == DVector::t_complex )
        {
            const fComplex* p =
                reinterpret_cast< const fComplex* >( refData( ) );
            for ( size_type i = bMin; i < bMax; i++ )
                sum += norm( p[ i ] );
        }
        else if ( mData->getType( ) == DVector::t_dcomplex )
        {
            const dComplex* p =
                reinterpret_cast< const dComplex* >( refData( ) );
            for ( size_type i = bMin; i < bMax; i++ )
                sum += norm( p[ i ] );
        }
        else if ( mData->getType( ) == DVector::t_float )
        {
            const float* p = reinterpret_cast< const float* >( refData( ) );
            for ( size_type i = bMin; i < bMax; i++ )
                sum += p[ i ] * p[ i ];
        }
        else
        {
            lcl_array< float > p( N + 1 );
            mData->getData( 0, N + 1, p );
            for ( size_type i = bMin; i < bMax; i++ )
                sum += p[ i ] * p[ i ];
        }
        break;
    default:
        break;
    }

    return sum * mDf;
}

//======================================  Assignment operator
FSeries&
FSeries::operator=( const FSeries& fs )
{
    mName = fs.mName;
    mF0 = fs.mF0;
    mDf = fs.mDf;
    mT0 = fs.mT0;
    mDt = fs.mDt;
    mDSMode = fs.mDSMode;
    if ( fs.null( ) )
        mData.reset( );
    else
        mData.reset( fs.mData->clone( ) );
    return *this;
}

//======================================  operator +=(double)
FSeries&
FSeries::operator+=( double bias )
{
    if ( !empty( ) )
        *mData += bias;
    return *this;
}

//======================================  operator +=(FSeries)
FSeries&
FSeries::operator+=( const FSeries& rhs )
{
    if ( rhs.empty( ) )
        return *this;
    if ( empty( ) )
    {
        *this = rhs;
    }
    else if ( mF0 == rhs.mF0 && mDf == rhs.mDf )
    {
        *mData += *rhs.mData;
    }
    else
    {
        throw runtime_error( "Frequency mismatch" );
    }
    return *this;
}

//======================================  operator -=(FSeries)
FSeries&
FSeries::operator-=( const FSeries& rhs )
{
    if ( rhs.empty( ) )
        return *this;
    if ( empty( ) )
    {
        *this = rhs;
        *this *= -1.0;
    }
    else if ( mF0 == rhs.mF0 && mDf == rhs.mDf )
    {
        *mData -= *rhs.mData;
    }
    else
    {
        throw runtime_error( "Frequency mismatch" );
    }
    return *this;
}

//======================================  operator *=(double)
FSeries&
FSeries::operator*=( double rhs )
{
    if ( !empty( ) )
        *mData *= rhs;
    return *this;
}

//======================================  operator *=(FSeries)
FSeries&
FSeries::operator*=( const FSeries& rhs )
{
    if ( empty( ) || rhs.empty( ) )
        return *this;
    if ( mF0 == rhs.mF0 && mDf == rhs.mDf )
    {
        *mData *= *rhs.mData;
    }
    else
    {
        throw runtime_error( "Frequency mismatch" );
    }
    return *this;
}

//======================================  operator /=(FSeries)
FSeries&
FSeries::operator/=( const FSeries& rhs )
{
    if ( empty( ) || rhs.empty( ) )
        return *this;
    if ( mF0 == rhs.mF0 && mDf == rhs.mDf )
    {
        *mData /= *rhs.mData;
    }
    else
    {
        throw runtime_error( "Frequency mismatch" );
    }
    return *this;
}

//======================================  cdot(FSeries)
FSeries::Complex
FSeries::cdot( const FSeries& rhs ) const
{
    Complex   r( 0 );
    size_type nw = mData->getLength( );
    if ( !nw || !rhs.mData->getLength( ) )
    {
        return r;
    }
    else if ( mData->C_data( ) || rhs.mData->C_data( ) )
    {
        Complex* V1 = new Complex[ 2 * nw ];
        Complex* p1 = V1;
        getData( nw, p1 );
        Complex*  p2 = V1 + nw;
        size_type nd = rhs.getData( nw, p2 );
        for ( size_type i = 0; i < nd; i++ )
            r += xcc( *p1++, *p2++ );
        delete[] V1;
    }
    else
    {
        float* V1 = new float[ 2 * nw ];
        float* p1 = V1;
        getData( nw, p1 );
        float*    p2 = V1 + nw;
        size_type nd = rhs.getData( nw, p2 );
        float     s = 0;
        for ( size_type i = 0; i < nd; i++ )
            s += ( *p1++ ) * ( *p2++ );
        r = Complex( s );
        delete[] V1;
    }
    return r;
}

FSeries::Complex
FSeries::operator( )( double f ) const
{
    if ( empty( ) )
        return Complex( 0.0 );
    if ( f > mF0 || isDoubleSided( ) )
        return mData->getCplx( getBin( f ) );
    return ~( mData->getCplx( getBin( 2.0 * mF0 - f ) ) );
}

//======================================  Mutators
void
FSeries::clear( void )
{
    if ( !null( ) )
        mData->Clear( );
    mDSMode = kEmpty;
}

//======================================  Accessors
void
FSeries::extend( double fmax )
{
    if ( null( ) || !mDf )
        return;
    size_type lBin = size_type( ( fmax - mF0 ) / mDf ) + 1;
    mData->Extend( lBin );
}

void
FSeries::ReSize( size_type len )
{
    if ( null( ) )
        mData.reset( new DVectC( len ) );
    else
        mData->ReSize( len );
}

void
FSeries::appName( const char* name )
{
    mName += name;
}

void
FSeries::setName( const char* name )
{
    mName = name;
}

void
FSeries::setTimeSpan( const Time& t0, Interval dT )
{
    mT0 = t0;
    mDt = dT;
}

void
FSeries::setT0( const Time& t0 )
{
    mT0 = t0;
}

//======================================  Set the data with a Complex array
void
FSeries::setData( size_type len, const Complex data[] )
{
    if ( !len )
        clear( );
    else
        setData( new DVecType< Complex >( len, data ) );
}

//======================================  Set the data with a float array
void
FSeries::setData( size_type len, const float data[] )
{
    if ( !len )
        clear( );
    else
        setData( new DVectF( len, data ) );
}

//======================================  Set the data with a DVector
void
FSeries::setData( DVector* dv )
{
    clear( );
    if ( !dv || !dv->size( ) )
        return;
    if ( mData.get( ) != dv )
        mData.reset( dv );
    if ( mF0 < 0.0 )
        mDSMode = kFull;
    else
        mDSMode = kFolded;
}

//======================================  Set the data to the FFT of a TSeries
void
FSeries::setData( const TSeries& TS )
{
    clear( );
    size_type NData = TS.getNSample( );
    if ( !NData )
        return;

    mF0 = TS.getF0( );
    mT0 = TS.getStartTime( );
    mDt = TS.getInterval( );
    mDf = 1. / mDt;

    const DVector* tsVect = TS.refDVect( );
    size_type      nOut( NData );
    float          norm = sqrt( 2.0 * mDt ) / double( NData );
    if ( !tsVect->C_data( ) )
        nOut = NData / 2 + 1;

    //----------------------------------  Set the vector size.
    if ( !null( ) && !mData->C_data( ) )
        mData.reset( );
    ReSize( nOut );

    //----------------------------------  Fill from a Complex TSeries.
    Complex* CVec = reinterpret_cast< Complex* >( refData( ) );
    if ( tsVect->C_data( ) )
    {
        mDSMode = kFull;
        TS.getData( NData, CVec );
        wfft( CVec, NData, 1 );

        //------------------------------  Reorder the output vector.
        size_type N2 = NData / 2;
        Complex*  pP = CVec + N2;
        Complex*  pN = CVec;
        for ( size_type i = 0; i < N2; i++ )
        {
            Complex t = *pN;
            *pN++ = *pP;
            *pP++ = t;
        }
        mF0 -= double( N2 ) * mDf;
    }
    else if ( tsVect->F_data( ) )
    {
        mDSMode = kFolded;
        wfft( (const float*)TS.refData( ), CVec, NData );
        CVec[ 0 ] *= sqrt( 0.5 );
        CVec[ nOut - 1 ] *= sqrt( 0.5 );
    }
    else if ( infoFFT( kFFTDouble ) && tsVect->D_data( ) )
    {
        mDSMode = kFolded;
        dComplex* c = new dComplex[ NData ];
        wfft( (const double*)TS.refData( ), c, NData );
        c[ 0 ] *= sqrt( 0.5 );
        c[ nOut - 1 ] *= sqrt( 0.5 );
        for ( size_type i = 0; i < nOut; i++ )
            CVec[ i ] = c[ i ];
        delete[] c;
    }
    else
    {
        mDSMode = kFolded;
        TS.getData( NData, (float*)refData( ) );
        wfft( (const float*)refData( ), CVec, NData );
        CVec[ 0 ] *= sqrt( 0.5 );
        CVec[ nOut - 1 ] *= sqrt( 0.5 );
    }

    //-------------------------------  Normalize the data
    for ( size_type i = 0; i < nOut; i++ )
        CVec[ i ] *= norm;
}

//======================================  Do a time derivative.
void
FSeries::tDerivative( void )
{
    if ( null( ) || !mData->C_data( ) )
        return;
    Complex*  CVec = reinterpret_cast< Complex* >( refData( ) );
    size_type N = mData->getLength( );
    for ( size_type i = 0; i < N; i++ )
    {
        CVec[ i ] *= Complex( 0, twopi * getBinF( i ) );
    }
}

//======================================  Do a time integral.
void
FSeries::tIntegral( void )
{
    if ( null( ) || !mData->C_data( ) )
        return;
    Complex* CVec = reinterpret_cast< Complex* >( refData( ) );
    CVec[ 0 ] = Complex( 0 );
    size_type N = mData->getLength( );
    for ( size_type i = 0; i < N; i++ )
    {
        Complex if2pi( 0, twopi * getBinF( i ) );
        if ( if2pi.imag( ) )
            CVec[ i ] /= if2pi;
    }
}

//======================================  Dump the contents
std::ostream&
FSeries::Dump( std::ostream& out ) const
{
    out << "FSeries " << mName << ": Start time " << getStartTime( )
        << " End Time " << getEndTime( ) << std::endl;
    out << "Data storage type: ";
    switch ( mDSMode )
    {
    case kEmpty:
        out << "None" << std::endl;
        break;
    case kFolded:
        out << "Folded" << std::endl;
        break;
    case kFull:
        out << "Full" << std::endl;
        break;
    }
    out << "Low Frequency " << getLowFreq( ) << " High Frequency "
        << getHighFreq( ) << " No. frequency steps " << getNStep( )
        << std::endl;
    if ( null( ) )
        return out;
    else
        return mData->Dump( out );
}

//======================================  Evolve the data in time
void
FSeries::evolve( const Interval& dT )
{

    //----------------------------------  Avoid trivial transformations.
    size_type NData = mData->getLength( );
    if ( !dT || !NData )
        return;

    //----------------------------------  Convert to complex if necessary
    if ( !mData->C_data( ) )
        mData.reset( new DVectC( *mData ) );

    //----------------------------------  Get starting, incremental rotations
    Complex  dPhi( 0.0, getFStep( ) * twopi * dT );
    Complex  expDPhi = exp( dPhi );
    Complex  phi( 1.0 );
    Complex* myData = (Complex*)refData( );

    //----------------------------------  Loop over everything
    for ( size_type i = 1; i < NData; i++ )
    {
        phi *= expDPhi;
        myData[ i ] *= phi;
    }
}
