#include "FSpectrum.hh"
#include "FSeries.hh"
#include "DVecType.hh"
#include <iostream>
#include <stdexcept>

using namespace std;

//======================================  Default constructor
FSpectrum::FSpectrum( )
    : mName( "" ), mF0( 0.0 ), mDf( 0.0 ), mT0( 0 ), mDt( 0.0 ), mData( 0 ),
      mCount( 0 )
{
}

//======================================  Copy constructor
FSpectrum::FSpectrum( const FSpectrum& fs ) : mData( 0 )
{
    *this = fs;
}

//======================================  Data Constructor
FSpectrum::FSpectrum( const FSeries& fs ) : mData( 0 )
{
    mName = fs.getName( );
    setData( fs );
}

//======================================  Data Constructor
FSpectrum::FSpectrum( double          F0,
                      double          dF,
                      const Time&     t0,
                      const Interval& dT,
                      size_type       nWord,
                      data_type*      data )
    : mF0( F0 ), mDf( dF ), mT0( t0 ), mDt( dT ), mData( 0 ), mCount( 1 )
{
    setData( nWord, data );
}

//======================================  Destructor
FSpectrum::~FSpectrum( void )
{
    if ( mData )
        delete mData;
    mData = 0;
}

//======================================  Extract data
FSpectrum
FSpectrum::extract( double f0, double dF ) const
{
    FSpectrum r;
    if ( !mData )
        return r;
    size_type inx = getBin( f0 );
    size_type len = getBin( f0 + dF ) - inx;
    r.mF0 = mF0 + inx * mDf;
    r.mDf = mDf;
    r.mT0 = mT0;
    r.mDt = mDt;
    r.mCount = mCount;
    if ( !len )
        return r;
    r.mData = mData->Extract( inx, len );
    return r;
}

//======================================  Get data length
FSpectrum::size_type
FSpectrum::getNStep( void ) const
{
    if ( !mData || !mData->getLength( ) )
        return 0;
    return mData->getLength( ) - 1;
}

//======================================  Get spectrum data.
FSpectrum::size_type
FSpectrum::getData( size_type len, data_type* data ) const
{
    if ( !mData )
        return 0;
    return mData->getData( 0, len, data );
}

//======================================  Get spectrum data.
double
FSpectrum::getSum( float f0, float dF ) const
{
    if ( !mData )
        return 0.0;
    size_type inx = getBin( f0 );
    size_type jnx = getBin( f0 + dF );
    if ( jnx > inx )
        return mData->VSum( inx, jnx - inx );
    return 0.0;
}

//======================================  Get pointer to spectrum data.
const FSpectrum::data_type*
FSpectrum::refData( void ) const
{
    if ( !mData )
        return 0;
    return reinterpret_cast< const DVecType< data_type >* >( mData )
        ->refTData( );
}

FSpectrum::data_type*
FSpectrum::refData( void )
{
    if ( !mData )
        return 0;
    return reinterpret_cast< DVecType< data_type >* >( mData )->refTData( );
}

//======================================  Dump out data object
ostream&
FSpectrum::Dump( ostream& out ) const
{
    out << "FSpectrum: " << mName << " length = " << getNStep( ) << std::endl;
    out << "Frequency range= " << getLowFreq( ) << "-" << getHighFreq( )
        << " Number of averages = " << getCount( ) << endl;
    out << "Time interval = " << getStartTime( ) << "-" << getEndTime( )
        << endl;
    if ( mData )
        mData->Dump( out );
    return out;
}

//======================================  Append text to name field.
void
FSpectrum::appName( const char* name )
{
    mName += name;
}

//======================================  Clear data.
void
FSpectrum::clear( double f0, double dF, Time t0, Interval dT )
{
    if ( mData )
        mData->Clear( );
    mF0 = f0;
    mDf = dF;
    mT0 = t0;
    mDt = dT;
    mCount = 0;
}

void
FSpectrum::setT0( const Time& t0 )
{
    mT0 = t0;
}

void
FSpectrum::setCount( size_type Count )
{
    mCount = Count;
}

void
FSpectrum::setName( const char* name )
{
    mName = name;
}

void
FSpectrum::setData( size_type len, const data_type* data )
{
    if ( mData )
    {
        mData->Clear( );
        mData->Append( len, data );
    }
    else
    {
        mData = new DVecType< data_type >( len, data );
    }
}

//======================================  Set data from an FSeries.
void
FSpectrum::setData( const FSeries& fs )
{
    mF0 = fs.getLowFreq( );
    mDf = fs.getFStep( );
    mT0 = fs.getStartTime( );
    mDt = fs.getEndTime( ) - mT0;
    mCount = 1;
    size_type nw = fs.getNStep( );
    double    fHi = fs.getHighFreq( );
    if ( mF0 < 0.0 && fHi > 0 )
    {
        mF0 = 0.0;
        nw = long( ( fHi - mF0 ) / mDf );
    }

    if ( !nw )
    {
        if ( mData )
            mData->Clear( );
        return;
    }
    if ( !mData )
        mData = new DVecType< data_type >( nw + 1 );
    mData->ReSize( nw + 1 );

    data_type* pDV = refData( );
    if ( fs.refDVect( )->getType( ) ==
         DVecType< FSeries::Complex >::getDataType( ) )
    {
        const FSeries::Complex* pC =
            reinterpret_cast< const FSeries::Complex* >( fs.refData( ) );
        size_type bin0 = fs.getBin( mF0 );
        for ( size_type i = 0; i < nw; ++i )
            pDV[ i ] = norm( pC[ bin0 + i ] );
        if ( fs.isSingleSided( ) )
            pDV[ nw ] = norm( pC[ bin0 + nw ] );
        else
            pDV[ nw ] = norm( pC[ 0 ] );
    }
    else
    {
        size_type N = fs.refDVect( )->getData( fs.getBin( mF0 ), nw + 1, pDV );
        for ( size_type i = 0; i < N; ++i )
            pDV[ i ] *= pDV[ i ];
    }

    if ( fHi < 0.0 )
    {
        mF0 = -fHi;
        reinterpret_cast< DVecType< data_type >* >( mData )->reverse(
            0, pDV, nw + 1 );
    }
}

//======================================  Copy operator.
FSpectrum&
FSpectrum::copy( const FSpectrum& rhs )
{
    mF0 = rhs.mF0;
    mDf = rhs.mDf;
    mT0 = rhs.mT0;
    mDt = rhs.mDt;
    mCount = rhs.mCount;

    if ( mData && rhs.mData )
        mData->replace( 0, mData->getLength( ), *rhs.mData );
    else if ( mData )
        mData->Clear( );
    else if ( rhs.mData )
        mData = rhs.mData->clone( );
    mName = rhs.mName;
    return *this;
}

//======================================  Overloaded Operators
FSpectrum&
FSpectrum::operator=( const FSpectrum& rhs )
{
    mF0 = rhs.getLowFreq( );
    mDf = rhs.getFStep( );
    mT0 = rhs.getStartTime( );
    mDt = rhs.getEndTime( ) - mT0;
    mCount = rhs.getCount( );
    if ( mData && rhs.mData )
        *mData = *rhs.mData;
    else if ( rhs.mData )
        mData = rhs.mData->clone( );
    else if ( mData )
        mData->Clear( );
    mName = rhs.mName;
    return *this;
}

FSpectrum&
FSpectrum::operator+=( const FSpectrum& rhs )
{
    if ( rhs.isEmpty( ) )
        return *this;
    if ( isEmpty( ) )
    {
        *this = rhs;
    }
    else if ( mF0 == rhs.mF0 && mDf == rhs.mDf )
    {
        if ( !mT0 || rhs.mT0 < mT0 )
            mT0 = rhs.mT0;
        if ( rhs.mT0 + rhs.mDt > mT0 + mDt )
            mDt = rhs.mT0 + rhs.mDt - mT0;
        *mData += *rhs.mData;
        mCount += rhs.mCount;
    }
    else
    {
        cout << "fs{" << mF0 << "," << mDf << "} += fs{" << rhs.mF0 << ","
             << rhs.mDf << "}" << endl;
        throw std::runtime_error( "Frequency range mismatch" );
    }
    return *this;
}

FSpectrum&
FSpectrum::operator-=( const FSpectrum& rhs )
{
    if ( rhs.isEmpty( ) )
        return *this;
    if ( isEmpty( ) )
    {
        *this = rhs;
        data_type* pF = reinterpret_cast< data_type* >( mData->refData( ) );
        for ( size_type i = 0; i < getNStep( ); ++i )
            pF[ i ] = -pF[ i ];
    }
    else if ( mF0 == rhs.mF0 && mDf == rhs.mDf )
    {
        if ( !mT0 || rhs.mT0 < mT0 )
            mT0 = rhs.mT0;
        if ( rhs.mT0 + rhs.mDt > mT0 + mDt )
            mDt = rhs.mT0 + rhs.mDt - mT0;
        *mData -= *rhs.mData;
        mCount -= rhs.getCount( );
    }
    else
    {
        cout << "fs{" << mF0 << "," << mDf << "} -= fs{" << rhs.mF0 << ","
             << rhs.mDf << "}" << endl;
        throw std::runtime_error( "Frequency range mismatch" );
    }
    return *this;
}

FSpectrum&
FSpectrum::operator*=( double scale )
{
    if ( mData )
        *mData *= scale;
    return *this;
}

double
FSpectrum::operator( )( double freq ) const
{
    if ( !mData )
        return 0.0;
    return refData( )[ getBin( freq ) ];
}
