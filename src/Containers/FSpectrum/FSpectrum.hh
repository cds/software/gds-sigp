#ifndef FSPECTRUM_HH
#define FSPECTRUM_HH

#include <iosfwd>
#include <string>
#include "Time.hh"
#include "Interval.hh"

class FSeries;
class DVector;

/**  The frequency spectrum class is used to represent a power spectrum.
  *  Although the spectrum data iscontained in a DVector, only float
  *  type data are allowed.
  *  @brief Frequency spectrum class.
  *  @author J. Zweizig
  *  @version 1.0; Modified October 25, 1999
  */
class FSpectrum
{
public:
    /// Data length and index type
    typedef unsigned long size_type;
    /// Default data type
    typedef float data_type;

    /**  Construct an empty FSpectrum.
    *  @brief Default constructor.
    */
    FSpectrum( void );

    /**  Construct an FSpectrum and initialize it from another spectrum.
    *  @brief Copy constructor.
    *  @param fs Constant spectrum from which the new spectrum is initialized.
    */
    FSpectrum( const FSpectrum& fs );

    /**  Construct a FSpectrum and initilize it from the modulus-squared of
    *  an FSeries.
    *  @brief Data constructor.
    *  @param fs Frequency series from which the spectrum is initialized
    */
    FSpectrum( const FSeries& fs );

    /**  Construct a FSpectrum and initialize it from data.
    *  @brief Data constructor.
    *  @param F0 Minimum frequency.
    *  @param dF Frequency step.
    *  @param t0 Start time.
    *  @param dT Time interval.
    *  @param nWord Number of data words.
    *  @param data Spectral densities.
    */
    FSpectrum( double          F0,
               double          dF,
               const Time&     t0,
               const Interval& dT,
               size_type       nWord,
               data_type*      data );

    /**  Destroy a spectrum object.
    *  @brief FSpectrum Destructor.
    */
    ~FSpectrum( void );

    /**  Copy the specified spectrum argument here. A full data copy is 
    *  performed.
    *  @brief Copy an FSpectrum.
    *  @param fs FSpectrum to be copied.
    */
    FSpectrum& copy( const FSpectrum& fs );

    //------------------------------  Accessors
    /**  Returns an FSpectrum containing a subset of the parent spectrum.
    *  The subset is specified as a starting frequency and frequency 
    *  interval. No rebinning is performed, so the actual frequency 
    *  range wil be rounded to the nearest bin edges.
    *  @brief Get a substring of the FSpectrum.
    *  @param f0 lowest frequency to be extracted (in Hz).
    *  @param dF Frequency interval (in Hz).
    *  @return the extracted spectrum.
    */
    FSpectrum extract( double f0, double dF ) const;

    /**  Returns the start time as specified in the source data.
    *  @brief Get the start time.
    *  @return The start time of data from which the spectrum was extracted.
    */
    Time getStartTime( void ) const;

    /**  Returns the End time as specified in the sourrce data.
    *  @brief Get the End time.
    *  @return The end time of data from which the spectrum was extracted.
    */
    Time getEndTime( void ) const;

    /**  Returns the minimum frequency covered by the data.
    *  @brief Get the minimum frequency.
    *  @return The minimum frequency covered by the spectrum.
    */
    double getLowFreq( void ) const;

    /**  Returns the maximum (Nyquist) frequency of the data.
    *  @brief Get the maximum Frequency.
    *  @return The maximum frequency covered by the spectrum.
    */
    double getHighFreq( void ) const;

    /**  Returns the frequency interval between two adjacen spectrum points.
    *  @brief Get the frequency step.
    *  @return THe frequency step size in Hz.
    */
    double getFStep( void ) const;

    /**  Returns a pointer to the spectrum name.
    *  @brief Get the spectrum name.
    *  @return pointer to the Spectrum name.
    */
    const char* getName( void ) const;

    /**  Returns the number of frequency steps. Note that the number of
    *  data points is in fact one greater than the number of steps
    *  because both the f=0 and f=Nyquist entries are included.
    *  @brief Get the number of frequency steps.
    */
    size_type getNStep( void ) const;

    /**  Optionally convert and copy the first 'len' entries of a spectrum to 
    *  a float array 'data'. getData() returns the number of entries copied.
    *  @brief Get the spectrum data.
    */
    size_type getData( size_type len, data_type* data ) const;

    /**  Calculate the sum of the bins in the specified frequency interval.
    *  @brief Get spectral sum.
    *  @param f0 lowest frequency bin to be summed.
    *  @param dF frequency interval width.
    *  @return the sum of all spectrum points in the frequency interval.
    */
    double getSum( float f0, float dF ) const;

    /**  Returns the number of spectra that have been summed.
    *  @brief Get the number of spectra averaged.
    *  @return The number of averaged spectra.
    */
    size_type getCount( void ) const;

    /**  Returns a pointer to the data. refData returns NULL if no 
    *  DVector has been defined.
    *  @brief Get a pointer to the spectrum data.
    */
    const data_type* refData( void ) const;

    /**  Get the data vector pointer
    */
    const DVector*
    refDVect( void ) const
    {
        return mData;
    }

    /**  A formatted dump of the FSpectrum header and data are written to the
    *  output stream.
    *  @brief Dump the contents of the FSpectrum to an output stream.
    */
    std::ostream& Dump( std::ostream& out ) const;

    //------------------------------  Mutators
    /**  the specified string "name" is appended to the existing spectrum name.
    *  @brief Append a string to the spectrum name.
    */
    void appName( const char* name );

    /**  Delete the data and optionally reset the frequency and time
    *  parameters. The data array remains allocated. The count field 
    *  is reset to zero.
    *  @brief Clear the spectrum.
    *  @param F0 New lower frequency
    *  @param dF New frequency step
    *  @param t0 New start time
    *  @param dT New time interval
    */
    void clear( double   F0 = 0,
                double   dF = 0,
                Time     t0 = Time( 0 ),
                Interval dT = 0.0 );

    /**  The spectrum start time is set to 't0'.
    *  @brief Set the start time.
    */
    void setT0( const Time& t0 );

    /**  The spectrum name is set to the 'name' string.
    *  @brief Set the spectrum name.
    */
    void setName( const char* name );

    /**  The spectrum average count is set to Count.
    *  @brief Set the spectrum count.
    */
    void setCount( size_type Count );

    /**  The float data in 'data' are optionally converted to the data vector
    *  type and then used to overwrite the spectrum data.
    *  @brief Overwrite the spectrum with float data.
    */
    void setData( size_type len, const data_type* data );

    /**  The square of the frequency spectrum in 'data' is stored in the 
   *   FSpectrum. 
    *  @brief Replace spectrum with an FSeries squared.
    */
    void setData( const FSeries& data );

    //------------------------------  Overloaded Operators
    /**  The rhs spectrum is copied the lhs spectrum.
    *  @brief Copy a spectrum.
    */
    FSpectrum& operator=( const FSpectrum& rhs );

    /**  The rhs spectrum is added to the lhs spectrum on a element by element 
    *  basis.
    *  @brief Add two spectra.
    */
    FSpectrum& operator+=( const FSpectrum& rhs );

    /**  The rhs spectrum is subtracted from the lhs spectrum on a element by 
    *  element basis.
    *  @brief Subtract a spectrum.
    */
    FSpectrum& operator-=( const FSpectrum& rhs );

    /**  The lhs spectrum is scaled by the rhs float.
    *  @brief Scale a spectrum.
    */
    FSpectrum& operator*=( double scale );

    /**  Get the amplitude at a specified frequency.
    *  The complex amplitude at the closest bin is returned.
    */
    double operator( )( double freq ) const;

    /**  Test whether the spectrum is empty.
    *  @brief Test for empty.
    *  @return true if empty.
    */
    bool isEmpty( void ) const;

    /**  Calculate the closest bin to a given frequency.
   */
    size_type getBin( double f ) const;

    /**  Calculate the frequency of a given bin.
   */
    double getBinF( size_type bin ) const;

    /**  Get the data vector pointer
    */
    DVector*
    refDVect( )
    {
        return mData;
    }

    /**  Get the data pointer
    */
    data_type* refData( );

private:
    /**  Data Name
    *  Frequency spectrum name string.
    */
    std::string mName;

    /**  Minimum frequency.
    *  Not zero if the spectrum source was heterodyned.
    */
    double mF0;

    /**  Frequency bin size
    */
    double mDf;

    /**  GPS time of the first data sample use to produce the spectrum.
    *  @brief Starting absolute time.
    */
    Time mT0;

    /**  Time interval over which data was taken.
    */
    Interval mDt;

    /**  Data array
    */
    DVector* mData;

    /**  Sum count
   */
    size_type mCount;
};

#ifndef __CINT__
//--------------------------------------  Find bin closest to a frequency.
inline FSpectrum::size_type
FSpectrum::getBin( double f ) const
{
    if ( f <= mF0 )
        return 0;
    size_type r = size_type( ( f - mF0 ) / mDf + 0.5 );
    size_type Nmax = getNStep( );
    if ( r > Nmax )
        r = Nmax;
    return r;
}

//---------------------------------------  Calculate the frequency of a bin.
inline double
FSpectrum::getBinF( size_type bin ) const
{
    return mF0 + bin * mDf;
}

//---------------------------------------  Test if a spectrum is empty
inline bool
FSpectrum::isEmpty( void ) const
{
    return ( getNStep( ) == 0 );
}

//---------------------------------------  Get data start time
inline Time
FSpectrum::getStartTime( void ) const
{
    return mT0;
}

//---------------------------------------  Get data end time
inline Time
FSpectrum::getEndTime( void ) const
{
    return mT0 + mDt;
}

//---------------------------------------  Get the minimum frequency
inline double
FSpectrum::getLowFreq( void ) const
{
    return mF0;
}

//---------------------------------------  Get the maximum frequency
inline double
FSpectrum::getHighFreq( void ) const
{
    return mF0 + getNStep( ) * mDf;
}

//---------------------------------------  Get the frequency step
inline double
FSpectrum::getFStep( void ) const
{
    return mDf;
}

//---------------------------------------  Get name string pointer
inline const char*
FSpectrum::getName( void ) const
{
    return mName.c_str( );
}

//---------------------------------------  Get the number of averages
inline FSpectrum::size_type
FSpectrum::getCount( void ) const
{
    return mCount;
}

//---------------------------------------  Scale a spectrum
inline FSpectrum&
operator/=( FSpectrum& f, double scale )
{
    return ( f *= 1.0 / scale );
}
#endif

#endif //  FSPECTRUM_HH
