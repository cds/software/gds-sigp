#ifndef _CALTYPE_H
#define _CALTYPE_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: caltype							*/
/*                                                         		*/
/* Module Description: Basic calibration type 				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 14Mar01  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: doc++						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef __CINT__
#ifdef __cplusplus
extern "C" {
#endif
#endif

/** @name Basic Calibration Types
    This API provides the types for the calibration records.

    @memo C types
    @author Written January 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

/*@{*/

/** @name Calibration flags
    List of flags used by the calibration API.
    @memo Calibration flags
 ************************************************************************/

/*@{*/

/** Amplitude conversion flag */
#define CALAMPLITUDE 0x0001
/** Offset correction flag */
#define CALOFFSET 0x0002
/** Time delay correction flag */
#define CALTIMEDELAY 0x0004
/** Transfer function conversion flag */
#define CALTRANSFERFUNCTION 0x0008
/** Transfer function stored in pole/zero notation */
#define CALPOLEZERO 0x0010
/** Square flag */
#define CALSQUARE 0x10000
/** Root Hertz flag */
#define CALROOTHERTZ 0x20000

/** Maximum size of channel name (60) */
#define CALCHANNELSIZE 60
/** Maximum size of reference point (40) */
#define CALREFERENCESIZE 40
/** Maximum size of unit string (40) */
#define CALUNITSIZE 40

/** Flag representing a normal calibration record */
#define CALNORMAL 0
/** Flag representing a record to be added */
#define CALADD 1
/** Flag representing a record to be deleted */
#define CALDELETE 2
/** Flag representing a query record */
#define CALQUERY 3
/** Flag representing an error record */
#define CALERROR 4

/** Flag representing a unit transformation into power spectral density */
#define CALTRANSPSD
/** Flag representing a unit transformation into a channel ratio */
#define CALTRANSRATIO
/** Flag representing a unit transformation into a channel product */
#define CALTRANSPRODUCT

/*@}*/

/** @name Calibration record
    The definition of the calibration record.
    @memo Calibration record
 ************************************************************************/

/*@{*/

/** @name Pole-zero structure
    This record stores a transfer function in the pole-zero notation.
    It is used internally by the calibration record.
    @memo Pole-zero record
 ************************************************************************/
struct polezero_t
{
    /** Gain */
    double fGain;
    /** Number of poles */
    int fPoleNum;
    /** Number of zeros */
    int fZeroNum;
    /** List of poles and zeros: complex data array storing 
          pole/zero pairs. The length of the array in doubles is
          four times the maximum of numbers of poles and zeros  */
    float* fPoleZeros;
};
#ifndef __CINT__
typedef struct polezero_t polezero_t;
#endif

/** This structure represents a calibration record. Users should not
    access the fields of this structure directly but rather use the
    methods provided below. When dynamically allocating new calibration
    records the calnew and calfree functions should be used. Before
    a statically allocated calibration record is used the calinit function
    must be called. Similarly, after being finshed the calrelease
    function must be called to free associated memory.

    @memo Calibration record
 ************************************************************************/
struct calrec_t
{
    /** Channel name */
    char fChannel[ CALCHANNELSIZE ];
    /** Time when calibration takes effect */
    unsigned long fTime;
    /** Duration while calibration is valid */
    unsigned long fDuration;
    /** Reference point of calibration */
    char fReference[ CALREFERENCESIZE ];
    /** Unit string */
    char fUnit[ CALUNITSIZE ];
    /** Calibration type */
    int fType;
    /** Conversion coefficient (Unit / ADC unit) */
    double fConversion;
    /** Offset correction (ADC units) */
    double fOffset;
    /** Time delay of signal (sec) */
    double fTimeDelay;
    /** Transfer function with elements in format (f, ratio, phase) */
    float* fTransferFunction;
    /** Number of points in the transfer function */
    int fTransferFunctionLen;
    /** Transfer function in pole zero notation */
    polezero_t fPoleZero;
    /** 1 if default calibration, 0 otherwise */
    int fDefault;
    /** preferrd magnitude (power of 10) */
    int fPreferredMag;
    /** preferrd derivative */
    int fPreferredD;
    /** User comment */
    char* fComment;
};
#ifndef __CINT__
typedef struct calrec_t calrec_t;
#endif

/*@}*/

/** @name Unit types
    A set of types used for related units, deduced units and 
    add a magnitude qualifier.
    @memo Unit functions
 ************************************************************************/

/*@{*/

struct caldeduced_t;
#ifndef __CINT__
typedef struct caldeduced_t caldeduced_t;
#endif

/** This enumerated type specifies a channel relation.
    @memo Channel relation enumerated type
 ************************************************************************/
enum ECalChnRelation
{
    /** Single channel quantity */
    kCalChnRelSingle = 0,
    /** Two channel quantity, additive */
    kCalChnRelAdd = 1,
    /** Two channel quantity, multiplicative. */
    kCalChnRelMul = 2,
    /** Two channel quantity, B against A */
    kCalChnRelXY = 3
};
#ifndef __CINT__
typedef enum ECalChnRelation ECalChnRelation;
#endif

/** This enumerated type specifies the domain.
    @memo Domain enumerated type
 ************************************************************************/
enum ECalDomain
{
    /** Represents values */
    kCalDomainValue = 0,
    /** Time domain */
    kCalDomainTime = 1,
    /** Frequency domain */
    kCalDomainFrequency = 2
};
#ifndef __CINT__
typedef enum ECalDomain ECalDomain;
#endif

/** This enumerated type specifies the density unit.
    @memo Density unit enumerated type
 ************************************************************************/
enum ECalDensity
{
    /** No */
    kCalDensityNo = 0,
    /** Amplitude spectral density */
    kCalDensityAmpPerRtHz = 1,
    /** Power spectral density */
    kCalDensityPwrPerHz = 2,
    /** Amplitude */
    kCalDensityAmp = 3,
    /** Power */
    kCalDensityPwr = 4
};
#ifndef __CINT__
typedef enum ECalDensity ECalDensity;
#endif

/** This enumerated type specifies the axis type.
    @memo Axis type enumerated type
 ************************************************************************/
enum ECalUnit
{
    /** X unit */
    kCalUnitX = 0,
    /** Y unit */
    kCalUnitY = 1
};
#ifndef __CINT__
typedef enum ECalUnit ECalUnit;
#endif

/** This structure is used to describe a signal, so that the
    corresponding calibration correction can be applied.
    @memo Signal info structure
 ************************************************************************/
struct calsignalinfo_t
{
    /** channel relation */
    ECalChnRelation fChnRelation;
    /** coefficient in front of A and B channel */
    double fCoeff[ 2 ];
    /** exponent of A and B channel */
    int fExpo[ 2 ];
    /** if true complex conjugate A or B channel */
    int fConj[ 2 ];
    /** domain specification */
    ECalDomain fDomain;
    /** Density units */
    ECalDensity fDensityUnits;
    /** Bandwidth */
    double fBW;
};
#ifndef __CINT__
typedef struct calsignalinfo_t calsignalinfo_t;
#endif

/** This function prototype is used to caluclate the transformation
    between normal and deduced units. This is typically just a linear
    transformation with a offset and slope correction.

    @memo Deduced unit transformation prototype
    @param ded deduced unit structure
    @param flag 0 - x is array; 1 - x is (x0, dx) pair
    @param x x values
    @param y y values
    @param N number of points
    @param mag Magnitude qualifier: 0 - 1, 3 - 10^3, etc.
    @param cmplx True if complex
    @return true if successful
 ************************************************************************/
typedef int ( *calded_trans )( const caldeduced_t* ded,
                               int                 flag,
                               float*              x,
                               float*              y,
                               int                 N,
                               int                 mag,
                               int                 cmplx );

/** This structure is used to describe a deduced unit. This 
    structure can be copied with memcpy. It contains referenced to 
    a signal information structure and calibration records which
    have to be owned by someone else.
    @memo Deduced unit structure
 ************************************************************************/
struct caldeduced_t
{
    /** String of deduced unit */
    char fUnit[ CALUNITSIZE ];
    /** If set this is a power unit rather than an amplitude unit;
          this is important to know when calculating dB */
    int fPowerUnit;
    /** Reference to calibration signal info */
    const calsignalinfo_t* fInfo;
    /** Reference to original calibration record (A channel) */
    const calrec_t* fChn1;
    /** Reference to original calibration record (B channel) */
    const calrec_t* fChn2;
    /** Deduced unit transformation (can be zero) */
    calded_trans fTrans;
    /** Private data: do not change; used for factor and offset 
          of related units; channel 1 and 2, respectively */
    double fParam[ 4 ];
    /** Private data: do not change; used for derivative order */
    int fiParam[ 4 ];
};
/*@}*/

#ifndef __CINT__
#ifdef __cplusplus
}
#endif
#endif

/*@}*/

#endif /* _CALTYPE_H */
