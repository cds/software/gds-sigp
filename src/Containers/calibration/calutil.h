#ifndef _CALUTIL_H
#define _CALUTIL_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: calutil							*/
/*                                                         		*/
/* Module Description: Utility functions for calibration		*/
/*                                                         		*/
/* Module Arguments:					   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer   		Comments       	       		*/
/* 0.1   1/1/00   Daniel Sigg       					*/
/*                                                         		*/
/* Documentation References: 						*/
/*	Man Pages: doc++ generated html					*/
/*	References:							*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8336  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Unix						*/
/*	Compiler Used: Sun cc						*/
/*	Runtime environment: Solaris 					*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			TBD			*/
/*			  POSIX			TBD			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include "caltype.h"

#ifdef __cplusplus
extern "C" {
#endif

/** @name Calibration Utility Functions
    This API provides functions to work with calibration records.

    @memo C functions
    @author Written January 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

/*@{*/

/** @name Calibration record methods
    The definition of the calibration access methods.
    @memo Calibration record methods
 ************************************************************************/

/*@{*/

/** Initializes a calibration record. This function must be called
    before a calibration record is used. When finished the calrelease
    function frees the memory allocated by this function.

    @memo Constructor
    @param cal Calibration record
 ************************************************************************/
void calinit( calrec_t* cal );
/** Releases a calibration record. This function must be called
    after a calibration record has been used to free the associated
    memory.

    @memo Destructor
    @param cal Calibration record
 ************************************************************************/
void calrelease( calrec_t* cal );

/** Allocates an array of initialized calibration records. Returns
    Null if failed.

    @memo New operator
    @param num Number of records to allocate
    @return Array of calibration records
 ************************************************************************/
calrec_t* calnew( int num );
/** Frees an array of calibration records. This function calls the
    release function first.

    @memo Delete operator
    @param cal Pointer to calibration record array
 ************************************************************************/
void caldelete( calrec_t* cal );
/** Resizes an array of calibration records. Returns Null if failed
    (the original array pointer is still valid). If success, a new
    array pointer with the specified length is returned and the old
    array pointer is not valid anymore. Calibration records are add
    or deleted depending if the new length is larger or smaller than
    the old one. If the new length is zero, the memory is freed and
    NULL is returned.

    @memo Resize operator
    @param cal Pointer to old calibration record array
    @param num Number of records in new array
    @return New array of calibration records
 ************************************************************************/
calrec_t* calresize( calrec_t* cal, int num );
/** Returns the size of the calibration array.

    @memo Size operator
    @param cal Pointer to calibration record array
    @return Size of array
 ************************************************************************/
int calsize( const calrec_t* cal );

/** Copies cal2 into cal1.
    @memo Copy operator
    @param cal1 calibration record (destination)
    @param cal2 calibration record (source)
 ************************************************************************/
void calcpy( calrec_t* cal1, const calrec_t* cal2 );

/** Compares two calibration records. Returns 0 if equal, <0 if
    cal1 < cal2, and >0 if cal1 > cal2. The comparison is case 
    insensitive. The following fields are included in the compare
    (in this order): fChannel, fReference, fUnit and fTime.
    @memo Compare method
    @param cal1 calibration record
    @param cal2 calibration record
    @return 0 if equal, <0 if cal1<cal2, >0 if cal1>cal2
 ************************************************************************/
int calcmp( const calrec_t* cal1, const calrec_t* cal2 );

/** Compares two calibration records. Returns 0 if equal, <0 if
    cal1 < cal2, and >0 if cal1 > cal2. The comparison is case 
    insensitive. The following fields are included in the compare
    (in this order): fChannel, fReference and fUnit.
    @memo Simple compare method
    @param cal1 calibration record
    @param cal2 calibration record
    @return 0 if equal, <0 if cal1<cal2, >0 if cal1>cal2
 ************************************************************************/
int calcmp2( const calrec_t* cal1, const calrec_t* cal2 );

/** This function is used by the query function to determine if 
    a query record matches a calibration record.
    @memo Match method
    @param query query record
    @param cal calibration record
    @return 1 if match, 0 otherwise
 ************************************************************************/
int calmatch( const calrec_t* query, const calrec_t* cal );

/** This function sorts an array of calibration records.
    @memo Sort method
    @param cal calibration record array
    @param len Length of array
    @return 1 if match, 0 otherwise
 ************************************************************************/
void calsort( calrec_t* cal, int len );

/** This function searches through a sorted calibration record array.
    @memo Search method
    @param key search key
    @param cal calibration record array
    @param len Length of array
    @return calibration record if found, NULL if not
 ************************************************************************/
calrec_t* calbsearch( const calrec_t* key, const calrec_t* cal, int len );

/** This function insert a record into a sorted calibration record array.
    If the record already exists it will be overwritten. The record is
    copied. callist has to be allocated by calnew.
    @memo Insert method
    @param cal calibration record array
    @param len Length of array
    @param newcal new element to be inserted
    @return 0 if successful, <0 if not
 ************************************************************************/
int calinsert( calrec_t** cal, int* len, const calrec_t* newcal );

/** This function deletes a record from a sorted calibration record array.
    If the record already exists it will be overwritten. callist has to 
    be allocated by calnew.
    @memo Delete method
    @param cal calibration record array
    @param len Length of array
    @param delcal element to be deleted
    @return 0 if successful, <0 if not
 ************************************************************************/
int calremove( calrec_t** cal, int* len, const calrec_t* delcal );

/** This function reads calibration records from a file. The pointer
    to the calibration record array must either point to NULL or 
    point to an array which was previously allocated with calnew or
    calresize. Upon successful return array pointer will contain
    a valid address if at least one record was read; otherwise this
    argument will be unchanged. This function will call calresize
    if the number of records in the file is larger than the size of
    the specified array. If max is negative, all records are read.
    (This function uses the expat xml parser.)
    @memo Read method
    @param flag flag indication the type of records to be read
    @param cal pointer to calibration record array
    @param len Length of array
    @param file name of file to read
    @return Number of read records, <0 on error
 ************************************************************************/
int calread( int flag, calrec_t** cal, int max, const char* file );

/** This function writes calibration records to a file.
    @memo Write method
    @param flag flag indication the type of records to write
    @param cal calibration record array
    @param len Length of array
    @param file name of file to write
    @return 0 if successful, <0 on error
 ************************************************************************/
int calwrite( int flag, calrec_t* cal, int len, const char* file );

/** Sets the channel name.
    @memo Set channel name method
    @param cal calibration record
    @param chnname channel name
 ************************************************************************/
void calsetchannel( calrec_t* cal, const char* chnname );
/** Gets the channel name.
    @memo Get channel name method
    @param cal calibration record
    @return channel name
 ************************************************************************/
const char* calgetchannel( const calrec_t* cal );

/** Sets the reference point.
    @memo Set reference point method
    @param cal calibration record
    @param ref channel name
 ************************************************************************/
void calsetref( calrec_t* cal, const char* ref );
/** Gets the reference point.
    @memo Get reference point method
    @param cal calibration record
    @return channel name
 ************************************************************************/
const char* calgetref( const calrec_t* cal );

/** Sets the unit string.

    @memo Set unit string method
    @param cal calibration record
    @param unit unit string
 ************************************************************************/
void calsetunit( calrec_t* cal, const char* unit );
/** Gets the unit string.

    @memo Get unit string method
    @param cal calibration record
    @return unit string
 ************************************************************************/
const char* calgetunit( const calrec_t* cal );

/** Sets the calibration time.

    @memo Set calibration time method
    @param cal calibration record
    @param time time
 ************************************************************************/
void calsettime( calrec_t* cal, unsigned long time );
/** Gets the calibration time.

    @memo Get calibration time method
    @param cal calibration record
    @return time
 ************************************************************************/
unsigned long calgettime( const calrec_t* cal );

/** Sets the calibration duration.

    @memo Set calibration duration method
    @param cal calibration record
    @param dur duration
 ************************************************************************/
void calsetduration( calrec_t* cal, unsigned long dur );
/** Gets the calibration duration.

    @memo Get calibration duration method
    @param cal calibration record
    @return duration
 ************************************************************************/
unsigned long calgetduration( const calrec_t* cal );

/** Gets the calibration type.

    @memo Get calibration type method
    @param cal calibration record
    @return type
 ************************************************************************/
int calgettype( const calrec_t* cal );

/** Sets the conversion factor of a calibration record.

    @memo Set conversion factor method
    @param cal calibration record
    @param conv conversion factor
 ************************************************************************/
void calsetconversion( calrec_t* cal, double conv );
/** Resets the conversion factor of a calibration record.

    @memo Reset conversion factor method
    @param cal calibration record
 ************************************************************************/
void calresetconversion( calrec_t* cal );
/** Gets the conversion factor of a calibration record.

    @memo Get conversion factor method
    @param cal calibration record
    @return conversion factor
 ************************************************************************/
double calgetconversion( const calrec_t* cal );

/** Sets the offset of a calibration record.

    @memo Set offset method
    @param cal calibration record
    @param ofs offset
 ************************************************************************/
void calsetoffset( calrec_t* cal, double ofs );
/** Resets the offset of a calibration record.

    @memo Reset offset method
    @param cal calibration record
 ************************************************************************/
void calresetoffset( calrec_t* cal );
/** Gets the offset of a calibration record.

    @memo Get offset method
    @param cal calibration record
    @return offset
 ************************************************************************/
double calgetoffset( const calrec_t* cal );

/** Sets the time delay of a calibration record.

    @memo Set time delay method
    @param cal calibration record
    @param dt time delay
 ************************************************************************/
void calsettimedelay( calrec_t* cal, double td );
/** Resets the time delay of a calibration record.

    @memo Reset time delay method
    @param cal calibration record
 ************************************************************************/
void calresettimedelay( calrec_t* cal );
/** Gets the time delay of a calibration record.

    @memo Get time delay method
    @param cal calibration record
    @return time delay
 ************************************************************************/
double calgettimedelay( const calrec_t* cal );

/** Sets the transfer function of a calibration record. If a
    NULL pointer is specified the transfer function is disabled.

    @memo Set transfer function method
    @param cal calibration record
    @param trans Transfer function array
    @param len Number of points
 ************************************************************************/
void calsettransferfunction( calrec_t* cal, const float* trans, int len );
/** Gets the transfer function of a calibration record.

    @memo Get transfer function method
    @param cal calibration record
    @param trans Transfer function array pointer (return)
    @return Number of points
 ************************************************************************/
int calgettransferfunction( const calrec_t* cal, const float** trans );

/** Sets the pole and zeros of a calibration record. If a Null pointer
    is specified the poles and zeros are disabled.

    @memo Set pole-zero method
    @param cal calibration record
    @param gain Gain
    @param pnum Number of poles
    @param znum Number of zeros
    @param pzs Complex array of poles and zeros
 ************************************************************************/
void calsetpolezeros(
    calrec_t* cal, double gain, int pnum, int znum, const float* pzs );
/** Gets the poles and zeros of a calibration record.

    @memo Get pole-zero method
    @param cal calibration record
    @param gain Gain (return)
    @param pnum Number of poles (return)
    @param znum Number of zeros (return)
    @param pzs Complex array of poles and zeros (return)
    @return true if successful
 ************************************************************************/
int calgetpolezeros( const calrec_t* cal,
                     double*         gain,
                     int*            pnum,
                     int*            znum,
                     const float**   pzs );

/** Sets the default flag of a calibration record.

    @memo Set default method
    @param cal calibration record
    @param def default
 ************************************************************************/
void calsetdefault( calrec_t* cal, int def );
/** Gets the default flag of a calibration record.

    @memo Get default method
    @param cal calibration record
    @return default state
 ************************************************************************/
int calgetdefault( const calrec_t* cal );

/** Sets the preferred magnitude of a calibration record.

    @memo Set preferred magnitude method
    @param cal calibration record
    @param mag preferred magnitude (power of 10)
 ************************************************************************/
void calsetpreferredmag( calrec_t* cal, int mag );
/** Gets the preferred magnitude of a calibration record.

    @memo Get preferred magnitude method
    @param cal calibration record
    @return preferred magnitude
 ************************************************************************/
int calgetpreferredmag( const calrec_t* cal );

/** Sets the preferred derivative of a calibration record.

    @memo Set preferred derivative method
    @param cal calibration record
    @param d preferred derivative
 ************************************************************************/
void calsetpreferredd( calrec_t* cal, int d );
/** Gets the preferred derivative of a calibration record.

    @memo Get preferred derivative method
    @param cal calibration record
    @return preferred derivative
 ************************************************************************/
int calgetpreferredd( const calrec_t* cal );

/** Sets the comment field of a calibration record.

    @memo Set comment method
    @param cal calibration record
    @param comment Comment
 ************************************************************************/
void calsetcomment( calrec_t* cal, const char* comment );
/** Gets the comment field of a calibration record.

    @memo Get comment method
    @param cal calibration record
    @return Comment
 ************************************************************************/
const char* calgetcomment( const calrec_t* cal );

/*@}*/

/** @name XML functions
    A set of functions to convert calibration records to and from
    the LIGO lightweight data format. The XML parser function uses
    the expat XML parser, i.e. the expat library has to be linked in
    for all programs which use the XML parser function.
    @memo XML functions
 ************************************************************************/

/*@{*/

/** This function prototype is used in the callback of the XML parser.

    @memo XML callback function prototype
    @param flag indicating the record type
    @param cal calibration record
    @param user user argument
    @param errmsg error message for error records
 ************************************************************************/
typedef void ( *cal_cbfunc )( int             flag,
                              const calrec_t* cal,
                              void*           user,
                              const char*     errmsg );
/** This function prototype is used in the authorization callback of 
    the XML parser.

    @memo XML authorization callback function prototype
    @param username user name
    @param passwd password
    @param user user argument
 ************************************************************************/
typedef void ( *calauth_cbfunc )( const char* username,
                                  const char* passwd,
                                  void*       user );

/** This structure is used by the XML parser to determine callback
    function and user argument. This strucrure MUST be allocated 
    with xml2cal_new and MUST be freed with xml2cal_free. After a new
    structure is created the user has to set the callback and user
    argument before making calls to the parser function.

    @memo XML parser structure
 ************************************************************************/
struct xml2cal_t
{
    /** Calibration record callback */
    cal_cbfunc fCal;
    /** Authorization callback */
    calauth_cbfunc fCalAuth;
    /** User argument which is simply passed to the callback */
    void* fUser;
};
typedef struct xml2cal_t xml2cal_t;

/** Create a new XML parser strcuture for parsing calibration records
    in XML format.

    @memo Create a new XML parser structure
    @return new XML parser structure
 ************************************************************************/
xml2cal_t* xml2cal_new( void );
/** Frees a XML parser structure.

    @memo Frees an XML parser structure
    @param parser XML parser structure
 ************************************************************************/
void xml2cal_free( xml2cal_t* parser );
/** Sets the callback arguments of the XML calibration parser. If a 
    NULL pointer is supplied as the callback function, no call is
    made.

    @memo Set callback parameters
    @param parser XML parser structure
    @param cal calibration record callback function
    @param auth authorization record callback function
    @param user user argument
 ************************************************************************/
void xml2cal_set( xml2cal_t*     parser,
                  cal_cbfunc     cal,
                  calauth_cbfunc auth,
                  void*          user );

/** Parses an XML file containing calibration records. The function
    can be called successively feeding in additional data. The function
    returns 0 if an end-of-file mark (empty LIGO_LW element) is 
    discovered; 1 if to continue; -1 on error. Whenever a complete 
    calibration record or authorization record has been scanned in, 
    the corresponding callback function is called.

    @memo XML parser for calibration records
    @param parser XML parser structure
    @param buf text buffer with new data
    @param size size of text buffer
    @return 1 to continue, 0 when done, -1 on error
 ************************************************************************/
int xml2cal( xml2cal_t* parser, const char* buf, int size );

/** Converts a calibration record into its XML representation.
    The buffer must be large enough to hold the complete XML text;
    if not an error is returned.
    @memo Convert calibration record into XML
    @param flag flag indicating the record type
    @param buf text buffer for XML result
    @param size size of text buffer
    @param index Calibration record number (<0 for none)
    @param errmsg optional error message for error records
    @return length of XML text if successful, <0 on error
 ************************************************************************/
int cal2xml( int             flag,
             const calrec_t* cal,
             char*           buf,
             int             size,
             int             index,
             const char*     errormsg );
/** Converts an authorization record into its XML representation.
    The buffer must be large enough to hold the complete XML text;
    if not an error is returned.
    @memo Convert authorization record into XML
    @param username user name
    @param passwd password
    @param buf text buffer for XML result
    @param size size of text buffer
    @return length of XML text if successful, <0 on error
 ************************************************************************/
int auth2xml( const char* username, const char* passwd, char* buf, int size );
/*@}*/

/** @name Unit functions
    A set of functions to find related units, deduced units and 
    add a magnitude qualifier.
    @memo Unit functions
 ************************************************************************/

/*@{*/

/** This function initializes a signal information strcuture to:
    \begin{verbatim}
    channel relation = single channel
    coefficients = 1
    exponents = 1
    domain[0] = time 
    domain[1] = value
    densitity units = no
    Bandwidths = 0
    \end{verbatim}

    @memo Initializes a signal info structure
 ************************************************************************/
void calsiginfo_init( calsignalinfo_t* sig );

/** Finds related units. This function uses a table to find
    related units, e.g., Celsius and Kelvin. This function takes
    a signal information and up to two calibration records as 
    arguments. If related units are found deduced unit structures 
    are written to the result array and their number is returned.
    The returned deduced unit structures contain references to
    the signal info and the calibration records. This structures
    have to stay in memory during the lifetime of the returned
    deduced units.

    @memo Find related units
    @param flag set to 0
    @param info signal information
    @param chn1 calibration record of first channel
    @param chn2 calibration record of second channel
    @param ded array of deduced units
    @param max size of deduced unit array
    @return number of deduced units added, <0 on error
 ************************************************************************/
int cal_deduce( int                    flag,
                const calsignalinfo_t* info,
                const calrec_t*        chn1,
                const calrec_t*        chn2,
                caldeduced_t*          ded,
                int                    max );

/** Transforms units from time series into power spectral densities,
    channel ratios and channel products. This function uses 1 (PSD) or
    2 (ratio and product) input arrays of calibration records and 
    generates deduced units in an output array which has to be the 
    length of the input array (PSD) or the product of the input arrays
    (ratio and product), respectively. The function returns the number
    of deduced unit records, or <0 on error.
    @memo Create transformed units
    @param flag describes the transformation
    @param calA 1st calibration record array
    @param lenA number of calibration records in 1st array
    @param calB 2nd calibration record array
    @param lenB number of calibration records in 2nd array
    @param cal output calibration record array
    @param max maximum size of output calibration record array
    @return number of transformed records, <0 on error
 ************************************************************************/
int cal_transform( int       flag,
                   calrec_t* calA,
                   int       lenA,
                   calrec_t* calB,
                   int       lenB,
                   calrec_t* cal,
                   int       max );
/** Returns the unit string of a calibration record and optionally
    adds a magnitude qualifier.
    @memo return unit strings
    @param flag set to 0
    @param cal calibration record array
    @param mag Magnitude (power of 10)
    @param unit unit string (return)
    @param size maximum size of unit string
    @return 0 if successful, <0 on error
 ************************************************************************/
int cal_string( int flag, calrec_t* cal, int mag, char* unit, int size );
/*@}*/

/** @name Calibration transformation functions
    A set of functions to perform calibration transformations in
    both the time domain and the frequency domain.
    @memo Calibration transformation functions
 ************************************************************************/

/*@{*/

/** Performs a simple calibration correction on an array of data 
    points in the time domain. This function only uses the conversion
    factor and the offset correction.
    @memo Simple calibration correction for time domain data
    @param flag set to 0
    @param cal calibration record
    @param value input data
    @param phys converted output array
    @param len number of data points
    @return 0 if successful, <0 on error
 ************************************************************************/
int tcal_simple(
    int flag, const calrec_t* cal, const float* value, float* phys, int len );
/** Performs a calibration correction on an array of data 
    points in the frequency domain. This function uses the 
    transfer function if supplied and the conversion factor otherwise.
    If a zero frequency data point is supplied, an offset correction
    will be done only if the specified bandwidth is non-zero. The
    value is subtracted is then offset/sqrt(bandwidth). If a PSD
    unit of unit^2 or unit^2/Hz is specifed the function will do
    the square. PSD arrays are assumed to be specified in units per 
    root Hertz. If the desired units are either straight units or
    unit^2, a valid bandwidth must specified.

    @memo Calibration correction for frequency domain data
    @param flag set to 0
    @param cal calibration record
    @param f array of frequency values
    @param value input data
    @param phys converted output array
    @param len number of data points
    @param complex set to 1 if complex
    @param bw Bandwidth (set to zero if offset should be ignored)
    @return 0 if successful, <0 on error
 ************************************************************************/
int fcal( int             flag,
          const calrec_t* cal,
          const float*    f,
          const float*    value,
          float*          phys,
          int             len,
          int             complex,
          double          bw );
/** Performs a calibration correction on an array of data 
    points in the frequency domain. This function uses the 
    transfer function if supplied and the conversion factor otherwise.
    This functions behaves the same as fcal.

    @memo Calibration correction for frequency domain data
    @param flag set to 0
    @param cal calibration record
    @param f0 start frequency
    @param df frequency spacing
    @param value input data
    @param phys converted output array
    @param len number of data points
    @param complex set to 1 if complex
    @param bw Bandwidth (set to zero if offset should be ignored)
    @return 0 if successful, <0 on error
 ************************************************************************/
int fcal2( int             flag,
           const calrec_t* cal,
           double          f0,
           double          df,
           const float*    value,
           float*          phys,
           int             len,
           int             complex,
           double          bw );
/*@}*/

#ifdef __cplusplus
}
#endif

/*@}*/

#endif /*_CALUTIL_H */
