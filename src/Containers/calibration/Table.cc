#ifndef __EXTENSIONS__
#define __EXTENSIONS__
#endif

#include "Table.hh"
#include "Calibration.hh"
#include "Descriptor.hh"
#include "calutil.h"
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <fstream>

//================================== Debug print whether default units are used
// #define _USED_DEF_UNITS 1

namespace calibration
{
    using namespace std;

    const char* const kCalibrationFileEnv = "CALIBRATIONFILE";
    const char* const kDefaultCalName = "[Default]";
    const char* const kDefaultCalTFName = "[Default-TF]";

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // Test if an XML file					                //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    bool
    isXML( const char* filename, bool* exists )
    {
        bool     isXML = false;
        ifstream inp( filename );
        if ( inp )
        {
            if ( exists != 0 )
                *exists = true;
            // get the first non-empty line
            string line;
            while ( inp )
            {
                getline( inp, line, '\n' );
                while ( !line.empty( ) && ( line[ 0 ] == ' ' ) )
                {
                    line.erase( 0, 1 );
                }
                if ( !line.empty( ) )
                {
                    break;
                }
            }
            // check if it has an xml header
            isXML = line.find( "<?xml version=\"1.0\"?>" ) != string::npos;
        }
        else
        {
            if ( exists != 0 )
                *exists = false;
        }
        inp.close( );
        return isXML;
    }

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // Specialized unit descriptors (reference to another one)              //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    class UnitRef : public Unit
    {
    protected:
        const Unit* fRef;

    public:
        UnitRef( const char* name, const Unit* ref ) : Unit( name ), fRef( ref )
        {
        }
        virtual ~UnitRef( )
        {
        }
        /// Unit is power rather than amplitude
        virtual bool
        IsPower( ) const
        {
            return fRef ? fRef->IsPower( ) : false;
        }
        virtual bool
        Apply( float*         x,
               float*         y,
               int            N,
               EUnitMagnitude mag = kUnitMagOne,
               bool           complex = false ) const
        {
            return fRef ? fRef->Apply( x, y, N, mag, complex ) : true;
        }
        virtual const char*
        GetTrueName( ) const
        {
            return fRef->GetTrueName( );
        }
    };

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // Specialized unit descriptors (reference to a deduced unit struct)    //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    class UnitDeduced : public Unit
    {
        friend class Table;

    protected:
        /// Deduced unit structure
        caldeduced_t fDeduced;

    public:
        /// Constructor
        UnitDeduced( const caldeduced_t& ded, Descriptor* cald = 0 )
            : Unit( ded.fUnit, cald ), fDeduced( ded )
        {
        }
        /// Destructor
        virtual ~UnitDeduced( )
        {
        }
        /// Unit is power rather than amplitude
        virtual bool
        IsPower( ) const
        {
            return fDeduced.fPowerUnit;
        }
        /// Correct
        virtual bool
        Apply( float*         x,
               float*         y,
               int            N,
               EUnitMagnitude mag = kUnitMagOne,
               bool           complex = false ) const
        {
            if ( ( y == 0 ) || ( fDeduced.fTrans == 0 ) )
            {
                return false;
            }
            else
            {
                return fDeduced.fTrans( &fDeduced, 0, x, y, N, mag, complex );
            }
        }
    };

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // ChannelItem				                                //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    bool
    Table::ChannelItem::operator==( const ChannelItem& obj ) const
    {
        return strcasecmp( fName.c_str( ), obj.fName.c_str( ) ) == 0;
    }

    //______________________________________________________________________________
    bool
    Table::ChannelItem::operator<( const ChannelItem& obj ) const
    {
        return strcasecmp( fName.c_str( ), obj.fName.c_str( ) ) < 0;
    }

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // ChannelNameCmp			                                //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    bool
    Table::ChannelNameCmp::operator( )( const std::string& c1,
                                        const std::string& c2 ) const
    {
        return strcasecmp( c1.c_str( ), c2.c_str( ) ) < 0;
    }

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // Calibration table channel item class                                 //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////

    int Table::ChannelItem::fLast = 0;

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // Calibration table class                                              //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    Table::Table( const char* lookupfile, bool supdef ) : fSupDefault( supdef )
    {
        fLookupFile = lookupfile ? lookupfile : "";
        if ( fSupDefault )
            AddChannel( kDefaultCalName );
        if ( fSupDefault )
            AddChannel( kDefaultCalTFName );
    }

    //______________________________________________________________________________
    Table::~Table( )
    {
    }

    //______________________________________________________________________________
    Calibration&
    Table::operator[]( int i )
    {
        return fCal[ i ];
    }

    //______________________________________________________________________________
    const Calibration&
    Table::operator[]( int i ) const
    {
        return fCal[ i ];
    }

    //______________________________________________________________________________
    int
    Table::FindFirst( const char* chn ) const
    {
        Calibration cal;
        cal.SetChannel( chn );
        CalibrationCmp                  cmp( CalibrationCmp::kNameOnly );
        CalibrationList::const_iterator i =
            lower_bound( fCal.begin( ), fCal.end( ), cal, cmp );
        if ( ( i == fCal.end( ) ) || !cmp.IsEqual( *i, cal ) )
        {
            return -1;
        }
        else
        {
            return i - fCal.begin( );
        }
    }

    //______________________________________________________________________________
    const Calibration*
    Table::Search( const char* chn,
                   const char* ref,
                   const char* unit,
                   const Time& time ) const
    {
        Calibration cal;
        cal.SetChannel( chn );
        cal.SetRef( ref );
        cal.SetUnit( unit );
        cal.SetTime( time );
        return Search( cal, CalibrationCmp( CalibrationCmp::kNormal ) );
    }

    //______________________________________________________________________________
    Calibration*
    Table::Search( const char* chn,
                   const char* ref,
                   const char* unit,
                   const Time& time )
    {
        Calibration cal;
        cal.SetChannel( chn );
        cal.SetRef( ref );
        cal.SetUnit( unit );
        cal.SetTime( time );
        return Search( cal, CalibrationCmp( CalibrationCmp::kNormal ) );
    }

    //______________________________________________________________________________
    const Calibration*
    Table::Search( const Calibration& rec, CalibrationCmp cmp ) const
    {
        CalibrationList::const_iterator i =
            lower_bound( fCal.begin( ), fCal.end( ), rec, cmp );
        if ( ( i == fCal.end( ) ) || !cmp.IsEqual( *i, rec ) )
        {
            return 0;
        }
        else
        {
            return &*i;
        }
    }

    //______________________________________________________________________________
    Calibration*
    Table::Search( const Calibration& rec, CalibrationCmp cmp )
    {
        CalibrationList::iterator i =
            lower_bound( fCal.begin( ), fCal.end( ), rec, cmp );
        if ( ( i == fCal.end( ) ) || !cmp.IsEqual( *i, rec ) )
        {
            return 0;
        }
        else
        {
            return &*i;
        }
    }

    //______________________________________________________________________________
    bool
    Table::Add( const char* chn,
                const char* ref,
                const char* unit,
                const Time& start,
                bool        overwrite )
    {
        Calibration cal;
        cal.SetChannel( chn );
        cal.SetRef( ref );
        cal.SetUnit( unit );
        cal.SetTime( start );
        return Add( cal, overwrite );
    }

    //______________________________________________________________________________
    bool
    Table::Add( const Calibration& rec, bool overwrite )
    {
        CalibrationList::iterator i =
            lower_bound( fCal.begin( ), fCal.end( ), rec );
        if ( ( i != fCal.end( ) ) && ( *i == rec ) )
        {
            if ( overwrite )
                *i = rec;
        }
        else
        {
            fCal.insert( i, rec );
        }
        return true;
    }

    //______________________________________________________________________________
    bool
    Table::Delete( const char* chn,
                   const char* ref,
                   const char* unit,
                   const Time& time )
    {
        Calibration cal;
        cal.SetChannel( chn );
        cal.SetRef( ref );
        cal.SetUnit( unit );
        cal.SetTime( time );
        return Delete( cal );
    }

    //______________________________________________________________________________
    bool
    Table::Delete( const Calibration& rec )
    {
        CalibrationCmp            cmp( CalibrationCmp::kNormal );
        CalibrationList::iterator i =
            lower_bound( fCal.begin( ), fCal.end( ), rec, cmp );
        while ( ( i != fCal.end( ) ) && cmp.IsEqual( *i, rec ) )
        {
            fCal.erase( i );
        }
        return true;
    }

    //______________________________________________________________________________
    void
    Table::Clear( )
    {
        fCal.clear( );
    }

    //______________________________________________________________________________
    const Table::ChannelItem*
    Table::FindChannel( const char* chn ) const
    {
        ChannelList::const_iterator f = fChannels.find( chn );
        if ( f == fChannels.end( ) )
        {
            return 0;
        }
        else
        {
            return &f->second;
        }
    }

    //______________________________________________________________________________
    bool
    Table::AddChannel( const char* chn )
    {
        if ( !chn || !*chn )
        {
            return false;
        }
        fLookupChns.insert(
            ChannelList::value_type( chn, ChannelItem( chn ) ) );
        return fChannels
            .insert( ChannelList::value_type( chn, ChannelItem( chn ) ) )
            .second;
    }

    //______________________________________________________________________________
    bool
    Table::DeleteChannel( const char* chn )
    {
        if ( fSupDefault && chn && ( strcmp( chn, kDefaultCalName ) == 0 ) )
        {
            return false;
        }

        return fChannels.erase( chn ) > 0;
    }

    //______________________________________________________________________________
    bool
    Table::ClearChannels( )
    {
        fChannels.clear( );
        if ( fSupDefault )
            AddChannel( kDefaultCalName );
        if ( fSupDefault )
            AddChannel( kDefaultCalTFName );

        return true;
    }

    //______________________________________________________________________________
    bool
    Table::EnableChannel( const char* chn, bool set )
    {
        ChannelList::iterator f = fChannels.find( chn );
        if ( f == fChannels.end( ) )
        {
            return false;
        }
        else
        {
            f->second.SetEnable( set );
            return true;
        }
    }

    //______________________________________________________________________________
    bool
    Table::IsEnabledChannel( const char* chn )
    {
        ChannelList::iterator f = fChannels.find( chn );
        if ( f == fChannels.end( ) )
        {
            return false;
        }
        else
        {
            return f->second.IsEnabled( );
        }
    }

    //______________________________________________________________________________
    bool
    Table::Import( const char* filename )
    {
        bool ret = false;

        calrec_t* callist = NULL;
        int       n = calread( CALNORMAL, &callist, -1, filename );
        if ( n < 0 )
        {
            ret = false;
        }
        else
        {
            for ( int i = 0; i < n; i++ )
            {
                //if (AddChannel (calgetchannel (callist + i))) {
                AddChannel( calgetchannel( callist + i ) );
                Add( callist[ i ] );
                //}
            }
            ret = true;
        }
        if ( callist )
        {
            caldelete( callist );
        }
        return ret;
    }

    //______________________________________________________________________________
    bool
    Table::Export( const char* filename ) const
    {
        if ( fCal.empty( ) )
        {
            return false;
        }
        return fCal.Write( filename );
    }

    //______________________________________________________________________________
    void
    Table::AddUnits( Descriptor& cal )
    {
        // Check if channels are in list
        if ( fChannels.find( cal.GetChannel( 0 ) ) == fChannels.end( ) )
        {
            AddChannel( cal.GetChannel( 0 ) );
        }
        if ( ( (ECalChnRelation)cal.GetRelation( ) != kCalChnRelSingle ) &&
             ( fChannels.find( cal.GetChannel( 1 ) ) == fChannels.end( ) ) )
        {
            AddChannel( cal.GetChannel( 1 ) );
        }
        // Check for lookup
        if ( !fLookupChns.empty( ) )
        {
            Lookup( );
        }
        // cout << "add units to " << cal.GetChannel(0) << " : " <<
        // cal.GetChannel(1) <<  endl;
        // Check if we use default calibration
#ifdef _USED_DEF_UNITS
        bool usedef = false;
#endif
        bool   isTF = false;
        string channelZero = cal.GetChannel( 0 );
        string channelOne = cal.GetChannel( 1 );
        if ( cal.IsValid( ) && ( FindFirst( channelZero.c_str( ) ) < 0 ) &&
             ( ( (ECalChnRelation)cal.GetRelation( ) == kCalChnRelSingle ) ||
               ( FindFirst( cal.GetChannel( 1 ) ) < 0 ) ) )
        {
            if ( (ECalChnRelation)cal.GetRelation( ) != kCalChnRelSingle )
            {
                channelOne = kDefaultCalName;
            }
            // Is it a transfer function?
            if ( ( (ECalChnRelation)cal.GetRelation( ) != kCalChnRelSingle ) &&
                 ( cal.GetExpo( 0 ) == -1 ) && ( cal.GetExpo( 1 ) == 1 ) )
            {
                channelZero = kDefaultCalTFName;
                isTF = true;
            }
            else
            {
                channelZero = kDefaultCalName;
            }
#ifdef _USED_DEF_UNITS
            usedef = true;
#endif
        }
#ifdef _USED_DEF_UNITS
        cout << "Use default units " << usedef << endl;
#endif
        //cout << "Use default TF units " << isTF << endl;
        // Loop over units
        Time caltime = cal.GetTime( );
        for ( int i = 0; i < 2; i++ )
        {
            Descriptor::ECalUnit u = (Descriptor::ECalUnit)i;
            // start with none
            cal.Units( u ).Clear( );
            Unit* def = new UnitRef( "none",
                                     i == 0 ? &UnitScaling::UnitIdentityX( )
                                            : &UnitScaling::UnitIdentityY( ) );
            cal.Units( u ).Add( def );

            // add units for function values
            if ( cal.IsValid( ) && ( cal.Domain( u ) == kCalDomainValue ) )
            {
                // :TODO: currently only support tY and fY
                if ( ( cal.GetRelation( ) == kCalChnRelXY ) || ( i == 0 ) )
                {
                    continue;
                }
                SignalInfo* siginfo = new ( std::nothrow ) SignalInfo;
                if ( siginfo != 0 )
                {
                    calsignalinfo_t* info = *siginfo;
                    cal.SetInfo( siginfo );
                    const int    maxu = 100;
                    caldeduced_t ded[ maxu ];
                    // fill in info
                    calsiginfo_init( info );
                    info->fChnRelation = (ECalChnRelation)cal.GetRelation( );
                    info->fCoeff[ 0 ] = cal.GetCoeff( 0 );
                    info->fCoeff[ 1 ] = cal.GetCoeff( 1 );
                    info->fExpo[ 0 ] = cal.GetExpo( 0 );
                    info->fExpo[ 1 ] = cal.GetExpo( 1 );
                    info->fConj[ 0 ] = cal.GetConj( 0 );
                    info->fConj[ 1 ] = cal.GetConj( 1 );
                    // Default TF cal is special
                    if ( isTF )
                    {
                        info->fChnRelation = kCalChnRelSingle;
                        if ( (ECalChnRelation)cal.GetRelation( ) !=
                             kCalChnRelSingle )
                        {
                            info->fExpo[ 0 ] = 1;
                            info->fExpo[ 1 ] = 0;
                        }
                    }
                    info->fDomain = (ECalDomain)cal.Domain(
                        ( Descriptor::ECalUnit )( ( i + 1 ) % 2 ) );
                    info->fDensityUnits = (ECalDensity)cal.GetDensity( u );
                    info->fBW = cal.GetBW( u );
                    // loop over A channel calibration records
                    UnitDeduced* defnew = 0;
                    bool         firstmatch = false;
                    Calibration  rec;
                    rec.SetChannel( channelZero.c_str( ) );
                    CalibrationCmp cmp( CalibrationCmp::kNameOnly );
                    CalibrationCmp cmp2( CalibrationCmp::kNameRefUnit );
                    for ( int k = FindFirst( channelZero.c_str( ) );
                          ( k >= 0 ) && ( k < (int)fCal.size( ) ) &&
                          cmp.IsEqual( fCal[ k ], rec );
                          ++k )
                    {
                        // check time
                        if ( ( caltime < fCal[ k ].GetTime( ) ) ||
                             ( ( k + 1 < (int)fCal.size( ) ) &&
                               cmp2.IsEqual( fCal[ k ], fCal[ k + 1 ] ) &&
                               caltime >= fCal[ k + 1 ].GetTime( ) ) )
                        {
                            continue;
                        }
                        // cout << "found a unit for A " << fCal[k].GetUnit () << endl;
                        bool defA = fCal[ k ].GetDefault( );
                        // single channel or default cal?
                        if ( info->fChnRelation == kCalChnRelSingle )
                        {
                            int n =
                                cal_deduce( 0, info, fCal[ k ], 0, ded, maxu );
                            // cout << "Number of deduced units = " << n << endl;
                            bool firstdef = false;
                            for ( int j = 0; j < n; j++ )
                            {
                                // cout << "add unit " << ded[j].fUnit << endl;
                                UnitDeduced* ud = new UnitDeduced( ded[ j ] );
                                cal.Units( u ).Add( ud );
                                if ( defA && !firstmatch )
                                {
                                    // generally take the first one
                                    if ( j == 0 )
                                    {
                                        defnew = ud;
                                        if ( strlen( ded[ j ].fUnit ) == 0 )
                                        {
                                            firstdef = true;
                                        }
                                    }
                                    // but overwrite it with first unit with correct
                                    // derivative
                                    if ( !firstdef &&
                                         ( ded[ j ].fiParam[ 0 ] ==
                                           fCal[ k ].GetPreferredD( ) ) )
                                    {
                                        defnew = ud;
                                        firstdef = true;
                                    }
                                }
                            }
                            if ( defA )
                            {
                                firstmatch = true;
                            }
                            continue;
                        }
                        // two channels...
                        // loop over B channel calibration records if needed
                        Calibration rec2;
                        rec2.SetChannel( channelOne.c_str( ) );
                        for ( int l = FindFirst( channelOne.c_str( ) );
                              ( l >= 0 ) && ( l < (int)fCal.size( ) ) &&
                              cmp.IsEqual( fCal[ l ], rec2 );
                              ++l )
                        {
                            // check time
                            if ( ( caltime < fCal[ l ].GetTime( ) ) ||
                                 ( ( l + 1 < (int)fCal.size( ) ) &&
                                   cmp2.IsEqual( fCal[ l ], fCal[ l + 1 ] ) &&
                                   caltime >= fCal[ l + 1 ].GetTime( ) ) )
                            {
                                continue;
                            }
                            // cout << "found a unit for B " << fCal[l].GetUnit () << endl;
                            bool defB = fCal[ l ].GetDefault( );
                            ;
                            int n = cal_deduce(
                                0, info, fCal[ k ], fCal[ l ], ded, maxu );
                            // cout << "Number of deduced units = " << n << endl;
                            bool firstdef = false;
                            for ( int j = 0; j < n; j++ )
                            {
                                // cout << "add unit " << ded[j].fUnit << endl;
                                UnitDeduced* ud = new UnitDeduced( ded[ j ] );
                                cal.Units( u ).Add( ud );
                                if ( defA && defB && !firstmatch )
                                {
                                    // generally take the first one
                                    if ( j == 0 )
                                    {
                                        defnew = ud;
                                        if ( strlen( ded[ j ].fUnit ) == 0 )
                                        {
                                            firstdef = true;
                                        }
                                    }
                                    // but overwrite it with first unit with correct
                                    // derivative
                                    if ( !firstdef &&
                                         ( ded[ j ].fiParam[ 0 ] ==
                                           fCal[ k ].GetPreferredD( ) ) &&
                                         ( ded[ j ].fiParam[ 2 ] ==
                                           fCal[ l ].GetPreferredD( ) ) )
                                    {
                                        defnew = ud;
                                        firstdef = true;
                                    }
                                }
                            }
                            if ( defA && defB )
                            {
                                firstmatch = true;
                            }
                        }
                    }
                    if ( defnew )
                    {
                        def = defnew;
                        if ( ( defnew->fDeduced.fChn1 != 0 ) &&
                             ( defnew->fDeduced.fChn2 == 0 ) )
                        {
                            cal.SetPreferredMag(
                                u,
                                calgetpreferredmag( defnew->fDeduced.fChn1 ) );
                        }
                    }
                }
            }

            // add time units
            else if ( cal.IsValid( ) && ( cal.Domain( u ) == kCalDomainTime ) )
            {
                def = new UnitScaling( UnitScaling::UnitSec( ) );
                cal.Units( u ).Add( def );
                cal.Units( u ).Add(
                    new UnitScaling( UnitScaling::UnitMin( ) ) );
                cal.Units( u ).Add(
                    new UnitScaling( UnitScaling::UnitHour( ) ) );
                cal.Units( u ).Add(
                    new UnitScaling( UnitScaling::UnitDay( ) ) );
            }

            // add frequency units
            else if ( cal.IsValid( ) &&
                      ( cal.Domain( u ) == kCalDomainFrequency ) )
            {
                def = new UnitScaling( UnitScaling::UnitHertz( ) );
                cal.Units( u ).Add( def );
                cal.Units( u ).Add(
                    new UnitScaling( UnitScaling::UnitRadPerSec( ) ) );
            }

            // add default
            if ( def )
            {
                cal.Units( u ).Add( new UnitRef( "default", def ) );
            }
        }
        // cout << "added units" << endl;
    }

    //______________________________________________________________________________
    bool
    Table::Lookup( bool forceall )
    {
        // check filename for calibration information
        if ( fLookupFile.empty( ) )
        {
            char* p = getenv( kCalibrationFileEnv );
            if ( p )
                fLookupFile = p;
        }
        if ( fLookupFile.empty( ) )
        {
            //cerr << "Unable to lookup calibration information" << endl;
            //fLookupChns.clear();
            return false;
        }
        // if forceall, add all channels to lookup list
        if ( forceall )
        {
            for ( ChannelList::iterator i = fChannels.begin( );
                  i != fChannels.end( );
                  ++i )
            {
                fLookupChns.insert( *i );
            }
        }

        // read file
        bool exists;
        if ( isXML( fLookupFile.c_str( ), &exists ) )
        {
            ImportQ( fLookupFile.c_str( ) );
        }
        else if ( !exists )
        {
            //cerr << "Unable to lookup calibration information" << endl;
            return false;
        }
        else
        {
            ifstream inp( fLookupFile.c_str( ) );
            if ( !inp )
            {
                return false;
            }
            // get non-empty/non-comment lines
            string line;
            while ( inp )
            {
                getline( inp, line, '\n' );
                while ( !line.empty( ) && isspace( line[ 0 ] ) )
                {
                    line.erase( 0, 1 );
                }
                if ( line.empty( ) || ( line[ 0 ] == '#' ) )
                {
                    continue;
                }
                ImportQ( line.c_str( ) );
            }
            inp.close( );
        }
        // clear lookup list
        fLookupChns.clear( );
        return true;
    }

    //______________________________________________________________________________
    bool
    Table::Lookup( const char* filename, bool forceall )
    {
        fLookupFile = filename ? filename : "";
        return Lookup( forceall );
    }

    //______________________________________________________________________________
    bool
    Table::ImportQ( const char* filename )
    {
        bool ret = false;

        calrec_t* callist = 0;
        int       n = calread( CALNORMAL, &callist, -1, filename );
        if ( n >= 0 )
        {
            for ( int i = 0; i < n; i++ )
            {
                if ( fLookupChns.find( calgetchannel( callist + i ) ) !=
                     fLookupChns.end( ) )
                {
                    Add( callist[ i ] );
                }
            }
            ret = true;
        }
        if ( callist )
        {
            caldelete( callist );
        }
        return ret;
    }

    //______________________________________________________________________________
    void
    Table::SupportDefault( bool sup )
    {
        fSupDefault = sup;
        if ( fSupDefault )
            AddChannel( kDefaultCalName );
        if ( fSupDefault )
            AddChannel( kDefaultCalTFName );
    }

    //______________________________________________________________________________
    const char*
    Table::DefaultChannel( ) const
    {
        return kDefaultCalName;
    }

    //______________________________________________________________________________
    const char*
    Table::DefaultTFChannel( ) const
    {
        return kDefaultCalTFName;
    }

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // Set default calibration table                                        //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    void
    SetDefaultTable( Table& cal )
    {
        cal.Clear( );
    }

#ifndef __NO_NAMESPACE
}
#endif
