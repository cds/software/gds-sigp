#ifndef __EXTENSIONS__
#define __EXTENSIONS__
#endif

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "expat.h"
#include "calutil.h"

#define MAXBUF 128 * 1024
#define MAXERR 256
#define MAXUSER 32
#define MAXPASSWD 32
#define MAXPARAM 64

struct xml2cal_alt
{
    cal_cbfunc     fCal;
    calauth_cbfunc fCalAuth;
    void*          fUser;
    XML_Parser     expat;
    int            finished;
    char           buf[ MAXBUF + 1 ];
    int            bufsize;
    calrec_t       cal;
    int            caltype;
    int            empty;
    char           errmsg[ MAXERR ];
    char           user[ MAXUSER ];
    char           passwd[ MAXPASSWD ];
    int            objlevel;
    int            callevel;
    int            objtype;
    int            prmlevel;
    char           prm[ MAXPARAM ];
    int            dim;
    float*         trans;
    int            tindx;
    int            gps;
};
typedef struct xml2cal_alt xml2cal_alt;

static void
eliminateSpaces( char* p )
{
    char* p2;
    int   i;

    p2 = p;
    while ( ( *p2 != 0 ) &&
            ( ( *p2 == ' ' ) || ( *p2 == '\t' ) || ( *p2 == '\n' ) ) )
    {
        p2++;
    }
    if ( p != p2 )
    {
        memmove( p, p2, strlen( p2 ) + 1 );
    }
    i = strlen( p ) - 1;
    while ( ( i >= 0 ) &&
            ( ( p[ i ] == ' ' ) || ( p[ i ] == '\t' ) || ( p[ i ] == '\n' ) ) )
    {
        p[ i ] = 0;
        i--;
    }
}

static void
startelement( xml2cal_alt* p, const char* name, const char** attributes )
{
    int i;

    /* LIGO_LW object */
    if ( ( strcasecmp( name, "LIGO_LW" ) == 0 ) ||
         ( strcasecmp( name, "XSIL" ) == 0 ) )
    {
        p->objlevel++;
        if ( p->callevel == -1 )
        {
            p->caltype = CALNORMAL;
            p->objtype = 0;
            p->empty = 1;
            i = 0;
            while ( attributes[ i ] != 0 )
            {
                p->empty = 0;
                if ( strcasecmp( attributes[ i ], "Name" ) == 0 )
                {
                    if ( strncasecmp(
                             attributes[ i + 1 ], "Calibration", 11 ) == 0 )
                    {
                        p->objtype = 1;
                    }
                    else if ( strcasecmp( attributes[ i + 1 ],
                                          "Authorization" ) == 0 )
                    {
                        p->objtype = 2;
                    }
                }
                if ( strcasecmp( attributes[ i ], "Type" ) == 0 )
                {
                    if ( strcasecmp( attributes[ i + 1 ], "Add" ) == 0 )
                    {
                        p->caltype = CALADD;
                    }
                    else if ( strcasecmp( attributes[ i + 1 ], "Delete" ) == 0 )
                    {
                        p->caltype = CALDELETE;
                    }
                    else if ( strcasecmp( attributes[ i + 1 ], "Query" ) == 0 )
                    {
                        p->caltype = CALQUERY;
                    }
                    else if ( strcasecmp( attributes[ i + 1 ], "Error" ) == 0 )
                    {
                        p->caltype = CALERROR;
                    }
                }
                i += 2;
            }
            if ( p->objtype != 0 )
            {
                p->callevel = p->objlevel;
            }
        }
    }
    /* Param object */
    else if ( ( p->objlevel == p->callevel ) &&
              ( strcasecmp( name, "Param" ) == 0 ) )
    {
        p->empty = 0;
        if ( ++p->prmlevel == 1 )
        {
            p->bufsize = 0;
            p->dim = 0;
            p->tindx = 0;
            strcpy( p->prm, "" );
            i = 0;
            while ( attributes[ i ] != 0 )
            {
                if ( strcasecmp( attributes[ i ], "Name" ) == 0 )
                {
                    strncpy( p->prm, attributes[ i + 1 ], MAXPARAM - 1 );
                }
                else if ( strcasecmp( attributes[ i ], "Dim" ) == 0 )
                {
                    p->dim = atoi( attributes[ i + 1 ] );
                }
                i += 2;
            }
        }
    }
    /* Time object */
    else if ( ( p->objlevel == p->callevel ) &&
              ( strcasecmp( name, "Time" ) == 0 ) )
    {
        p->empty = 0;
        if ( ++p->prmlevel == 1 )
        {
            p->bufsize = 0;
            strcpy( p->prm, "Time" );
            p->gps = 0;
            i = 0;
            while ( attributes[ i ] != 0 )
            {
                if ( ( strcasecmp( attributes[ i ], "Type" ) == 0 ) &&
                     ( strcasecmp( attributes[ i + 1 ], "GPS" ) == 0 ) )
                {
                    p->gps = 1;
                }
                i += 2;
            }
        }
    }
}

static void
endelement( xml2cal_alt* p, const char* name )
{
    int           i;
    double        x;
    unsigned long u;

    /* LIGO_LW object */
    if ( ( strcasecmp( name, "LIGO_LW" ) == 0 ) ||
         ( strcasecmp( name, "XSIL" ) == 0 ) )
    {
        if ( p->objlevel-- == p->callevel )
        {
            p->callevel = -1;
            if ( p->empty )
            {
            }
            else if ( p->objtype == 1 )
            {
                if ( p->fCal != NULL )
                {
                    p->fCal( p->caltype, &p->cal, p->fUser, p->errmsg );
                }
                calrelease( &p->cal );
                calinit( &p->cal );
                strcpy( p->errmsg, "" );
                calsetconversion( &p->cal, 1. );
            }
            else if ( p->objtype == 2 )
            {
                if ( p->fCalAuth != NULL )
                {
                    p->fCalAuth( p->user, p->passwd, p->user );
                }
                strcpy( p->user, "" );
                strcpy( p->passwd, "" );
            }
        }
        if ( ( p->objlevel == 0 ) && ( p->empty ) )
        {
            p->finished = 1;
        }
    }
    /* Param object */
    else if ( ( p->objlevel == p->callevel ) &&
              ( ( strcasecmp( name, "Param" ) == 0 ) ||
                ( strcasecmp( name, "Time" ) == 0 ) ) )
    {
        if ( --p->prmlevel == 0 )
        {
            p->buf[ p->bufsize ] = 0;
            eliminateSpaces( p->buf );
            if ( ( strcasecmp( p->prm, "Channel" ) == 0 ) &&
                 ( p->objtype == 1 ) )
            {
                calsetchannel( &p->cal, p->buf );
            }
            else if ( ( strcasecmp( p->prm, "Time" ) == 0 ) &&
                      ( p->objtype == 1 ) && p->gps )
            {
                u = strtoul( p->buf, NULL, 10 );
                calsettime( &p->cal, u );
            }
            else if ( ( strcasecmp( p->prm, "Duration" ) == 0 ) &&
                      ( p->objtype == 1 ) )
            {
                u = strtoul( p->buf, NULL, 10 );
                calsetduration( &p->cal, u );
            }
            else if ( ( strcasecmp( p->prm, "Reference" ) == 0 ) &&
                      ( p->objtype == 1 ) )
            {
                calsetref( &p->cal, p->buf );
            }
            else if ( ( strcasecmp( p->prm, "Unit" ) == 0 ) &&
                      ( p->objtype == 1 ) )
            {
                calsetunit( &p->cal, p->buf );
            }
            /* else if ((strcasecmp (p->prm, "Type") == 0) && 
                    (p->objtype == 1)) {
               i = atoi (p->buf);
               p->cal.fType = i;
            }*/
            else if ( ( strcasecmp( p->prm, "Conversion" ) == 0 ) &&
                      ( p->objtype == 1 ) )
            {
                x = atof( p->buf );
                calsetconversion( &p->cal, x );
            }
            else if ( ( strcasecmp( p->prm, "Offset" ) == 0 ) &&
                      ( p->objtype == 1 ) )
            {
                x = atof( p->buf );
                calsetoffset( &p->cal, x );
            }
            else if ( ( strcasecmp( p->prm, "TimeDelay" ) == 0 ) &&
                      ( p->objtype == 1 ) )
            {
                x = atof( p->buf );
                calsettimedelay( &p->cal, x );
            }
            else if ( ( strcasecmp( p->prm, "TransferFunction" ) == 0 ) &&
                      ( p->objtype == 1 ) )
            {
                if ( ( p->trans != NULL ) && ( p->tindx == p->dim ) )
                {
                    calsettransferfunction( &p->cal, p->trans, p->dim / 3 );
                }
                free( p->trans );
                p->trans = NULL;
            }
            else if ( ( strcasecmp( p->prm, "Gain" ) == 0 ) &&
                      ( p->objtype == 1 ) )
            {
                double       gain;
                int          pnum = 0;
                int          znum = 0;
                const float* pzs = 0;
                float        dummy;
                if ( !calgetpolezeros( &p->cal, &gain, &pnum, &znum, &pzs ) )
                {
                    pzs = &dummy;
                }
                gain = atof( p->buf );
                calsetpolezeros( &p->cal, gain, pnum, znum, pzs );
            }
            else if ( ( strcasecmp( p->prm, "Poles" ) == 0 ) &&
                      ( p->objtype == 1 ) )
            {
                double       gain = 1.0;
                int          pnum = 0;
                int          znum = 0;
                const float* pzs = 0;
                float*       newpzs;
                int          i;
                int          len;
                calgetpolezeros( &p->cal, &gain, &pnum, &znum, &pzs );
                if ( ( p->trans != NULL ) && ( p->tindx == 2 * p->dim ) )
                {
                    pnum = p->dim;
                    len = ( pnum > znum ) ? pnum : znum;
                    newpzs = malloc( 4 * len * sizeof( float ) );
                    if ( newpzs != 0 )
                    {
                        for ( i = 0; i < len; i++ )
                        {
                            newpzs[ 4 * i + 0 ] =
                                ( i < pnum ) ? p->trans[ 2 * i ] : 0;
                            newpzs[ 4 * i + 1 ] =
                                ( i < pnum ) ? p->trans[ 2 * i + 1 ] : 0;
                            newpzs[ 4 * i + 2 ] =
                                ( i < znum ) ? pzs[ 4 * i + 2 ] : 0;
                            newpzs[ 4 * i + 3 ] =
                                ( i < znum ) ? pzs[ 4 * i + 3 ] : 0;
                        }
                        calsetpolezeros( &p->cal, gain, pnum, znum, newpzs );
                        free( newpzs );
                    }
                }
                free( p->trans );
                p->trans = NULL;
            }
            else if ( ( strcasecmp( p->prm, "Zeros" ) == 0 ) &&
                      ( p->objtype == 1 ) )
            {
                double       gain = 1.0;
                int          pnum = 0;
                int          znum = 0;
                const float* pzs = 0;
                float*       newpzs;
                int          i;
                int          len;
                calgetpolezeros( &p->cal, &gain, &pnum, &znum, &pzs );
                if ( ( p->trans != NULL ) && ( p->tindx == 2 * p->dim ) )
                {
                    znum = p->dim;
                    len = ( pnum > znum ) ? pnum : znum;
                    newpzs = malloc( 4 * len * sizeof( float ) );
                    if ( newpzs != 0 )
                    {
                        for ( i = 0; i < len; i++ )
                        {
                            newpzs[ 4 * i + 0 ] =
                                ( i < pnum ) ? pzs[ 4 * i + 0 ] : 0;
                            newpzs[ 4 * i + 1 ] =
                                ( i < pnum ) ? pzs[ 4 * i + 1 ] : 0;
                            newpzs[ 4 * i + 2 ] =
                                ( i < znum ) ? p->trans[ 2 * i ] : 0;
                            newpzs[ 4 * i + 3 ] =
                                ( i < znum ) ? p->trans[ 2 * i + 1 ] : 0;
                        }
                        calsetpolezeros( &p->cal, gain, pnum, znum, newpzs );
                        free( newpzs );
                    }
                }
                free( p->trans );
                p->trans = NULL;
            }
            else if ( ( strcasecmp( p->prm, "Default" ) == 0 ) &&
                      ( p->objtype == 1 ) )
            {
                if ( isalpha( (int)*p->buf ) )
                {
                    calsetdefault( &p->cal,
                                   ( tolower( *p->buf ) == 't' ) ||
                                       ( tolower( *p->buf ) == 'y' ) );
                }
                else
                {
                    i = atoi( p->buf );
                    calsetdefault( &p->cal, i != 0 );
                }
            }
            else if ( ( strcasecmp( p->prm, "PreferredMag" ) == 0 ) &&
                      ( p->objtype == 1 ) )
            {
                i = atoi( p->buf );
                calsetpreferredmag( &p->cal, i );
            }
            else if ( ( strcasecmp( p->prm, "PreferredD" ) == 0 ) &&
                      ( p->objtype == 1 ) )
            {
                i = atoi( p->buf );
                calsetpreferredd( &p->cal, i );
            }
            else if ( ( strcasecmp( p->prm, "Comment" ) == 0 ) &&
                      ( p->objtype == 1 ) )
            {
                calsetcomment( &p->cal, p->buf );
            }
            else if ( ( strcasecmp( p->prm, "Error" ) == 0 ) &&
                      ( p->objtype == 1 ) )
            {
                strncpy( p->errmsg, p->buf, MAXERR - 1 );
            }
            else if ( ( strcasecmp( p->prm, "User" ) == 0 ) &&
                      ( p->objtype == 2 ) )
            {
                strncpy( p->user, p->buf, MAXUSER - 1 );
            }
            else if ( ( strcasecmp( p->prm, "Password" ) == 0 ) &&
                      ( p->objtype == 2 ) )
            {
                strncpy( p->passwd, p->buf, MAXPASSWD - 1 );
            }
        }
    }
}

static void
texthandler( xml2cal_alt* p, const char* text, int len )
{
    int size;
    int type = 0;

    p->empty = 0;
    /* load text into buffer if parameter */
    if ( ( p->objlevel == p->callevel ) && ( p->prmlevel == 1 ) )
    {
        /* read transfer function & poles and zeros */
        if ( p->dim > 0 )
        {
            type = 0;
            if ( strcasecmp( p->prm, "TransferFunction" ) == 0 )
                type = 1;
            if ( strcasecmp( p->prm, "Poles" ) == 0 )
                type = 2;
            if ( strcasecmp( p->prm, "Zeros" ) == 0 )
                type = 3;
        }
        if ( ( p->dim > 0 ) && ( type > 0 ) )
        {
            char*  s;
            char*  prev;
            char*  next;
            double x;

            if ( ( p->tindx != 0 ) && ( p->trans == NULL ) )
            {
                return;
            }
            if ( ( ( type == 1 ) && ( p->tindx >= p->dim ) ) ||
                 ( ( type > 1 ) && ( p->tindx >= 2 * p->dim ) ) )
            {
                return;
            }
            if ( p->trans == NULL )
            {
                p->trans =
                    calloc( ( type == 1 ? 1 : 2 ) * p->dim, sizeof( float ) );
                if ( p->trans == NULL )
                {
                    p->tindx = -1;
                    return;
                }
            }
            s = malloc( len + 1 );
            if ( s == NULL )
            {
                free( p->trans );
                p->trans = NULL;
                p->tindx = -1;
                return;
            }
            memcpy( s, text, len );
            s[ len ] = 0;
            next = s;
            while ( p->tindx < ( type == 1 ? 1 : 2 ) * p->dim )
            {
                prev = next;
                x = strtod( prev, &next );
                if ( next == prev )
                {
                    break;
                }
                p->trans[ p->tindx ] = x;
                p->tindx++;
            }
            free( s );
        }
        /* read data into buffer */
        else
        {
            if ( len + p->bufsize < MAXBUF )
            {
                size = len;
            }
            else
            {
                size = MAXBUF - p->bufsize;
            }
            memcpy( p->buf + p->bufsize, text, size );
            p->bufsize += size;
        }
    }
}

xml2cal_t*
xml2cal_new( void )
{
    xml2cal_alt* p;

    p = malloc( sizeof( xml2cal_alt ) );
    if ( p == NULL )
    {
        return NULL;
    }
    memset( p, 0, sizeof( xml2cal_alt ) );

    p->expat = XML_ParserCreate( NULL );
    if ( p->expat == NULL )
    {
        free( p );
        return NULL;
    }
    XML_SetUserData( p->expat, p );
    XML_SetElementHandler( p->expat,
                           (XML_StartElementHandler)startelement,
                           (XML_EndElementHandler)endelement );
    XML_SetCharacterDataHandler( p->expat,
                                 (XML_CharacterDataHandler)texthandler );

    p->finished = 0;
    p->bufsize = 0;
    p->objlevel = 0;
    p->callevel = -1;
    p->prmlevel = 0;
    calinit( &p->cal );
    return (xml2cal_t*)p;
}

void
xml2cal_free( xml2cal_t* parser )
{
    xml2cal_alt* p;
    char         buf[ 1 ];

    p = (xml2cal_alt*)parser;
    free( p->trans );
    p->trans = NULL;
    calrelease( &p->cal );
    XML_Parse( p->expat, buf, 0, 1 );
    XML_ParserFree( p->expat );
    free( parser );
}

void
xml2cal_set( xml2cal_t*     parser,
             cal_cbfunc     cal,
             calauth_cbfunc auth,
             void*          user )
{
    parser->fCal = cal;
    parser->fCalAuth = auth;
    parser->fUser = user;
}

int
xml2cal( xml2cal_t* parser, const char* buf, int size )
{
    xml2cal_alt* p;

    p = (xml2cal_alt*)parser;
    if ( XML_Parse( p->expat, buf, size, 0 ) == 0 )
    {
        return -1;
    }
    return ( p->finished ) ? 0 : 1;
}

struct readdata_t
{
    int        flag;
    calrec_t** cal;
    int        max;
    int        n;
};
typedef struct readdata_t readdata_t;

void
readcal( int flag, const calrec_t* cal, readdata_t* priv, const char* errmsg )
{
    if ( ( priv->flag != flag ) ||
         ( ( priv->max >= 0 ) && ( priv->n >= priv->max ) ) )
    {
        return;
    }
    /* setup memory */
    if ( *priv->cal == NULL )
    {
        *priv->cal = calnew( 100 );
    }
    if ( *priv->cal == NULL )
    {
        return;
    }
    if ( calsize( *priv->cal ) >= priv->n + 1 )
    {
        calrec_t* newcal = calresize( *priv->cal, priv->n + 101 );
        if ( newcal == NULL )
        {
            return;
        }
        *priv->cal = newcal;
    }
    /* set new record */
    calcpy( ( *priv->cal ) + priv->n, cal );
    priv->n++;
}

int
calread( int flag, calrec_t** cal, int max, const char* file )
{
    xml2cal_t* parser; /* xml parser */
    int        fd; /* file id */
    char*      addr; /* file memory address */
    int        len; /* file length */
    readdata_t priv; /* private data */

    /* setup struct */
    priv.flag = flag;
    priv.cal = cal;
    priv.max = max;
    priv.n = 0;

    /* open file */
    fd = open( file, O_RDONLY );
    if ( fd == -1 )
    {
        return -1;
    }
    /* map file into memory */
    len = lseek( fd, 0, SEEK_END );
    if ( len == -1 )
    {
        close( fd );
        return -2;
    }
    addr = mmap( 0, len, PROT_READ, MAP_PRIVATE, fd, 0 );
    close( fd );
    if ( addr == MAP_FAILED )
    {
        return -2;
    }
    /* setup parser */
    parser = xml2cal_new( );
    if ( parser == NULL )
    {
        munmap( addr, len );
        return -3;
    }
    xml2cal_set( parser, (cal_cbfunc)readcal, NULL, &priv );
    /* parse database */
    xml2cal( parser, addr, len );
    /* done: cleanup */
    xml2cal_free( parser );
    munmap( addr, len );

    return priv.n;
}
