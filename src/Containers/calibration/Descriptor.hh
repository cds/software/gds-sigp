#ifndef _LIGO_CALIBRATIONDESC_H
#define _LIGO_CALIBRATIONDESC_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: Descriptor						*/
/*                                                         		*/
/* Module Description: Calibration descriptor				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 14Mar01  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: doc++						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include "caltype.h"
#include "Unit.hh"
#include "SignalInfo.hh"
#include "Time.hh"
#include <string>

namespace calibration
{

    class Descriptor;
    class Table;

    /** @name Calibration Descriptor
    Descriptors for simple units and calibration information.
   
    @brief Calibration Descriptor
    @author Written March 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

    //@{

    /** Describes the calibration information associated with a plot.
    A one channel quantity describes the data as follows:
    data = coeff_A * channel_A ^ expo_A. A two channel quantity is
    formed as follows:
    data = coeff_A * channel_A ^ expo_A (x) coeff_B * channel_B ^ expo_B
    where (x) is either a + (additive) or a * (multiplicative).
   
    @brief Calibration descriptor
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
    class Descriptor
    {
    public:
#ifndef __CINT__
        typedef ::ECalChnRelation ECalChnRelation;
        typedef ::ECalDomain      ECalDomain;
        typedef ::ECalDensity     ECalDensity;
        typedef ::ECalUnit        ECalUnit;
#endif
    public:
        /// Default constructor
        explicit Descriptor( bool valid = false )
            : fValid( valid ), fSigInfo( 0 )
        {
            Init( );
        }
        /// Constructor
        // Descriptor (const char* graphtype,
        // const char* Achn, const char* Bchn = 0,
        // double bw = 0.0)
        // : fValid (false), fSigInfo (0) {
        // Setup (graphtype, Achn, Bchn, bw, Time (0,0)); }
        /// Constructor
        Descriptor( const Time& time,
                    const char* graphtype,
                    const char* Achn,
                    const char* Bchn = 0,
                    double      bw = 0.0 )
            : fValid( false ), fSigInfo( 0 )
        {
            Setup( graphtype, Achn, Bchn, bw, time );
        }
        /// Similar to copy constructor
        explicit Descriptor( const Descriptor* cald );
        /// Destructor
        virtual ~Descriptor( );
        /// Initializes the descriptor to its default values
        virtual void Init( );
        /// Clone from another descriptor
        virtual void Clone( const Descriptor& desc, Table* caltable = 0 );
        /// Sets the descriptor to a graphtype
        virtual bool Setup( const char* graphtype,
                            const char* Achn,
                            const char* Bchn = 0,
                            double      bw = 0.0,
                            const Time& time = Time( 0, 0 ) );
        /// Is valid?
        virtual bool
        IsValid( ) const
        {
            return fValid;
        }
        /// Set valid
        virtual void
        SetValid( bool valid = true )
        {
            fValid = valid;
        }
        /// Get time
        virtual Time
        GetTime( ) const
        {
            return fTime;
        }
        /// Set channel relation
        virtual void
        SetTime( const Time& time )
        {
            fTime = time;
        }
        /// Get channel relation
        virtual ECalChnRelation
        GetRelation( ) const
        {
            return fChnRelation;
        }
        /// Set channel relation
        virtual void
        SetRelation( ECalChnRelation rel )
        {
            fChnRelation = rel;
        }
        /// Get domain
        virtual ECalDomain
        Domain( ECalUnit unit = kCalUnitX ) const
        {
            return fDomain[ (int)unit % 2 ];
        }
        /// Set domain
        virtual void
        SetDomain( ECalUnit unit, ECalDomain dom )
        {
            fDomain[ (int)unit % 2 ] = dom;
        }
        /// Get channel name
        virtual const char*
        GetChannel( bool b ) const
        {
            return fChannel[ b ? 1 : 0 ].c_str( );
        }
        /// Get channel name
        virtual void
        SetChannel( bool b, const char* name )
        {
            fChannel[ b ? 1 : 0 ] = name ? name : "";
        }
        /// Get coefficient
        virtual double
        GetCoeff( bool b ) const
        {
            return fCoeff[ b ? 1 : 0 ];
        }
        /// Set coefficient
        virtual void
        SetCoeff( bool b, double coeff )
        {
            fCoeff[ b ? 1 : 0 ] = coeff;
        }
        /// Get exponent
        virtual int
        GetExpo( bool b ) const
        {
            return fExpo[ b ? 1 : 0 ];
        }
        /// Set exponent
        virtual void
        SetExpo( bool b, int expo )
        {
            fExpo[ b ? 1 : 0 ] = expo;
        }
        /// Get conjugate
        virtual bool
        GetConj( bool b ) const
        {
            return fConj[ b ? 1 : 0 ];
        }
        /// Set exponent
        virtual void
        SetConj( bool b, bool conj )
        {
            fConj[ b ? 1 : 0 ] = conj;
        }
        /// Get density unit
        virtual ECalDensity
        GetDensity( ECalUnit unit ) const
        {
            return fDensityUnits[ (int)unit % 2 ];
        }
        /// Set density unit
        virtual void
        SetDensity( ECalUnit unit, ECalDensity dens )
        {
            fDensityUnits[ (int)unit % 2 ] = dens;
        }
        /// Get bandwidth
        virtual double
        GetBW( ECalUnit unit ) const
        {
            return fBW[ (int)unit % 2 ];
        }
        /// Set bandwidth
        virtual void
        SetBW( ECalUnit unit, double bw )
        {
            fBW[ (int)unit % 2 ] = bw;
        }
        /// Get preferred magnitude
        virtual int
        GetPreferredMag( ECalUnit unit ) const
        {
            return fPreferredMag[ (int)unit % 2 ];
        }
        /// Set preferred magnitude
        virtual void
        SetPreferredMag( ECalUnit unit, int mag )
        {
            fPreferredMag[ (int)unit % 2 ] = mag;
        }
        /// Get list of units
        virtual UnitList&
        Units( ECalUnit unit )
        {
            return fUnits[ (int)unit % 2 ];
        }
        virtual const UnitList&
        Units( ECalUnit unit ) const
        {
            return fUnits[ (int)unit % 2 ];
        }

        /** Find the specified unit; the names 'none' and 'default' are
          always recognized */
        virtual const Unit* FindUnit( ECalUnit unit, const char* name ) const;
        /** copies all unit names into list as TNamed object */
        //virtual void GetUnitNames (ECalUnit unit, TList* list) const;

        /// Set info (used by the calibration table)
        virtual void
        SetInfo( SignalInfo* info )
        {
            fSigInfo.Set( info );
        }
        /// Get info (used by the calibration table)
        virtual SignalInfo*
        GetInfo( ) const
        {
            return fSigInfo.Get( );
        }

    public:
        /// Info pointer (used as a smart pointer)
        class infoptr
        {
        public:
            /// Pointer type
            typedef SignalInfo* Pointer;

        protected:
            /// Pointer
            mutable Pointer fInfo;

        public:
            /// Constructor
            explicit infoptr( Pointer p = 0 ) : fInfo( p )
            {
            }
            /// Copy constructor
            infoptr( const infoptr& info ) : fInfo( info.fInfo )
            {
                info.fInfo = 0;
            }
            /// Destructor
            ~infoptr( )
            {
                Set( 0 );
            }
            /// Assignment operator
            infoptr& operator=( const infoptr& info );
            /// Get function
            Pointer
            Get( ) const
            {
                return fInfo;
            }
            /// Set function
            void Set( Pointer p );
        };

    private:
        /// Valid calibration information?
        bool fValid;
        /// Time when calibration is needed
        Time fTime;
        /// Channel relation: single or two; add, mul or against
        ECalChnRelation fChnRelation;
        /// A/B channels
        std::string fChannel[ 2 ];
        /// Coefficient for A/B channels
        double fCoeff[ 2 ];
        /// Exponent for A/B channels
        int fExpo[ 2 ];
        /// Conjugate? for A/B channels
        bool fConj[ 2 ];
        /// Domain of X/Y axes
        ECalDomain fDomain[ 2 ];
        /// X/Y axis density units
        ECalDensity fDensityUnits[ 2 ];
        /// X/Y axis bandwidth
        double fBW[ 2 ];
        /// X/Y axis preferred magnitude
        int fPreferredMag[ 2 ];
        /// Calibration signal info (used by the calibration table)
        infoptr fSigInfo;
        /// List of X/Y units
        UnitList fUnits[ 2 ];
    };

    //@}

} // namespace calibration

#endif // _LIGO_CALIBRATIONDESC_H
