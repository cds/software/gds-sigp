#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "calutil.h"

static int
strpos( const char* s, char c )
{
    int pos;
    for ( pos = 0; s[ pos ] != 0; pos++ )
    {
        if ( s[ pos ] == c )
        {
            return pos;
        }
    }
    return -1;
}

static char*
strend( const char* s )
{
    return (char*)( s + strlen( s ) );
}

static int
xmlescape( const char* s, char* buf, int max )
{
    int i = 0;
    for ( ; *s; ++s )
    {
        if ( *s == '<' )
        {
            if ( i >= max - 4 )
            {
                return -1;
            }
            strcpy( buf + i, "&lt;" );
            i += 4;
        }
        else if ( *s == '>' )
        {
            if ( i >= max - 4 )
            {
                return -1;
            }
            strcpy( buf + i, "&gt;" );
            i += 4;
        }
        else if ( *s == '&' )
        {
            if ( i >= max - 5 )
            {
                return -1;
            }
            strcpy( buf + i, "&amp;" );
            i += 5;
        }
        else if ( *s == '"' )
        {
            if ( i >= max - 6 )
            {
                return -1;
            }
            strcpy( buf + i, "&quot;" );
            i += 6;
        }
        else if ( *s == '\'' )
        {
            if ( i >= max - 6 )
            {
                return -1;
            }
            strcpy( buf + i, "&apos;" );
            i += 6;
        }
        else
        {
            if ( i >= max - 1 )
            {
                return -1;
            }
            buf[ i++ ] = *s;
        }
    }
    buf[ i ] = 0;
    return i;
}

static int
strwrite( char*       buf,
          int         max,
          const char* prefix,
          const char* data,
          const char* postfix )
{
    int   len;
    char* p = buf;
    if ( prefix )
    {
        len = strlen( prefix );
        if ( len >= max )
        {
            return -1;
        }
        strcpy( p, prefix );
        p += len;
        max -= len;
    }
    if ( data )
    {
        len = xmlescape( data, p, max );
        if ( len < 0 )
        {
            return -1;
        }
        p += len;
        max -= len;
    }
    if ( postfix )
    {
        len = strlen( postfix );
        if ( len >= max )
        {
            return -1;
        }
        strcpy( p, postfix );
        p += len;
        max -= len;
    }
    return p - buf;
}

int
cal2xml( int             flag,
         const calrec_t* cal,
         char*           buf,
         int             size,
         int             index,
         const char*     errmsg )
{
    int  pos = 0; /* buffer position */
    char p[ 256 ]; /* temp. buffer */
    int  len; /* length */
    int  i; /* index */
    int  j; /* index */

    /* end-of-file mark */
    if ( cal == 0 )
    {
        sprintf( p, "<LIGO_LW/>\n" );
        if ( pos + strlen( p ) < size )
        {
            strcpy( buf + pos, p );
            pos += strlen( p );
        }
        else
        {
            return -1;
        }
        return 0;
    }
    /* calibration head */
    if ( index < 0 )
    {
        sprintf( p, "  <LIGO_LW Name=\"Calibration\"" );
    }
    else
    {
        sprintf( p, "  <LIGO_LW Name=\"Calibration[%i]\"", index );
    }
    switch ( flag )
    {
    case CALADD:
        sprintf( strend( p ), " Type=\"Add\"" );
        break;
    case CALDELETE:
        sprintf( strend( p ), " Type=\"Delete\"" );
        break;
    case CALQUERY:
        sprintf( strend( p ), " Type=\"Query\"" );
        break;
    case CALERROR:
        sprintf( strend( p ), " Type=\"Error\"" );
        break;
    }
    sprintf( strend( p ), ">\n" );
    if ( pos + strlen( p ) < size )
    {
        strcpy( buf + pos, p );
        pos += strlen( p );
    }
    else
    {
        return -1;
    }
    /* error message */
    if ( ( errmsg != 0 ) && ( flag == CALERROR ) )
    {
        if ( pos + strlen( errmsg ) + 32 > size )
        {
            return -1;
        }
        sprintf( buf + pos,
                 "    <Param Name=\"Error\" Type=\"string\">%s</Param>\n",
                 errmsg );
        pos += strlen( buf + pos );
    }
    /* Channel name */
    if ( ( flag != CALQUERY ) && ( strpos( calgetchannel( cal ), '*' ) != -1 ) )
    {
        return -2;
    }
    if ( strpos( calgetchannel( cal ), '*' ) != 0 )
    {
        len = strwrite( buf + pos,
                        size - pos,
                        "    <Param Name=\"Channel\" Type=\"string\">",
                        calgetchannel( cal ),
                        "</Param>\n" );
        if ( len < 0 )
        {
            return -1;
        }
        pos += len;
        /*sprintf (p, "    <Param Name=\"Channel\" Type=\"string\">%s</Param>\n",
                 calgetchannel (cal));
         if (pos + strlen (p) < size) {
            strcpy (buf + pos, p);
            pos += strlen (p);
         }
         else {
            return -1;
         }*/
    }
    /* Time */
    if ( calgettime( cal ) != 0 )
    {
        sprintf(
            p, "    <Time Type=\"GPS\">%lu.0</Time>\n", calgettime( cal ) );
        if ( pos + strlen( p ) < size )
        {
            strcpy( buf + pos, p );
            pos += strlen( p );
        }
        else
        {
            return -1;
        }
    }
    /* Duration */
    if ( ( ( flag == CALNORMAL ) || ( ( flag == CALQUERY ) ) ) &&
         ( calgetduration( cal ) != 0 ) )
    {
        sprintf( p,
                 "    <Param Name=\"Duration\" Type=\"int\">%lu</Param>\n",
                 calgetduration( cal ) );
        if ( pos + strlen( p ) < size )
        {
            strcpy( buf + pos, p );
            pos += strlen( p );
        }
        else
        {
            return -1;
        }
    }
    /* Reference point */
    if ( ( flag != CALQUERY ) && ( strpos( calgetref( cal ), '*' ) != -1 ) )
    {
        return -2;
    }
    if ( strpos( calgetref( cal ), '*' ) != 0 )
    {
        len = strwrite( buf + pos,
                        size - pos,
                        "    <Param Name=\"Reference\" Type=\"string\">",
                        calgetref( cal ),
                        "</Param>\n" );
        if ( len < 0 )
        {
            return -1;
        }
        pos += len;
        /*sprintf (p, "    <Param Name=\"Reference\" Type=\"string\">%s</Param>\n",
                 calgetref (cal));
         if (pos + strlen (p) < size) {
            strcpy (buf + pos, p);
            pos += strlen (p);
         }
         else {
            return -1;
         }*/
    }
    /* unit string */
    if ( ( flag != CALQUERY ) && ( strpos( calgetunit( cal ), '*' ) != -1 ) )
    {
        return -2;
    }
    if ( strpos( calgetunit( cal ), '*' ) != 0 )
    {
        len = strwrite( buf + pos,
                        size - pos,
                        "    <Param Name=\"Unit\" Type=\"string\">",
                        calgetunit( cal ),
                        "</Param>\n" );
        if ( len < 0 )
        {
            return -1;
        }
        pos += len;
        /*sprintf (p, "    <Param Name=\"Unit\" Type=\"string\">%s</Param>\n",
                 calgetunit (cal));
         if (pos + strlen (p) < size) {
            strcpy (buf + pos, p);
            pos += strlen (p);
         }
         else {
            return -1;
         }*/
    }
    /* only continue for normal, add and error record */
    if ( ( flag != CALQUERY ) && ( flag != CALDELETE ) )
    {
        /* Type
         sprintf (p, "    <Param Name=\"Type\">%i</Param>\n",
                 calgettype (cal));
         if (pos + strlen (p) < size) {
            strcpy (buf + pos, p);
            pos += strlen (p);
         }
         else {
            return -1;
         }*/
        /* Conversion */
        if ( ( calgettype( cal ) & CALAMPLITUDE ) != 0 )
        {
            sprintf(
                p,
                "    <Param Name=\"Conversion\" Type=\"double\">%g</Param>\n",
                calgetconversion( cal ) );
            if ( pos + strlen( p ) < size )
            {
                strcpy( buf + pos, p );
                pos += strlen( p );
            }
            else
            {
                return -1;
            }
        }
        /* Offset */
        if ( ( calgettype( cal ) & CALOFFSET ) != 0 )
        {
            sprintf( p,
                     "    <Param Name=\"Offset\" Type=\"double\">%g</Param>\n",
                     calgetoffset( cal ) );
            if ( pos + strlen( p ) < size )
            {
                strcpy( buf + pos, p );
                pos += strlen( p );
            }
            else
            {
                return -1;
            }
        }
        /* Time delay */
        if ( ( calgettype( cal ) & CALTIMEDELAY ) != 0 )
        {
            sprintf(
                p,
                "    <Param Name=\"TimeDelay\" Type=\"double\">%g</Param>\n",
                calgettimedelay( cal ) );
            if ( pos + strlen( p ) < size )
            {
                strcpy( buf + pos, p );
                pos += strlen( p );
            }
            else
            {
                return -1;
            }
        }
        /* Transfer function */
        if ( ( calgettype( cal ) & CALTRANSFERFUNCTION ) != 0 )
        {
            const float* trans;
            len = calgettransferfunction( cal, &trans );
            if ( trans == NULL )
            {
                len = 0;
            }
            sprintf( p,
                     "    <Param Name=\"TransferFunction\" Type=\"double\" "
                     "Dim=\"%i\">\n",
                     3 * len );
            if ( pos + strlen( p ) < size )
            {
                strcpy( buf + pos, p );
                pos += strlen( p );
            }
            else
            {
                return -1;
            }
            for ( i = 0; i < len; i++ )
            {
                sprintf( p,
                         "  %g %g %g\n",
                         trans[ 3 * i ],
                         trans[ 3 * i + 1 ],
                         trans[ 3 * i + 2 ] );
                if ( pos + strlen( p ) < size )
                {
                    strcpy( buf + pos, p );
                    pos += strlen( p );
                }
                else
                {
                    return -1;
                }
            }
            sprintf( p, "  </Param>\n" );
            if ( pos + strlen( p ) < size )
            {
                strcpy( buf + pos, p );
                pos += strlen( p );
            }
            else
            {
                return -1;
            }
        }
        /* Poles and zeros */
        if ( ( calgettype( cal ) & CALPOLEZERO ) != 0 )
        {
            int          pnum;
            int          znum;
            double       gain;
            const float* pzs;
            if ( calgetpolezeros( cal, &gain, &pnum, &znum, &pzs ) )
            {
                sprintf(
                    p,
                    "    <Param Name=\"Gain\" Type=\"double\">%g</Param>\n",
                    gain );
                if ( pos + strlen( p ) < size )
                {
                    strcpy( buf + pos, p );
                    pos += strlen( p );
                }
                else
                {
                    return -1;
                }
                for ( i = 0; i < 2; i++ )
                {
                    sprintf( p,
                             "    <Param Name=\"%s\" Type=\"doubleComplex\" "
                             "Dim=\"%i\">\n",
                             i == 0 ? "Poles" : "Zeros",
                             ( i == 0 ? pnum : znum ) );
                    if ( pos + strlen( p ) < size )
                    {
                        strcpy( buf + pos, p );
                        pos += strlen( p );
                    }
                    else
                    {
                        return -1;
                    }
                    for ( j = 0; j < ( i == 0 ? pnum : znum ); j++ )
                    {
                        sprintf( p,
                                 "  %g %g\n",
                                 pzs[ 4 * j + 2 * i ],
                                 pzs[ 4 * j + 2 * i + 1 ] );
                        if ( pos + strlen( p ) < size )
                        {
                            strcpy( buf + pos, p );
                            pos += strlen( p );
                        }
                        else
                        {
                            return -1;
                        }
                    }
                    sprintf( p, "  </Param>\n" );
                    if ( pos + strlen( p ) < size )
                    {
                        strcpy( buf + pos, p );
                        pos += strlen( p );
                    }
                    else
                    {
                        return -1;
                    }
                }
            }
        }
        /* Default */
        if ( calgetdefault( cal ) )
        {
            sprintf(
                p, "    <Param Name=\"Default\" Type=\"boolean\">1</Param>\n" );
            if ( pos + strlen( p ) < size )
            {
                strcpy( buf + pos, p );
                pos += strlen( p );
            }
            else
            {
                return -1;
            }
        }
        /* Preferred magnitude */
        if ( calgetpreferredmag( cal ) != 0 )
        {
            sprintf( p,
                     "    <Param Name=\"PreferredMag\" Type=\"int\">%i"
                     "</Param>\n",
                     calgetpreferredmag( cal ) );
            if ( pos + strlen( p ) < size )
            {
                strcpy( buf + pos, p );
                pos += strlen( p );
            }
            else
            {
                return -1;
            }
        }
        /* Preferred derivative */
        if ( calgetpreferredd( cal ) != 0 )
        {
            sprintf( p,
                     "    <Param Name=\"PreferredD\" Type=\"int\">%i"
                     "</Param>\n",
                     calgetpreferredd( cal ) );
            if ( pos + strlen( p ) < size )
            {
                strcpy( buf + pos, p );
                pos += strlen( p );
            }
            else
            {
                return -1;
            }
        }
        /* Comment */
        if ( calgetcomment( cal ) != NULL )
        {
            len = strwrite( buf + pos,
                            size - pos,
                            "    <Param Name=\"Comment\" Type=\"string\">",
                            calgetcomment( cal ),
                            "</Param>\n" );
            if ( len < 0 )
            {
                return -1;
            }
            pos += len;
            /*if (pos + strlen (calgetcomment (cal)) + 34 > size) {
               return -1;
            }
            sprintf (buf + pos, 
                    "    <Param Name=\"Comment\" Type=\"string\">%s</Param>\n",
                    calgetcomment (cal));
            pos += strlen (buf + pos);*/
        }
    }

    /* calibration record trailer */
    sprintf( p, "  </LIGO_LW>\n" );
    if ( pos + strlen( p ) < size )
    {
        strcpy( buf + pos, p );
        pos += strlen( p );
    }
    else
    {
        return -1;
    }
    return pos;
}

int
auth2xml( const char* username, const char* passwd, char* buf, int size )
{
    int  pos = 0; /* buffer position */
    char p[ 256 ]; /* temp. buffer */

    /* authorization head */
    sprintf( p, "  <LIGO_LW Name=\"Authorization\">\n" );
    if ( pos + strlen( p ) < size )
    {
        strcpy( buf + pos, p );
        pos += strlen( p );
    }
    else
    {
        return -1;
    }
    /* user */
    if ( username != NULL )
    {
        if ( pos + strlen( username ) + 31 > size )
        {
            return -1;
        }
        sprintf( buf + pos, "    <Param Name=\"User\">%s</Param>\n", username );
        pos += strlen( buf + pos );
    }
    /* password */
    if ( passwd != NULL )
    {
        if ( pos + strlen( passwd ) + 35 > size )
        {
            return -1;
        }
        sprintf(
            buf + pos, "    <Param Name=\"Password\">%s</Param>\n", passwd );
        pos += strlen( buf + pos );
    }
    /* authorization trailer */
    sprintf( p, "  </LIGO_LW>\n" );
    if ( pos + strlen( p ) < size )
    {
        strcpy( buf + pos, p );
        pos += strlen( p );
    }
    else
    {
        return -1;
    }
    return pos;
}
