#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "calutil.h"

/* Numerical recipes */
static void
hunt( const float* xx, int n, float x, int* jlo, int stride )
{
    int jm, jhi, inc;
    int ascnd;

    ascnd = ( xx[ stride * ( n - 1 ) ] >= xx[ 0 ] );
    /* check if initial guess is useful */
    if ( ( *jlo < 0 ) || ( *jlo >= n ) )
    {
        *jlo = -1;
        jhi = n;
    }
    /* hunt phase */
    else
    {
        inc = 1;
        if ( ( x > xx[ stride * ( *jlo ) ] ) == ascnd )
        { /* hunt up */
            if ( *jlo == n - 1 )
            {
                return;
            }
            jhi = ( *jlo ) + 1;
            while ( ( x >= xx[ stride * jhi ] ) == ascnd )
            {
                *jlo = jhi;
                inc += inc;
                jhi = ( *jlo ) + inc;
                if ( jhi >= n )
                {
                    jhi = n;
                    break;
                }
            }
        }
        else
        { /* hunt down */
            if ( *jlo == 0 )
            {
                *jlo = -1;
                return;
            }
            jhi = ( *jlo )--;
            while ( ( x < xx[ stride * ( *jlo ) ] ) == ascnd )
            {
                jhi = ( *jlo );
                inc <<= 1;
                if ( inc > jhi )
                {
                    *jlo = -1;
                    break;
                }
                else
                {
                    *jlo = jhi - inc;
                }
            }
        }
    }
    /* bisection phase */
    while ( jhi - ( *jlo ) != 1 )
    {
        jm = ( jhi + ( *jlo ) ) >> 1;
        if ( ( x >= xx[ stride * jm ] ) == ascnd )
        {
            *jlo = jm;
        }
        else
        {
            jhi = jm;
        }
    }
    if ( x == xx[ stride * ( n - 1 ) ] )
        *jlo = n - 2;
    if ( x == xx[ 0 ] )
        *jlo = 0;
}

static int hunt_guess = -1;

/* linear interpolation */
static double
lin_ipol( double x, double x1, double x2, double y1, double y2 )
{
    double dx;

    dx = x2 - x1;
    if ( fabs( dx ) > 0 )
    {
        return y1 + ( x - x1 ) / dx * ( y2 - y1 );
    }
    else
    {
        return ( y1 + y2 ) / 2;
    }
}

/* cubic interpolation */
static double
cubic_ipol( double x,
            double x1,
            double x2,
            double y1,
            double y2,
            double y1p,
            double y2p )
{
    double dx, t;
    double g0, g1, h0, h1;

    dx = x2 - x1;
    if ( fabs( dx ) == 0 )
    {
        return ( y1 + y2 ) / 2.0;
    }
    t = ( x - x1 ) / dx;
    g0 = 0.5 - 1.5 * ( t - 0.5 ) + 2 * ( t - 0.5 ) * ( t - 0.5 ) * ( t - 0.5 );
    g1 = 1 - g0;
    h0 = t * ( t - 1.0 ) * ( t - 1.0 );
    h1 = t * t * ( t - 1.0 );
    return y1 * g0 + y2 * g1 + y1p * dx * h0 + y2p * dx * h1;
}

/* akima derivative */
static double
akima_D( double d0, double d1, double d2, double d3 )
{
    double w1, w2;

    w1 = fabs( d3 - d2 );
    w2 = fabs( d1 - d0 );
    if ( fabs( d1 - d2 ) < 1E-10 )
    {
        return d1;
    }
    else if ( ( w1 < 1E-10 ) && ( w2 >= 1E-10 ) )
    {
        return d2;
    }
    else if ( ( w1 >= 1E-10 ) && ( w2 < 1E-10 ) )
    {
        return d1;
    }
    else if ( ( w1 < 1E-10 ) && ( w2 < 1E-10 ) )
    {
        return ( d1 + d2 ) / 2.0;
    }
    else
    {
        return ( w1 * d1 + w2 * d2 ) / ( w1 + w2 );
    }
}

/* akima interpolation */
static int
akima( double       f,
       const float* tf,
       int          n,
       int          i,
       double*      cmag,
       double*      cphase,
       int          uselog )
{
    int    k, j;
    double d[ 6 ];
    double e[ 6 ];
    double x1, x2, dx;
    double d1, d2, d1p, d2p;
    double e1, e2, e1p, e2p;

    if ( ( i < 0 ) || ( i >= n - 1 ) || ( n < 4 ) )
    {
        return 0;
    }
    if ( uselog && ( f <= 0 ) )
    {
        return 0;
    }
    /* compute slopes between points */
    if ( uselog )
        f = log( f );
    for ( j = 0; j < 5; ++j )
    {
        k = i + j - 2;
        if ( ( k < 0 ) || ( k + 1 >= n ) )
        {
            continue;
        }
        if ( ( tf[ 3 * k ] <= 0 ) || ( tf[ 3 * ( k + 1 ) ] <= 0 ) ||
             ( tf[ 3 * k + 1 ] <= 0 ) || ( tf[ 3 * ( k + 1 ) + 1 ] <= 0 ) )
        {
            return 0;
        }
        dx = uselog ? log( tf[ 3 * ( k + 1 ) ] ) - log( tf[ 3 * k ] )
                    : tf[ 3 * ( k + 1 ) ] - tf[ 3 * k ];
        if ( fabs( dx ) == 0 )
        {
            return 0;
        }
        d[ j ] = uselog
            ? ( log( tf[ 3 * ( k + 1 ) + 1 ] ) - log( tf[ 3 * k + 1 ] ) ) / dx
            : ( tf[ 3 * ( k + 1 ) + 1 ] - tf[ 3 * k + 1 ] ) / dx;
        e[ j ] = ( tf[ 3 * ( k + 1 ) + 2 ] - tf[ 3 * k + 2 ] ) / dx;
    }
    if ( i == 0 )
    {
        d[ 1 ] = 2 * d[ 2 ] - d[ 3 ];
        e[ 1 ] = 2 * e[ 2 ] - e[ 3 ];
    }
    if ( i <= 1 )
    {
        d[ 0 ] = 2 * d[ 1 ] - d[ 2 ];
        e[ 0 ] = 2 * e[ 1 ] - e[ 2 ];
    }
    if ( i == n - 2 )
    {
        d[ 4 ] = 2 * d[ 3 ] - d[ 2 ];
        e[ 4 ] = 2 * e[ 3 ] - e[ 2 ];
    }
    if ( i >= n - 3 )
    {
        d[ 5 ] = 2 * d[ 4 ] - d[ 3 ];
        e[ 5 ] = 2 * e[ 4 ] - e[ 3 ];
    }
    /* compute 1st derivatives */
    d1p = akima_D( d[ 0 ], d[ 1 ], d[ 2 ], d[ 3 ] );
    e1p = akima_D( e[ 0 ], e[ 1 ], e[ 2 ], e[ 3 ] );
    d2p = akima_D( d[ 1 ], d[ 2 ], d[ 3 ], d[ 4 ] );
    e2p = akima_D( e[ 1 ], e[ 2 ], e[ 3 ], e[ 4 ] );
    /* compute x points */
    x1 = uselog ? log( tf[ 3 * i ] ) : tf[ 3 * i ];
    x2 = uselog ? log( tf[ 3 * ( i + 1 ) ] ) : tf[ 3 * ( i + 1 ) ];
    /* compute data points */
    d1 = uselog ? log( tf[ 3 * i + 1 ] ) : tf[ 3 * i + 1 ];
    e1 = tf[ 3 * i + 2 ];
    d2 = uselog ? log( tf[ 3 * ( i + 1 ) + 1 ] ) : tf[ 3 * ( i + 1 ) + 1 ];
    e2 = tf[ 3 * ( i + 1 ) + 2 ];
    /* interpolate */
    *cmag = cubic_ipol( f, x1, x2, d1, d2, d1p, d2p );
    *cphase = cubic_ipol( f, x1, x2, e1, e2, e1p, e2p );
    if ( uselog )
        *cmag = exp( *cmag );
    return 1;
}

int
tcal_simple(
    int flag, const calrec_t* cal, const float* value, float* phys, int len )
{
    double ofs; /* offset */
    double c; /* conversion factor */
    int    i; /* index */

    /* check conversion */
    if ( ( cal->fType & CALAMPLITUDE ) == 0 )
    {
        return -1;
    }
    c = cal->fConversion;
    /* get offset */
    if ( ( cal->fType & CALOFFSET ) == 0 )
    {
        ofs = 0.;
    }
    else
    {
        ofs = cal->fOffset;
    }
    /* make calibration transformation */
    for ( i = 0; i < len; i++ )
    {
        phys[ i ] = c * ( value[ i ] - ofs );
    }
    return 0;
}

/* linear only */
void
cal_interpolate_cmplx(
    int flag, double f, const float* tf, int len, double* cmag, double* cphase )
{
    int pos; /* point below f */

    *cmag = *cphase = 0.;
    /* determine position in transfer function */
    pos = hunt_guess; /* this is MT safe because guess is just that */
    /*i = pos;*/
    hunt( tf, len, f, &pos, 3 );
    hunt_guess = pos;
    /*printf ("hunt for %g / guess was %i / found at %i\n", f, i, pos);*/

    /* before first point */
    if ( pos == -1 )
    {
        *cmag = tf[ 1 ];
        *cphase = tf[ 2 ];
        return;
    }
    /* after last point */
    if ( pos == len - 1 )
    {
        *cmag = tf[ 3 * pos + 1 ];
        *cphase = tf[ 3 * pos + 2 ];
        return;
    }
    /* try akima first */
    if ( flag >= 1 )
    {
        /* akmia log interpolation */
        if ( akima( f, tf, len, pos, cmag, cphase, 1 ) )
        {
            return;
        }
        /* akmia linear interpolation */
        if ( akima( f, tf, len, pos, cmag, cphase, 0 ) )
        {
            return;
        }
    }
    /* otherwise straight line interpolation */

    /* linear or linear in log coords interpolation? */
    if ( ( tf[ 3 * pos ] > 0 ) && ( tf[ 3 * ( pos + 1 ) ] > 0 ) &&
         ( tf[ 3 * pos + 1 ] > 0 ) && ( tf[ 3 * ( pos + 1 ) + 1 ] > 0 ) )
    {
        *cmag = lin_ipol( log( f ),
                          log( tf[ 3 * pos ] ),
                          log( tf[ 3 * ( pos + 1 ) ] ),
                          log( tf[ 3 * pos + 1 ] ),
                          log( tf[ 3 * ( pos + 1 ) + 1 ] ) );
        *cphase = lin_ipol( log( f ),
                            log( tf[ 3 * pos ] ),
                            log( tf[ 3 * ( pos + 1 ) ] ),
                            tf[ 3 * pos + 2 ],
                            tf[ 3 * ( pos + 1 ) + 2 ] );
        *cmag = exp( *cmag );
    }
    else
    {
        *cmag = lin_ipol( f,
                          tf[ 3 * pos ],
                          tf[ 3 * ( pos + 1 ) ],
                          tf[ 3 * pos + 1 ],
                          tf[ 3 * ( pos + 1 ) + 1 ] );
        *cphase = lin_ipol( f,
                            tf[ 3 * pos ],
                            tf[ 3 * ( pos + 1 ) ],
                            tf[ 3 * pos + 2 ],
                            tf[ 3 * ( pos + 1 ) + 2 ] );
    }
}

int
fcal( int             flag,
      const calrec_t* cal,
      const float*    f,
      const float*    value,
      float*          phys,
      int             len,
      int             complex,
      double          bw )
{
    double cre; /* conversion factor real */
    double cim; /* conversion factor imag */
    double r; /* temp */
    int    i; /* index */

    /* check if transfer function */
    if ( ( cal->fType & CALTRANSFERFUNCTION ) != 0 )
    {
        for ( i = 0; i < len; i++ )
        {
            cal_interpolate_cmplx( flag,
                                   f[ i ],
                                   cal->fTransferFunction,
                                   cal->fTransferFunctionLen,
                                   &cre,
                                   &cim );
            if ( complex )
            {
                /* make sure it works inline */
                r = cre * value[ 2 * i ] - cim * value[ 2 * i + 1 ];
                phys[ 2 * i + 1 ] =
                    cre * value[ 2 * i + 1 ] + cim * value[ 2 * i ];
                phys[ 2 * i ] = r;
            }
            else
            {
                cre = sqrt( cre * cre + cim * cim );
                phys[ i ] = cre * value[ i ];
            }
        }
        return 0;
    }
    /* check if simple conversion */
    else if ( ( cal->fType & CALAMPLITUDE ) != 0 )
    {
        cre = cal->fConversion;
        for ( i = 0; i < ( complex ? 2 * len : len ); i++ )
        {
            phys[ i ] = cre * value[ i ];
        }
        return 0;
    }
    else
    {
        return -1;
    }
}

int
fcal2( int             flag,
       const calrec_t* cal,
       double          f0,
       double          df,
       const float*    value,
       float*          phys,
       int             len,
       int             complex,
       double          bw )
{
    double cre; /* conversion factor real */
    double cim; /* conversion factor imag */
    double r; /* temp */
    int    i; /* index */

    /* check if transfer function */
    if ( ( cal->fType & CALTRANSFERFUNCTION ) != 0 )
    {
        for ( i = 0; i < len; i++ )
        {
            cal_interpolate_cmplx( flag,
                                   f0 + i * df,
                                   cal->fTransferFunction,
                                   cal->fTransferFunctionLen,
                                   &cre,
                                   &cim );
            if ( complex )
            {
                /* make sure it works inline */
                r = cre * value[ 2 * i ] - cim * value[ 2 * i + 1 ];
                phys[ 2 * i + 1 ] =
                    cre * value[ 2 * i + 1 ] + cim * value[ 2 * i ];
                phys[ 2 * i ] = r;
            }
            else
            {
                cre = sqrt( cre * cre + cim * cim );
                phys[ i ] = cre * value[ i ];
            }
        }
        return 0;
    }
    /* check if simple conversion */
    else if ( ( cal->fType & CALAMPLITUDE ) != 0 )
    {
        cre = cal->fConversion;
        for ( i = 0; i < ( complex ? 2 * len : len ); i++ )
        {
            phys[ i ] = cre * value[ i ];
        }
        return 0;
    }
    else
    {
        return -1;
    }
}
