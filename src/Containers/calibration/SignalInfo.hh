#ifndef _LIGO_CALSIGNALINFO_H
#define _LIGO_CALSIGNALINFO_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: SignalInfo						*/
/*                                                         		*/
/* Module Description: SignalInfo signal information 			*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 14Mar01  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: doc++						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include "caltype.h"

namespace calibration
{

    /** A container for a calibration signal information.
   
    @brief SignalInfo container
    @author Written March 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/
    class SignalInfo : private calsignalinfo_t
    {
    public:
        /// Default constructor
        SignalInfo( );
        /// Constructor from calsignalinfo_t
        SignalInfo( const calsignalinfo_t& cal );
        /// Conversion operator to const calsignalinfo_t*
        operator const calsignalinfo_t*( ) const
        {
            return this;
        }
        /// Conversion operator to calsignalinfo_t*
        operator calsignalinfo_t*( )
        {
            return this;
        }

        /** Set the channel relation.
       @brief Set channel relation
       @param rel channel relation
    *********************************************************************/
        void
        SetChnRelation( ECalChnRelation rel )
        {
            fChnRelation = rel;
        }
        /** Get the channel relation.
       @brief Get channel relation
       @return channel relation
    *********************************************************************/
        ECalChnRelation
        GetChnRelation( ) const
        {
            return fChnRelation;
        }

        /** Set the coefficient.
       @brief Set coefficient
       @param i Channel index (0 - A, 1 - B)
       @param coeff Coefficient
    *********************************************************************/
        void
        SetCoeff( int i, double coeff )
        {
            if ( ( i >= 0 ) && ( i <= 2 ) )
                fCoeff[ i ] = coeff;
        }
        /** Get the coefficient.
       @brief Get coefficient
       @param i Channel index (0 - A, 1 - B)
       @return Coefficient
    *********************************************************************/
        double
        GetCoeff( int i ) const
        {
            if ( ( i >= 0 ) && ( i <= 2 ) )
                return fCoeff[ i ];
            else
                return 0;
        }
        /** Set the exponent.
       @brief Set exponent
       @param i Channel index (0 - A, 1 - B)
       @param expo Exponent
    *********************************************************************/
        void
        SetExpo( int i, int expo )
        {
            if ( ( i >= 0 ) && ( i <= 2 ) )
                fExpo[ i ] = expo;
        }
        /** Get the exponent.
       @brief Get exponent
       @param i Channel index (0 - A, 1 - B)
       @return Exponent
    *********************************************************************/
        int
        GetExpo( int i ) const
        {
            if ( ( i >= 0 ) && ( i <= 2 ) )
                return fExpo[ i ];
            else
                return 0;
        }
        /** Set complex conjugate.
       @brief Set complex conjugate
       @param i Channel index (0 - A, 1 - B)
       @param conj Complex conjugate
    *********************************************************************/
        void
        SetConj( int i, bool conj )
        {
            if ( ( i >= 0 ) && ( i <= 2 ) )
                fConj[ i ] = conj;
        }
        /** Get complex conjugate.
       @brief Get complex conjugate
       @param i Channel index (0 - A, 1 - B)
       @return Complex conjugate
    *********************************************************************/
        bool
        GetConf( int i ) const
        {
            if ( ( i >= 0 ) && ( i <= 2 ) )
                return fConj[ i ] != 0;
            else
                return false;
        }
        /** Set the domain.
       @brief Set domain
       @param domain Domain
    *********************************************************************/
        void
        SetDomain( ECalDomain domain )
        {
            fDomain = domain;
        }
        /** Get the domain.
       @brief Get domain
       @return Domain
    *********************************************************************/
        ECalDomain
        GetDomain( ) const
        {
            return fDomain;
        }
        /** Set the density units.
       @brief Set density units
       @param dens Density units
    *********************************************************************/
        void
        SetDensityUnits( ECalDensity dens )
        {
            fDensityUnits = dens;
        }
        /** Get the density units.
       @brief Get density units
       @return Density units
    *********************************************************************/
        ECalDensity
        GetDensityUnits( ) const
        {
            return fDensityUnits;
        }
        /** Set the bandwidth.
       @brief Set bandwidth
       @param bw Bandwidth
    *********************************************************************/
        void
        SetBandwidth( double bw )
        {
            fBW = bw;
        }
        /** Gets the conversion factor of a calibration record.
       @brief Get conversion factor method
       @return conversion factor
    *********************************************************************/
        double
        GetBandwidth( ) const
        {
            return fBW;
        }
    };

} // namespace calibration

#endif // _LIGO_CALSIGNALINFO_H
