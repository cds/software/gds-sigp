#ifndef _LIGO_UNITDESC_H
#define _LIGO_UNITDESC_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: Unit							*/
/*                                                         		*/
/* Module Description: Unit descriptor					*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 14Mar01  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: doc++						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include "Unit.hh"
#include <string>

namespace calibration
{

    class Descriptor;

    /** @name Unit Descriptor
    Descriptors for simple units and calibration information.
   
    @brief Unit Descriptor
    @author Written March 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

    //@{

    /** Describes a unit and its corresponding calibration correction
    formula. A calibration descriptor will maintain a list of 
    unit descriptors appropriate for the quanity described.
   
    @brief Unit descriptor
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
    class Unit
    {
    public:
        /// Unit Magnitude
        enum EUnitMagnitude
        {
            /// Femto
            kUnitMagFemto = -15,
            /// Pico
            kUnitMagPico = -12,
            /// Nano
            kUnitMagNano = -9,
            /// Micron
            kUnitMagMicron = -6,
            /// milli
            kUnitMagMilli = -3,
            /// 1
            kUnitMagOne = 0,
            /// Kilo
            kUnitMagKilo = 3,
            /// Mega
            kUnitMagMega = 6,
            /// Giga
            kUnitMagGiga = 9,
            /// Terra
            kUnitMagTerra = 12,
            /// Peta
            kUnitMagPeta = 15
        };

        /// Constructor
        explicit Unit( const char* name, Descriptor* cald = 0 )
            : fName( name ), fpcd( cald )
        {
        }
        /// Destructor
        virtual ~Unit( )
        {
        }

        /// Get name of unit
        virtual const char*
        GetName( ) const
        {
            return fName.c_str( );
        }
        /// Get its true name (useful for "none" and "default")
        virtual const char*
        GetTrueName( ) const
        {
            return fName.c_str( );
        }
        /// Unit is power rather than amplitude
        virtual bool
        IsPower( ) const
        {
            return false;
        }

        /// Apply calibration correction for this unit
        virtual bool
        Apply( float*         x,
               float*         y,
               int            N,
               EUnitMagnitude mag = kUnitMagOne,
               bool           complex = false ) const
        {
            return true;
        }

        /// Gets factor from maginutde
        static double Factor( EUnitMagnitude mag );
        /// Gets string from magnitude
        static std::string Mag( EUnitMagnitude mag );
        /// Get a unit descriptor without corrections
        static const Unit& UnitNone( );

    private:
        /// Name of unit
        std::string fName;
        /// Parent calibration descriptor
        Descriptor* fpcd;

        /// a unit which represents no correction
        static const Unit kNone;
    };

    /** Describes a unit which implements simple scaling and offset
    corrections.
   
    @brief Scaling unit descriptor
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
    class UnitScaling : public Unit
    {
    protected:
        /// Slope
        double fSlope;
        /// Offset
        double fOffset;
        /// Scale Y data
        bool fY;

    public:
        /// Constructor
        UnitScaling( const char* name,
                     double      slope,
                     double      ofs = 0,
                     bool        scaleY = true,
                     Descriptor* cald = 0 )
            : Unit( name, cald ), fSlope( slope ), fOffset( ofs ), fY( scaleY )
        {
        }
        /// Destructor
        virtual ~UnitScaling( )
        {
        }
        /// Scale
        virtual bool Apply( float*         x,
                            float*         y,
                            int            N,
                            EUnitMagnitude mag = kUnitMagOne,
                            bool           complex = false ) const;
        /// Get slope
        double
        GetSlope( ) const
        {
            return fSlope;
        }
        /// Get offset
        double
        GetOffset( ) const
        {
            return fOffset;
        }
        /// Scale Y?
        bool
        IsYScaling( ) const
        {
            return fY;
        }

        /// Get a unit descriptor for the identity unit (x axis)
        static const UnitScaling& UnitIdentityX( );
        /// Get a unit descriptor for the identity unit (y axis)
        static const UnitScaling& UnitIdentityY( );
        /// Get a unit descriptor for f -> rad/s
        static const UnitScaling& UnitRadPerSec( );
        /// Get a unit descriptor for f -> Hz
        static const UnitScaling& UnitHertz( );
        /// Get a unit descriptor for t -> sec
        static const UnitScaling& UnitSec( );
        /// Get a unit descriptor for t -> min
        static const UnitScaling& UnitMin( );
        /// Get a unit descriptor for t -> hour
        static const UnitScaling& UnitHour( );
        /// Get a unit descriptor for t -> day
        static const UnitScaling& UnitDay( );

    protected:
        /// the identity unit (x axis)
        static const UnitScaling kIdentityX;
        /// the identity unit (y axis)
        static const UnitScaling kIdentityY;
        /// a unit which represents f -> rad/s
        static const UnitScaling kRadPerSec;
        /// a unit which represents f -> Hz
        static const UnitScaling kHertz;
        /// a unit which represents t -> sec
        static const UnitScaling kSec;
        /// a unit which represents t -> min
        static const UnitScaling kMin;
        /// a unit which represents t -> hour
        static const UnitScaling kHour;
        /// a unit which represents t -> day
        static const UnitScaling kDay;
    };

    /** Describes a list of unit descriptors.
   
    @brief Unit descriptor list
    @author Written May 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
    class UnitList
    {
    protected:
        /** List of unit descriptors; the last descriptor in the list 
          is a sentinel and has to be zero! */
        Unit** fList;
        /// Length of list
        int fLen;

        /// Size of list
        int fSize;
        /// Allocate space for list
        virtual bool Allocate( int size );

    public:
        /// Iterator to traverse the unit list
        typedef Unit* const* iterator;
        /// Const iterator to traverse the unit list
        typedef const Unit* const* const_iterator;

        /// Default constructor
        UnitList( ) : fList( 0 ), fLen( 0 ), fSize( 0 )
        {
            Allocate( 100 );
        }
        /// Destructor
        virtual ~UnitList( )
        {
            Clear( );
            Allocate( 0 );
        }
        /// Copy constructor: ownership of units will be transferred
        UnitList( const UnitList& list );
        /// Copy operator: ownership of units will be transferred
        UnitList& operator=( const UnitList& list );

        /// Size of list
        virtual int
        Size( ) const
        {
            return fLen;
        }
        /// Add an element to list; unit is adopted
        virtual bool Add( Unit* unit );
        /// Find an element by name
        virtual const Unit* Find( const char* unit ) const;
        /// Clear list
        virtual void Clear( );

        /// Beginning of list
        virtual iterator
        begin( )
        {
            return fList;
        }
        /// End of list (returns the sentinel)
        virtual iterator
        end( )
        {
            return fList + fLen;
        }
        /// Beginning of list
        virtual const_iterator
        begin( ) const
        {
            return fList;
        }
        /// End of list (returns the sentinel)
        virtual const_iterator
        end( ) const
        {
            return fList + fLen;
        }
    };

    //@}

} // namespace calibration

#endif // _LIGO_UNITDESC_H
