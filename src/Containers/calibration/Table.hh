#ifndef _LIGO_CALIBRATIONTABLE_H
#define _LIGO_CALIBRATIONTABLE_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGCal							*/
/*                                                         		*/
/* Module Description: Calibration functions.		      		*/
/* 							   		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 17Mai00  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGCal.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include "Calibration.hh"
#include <string>
#include <map>

namespace calibration
{

    class Descriptor;

    /** @name Table
    This header exports functions and options to apply calibration
    corrections and unit adjustments to measured data arrays. It
    also supports editing, importing and exporting of calibration
    records and managing them in a calibration record list.

    The basic unit descriptor and calibration descriptors are
    defined in "CalibrationDesc.h".
   
    @brief Calibration functions and options
    @author Written Mai 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

    //@{

    /** A list of calibration records.
    @brief Calibration table.
    @author Written Mai 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
    class Table
    {
    public:
        /// channel list item
        class ChannelItem
        {
        private:
            /// store last id
            static int fLast;
            /// Unique ID
            int fUnique;
            /// Name of channel
            std::string fName;
            /// Channel enabled?
            bool fEnable;

        public:
            /// Constructor
            ChannelItem( const char* name = "", bool enable = true )
                : fName( name ), fEnable( enable )
            {
                fUnique = ++fLast;
            }
            /// Is equal
            bool operator==( const ChannelItem& obj ) const;
            /// Compare
            bool operator<( const ChannelItem& obj ) const;
            /// Set channel name
            void
            SetName( const char* name )
            {
                fName = name ? name : "";
            }
            /// Get channel name
            const char*
            GetName( ) const
            {
                return fName.c_str( );
            }
            int
            GetUniqueID( ) const
            {
                return fUnique;
            }
            /// Set enable flag
            void
            SetEnable( bool set = true )
            {
                fEnable = set;
            }
            /// Is enabled?
            bool
            IsEnabled( ) const
            {
                return fEnable;
            }
        };
#ifndef __CINT__
        /// Compare two channel names
        class ChannelNameCmp
        {
        public:
            bool operator( )( const std::string& c1,
                              const std::string& c2 ) const;
        };
        /// Channel list type
        typedef std::map< std::string, ChannelItem, ChannelNameCmp >
            ChannelList;
#else
        typedef std::map< std::string, ChannelItem > ChannelList;
#endif
    public:
        /// Default constructor
        explicit Table( const char* lookupfile = 0, bool supdef = true );
        /// Destructor
        virtual ~Table( );
        /// Get calibration list
        virtual const CalibrationList&
        Cal( ) const
        {
            return fCal;
        }
        /// Get length of calibration list
        virtual int
        Len( ) const
        {
            return fCal.size( );
        }
        /// Get Calibration records at position i
        Calibration& operator[]( int i );
        /// Get Calibration records at position i
        const Calibration& operator[]( int i ) const;
        /// Find first record with this channel name
        virtual int FindFirst( const char* chn ) const;
        /// Search for record
        virtual const Calibration* Search( const char* chn,
                                           const char* ref,
                                           const char* unit,
                                           const Time& time ) const;
        /// Search for record
        virtual Calibration* Search( const char* chn,
                                     const char* ref,
                                     const char* unit,
                                     const Time& time );
        /// Search for record
        virtual const Calibration*
                             Search( const Calibration& rec,
                                     CalibrationCmp     cmp = CalibrationCmp( ) ) const;
        virtual Calibration* Search( const Calibration& rec,
                                     CalibrationCmp cmp = CalibrationCmp( ) );
        /// Add a record; a pointer to the new record is returned
        virtual bool Add( const char* chn,
                          const char* ref,
                          const char* unit,
                          const Time& start = Time( 0, 0 ),
                          bool        overwrite = true );
        /// Add a record; the record will be copied
        virtual bool Add( const Calibration& rec, bool overwrite = true );
        /// Delete a record from the list; true if successful
        virtual bool Delete( const char* chn,
                             const char* ref,
                             const char* unit,
                             const Time& time );
        /// Delete a record from the list
        virtual bool Delete( const Calibration& rec );
        /// Clear table
        virtual void Clear( );
        /// Return the channel list
        virtual const ChannelList&
        GetChannels( ) const
        {
            return fChannels;
        }
        /// Find a channel
        virtual const ChannelItem* FindChannel( const char* chn ) const;
        /// Add a channel
        virtual bool AddChannel( const char* chn );
        /// Delete a channel
        virtual bool DeleteChannel( const char* chn );
        /// Clear all channels
        virtual bool ClearChannels( );
        /// Enable a channel
        virtual bool EnableChannel( const char* chn, bool set = true );
        /// Is a channel enabled
        virtual bool IsEnabledChannel( const char* chn );
        /// Number of channels in list
        virtual int
        NumChannels( ) const
        {
            return fChannels.size( );
        }

        /// Import from file
        virtual bool Import( const char* filename );
        /// Export to file
        virtual bool Export( const char* filename ) const;

        /// Lookup calibration information
        virtual bool Lookup( bool forceall = false );
        /// Lookup calibration information
        virtual bool Lookup( const char* filename, bool forceall = false );

        /// Add units to a calibration descriptor
        virtual void AddUnits( Descriptor& cal );

        /// Is default calibration supported
        virtual bool
        HasDefault( ) const
        {
            return fSupDefault;
        }
        /// Set default calibration support
        virtual void SupportDefault( bool sup = true );
        /// return the default calibration channel name
        virtual const char* DefaultChannel( ) const;
        /// return the default transfer function calibration channel name
        virtual const char* DefaultTFChannel( ) const;

    private:
        /// List of calibration records
        CalibrationList fCal;
        /// Channel list
        ChannelList fChannels;
        /// List of channels to lookup
        ChannelList fLookupChns;
        /// Lookup filename
        std::string fLookupFile;
        /// Support default calibration?
        bool fSupDefault;

        /// Import from file (only channels which are in the lookup list)
        virtual bool ImportQ( const char* filename );
        /// Disable copy constructor
        Table( const Table& );
        /// Disable copy operator
        Table& operator=( const Table& );
    };

    /** Function to set a calibration table structure to its default 
    values.
    @brief Set default calibration table options.
    @param cal calibration table
    @return void
 ************************************************************************/
    void SetDefaultTable( Table& cal );

    //@}
} // namespace calibration

#endif // _LIGO_CALIBRATIONTABLE_H
