#ifndef _LIGO_CALIBRATION_H
#define _LIGO_CALIBRATION_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: calibration						*/
/*                                                         		*/
/* Module Description: Calibration container 				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 14Mar01  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: doc++						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include "caltype.h"
#include "Time.hh"
#include "Interval.hh"
#include <string>
#include <vector>

namespace calibration
{

    /** @name Calibration
    This header contains the basic calibration record and list.
   
    @brief Calibration
    @author Written March 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

    //@{

    /** A container for a calibration record.
   
    @brief Calibration container
    @author Written March 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/
    class Calibration : private calrec_t
    {
    public:
        /// Default constructor
        Calibration( );
        /// Copy constructor
        Calibration( const Calibration& cal );
        /// Constructor from calrec_t
        Calibration( const calrec_t& cal );
        /// Destructor
        ~Calibration( );
        /// Assignment operator
        Calibration& operator=( const Calibration& cal );
        /// Conversion operator to calrec_t*
        operator const calrec_t*( ) const
        {
            return this;
        }

        /// Equal
        bool operator==( const Calibration& cal ) const;
        /// Unequal
        bool
        operator!=( const Calibration& cal ) const
        {
            return !( *this == cal );
        }
        /// Smaller
        bool operator<( const Calibration& cal ) const;
        /// Smaller or equal
        bool
        operator<=( const Calibration& cal ) const
        {
            return ( *this < cal ) || ( *this == cal );
        }
        /// Greater
        bool
        operator>( const Calibration& cal ) const
        {
            return !( *this <= cal );
        }
        /// Greater or equal
        bool
        operator>=( const Calibration& cal ) const
        {
            return !( *this < cal );
        }

        /** This function is used by the query function to determine if 
       a query record matches a calibration record.
       @brief Match method
       @param query query record
       @return true if match, false otherwise
    *********************************************************************/
        bool Match( const Calibration& query ) const;

        /** Sets the channel name.
       @brief Set channel name method
       @param chnname channel name
    *********************************************************************/
        void SetChannel( const char* chnname );
        /** Gets the channel name.
       @brief Get channel name method
       @return channel name
    *********************************************************************/
        const char* GetChannel( ) const;

        /** Sets the reference point.
       @brief Set reference point method
       @param ref channel name
    *********************************************************************/
        void SetRef( const char* ref );
        /** Gets the reference point.
       @brief Get reference point method
       @return channel name
    *********************************************************************/
        const char* GetRef( ) const;

        /** Sets the unit string.
       @brief Set unit string method
       @param unit unit string
    *********************************************************************/
        void SetUnit( const char* unit );
        /** Gets the unit string.
       @brief Get unit string method
       @return unit string
    *********************************************************************/
        const char* GetUnit( ) const;

        /** Sets the calibration time.
       @brief Set calibration time method
       @param time time
    *********************************************************************/
        void SetTime( const Time& time );
        /** Gets the calibration time.
       @brief Get calibration time method
       @return time
    *********************************************************************/
        Time GetTime( ) const;

        /** Sets the calibration duration.
       @brief Set calibration duration method
       @param dur duration
    *********************************************************************/
        void SetDuration( const Interval& dur );
        /** Gets the calibration duration.
       @brief Get calibration duration method
       @return duration
    *********************************************************************/
        Interval GetDuration( ) const;

        /** Gets the calibration type.
       @brief Get calibration type method
       @return type
    *********************************************************************/
        int GetType( ) const;

        /** Sets the conversion factor of a calibration record.
       @brief Set conversion factor method
       @param conv conversion factor
    *********************************************************************/
        void SetConversion( double conv );
        /** Resets the conversion factor of a calibration record.
       @brief Reset conversion factor method
    *********************************************************************/
        void ResetConversion( );
        /** Gets the conversion factor of a calibration record.
       @brief Get conversion factor method
       @return conversion factor
    *********************************************************************/
        double GetConversion( ) const;

        /** Sets the offset of a calibration record.
       @brief Set offset method
       @param ofs offset
    *********************************************************************/
        void SetOffset( double ofs );
        /** Resets the offset of a calibration record.
       @brief Reset offset method
    *********************************************************************/
        void ResetOffset( );
        /** Gets the offset of a calibration record.
       @brief Get offset method
       @return offset
    *********************************************************************/
        double GetOffset( ) const;

        /** Sets the time delay of a calibration record.
       @brief Set time delay method
       @param dt time delay
    *********************************************************************/
        void SetTimeDelay( double dt );
        /** Resets the time delay of a calibration record.
       @brief Reset time delay method
    *********************************************************************/
        void ResetTimeDelay( );
        /** Gets the time delay of a calibration record.
       @brief Get time delay method
       @return time delay
    *********************************************************************/
        double GetTimeDelay( ) const;

        /** Sets the transfer function of a calibration record. If a
       NULL pointer is specified the transfer function is disabled.
       @brief Set transfer function method
       @param trans Transfer function array
       @param len Number of points
    *********************************************************************/
        void SetTransferFunction( const float* trans, int len );
        /** Gets the transfer function of a calibration record.
       @brief Get transfer function method
       @param trans Transfer function array pointer (return)
       @return Number of points
    *********************************************************************/
        int GetTransferFunction( const float*& trans ) const;

        /** Sets the pole and zeros of a calibration record. If a Null pointer
       is specified the poles and zeros are disabled.
       @brief Set pole-zero method
       @param gain Gain
       @param pnum Number of poles
       @param znum Number of zeros
       @param pzs Complex array of poles and zeros
    *********************************************************************/
        void SetPoleZeros( double gain, int pnum, int znum, const float* pzs );
        /** Gets the poles and zeros of a calibration record.
       @brief Get pole-zero method
       @param gain Gain (return)
       @param pnum Number of poles (return)
       @param znum Number of zeros (return)
       @param pzs Complex array of poles and zeros (return)
       @return true if successful
    *********************************************************************/
        int GetPoleZeros( double&       gain,
                          int&          pnum,
                          int&          znum,
                          const float*& pzs ) const;

        /** Sets the default flag of a calibration record.
       @brief Set default method
       @param def default
    *********************************************************************/
        void SetDefault( bool def = true );
        /** Gets the default flag of a calibration record.
       @brief Get default method
       @return default state
    *********************************************************************/
        bool GetDefault( ) const;

        /** Sets the preferred magnitude of a calibration record.
       @brief Set preferred magnitude method
       @param mag preferred magnitude (power of 10)
    *********************************************************************/
        void SetPreferredMag( int mag = 0 );
        /** Gets the preferred magnitude of a calibration record.
       @brief Get preferred magnitude method
       @return preferred magnitude
    *********************************************************************/
        int GetPreferredMag( ) const;

        /** Sets the preferred derivative of a calibration record.
       @brief Set preferred derivative method
       @param d preferred derivative
    *********************************************************************/
        void SetPreferredD( int d = 0 );
        /** Gets the preferred derivative of a calibration record.
       @brief Get preferred derivative method
       @return preferred derivative
    *********************************************************************/
        int GetPreferredD( ) const;

        /** Sets the comment field of a calibration record.
       @brief Set comment method
       @param comment Comment
    *********************************************************************/
        void SetComment( const char* comment = 0 );
        /** Gets the comment field of a calibration record.
       @brief Get comment method
       @return Comment
    *********************************************************************/
        const char* GetComment( ) const;

        /** Get an XML representation of the record.
       @brief XML method
       @return true if successful
    *********************************************************************/
        bool Xml( int          flag,
                  std::string& s,
                  int          index = -1,
                  std::string* errormsg = 0 ) const;
    };

    /** Compare calibration records.
   
    @brief Compare calibration records
    @author Written March 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/
    class CalibrationCmp
    {
    public:
        /// Compare mode
        enum calcmp
        {
            /// Compare name, reference, unit and time
            kNormal = 0,
            /// Compare name only
            kNameOnly = 1,
            /// Compare name, reference and unit
            kNameRefUnit = 2,
            /// Compare name and reference
            kNameRef = 3
        };

        /// Constructor
        explicit CalibrationCmp( calcmp mode = kNormal ) : fMode( mode )
        {
        }
        /// Compare
        bool
        operator( )( const Calibration& c1, const Calibration& c2 ) const
        {
            return IsSmaller( c1, c2 );
        }
        /// Is Equal
        bool IsEqual( const Calibration& c1, const Calibration& c2 ) const;
        /// Is Equal
        bool IsSmaller( const Calibration& c1, const Calibration& c2 ) const;

    private:
        /// Compare mode
        calcmp fMode;
    };

    /** A calibration array holds a vector of calibration records.
   
    @brief Calibration array
    @author Written March 2001 by Daniel Sigg
    @version 1.0
   ************************************************************************/
    class CalibrationList : public std::vector< Calibration >
    {
    public:
        /// Default constructor
        CalibrationList( )
        {
        }
        /// Construct from a file
        CalibrationList( const char* filename );

        /** This function reads calibration records from a file. 
       (This function uses the expat xml parser.)
       @brief Read method
       @param filename Name of file to read
       @return true if success, false otherwise
      *********************************************************************/
        bool Read( const char* filename );
        /** This function writes calibration records to a file.
       @brief Write method
       @param filename Name of file to write
       @return true if success, false otherwise
      *********************************************************************/
        bool Write( const char* filename ) const;
    };

    //@}

} // namespace calibration

#endif // _LIGO_CALIBRATION_H
