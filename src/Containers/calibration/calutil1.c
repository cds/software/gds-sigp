#ifndef __EXTENSIONS__
#define __EXTENSIONS__
#endif
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <strings.h>
#include "calutil.h"

const char* const xml_header =
    "<?xml version=\"1.0\"?>\n"
    "<!DOCTYPE LIGO_LW [\n"
    "<!ELEMENT LIGO_LW ((LIGO_LW|Comment|Param|Time|Table|Array|Stream)*)>\n"
    "<!ATTLIST LIGO_LW Name CDATA #IMPLIED Type CDATA #IMPLIED>\n"
    "<!ELEMENT Comment (#PCDATA)>\n"
    "<!ELEMENT Param (#PCDATA)>\n"
    "<!ATTLIST Param Name CDATA #IMPLIED Type CDATA #IMPLIED Dim CDATA "
    "#IMPLIED\n"
    "                Unit CDATA #IMPLIED>\n"
    "<!ELEMENT Table (Comment?,Column*,Stream?)>\n"
    "<!ATTLIST Table Name CDATA #IMPLIED Type CDATA #IMPLIED>\n"
    "<!ELEMENT Column EMPTY>\n"
    "<!ATTLIST Column Name CDATA #IMPLIED Type CDATA #IMPLIED Unit CDATA "
    "#IMPLIED>\n"
    "<!ELEMENT Array (Dim*,Stream?)>\n"
    "<!ATTLIST Array Name CDATA #IMPLIED Type CDATA #IMPLIED>\n"
    "<!ELEMENT Dim (#PCDATA)>\n"
    "<!ATTLIST Dim Name CDATA #IMPLIED>\n"
    "<!ELEMENT Stream (#PCDATA)>\n"
    "<!ATTLIST Stream Name CDATA #IMPLIED Type (Remote|Local) \"Local\"\n"
    "          Delimiter CDATA \",\" Encoding CDATA #IMPLIED Content CDATA "
    "#IMPLIED>\n"
    "<!ELEMENT Time (#PCDATA)>\n"
    "<!ATTLIST Time Name CDATA #IMPLIED Type (GPS|Unix|ISO-8601) "
    "\"ISO-8601\">\n"
    "]>\n";

static int
strpos( const char* s, char c )
{
    int pos;
    for ( pos = 0; s[ pos ] != 0; pos++ )
    {
        if ( s[ pos ] == c )
        {
            return pos;
        }
    }
    return -1;
}

void
calinit( calrec_t* cal )
{
    memset( cal, 0, sizeof( calrec_t ) );
}

void
calrelease( calrec_t* cal )
{
    free( cal->fTransferFunction );
    cal->fTransferFunction = NULL;
    free( cal->fPoleZero.fPoleZeros );
    cal->fPoleZero.fPoleZeros = NULL;
    free( cal->fComment );
    cal->fComment = NULL;
}

struct calhead_t
{
    int      num;
    calrec_t cal[ 1 ];
};
typedef struct calhead_t calhead_t;

calrec_t*
calnew( int num )
{
    calhead_t* p;
    int        i;

    if ( num <= 0 )
    {
        return NULL;
    }
    p = malloc( offsetof( calhead_t, cal ) + num * sizeof( calrec_t ) );
    if ( p == NULL )
    {
        return NULL;
    }
    for ( i = 0; i < num; i++ )
    {
        calinit( p->cal + i );
    }
    p->num = num;
    return p->cal;
}

void
caldelete( calrec_t* cal )
{
    calhead_t* p;
    int        i;

    if ( cal == NULL )
    {
        return;
    }
    p = (calhead_t*)( (char*)cal - offsetof( calhead_t, cal ) );
    for ( i = 0; i < p->num; i++ )
    {
        calrelease( p->cal + i );
    }
    free( p );
}

calrec_t*
calresize( calrec_t* cal, int num )
{
    calhead_t* p;
    calhead_t* newp;
    int        i;

    /* handle special cases */
    if ( num < 0 )
    {
        return NULL;
    }
    else if ( num == 0 )
    {
        caldelete( cal );
        return NULL;
    }
    p = (calhead_t*)( (char*)cal - offsetof( calhead_t, cal ) );
    if ( num == p->num )
    {
        return cal;
    }
    /* realloc */
    for ( i = num; i < p->num; i++ )
    {
        calrelease( p->cal + i );
    }
    newp = realloc( p, offsetof( calhead_t, cal ) + num * sizeof( calrec_t ) );
    if ( newp == NULL )
    {
        return NULL;
    }
    for ( i = p->num; i < num; i++ )
    {
        calinit( p->cal + i );
    }
    newp->num = num;
    return newp->cal;
}

int
calsize( const calrec_t* cal )
{
    calhead_t* p;

    if ( cal == NULL )
    {
        return -1;
    }
    p = (calhead_t*)( (char*)cal - offsetof( calhead_t, cal ) );
    return p->num;
}

void
calcpy( calrec_t* cal1, const calrec_t* cal2 )
{
    int len;

    if ( cal1 != cal2 )
    {
        calrelease( cal1 );
        memcpy( cal1, cal2, sizeof( calrec_t ) );
        cal1->fTransferFunction = NULL;
        cal1->fPoleZero.fPoleZeros = NULL;
        cal1->fComment = NULL;
        /* transfer function */
        if ( ( cal1->fType & CALTRANSFERFUNCTION ) != 0 )
        {
            cal1->fTransferFunction =
                calloc( cal1->fTransferFunctionLen, 3 * sizeof( float ) );
            if ( cal1->fTransferFunction != NULL )
            {
                memcpy( cal1->fTransferFunction,
                        cal2->fTransferFunction,
                        3 * cal1->fTransferFunctionLen * sizeof( float ) );
            }
            else
            {
                cal1->fType &= ~CALTRANSFERFUNCTION;
            }
        }
        /* pole zeros */
        if ( ( cal1->fType & CALPOLEZERO ) != 0 )
        {
            if ( cal1->fPoleZero.fPoleNum < 0 )
                cal1->fPoleZero.fPoleNum = 0;
            if ( cal1->fPoleZero.fZeroNum < 0 )
                cal1->fPoleZero.fZeroNum = 0;
            len = ( cal1->fPoleZero.fPoleNum > cal1->fPoleZero.fZeroNum )
                ? cal1->fPoleZero.fPoleNum
                : cal1->fPoleZero.fZeroNum;
            cal1->fPoleZero.fPoleZeros = calloc( len + 1, 4 * sizeof( float ) );
            if ( ( cal1->fPoleZero.fPoleZeros != NULL ) &&
                 ( cal2->fPoleZero.fPoleZeros != NULL ) )
            {
                memcpy( cal1->fPoleZero.fPoleZeros,
                        cal2->fPoleZero.fPoleZeros,
                        4 * len * sizeof( float ) );
            }
            else
            {
                cal1->fType &= ~CALPOLEZERO;
            }
        }
        /* comment */
        calsetcomment( cal1, cal2->fComment );
    }
}

int
calcmp( const calrec_t* cal1, const calrec_t* cal2 )
{
    int cmp;
    if ( ( cmp = strcasecmp( cal1->fChannel, cal2->fChannel ) ) != 0 )
    {
        return cmp;
    }
    if ( ( cmp = strcasecmp( cal1->fReference, cal2->fReference ) ) != 0 )
    {
        return cmp;
    }
    if ( ( cmp = strcasecmp( cal1->fUnit, cal2->fUnit ) ) != 0 )
    {
        return cmp;
    }
    if ( cal1->fTime < cal2->fTime )
    {
        return -1;
    }
    else if ( cal1->fTime > cal2->fTime )
    {
        return 1;
    }
    return 0;
}

int
calcmp2( const calrec_t* cal1, const calrec_t* cal2 )
{
    int cmp;
    if ( ( cmp = strcasecmp( cal1->fChannel, cal2->fChannel ) ) != 0 )
    {
        return cmp;
    }
    if ( ( cmp = strcasecmp( cal1->fReference, cal2->fReference ) ) != 0 )
    {
        return cmp;
    }
    if ( ( cmp = strcasecmp( cal1->fUnit, cal2->fUnit ) ) != 0 )
    {
        return cmp;
    }
    return 0;
}

int
calmatch( const calrec_t* query, const calrec_t* cal )
{
    int pos; /* wildcard position */

    /* test time */
    if ( query->fDuration == 0 )
    {
        if ( ( cal->fDuration != 0 ) || ( query->fTime < cal->fTime ) )
        {
            return 0;
        }
    }
    else if ( cal->fDuration == 0 )
    {
        if ( query->fTime + query->fDuration < cal->fTime )
        {
            return 0;
        }
    }
    else
    {
        if ( ( query->fTime + query->fDuration < cal->fTime ) ||
             ( query->fTime >= cal->fTime + cal->fDuration ) )
        {
            return 0;
        }
    }
    /* test channel name */
    pos = strpos( query->fChannel, '*' );
    if ( pos == -1 )
    {
        if ( strcasecmp( query->fChannel, cal->fChannel ) != 0 )
        {
            return 0;
        }
    }
    else if ( pos > 0 )
    {
        if ( strncasecmp( query->fChannel, cal->fChannel, pos ) != 0 )
        {
            return 0;
        }
    }
    /* test reference point */
    pos = strpos( query->fReference, '*' );
    if ( pos == -1 )
    {
        if ( strcasecmp( query->fReference, cal->fReference ) != 0 )
        {
            return 0;
        }
    }
    else if ( pos > 0 )
    {
        if ( strncasecmp( query->fReference, cal->fReference, pos ) != 0 )
        {
            return 0;
        }
    }
    /* test unit string */
    pos = strpos( query->fUnit, '*' );
    if ( pos == -1 )
    {
        if ( strcasecmp( query->fUnit, cal->fUnit ) != 0 )
        {
            return 0;
        }
    }
    else if ( pos > 0 )
    {
        if ( strncasecmp( query->fUnit, cal->fUnit, pos ) != 0 )
        {
            return 0;
        }
    }
    /* match */
    return 1;
}

void
calsort( calrec_t* cal, int len )
{
    qsort( cal,
           len,
           sizeof( calrec_t ),
           (int ( * )( const void*, const void* ))calcmp );
}

calrec_t*
calbsearch( const calrec_t* key, const calrec_t* cal, int len )
{
    return (calrec_t*)bsearch( key,
                               cal,
                               len,
                               sizeof( calrec_t ),
                               (int ( * )( const void*, const void* ))calcmp );
}

int
calinsert( calrec_t** cal, int* len, const calrec_t* newcal )
{
    int l; /* left */
    int r; /* right */
    int m; /* middle */

    if ( ( cal == 0 ) || ( len == 0 ) || ( newcal == 0 ) )
    {
        return -1;
    }
    /* allocate enough space */
    if ( calsize( *cal ) < *len + 1 )
    {
        calrec_t* ncal = calresize( *cal, *len + 100 );
        if ( ncal != 0 )
        {
            *cal = ncal;
        }
        else
        {
            return -1;
        }
    }
    /* if list empty add at first place */
    if ( *len == 0 )
    {
        calcpy( *cal, newcal );
        ( *len )++;
        return 0;
    }
    /* if before first element shift everything to the right */
    if ( calcmp( newcal, *cal ) < 0 )
    {
        memmove( *cal + 1, *cal, *len * sizeof( calrec_t ) );
        calinit( *cal );
        calcpy( *cal, newcal );
        ( *len )++;
        return 0;
    }
    /* binary search for insert position */
    l = 0;
    r = *len;
    while ( l < r - 1 )
    {
        m = ( l + r ) / 2;
        if ( calcmp( newcal, *cal + m ) < 0 )
        {
            r = m;
        }
        else
        {
            l = m;
        }
    }
    /* check if duplicate */
    if ( calcmp( newcal, *cal + l ) == 0 )
    {
        calcpy( *cal + l, newcal );
        return 0;
    }
    /* now insert at position r */
    if ( r < *len )
    {
        memmove( *cal + r + 1, *cal + r, ( *len - r ) * sizeof( calrec_t ) );
        calinit( *cal + r );
    }
    calcpy( *cal + r, newcal );
    ( *len )++;
    return 0;
}

int
calremove( calrec_t** cal, int* len, const calrec_t* delcal )
{
    int l; /* left */
    int r; /* right */
    int m; /* middle */

    if ( ( cal == 0 ) || ( len == 0 ) || ( delcal == 0 ) )
    {
        return -1;
    }
    /* if before first element return */
    if ( ( *len <= 0 ) || ( calcmp( delcal, *cal ) < 0 ) )
    {
        return -1;
    }
    l = 0;
    r = *len;
    while ( l < r - 1 )
    {
        m = ( l + r ) / 2;
        if ( calcmp( delcal, *cal + m ) < 0 )
        {
            r = m;
        }
        else
        {
            l = m;
        }
    }
    /* check if found */
    if ( calcmp( delcal, *cal + l ) == 0 )
    {
        calrelease( *cal + l );
        if ( l + 1 < *len )
        {
            memmove(
                *cal + l, *cal + l + 1, ( *len - l - 1 ) * sizeof( calrec_t ) );
        }
        ( *len )--;
        return 0;
    }
    else
    {
        return -1;
    }
}

/* calread is in calutil3.c */

#define _MAXBUF ( 128 * 1024 )

int
calwrite( int flag, calrec_t* cal, int len, const char* file )
{
    int   i;
    FILE* fd;
    char* buf;
    int   blen;

    fd = fopen( file, "w" );
    if ( fd == NULL )
    {
        return -1;
    }
    buf = malloc( _MAXBUF );
    if ( buf == NULL )
    {
        fclose( fd );
        return -2;
    }
    /* write header */
    /*strcpy (buf, "<?xml version=\"1.0\"?>\n"
             "<!DOCTYPE LIGO_LW SYSTEM "
             "\"http://www.cacr.caltech.edu/projects/ligo_lw.dtd\">\n"
             "<LIGO_LW>\n");*/
    strcpy( buf, xml_header );
    blen = strlen( buf );
    fwrite( buf, sizeof( char ), blen, fd );
    /* write records */
    for ( i = 0; i < len; i++ )
    {
        blen = cal2xml( flag, cal + i, buf, _MAXBUF - 1, i, NULL );
        if ( blen > 0 )
        {
            fwrite( buf, sizeof( char ), blen, fd );
        }
    }
    /* write trailer */
    strcpy( buf, "</LIGO_LW>\n" );
    blen = strlen( buf );
    fwrite( buf, sizeof( char ), blen, fd );

    free( buf );
    fclose( fd );
    return 0;
}

void
calsetchannel( calrec_t* cal, const char* chnname )
{
    strncpy( cal->fChannel, chnname, CALCHANNELSIZE - 1 );
}

const char*
calgetchannel( const calrec_t* cal )
{
    return cal->fChannel;
}

void
calsetref( calrec_t* cal, const char* ref )
{
    strncpy( cal->fReference, ref, CALREFERENCESIZE - 1 );
}

const char*
calgetref( const calrec_t* cal )
{
    return cal->fReference;
}

void
calsetunit( calrec_t* cal, const char* unit )
{
    strncpy( cal->fUnit, unit, CALUNITSIZE - 1 );
}

const char*
calgetunit( const calrec_t* cal )
{
    return cal->fUnit;
}

void
calsettime( calrec_t* cal, unsigned long time )
{
    cal->fTime = time;
}

unsigned long
calgettime( const calrec_t* cal )
{
    return cal->fTime;
}

void
calsetduration( calrec_t* cal, unsigned long dur )
{
    cal->fDuration = dur;
}

unsigned long
calgetduration( const calrec_t* cal )
{
    return cal->fDuration;
}

int
calgettype( const calrec_t* cal )
{
    return cal->fType;
}

void
calsetconversion( calrec_t* cal, double conv )
{
    cal->fConversion = conv;
    cal->fType |= CALAMPLITUDE;
}

void
calresetconversion( calrec_t* cal )
{
    cal->fType &= ~CALAMPLITUDE;
}

double
calgetconversion( const calrec_t* cal )
{
    if ( ( cal->fType & CALAMPLITUDE ) != 0 )
    {
        return cal->fConversion;
    }
    else
    {
        return 1.0;
    }
}

void
calsetoffset( calrec_t* cal, double ofs )
{
    cal->fOffset = ofs;
    cal->fType |= CALOFFSET;
}

void
calresetoffset( calrec_t* cal )
{
    cal->fType &= ~CALOFFSET;
}

double
calgetoffset( const calrec_t* cal )
{
    if ( ( cal->fType & CALOFFSET ) != 0 )
    {
        return cal->fOffset;
    }
    else
    {
        return 0.0;
    }
}

void
calsettimedelay( calrec_t* cal, double td )
{
    cal->fTimeDelay = td;
    cal->fType |= CALTIMEDELAY;
}

void
calresettimedelay( calrec_t* cal )
{
    cal->fType &= ~CALTIMEDELAY;
}

double
calgettimedelay( const calrec_t* cal )
{
    if ( ( cal->fType & CALTIMEDELAY ) != 0 )
    {
        return cal->fTimeDelay;
    }
    else
    {
        return 0.0;
    }
}

void
calsettransferfunction( calrec_t* cal, const float* trans, int len )
{
    float* temp;

    temp = cal->fTransferFunction;
    if ( len > 0 )
    {
        cal->fTransferFunction = calloc( len, 3 * sizeof( float ) );
        cal->fTransferFunctionLen = len;
    }
    else
    {
        cal->fTransferFunction = NULL;
        cal->fTransferFunctionLen = 0;
    }
    if ( ( trans != 0 ) && ( cal->fTransferFunction != NULL ) )
    {
        memcpy( cal->fTransferFunction, trans, 3 * len * sizeof( float ) );
        cal->fType |= CALTRANSFERFUNCTION;
    }
    else
    {
        cal->fType &= ~CALTRANSFERFUNCTION;
    }
    free( temp );
}

int
calgettransferfunction( const calrec_t* cal, const float** trans )
{
    if ( ( ( cal->fType & CALTRANSFERFUNCTION ) != 0 ) &&
         ( cal->fTransferFunction != NULL ) )
    {
        *trans = cal->fTransferFunction;
        return cal->fTransferFunctionLen;
    }
    else
    {
        *trans = NULL;
        return 0;
    }
}

void
calsetpolezeros(
    calrec_t* cal, double gain, int pnum, int znum, const float* pzs )
{
    int    len;
    float* temp;

    temp = cal->fPoleZero.fPoleZeros;
    if ( pnum < 0 )
        pnum = 0;
    if ( znum < 0 )
        znum = 0;
    len = ( pnum > znum ) ? pnum : znum;
    if ( pzs == NULL )
    {
        cal->fPoleZero.fPoleZeros = NULL;
        cal->fPoleZero.fGain = gain;
        cal->fPoleZero.fPoleNum = 0;
        cal->fPoleZero.fZeroNum = 0;
        cal->fType &= ~CALPOLEZERO;
    }
    else
    {
        cal->fPoleZero.fPoleZeros = calloc( len + 1, 4 * sizeof( float ) );
        cal->fPoleZero.fGain = gain;
        cal->fPoleZero.fPoleNum = pnum;
        cal->fPoleZero.fZeroNum = znum;
        if ( cal->fPoleZero.fPoleZeros != NULL )
        {
            memcpy( cal->fPoleZero.fPoleZeros, pzs, 4 * len * sizeof( float ) );
            cal->fType |= CALPOLEZERO;
        }
        else
        {
            cal->fPoleZero.fPoleNum = 0;
            cal->fPoleZero.fZeroNum = 0;
            cal->fType &= ~CALPOLEZERO;
        }
    }
    if ( temp != NULL )
        free( temp );
}

int
calgetpolezeros(
    const calrec_t* cal, double* gain, int* pnum, int* znum, const float** pzs )
{
    if ( ( cal->fType & CALPOLEZERO ) != 0 )
    {
        if ( gain != 0 )
            *gain = cal->fPoleZero.fGain;
        if ( pnum != 0 )
            *pnum = cal->fPoleZero.fPoleNum;
        if ( znum != 0 )
            *znum = cal->fPoleZero.fZeroNum;
        if ( pzs != 0 )
            *pzs = cal->fPoleZero.fPoleZeros;
        return 1;
    }
    else
    {
        return 0;
    }
}

void
calsetdefault( calrec_t* cal, int def )
{
    cal->fDefault = ( def != 0 );
}

int
calgetdefault( const calrec_t* cal )
{
    return cal->fDefault;
}

void
calsetpreferredmag( calrec_t* cal, int mag )
{
    cal->fPreferredMag = mag;
}

int
calgetpreferredmag( const calrec_t* cal )
{
    return cal->fPreferredMag;
}

void
calsetpreferredd( calrec_t* cal, int d )
{
    cal->fPreferredD = d;
}

int
calgetpreferredd( const calrec_t* cal )
{
    return cal->fPreferredD;
}

void
calsetcomment( calrec_t* cal, const char* comment )
{
    free( cal->fComment );
    if ( comment == NULL )
    {
        cal->fComment = NULL;
    }
    else
    {
        cal->fComment = malloc( strlen( comment ) + 1 );
        if ( cal->fComment != NULL )
        {
            strcpy( cal->fComment, comment );
        }
    }
}

const char*
calgetcomment( const calrec_t* cal )
{
    return cal->fComment;
}
