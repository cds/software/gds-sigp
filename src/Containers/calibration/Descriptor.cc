#ifndef __EXTENSIONS__
#define __EXTENSIONS__
#endif
#include "Descriptor.hh"
#include "Table.hh"
#include <cmath>
#include <cstring>

namespace calibration
{
    using namespace std;

    /// Time series
    const char kPTTimeSeries[] = "Time series";
    /// Frequency series
    const char kPTFrequencySeries[] = "Frequency series";
    /// Power spectrum
    const char kPTPowerSpectrum[] = "Power spectrum";
    /// Coherence
    const char kPTCoherence[] = "Coherence";
    /// Cross correlation
    const char kPTCrossCorrelation[] = "Cross power spectrum";
    /// Transfer function
    const char kPTTransferFunction[] = "Transfer function";
    /// Coherence function
    const char kPTCoherenceFunction[] = "Coherence function";
    /// Transfer coefficients
    const char kPTTransferCoefficients[] = "Transfer coefficients";
    /// Coherence coeffcients
    const char kPTCoherenceCoefficients[] = "Coherence coefficients";
    /// Harmonic coefficients
    const char kPTHarmonicCoefficients[] = "Harmonic coefficients";
    /// Intermodulation coefficients
    const char kPTIntermodulationCoefficients[] =
        "Intermodulation coeffiecients";
    /// XY plot
    const char kPTXY[] = "XY";

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // Descriptor                                                           //
    //                                                                      //
    // Descriptor for calibrations                                          //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    Descriptor::infoptr&
    Descriptor::infoptr::operator=( const infoptr& info )
    {
        if ( this != &info )
        {
            fInfo = info.fInfo;
            info.fInfo = 0;
        }
        return *this;
    }

    //______________________________________________________________________________
    void
    Descriptor::infoptr::Set( Pointer p )
    {
        if ( fInfo )
            delete fInfo;
        fInfo = p;
    }

    //______________________________________________________________________________
    Descriptor::Descriptor( const Descriptor* cald )
        : fValid( false ), fSigInfo( 0 )
    {
        if ( ( cald != 0 ) && cald->IsValid( ) )
        {
            *this = *cald;
        }
        else
        {
            Init( );
        }
    }

    //______________________________________________________________________________
    Descriptor::~Descriptor( )
    {
    }

    //______________________________________________________________________________
    void
    Descriptor::Init( )
    {
        fTime = Time( 0, 0 );
        fChnRelation = kCalChnRelSingle;
        fChannel[ 0 ] = "";
        fChannel[ 1 ] = "";
        fCoeff[ 0 ] = 1.0;
        fCoeff[ 1 ] = 1.0;
        fExpo[ 0 ] = 1;
        fExpo[ 1 ] = 1;
        fConj[ 0 ] = false;
        fConj[ 1 ] = false;
        fDomain[ 0 ] = kCalDomainTime;
        fDomain[ 1 ] = kCalDomainValue;
        fDensityUnits[ 0 ] = kCalDensityNo;
        fDensityUnits[ 1 ] = kCalDensityNo;
        fBW[ 0 ] = 0;
        fBW[ 1 ] = 0;
        fPreferredMag[ 0 ] = 0;
        fPreferredMag[ 1 ] = 0;
        fUnits[ 0 ].Clear( );
        fUnits[ 1 ].Clear( );
        fSigInfo.Set( 0 );
    }

    //______________________________________________________________________________
    void
    Descriptor::Clone( const Descriptor& desc, Table* caltable )
    {
        fValid = desc.fValid;
        fTime = desc.fTime;
        fChnRelation = desc.fChnRelation;
        fChannel[ 0 ] = desc.fChannel[ 0 ];
        fChannel[ 1 ] = desc.fChannel[ 1 ];
        fCoeff[ 0 ] = desc.fCoeff[ 0 ];
        fCoeff[ 1 ] = desc.fCoeff[ 1 ];
        fExpo[ 0 ] = desc.fExpo[ 0 ];
        fExpo[ 1 ] = desc.fExpo[ 1 ];
        fConj[ 0 ] = desc.fConj[ 0 ];
        fConj[ 1 ] = desc.fConj[ 1 ];
        fDomain[ 0 ] = desc.fDomain[ 0 ];
        fDomain[ 1 ] = desc.fDomain[ 1 ];
        fDensityUnits[ 0 ] = desc.fDensityUnits[ 0 ];
        fDensityUnits[ 1 ] = desc.fDensityUnits[ 1 ];
        fBW[ 0 ] = desc.fBW[ 0 ];
        fBW[ 1 ] = desc.fBW[ 1 ];
        fPreferredMag[ 0 ] = desc.fPreferredMag[ 0 ];
        fPreferredMag[ 1 ] = desc.fPreferredMag[ 1 ];
        fUnits[ 0 ].Clear( );
        fUnits[ 1 ].Clear( );
        fSigInfo.Set( 0 );
        if ( caltable )
        {
            caltable->AddUnits( *this );
        }
    }

    //______________________________________________________________________________
    bool
    Descriptor::Setup( const char* graphtype,
                       const char* Achn,
                       const char* Bchn,
                       double      bw,
                       const Time& time )
    {
        Init( );
        fTime = time;
        // init only
        if ( ( graphtype == 0 ) || ( Achn == 0 ) )
        {
            return true;
        }
        // remove index/comments from channel names
        char  a[ 1024 ];
        char  bb[ 1024 ];
        char* b;
        char* p;
        strncpy( a, Achn, 1024 );
        a[ 1023 ] = 0;
        // get rid of index
        if ( ( p = strchr( a, '[' ) ) != 0 )
        {
            *p = 0;
        }
        // get rid of special type id
        if ( ( p = strchr( a, '(' ) ) != 0 )
        {
            *p = 0;
        }
        // get rid of mangling
        if ( ( p = strstr( a, "_!_" ) ) != 0 )
        {
            *p = 0;
        }

        // get rid of copy index
        // if (((p = strchr (a, '.')) != 0) && isdigit (p[1])) {
        // *p = 0;
        // }
        if ( Bchn )
        {
            strncpy( bb, Bchn, 1024 );
            bb[ 1023 ] = 0;
            if ( ( p = strchr( bb, '[' ) ) != 0 )
            {
                *p = 0;
            }
            if ( ( p = strchr( bb, '(' ) ) != 0 )
            {
                *p = 0;
            }
            if ( ( p = strstr( a, "_!_" ) ) != 0 )
            {
                *p = 0;
            }
            // if (((p = strchr (bb, '.')) != 0) && isdigit (p[1])) {
            // *p = 0;
            // }
            b = bb;
        }
        else
        {
            b = 0;
        }

        // time series
        if ( strcasecmp( graphtype, kPTTimeSeries ) == 0 )
        {
            SetValid( );
            SetDomain( kCalUnitX, kCalDomainTime );
            SetChannel( 0, a );
            return true;
        }
        // frequency series
        else if ( strcasecmp( graphtype, kPTFrequencySeries ) == 0 )
        {
            SetValid( );
            SetDomain( kCalUnitX, kCalDomainFrequency );
            SetChannel( 0, a );
            SetBW( kCalUnitY, bw );
            SetDensity( kCalUnitY, kCalDensityAmpPerRtHz );
            return true;
        }
        // power spectrum
        else if ( strcasecmp( graphtype, kPTPowerSpectrum ) == 0 )
        {
            SetValid( );
            SetDomain( kCalUnitX, kCalDomainFrequency );
            SetChannel( 0, a );
            SetBW( kCalUnitY, bw );
            SetDensity( kCalUnitY, kCalDensityAmpPerRtHz );
            return true;
        }
        // coherence
        else if ( strcasecmp( graphtype, kPTCoherence ) == 0 )
        {
            SetValid( );
            SetDomain( kCalUnitX, kCalDomainFrequency );
            SetRelation( kCalChnRelMul );
            SetChannel( 0, a );
            SetChannel( 1, b );
            SetExpo( 0, 0 );
            SetExpo( 1, 0 );
            SetBW( kCalUnitY, bw );
            return true;
        }
        // cross correlation
        else if ( strcasecmp( graphtype, kPTCrossCorrelation ) == 0 )
        {
            SetValid( );
            SetDomain( kCalUnitX, kCalDomainFrequency );
            SetRelation( kCalChnRelMul );
            SetChannel( 0, a );
            SetChannel( 1, b );
            SetDensity( kCalUnitY, kCalDensityPwrPerHz );
            SetExpo( 0, 1 );
            SetExpo( 1, 1 );
            SetConj( 1, true );
            SetBW( kCalUnitY, bw );
            return true;
        }
        // transfer function
        else if ( strcasecmp( graphtype, kPTTransferFunction ) == 0 )
        {
            SetValid( );
            SetDomain( kCalUnitX, kCalDomainFrequency );
            SetRelation( kCalChnRelMul );
            SetChannel( 0, a );
            SetChannel( 1, b );
            SetExpo( 0, -1 );
            return true;
        }
        // coherence function
        else if ( strcasecmp( graphtype, kPTCoherenceFunction ) == 0 )
        {
            SetValid( );
            SetDomain( kCalUnitX, kCalDomainFrequency );
            SetRelation( kCalChnRelMul );
            SetChannel( 0, a );
            SetChannel( 1, b );
            SetExpo( 0, 0 );
            SetExpo( 1, 0 );
            return true;
        }
        // transfer coefficients
        else if ( strcasecmp( graphtype, kPTTransferCoefficients ) == 0 )
        {
            SetValid( );
            SetDomain( kCalUnitX, kCalDomainFrequency );
            SetChannel( 0, a );
            return true;
        }
        // transfer matrix
        else if ( ( strncasecmp( graphtype,
                                 kPTTransferCoefficients,
                                 strlen( kPTTransferCoefficients ) ) == 0 ) &&
                  ( strstr( graphtype, "matrix" ) != 0 ) )
        {
            SetValid( );
            SetDomain( kCalUnitX, kCalDomainFrequency );
            SetChannel( 0, a );
            SetExpo( 0, 0 );
            return true;
        }
        // coherence coefficients
        else if ( strcasecmp( graphtype, kPTCoherenceCoefficients ) == 0 )
        {
            SetValid( );
            SetDomain( kCalUnitX, kCalDomainFrequency );
            SetChannel( 0, a );
            SetExpo( 0, 0 );
            return true;
        }
        // harmonic coefficients
        else if ( strcasecmp( graphtype, kPTHarmonicCoefficients ) == 0 )
        {
            SetValid( );
            SetDomain( kCalUnitX, kCalDomainFrequency );
            SetChannel( 0, a );
            return true;
        }
        // intermodulation coefficients
        else if ( strcasecmp( graphtype, kPTIntermodulationCoefficients ) == 0 )
        {
            SetValid( );
            SetDomain( kCalUnitX, kCalDomainFrequency );
            SetChannel( 0, a );
            return true;
        }
        // XY values
        else if ( strcasecmp( graphtype, kPTXY ) == 0 )
        {
            SetValid( );
            SetDomain( kCalUnitX, kCalDomainValue );
            SetDomain( kCalUnitY, kCalDomainValue );
            SetRelation( kCalChnRelXY );
            SetChannel( 0, a );
            SetChannel( 1, b );
            return true;
        }
        else
        {
            return false;
        }
    }

    //______________________________________________________________________________
    const Unit*
    Descriptor::FindUnit( ECalUnit unit, const char* name ) const
    {
        if ( ( unit != kCalUnitX ) && ( unit != kCalUnitY ) )
        {
            return 0;
        }
        int         u = (int)unit;
        const Unit* ud = fUnits[ u ].Find( name );
        return ud ? ud
                  : ( u == 0 ? &UnitScaling::UnitIdentityX( )
                             : &UnitScaling::UnitIdentityY( ) );
    }

} // namespace calibration
