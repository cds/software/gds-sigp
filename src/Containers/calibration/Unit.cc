/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef __EXTENSIONS__
#define __EXTENSIONS__
#endif

#include "Unit.hh"
#include <cmath>
#include <cstring>

using namespace std;

namespace calibration
{

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // Unit                                                   		//
    //                                                                      //
    // Descriptor for a unit and its correction                             //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////

    const Unit Unit::kNone( "" );

    //______________________________________________________________________________
    double
    Unit::Factor( EUnitMagnitude mag )
    {
        return pow( 10, -(double)mag );
    }

    //______________________________________________________________________________
    string
    Unit::Mag( EUnitMagnitude mag )
    {
        switch ( mag )
        {
        case -15:
            return "f";
        case -12:
            return "p";
        case -9:
            return "n";
        case -6:
            return "\\mu";
        case -3:
            return "m";
        case 0:
        default:
            return "";
        case 3:
            return "k";
        case 6:
            return "M";
        case 9:
            return "G";
        case 12:
            return "T";
        case 15:
            return "P";
        }
    }

    //______________________________________________________________________________
    const Unit&
    Unit::UnitNone( )
    {
        return kNone;
    }

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // UnitScaling                                                          //
    //                                                                      //
    // Descriptor for a scaling unit                                        //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////

    const UnitScaling UnitScaling::kIdentityX( "", 1, 0, false );

    const UnitScaling UnitScaling::kIdentityY( "", 1, 0, true );

    const UnitScaling UnitScaling::kRadPerSec( "rad/s", 6.283185308, 0, false );

    const UnitScaling UnitScaling::kHertz( "Hz", 1, 0, false );

    const UnitScaling UnitScaling::kSec( "s", 1, 0, false );

    const UnitScaling UnitScaling::kMin( "m", 1. / 60., 0, false );

    const UnitScaling UnitScaling::kHour( "h", 1. / 3600., 0, false );

    const UnitScaling
        UnitScaling::kDay( "day", 1. / ( 24. * 3600. ), 0, false );

    //______________________________________________________________________________
    bool
    UnitScaling::Apply(
        float* x, float* y, int N, EUnitMagnitude mag, bool complex ) const
    {
        float* z = fY ? y : x;
        float  c = Factor( mag );
        if ( z == 0 )
        {
            return false;
        }
        for ( int i = 0; i < N; i++ )
        {
            if ( !complex )
            {
                z[ i ] = c * fSlope * ( z[ i ] - fOffset );
            }
            else
            {
                z[ 2 * i ] = c * fSlope * ( z[ 2 * i ] - fOffset );
                z[ 2 * i + 1 ] = c * fSlope * z[ 2 * i + 1 ];
            }
        }
        return true;
    }

    //______________________________________________________________________________
    const UnitScaling&
    UnitScaling::UnitIdentityX( )
    {
        return kIdentityX;
    }

    //______________________________________________________________________________
    const UnitScaling&
    UnitScaling::UnitIdentityY( )
    {
        return kIdentityY;
    }

    //______________________________________________________________________________
    const UnitScaling&
    UnitScaling::UnitRadPerSec( )
    {
        return kRadPerSec;
    }

    //______________________________________________________________________________
    const UnitScaling&
    UnitScaling::UnitHertz( )
    {
        return kHertz;
    }

    //______________________________________________________________________________
    const UnitScaling&
    UnitScaling::UnitSec( )
    {
        return kSec;
    }

    //______________________________________________________________________________
    const UnitScaling&
    UnitScaling::UnitMin( )
    {
        return kMin;
    }

    //______________________________________________________________________________
    const UnitScaling&
    UnitScaling::UnitHour( )
    {
        return kHour;
    }

    //______________________________________________________________________________
    const UnitScaling&
    UnitScaling::UnitDay( )
    {
        return kDay;
    }

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // UnitList                                                             //
    //                                                                      //
    // Descriptor for a unit and its correction                             //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    bool
    UnitList::Allocate( int size )
    {
        if ( fList == 0 )
            fSize = 0;
        if ( size == 0 )
        {
            if ( fList )
            {
                Clear( );
                //free (fList);
                delete[] fList;
                fList = 0;
            }
            fSize = 0;
            return true;
        }
        if ( ( size < fLen + 1 ) && fList )
        {
            for ( int i = size; i < fLen; i++ )
            {
                delete fList[ i ];
                fList[ i ] = 0;
            }
            fLen = size;
        }
        if ( fList == 0 )
        {
            //fList = (Unit**) malloc (size * sizeof (Unit*));
            fList = new Unit*[ size ];
            fLen = 0;
            if ( fList )
            {
                memset( fList, 0, size * sizeof( Unit* ) );
                fSize = size;
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            // Unit** l = (Unit**) realloc (fList, size * sizeof (Unit*));
            Unit** l = new Unit*[ size ];
            if ( l )
            {
                memcpy( l, fList, fLen * sizeof( Unit* ) );
                delete[] fList;
                fList = l;

                fSize = size;
                memset( fList + fLen, 0, ( fSize - fLen ) * sizeof( Unit* ) );
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    //______________________________________________________________________________
    bool
    UnitList::Add( Unit* unit )
    {
        if ( !unit )
        {
            return false;
        }
        if ( !fList )
        {
            delete unit;
            return false;
        }
        if ( fLen + 1 >= fSize )
        {
            if ( !Allocate( fSize + 100 ) )
            {
                delete unit;
                return false;
            }
        }
        if ( fLen == 0 )
        {
            fList[ 0 ] = unit;
            fLen = 1;
            return true;
        }
        if ( strcmp( unit->GetName( ), fList[ 0 ]->GetName( ) ) < 0 )
        {
            memmove( fList + 1, fList, fLen * sizeof( Unit* ) );
            fList[ 0 ] = unit;
            fLen++;
            return true;
        }
        int l = 0;
        int r = fLen;
        while ( l < r - 1 )
        {
            int m = ( l + r ) / 2;
            if ( strcmp( unit->GetName( ), fList[ m ]->GetName( ) ) < 0 )
            {
                r = m;
            }
            else
            {
                l = m;
            }
        }
        if ( r < fLen )
        {
            memmove( fList + r + 1, fList + r, ( fLen - r ) * sizeof( Unit* ) );
        }
        fList[ r ] = unit;
        fLen++;
        return true;
    }

    //______________________________________________________________________________
    const Unit*
    UnitList::Find( const char* unit ) const
    {
        if ( !unit || !fList || ( fLen == 0 ) )
        {
            return 0;
        }
        if ( strcmp( unit, fList[ 0 ]->GetName( ) ) < 0 )
        {
            return 0;
        }
        int l = 0;
        int r = fLen;
        while ( l < r - 1 )
        {
            int m = ( l + r ) / 2;
            if ( strcmp( unit, fList[ m ]->GetName( ) ) < 0 )
            {
                r = m;
            }
            else
            {
                l = m;
            }
        }
        if ( strcmp( unit, fList[ l ]->GetName( ) ) == 0 )
        {
            return fList[ l ];
        }
        else
        {
            return 0;
        }
    }

    //______________________________________________________________________________
    void
    UnitList::Clear( )
    {
        for ( int i = 0; i < fLen; i++ )
        {
            delete fList[ i ];
            fList[ i ] = 0;
        }
        fLen = 0;
        if ( fSize > 200 )
        {
            Allocate( 200 );
        }
    }

    //______________________________________________________________________________
    UnitList::UnitList( const UnitList& list )
        : fList( 0 ), fLen( 0 ), fSize( 0 )
    {
        *this = list;
    }

    //______________________________________________________________________________
    UnitList&
    UnitList::operator=( const UnitList& list )
    {
        if ( this != &list )
        {
            Clear( );
            Allocate( list.fSize );
            if ( fList )
            {
                memcpy( fList, list.fList, fSize * sizeof( Unit* ) );
                fLen = list.fLen;
                memset(
                    ( (UnitList*)&list )->fList, 0, fSize * sizeof( Unit* ) );
                ( (UnitList*)&list )->fLen = 0;
            }
        }
        return *this;
    }

} // namespace calibration
