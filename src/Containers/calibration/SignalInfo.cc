#include "SignalInfo.hh"
#include "calutil.h"
#include <strings.h>

namespace calibration
{

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // SignalInfo                                                           //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////

    //______________________________________________________________________________
    SignalInfo::SignalInfo( )
    {
        fChnRelation = kCalChnRelSingle;
        fCoeff[ 0 ] = 1.0;
        fCoeff[ 1 ] = 1.0;
        fExpo[ 0 ] = 1;
        fExpo[ 1 ] = 1;
        fConj[ 0 ] = 0;
        fConj[ 1 ] = 0;
        fDomain = kCalDomainValue;
        fDensityUnits = kCalDensityNo;
        fBW = 0.0;
    }

    //______________________________________________________________________________
    SignalInfo::SignalInfo( const calsignalinfo_t& cal )
    {
        fChnRelation = cal.fChnRelation;
        fCoeff[ 0 ] = cal.fCoeff[ 0 ];
        fCoeff[ 1 ] = cal.fCoeff[ 1 ];
        fExpo[ 0 ] = cal.fExpo[ 0 ];
        fExpo[ 1 ] = cal.fExpo[ 1 ];
        fConj[ 0 ] = cal.fConj[ 0 ];
        fConj[ 1 ] = cal.fConj[ 1 ];
        fDomain = cal.fDomain;
        fDensityUnits = cal.fDensityUnits;
        fBW = cal.fBW;
    }

} // namespace calibration
