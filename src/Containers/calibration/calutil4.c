/* enable definition of M_PI in <math.h> */
#define _DEFAULT_SOURCE
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "calutil.h"

extern void cal_interpolate_cmplx( int          flag,
                                   double       f,
                                   const float* tf,
                                   int          len,
                                   double*      cmag,
                                   double*      cphase );

/* #define PI		3.1415926535897932384626433832795029 */
#define PI M_PI
#define TWO_PI ( 2 * PI )

#define PRM_CONV( i ) ( 2 * ( i ) )
#define PRM_OFFS( i ) ( 2 * ( i ) + 1 )
#define PRM_D( i ) ( 2 * ( i ) )
#define PRM_DENSITY 1
#define PRM_DENSITYRES 3

#define power( x, y ) ( exp( (y)*log( x ) ) )

/* List of atomic units */
const char* const atomicUnits[] = { "s",   "m",   "Hz",  "#",      "N",   "Pa",
                                    "l",   "W",   "C",   "V",      "A",   "T",
                                    "Ohm", "rad", "deg", "cycles", "bar", "K" };

/* List of related units */
struct related_t
{
    const char* unit;
    double      fac;
    double      ofs;
};
typedef struct related_t related_t;

static const related_t unitC = { "C", 1, 0 };
static const related_t unitK = { "K", 1, -273.15 };
static const related_t unitK2 = { "K", 1, 0 };
static const related_t unitC2 = { "C", 1, 273.15 };
static const related_t unitF3 = { "F", 1, 0 };
static const related_t unitC3 = { "C", 1. / 1.8, 32 };
static const related_t unitK3 = { "K", 1. / 1.8, 32 - 1.8 * 273.15 };
static const related_t unitrad = { "rad", 1, 0 };
static const related_t unitdeg = { "deg", 180. / PI, 0 };
static const related_t unitcyc = { "cycles", 1. / TWO_PI, 0 };
static const related_t unitPa = { "Pa", 1, 0 };
static const related_t unitmBar = { "bar", 0.00001, 0 };
static const related_t unitHz = { "Hz", 1, 0 };
static const related_t unitrads = { "rad/s", TWO_PI, 0 };
static const related_t unitms = { "m/s", 1, 0 };
static const related_t unitkmh = { "m/h", 3600, 0 };
static const related_t unitcyc2 = { "cycles", 1, 0 };
static const related_t unitrad2 = { "rad", TWO_PI, 0 };
static const related_t unitrads2 = { "rad/s", 1, 0 };
static const related_t unitHz2 = { "Hz", 1. / TWO_PI, 0 };

static const related_t* const temp[] = { &unitC, &unitK, 0 };
static const related_t* const temp2[] = { &unitK2, &unitC2, 0 };
static const related_t* const temp3[] = { &unitF3, &unitC3, &unitK3, 0 };
static const related_t* const angle[] = { &unitrad, &unitcyc, &unitdeg, 0 };
static const related_t* const cycle[] = { &unitcyc2, &unitrad2, 0 };
static const related_t* const press[] = { &unitPa, &unitmBar, 0 };
static const related_t* const speed[] = { &unitms, &unitkmh, 0 };
static const related_t* const freq[] = { &unitHz, &unitrads, 0 };
static const related_t* const rads[] = { &unitrads2, &unitHz2, 0 };
static const related_t* const* const related[] = { temp,  temp2, temp3, angle,
                                                   cycle, press, speed, freq,
                                                   rads,  0 };

/* List of derivatives */
struct derive_t
{
    const char* unit;
    int         D;
};
typedef struct derive_t derive_t;

static const derive_t der_m_m = { "m", 0 };
static const derive_t der_m_ms = { "m/s", 1 };
static const derive_t der_m_mss = { "m/s^{2}", 2 };
static const derive_t der_ms_ms = { "m/s", 0 };
static const derive_t der_ms_m = { "m", -1 };
static const derive_t der_ms_mss = { "m/s^{2}", 1 };
static const derive_t der_mss_mss = { "m/s^{2}", 0 };
static const derive_t der_mss_m = { "m", -2 };
static const derive_t der_mss_ms = { "m/s", -1 };
static const derive_t der_Hz_Hz = { "Hz", 0 };
static const derive_t der_Hz_cyc = { "cycles", -1 };
static const derive_t der_cyc_cyc = { "cycles", 0 };
static const derive_t der_cyc_Hz = { "Hz", 1 };
static const derive_t der_rad_rad = { "rad", 0 };
static const derive_t der_rad_rads = { "rad/s", 1 };
static const derive_t der_rads_rads = { "rad/s", 0 };
static const derive_t der_rads_rad = { "rad", -1 };

static const derive_t* const der_m[] = { &der_m_m, &der_m_ms, &der_m_mss, 0 };
static const derive_t* const der_ms[] = {
    &der_ms_ms, &der_ms_m, &der_ms_mss, 0
};
static const derive_t* const der_mss[] = {
    &der_mss_mss, &der_mss_m, &der_mss_ms, 0
};
static const derive_t* const der_Hz[] = { &der_Hz_Hz, &der_Hz_cyc, 0 };
static const derive_t* const der_cyc[] = { &der_cyc_cyc, &der_cyc_Hz, 0 };
static const derive_t* const der_rad[] = { &der_rad_rad, &der_rad_rads, 0 };
static const derive_t* const der_rads[] = { &der_rads_rads, &der_rads_rad, 0 };
static const derive_t* const* const derived[] = { der_m,    der_ms,  der_mss,
                                                  der_Hz,   der_cyc, der_rad,
                                                  der_rads, 0 };

/* Table of density unit conversion ids:
      first digit:  0 - 1, 1 - sqr,  2 - sqrt;
      second digit: 0 - 1, 1 - 1/BW, 2 - 1/rtBW, 3 - rtBW, 4 - BW */
const int densconv[ 5 ][ 5 ] = { { 0, 0, 0, 0, 0 },
                                 { 0, 0, 10, 3, 14 },
                                 { 0, 20, 0, 23, 4 },
                                 { 0, 2, 11, 0, 10 },
                                 { 0, 22, 1, 20, 0 } };

static char*
strend( const char* p )
{
    return (char*)( p + strlen( p ) );
}

static double
cmplxAbs( const float* z )
{
    return hypot( z[ 0 ], z[ 1 ] );
}

static double
cmplxArg( const float* z )
{
    return atan2( z[ 1 ], z[ 0 ] );
}

static void
cmplxConj( float* zz, const float* z1 )
{
    zz[ 0 ] = z1[ 0 ];
    zz[ 1 ] = -z1[ 1 ];
}

static void
cmplxMul( float* zz, const float* z1, const float* z2 )
{
    double z[ 2 ];
    z[ 0 ] = (double)z1[ 0 ] * z2[ 0 ] - (double)z1[ 1 ] * z2[ 1 ];
    z[ 1 ] = (double)z1[ 0 ] * z2[ 1 ] + (double)z1[ 1 ] * z2[ 0 ];
    zz[ 0 ] = z[ 0 ];
    zz[ 1 ] = z[ 1 ];
}

static void
cmplxDiv( float* zz, const float* z1, const float* z2 )
{
    double z[ 2 ];
    double norm = (double)z2[ 0 ] * z2[ 0 ] + (double)z2[ 1 ] * z2[ 1 ];
    z[ 0 ] = ( (double)z1[ 0 ] * z2[ 0 ] + (double)z1[ 1 ] * z2[ 1 ] ) / norm;
    z[ 1 ] = ( (double)z1[ 1 ] * z2[ 0 ] - (double)z1[ 0 ] * z2[ 1 ] ) / norm;
    zz[ 0 ] = z[ 0 ];
    zz[ 1 ] = z[ 1 ];
}

static void
cmplxSqrt( float* zz, const float* z1 )
{
    double r = cmplxAbs( z1 );
    double arg = cmplxArg( z1 );
    zz[ 0 ] = sqrt( r ) * cos( arg / 2. );
    zz[ 1 ] = sqrt( r ) * sin( arg / 2. );
}

static void
cmplxPower( float* zz, const float* z1, float expo )
{
    double n;
    double arg;
    if ( expo == 0 )
    {
        zz[ 0 ] = 1;
        zz[ 1 ] = 0;
    }
    else if ( ( z1[ 0 ] == 0 ) && ( z1[ 1 ] == 0 ) )
    {
        zz[ 0 ] = 0;
        zz[ 1 ] = 0;
    }
    else
    {
        n = power( cmplxAbs( z1 ), expo );
        arg = cmplxArg( z1 );
        zz[ 0 ] = n * cos( expo * arg );
        zz[ 1 ] = n * sin( expo * arg );
    }
}

static int
densityConv( ECalDensity in, ECalDensity out )
{
    return densconv[ (int)in ][ (int)out ];
}

static void
DfacCmplx( float* z, float f, int D )
{
    if ( D == 0 )
    {
        z[ 0 ] = 1;
        z[ 1 ] = 0;
    }
    else
    {
        switch ( ( D + 4444 ) % 4 )
        {
        case 0:
            z[ 0 ] = pow( TWO_PI * f, D );
            z[ 1 ] = 0;
            break;
        case 1:
            z[ 0 ] = 0;
            z[ 1 ] = pow( TWO_PI * f, D );
            break;
        case 2:
            z[ 0 ] = -pow( TWO_PI * f, D );
            z[ 1 ] = 0;
            break;
        case 3:
            z[ 0 ] = 0;
            z[ 1 ] = -pow( TWO_PI * f, D );
            break;
        }
    }
}

static void
pole( float* c, const float* p, float f )
{
    if ( ( p[ 0 ] == 0 ) && ( p[ 1 ] == 0 ) )
    {
        c[ 0 ] = 0;
        c[ 1 ] = -1. / f;
    }
    else
    {
        float nom[ 2 ];
        float denom[ 2 ];
        nom[ 0 ] = p[ 0 ];
        nom[ 1 ] = p[ 1 ];
        denom[ 0 ] = p[ 0 ];
        denom[ 1 ] = f + p[ 1 ];
        cmplxDiv( c, nom, denom );
    }
}

static void
zero( float* c, const float* z, float f )
{
    if ( ( z[ 0 ] == 0 ) && ( z[ 1 ] == 0 ) )
    {
        c[ 0 ] = 0;
        c[ 1 ] = f;
    }
    else
    {
        float nom[ 2 ];
        float denom[ 2 ];
        nom[ 0 ] = z[ 0 ];
        nom[ 1 ] = f + z[ 1 ];
        denom[ 0 ] = z[ 0 ];
        denom[ 1 ] = z[ 1 ];
        cmplxDiv( c, nom, denom );
    }
}

static void
calcFreqCorr(
    float* c, float f, const calrec_t* cal, int expo, double conv, int D )
{
    int   i;
    float z[ 2 ];

    if ( expo == 0 )
    {
        c[ 0 ] = 1;
        c[ 1 ] = 0;
        return;
    }

    DfacCmplx( c, f, D );

    /* pole zero transfer function */
    if ( ( calgettype( cal ) & CALPOLEZERO ) != 0 )
    {
        double       gain;
        int          pnum = 0;
        int          znum = 0;
        const float* pzs;
        if ( calgetpolezeros( cal, &gain, &pnum, &znum, &pzs ) )
        {
            /*if (f < 1)
               printf ("c(%g) = %g + i %g\n", f, c[0], c[1]);*/
            for ( i = 0; i < pnum; i++ )
            {
                pole( z, pzs + 4 * i, f );
                cmplxMul( c, c, z );
                /* if (f < 1)
                  printf ("  c(%g) = %g + i %g   --  pole %g %g \n", 
                         f, c[0], c[1], pzs[4*i], pzs[4*i+1]);*/
            }
            for ( i = 0; i < znum; i++ )
            {
                zero( z, pzs + 4 * i + 2, f );
                cmplxMul( c, c, z );
                /*if (f < 1)
                  printf ("  c(%g) = %g + i %g   --  zero %g %g \n", 
                         f, c[0], c[1], pzs[4*i+2], pzs[4*i+3]);*/
            }
            c[ 0 ] *= gain;
            c[ 1 ] *= gain;
            /*if (f < 1)
               printf ("  c(%g) = %g + i %g\n", f, c[0], c[1]);*/
        }
    }

    /* interpolating transfer function */
    else if ( ( calgettype( cal ) & CALTRANSFERFUNCTION ) != 0 )
    {
        const float* trans;
        double       cmag;
        double       cphase;
        int          len = calgettransferfunction( cal, &trans );
        if ( len > 0 )
        {
            cal_interpolate_cmplx( 1, f, trans, len, &cmag, &cphase );
            z[ 0 ] = cmag * cos( cphase );
            z[ 1 ] = cmag * sin( cphase );
            cmplxMul( c, c, z );
        }
    }

    /* simple linear conversion */
    else
    {
        c[ 0 ] *= calgetconversion( cal );
        c[ 1 ] *= calgetconversion( cal );
    }

    /* multiply by conversion factor and set exponent */
    c[ 0 ] *= conv;
    c[ 1 ] *= conv;
    cmplxPower( c, c, expo );
}

static int
unitAtomic( const char* u )

{
    const char* const* p = atomicUnits;

    /* one letter or smaller is atomic */
    if ( strlen( u ) <= 1 )
    {
        return 1;
    }
    /* go through list of atomic units */
    while ( *p != 0 )
    {
        if ( strcmp( *p, u ) == 0 )
        {
            return 1;
        }
        p++;
    }
    /* now check brackets */
    if ( u[ 0 ] == '(' )
    {
        const char* p = u + 1;
        int         level = 1;
        while ( *p != 0 )
        {
            if ( level == 0 )
            {
                return 0;
            }
            if ( *p == '(' )
                level++;
            if ( *p == ')' )
                level--;
            p++;
        }
        return 1;
    }
    return 0;
}

static int
unitExpo( const char* u, int* pos )
{
    int  e;
    int  p = strlen( u ) - 1;
    char pp[ 50 ];

    if ( p <= 0 )
    {
        return 0;
    }
    if ( u[ p ] != '}' )
    {
        return 0;
    }
    p--;
    while ( ( p >= 0 ) &&
            ( ( ( u[ p ] >= '0' ) && ( u[ p ] < '9' ) ) || ( u[ p ] == '-' ) ) )
    {
        p--;
    }
    e = atoi( u + p + 1 );
    if ( ( p < 0 ) || ( u[ p ] != '{' ) )
    {
        return 0;
    }
    p--;
    if ( ( p < 0 ) || ( u[ p ] != '^' ) )
    {
        return 0;
    }
    if ( p > 48 )
    {
        return 0;
    }
    strncpy( pp, u, p );
    pp[ p ] = 0;
    if ( !unitAtomic( pp ) )
    {
        return 0;
    }
    if ( pos )
        *pos = p;
    return e;
}

static int
caltrans( const caldeduced_t* ded,
          int                 flag,
          float*              x,
          float*              y,
          int                 N,
          int                 mag,
          int                 cmplx )
{
    int i; /* index */

    if ( ( ded == 0 ) || ( y == 0 ) )
    {
        return 0;
    }
    /* single channel */
    if ( ded->fInfo->fChnRelation == kCalChnRelSingle )
    {

        /* time domain or value */
        if ( ded->fInfo->fDomain != kCalDomainFrequency )
        {
            /* magnitude qualifier */
            double mc = pow( 10, -mag );
            /* factor / exponent */
            double a = ded->fInfo->fCoeff[ 0 ];
            int    b = ded->fInfo->fExpo[ 0 ];
            /* cal correction */
            double conv = calgetconversion( ded->fChn1 );
            double ofs = calgetoffset( ded->fChn1 );
            /* related unit correction */
            if ( conv != 0 )
                ofs += ded->fParam[ PRM_OFFS( 0 ) ] / conv;
            conv *= ded->fParam[ PRM_CONV( 0 ) ];
            if ( cmplx )
                ofs = 0;

            /* no exponent */
            if ( b == 0 )
            {
                for ( i = 0; i < ( cmplx ? 2 * N : N ); i++ )
                {
                    y[ i ] = mc * y[ i ];
                }
            }
            /* exponent 1 */
            else if ( b == 1 )
            {
                conv *= mc;
                for ( i = 0; i < ( cmplx ? 2 * N : N ); i++ )
                {
                    y[ i ] = conv * ( y[ i ] - a * ofs );
                }
            }
            /* exponent -1 */
            else if ( b == -1 )
            {
                conv /= mc;
                if ( !cmplx )
                {
                    for ( i = 0; i < N; i++ )
                    {
                        y[ i ] = ( y[ i ] == 0 )
                            ? 0
                            : 1. / ( conv * ( 1 / y[ i ] - ofs / a ) );
                    }
                }
                else
                {
                    for ( i = 0; i < 2 * N; i++ )
                    {
                        y[ i ] /= conv;
                    }
                }
            }
            /* other exponents */
            else
            {
                if ( !cmplx && ( ofs != 0 ) )
                {
                    for ( i = 0; i < N; i++ )
                    {
                        y[ i ] = a * mc * pow( conv, b ) *
                            power( ( power( y[ i ] / a, 1. / b ) - ofs / a ),
                                   b );
                    }
                }
                else
                {
                    for ( i = 0; i < ( cmplx ? 2 * N : N ); i++ )
                    {
                        y[ i ] *= mc * pow( conv, b );
                    }
                }
            }
        }

        /* frequency domain */
        else
        {
            double mc = pow( 10, -mag );
            double f;
            float  c[ 2 ];
            /*
            printf ("correct freq. domain %s (%s)\n", 
                   calgetchannel (ded->fChn1), ded->fUnit);
            printf ("expo %i conv %g D %i\n", ded->fInfo->fExpo[0],
                   ded->fParam[PRM_CONV(0)], ded->fiParam[PRM_D(0)]);
            printf ("cal sup %x  flag %i\n", (calgettype (ded->fChn1)), flag);*/
            for ( i = 0; i < N; i++ )
            {
                f = ( ( flag == 0 ) ? x[ i ] : x[ 0 ] + i * x[ 1 ] );
                calcFreqCorr( c,
                              f,
                              ded->fChn1,
                              ded->fInfo->fExpo[ 0 ],
                              ded->fParam[ PRM_CONV( 0 ) ],
                              ded->fiParam[ PRM_D( 0 ) ] );
                if ( ded->fInfo->fConj[ 0 ] )
                    cmplxConj( c, c );
                c[ 0 ] *= mc;
                c[ 1 ] *= mc;
                if ( !cmplx )
                {
                    y[ i ] *= cmplxAbs( c );
                }
                else
                {
                    cmplxMul( y + 2 * i, y + 2 * i, c );
                }
            }
        }
    }

    /* two channel additive */
    else if ( ded->fInfo->fChnRelation == kCalChnRelAdd )
    {
        /* magnitude qualifier */
        double mc = pow( 10, -mag );
        double ofs = 0;
        double conv = 1;
        double conv1 = 1;
        double conv2 = 1;
        /* only support A + B in time domain */
        if ( ( ded->fInfo->fDomain == kCalDomainFrequency ) ||
             ( ded->fInfo->fExpo[ 0 ] != 1 ) ||
             ( ded->fInfo->fExpo[ 1 ] != 1 ) )
        {
            return 0;
        }
        /* conversion factors have to be identical */
        conv1 = calgetconversion( ded->fChn1 ) * ded->fParam[ PRM_CONV( 0 ) ];
        conv2 = calgetconversion( ded->fChn2 ) * ded->fParam[ PRM_CONV( 1 ) ];
        if ( ( conv2 - conv1 ) > 1E-4 * ( conv1 + conv2 ) )
        {
            return 0;
        }
        ofs = ded->fInfo->fCoeff[ 0 ] * calgetoffset( ded->fChn1 ) +
            ded->fInfo->fCoeff[ 1 ] * calgetoffset( ded->fChn2 );
        if ( cmplx )
            ofs = 0;
        conv = conv1 * mc;
        for ( i = 0; i < cmplx ? 2 * N : N; i++ )
        {
            y[ i ] = conv * ( y[ i ] - ofs );
        }
    }

    /* two channel multiplicative */
    else
    {
        /* time domain or value */
        if ( ded->fInfo->fDomain != kCalDomainFrequency )
        {
            double mc = pow( 10, -mag );
            float  c1;
            float  c2;
            if ( ( ded->fInfo->fExpo[ 0 ] != 0 ) ||
                 ( ded->fInfo->fExpo[ 1 ] != 0 ) )
            {
                c1 = ( ded->fInfo->fExpo[ 0 ] == 0 )
                    ? 1
                    : pow( calgetconversion( ded->fChn1 ) *
                               ded->fParam[ PRM_CONV( 0 ) ],
                           ded->fInfo->fExpo[ 0 ] );
                c2 = ( ded->fInfo->fExpo[ 1 ] == 0 )
                    ? 1
                    : pow( calgetconversion( ded->fChn1 ) *
                               ded->fParam[ PRM_CONV( 1 ) ],
                           ded->fInfo->fExpo[ 1 ] );
                c1 *= c2 * mc;
                for ( i = 0; i < cmplx ? 2 * N : N; i++ )
                {
                    y[ i ] *= c1;
                }
            }
        }

        /* frequency domain */
        else
        {
            /* magnitude qualifier */
            double mc = pow( 10, -mag );
            double f;
            float  c1[ 2 ];
            float  c2[ 2 ];
            if ( ( ded->fInfo->fExpo[ 0 ] != 0 ) ||
                 ( ded->fInfo->fExpo[ 1 ] != 0 ) )
            {
                for ( i = 0; i < N; i++ )
                {
                    f = ( ( flag == 0 ) ? x[ i ] : x[ 0 ] + i * x[ 1 ] );
                    calcFreqCorr( c1,
                                  f,
                                  ded->fChn1,
                                  ded->fInfo->fExpo[ 0 ],
                                  ded->fParam[ PRM_CONV( 0 ) ],
                                  ded->fiParam[ PRM_D( 0 ) ] );
                    calcFreqCorr( c2,
                                  f,
                                  ded->fChn2,
                                  ded->fInfo->fExpo[ 1 ],
                                  ded->fParam[ PRM_CONV( 1 ) ],
                                  ded->fiParam[ PRM_D( 1 ) ] );
                    /*if ((f > 59.9) && (f <= 60.1)) {
                     printf ("  c1(%g) = %g + i %g\n", f, c1[0], c1[1]);
                     printf ("  c2(%g) = %g + i %g\n", f, c2[0], c2[1]);
                  }*/
                    if ( ded->fInfo->fConj[ 0 ] )
                        cmplxConj( c1, c1 );
                    if ( ded->fInfo->fConj[ 1 ] )
                        cmplxConj( c2, c2 );
                    cmplxMul( c1, c1, c2 );
                    c1[ 0 ] *= mc;
                    c1[ 1 ] *= mc;
                    /*if ((f > 9.5) && (f <= 10)) {
                     printf ("  conj(%g) = %g + i %g\n", f, c1[0], c1[1]);
                  }*/
                    if ( !cmplx )
                    {
                        y[ i ] *= cmplxAbs( c1 );
                    }
                    else
                    {
                        /*if ((f > 59.9) && (f <= 60.1)) {
                        printf ("  y (%g) = %g + i %g\n", f, y[2*i], y[2*i+1]);
                     }*/
                        cmplxMul( y + 2 * i, y + 2 * i, c1 );
                        /*if ((f > 59.9) && (f <= 60.1)) {
                        printf ("  y (%g) = %g + i %g\n", f, y[2*i], y[2*i+1]);
                     }*/
                    }
                }
            }
        }
    }

    /* shift time if necessary */
    if ( ( ded->fInfo->fDomain == kCalDomainTime ) &&
         ( ( calgettype( ded->fChn1 ) & CALTIMEDELAY ) != 0 ) &&
         ( ( ded->fInfo->fChnRelation == kCalChnRelSingle ) ||
           ( ( calgettype( ded->fChn2 ) & CALTIMEDELAY ) != 0 ) ) )
    {
        double dt = calgettimedelay( ded->fChn1 );
        if ( ded->fInfo->fChnRelation != kCalChnRelSingle )
        {
            double dt2 = calgettimedelay( ded->fChn1 );
            if ( dt2 - dt > 1E-4 * ( dt + dt2 ) )
            {
                dt = 0;
            }
        }
        if ( flag == 0 )
        {
            for ( i = 0; i < N; i++ )
            {
                x[ i ] -= dt;
            }
        }
        else
        {
            x[ 0 ] -= dt;
        }
    }

    /* Adjust density units if necessary */
    if ( ( ded->fInfo->fDomain == kCalDomainFrequency ) &&
         ( ded->fiParam[ PRM_DENSITY ] != 0 ) && ( ded->fInfo->fBW > 0 ) )
    {
        float bwc;
        /*printf ("DENSITY UNIT ADJUST %i %g\n", 
                ded->fiParam[PRM_DENSITY], ded->fInfo->fBW);*/
        switch ( ded->fiParam[ PRM_DENSITY ] % 10 )
        {
        case 0:
        default:
            bwc = 1;
            break;
        case 1:
            bwc = 1 / ded->fInfo->fBW;
            break;
        case 2:
            bwc = 1 / sqrt( ded->fInfo->fBW );
            break;
        case 3:
            bwc = sqrt( ded->fInfo->fBW );
            break;
        case 4:
            bwc = ded->fInfo->fBW;
            break;
        }
        if ( !cmplx )
        {
            switch ( ded->fiParam[ PRM_DENSITY ] / 10 )
            {
            case 0:
                for ( i = 0; i < N; i++ )
                {
                    y[ i ] *= bwc;
                }
                break;
            case 1:
                for ( i = 0; i < N; i++ )
                {
                    y[ i ] = bwc * y[ i ] * y[ i ];
                }
                break;
            case 2:
                for ( i = 0; i < N; i++ )
                {
                    y[ i ] = bwc * sqrt( y[ i ] );
                }
                break;
            }
        }
        else
        {
            /* CALIBRATION CORRECTION FOR COMPLEX MAY CHANGE THE PHASE!!! */
            switch ( ded->fiParam[ PRM_DENSITY ] / 10 )
            {
            case 0:
                for ( i = 0; i < 2 * N; i++ )
                {
                    y[ i ] *= bwc;
                }
                break;
            case 1:
                for ( i = 0; i < N; i++ )
                {
                    cmplxMul( y + 2 * i, y + 2 * i, y + 2 * i );
                    y[ 2 * i ] *= bwc;
                    y[ 2 * i + 1 ] *= bwc;
                }
                break;
            case 2:
                for ( i = 0; i < N; i++ )
                {
                    cmplxSqrt( y + 2 * i, y + 2 * i );
                    y[ 2 * i ] *= bwc;
                    y[ 2 * i + 1 ] *= bwc;
                }
                break;
            }
        }
    }

    return 1;
}

void
calsiginfo_init( calsignalinfo_t* sig )
{
    if ( sig == 0 )
    {
        return;
    }
    sig->fChnRelation = kCalChnRelSingle;
    sig->fCoeff[ 0 ] = 1;
    sig->fCoeff[ 1 ] = 1;
    sig->fExpo[ 0 ] = 1;
    sig->fExpo[ 1 ] = 1;
    sig->fConj[ 0 ] = 0;
    sig->fConj[ 1 ] = 0;
    sig->fDomain = kCalDomainValue;
    sig->fDensityUnits = kCalDensityNo;
    sig->fBW = 0;
}

static int
cal_deduce1( int                    flag,
             const calsignalinfo_t* info,
             const calrec_t*        chn1,
             const calrec_t*        chn2,
             caldeduced_t*          ded,
             int                    max )
{
    caldeduced_t                   unit; /* unit info */
    int                            n = 0; /* number of deduced units so far */
    const calrec_t*                cal; /* cal ptr */
    int                            i; /* index */
    int                            j; /* index */
    int                            m; /* index */
    const related_t* const* const* rel; /* list of related units */
    const related_t* const*        r; /* related units */
    const derive_t* const* const*  der; /* list of derived units */
    const derive_t* const*         d; /* derived units */

    if ( max <= 0 )
    {
        return 0;
    }
    cal = chn1 ? chn1 : chn2;
    if ( !cal )
    {
        return 0;
    }

    /* check for valid calibration */
    i = chn1 ? 0 : 1;
    if ( info->fExpo[ i ] == 0 )
    {
        return 0;
    }

    /* init unit */
    strcpy( unit.fUnit, "" );
    unit.fPowerUnit = 0;
    unit.fInfo = info;
    unit.fChn1 = chn1;
    unit.fChn2 = chn2;
    unit.fTrans = caltrans;
    unit.fParam[ PRM_CONV( 0 ) ] = 1;
    unit.fParam[ PRM_CONV( 1 ) ] = 1;
    unit.fParam[ PRM_OFFS( 0 ) ] = 0;
    unit.fParam[ PRM_OFFS( 1 ) ] = 0;
    unit.fiParam[ PRM_D( 0 ) ] = 0;
    unit.fiParam[ PRM_D( 1 ) ] = 0;
    unit.fiParam[ PRM_DENSITY ] = 0;
    unit.fiParam[ PRM_DENSITYRES ] = 0;
    /* unit.fiParam[3] = 0;*/

    ded[ 0 ] = unit;
    strncpy( ded[ 0 ].fUnit, calgetunit( cal ), CALUNITSIZE );
    n++;

    /* go through list of related units */
    rel = related;
    while ( *rel != 0 )
    {
        if ( strcmp( ded[ 0 ].fUnit, ( *rel )[ 0 ]->unit ) == 0 )
        {
            r = ( *rel ) + 1;
            while ( ( n <= max ) && ( *r != 0 ) )
            {
                ded[ n ] = ded[ 0 ];
                strncpy( ded[ n ].fUnit, ( *r )->unit, CALUNITSIZE );
                ded[ n ].fParam[ PRM_CONV( i ) ] = ( *r )->fac;
                ded[ n ].fParam[ PRM_OFFS( i ) ] = ( *r )->ofs;
                n++;
                r++;
            }
        }
        rel++;
    }

    /* tackle derivatives */
    if ( info->fDomain == kCalDomainFrequency )
    {
        m = n;
        for ( j = 0; j < m; j++ )
        {
            der = derived;
            while ( *der != 0 )
            {
                if ( strcmp( ded[ j ].fUnit, ( *der )[ 0 ]->unit ) == 0 )
                {
                    d = ( *der ) + 1;
                    while ( ( n <= max ) && ( *d != 0 ) )
                    {
                        ded[ n ] = ded[ j ];
                        strncpy( ded[ n ].fUnit, ( *d )->unit, CALUNITSIZE );
                        ded[ n ].fUnit[ CALUNITSIZE - 1 ] = 0;
                        ded[ n ].fiParam[ PRM_D( i ) ] = ( *d )->D;
                        n++;
                        d++;
                    }
                }
                der++;
            }
        }
    }

    return n;
}

static void
unit_combine( caldeduced_t*          ded,
              const caldeduced_t*    ded1,
              const caldeduced_t*    ded2,
              const calsignalinfo_t* info,
              const calrec_t*        chn1,
              const calrec_t*        chn2 )
{
    ded->fInfo = info;
    ded->fChn1 = chn1;
    ded->fChn2 = chn2;
    ded->fTrans = caltrans;
    ded->fParam[ PRM_CONV( 0 ) ] = ded1->fParam[ PRM_CONV( 0 ) ];
    ded->fParam[ PRM_CONV( 1 ) ] = ded2->fParam[ PRM_CONV( 1 ) ];
    ded->fParam[ PRM_OFFS( 0 ) ] = ded1->fParam[ PRM_OFFS( 0 ) ];
    ded->fParam[ PRM_OFFS( 1 ) ] = ded2->fParam[ PRM_OFFS( 1 ) ];
    ded->fiParam[ PRM_D( 0 ) ] = ded1->fiParam[ PRM_D( 0 ) ];
    ded->fiParam[ PRM_D( 1 ) ] = ded2->fiParam[ PRM_D( 1 ) ];
    ded->fiParam[ PRM_DENSITY ] = 0;
    ded->fiParam[ PRM_DENSITYRES ] = 0;
    ded->fPowerUnit = 0;
    /*printf ("conv %g %g  ofs %g %g  D %i %i\n",
             ded->fParam[PRM_CONV(0)], ded->fParam[PRM_CONV(1)],
             ded->fParam[PRM_OFFS(0)], ded->fParam[PRM_OFFS(1)],
             ded->fiParam[PRM_D(0)], ded->fiParam[PRM_D(1)]);*/
}

int
cal_deduce( int                    flag,
            const calsignalinfo_t* info,
            const calrec_t*        chn1,
            const calrec_t*        chn2,
            caldeduced_t*          ded,
            int                    max )
{
    int n = 0; /* number of deduced units so far */
    int m; /* iterator */
    int i; /* iterator */
    int j; /* iterator */

    /* check */
    i = ( chn1 ? 1 : 0 ) + ( chn2 ? 1 : 0 );
    if ( ( info == 0 ) || ( ded == 0 ) || ( i == 0 ) ||
         ( ( info->fChnRelation == kCalChnRelSingle ) && ( i != 1 ) ) ||
         ( ( info->fChnRelation == kCalChnRelSingle ) && ( chn2 != 0 ) ) ||
         ( ( info->fChnRelation != kCalChnRelSingle ) && ( i != 2 ) ) )
    {
        return -1;
    }
    if ( max <= 0 )
    {
        return 0;
    }

    /* single channel */
    if ( info->fChnRelation == kCalChnRelSingle )
    {
        n = cal_deduce1( flag, info, chn1, chn2, ded, max );
        /* adjust exponent */
        if ( info->fExpo[ 0 ] != 1 )
        {
            for ( j = 0; j < n; j++ )
            {
                char buf[ 2 * CALUNITSIZE ];
                if ( unitAtomic( ded[ j ].fUnit ) )
                {
                    sprintf( buf, "%s", ded[ j ].fUnit );
                }
                else
                {
                    sprintf( buf, "(%s)", ded[ j ].fUnit );
                }
                if ( strlen( buf ) > 0 )
                {
                    sprintf( strend( buf ), "^{%i}", info->fExpo[ 0 ] );
                }
                strncpy( ded[ j ].fUnit, buf, CALUNITSIZE );
                ded[ j ].fUnit[ CALUNITSIZE - 1 ] = 0;
            }
        }
    }

    /* two channel */
    else
    {
        caldeduced_t* ded1[ 2 ] = { 0,
                                    0 }; /* deduced units for single channel */
        int           ded1num[ 2 ] = { 0, 0 }; /* deduced units length */

        /* deduce units for each channel */
        for ( i = 0; i < 2; i++ )
        {
            ded1[ i ] = calloc( max, sizeof( caldeduced_t ) );
            if ( ded1[ i ] == 0 )
            {
                continue;
            }
            ded1num[ i ] = cal_deduce1( flag,
                                        info,
                                        i == 0 ? chn1 : 0,
                                        i == 0 ? 0 : chn2,
                                        ded1[ i ],
                                        max );
        }
        /* combine units: add */
        if ( info->fChnRelation == kCalChnRelAdd )
        {
        }
        /* combine units: mul */
        else
        {
            /* first check if a dimension-less unit exists */
            if ( ( info->fExpo[ 0 ] == -info->fExpo[ 1 ] ) &&
                 ( ded1num[ 0 ] > 0 ) && ( ded1num[ 1 ] > 0 ) &&
                 ( strcmp( ded1[ 0 ][ 0 ].fUnit, ded1[ 1 ][ 0 ].fUnit ) ==
                   0 ) &&
                 ( n < max ) )
            {
                unit_combine( ded + n, ded1[ 0 ], ded1[ 1 ], info, chn1, chn2 );
                strncpy( ded[ n ].fUnit, "", CALUNITSIZE );
                n++;
            }
            /* check if basic units are the same */
            else if ( ( ded1num[ 0 ] > 0 ) && ( ded1num[ 1 ] > 0 ) &&
                      ( strcmp( ded1[ 0 ][ 0 ].fUnit, ded1[ 1 ][ 0 ].fUnit ) ==
                        0 ) )
            {
                int expo = info->fExpo[ 0 ] + info->fExpo[ 1 ];
                for ( i = 0; i < ded1num[ 0 ]; i++ )
                {
                    char newu[ 3 * CALUNITSIZE ];
                    if ( n >= max )
                    {
                        continue;
                    }
                    if ( expo == 0 )
                    {
                        strcpy( newu, "" );
                    }
                    else if ( expo == 1 )
                    {
                        sprintf( newu, "%s", ded1[ 0 ][ i ].fUnit );
                    }
                    else if ( unitAtomic( ded1[ 0 ][ i ].fUnit ) )
                    {
                        sprintf( newu, "%s^{%i}", ded1[ 0 ][ i ].fUnit, expo );
                    }
                    else
                    {
                        sprintf(
                            newu, "(%s)^{%i}", ded1[ 0 ][ i ].fUnit, expo );
                    }
                    unit_combine( ded + n,
                                  ded1[ 0 ] + i,
                                  ded1[ 1 ] + i,
                                  info,
                                  chn1,
                                  chn2 );
                    strncpy( ded[ n ].fUnit, newu, CALUNITSIZE );
                    ded[ n ].fUnit[ CALUNITSIZE - 1 ] = 0;
                    n++;
                }
            }
            /* else loop over all possibilities */
            else
            {
                for ( i = 0; i < ded1num[ 0 ]; i++ )
                {
                    for ( j = 0; j < ded1num[ 1 ]; j++ )
                    {
                        char newu[ 3 * CALUNITSIZE ];
                        if ( n >= max )
                        {
                            continue;
                        }
                        /* special case: B/A */
                        if ( ( info->fExpo[ 0 ] == -1 ) &&
                             ( info->fExpo[ 1 ] == 1 ) )
                        {
                            sprintf( newu, "%s", ded1[ 1 ][ j ].fUnit );
                            if ( strlen( ded1[ 0 ][ i ].fUnit ) == 0 )
                            {
                            }
                            else if ( unitAtomic( ded1[ 0 ][ i ].fUnit ) )
                            {
                                sprintf( strend( newu ),
                                         "/%s",
                                         ded1[ 0 ][ i ].fUnit );
                            }
                            else
                            {
                                sprintf( strend( newu ),
                                         "/(%s)",
                                         ded1[ 0 ][ i ].fUnit );
                            }
                        }
                        /* else */
                        else
                        {
                            strcpy( newu, "" );
                            if ( ( info->fExpo[ 0 ] != 0 ) &&
                                 ( strlen( ded1[ 0 ][ i ].fUnit ) > 0 ) )
                            {
                                if ( unitAtomic( ded1[ 0 ][ i ].fUnit ) ||
                                     ( info->fExpo[ 0 ] == 1 ) )
                                {
                                    sprintf( strend( newu ),
                                             "%s",
                                             ded1[ 0 ][ i ].fUnit );
                                }
                                else
                                {
                                    sprintf( strend( newu ),
                                             "(%s)",
                                             ded1[ 0 ][ i ].fUnit );
                                }
                                if ( info->fExpo[ 0 ] != 1 )
                                {
                                    sprintf( strend( newu ),
                                             "^{%i}",
                                             info->fExpo[ 0 ] );
                                }
                            }
                            if ( ( info->fExpo[ 1 ] != 0 ) &&
                                 ( strlen( ded1[ 1 ][ j ].fUnit ) > 0 ) )
                            {
                                if ( info->fExpo[ 1 ] < 0 )
                                {
                                    strcpy( strend( newu ), "/" );
                                }
                                if ( unitAtomic( ded1[ 1 ][ j ].fUnit ) ||
                                     ( info->fExpo[ 1 ] == 1 ) )
                                {
                                    sprintf( strend( newu ),
                                             "%s",
                                             ded1[ 1 ][ j ].fUnit );
                                }
                                else
                                {
                                    sprintf( strend( newu ),
                                             "(%s)",
                                             ded1[ 1 ][ j ].fUnit );
                                }
                                if ( abs( info->fExpo[ 1 ] ) != 1 )
                                {
                                    sprintf( strend( newu ),
                                             "^{%i}",
                                             abs( info->fExpo[ 1 ] ) );
                                }
                            }
                        }
                        unit_combine( ded + n,
                                      ded1[ 0 ] + i,
                                      ded1[ 1 ] + j,
                                      info,
                                      chn1,
                                      chn2 );
                        strncpy( ded[ n ].fUnit, newu, CALUNITSIZE );
                        ded[ n ].fUnit[ CALUNITSIZE - 1 ] = 0;
                        n++;
                    }
                }
            }
        }
    }

    /* handle density units */
    /*printf ("CHECK FOR POWER UNIT\n");*/
    for ( i = 0; i < n; i++ )
    {
        /*printf ("POWER UNITS TURNED %s FOR %s (%s %s)\n", 
                ded[i].fPowerUnit ? "ON" : "OFF", ded[i].fUnit,
                ded[i].fChn1 ? ded[i].fChn1->fChannel : "-",
                ded[i].fChn2 ? ded[i].fChn2->fChannel : "-");*/
        ded[ i ].fPowerUnit = 0;
    }
    if ( ( info->fDomain == kCalDomainFrequency ) &&
         ( info->fDensityUnits != kCalDensityNo ) )
    {
        /* first make sure we have the default */
        ECalDensity in = info->fDensityUnits;
        ECalDensity out = info->fDensityUnits;
        for ( i = 0; i < n; i++ )
        {
            ded[ i ].fiParam[ PRM_DENSITY ] = densityConv( in, out );
            ded[ i ].fiParam[ PRM_DENSITYRES ] = (int)out;
            if ( ( out == kCalDensityPwrPerHz ) || ( out == kCalDensityPwr ) )
            {
                /*printf ("POWER UNITS TURNED ON FOR %s (%s %s)\n", ded[i].fUnit,
                      ded[i].fChn1 ? ded[i].fChn1->fChannel : "-",
                      ded[i].fChn2 ? ded[i].fChn2->fChannel : "-");*/
                ded[ i ].fPowerUnit = 1;
            }
        }
        /* next: add other variations if BW > 0 */
        if ( info->fBW > 0 )
        {
            m = n;
            for ( j = 0; j < 3; j++ )
            {
                out = (ECalDensity)( ( (int)info->fDensityUnits + j ) % 4 + 1 );
                for ( i = 0; i < m; i++ )
                {
                    if ( n < max )
                    {
                        ded[ n ] = ded[ i ];
                        ded[ n ].fiParam[ PRM_DENSITY ] =
                            densityConv( in, out );
                        ded[ n ].fiParam[ PRM_DENSITYRES ] = (int)out;
                        if ( ( out == kCalDensityPwrPerHz ) ||
                             ( out == kCalDensityPwr ) )
                        {
                            ded[ n ].fPowerUnit = 1;
                        }
                        else if ( ( out == kCalDensityAmpPerRtHz ) ||
                                  ( out == kCalDensityAmp ) )
                        {
                            ded[ n ].fPowerUnit = 0;
                        }
                        /*printf ("New pwr unit: in = %s out = %s (conv %i)\n",
                            (in == kCalDensityNo) ? "kCalDensityNo" :
                            (in == kCalDensityAmpPerRtHz) ? "kCalDensityAmpPerRtHz" :
                            (in == kCalDensityPwrPerHz) ? "kCalDensityPwrPerHz" :
                            (in == kCalDensityAmp) ? "kCalDensityAmp" :
                            (in == kCalDensityPwr) ? "kCalDensityPwr" : "-",
                            (out == kCalDensityNo) ? "kCalDensityNo" :
                            (out == kCalDensityAmpPerRtHz) ? "kCalDensityAmpPerRtHz" :
                            (out == kCalDensityPwrPerHz) ? "kCalDensityPwrPerHz" :
                            (out == kCalDensityAmp) ? "kCalDensityAmp" :
                            (out == kCalDensityPwr) ? "kCalDensityPwr" : "-",
                            ded[n].fiParam[PRM_DENSITY]);*/
                        n++;
                    }
                }
            }
        }
        /* finally adjust unit string to reflect density */
        for ( i = 0; i < n; i++ )
        {
            char buf[ 2 * CALUNITSIZE ];
            /* exponent adjust */
            switch ( ded[ i ].fiParam[ PRM_DENSITY ] / 10 )
            {
            case 0: {
                strcpy( buf, ded[ i ].fUnit );
                break;
            }
            case 1: {
                int pos;
                if ( strlen( ded[ i ].fUnit ) == 0 )
                {
                    sprintf( buf, "()^{2}" );
                }
                else if ( unitAtomic( ded[ i ].fUnit ) )
                {
                    sprintf( buf, "%s^{2}", ded[ i ].fUnit );
                }
                else if ( ( j = unitExpo( ded[ i ].fUnit, &pos ) ) == 0 )
                {
                    sprintf( buf, "(%s)^{2}", ded[ i ].fUnit );
                }
                else
                {
                    sprintf( buf, "%s", ded[ i ].fUnit );
                    buf[ pos ] = 0;
                    if ( !unitAtomic( buf ) )
                    {
                        char buf2[ 2 * CALUNITSIZE ];
                        strcpy( buf2, buf );
                        sprintf( buf, "(%s)", buf2 );
                    }
                    sprintf( buf + strlen( buf ), "^{%i}", 2 * j );
                }
                break;
            }
            case 2: {
                int pos;
                if ( strlen( ded[ i ].fUnit ) == 0 )
                {
                    sprintf( buf, "()^{1/2}" );
                }
                else if ( unitAtomic( ded[ i ].fUnit ) )
                {
                    sprintf( buf, "%s^{1/2}", ded[ i ].fUnit );
                }
                else if ( ( j = unitExpo( ded[ i ].fUnit, &pos ) ) == 0 )
                {
                    sprintf( buf, "(%s)^{1/2}", ded[ i ].fUnit );
                }
                else
                {
                    sprintf( buf, "%s", ded[ i ].fUnit );
                    buf[ pos ] = 0;
                    if ( j != 2 )
                    {
                        if ( !unitAtomic( buf ) )
                        {
                            char buf2[ 2 * CALUNITSIZE ];
                            strcpy( buf2, buf );
                            sprintf( buf, "(%s)", buf2 );
                        }
                        if ( abs( j ) % 2 == 0 )
                        {
                            sprintf( buf + strlen( buf ), "^{%i}", j / 2 );
                        }
                        else
                        {
                            sprintf( buf + strlen( buf ), "^{%i/2}", j );
                        }
                    }
                }
                break;
            }
            }
            /* add bandwidth density */
            switch ( (ECalDensity)ded[ i ].fiParam[ PRM_DENSITYRES ] )
            {
            case kCalDensityAmp:
            case kCalDensityPwr:
            default: {
                break;
            }
            case kCalDensityAmpPerRtHz: {
                if ( strlen( buf ) == 0 )
                {
                    strcpy( buf, "Hz^{-1/2}" );
                }
                else
                {
                    strcpy( buf + strlen( buf ), "/Hz^{1/2}" );
                }
                break;
            }
            case kCalDensityPwrPerHz: {
                if ( strlen( buf ) == 0 )
                {
                    strcpy( buf, "Hz^{-1}" );
                }
                else
                {
                    strcpy( buf + strlen( buf ), "/Hz" );
                }
                break;
            }
            }
            strncpy( ded[ i ].fUnit, buf, CALUNITSIZE );
            ded[ i ].fUnit[ CALUNITSIZE - 1 ] = 0;
        }
    }

    return n;
}
