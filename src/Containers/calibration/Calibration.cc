#include "Calibration.hh"
#include "calutil.h"
#include <strings.h>

namespace calibration
{

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // Calibration                                                          //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    Calibration::Calibration( )
    {
        calinit( this );
    }

    //______________________________________________________________________________
    Calibration::Calibration( const Calibration& cal )
    {
        calinit( this );
        calcpy( this, &cal );
    }

    //______________________________________________________________________________
    Calibration::Calibration( const calrec_t& cal )
    {
        calinit( this );
        calcpy( this, &cal );
    }

    //______________________________________________________________________________
    Calibration::~Calibration( )
    {
        calrelease( this );
    }

    //______________________________________________________________________________
    Calibration&
    Calibration::operator=( const Calibration& cal )
    {
        calcpy( this, &cal );
        return *this;
    }

    //______________________________________________________________________________
    bool
    Calibration::operator==( const Calibration& cal ) const
    {
        return calcmp( this, &cal ) == 0;
    }

    //______________________________________________________________________________
    bool
    Calibration::operator<( const Calibration& cal ) const
    {
        return calcmp( this, &cal ) < 0;
    }

    //______________________________________________________________________________
    bool
    Calibration::Match( const Calibration& query ) const
    {
        return calmatch( this, &query ) != 0;
    }

    //______________________________________________________________________________
    void
    Calibration::SetChannel( const char* chnname )
    {
        calsetchannel( this, chnname );
    }

    //______________________________________________________________________________
    const char*
    Calibration::GetChannel( ) const
    {
        return calgetchannel( this );
    }

    //______________________________________________________________________________
    void
    Calibration::SetRef( const char* ref )
    {
        calsetref( this, ref );
    }

    //______________________________________________________________________________
    const char*
    Calibration::GetRef( ) const
    {
        return calgetref( this );
    }

    //______________________________________________________________________________
    void
    Calibration::SetUnit( const char* unit )
    {
        calsetunit( this, unit );
    }

    //______________________________________________________________________________
    const char*
    Calibration::GetUnit( ) const
    {
        return calgetunit( this );
    }

    //______________________________________________________________________________
    void
    Calibration::SetTime( const Time& time )
    {
        calsettime( this, time.getS( ) );
    }

    //______________________________________________________________________________
    Time
    Calibration::GetTime( ) const
    {
        return Time( calgettime( this ), 0 );
    }

    //______________________________________________________________________________
    void
    Calibration::SetDuration( const Interval& dur )
    {
        calsetduration( this, dur.GetS( ) );
    }

    //______________________________________________________________________________
    Interval
    Calibration::GetDuration( ) const
    {
        return Interval( calgetduration( this ), 0 );
    }

    //______________________________________________________________________________
    int
    Calibration::GetType( ) const
    {
        return calgettype( this );
    }

    //______________________________________________________________________________
    void
    Calibration::SetConversion( double conv )
    {
        calsetconversion( this, conv );
    }

    //______________________________________________________________________________
    void
    Calibration::ResetConversion( )
    {
        calresetconversion( this );
    }

    //______________________________________________________________________________
    double
    Calibration::GetConversion( ) const
    {
        return calgetconversion( this );
    }

    //______________________________________________________________________________
    void
    Calibration::SetOffset( double ofs )
    {
        calsetoffset( this, ofs );
    }

    //______________________________________________________________________________
    void
    Calibration::ResetOffset( )
    {
        calresetoffset( this );
    }

    //______________________________________________________________________________
    double
    Calibration::GetOffset( ) const
    {
        return calgetoffset( this );
    }

    //______________________________________________________________________________
    void
    Calibration::SetTimeDelay( double dt )
    {
        calsettimedelay( this, dt );
    }

    //______________________________________________________________________________
    void
    Calibration::ResetTimeDelay( )
    {
        calresettimedelay( this );
    }

    //______________________________________________________________________________
    double
    Calibration::GetTimeDelay( ) const
    {
        return calgettimedelay( this );
    }

    //______________________________________________________________________________
    void
    Calibration::SetTransferFunction( const float* trans, int len )
    {
        calsettransferfunction( this, trans, len );
    }

    //______________________________________________________________________________
    int
    Calibration::GetTransferFunction( const float*& trans ) const
    {
        return calgettransferfunction( this, &trans );
    }

    //______________________________________________________________________________
    void
    Calibration::SetPoleZeros( double       gain,
                               int          pnum,
                               int          znum,
                               const float* pzs )
    {
        calsetpolezeros( this, gain, pnum, znum, pzs );
    }

    //______________________________________________________________________________
    int
    Calibration::GetPoleZeros( double&       gain,
                               int&          pnum,
                               int&          znum,
                               const float*& pzs ) const
    {
        return calgetpolezeros( this, &gain, &pnum, &znum, &pzs );
    }

    //______________________________________________________________________________
    void
    Calibration::SetDefault( bool def )
    {
        calsetdefault( this, def );
    }

    //______________________________________________________________________________
    bool
    Calibration::GetDefault( ) const
    {
        return calgetdefault( this ) != 0;
    }

    //______________________________________________________________________________
    void
    Calibration::SetPreferredMag( int mag )
    {
        calsetpreferredmag( this, mag );
    }

    //______________________________________________________________________________
    int
    Calibration::GetPreferredMag( ) const
    {
        return calgetpreferredmag( this );
    }

    //______________________________________________________________________________
    void
    Calibration::SetPreferredD( int d )
    {
        calsetpreferredd( this, d );
    }

    //______________________________________________________________________________
    int
    Calibration::GetPreferredD( ) const
    {
        return calgetpreferredd( this );
    }

    //______________________________________________________________________________
    void
    Calibration::SetComment( const char* comment )
    {
        calsetcomment( this, comment );
    }

    //______________________________________________________________________________
    const char*
    Calibration::GetComment( ) const
    {
        return calgetcomment( this );
    }

    //______________________________________________________________________________
    bool
    Calibration::Xml( int          flag,
                      std::string& s,
                      int          index,
                      std::string* errormsg ) const
    {
        char buf[ 16 * 1024 ];
        char err[ 1024 ];
        if ( cal2xml( flag, this, buf, sizeof( buf ), index, err ) < 0 )
        {
            if ( errormsg )
                *errormsg = err;
            return false;
        }
        s = buf;
        return true;
    }

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // CalibrationCmp                                                       //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    bool
    CalibrationCmp::IsEqual( const Calibration& c1,
                             const Calibration& c2 ) const
    {
        switch ( fMode )
        {
        default:
        case kNormal: {
            return c1 == c2;
        }
        case kNameOnly: {
            return strcasecmp( c1.GetChannel( ), c2.GetChannel( ) ) == 0;
        }
        case kNameRef: {
            return ( strcasecmp( c1.GetChannel( ), c2.GetChannel( ) ) == 0 ) &&
                ( strcasecmp( c1.GetRef( ), c2.GetRef( ) ) == 0 );
        }
        case kNameRefUnit: {
            return ( strcasecmp( c1.GetChannel( ), c2.GetChannel( ) ) == 0 ) &&
                ( strcasecmp( c1.GetRef( ), c2.GetRef( ) ) == 0 ) &&
                ( strcasecmp( c1.GetUnit( ), c2.GetUnit( ) ) == 0 );
        }
        }
    }

    //______________________________________________________________________________
    bool
    CalibrationCmp::IsSmaller( const Calibration& c1,
                               const Calibration& c2 ) const
    {
        switch ( fMode )
        {
        default:
        case kNormal: {
            return c1 < c2;
        }
        case kNameOnly: {
            return strcasecmp( c1.GetChannel( ), c2.GetChannel( ) ) < 0;
        }
        case kNameRef: {
            int cmp;
            if ( ( cmp = strcasecmp( c1.GetChannel( ), c2.GetChannel( ) ) ) !=
                 0 )
            {
                return cmp < 0;
            }
            return strcasecmp( c1.GetRef( ), c2.GetRef( ) ) < 0;
        }
        case kNameRefUnit: {
            int cmp;
            if ( ( cmp = strcasecmp( c1.GetChannel( ), c2.GetChannel( ) ) ) !=
                 0 )
            {
                return cmp < 0;
            }
            if ( ( cmp = strcasecmp( c1.GetRef( ), c2.GetRef( ) ) ) != 0 )
            {
                return cmp < 0;
            }
            return strcasecmp( c1.GetUnit( ), c2.GetUnit( ) ) < 0;
        }
        }
    }

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // CalibrationList                                                      //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    CalibrationList::CalibrationList( const char* filename )
    {
        Read( filename );
    }

    //______________________________________________________________________________
    bool
    CalibrationList::Read( const char* filename )
    {
        calrec_t* clist = 0;
        int       num = calread( CALNORMAL, &clist, -1, filename );
        if ( num > 0 )
        {
            for ( int i = 0; i < num; ++i )
            {
                push_back( clist[ i ] );
            }
        }
        caldelete( clist );
        return ( num >= 0 );
    }

    //______________________________________________________________________________
    bool
    CalibrationList::Write( const char* filename ) const
    {
        calrec_t* clist = calnew( size( ) );
        if ( !clist )
        {
            return false;
        }
        for ( int i = 0; i < (int)size( ); ++i )
        {
            calcpy( clist + i, ( *this )[ i ] );
        }
        int ret = calwrite( CALNORMAL, clist, size( ), filename );
        caldelete( clist );
        return ( ret == 0 );
    }

} // namespace calibration
