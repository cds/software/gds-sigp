#ifndef _LIGO_POLYROOT_H
#define _LIGO_POLYROOT_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: polyroot						*/
/*                                                         		*/
/* Module Description: polyroot						*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 23Jan03  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: polyroot.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include <complex>

/** Finds the real and complex roots of a real polynominal. 
    Uses the Jenkins-Traub method and is based on the code found at
    http://www.crbond.com/roots.htm

    The supplied polynomial must have a non-zero leading coefficient.
    The length of the roots array must be at least order. The number
    of specified coefficients must be order+1 with the coefficient of
    highest power starting first.

    @memo Finds the real and complex roots of a real polynominal
    @author Written March 2003 by Daniel Sigg
    @version 1.0
    @param poly Coefficients of polynomial in decreasing order
    @param order Order of polynomial
    @param roots Roots of the polynomial (return)
    @return Number of roots (-1 on error)
 ************************************************************************/
int polyroot( const double* poly, int order, std::complex< double >* roots );

/** Expands the roots into a polynomial with real coefficients. 

    The supplied roots must come in complex pairs or be real. A complex
    root must be followed by its complex conjugate, or else an error is 
    returned. The length of the coefficents array must be at least n+1.
    the polynomial coeffcient of highest power is returned first. The

    @memo Expand into a real polynominal
    @author Written March 2003 by Daniel Sigg
    @version 1.0
    @param roots Roots of the polynomial
    @param order Number of  roots
    @param poly Coefficients of polynomial in decreasing order (return)
    @return Order of polynomial (-1 on error)
 ************************************************************************/
int polyexpand( const std::complex< double >* roots, int n, double* poly );

#endif // _LIGO_POLYROOT_H
