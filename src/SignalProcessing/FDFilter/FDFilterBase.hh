/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef FDFilter_Base_HH
#define FDFilter_Base_HH

#include "FSpectrum.hh"
#include "FSeries.hh"
#include "fSeries/DFT.hh"
#include "fSeries/PSD.hh"

/**  The FDFilterBase class defines an API for a frequency domain filter. 
  *  Frequency domain filters operate on either new or old style frequency
  *  series e.g. %FSeries (%DFT) or %FSpectrum (%PSD) class instances.
  *  The filter is defined by a real or complex frequency series that is
  *  mutiplied into the argument frequency series on an element-by-element 
  *  basis to form the result series.
  *  \brief Frequency domain filter base class.
  *  \author J. Zweizig
  *  \version 1.0; Last modified September 28, 2004
  */
class FDFilterBase
{

public:
    /**  Default (null) destructor for the FDFilterBase class.
      *  @memo Default constructor.
      */
    FDFilterBase( void );

    /**  Virtual destructor to allow cleaning up objects of derived classes.
      *  @memo Destroy a frequency domain filter. 
      */
    virtual ~FDFilterBase( void );

    /**  This method extracts from the specified DFT that frequency
      *  range for which the filter is defined, multiplies it by the filter 
      *  function, and returns.
      *  @memo Filter the specified DFT. 
      *  @param Int_DFT Input DFT to be filtered.
      *  @param Out_DFT Filtered output DFT.
      */
    virtual void Apply( const containers::DFT& Int_DFT,
                        containers::DFT&       Out_DFT ) = 0;

    /**  This method extracts from the specified FSereis that frequency
      *  range for which the filter is defined, multiplies it by the filter 
      *  function, and returns.
      *  @memo Filter the specified FSeries. 
      *  @param Int_DFT Input FSeries to be filtered.
      *  @param Out_DFT Filtered output FSeries.
      */
    virtual void Apply( const FSeries& Int_DFT, FSeries& Out_DFT ) = 0;

    /**  This method extracts from the specified input FSpectrum that frequency
      *  range for which the filter is defined, multiplies it by the filter 
      *  function, and writes the result to the specified output FSpectrum.
      *  The input FSpectrum must be sampled with the same frequency spacing 
      *  as the filter.
      *  @memo Filter the specified %FSpectrum. 
      *  @param Int_PSD Input %FSpectrum to be filtered.
      *  @param Out_PSD Filtered output %FSpectrum.
      */
    virtual void Apply( const FSpectrum& Int_PSD, FSpectrum& Out_PSD ) = 0;

    /**  This method extracts from the specified input PSD that frequency
      *  range for which the filter is defined, multiplies it by the filter 
      *  function, and writes the result to the specified output PSD.
      *  The input PSD must be sampled with the same frequency spacing 
      *  as the filter.
      *  @memo Filter the specified %PSD.
      *  @param Int_PSD Input %PSD to be filtered.
      *  @param Out_PSD Filtered output %PSD.
      */
    virtual void Apply( const containers::PSD& Int_PSD,
                        containers::PSD&       Out_PSD ) = 0;

    /**  This method extracts from the specified %DFT that frequency
      *  range for which the filter is defined, multiplies it by the filter 
      *  function, and returns.
      *  \brief Filter the specified %DFT.
      *  \param  Int_DFT Input %DFT to be filtered.
      *  \return Filtered %DFT data
      */
    virtual containers::DFT Apply( const containers::DFT& Int_DFT );

    /**  This method extracts from the specified FSpectrum that frequency
      *  range for which the filter is defined, multiplies it by the filter 
      *  function, and returns.
      *  \brief Filter the specified %FSeries.
      *  \param  Int_DFT Input %FSeries to be filtered.
      *  \return Filtered %FSeries data
      */
    virtual FSeries Apply( const FSeries& Int_DFT );

    /**  This method extracts from the specified FSpectrum that frequency
      *  range for which the filter is defined, multiplies it by the filter 
      *  function, and returns the filtered FSpectrum.
      *  The input FSpectrum must be sampled with the same frequency spacing 
      *  as the filter.
      *  \brief  Filter the specified FSpectrum. 
      *  \param  Int_PSD Input %FSpectrum to be filtered.
      *  \return Filtered %FSpectrum data
      */
    virtual FSpectrum Apply( const FSpectrum& Int_PSD );

    /**  This method extracts from the specified PSD that frequency range
      *  for which the filter is defined, multiplies it by the filter 
      *  function, and returns the filtered PSD.
      *  The input PSD must be sampled with the same frequency spacing 
      *  as the filter.
      *  \brief Filter the specified PSD. 
      *  \param Int_PSD Input PSD to be filtered.
      *  \return Filtered PSD data
      */
    virtual containers::PSD Apply( const containers::PSD& Int_PSD );

    /**  Create an identical copy of this filter.
      *  \brief Clone the filter.
      *  \return Pointer to the newly created filter intance.
      */
    virtual FDFilterBase* clone( void ) const = 0;

private:
};

//======================================  Inline methods
inline FDFilterBase::FDFilterBase( void )
{
}

inline FDFilterBase::~FDFilterBase( void )
{
}

inline containers::DFT
FDFilterBase::Apply( const containers::DFT& In_DFT )
{
    containers::DFT out;
    Apply( In_DFT, out );
    return out;
}

inline FSeries
FDFilterBase::Apply( const FSeries& Int_DFT )
{
    FSeries out;
    Apply( Int_DFT, out );
    return out;
}

inline FSpectrum
FDFilterBase::Apply( const FSpectrum& Int_PSD )
{
    FSpectrum out;
    Apply( Int_PSD, out );
    return out;
}

inline containers::PSD
FDFilterBase::Apply( const containers::PSD& In_PSD )
{
    containers::PSD out;
    Apply( In_PSD, out );
    return out;
}

#endif //----- FDFilter_HH
