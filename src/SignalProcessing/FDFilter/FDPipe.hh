/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef FDPIPE_HH
#define FDPIPE_HH

#include "FDFilter.hh"
#include "Pipe.hh"
#include "TSeries.hh"
#include <memory>

class DVector;

/**  %FDPipe filters a time series in the Fourier domain. After 
  *  constructing a %FDPipe filter from the appropriate FDFilterBase, the 
  *  %FDPipe can be used identically to a Pipe.
  *  @memo f-domain filtering of a TSeries.
  */

class FDPipe : public Pipe
{
public:
    /**  Define the processing of the input time series. The following options 
    *  are available:
    *  <ol>
    *    <li><b>discrete:</b>The input time series is fourier transformed, 
    *         filtered and inverse transformed with no additional processing.
    *    </li>
    *    <li><b>overlap:</b>The input time series is fourier transformed, 
    *         filtered and inverse transformed. Only the central half of the
    *         data are returned.
    *    </li>
    *    <li><b>inmux:</b>Input data strides are overlapped with previous data.
    *         The input time series is windowed with  
    *         \f$ \frac{1}{2}(1 + \cos{2 \pi x / N})\f$, fourier transformed, 
    *         filtered and inverse transformed. The resulting TSeries is
    *         added to the last overlapping portion of the previous stride.
    *    </li>
    *    <li><b>outmux:</b>Input data strides are overlapped with previous data.
    *         The combined data segment is fourier transformed, filtered and 
    *         inverse transformed. The resulting TSeries is windowed and
    *         added to the last overlapping portion of the previous stride.
    *    </li>
    *  </ol>
    */
    enum mode_flags
    {
        discrete, ///< Filter entire data segment
        overlap, ///< Filter segment and save only the central half.
        inmux, ///< Window overlapping data filter and add it to the
        ///< previously calculated overlapping segment
        outmux ///< Filter overlapped data and add windowed result to the
        ///< previously calculated overlapping segment
    };

public:
    using Pipe::apply;
    using Pipe::dataCheck;

    /**  Null pipe constructor.
    *  \brief Null constructor.
    */
    FDPipe( void );

    /**  Construct an %FDPipe and specify the filtering
    *  \brief Filter pipe constructor.
    *  \param fd     Frequency domain filter.
    *  \param mode   Processing/overlap mode.
    *  \param tChomp Length of data to filter in a single chomp
    */
    FDPipe( const FDFilterBase& fd,
            mode_flags          mode = discrete,
            Interval            tChomp = 0.0 );

    /**  Construct an identical copy of the specified filter.
    *  \brief Copy constructor.
    *  \param fp %FDPipe to be copied.
    */
    FDPipe( const FDPipe& fp );

    /**  Destroy an %FDPipe 
    *  \brief Destructor.
    */
    virtual ~FDPipe( void );

    /**  Check that input data is compatible with expected filter input. This
    *  method implements the Pipe API data check.
    *  \brief Input data validity check.
    *  \param ts Input data.
    */
    void dataCheck( const TSeries& ts ) const;

    /**  Apply a filter to a time series.
    *  \brief Apply the filter
    *  \param ts Input time series
    *  \return Filtered time series.
    */
    TSeries apply( const TSeries& ts );

    /**  Create a copy of the %FDPipe filter. This method implements the Pipe
    *  API clone method.
    *  \brief Clone a filter.
    *  \return Pointer to an identical copy of the filter.
    */
    FDPipe* clone( void ) const;

    /**  Flush out any unfiltered data.
    *  \brief Flush input data
    *  \return Unfiltered data.
    */
    TSeries flush( void );

    /**  Get the filter mode. The filter mode determines how data from a 
    *  time series longer the and chomp width will be combined.
    *  \brief Filter mode
    *  \return Filter mode.
    */
    mode_flags getMode( void ) const;

    /**  Time of the first sample processed after the filter was constructed 
    *  or reset. If the filter has not been used to process data, getStartTime
    *  return zero time. 
    *  \brief Get the start time.
    *  \return Start of processed data.
    */
    Time getStartTime( void ) const;

    /**  Get the time of the first sample in the next expected  interval.
    *  \brief Get current time.
    *  \return Start time of next interval.
    */
    Time getCurrentTime( void ) const;

    /**  Test whether the filter has been used. 
    *  \brief Test filter in use.
    *  \return true if filter is in use.
    */
    bool inUse( void ) const;

    /**  %FDPipe assignment operator.
    *  \brief Assignment operator.
    *  \param fp %FDPipe to be copied to this instance.
    *  \return Reference to this instance.
    */
    FDPipe& operator=( const FDPipe& fp );

    /**  Reset %FDPipe.
   */
    void reset( void );

    /**  Set the filter processing stride.
    *  \brief Set the chomp size.
    *  \param tChomp Length of time stride.
    */
    void setChomp( Interval tChomp );

    /**  Set the frequency domain filter.
    *  \brief Set the f-domain filter.
    *  \param fd f-Domain filter used to process data.
    */
    void setFilter( const FDFilterBase& fd );

    /**  Set the overlap and processing mode.
    *  \brief Set the mode.
    *  \param mode Overlap mode to be used.
    */
    void setMode( mode_flags mode );

private:
    /**  Filter a time series and place the result in mFiltered.
   */
    void filter( const TSeries& ts );

    /**  Filter a time series and add it to the results in mFiltered.
   */
    void filterAdd( const TSeries& ts );

    /**  Filter a time series and add it to the results in mFiltered.
   */
    void setMux( Interval tStep );

private:
    std::unique_ptr< FDFilterBase > mFilter;
    mode_flags                      mMode;
    Time                            mStartTime;
    Time                            mCurrentTime;
    Interval                        mChomp;
    Interval                        mTStep; // input data time step
    Interval                        mDt;
    std::unique_ptr< DVector >      mSinSq;
    TSeries                         mData;
    TSeries                         mFiltered;
};

//======================================  Inlne methods
inline Time
FDPipe::getCurrentTime( void ) const
{
    return mCurrentTime;
}
inline FDPipe::mode_flags
FDPipe::getMode( void ) const
{
    return mMode;
}

inline Time
FDPipe::getStartTime( void ) const
{
    return mStartTime;
}

inline bool
FDPipe::inUse( void ) const
{
    return mStartTime != Time( 0 );
}

#endif // FDPIPE_HH
