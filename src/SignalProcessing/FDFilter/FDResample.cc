/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "FDResample.hh"
#include "DVector.hh"

using namespace containers;

//======================================  Resample constructor
FDResample::FDResample( double rate ) : mSampleRate( rate )
{
}

//======================================  Resample destructor
FDResample::~FDResample( void )
{
}

//======================================  Resample an FSeries
void
FDResample::Apply( const FSeries& in, FSeries& out )
{
    double in_sample = 2.0 * in.getHighFreq( );
    if ( mSampleRate > in_sample )
    {
        if ( &out != &in )
            out = in;
        out.extend( 0.5 * mSampleRate );
    }
    else if ( in.isSingleSided( ) )
    {
        out = in.extract( 0.0, 0.5 * mSampleRate );
    }
    else
    {
        out = in.extract( -.5 * mSampleRate, mSampleRate );
    }
}

//======================================  Resample an FSpectrum
void
FDResample::Apply( const FSpectrum& in, FSpectrum& out )
{
    double in_sample = 2.0 * in.getHighFreq( );
    if ( mSampleRate > in_sample )
    {
        if ( &out != &in )
            out = in;
        long N = long( 0.5 * mSampleRate / in.getFStep( ) ) + 1;
        out.refDVect( )->Extend( N );
    }
    else
    {
        out = in.extract( 0, 0.5 * mSampleRate );
    }
}

//======================================  Resample an DFT
void
FDResample::Apply( const DFT& in, DFT& out )
{
    double in_sample = 2.0 * in.getHighFreq( );
    if ( mSampleRate > in_sample )
    {
        if ( &out != &in )
            out = in;
        out.extend( 0.5 * mSampleRate );
    }
    else
    {
        out = in.extract_dft( -.5 * mSampleRate, mSampleRate );
    }
}

//======================================  Resample a PSD
void
FDResample::Apply( const PSD& in, PSD& out )
{
    double in_sample = 2.0 * in.getHighFreq( );
    if ( mSampleRate > in_sample )
    {
        if ( &out != &in )
            out = in;
        long N = long( 0.5 * mSampleRate / in.getFStep( ) ) + 1;
        out.refDVect( ).Extend( N );
    }
    else
    {
        out = in.extract_psd( 0, 0.5 * mSampleRate );
    }
}

//======================================  Clone an FDResample
FDResample*
FDResample::clone( void ) const
{
    return new FDResample( *this );
}

//======================================  Set the sample rate
void
FDResample::setSampleRate( double rate )
{
    mSampleRate = rate;
}
