/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "MultiFD.hh"
#include "FSpectrum.hh"
#include "FSeries.hh"

using namespace containers;

//======================================  MultiFD default constructor
MultiFD::MultiFD( void )
{
}

//======================================  MultiFD copy constructor
MultiFD::MultiFD( const MultiFD& mfd )
{
    *this = mfd;
}

//======================================  MultiFD destructor
MultiFD::~MultiFD( void )
{
    delete_filters( );
}

//======================================  Assignment operator
MultiFD&
MultiFD::operator=( const MultiFD& mfd )
{
    delete_filters( );
    filter_vect::size_type N = mfd.mFilter.size( );
    mFilter.resize( N );
    for ( filter_vect::size_type i = 0; i < N; ++i )
    {
        mFilter[ i ] = mfd.mFilter[ i ]->clone( );
    }
    return *this;
}

//======================================  Clone a MultiFD
MultiFD*
MultiFD::clone( void ) const
{
    return new MultiFD( *this );
}

//======================================  Apply all filters in order
void
MultiFD::Apply( const FSeries& fin, FSeries& fout )
{
    filter_vect::size_type N = mFilter.size( );
    if ( !N )
        return;
    mFilter[ 0 ]->Apply( fin, fout );
    for ( filter_vect::size_type i = 1; i < N; ++i )
        mFilter[ i ]->Apply( fout, fout );
}

//======================================  Apply all filters in order
void
MultiFD::Apply( const FSpectrum& fin, FSpectrum& fout )
{
    filter_vect::size_type N = mFilter.size( );
    if ( !N )
        return;
    mFilter[ 0 ]->Apply( fin, fout );
    for ( filter_vect::size_type i = 1; i < N; ++i )
        mFilter[ i ]->Apply( fout, fout );
}

//======================================  Apply all filters in order
void
MultiFD::Apply( const DFT& fin, DFT& fout )
{
    filter_vect::size_type N = mFilter.size( );
    if ( !N )
        return;
    mFilter[ 0 ]->Apply( fin, fout );
    for ( filter_vect::size_type i = 1; i < N; ++i )
        mFilter[ i ]->Apply( fout, fout );
}

//======================================  Apply all filters in order
void
MultiFD::Apply( const PSD& fin, PSD& fout )
{
    filter_vect::size_type N = mFilter.size( );
    if ( !N )
        return;
    mFilter[ 0 ]->Apply( fin, fout );
    for ( filter_vect::size_type i = 1; i < N; ++i )
        mFilter[ i ]->Apply( fout, fout );
}

//======================================  delete filters
void
MultiFD::delete_filters( void )
{
    filter_vect::size_type N = mFilter.size( );
    for ( filter_vect::size_type i = 0; i < N; ++i )
        delete mFilter[ i ];
    mFilter.clear( );
}

//======================================  delete filters
void
MultiFD::addFilter( const FDFilterBase& fdf )
{
    mFilter.push_back( fdf.clone( ) );
}
