/* -*- mode: c++; c-basic-offset: 4; -*- */
//
//  Class FDFilter: General frequency-domain filter class.
//    version:  0.3
//       date:  2004.09.24
//    authors:  Patrick J. Sutton (psutton@ligo.caltech.edu)
//              John G. Zweizig (jzweizig@ligo.caltech.edu)
//
///////////////////////////////////////////////////////////////////////////

#ifndef FDFilter_HH
#define FDFilter_HH

#include "FDFilterBase.hh"

/**  The FDFilter class applies a frequency domain filter specified as an 
  *  FSeries or an FSpectrum to data in the same format; ie, it
  *  multiplies frequency series objects frequency-by-frequency.
  *  @memo Frequency-domain filter.
  *  @author Patrick Sutton and John Zweizig.
  *  @version $Id$
  */
class FDFilter : public FDFilterBase
{

public:
    using FDFilterBase::Apply;

    /**  Constructor a frequency -domain filter with an undefined transfer 
      *  function.
      *  @memo Default constructor.
      */
    FDFilter( void );

    /**  Construct a frequency domain filter and set the transfer function 
      *  as specified by an FSeries.
      *  @memo Construct an FSeries filter.
      *  @param transfunc Filter transfer function.
      */
    FDFilter( const FSeries& transfunc );

    /**  Construct a frequency domain spectrum filter and set the transfer 
      *  function as specified by an FSpectrum. Note that this is equivalent
      *  to an FSeries filter, but wil only operate on power spectra.
      *  @memo Construct an FSpectrum filter.
      *  @param transfunc Filter transfer function.
      */
    FDFilter( const FSpectrum& transfunc );

    /**  Construct a frequency domain filter and set the transfer function 
      *  as specified by an fSeries.
      *  @memo Construct an fSeries filter.
      *  @param transfunc Filter transfer function.
      */
    FDFilter( const containers::fSeries& transfunc );

    /**  Construct a frequency domain filter identical to the argument.
      *  @memo Copy constructor.
      *  @param fdfilt %FDFilter to be copied.
      */
    FDFilter( const FDFilter& fdfilt );

    /**  Assign a frequency domain filter identical to the argument.
      *  @memo Assignment operator.
      *  @param fdfilt %FDFilter to be copied.
      */
    FDFilter& operator=( const FDFilter& fdfilt );

    /**  Destroy a frequency-domain filter and release allocated storage.
      *  @memo Destructor.
      */
    virtual ~FDFilter( void );

    /**  This method extracts from the specified DFT that frequency
      *  range for which the filter is defined, multiplies it by the filter 
      *  function, and returns. Apply must work correctly when the input 
      *  series is the same as the output series i.e. when filtering is 
      *  performed in place.
      *  @memo Filter the specified DFT.
      *  @param In_DFT Unfiltered (input) DFT.
      *  @param Out_DFT Filtered (output) DFT.
      *  @exception std::runtime_error if DFT frequency step doesn't 
      *  match filter or if DFT transfer function not defined.
      */
    virtual void Apply( const containers::DFT& In_DFT,
                        containers::DFT&       Out_DFT );

    /**  This method extracts from the specified FSeries that frequency
      *  range for which the filter is defined, multiplies it by the filter 
      *  function, and returns. Apply must work correctly when the input 
      *  series is the same as the output series i.e. when filtering is 
      *  performed in place.
      *  @memo Filter the specified FSeries. 
      *  @param In_DFT Unfiltered (input) FSeries.
      *  @param Out_DFT Filtered (output) FSeries.
      *  @exception std::runtime_error if FSeries frequency step doesn't 
      *  match filter.
      */
    virtual void Apply( const FSeries& In_DFT, FSeries& Out_DFT );

    /**  This method extracts from the specified input FSpectrum that frequency
      *  range for which the filter is defined, multiplies it by the filter 
      *  function, and writes the result to the specified output FSpectrum.
      *  The input FSpectrum must be sampled with the same frequency spacing 
      *  as the filter. Apply must work correctly when the input spectrum is
      *  the same as the output spectrum i.e. when filtering is 
      *  performed in place.
      *  @memo Filter the specified FSpectrum. 
      *  @param In_PSD  Input FSpectrum to be filtered.
      *  @param Out_PSD Filtered output FSpectrum.
      *  @exception std::runtime_error if FSpectrum frequency step doesn't 
      *  match filter.
      */
    virtual void Apply( const FSpectrum& In_PSD, FSpectrum& Out_PSD );

    /**  This method extracts from the specified input PSD that frequency
      *  range for which the filter is defined, multiplies it by the filter 
      *  function, and writes the result to the specified output PSD.
      *  The input PSD must be sampled with the same frequency spacing 
      *  as the filter. Apply must work correctly when the input PSD is
      *  the same as the output spectrum i.e. when filtering is 
      *  performed in place.
      *  @memo Filter the specified PSD. 
      *  @param In_PSD  Input PSD to be filtered.
      *  @param Out_PSD Filtered output PSD.
      *  @exception std::runtime_error if PSD frequency step doesn't 
      *  match filter or if PSD transfer function not defined.
      */
    virtual void Apply( const containers::PSD& In_PSD,
                        containers::PSD&       Out_PSD );

    /**  Create an identical copy of this filter.
      *  @memo Clone the filter.
      *  @return Pointer to the identical copy.
      */
    virtual FDFilter* clone( void ) const;

    /**  Retrieve the modulus squared of the filter transfer function into 
      *  an FSpectrum.
      *  @memo Retrieve transfer function modulus squared.
      *  @return FSpectrum containing the transfer function modulus squared.
      */
    const FSpectrum& getFSpectrum( void ) const;

    /**  Return the maximum frequency for which the filter is defined.
      *  @memo Return maximum frequency for which the filter is defined.
      *  @return Highest frequency for which the transfer function is defined.
      */
    double getHighFreq( void ) const;

    /**  Return the minimum frequency for which the filter is defined.
      *  @memo Return minimum frequency for which the filter is defined.
      *  @return Lowest frequency for which the transfer function is defined.
      */
    double getLowFreq( void ) const;

    /**  Set the filter transfer function to that specified by an FSeries.
      *  FDFilters with FSeries transfer functions may be used for FSeries
      *  DFT, FSpectrum or PSD objects.
      *  @memo Set the filter transfer function.
      *  @param calib Transfer function FSeries.
      */
    void setFDFilter( const FSeries& calib );

    /**  Set the filter transfer function to that specified by an FSpectrum.
      *  FDFilters with FSpectrum transfer functions may be used onlly on 
      *  FSpectrum or PSD objects.
      *  @memo Set the filter transfer function.
      *  @param calib Transfer function FSpectrum.
      */
    void setFDFilter( const FSpectrum& calib );

    /**  Set the interpolation enable option. If the input frequency series 
      *  does not match the frequency range or spacing of the transfer function
      *  This option will allow the transfer function to be interpolated to
      *  match the input series.
      *  @memo Enable transfer function interpolation.
      *  @param yorn Enable interpolation if true
      */
    void setInterpolate( bool yorn );

private:
    /**  This method extracts from the specified input fSeries that frequency
      *  range for which the filter is defined, multiplies it by the filter 
      *  function, and writes the result to the specified output PSD.
      *  The input PSD must be sampled with the same frequency spacing 
      *  as the filter. Apply must work correctly when the input PSD is
      *  the same as the output spectrum i.e. when filtering is 
      *  performed in place.
      *  @memo Combined FSeries filter mechanism. 
      *  @param Int_PSD Input FSpectrum to be filtered.
      *  @param Out_PSD Filtered output FSpectrum.
      */
    void fApply( const containers::fSeries& In, containers::fSeries& Out );

private:
    //  FSpectrum holding filter function.
    FSpectrum FSpecFilter;

    //  FSeries holding filter function.
    FSeries FSeriesFilter;

    //  Is filter stored as FSeries, so phase data is available (true if yes)?
    bool isFSeries;

    //  fSeries version (PSD or DFT)
    containers::fSeries* fSeriesFilter;

    //  Enable interpolation.
    bool interpEnable;
};

//======================================  Inline methods
inline const FSpectrum&
FDFilter::getFSpectrum( void ) const
{
    // cout << "FDFilter::getFSpectrum called.\n";
    return FSpecFilter;
}

inline double
FDFilter::getHighFreq( void ) const
{
    if ( fSeriesFilter )
        return fSeriesFilter->getHighFreq( );
    if ( isFSeries )
        FSeriesFilter.getHighFreq( );
    return FSpecFilter.getHighFreq( );
}

inline double
FDFilter::getLowFreq( void ) const
{
    // cout << "FDFilter::getLowFreq called.\n";
    if ( fSeriesFilter )
        return fSeriesFilter->getLowFreq( );
    if ( isFSeries )
        FSeriesFilter.getLowFreq( );
    return FSpecFilter.getLowFreq( );
}

#endif //----- FDFilter_HH
