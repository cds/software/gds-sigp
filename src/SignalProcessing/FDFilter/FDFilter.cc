/* -*- mode: c++; c-basic-offset: 4; -*- */
//
//  Class FDFilter: General frequency-domain filter class.
//    version:  0.3
//       date:  2004.09.24
//    authors:  Patrick J. Sutton (psutton@ligo.caltech.edu)
//              John G. Zweizig (jzweizig@ligo.caltech.edu)
//
//  Validation done:
//  Cursory: Can call all methods with reasonable output.
//
///////////////////////////////////////////////////////////////////////////

#include "FDFilter.hh"
#include "fSeries/fSeries.hh"

#ifndef __CINT__
#include "DVector.hh"
#include <iostream>
#include <stdexcept>
#endif //------ !def(__CINT__)

using namespace std;

//======================================  FDFilter constructor
FDFilter::FDFilter( void )
    : isFSeries( false ), fSeriesFilter( 0 ), interpEnable( false )
{
}

//======================================  FDFilter data constructor
FDFilter::FDFilter( const FSpectrum& transfunc )
    : FSpecFilter( transfunc ), isFSeries( false ), fSeriesFilter( 0 ),
      interpEnable( false )
{
}

//======================================  FDFilter data constructor
FDFilter::FDFilter( const FSeries& transfunc )
    : FSeriesFilter( transfunc ), isFSeries( true ), fSeriesFilter( 0 ),
      interpEnable( false )
{
}

//======================================  FDFilter data constructor
FDFilter::FDFilter( const containers::fSeries& transfunc )
{
    fSeriesFilter = transfunc.clone( );
    isFSeries = ( fSeriesFilter->getFSType( ) == containers::fSeries::kDFT );
    interpEnable = false;
}

//======================================  FDFilter data constructor
FDFilter::FDFilter( const FDFilter& fdfilt ) : fSeriesFilter( 0 )
{
    *this = fdfilt;
}

//======================================  FDFilter data constructor
FDFilter&
FDFilter::operator=( const FDFilter& fdf )
{
    FSpecFilter = fdf.FSpecFilter;
    FSeriesFilter = fdf.FSeriesFilter;
    isFSeries = fdf.isFSeries;
    if ( fSeriesFilter )
        delete fSeriesFilter;
    if ( !fdf.fSeriesFilter )
        fSeriesFilter = 0;
    else
        fSeriesFilter = fdf.fSeriesFilter->clone( );
    interpEnable = fdf.interpEnable;
    return *this;
}

//======================================  FDFilter destructor
FDFilter::~FDFilter( void )
{
    if ( fSeriesFilter )
        delete fSeriesFilter;
    fSeriesFilter = 0;
}

//======================================  Clone a filter
FDFilter*
FDFilter::clone( void ) const
{
    return new FDFilter( *this );
}

//======================================  Apply the filter to an FSpectrum
void
FDFilter::Apply( const FSpectrum& Int_PSD, FSpectrum& out_data )
{
    //---------- Useful quantities

    //---------- Check that frequency resolution of interpolated
    //           calibration data matches that of Fspectrum to be
    //           calibrated.
    double freq_step = FSpecFilter.getFStep( );
    if ( freq_step != Int_PSD.getFStep( ) )
    {
        throw runtime_error(
            "FDFilter: FSpectrum frequency step does not match filter." );
    }

    //---------- Determine frequency range for which we have both
    //           filter and data.
    double freq_max = FSpecFilter.getHighFreq( );
    double freq_min = FSpecFilter.getLowFreq( );
    if ( freq_min < Int_PSD.getLowFreq( ) )
    {
        freq_min = Int_PSD.getLowFreq( );
    }
    if ( freq_max > Int_PSD.getHighFreq( ) )
    {
        freq_max = Int_PSD.getHighFreq( );
    }

    //---------- Extract appropriate subsection of data, filter.
    if ( freq_min != Int_PSD.getLowFreq( ) ||
         freq_max != Int_PSD.getHighFreq( ) )
    {
        out_data = Int_PSD.extract( freq_min, freq_max - freq_min + freq_step );
    }
    else if ( &out_data != &Int_PSD )
    {
        out_data = Int_PSD;
    }

    unsigned long jMin = FSpecFilter.getBin( freq_min );
    unsigned long jMax = FSpecFilter.getBin( freq_max + freq_step );
    if ( jMin == 0 && jMax == FSpecFilter.getNStep( ) )
    {
        *( out_data.refDVect( ) ) *= *FSpecFilter.refDVect( );
    }
    else
    {
        DVector* temp = FSpecFilter.refDVect( )->Extract( jMin, jMax - jMin );
        *( out_data.refDVect( ) ) *= *temp;
        delete temp;
    }
}

//======================================  Apply the filter to an FSeries
void
FDFilter::Apply( const FSeries& Int_PSD, FSeries& out_data )
{

    //---------- Check that frequency resolution of interpolated
    //           calibration data matches that of Fspectrum to be
    //           calibrated.
    double f_step = FSeriesFilter.getFStep( );
    if ( f_step != Int_PSD.getFStep( ) )
    {
        throw runtime_error(
            "FDFilter: FSeries frequency step does not match filter." );
    }

    //---------- Determine frequency range for which we have both
    //           filter and data.
    double f_max = FSpecFilter.getHighFreq( );
    double f_min = FSpecFilter.getLowFreq( );
    if ( f_min < Int_PSD.getLowFreq( ) )
    {
        f_min = Int_PSD.getLowFreq( );
    }
    if ( f_max > Int_PSD.getHighFreq( ) )
    {
        f_max = Int_PSD.getHighFreq( );
    }

    //---------- Extract appropriate subsection of data, filter.
    if ( f_min != Int_PSD.getLowFreq( ) || f_max != Int_PSD.getHighFreq( ) )
    {
        out_data = Int_PSD.extract( f_min, f_max - f_min + f_step );
    }
    else if ( &out_data != &Int_PSD )
    {
        out_data = Int_PSD;
    }

    unsigned long jMin = FSeriesFilter.getBin( f_min );
    unsigned long jMax = FSeriesFilter.getBin( f_max + f_step );
    out_data.refDVect( )->mpy(
        0, *FSeriesFilter.refDVect( ), jMin, jMax - jMin );
}

//======================================  Apply the filter to a PSD
void
FDFilter::Apply( const containers::PSD& In, containers::PSD& Out )
{
    if ( !fSeriesFilter )
        throw logic_error( "PSD filter not available" );
    fApply( static_cast< const containers::fSeries& >( In ),
            static_cast< containers::fSeries& >( Out ) );
}

//======================================  Apply the filter to a DFT
void
FDFilter::Apply( const containers::DFT& In, containers::DFT& Out )
{
    if ( !fSeriesFilter || !isFSeries )
    {
        throw logic_error( "DFT filter not available" );
    }
    fApply( static_cast< const containers::fSeries& >( In ),
            static_cast< containers::fSeries& >( Out ) );
}

//======================================  Apply the filter to an fSeries
void
FDFilter::fApply( const containers::fSeries& In, containers::fSeries& Out )
{
    //---------- Check that frequency resolution of interpolated
    //           calibration data matches that of fSeries to be
    //           calibrated.
    double f_step = In.getFStep( );
    if ( f_step == fSeriesFilter->getFStep( ) )
    {
    }
    else if ( interpEnable )
    {
        *fSeriesFilter =
            fSeriesFilter->interpolate( 0.0, In.getHighFreq( ), f_step, true );
    }
    else
    {
        throw runtime_error(
            "FDFilter: frequency step does not match filter." );
    }

    //---------- Determine frequency range for which we have both
    //           filter and data.
    double f_max = In.getHighFreq( );
    double f_min = In.getLowFreq( );
    bool   all_data = true;
    if ( f_min < fSeriesFilter->getLowFreq( ) )
    {
        f_min = fSeriesFilter->getLowFreq( );
        all_data = false;
    }
    if ( f_max > fSeriesFilter->getHighFreq( ) )
    {
        f_max = fSeriesFilter->getHighFreq( );
        all_data = false;
    }

    //----------------------------------  Extract subsection of data, filter.
    if ( !all_data )
        Out = In.extract( f_min, f_max - f_min + f_step );
    else if ( &Out != &In )
        Out = In;

    //----------------------------------  Filter the fSeries.
    unsigned long jMin = fSeriesFilter->getBin( f_min );
    unsigned long jMax = fSeriesFilter->getBin( f_max );
    if ( fSeriesFilter->single_sided( ) )
        jMax += 1;
    Out.refDVect( ).mpy( 0, fSeriesFilter->refDVect( ), jMin, jMax - jMin );
}

//======================================  Set the filter series.
void
FDFilter::setFDFilter( const FSeries& newfilter )
{
    FSeriesFilter = newfilter;
    FSpecFilter = FSpectrum( newfilter );
    if ( fSeriesFilter )
        delete fSeriesFilter;
    Interval dT = newfilter.getEndTime( ) - newfilter.getStartTime( );
    fSeriesFilter = new containers::fSeries( newfilter.getCenterFreq( ),
                                             newfilter.getFStep( ),
                                             newfilter.getStartTime( ),
                                             dT,
                                             *newfilter.refDVect( ) );
    isFSeries = true;
}

//======================================  Set the filter spectrum.
void
FDFilter::setFDFilter( const FSpectrum& newfilter )
{
    FSpecFilter = newfilter;
    if ( fSeriesFilter )
        delete fSeriesFilter;
    Interval dT = newfilter.getEndTime( ) - newfilter.getStartTime( );
    fSeriesFilter = new containers::fSeries( newfilter.getLowFreq( ),
                                             newfilter.getFStep( ),
                                             newfilter.getStartTime( ),
                                             dT,
                                             *newfilter.refDVect( ) );
    isFSeries = false;
}

//======================================  Enable interpolation
void
FDFilter::setInterpolate( bool interp )
{
    interpEnable = interp;
}
