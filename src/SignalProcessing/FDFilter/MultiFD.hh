/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef MULTIFD_HH
#define MULTIFD_HH

#include "FDFilterBase.hh"
#include <vector>

class FSpectrum;
class FSeries;

/**  The %MultiFD class combines multiple f-domain filters into a single
  *  filter. Filters are added to the MultiFD instance using the addFilter
  *  method. The filters are applied to input frequenxy series in the order
  *  that they were added to the %MultiFD instance.
  */
class MultiFD : public FDFilterBase
{
public:
    using FDFilterBase::Apply;

    /**  Construct an empty %MultiFD instance.
      *  \brief Default constructor.
      */
    MultiFD( void );

    /**  Construct an identical copy of the argument %MultiFD instance.
      *  \brief Copy Constructor
      *  \param mfd %MultiFD instance to be copied.
      */
    MultiFD( const MultiFD& mfd );

    /**  Destroy a MultiFD instance. All cloned filters are deleted.
      *  \brief Destructor.
      */
    virtual ~MultiFD( void );

    /**  Copy the argument %MultiFD to this instance. Any existing f-domain
      *  filters are destroyed.
      *  \brief Assignment operator.
      *  \param mfd %MultiFD instance to be copied.
      *  \return Reference to this instance.
      */
    virtual MultiFD& operator=( const MultiFD& mfd );

    /**  Make an identical copy of this filter.
      *  \brief Clone this %MultiFD instance.
      *  \return Pointer to the new filter instance.
      */
    virtual MultiFD* clone( void ) const;

    /**  Apply the filter(s) to the specified %FSeries.
      *  \brief Apply the filter to an %FSeries.
      *  \param fin  Constant reference to the input %FSeries.
      *  \param fout Reference to the output %FSeries.
      */
    virtual void Apply( const FSeries& fin, FSeries& fout );

    /**  Apply the filter(s) to the specified %FSpectrum.
      *  \brief Apply the filter to an %FSpectrum.
      *  \param fin  Constant reference to the input %FSpectrum.
      *  \param fout Reference to the output %FSpectrum.
      */
    virtual void Apply( const FSpectrum& fin, FSpectrum& fout );

    /**  Apply the filter(s) to the specified %DFT.
      *  \brief Apply the filter to an %DFT.
      *  \param fin  Constant reference to the input DFT.
      *  \param fout Reference to the output %DFT.
      */
    virtual void Apply( const containers::DFT& fin, containers::DFT& fout );

    /**  Apply the filter(s) to the specified %PSD.
      *  \brief Apply the filter to an %PSD.
      *  \param fin  Constant reference to the input %PSD.
      *  \param fout Reference to the output %PSD.
      */
    virtual void Apply( const containers::PSD& fin, containers::PSD& fout );

    /**  Clone the argument filter and add it to the list of f-Domain filters
      *  to be applied by the %MultiFD.
      *  \brief Add a filter
      *  \param fdf Filter to be cloned and added to this %MultiFD instance.
      */
    void addFilter( const FDFilterBase& fdf );

private:
    void delete_filters( void );

private:
    typedef std::vector< FDFilterBase* > filter_vect;
    typedef filter_vect::iterator        filter_iter;
    typedef filter_vect::const_iterator  const_filter_iter;

    filter_vect mFilter;
};

#endif // MULTIFD_HH
