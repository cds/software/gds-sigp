/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef FDRESAMPLE_HH
#define FDRESAMPLE_HH

#include "FDFilterBase.hh"

/**  FDResample changes the sampling rate of a spectrum to a specified value.
  *  The resampling is accomplished by extending or shrinking the input
  *  
  *  @memo Frequency domain resampling filter.
  *  @author J. Zweizig
  *  @version 1.0; Last modified September 28, 2004
  */
class FDResample : public FDFilterBase
{
public:
    using FDFilterBase::Apply;

    /**  Construct a Fourier domain resampling filter.
      *  @memo FDResampl constructor
      *  @param rate output sampling rate.
      */
    FDResample( double rate );

    /** Destroy the resampling filter.
     */
    virtual ~FDResample( void );

    /**  The input FSeries is extended or truncated as appropriate for the
      *  specified sample rate.
      *  @memo Resample an FSeries
      *  @param in  Input FSeries.
      *  @param out Resampled FSeries.
      */
    virtual void Apply( const FSeries& in, FSeries& out );

    /**  The input FSpectrum is extended or truncated as appropriate for the
      *  specified sample rate.
      *  @memo Resample an FSpectrum
      *  @param in  Input FSpectrum.
      *  @param out Resampled FSpectrum.
      */
    virtual void Apply( const FSpectrum& in, FSpectrum& out );

    /**  The input DFT is extended or truncated as appropriate for the
      *  specified sample rate.
      *  @memo Resample an FSeries
      *  @param in  Input FSeries.
      *  @param out Resampled FSeries.
      */
    virtual void Apply( const containers::DFT& in, containers::DFT& out );

    /**  The input PSD is extended or truncated as appropriate for the
      *  specified sample rate.
      *  @memo Resample an PSD
      *  @param in  Input PSD.
      *  @param out Resampled PSD.
      */
    virtual void Apply( const containers::PSD& in, containers::PSD& out );

    /**  Create an identical resampling filter.
      *  @memo Clone a resampling filter.
      *  @return Pointer to a copy of this resampling filter.
      */
    virtual FDResample* clone( void ) const;

    /**  Get the sample rate to which the result series will be resampled.
      *  @memo Get the sample rate
      *  @return Sample rate in Hz
      */
    double getSampleRate( void ) const;

    /**  Set the sample rate to which the result series will be resampled.
      *  @memo Set the sample rate
      *  @param rate Sample rate in Hz
      */
    void setSampleRate( double rate );

private:
    double mSampleRate;
};

//=======================================  Inline method(s)
inline double
FDResample::getSampleRate( void ) const
{
    return mSampleRate;
}

#endif // !defined(FDRESAMPLE_HH)
