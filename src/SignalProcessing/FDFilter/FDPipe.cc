/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "FDPipe.hh"
#include "DVecType.hh"
#include <stdexcept>
#include <iostream>

using namespace std;
using namespace containers;

//======================================  Constructor
FDPipe::FDPipe( const FDFilterBase& fd, mode_flags mf, Interval tChomp )
    : mMode( mf ), mStartTime( 0 ), mCurrentTime( 0 ), mChomp( tChomp )
{
    setFilter( fd );
}

//======================================  Copy constructor
FDPipe::FDPipe( const FDPipe& fp )
{
    *this = fp;
}

//======================================  Default constructor
FDPipe::FDPipe( void ) : mMode( discrete ), mStartTime( 0 )
{
}

//======================================  Destructor
FDPipe::~FDPipe( void )
{
    reset( );
}

//======================================  Apply a filter
void
FDPipe::filter( const TSeries& ts )
{
    DFT tfs( ts );
    //tfs.Dump(cout);
    mFilter->Apply( tfs, tfs );
    //tfs.Dump(cout);
    mFiltered = tfs.iFFT( );
}

//======================================  Apply a filter
void
FDPipe::filterAdd( const TSeries& ts )
{
    mFiltered += mFilter->Apply( DFT( ts ) ).iFFT( );
}

//======================================  Set the multiplexer window
void
FDPipe::setMux( Interval tStep )
{
    long    N = long( mDt / tStep + 0.5 );
    DVectD* sindv = new DVectD( N );
    double* data = sindv->refTData( );
    double  dPhi = M_PI / double( N );
    for ( long i = 0; i < N; ++i )
        *data++ = pow( sin( double( i ) * dPhi ), 2 );
    mSinSq.reset( sindv );
}

//======================================  Apply a filter
TSeries
FDPipe::apply( const TSeries& ts )
{
    // cout << "Apply a FDFilter to a TSeries (len " << ts.getInterval()
    //	 << ") ...";
    // cout.flush();
    if ( ts.isEmpty( ) )
        return ts;

    //---------------------------------  Set up internals on first pass
    if ( !mStartTime )
    {
        mStartTime = ts.getStartTime( );
        mCurrentTime = mStartTime;
        if ( mChomp )
            mDt = mChomp;
        else
            mDt = ts.getInterval( );
        mTStep = ts.getTStep( );
        if ( mMode == inmux )
            setMux( mTStep );
        mData = ts;

        //---------------------------------  Check valid data, exception if not
    }
    else if ( ts.isEmpty( ) && !mData.isEmpty( ) )
    {
        return flush( );
    }
    else
    {
        dataCheck( ts );
        mData.Append( ts );
    }

    //----------------------------------  Append the input
    TSeries  r;
    Interval dT2 = 0.5 * mDt;
    Interval dT4 = 0.25 * mDt;
    long     N2 = long( mDt / mTStep + 0.5 ) / 2;
    Time     t0 = mCurrentTime;

    //----------------------------------  Select overlap mode
    switch ( mMode )
    {

    //----------------------------------  Filter data as it arrives.
    case discrete:
        while ( t0 + mDt <= mData.getEndTime( ) )
        {
            // cout << "discrete loop, mDt=" << mDt << endl;
            filter( mData.extract( t0, mDt ) );
            r.Append( mFiltered );
            t0 += mDt;
        }
        break;

    //----------------------------------  Return only central 50% of data.
    case overlap:
        if ( t0 == mStartTime )
        {
            filter( mData.extract( t0, mDt ) );
            r = mFiltered.extract( t0, 0.75 * mDt );
            t0 += dT2;
        }
        while ( t0 + mDt <= mData.getEndTime( ) )
        {
            // cout << "overlap loop, dT2=" << dT2 << endl;
            filter( mData.extract( t0, mDt ) );
            r.Append( mFiltered.extract( t0 + dT4, dT2 ) );
            t0 += dT2;
        }
        break;

    //----------------------------------  Premix data
    case inmux: {
        //------------------------------  First data segment: get data from
        //                                the first data stride, multiply it
        //                                by the (sin^2) windowing function.
        //                                perform the filter function and
        //                                save the result in temp (_/\_).
        if ( t0 == mStartTime )
        {
            TSeries temp( mData.extract( t0, mDt ) );
            temp.refDVect( )->mpy( N2, *mSinSq, N2, N2 );
            filter( temp );
            t0 += dT2;
        }

        //------------------------------  For each stride fetch a stride con-
        //                                sisting of the last half of the
        //                                previous segment and the first half
        //                                of the current. Window and filter
        //                                resulting stride and a result to
        //                                the present stride.
        while ( t0 + mDt <= mData.getEndTime( ) )
        {
            // cout << "inmux loop, mDt=" << mDt << endl;
            TSeries temp( mData.extract( t0, mDt ) );
            *( temp.refDVect( ) ) *= *mSinSq;
            mFiltered.extend( t0 + mDt );
            filterAdd( temp );
            t0 += dT2;
        }

        //------------------------------  output data start a half-stride
        //                                before the current data.
        Time     tOut = mFiltered.getStartTime( );
        Interval dTout = t0 - tOut;
        r = mFiltered.extract( tOut, dTout );
        mFiltered.eraseStart( dTout );
    }
    break;

    //----------------------------------  postmix overlapping data.
    case outmux: {
        if ( t0 == mStartTime )
        {
            filter( mData.extract( t0, mDt ) );
            setMux( mFiltered.getTStep( ) ); // in case rate is different.
            N2 = long( mDt / mFiltered.getTStep( ) + 0.5 ) / 2;
            mFiltered.refDVect( )->mpy( N2, *mSinSq, N2, N2 );
            r = mFiltered.extract( t0, dT2 );
            mFiltered.eraseStart( dT2 );
            t0 += dT2;
        }
        while ( t0 + mDt <= mData.getEndTime( ) )
        {
            TSeries temp = mFiltered;
            filter( mData.extract( t0, mDt ) );
            *mFiltered.refDVect( ) *= *mSinSq;
            temp += mFiltered.extract( t0, dT2 );
            r.Append( temp );
            mFiltered.eraseStart( dT2 );
            t0 += dT2;
        }
    }
    break;
    }
    mCurrentTime = t0;
    mData.eraseStart( t0 - mData.getStartTime( ) );
    // cout << " Done!" << endl;
    return r;
}

//======================================  Create an identical copy of the pipe
FDPipe*
FDPipe::clone( void ) const
{
    return new FDPipe( *this );
}

//======================================  Check the input series is compatible
void
FDPipe::dataCheck( const TSeries& ts ) const
{
    if ( !mStartTime )
        return;
    if ( mDt != ts.getInterval( ) )
    {
        throw runtime_error( "FDPipe: Bad input TSeries length" );
    }
    else if ( mTStep != ts.getTStep( ) )
    {
        throw runtime_error( "FDPipe: Bad TSeries sample rate" );
    }
    else if ( mData.getEndTime( ) != ts.getStartTime( ) )
    {
        throw runtime_error( "FDPipe: Bad input TSeries start time" );
    }
}

//======================================  Flush remaining data from the pipe.
TSeries
FDPipe::flush( void )
{
    TSeries r;
    Time    t0 = mData.getStartTime( );
    switch ( mMode )
    {
    case discrete:
        break;
    case overlap:
        mData.extend( t0 + mDt );
        filter( mData );
        r = mFiltered.extract( t0 + 0.25 * mDt, 0.25 * mDt );
        break;
    case inmux: {
        long N2 = long( mDt / mTStep + 0.5 ) / 2;
        mData.extend( t0 + mDt );
        mData.refDVect( )->mpy( 0, *mSinSq, 0, N2 );
        mFiltered.extend( t0 + mDt );
        filterAdd( mData );
        r = mFiltered.extract( t0, 0.5 * mDt );
    }
    break;
    case outmux: {
        r = mFiltered;
        mData.extend( t0 + mDt );
        filter( mData );
        long N2 = long( mDt / mTStep + 0.5 ) / 2;
        mFiltered.ReSize( N2 );
        mFiltered.refDVect( )->mpy( 0, *mSinSq, 0, N2 );
        r += mFiltered;
    }
    break;
    }
    reset( );
    return r;
}

//======================================  Copy the state from another pipe
FDPipe&
FDPipe::operator=( const FDPipe& fp )
{
    if ( fp.mFilter.get( ) )
        mFilter.reset( fp.mFilter->clone( ) );
    else
        mFilter.reset( 0 );
    mMode = fp.mMode;
    mStartTime = fp.mStartTime;
    mCurrentTime = fp.mCurrentTime;
    mChomp = fp.mChomp;
    mDt = fp.mDt;
    mTStep = fp.mTStep;
    if ( fp.mSinSq.get( ) )
        mSinSq.reset( fp.mSinSq->clone( ) );
    else
        mSinSq.reset( 0 );
    mData = fp.mData;
    mFiltered = fp.mFiltered;
    return *this;
}

//======================================  Reset the filter state
void
FDPipe::reset( void )
{
    mStartTime = Time( 0 );
    mData.Clear( );
    mFiltered.Clear( );
    mSinSq.reset( 0 );
}

void
FDPipe::setChomp( Interval chomp )
{
    mChomp = chomp;
}

void
FDPipe::setFilter( const FDFilterBase& fd )
{
    mFilter.reset( fd.clone( ) );
}

void
FDPipe::setMode( mode_flags mode )
{
    mMode = mode;
}
