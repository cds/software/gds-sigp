/* -*- mode: c++; c-basic-offset: 4; -*- */
///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                                LPEFilter.cc                               //
//                                                                           //
//               Linear predictive error filter implementation               //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include "LPEFilter.hh"
#include "DVecType.hh"
#include "lcl_array.hh"
#include "fft.hh"
#include "gds_veclib.hh"
#include <iostream>

// #define PRINT_TIMER 1
#ifdef PRINT_TIMER
#include "usertime.hh"
using namespace std;
#endif

//======================================  Default constructor.
LPEFilter::LPEFilter( int filterLength, int trainPeriod, int trainLength )
    : FIRdft( ), trainPeriod( trainPeriod ), trainLength( trainLength ),
      trainTime( 0 )
{
    setLength( filterLength );
}

//======================================  Copy constructor.
LPEFilter::LPEFilter( const LPEFilter& lpef )
    : FIRdft( lpef ), trainPeriod( lpef.trainPeriod ),
      trainLength( lpef.trainLength ), trained( false ), trainTime( 0 )
{
    reset( );
}

//======================================  Destructor.
LPEFilter::~LPEFilter( )
{
}

//======================================  Clone
LPEFilter*
LPEFilter::clone( void ) const
{
    return new LPEFilter( *this );
}

//======================================  Assignment operator
LPEFilter&
LPEFilter::operator=( const LPEFilter& lpef )
{

    if ( this != &lpef )
    {
        this->FIRdft::operator=( lpef );
        trainPeriod = lpef.trainPeriod;
        trainLength = lpef.trainLength;
        trainTime = Time( 0 );
        reset( );
    }

    return *this;
}

//======================================  Apply filter to TSeries
TSeries&
LPEFilter::apply( const TSeries& in, TSeries& out )
{

    if ( !trainTime )
    {
        trained = !train( in );
    }
    else if ( trainPeriod != 0 &&
              in.getStartTime( ) >= trainTime + Interval( trainPeriod ) )
    {
        trained = !train( in );
    }
    else
    {
        trained = false;
    }

    FIRdft::apply( in, out );

    return out;
}

//======================================  Determine filter coefficients
int
LPEFilter::train( const TSeries& data )
{
    int M = getLength( );
    if ( !M )
    {
        std::cerr << "LPEFilter: filter length not defined" << std::endl;
        return -1;
    }

    if ( !trainLength )
    {
        std::cerr << "LPEFilter: training length not defined" << std::endl;
        return -1;
    }

    if ( trainLength < M )
    {
        std::cerr << "LPEFilter: insufficient training length" << std::endl;
        return -1;
    }

    if ( int( data.getNSample( ) ) < trainLength )
    {
        std::cerr << "LPEFilter: insufficient training data" << std::endl;
        return -1;
    }

#ifdef PRINT_TIMER
    cout << "Train LPE filter, length=" << M << ", lTrain=" << trainLength
         << endl;
    user_timer tusr;
#endif

    lcl_array< double > x( trainLength );
    data.getData( trainLength, x );

    lcl_array< double > r( M );
    autocorr( x, r );

#ifdef PRINT_TIMER
    cout << "Autocorrelation complete, elapsed=" << tusr.get_time( ) << endl;
#endif

    lcl_array< double > c( M );
    levinson( r, c );

#ifdef PRINT_TIMER
    cout << "Levinson complete, elapsed=" << tusr.get_time( ) << endl;
#endif

    setCoefs( c );

    setRate( 1.0 / data.getTStep( ) );

    trainTime = data.getStartTime( );

    return 0;
}

//======================================  Compute (biased) autocorrelation vect
#define FFT_AUTOCORR 1
// #undef FFT_AUTOCORR
void
LPEFilter::autocorr( const double* x, double* r ) const
{
    int M = getLength( );
#ifndef FFT_AUTOCORR
    //---------------------------------- Biased autocorrelation - needed for
    //                                   levinson stability
    for ( int lag = 0; lag < M; ++lag )
    {
        double sum = 0.0;
        for ( int sample = lag; sample < trainLength; ++sample )
        {
            sum += x[ sample ] * x[ sample - lag ];
        }
        r[ lag ] = sum / double( trainLength );
    }
#else
    long               lTrans = wfft_pick_length( trainLength + M );
    DVecType< double > dv( trainLength, x );
    static_cast< DVector& >( dv ).Extend( lTrans );
    long                 lDft = lTrans / 2 + 1;
    DVecType< dComplex > dft( lDft );
    wfft( dv.refTData( ), dft.refTData( ), lTrans );
    dComplex* p = dft.refTData( );
    for ( long i = 0; i < lDft; ++i )
    {
        *p = norm( *p );
        p++;
    }
    wfft( dft.refTData( ), dv.refTData( ), lTrans );

    //---------------------------------- This is a biased autocorrelation
    //                                   and is needed for levinson stability
    double den = double( lTrans ) * double( trainLength );
    for ( long i = 0; i < M; ++i )
    {
        r[ i ] = dv[ i ] / den;
    }
#endif
}

//======================================  Solve symmetric Yule-Walker problem
void
LPEFilter::levinson( const double* r, double* c ) const
{

    //--------------------------------  Reverse r for easy access.
    int                 M = getLength( ) - 1;
    lcl_array< double > rr( M + 1 );
    for ( int i = 0; i <= M; i++ )
        rr[ i ] = r[ M - i ];

    c[ 0 ] = 1;
    c[ 1 ] = -r[ 1 ] / r[ 0 ];

    for ( int m = 1; m < M; ++m )
    {

        //-----------------------------  Calculate c[m+1]
        double cm1 = -vdot( rr + M - m - 1, c, m + 1 ) / vdot( c, r, m + 1 );

        double* pl = c + 1;
        double* ph = c + m;
        while ( pl < ph )
        {
            double tmp = cm1 * *ph;
            *ph-- += cm1 * *pl;
            *pl++ += tmp;
        }
        if ( ph == pl )
            *pl += *pl * cm1;
        c[ m + 1 ] = cm1;
    }
}

//======================================  Set Train Period
void
LPEFilter::setTrainPeriod( int period )
{
    trainPeriod = period;
}

//======================================  Set Train Length
void
LPEFilter::setTrainLength( int length )
{
    trainLength = length;
}

//======================================  Set History
void
LPEFilter::setHistory( const TSeries& in )
{

    reset( );

    if ( getRate( ) == 0.0 )
    {
        setRate( 1.0 / in.getTStep( ) );
    }

    FIRdft::setHistory( in );
}

//======================================  Get Train Period
int
LPEFilter::getTrainPeriod( void ) const
{
    return trainPeriod;
}

//======================================  Get Train Length
int
LPEFilter::getTrainLength( void ) const
{
    return trainLength;
}

//======================================  Get Train Time
Time
LPEFilter::getTrainTime( void ) const
{
    return trainTime;
}

//======================================  Get Transient Time
Interval
LPEFilter::getTransientTime( void ) const
{
    return getLength( ) / getRate( );
}
