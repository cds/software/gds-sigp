/* -*- mode: c++; c-basic-offset: 4; -*- */
///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                                LPEFilter.hh                               //
//                                                                           //
//               Linear predictive error filter implementation               //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#ifndef LPEFILTER_HH
#define LPEFILTER_HH

#include "FIRdft.hh"

/**  Linear predictive error filters are FIR filters whose coefficients
  *  are optimized in a least-squares sense to predict and remove the
  *  stationary components of a signal.  It is a useful technique for
  *  removing line frequencies or a constant background from a signal and
  *  provides a simple method to rapidly identify transient events.
  *
  *  The LPEFilter class implements a linear predictive error filter based
  *  on the FIRdft class.  In addition to the methods of FIRdft,
  *  LPEFilter provides the following functionality.
  *
  *  A method is provided to determine the optimal filter coefficients
  *  based on training data, while ensuring that the filter history is
  *  retained.
  *
  *  Initial training is automatically performed when the filter is applied
  *  to data for the first time.  Subsequent periodic retraining is
  *  automatically performed at predefined intervals.  In addition, the
  *  user can call the training functions directly.
  *
  *  Like FIRdft, LPEFilter operates on successive blocks of data.  To
  *  ensure that retraining does not occur across succesive data blocks,
  *  the user specified retraining period should be an integer multiple of
  *  the block duration.
  *
  *  All data are promoted to type double prior to filtering.
  *
  *  @memo Linear Predictive Error Filter implementation.
  *  @author Shourov K. Chatterji
  *  @version 1.2; Last modified: January 14, 2005
  */
class LPEFilter : public FIRdft
{

public:
    using Pipe::apply;

    /**  Construct a new filter with the given filter length, retraining
  *  period, and training length.  The filter history and start times
  *  are cleared.
  *  @memo Default constructor.
  *  @param filterLength Filter length in samples
  *  @param trainPeriod  Retraining period in seconds.
  *  @param trainLength  Length of training data in samples.
  */
    LPEFilter( int filterLength = 0, int trainPeriod = 0, int trainLength = 0 );

    /**  Creates new filter with the same filter length, retraining period,
  *  training length, sample rate, and filter coefficients as the given
  *  filter.  The filter history and start times are cleared.
  *  @memo Copy constructor.
  *  @param lpef %LPEFilter to be copied
  */
    LPEFilter( const LPEFilter& lpef );

    /**  Destroys the filter and releases any allocated memory.
  *  @memo Destructor.
  */
    virtual ~LPEFilter( void );

    /**  Make an identical copy (clone) of an %LPEfilter
  *  @memo Clone an %LPEFilter..
  *  @return Pointer to the new copy of the filter.
  */
    LPEFilter* clone( void ) const;

    /**  Creates new filter with the same filter length, retraining period,
  *  training length, sample rate, and filter coefficients as the given
  *  filter.  The filter history and start times are cleared.
  *  @memo Assignment operator.
  *  @param lpef LPE filter to be copied
  *  @return reference to this LPEFilter.
  */
    LPEFilter& operator=( const LPEFilter& lpef );

    /**  Filters an input TSeries and places the result in the given output
  *  TSeries.  Before filtering, training is performed if the filter has
  *  never been trained or if the retrain period has elapsed. If retraining
  *  is successful, the retrained status flag is set. The filter history is
  *  updated to reflect all processed data.
  *  @memo Filter TSeries
  *  @param in TSeries holding input data.
  *  @param out Output TSeries.
  *  @return Reference to the output series.
  */
    TSeries& apply( const TSeries& in, TSeries& out );

    /**  Determine the optimal filter coefficients based on training data.
  *  The entire TSeries is used as the training data.  The training data
  *  length must be greater than the filter length.  A non-zero return value
  *  indicates that the training was unsuccessful.
  *  @memo Determine filter coefficients
  *  @param data TSeries containing training data
  *  @return Non-zero on error
  */
    int train( const TSeries& data );

    /**  Test whether the filter was retrained before the last apply. The 
  *  \c retrained() flag is reset at the start of each apply call. If the
  *  training period has not been set or the training period (automatic
  *  retraining interval) has elapsed, the filter is retrained before 
  *  filtering the input time series and the retained flag is set.
  *  @memo Test whether the filter was retrained.
  *  @return True if successful retraining occurred before the last apply.
  */
    bool retrained( void ) const;

    /**  Sets the length of time (in seconds) between trainings.  To ensure
  *  that training does not occur across the boundary between successive
  *  data blocks, the retraining period should be an integer multiple of
  *  the data block duration.  A value of zero prevents automatic periodic
  *  retraining of the filter.
  *  @memo Set retraining period
  *  @param period Retraining period [seconds]
  */
    void setTrainPeriod( int period );

    /**  Sets the amount of data (in samples) which is used to train the filter.
  *  To determine the filter coefficients, the training algorithm requires a
  *  training length greater than the filter length.
  *  @memo Set training length
  *  @param length Training length [samples]
  */
    void setTrainLength( int length );

    /**  Sets the filter history by applying the filter to a TSeries.
  *  @memo Set History from TSeries
  *  @param in TSeries data
  */
    void setHistory( const TSeries& in );

    /**  Gets the length of time (in seconds) between trainings.
  *  @memo Get retraining period
  *  @return Retraining period [seconds]
  */
    int getTrainPeriod( void ) const;

    /**  Gets the amount of data (in samples) which is used to train the filter.
  *  @memo Get training length
  *  @return Training length [samples]
  */
    int getTrainLength( void ) const;

    /**  Gets the time of the most recent filter training.  The train time is
  *  set automatically whenever the filter is trained.
  *  @memo Get time of most recent training
  *  @return Time of last training
  */
    Time getTrainTime( void ) const;

    /**  Gets the length in seconds of the filter's impulse response.  Determines
  *  the dead time after an impulsive event, during which the filter output
  *  is unreliable.
  *  @memo Get the filter length in seconds.
  *  @return Filter length in seconds.
  */
    Interval getTransientTime( void ) const;

private:
    /**  Determines the first M autocorrelation values of the sequence x, where
  *  M is the filter length. The input sequence length is the filter training 
  *  length.
  *  @memo Compute autocorrelation vector
  *  @param x Training data vector
  *  @param r Autocorrelation vector
  */
    void autocorr( const double* x, double* r ) const;

    /**  Solves the symmetric Yule-Walker problem using Levinson-Durbin
  *  recursion to determine the filter coefficients.
  *  @memo Solve symmetric Yule-Walker problem
  *  @param r Autocorrelation vector
  *  @param c Filter coefficients (returned)
  */
    void levinson( const double* r, double* c ) const;

private:
    /**  The number of seconds between filter trainings.  A zero value
  *  indicates that the filter should not be retrained automatically.
  *  @memo Retraining Period [seconds]
  */
    int trainPeriod;

    /**  The length of data used for training the filter.
  *  @memo Training Length [samples]
  */
    int trainLength;

    /**  Flag set if the filter was automatically retrained in the previous
  *  call to apply().
  *  @memo Training flag
  */
    bool trained;

    /**  The time of the most recent training.
  *  @memo Last training time
  */
    Time trainTime;
};

//======================================  Test the retrained flag
inline bool
LPEFilter::retrained( void ) const
{
    return trained;
}

#endif // LPEFILTER_HH
