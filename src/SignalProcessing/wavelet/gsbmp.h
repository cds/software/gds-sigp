/*
File: gsbmp.h
Descr: declaration basic structures of *.bmp format and gs_bm_write(...).
Target: writing gray scale (8-bit 256 colors) bitmap from unsigned char * array.
Developer: Stadnik Alexei(maito:stadnik@cv.jinr.ru;http://www.jinr.ru/~stadnik/).
Date: 2 April 2000 year.
*/

#ifndef __BITMAP_H__
#define __BITMAP_H__

typedef struct tagbmfh
{ // bmfh
    unsigned short bfType;
    unsigned long  bfSize;
    unsigned short bfReserved1;
    unsigned short bfReserved2;
    unsigned long  bfOffBits;
} bmfh;

typedef struct tagbmih
{ // bmih
    unsigned long  biSize;
    long           biWidth;
    long           biHeight;
    unsigned short biPlanes;
    unsigned short biBitCount;
    unsigned long  biCompression;
    unsigned long  biSizeImage;
    long           biXPelsPerMeter;
    long           biYPelsPerMeter;
    unsigned long  biClrUsed;
    unsigned long  biClrImportant;
} bmih;

typedef struct tagrgbq
{ // rgbq
    unsigned char rgbBlue;
    unsigned char rgbGreen;
    unsigned char rgbRed;
    unsigned char rgbReserved;
} rgbq;

int gs_bm_write( const char*    name,
                 unsigned char* lpb,
                 unsigned int   w,
                 unsigned int   h,
                 unsigned int   palette = 0 );

#endif
