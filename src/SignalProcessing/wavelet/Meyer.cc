// Wavelet Analysis Tool
//--------------------------------------------------------------------
// Implementation of
// Meyer wavelets using Fast Wavelet Transform
// References:
//--------------------------------------------------------------------
//$Id$

#define MEYER_CC

#include "Meyer.hh"
#include <iostream>

//namespace datacondAPI {
//namespace wat {

extern const double mey[ 62 ] = {
    0.0000000000000000e+00,  -1.0099999569414229e-12, 8.5194596367962140e-09,
    -1.1119449525952780e-08, -1.0798819539621958e-08, 6.0669757413511352e-08,
    -1.0866516536735883e-07, 8.2006806503864813e-08,  1.1783004497663934e-07,
    -5.5063405652522782e-07, 1.1307947017916706e-06,  -1.4895492164971559e-06,
    7.3675728859037460e-07,  3.2054419133447798e-06,  -1.6312699734552807e-05,
    6.5543059305751491e-05,  -6.0115023435160925e-04, -2.7046721246437250e-03,
    2.2025341009110021e-03,  6.0458140973233040e-03,  -6.3877183184971563e-03,
    -1.1061496392513451e-02, 1.5270015130934803e-02,  1.7423434103729693e-02,
    -3.2130793990211758e-02, -2.4348745906078023e-02, 6.3739024322801596e-02,
    3.0655091960824263e-02,  -1.3284520043622938e-01, -3.5087555656258346e-02,
    4.4459300275757724e-01,  7.4458559231880628e-01,  4.4459300275757724e-01,
    -3.5087555656258346e-02, -1.3284520043622938e-01, 3.0655091960824263e-02,
    6.3739024322801596e-02,  -2.4348745906078023e-02, -3.2130793990211758e-02,
    1.7423434103729693e-02,  1.5270015130934803e-02,  -1.1061496392513451e-02,
    -6.3877183184971563e-03, 6.0458140973233040e-03,  2.2025341009110021e-03,
    -2.7046721246437250e-03, -6.0115023435160925e-04, 6.5543059305751491e-05,
    -1.6312699734552807e-05, 3.2054419133447798e-06,  7.3675728859037460e-07,
    -1.4895492164971559e-06, 1.1307947017916706e-06,  -5.5063405652522782e-07,
    1.1783004497663934e-07,  8.2006806503864813e-08,  -1.0866516536735883e-07,
    6.0669757413511352e-08,  -1.0798819539621958e-08, -1.1119449525952780e-08,
    8.5194596367962140e-09,  -1.0099999569414229e-12
};

// constructors

template < class DataType_t >
Meyer< DataType_t >::Meyer( const Wavelet& w ) : WaveDWT< DataType_t >( w )
{
    setFilter( );
}

template < class DataType_t >
Meyer< DataType_t >::Meyer( const Meyer< DataType_t >& w )
    : WaveDWT< DataType_t >( w )
{
    setFilter( );
}

template < class DataType_t >
Meyer< DataType_t >::Meyer( int tree, enum BORDER border )
    : WaveDWT< DataType_t >( 62, 62, tree, border )
{
    setFilter( );
}

// destructor
template < class DataType_t >
Meyer< DataType_t >::~Meyer( )
{
    if ( pLForward )
        delete[] pLForward;
    if ( pLInverse )
        delete[] pLInverse;
    if ( pHForward )
        delete[] pHForward;
    if ( pHInverse )
        delete[] pHInverse;
}

// clone
template < class DataType_t >
Meyer< DataType_t >*
Meyer< DataType_t >::Clone( ) const
{
    return new Meyer< DataType_t >( *this );
}

template < class DataType_t >
void
Meyer< DataType_t >::setFilter( )
{
    const double* pF = mey;

    pLInverse = new double[ this->m_H ];
    pLForward = new double[ this->m_H ];
    pHInverse = new double[ this->m_H ];
    pHForward = new double[ this->m_H ];

    for ( int i = 0; i < this->m_H; i += 2 )
    {
        pLForward[ i ] = pF[ i ];
        pLForward[ i + 1 ] = pF[ i + 1 ];
        pHForward[ i ] = -pF[ this->m_H - 1 - i ];
        pHForward[ i + 1 ] = pF[ this->m_H - 2 - i ];

        pLInverse[ i ] = pF[ this->m_H - 1 - i ];
        pLInverse[ i + 1 ] = pF[ this->m_H - 2 - i ];
        pHInverse[ i ] = pF[ i ];
        pHInverse[ i + 1 ] = -pF[ i + 1 ];
    }

    this->m_WaveType = MEYER;
}

// forward function does one step of forward transformation.
// <level> input parameter is the level to be reconstructed
// <layer> input parameter is the layer to be reconstructed.
template < class DataType_t >
void
Meyer< DataType_t >::forward( int level, int layer )
{
    this->forwardFWT( level, layer, pLForward, pHForward );
}

// inverse function does one step of inverse transformation.
// <level> input parameter is the level to be reconstructed
// <layer> input parameter is the layer to be reconstructed.
template < class DataType_t >
void
Meyer< DataType_t >::inverse( int level, int layer )
{
    this->inverseFWT( level, layer, pLInverse, pHInverse );
}

// instantiations

#define CLASS_INSTANTIATION( class_ ) template class Meyer< class_ >;

CLASS_INSTANTIATION( float )
CLASS_INSTANTIATION( double )
//CLASS_INSTANTIATION(std::complex<float>)
//CLASS_INSTANTIATION(std::complex<double>)

#undef CLASS_INSTANTIATION

//}  // end namespace wat
//}  // end namespace datacondAPI
