/*
File: gsbmp.cc(cpp)
Descr: implementation of gs_bm_write(const char *, unsigned char *, unsigned int , unsigned int )
Writing gray scale (8-bit 256 colors) bitmap from unsigned char * array.
Developer: Stadnik Alexei(maito:stadnik@cv.jinr.ru;http://www.jinr.ru/~stadnik/).
Date: 2 April 2000 year.
*/

#include <time.h>
#include <stdio.h>
#include "gsbmp.h"
#include <iostream>
#include <fstream>

//------------------------------------------------

int
gs_bm_write( const char*    name,
             unsigned char* lpb,
             unsigned int   w,
             unsigned int   h,
             unsigned int   palette )
{
    unsigned long  colors_table_size = 256;
    unsigned short bit_count = 8;
    unsigned long  bm_size = ( w * h * bit_count + 4 ) / 8;

    bmfh* lpbmfh = new bmfh;
    bmih* lpbmih = new bmih;
    rgbq* lpbmiColors = new rgbq[ colors_table_size ];

    lpbmfh->bfType = 19778; //BM
    lpbmfh->bfReserved1 = 0;
    lpbmfh->bfReserved2 = 0;
    lpbmfh->bfOffBits = /*sizeof(bmfh)*/ 14 + sizeof( bmih ) +
        sizeof( rgbq ) * colors_table_size; //1078;
    lpbmfh->bfSize = lpbmfh->bfOffBits + bm_size;

    lpbmih->biSize = sizeof( bmih ); //40
    lpbmih->biWidth = w;
    lpbmih->biHeight = h;
    lpbmih->biPlanes = 1;
    lpbmih->biBitCount = bit_count;
    lpbmih->biCompression = 0;
    lpbmih->biSizeImage = bm_size;
    lpbmih->biXPelsPerMeter = 0;
    lpbmih->biYPelsPerMeter = 0;
    lpbmih->biClrUsed = colors_table_size;
    lpbmih->biClrImportant = colors_table_size;

    if ( palette == 0 )
    {
        for ( unsigned long i = 0; i < colors_table_size; i++ )
        {
            lpbmiColors[ i ].rgbBlue = i;
            lpbmiColors[ i ].rgbGreen = i;
            lpbmiColors[ i ].rgbRed = i;
            lpbmiColors[ i ].rgbReserved = 0;
        }
    }
    if ( palette == 1 )
    {
        for ( unsigned long i = 0; i < colors_table_size; i++ )
        {
            lpbmiColors[ i ].rgbBlue = 255;
            lpbmiColors[ i ].rgbGreen = i;
            lpbmiColors[ i ].rgbRed = i;
            lpbmiColors[ i ].rgbReserved = 0;
        }
    }
    if ( palette == 2 )
    {
        for ( unsigned long i = 0; i < colors_table_size; i++ )
        {
            lpbmiColors[ i ].rgbBlue = i;
            lpbmiColors[ i ].rgbGreen = i;
            lpbmiColors[ i ].rgbRed = 255;
            lpbmiColors[ i ].rgbReserved = 0;
        }
    }
    if ( palette == 3 )
    {
        for ( unsigned long i = 0; i < colors_table_size; i++ )
        {
            lpbmiColors[ i ].rgbBlue = i;
            lpbmiColors[ i ].rgbGreen = 255;
            lpbmiColors[ i ].rgbRed = i;
            lpbmiColors[ i ].rgbReserved = 0;
        }
    }
    if ( palette == 4 )
    {
        for ( unsigned long i = 0; i < colors_table_size; i++ )
        {
            lpbmiColors[ i ].rgbBlue = i;
            lpbmiColors[ i ].rgbGreen = 255;
            lpbmiColors[ i ].rgbRed = 255;
            lpbmiColors[ i ].rgbReserved = 0;
        }
    }
    if ( palette == 5 )
    {
        for ( unsigned long i = 0; i < colors_table_size; i++ )
        {
            lpbmiColors[ i ].rgbBlue = 255;
            lpbmiColors[ i ].rgbGreen = i;
            lpbmiColors[ i ].rgbRed = 255;
            lpbmiColors[ i ].rgbReserved = 0;
        }
    }
    if ( palette == 6 )
    {
        for ( unsigned long i = 0; i < colors_table_size; i++ )
        {
            lpbmiColors[ i ].rgbBlue = 255;
            lpbmiColors[ i ].rgbGreen = 255;
            lpbmiColors[ i ].rgbRed = i;
            lpbmiColors[ i ].rgbReserved = 0;
        }
    }
    if ( palette == 7 )
    {
        unsigned long ic = 0;
        for ( unsigned long i = 0; i < colors_table_size; i += 4 )
        {
            lpbmiColors[ ic ].rgbBlue = 255;
            lpbmiColors[ ic ].rgbGreen = i;
            lpbmiColors[ ic ].rgbRed = 0;
            lpbmiColors[ ic++ ].rgbReserved = 0;
        }
        for ( unsigned long i = 0; i < colors_table_size; i += 4 )
        {
            lpbmiColors[ ic ].rgbBlue = 255;
            lpbmiColors[ ic ].rgbGreen = 255;
            lpbmiColors[ ic ].rgbRed = i;
            lpbmiColors[ ic++ ].rgbReserved = 0;
        }
        for ( unsigned long i = 0; i < colors_table_size; i += 4 )
        {
            lpbmiColors[ ic ].rgbBlue = 255 - i;
            lpbmiColors[ ic ].rgbGreen = 255;
            lpbmiColors[ ic ].rgbRed = 255;
            lpbmiColors[ ic++ ].rgbReserved = 0;
        }
        for ( unsigned long i = 0; i < colors_table_size; i += 4 )
        {
            lpbmiColors[ ic ].rgbBlue = 0;
            lpbmiColors[ ic ].rgbGreen = 255 - i;
            lpbmiColors[ ic ].rgbRed = 255;
            lpbmiColors[ ic++ ].rgbReserved = 0;
        }
    }
    if ( palette == 8 )
    {
        unsigned long ic = 0;
        for ( unsigned long i = 0; i < colors_table_size; i += 2 )
        {
            lpbmiColors[ ic ].rgbBlue = i;
            lpbmiColors[ ic ].rgbGreen = i;
            lpbmiColors[ ic ].rgbRed = i;
            lpbmiColors[ ic++ ].rgbReserved = 0;
        }
        for ( unsigned long i = 0; i < colors_table_size; i += 2 )
        {
            lpbmiColors[ ic ].rgbBlue = 255 - i;
            lpbmiColors[ ic ].rgbGreen = 255 - i;
            lpbmiColors[ ic ].rgbRed = 255 - i;
            lpbmiColors[ ic++ ].rgbReserved = 0;
        }
    }

    FILE* fp = fopen( name, "wb" );

    unsigned char wbyte;

    wbyte = 0x00FF & ( lpbmfh->bfType );
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( lpbmfh->bfType ) >> 8;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );

    wbyte = 0x000000FF & ( lpbmfh->bfSize );
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( 0x0000FF00 & ( lpbmfh->bfSize ) ) >> 8;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( 0x00FF0000 & ( lpbmfh->bfSize ) ) >> 16;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( lpbmfh->bfSize ) >> 24;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );

    wbyte = 0x00FF & ( lpbmfh->bfReserved1 );
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( lpbmfh->bfReserved1 ) >> 8;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );

    wbyte = 0x00FF & ( lpbmfh->bfReserved2 );
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( lpbmfh->bfReserved2 ) >> 8;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );

    wbyte = 0x000000FF & ( lpbmfh->bfOffBits );
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( 0x0000FF00 & ( lpbmfh->bfOffBits ) ) >> 8;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( 0x00FF0000 & ( lpbmfh->bfOffBits ) ) >> 16;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( lpbmfh->bfOffBits ) >> 24;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );

    wbyte = 0x000000FF & ( lpbmih->biSize );
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( 0x0000FF00 & ( lpbmih->biSize ) ) >> 8;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( 0x00FF0000 & ( lpbmih->biSize ) ) >> 16;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( lpbmih->biSize ) >> 24;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );

    wbyte = 0x000000FF & ( lpbmih->biWidth );
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( 0x0000FF00 & ( lpbmih->biWidth ) ) >> 8;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( 0x00FF0000 & ( lpbmih->biWidth ) ) >> 16;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( lpbmih->biWidth ) >> 24;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );

    wbyte = 0x000000FF & ( lpbmih->biHeight );
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( 0x0000FF00 & ( lpbmih->biHeight ) ) >> 8;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( 0x00FF0000 & ( lpbmih->biHeight ) ) >> 16;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( lpbmih->biHeight ) >> 24;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );

    wbyte = 0x00FF & ( lpbmih->biPlanes );
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( lpbmih->biPlanes ) >> 8;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );

    wbyte = 0x00FF & ( lpbmih->biBitCount );
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( lpbmih->biBitCount ) >> 8;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );

    wbyte = 0x000000FF & ( lpbmih->biCompression );
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( 0x0000FF00 & ( lpbmih->biCompression ) ) >> 8;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( 0x00FF0000 & ( lpbmih->biCompression ) ) >> 16;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( lpbmih->biCompression ) >> 24;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );

    wbyte = 0x000000FF & ( lpbmih->biSizeImage );
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( 0x0000FF00 & ( lpbmih->biSizeImage ) ) >> 8;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( 0x00FF0000 & ( lpbmih->biSizeImage ) ) >> 16;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( lpbmih->biSizeImage ) >> 24;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );

    wbyte = 0x000000FF & ( lpbmih->biXPelsPerMeter );
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( 0x0000FF00 & ( lpbmih->biXPelsPerMeter ) ) >> 8;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( 0x00FF0000 & ( lpbmih->biXPelsPerMeter ) ) >> 16;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( lpbmih->biXPelsPerMeter ) >> 24;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );

    wbyte = 0x000000FF & ( lpbmih->biYPelsPerMeter );
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( 0x0000FF00 & ( lpbmih->biYPelsPerMeter ) ) >> 8;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( 0x00FF0000 & ( lpbmih->biYPelsPerMeter ) ) >> 16;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( lpbmih->biYPelsPerMeter ) >> 24;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );

    wbyte = 0x000000FF & ( lpbmih->biClrUsed );
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( 0x0000FF00 & ( lpbmih->biClrUsed ) ) >> 8;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( 0x00FF0000 & ( lpbmih->biClrUsed ) ) >> 16;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( lpbmih->biClrUsed ) >> 24;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );

    wbyte = 0x000000FF & ( lpbmih->biClrImportant );
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( 0x0000FF00 & ( lpbmih->biClrImportant ) ) >> 8;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( 0x00FF0000 & ( lpbmih->biClrImportant ) ) >> 16;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );
    wbyte = ( lpbmih->biClrImportant ) >> 24;
    fwrite( (void*)&( wbyte ), sizeof( unsigned char ), 1, fp );

    fwrite( (void*)lpbmiColors, sizeof( rgbq ), colors_table_size, fp );

    fwrite( (void*)lpb, 1, bm_size, fp );

    if ( fp )
        fclose( fp );

    return 0;
}
/*
void main()
{
   unsigned char *bits=new unsigned char[256*256];
   for(int i=0;i<256;i++)for(int j=0;j<256;j++)bits[i*256+j]=(i+j+10)>255?(i+j-255+10):(i+j+10);

   gs_bm_write("test.bmp",bits,256,256);

   delete bits;
}
*/
