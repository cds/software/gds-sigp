/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "cubic_interpolate.hh"
#include "DVecType.hh"
#include "FSeries.hh"
#include <stdexcept>

using namespace std;

//  Cubic interpolation defines a cubic between one point and the next in
//  an equally spaced series. The curve value and slope of each point are
//  used to calculate the cubic coefficients.
//
//  If cubic is y(x) = Ax^3 + Bx^2 + Cx + D at the start of the segment
//  y(0) = D = p0
//  y'(0) = C = s0
//  y(1)  = A + B + C + D = p1
//  y'(1) = 3A + 2B + C = s1
//
//  A = s1 + s0 - 2(p1 - p0)
//  B = 3(p1-p0) - 2s0 - s1
//  C = s0
//  D = p0
//
inline double
cubic_val( double x, double p0, double s0, double p1, double s1 )
{
    double A = s0 + s1 - 2 * ( p1 - p0 );
    double B = 3 * ( p1 - p0 ) - 2 * s0 - s1;
    double C = s0;
    double D = p0;
    return ( ( ( A * x + B ) * x ) + C ) * x + D;
}

//======================================  Default constructor
cubic_interpolate::cubic_interpolate( void )
    : mFactor( 0 ), mFIRmode( fir_mode::fm_zero_phase )
{
    reset( );
}

//======================================  Initializing constructor
cubic_interpolate::cubic_interpolate( int factor )
    : mFIRmode( fir_mode::fm_zero_phase )
{
    reset( );
    setCoefs( factor );
}

//======================================  Copy constructor
cubic_interpolate::cubic_interpolate( const cubic_interpolate& model )
{
    mFactor = model.mFactor;
    mFIRmode = model.mFIRmode;
    if ( model.mCoefs )
        mCoefs.reset( model.mCoefs->clone( ) );
    reset( );
}

//======================================  Destructor
cubic_interpolate::~cubic_interpolate( void )
{
}

//======================================  Clone an interpolation filter
cubic_interpolate*
cubic_interpolate::clone( void ) const
{
    return new cubic_interpolate( *this );
}

//======================================  Check data compatibility
void
cubic_interpolate::dataCheck( const TSeries& ts ) const
{
    if ( !inUse( ) )
        return;
    if ( ts.getStartTime( ) != mCurTime )
    {
        throw runtime_error( "cubic_interpolate: incorrect input start time" );
    }
    else if ( ts.getTStep( ) != mStep )
    {
        throw runtime_error( "cubic_interpolate: incorrect input sample rate" );
    }
}

//======================================  apply time series;
TSeries
cubic_interpolate::apply( const TSeries& in )
{
    TSeries out;
    apply( in, out );
    return out;
}

//======================================  Apply filter to a TSeries
void
cubic_interpolate::apply( const TSeries& in, TSeries& out )
{
    if ( in.empty( ) )
        return;

    //-----------------------------------  Initialize the filter
    if ( !inUse( ) )
    {
        mStep = in.getTStep( );
        mStartTime = in.getStartTime( );
        mCurTime = mStartTime;
        if ( in.isComplex( ) )
            mHistory.reset( new DVectW );
        else
            mHistory.reset( new DVectD );
        mStat = false;
    }

    //-----------------------------------  Else check input series is compatible
    else
    {
        dataCheck( in );
        mStat = ( mCurTime >= mStartTime + getTimeDelay( ) );
    }

    //-----------------------------------  Extend history vector
    size_t N = in.getNSample( );
    mHistory->Extend( ( N + 3 ) * mFactor );

    //-----------------------------------  Add linear terms for each input point
    if ( in.isComplex( ) )
    {
        DVectW addend;
        for ( size_t i = 0; i < N; i++ )
        {
            addend = *mCoefs;
            addend *= in.getComplex( i );
            mHistory->add( i * mFactor, addend );
        }
    }
    else
    {
        DVectD addend;
        for ( size_t i = 0; i < N; i++ )
        {
            addend = *mCoefs;
            addend *= in.getDouble( i );
            mHistory->add( i * mFactor, addend );
        }
    }

    //-----------------------------------  Construct output TSeries
    Time   t0 = in.getStartTime( );
    size_t nOffset = 0;
    size_t nOut = N * mFactor;
    switch ( mFIRmode )
    {
    case fir_mode::fm_zero_phase:
        t0 -= getTimeDelay( );
        break;
    case fir_mode::fm_drop_start:
        t0 -= getTimeDelay( );
        if ( t0 < mStartTime )
        {
            nOffset =
                size_t( ( mStartTime - t0 ) / mStep + 0.5 ); // offset in input
            t0 += double( nOffset ) * mStep;
            if ( nOffset > N )
                nOffset = N;
            nOffset *= mFactor;
            nOut -= nOffset;
        }
        break;
    default:
        break;
    }
    out.setData(
        t0, mStep / double( mFactor ), mHistory->Extract( nOffset, nOut ) );
    mHistory->Erase( 0, N * mFactor );
    mCurTime = in.getEndTime( );
}

//======================================  Clone an interpolation filter
void
cubic_interpolate::reset( void )
{
    mCurTime = Time( 0 );
    mStartTime = Time( 0 );
    mStep = 0;
    mStat = false;
}

//======================================  Set up the cubic factors
void
cubic_interpolate::setCoefs( int upfactor )
{
    reset( );
    mFactor = upfactor;
    if ( !upfactor )
        return;
    size_t  nCoef = 4 * mFactor;
    DVectD* dvd = new DVectD;
    dvd->Extend( nCoef );
    mCoefs.reset( dvd );

    //------------------------------------- Segment 1, y  = p(2)
    //                                                 s1 = (p(2) - p(0)) / 2
    size_t inx = 0;
    double en( mFactor );
    for ( int i = 0; i < mFactor; i++ )
    {
        double x = double( i ) / en;
        //--------------------     p0   s0   p1   s1
        ( *dvd )[ inx++ ] = cubic_val( x, 0.0, 0.0, 0.0, 0.5 );
    }

    //------------------------------------- Segment 2, y  = p(1)
    //                                                 p1 = p(1),
    //                                                 s0 = (p(1) - p(-1)) / 2
    for ( int i = 0; i < mFactor; i++ )
    {
        double x = double( i ) / en;
        ( *dvd )[ inx++ ] = cubic_val( x, 0.0, 0.5, 1.0, 0.0 );
    }

    //------------------------------------- Segment 3, y  = p(0)
    //                                                 p0 = p(0)
    //                                                 s1 = (p(2) - p(0)) / 2
    for ( int i = 0; i < mFactor; i++ )
    {
        double x = double( i ) / en;
        ( *dvd )[ inx++ ] = cubic_val( x, 1.0, 0.0, 0.0, -.5 );
    }

    //------------------------------------- Segment 4, y  = p(-1)
    //                                                 s0 = (p(1) - p(-1)) / 2
    for ( int i = 0; i < mFactor; i++ )
    {
        double x = double( i ) / en;
        ( *dvd )[ inx++ ] = cubic_val( x, 0.0, -.5, 0.0, 0.0 );
    }
    //dvd->Dump(cout);
}

//======================================  Set the time mode.
void
cubic_interpolate::setMode( fir_mode mode )
{
    mFIRmode = mode;
}

//======================================  Calculate the transfer function,
FSeries
cubic_interpolate::Xfer( float Fmin, float Fmax, float dF ) const
{
    DVectC dvc;
    size_t N = size_t( ( Fmax - Fmin ) / dF + 0.5 );
    dvc.Extend( N );
    for ( size_t i = 0; i < N; i++ )
    {
        double f = Fmin + i * dF;
        xfer( dvc[ i ], f );
    }
    FSeries fs( Fmin, dF, Time( 0 ), 1. / dF, dvc );
    return fs;
}

//======================================  Calculate transfer function
bool
cubic_interpolate::xfer( fComplex& coeff, double f ) const
{
    if ( f < 0.5 / mStep )
        coeff = fComplex( 1.0 );
    else
        coeff = 0;
    return true;
}
