/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef CUBIC_INTERPOLATE_HH
#define CUBIC_INTERPOLATE_HH

#include "Complex.hh"
#include "Interval.hh"
#include "fir_filter.hh"
#include <iosfwd>
#include <memory>

class FSeries;
class DVector;

/**  The cubic_interpolate class implements a cubic spline interpolation 
  *  filter that can be used to up-sample time series (TSeries) data by an 
  *  integer factor. Filter class instances carry the signal history as well 
  *  as the filter coefficients to allow data streaming without the 
  *  introduction of edge effects. Each signal (channel) to be filtered must 
  *  use a separate Filter instance.
  *  @memo Cubic spline interpolation filter.
  *  @author  John G. Zweizig
  *  @version 1.0; Last modified: November 24, 2016
  */
class cubic_interpolate : public Pipe
{
public:
    typedef fir_filter::fir_mode fir_mode;

public:
    using Pipe::apply;
    using Pipe::dataCheck;
    using Pipe::Xfer;

    /**  Default constructor. No coefficient or history storage is allocated.
     *  @memo Default constructor.
     */
    cubic_interpolate( void );

    /**  A cubic spline interpolation filter is constructed which will up-sample 
     *  data by the specified multiplier. The filter coefficients are 
     *  initialized and the history data is cleared.
     *  @memo  Construct an empty filter instance.
     *  @param factor Upsampling factor.
     */
    cubic_interpolate( int factor );

    /**  A filter is constructed with the same length, frequency and 
     *  coefficients as the argument Filter. The history is zeroed.
     *  @memo  Copy constructor.
     *  @param model Filter to be copied.
     */
    cubic_interpolate( const cubic_interpolate& model );

    /**  Destroy the filter object and release any memory allocated for
     *  filter coefficients or history information.
     *  @memo Filter destructor.
     */
    virtual ~cubic_interpolate( void );

    /**  Create an identical interpolation filter and return a pointer to the new
     *  filter. The new filter's history is zeroed.
     *  @memo Clone a cubic_interpolation filter.
     *  @return pointer to an identical filter.
     */
    virtual cubic_interpolate* clone( void ) const;

    /**  Test whether the TSeries is valid as input data for the filter.
     *  a runtime_error exception is thrown if the data are invalid.
     *  \brief  Check input data validity.
     *  \param ts %Time series to be checked for consistency with the expected 
     *            input data time and sample rate.
     */
    void dataCheck( const TSeries& ts ) const;

    /**  Get the expected start time of the next TSeries to be filtered.
     *  @memo   Get the current time.
     *  @return Expected time of the next input sample.
     */
    Time getCurrentTime( void ) const;

    /**  Get the sample rate. The filter state is not affected.
     *  @memo   Get the sample rate.
     *  @return The nominal sample rate in Hz.
     */
    double getRate( void ) const;

    /**  Get the start time of this filter run. This is set by the first 
     *  filter operation after the filter has been created or reset.
     *  @memo   Get the start time.
     *  @return true The start time.
     */
    Time getStartTime( void ) const;

    /**  Tests whether the previous filter operation used valid history data.
     *  History data is considered to be valid if the history buffer is full 
     *  and the start time of the input data is equal to the time stamp on the 
     *  history data. The filter state is not affected.
     *  @memo   Test the history status.
     *  @return true if the filter history data was valid for the last 
     *  operation.
     */
    bool getStat( void ) const;

    /**  Get the time delay imposed by the Filter. The time delay is only
     *  non-zero for filters that have a well defined delay i.e. FIR filters.
     *  A positive value indicates that the filter delays a signal by the 
     *  specified time.
     *  @memo Get the time delay.
     *  @return Signal time delay.
     */
    Interval getTimeDelay( void ) const;

    /**  Tests whether the filter is in use.
     *  @memo   Test the filter acitivity status.
     *  @return true if the filter is being used.
     */
    bool inUse( void ) const;

    /**  All filter coefficients and history data are formatted and written 
     *  to the specified ostream.
     *  @memo  Print the Filter status.
     *  @param ostr Output stream to receive the Filter info.
     */
    void dump( std::ostream& ostr ) const;

    /**  The TSeries is filtered and the result placed in a new TSeries. 
     *  The resulting TSeries is left on the stack and must be copied to 
     *  be made permanent. The filter status flag is set if there are 
     *  insufficient history entries (less than the Filter order) or if 
     *  the TSeries start time isn't contiguous with the previous data.
     *  The input samples are appended to the filter history as the filter
     *  response is calculated.
     *  @memo   Filter a Time Series.
     *  @param  in Time series to be filtered.
     *  @return A new TSeries containing the filter response to the input
     *          series.
     */
    TSeries apply( const TSeries& in );

    /**  The TSeries is filtered and the result stored in a specified output 
     *  TSeries. The output TSeries must already exist. The filter status 
     *  flag is set if there are insufficient history entries (less than the
     *  Filter order) or if the TSeries start time isn't contiguous with the
     *  previous data.
     *  The input samples are appended to the filter history as the filter
     *  response is calculated.
     *  @memo   Filter a TSeries into an output object.
     *  @param  in  Time series to be filtered.
     *  @param  out Time series into which the filter response will be stored.
     */
    void apply( const TSeries& in, TSeries& out );

    /**  Zero the start time and current time and clear the history.
     *  @memo Reset the current time and history.
     */
    void reset( void );

    /**  A filter is constructed with the same length, frequency and 
     *  coefficients as the argument Filter. The history is zeroed.
     *  \brief Assignment operator.
     *  \param model Filter to be copied.
     *  \return Refernce to the current (modified) %cubic_interpolate instance.
     */
    cubic_interpolate& operator=( const cubic_interpolate& model );

    /**  Set the filter length and coefficients. The filter history buffer
     *  is not cleared.
     *  \brief  Set filter coefficients.
     *  @param  N     upsample factor.
     *  @param  Coefs A list of filter coefficients (filter impulse response).
     */
    void setCoefs( int upfactor );

    /**  The specified samples are copied to the Filter history vector. If 
     *  the number of samples specified is greater than the filter size, 
     *  only the last Norder samples are used. The filter current time is 
     *  set to the end-time of the input series.
     *  @memo  Set history.
     *  @param hist Filter history data.
     */
    void setHistory( const TSeries& hist );

    /**  Set the filter length (order+1). The coefficients and history
     *  of the filter are cleared.
     *  @memo  Set Length.
     *  @param N new filter length.
     */
    void setLength( int N );

    /**  Set the filter mode. If the mode is set to \c fm_causal the time 
     *  of the samples in the output series are the same as those in the 
     *  input series. If the fiter is in \c fm_zero_phase mode, the start 
     *  time of the filtered series is shifted by \c order*sample/2.
     *  \brief Set the filter mode.
     *  \param mode New mode flag value
     */
    void setMode( fir_mode mode );

    /**  The transfer function of the filter in the specified frequency 
     *  interval is calculated and returned as a complex frequency series. 
     *  @memo   Get the transfer function of a Filter.
     *  @param  Fmin Minimum frequency at which to sample the transfer function.
     *  @param  Fmax Maximum frequency at which to sample the transfer function.
     *  @param  dF   Frequency step.
     *  @return a complex FSeries containing the Filter response at each 
     *          frequency step.
     */
    virtual FSeries
    Xfer( float Fmin = 0.0, float Fmax = 0.0, float dF = 1.0 ) const;

protected:
    /** The transfer coefficient of the filter at the specified 
     * frequency is calculated and returned as a complex number. 
     * @memo Get a transfer coefficent of a Filter.
     * @param coeff a complex number representing the Filter response 
     *              at the specified frequency (return)
     * @param f Frequency at which to sample the transfer function.
     * @return true if successful       
     */
    bool xfer( fComplex& coeff, double f ) const;

protected:
    bool no_coefs( void ) const;

protected:
    /**  The number of filter coefficients - 1.
    *  @memo Order of filter.
    */
    int mFactor;

    /**  Coefficients.
     */
    std::unique_ptr< DVector > mCoefs;

    /**  Input time step
     */
    Interval mStep;

    /**  Intermediate upsampled results 
     */
    std::unique_ptr< DVector > mHistory;

    /**  Time of next expected filtered sample.
     *  @memo Current sample time.
     */
    Time mCurTime;

    /**  Time of First Sample processed since creation or resetting of
     *  the filter.
     *  @memo Start time.
     */
    Time mStartTime;

    /** Filter mode is either causal or linear-phase.
     */
    fir_mode mFIRmode;

    /** filter OK status
    */
    bool mStat;
};

//--------------------------------------  Get the sample rate
inline double
cubic_interpolate::getRate( void ) const
{
    return 1 / double( mStep );
}

//--------------------------------------  Get the processing start time
inline Time
cubic_interpolate::getStartTime( void ) const
{
    return mStartTime;
}

//--------------------------------------  Get the next expected sample time
inline Time
cubic_interpolate::getCurrentTime( void ) const
{
    return mCurTime;
}

//--------------------------------------  Get the next expected sample time
inline Interval
cubic_interpolate::getTimeDelay( void ) const
{
    return Interval( 2.0 * mStep );
}

//--------------------------------------  Get the history status
inline bool
cubic_interpolate::getStat( void ) const
{
    return mStat;
}

//--------------------------------------  Is the filter in use?
inline bool
cubic_interpolate::inUse( void ) const
{
    return mStartTime != Time( 0 );
}

#endif // CUBIC_INTERPOLATE_HH
