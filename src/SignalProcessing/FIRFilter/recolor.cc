/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "recolor.hh"
#include "DVecType.hh"
#include "fSeries/DFT.hh"
#include "fSeries/PSD.hh"
#include "fSeries/ASD.hh"
#include "FIRdft.hh"
#include "Tukey.hh"
#include <stdexcept>

using namespace std;
using namespace containers;

//======================================  Constructor.
recolor::recolor( const containers::PSD& out )
    : _psdOut( out ), _filterLength( 0 ), _inputSample( 0 ), _current( 0 ),
      _start( 0 )
{
}

//======================================  Whitening constructor
recolor::recolor( const containers::PSD& in, const containers::PSD& out )
    : _psdOut( out ), _psdIn( in ), _filterLength( 0 ), _inputSample( 0 ),
      _current( 0 ), _start( 0 )
{
}

//======================================  Destructor.
recolor::~recolor( void )
{
}

//======================================  Clone a filter.
recolor*
recolor::clone( void ) const
{
    return new recolor( *this );
}

//======================================  Apply whitening filter to constructor.
TSeries
recolor::apply( const TSeries& ts )
{
    if ( !_start )
        _start = ts.getStartTime( );
    remake( ts );
    return _filter( ts );
}

//======================================  Set up the FIR filter.
void
recolor::mkFilter( const PSD& psd )
{
    DFT dft;
    static_cast< fSeries& >( dft ) = ASD( psd );
    //dft.dump_header(cout);
    Tukey  win( 0.5 );
    double knormal = 1.0;

    //----------------------------------  Look into doing this with DCT
    // TSeries kernel = dft.iDCT(); ??????

    //----------------------------------  For now, use an FFT
    TSeries  kernel = dft.iFFT( );
    double   sampleRate = 1.0 / kernel.getTStep( );
    DVectD&  dvd = dynamic_cast< DVectD& >( *kernel.refDVect( ) );
    size_t   n = dvd.size( );
    size_t   nmov = ( n - 1 ) / 2;
    DVector* tmp = dvd.Extract( 0, n - nmov );
    tmp->force_copy( );
    dvd.Erase( 0, n - nmov );
    dvd.Append( *tmp );
    delete tmp;
    knormal /= sqrt( 2.0 * n ); // sqrt(2) from ASD definition? +/- Frequencies?

    //-----------------------------------  Window the kernel
    double magb4 = kernel * kernel;
    kernel = win( kernel );
    kernel *= knormal * sqrt( magb4 / ( kernel * kernel ) );
    long   firLength = kernel.getNSample( );
    FIRdft fir( firLength - 1, sampleRate );
    //fir.setMode(FIRdft::fm_zero_phase); ///< this is a recipe for disaster
    fir.setCoefs( dynamic_cast< DVectD* >( kernel.refDVect( ) )->refTData( ) );
    _filter.set( fir.clone( ) );
}

//======================================  Set up the FIR filter.
void
recolor::remake( const TSeries& ts )
{
    if ( _psdOut.empty( ) )
        throw runtime_error( "recolor: Empty output PSD" );

    //----------------------------------  Check the start time
    if ( !_current )
    {
        _current = ts.getStartTime( );
    }
    else if ( _current != ts.getStartTime( ) )
    {
        throw runtime_error( "recolor: Invalid input start time" );
    }

    //----------------------------------  Check the input sample rate is set
    if ( !_inputSample )
    {
        _inputSample = ts.getTStep( );
        if ( !_inputSample )
        {
            throw runtime_error( "recolor: Invalid input sample rate" );
        }
    }
    else if ( ts.getTStep( ) != _inputSample )
    {
        throw runtime_error( "recolor: Invalid input sample rate" );
    }

    //----------------------------------  Check filter length - match PSD dF.
    if ( !_filterLength )
    {
        setFilterLength( 1.0 / _psdOut.getFStep( ) );
    }

    //----------------------------------  See if the filter needs to be created.
    if ( _filter.null( ) )
    {
        double Df = 1.0 / _filterLength;
        double fMax = 0.5 / ts.getTStep( ); // Nyquist frequency for this
        PSD    xfunc;
        if ( _psdOut.getFStep( ) == Df )
        {
            xfunc = _psdOut.extract_psd( 0, fMax );
            if ( xfunc.empty( ) )
                throw runtime_error( "recolor: empty PSD" );
        }
        else
        {
            static_cast< fSeries& >( xfunc ) =
                _psdOut.interpolate( 0, fMax, Df );
        }
        if ( !_psdIn.empty( ) )
        {
            xfunc /= _psdIn.interpolate( 0, fMax, Df );
            //---------------------------  Ugly normalization factor, but...
            xfunc *= 2 * double( _filterLength ) * double( ts.getTStep( ) );
        }
        mkFilter( xfunc );
    }
    _current = ts.getEndTime( );
}

//======================================  Reset the filter
void
recolor::reset( void )
{
    _current = Time( 0 );
}

//======================================  Set the filter length
void
recolor::setFilterLength( Interval dt )
{
    _filterLength = dt;
}

//======================================  Chack that data is OK to filter.
void
recolor::dataCheck( const TSeries& ts ) const
{
    if ( !_current )
        return;
    if ( !Almost( _current, ts.getStartTime( ) ) )
        throw runtime_error( "recolor: Invalid input time series" );
    if ( !_inputSample || _inputSample != ts.getTStep( ) )
        throw runtime_error( "recolor: Invalid input time series" );
}
