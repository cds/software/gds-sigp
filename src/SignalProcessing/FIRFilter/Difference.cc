#include "Difference.hh"

//======================================  Constructor
Difference::Difference( double Hz ) : FIRFilter( 2, Hz )
{
    const double fCoef[ 2 ] = { -1.0, 1.0 };
    setCoefs( 2, fCoef );
}

//======================================  Destructor
Difference::~Difference( void )
{
}

//======================================  Clone method
Difference*
Difference::clone( void ) const
{
    return new Difference( *this );
}
