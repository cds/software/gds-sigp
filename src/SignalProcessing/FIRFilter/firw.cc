//-----------------------------------------------------------------------
//   Function FirW: Window design of linear phase low-pass, high-pass,
//                  band-pass and band-stop FIR digital filters.
//                  Written in c++ by J. Zweizig, based on public domain
//                  program FIR1 by L.R. Rabiner, C.A. McGonegal and D. Paul.
//
//    Arguments:
//       Nf:        The filter length (in samples). If itype == 7 and Nf==0
//                  nf will be calculated from the desired ripple and
//                  transition band-width.
//
//       itype      Type of window on which the filter is based.
//                    itype == 1  Rectangular
//                    itype == 2  Triangular
//                    itype == 3  Hamming
//                    itype == 4  Generalized Hamming. The form of this
//                                window is Wm = A + B*cos(2*pi*m/(Nf-1))
//                                A is specified with the Df argument and
//                                B is set to (1-A).
//                    itype == 5  Hanning
//                    itype == 6  Kaiser (I0-SINH). The stop-band attenuation
//                                is specified in the ripple argument (in dB).
//                    itype == 7  Chebyshev. 2 of the 3 parameters: nf, df
//                                and ripple must be specified. The third is
//                                calculated. nf is the filter length, df is
//                                the transition band width and ripple is the
//                                desired ripple in dB. The parameter to be
//                                inferred is specified as zero.
//
//        jtype     Filter type
//                    jtype == 1  low-pass  filter
//                    jtype == 2  high-pass filter
//                    jtype == 3  band-pass filter
//                    jtype == 4  band-stop filter
//
//        rate      Sampling rate.
//
//        f1        The cutoff frequency, for jtype=1, 2 or lower transition
//                  frequency for jtype=3, 4. f1 must be in range 0<f1<rate/2.
//
//        f2        The upper transition frequency for jtype=3, 4. f2 must be
//                  in the range f1<f2<rate/2 for band-pass/band-stop filters.
//
//        df        transition band width in units of Fny
//
//        ripple    Pass-band ripple or stop-band attenuation in dB.
//
//-----------------------------------------------------------------------
#ifndef __EXTENSIONS__
#define __EXTENSIONS__
#endif

#include "constant.hh"
#include <iostream>
#include <cmath>

using namespace std;

//--------------------------------------  Internal functions.
static void   triang( int Nf, double* w );
static void   hamming( int Nf, double* w, double alpha, double beta );
static void   kaiser( int Nf, double* w, double beta );
static double ino( double x );
static void   chebc( int& Nf, double& dp, double& df );
static void   cheby( int Nf, double* w, double dp, double df );

//--------------------------------------  Filter design function.
int
FirW( int&    Nf,
      int     itype,
      int     jtype,
      double  f1,
      double  f2,
      double  df,
      double  ripple,
      double* g )
{

    //----------------------------------  Check window-dependent arguments
    double dp( 0 );
    int    SaveN = Nf;
    switch ( itype )
    {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
        break;

    case 6:
        //------------------------------  Calculate Kaiser filter length if
        //                                not supplied.
        if ( Nf <= 0 )
        {
            if ( ripple <= 0 || df <= 0 )
            {
                cerr << "FirW: Can't calculate Kaiser filter order, "
                     << "ripple or df <= 0." << endl;
                return -1;
            }
            Nf = int( ( ripple - 7.95 ) / ( 14.36 * df ) ) + 1;
        }
        break;

    case 7:
        //------------------------------  Evaluate missing Chebyshev parameter
        dp = pow( 10.0, -ripple / 20.0 );
        chebc( Nf, dp, df );
        break;

    default:
        cerr << "Invalid filter type (" << itype << ")." << endl;
        return -1;
    }

    //----------------------------------  High-pass and Band-stop filters must
    //                                    have odd number of terms
    if ( ( jtype == 2 || jtype == 4 ) && ( Nf % 2 == 0 ) )
        Nf++;

    //----------------------------------  Check the Length
    if ( Nf < 3 )
    {
        cerr << "requested filter length (" << Nf << ") is invalid." << endl;
        return -1;
    }
    else if ( Nf > SaveN )
    {
        return 1;
    }
    int n = ( Nf + 1 ) / 2;

    //----------------------------------  Check cutoff frequencies, calculate
    //                                    the unwindowed impulse response.
    double c, fl, fh;
    switch ( jtype )
    {
    case 1: //-------------------------  Low-Pass and High-Pass
    case 2:
        if ( f1 <= 0 || f1 >= 0.5 )
        {
            cerr << "Invalid center frequency (" << f1 << ")." << endl;
            return -1;
        }

        if ( Nf % 2 == 0 )
        {
            for ( int i = 0; i < n; i++ )
            {
                c = pi * ( double( i ) + 0.5 );
                g[ i ] = sin( 2 * c * f1 ) / c;
            }
        }
        else
        {
            g[ 0 ] = 2.0 * f1;
            for ( int i = 1; i < n; i++ )
            {
                c = pi * double( i );
                g[ i ] = sin( 2 * c * f1 ) / c;
            }
        }
        break;

    case 3: //-------------------------  Band-Pass and Band-Stop
    case 4:
        fl = f1;
        if ( fl <= 0 || fl >= 0.5 )
        {
            cerr << "Invalid lower frequency (" << fl << ")." << endl;
            return -1;
        }
        fh = f2;
        if ( fh <= fl || fh >= 0.5 )
        {
            cerr << "Invalid upper frequency (" << fh << ")." << endl;
            return -1;
        }

        if ( Nf % 2 == 0 )
        {
            for ( int i = 0; i < n; i++ )
            {
                c = twopi * ( double( i ) + 0.5 );
                g[ i ] = ( sin( c * fh ) - sin( c * fl ) ) * 2.0 / c;
            }
        }
        else
        {
            g[ 0 ] = 2.0 * ( fh - fl );
            for ( int i = 1; i < n; i++ )
            {
                c = twopi * double( i );
                g[ i ] = ( sin( c * fh ) - sin( c * fl ) ) * 2.0 / c;
            }
        }
        break;

    default:
        cerr << "Invalid filter type (" << jtype << ")." << endl;
        return -1;
    }

    //----------------------------------  Compute window
    double* w = new double[ n + 1 ];
    double  alpha, beta;
    switch ( itype )
    {

    //----------------------------------  Rectangular window
    case 1:
        for ( int i = 0; i < n; i++ )
            w[ i ] = 1.0;
        break;

    //----------------------------------  Triangular window
    case 2:
        triang( Nf, w );
        break;

    //----------------------------------  Hamming window
    case 3:
        alpha = 0.54;
        beta = 1.0 - alpha;
        hamming( Nf, w, alpha, beta );
        break;

    //----------------------------------  Generalized Hamming window
    case 4:
        beta = 1.0 - df;
        hamming( Nf, w, df, beta );
        break;

    //----------------------------------  Hanning window
    case 5:
        alpha = 0.5;
        beta = 1.0 - alpha;
        hamming( Nf + 2, w, alpha, beta ); // Nf+2 is length including zeroes
        break;

    //----------------------------------  Kaiser window
    case 6:
        if ( ripple > 50.0 )
            beta = 0.1102 * ( ripple - 8.7 );
        else if ( ripple > 20.96 )
            beta = 0.58417 * pow( ripple - 20.96, 0.4 ) +
                0.07886 * ( ripple - 20.96 );
        else
            beta = 0;
        kaiser( Nf, w, beta );
        break;

    //----------------------------------  Chebyshev window
    case 7:
        cheby( Nf, w, dp, df );
        break;
    }

    //----------------------------------  Window the ideal response,
    //                                    depending on the filter type
    if ( jtype == 2 || jtype == 4 )
    {
        g[ 0 ] = 1.0 - w[ 0 ] * g[ 0 ];
        for ( int i = 1; i < n; i++ )
            g[ i ] *= -w[ i ];
    }
    else
    {
        for ( int i = 0; i < n; i++ )
            g[ i ] *= w[ i ];
    }
    delete[] w;
    int nset = ( Nf + 1 ) / 2;
    for ( int i = 1; i <= nset; i++ )
        g[ Nf - i ] = g[ nset - i ];
    for ( int i = 1; i <= nset; i++ )
        g[ i - 1 ] = g[ Nf - i ];
    return 0;
}

//--------------------------------------  Generate a triangular window
static void
triang( int Nf, double* w )
{
    int    n = ( Nf + 1 ) / 2;
    double fn = n;
    double xi = 0.0;
    if ( Nf % 2 == 0 )
        xi = 0.5;
    for ( int i = 0; i < n; i++ )
    {
        w[ i ] = 1.0 - xi / fn;
        xi += 1.0;
    }
}

//--------------------------------------  Generate a generalized Hamming window
//
//    The generalized hamming window is of the form
//
//        Wm(i) = alpha + beta * cos( 2*pi*i / (Nf-1) )
//
static void
hamming( int Nf, double* w, double alpha, double beta )
{
    double fn = Nf - 1;
    int    n = ( Nf + 1 ) / 2;
    double fi = 0;
    if ( Nf % 2 == 0 )
        fi = 0.5;
    for ( int i = 0; i < n; i++ )
    {
        w[ i ] = alpha + beta * cos( twopi * fi / fn );
        // w[i] = alpha - beta*cos(twopi*fi/fn); after Jackson - doesn't work
        fi += 1.0;
    }
}

//--------------------------------------  Generate a Kaiser window
static void
kaiser( int Nf, double* w, double beta )
{
    double bes = ino( beta );
    double xind = ( Nf - 1 ) * ( Nf - 1 );
    double xi = 0.0;
    if ( Nf % 2 == 0 )
        xi = 1.0;
    int n = ( Nf + 1 ) / 2;
    for ( int i = 0; i < n; i++ )
    {
        w[ i ] = ino( beta * sqrt( 1.0 - xi * xi / xind ) ) / bes;
        xi += 2.0;
    }
}

//--------------------------------------  Modified zeroth order bessel function
static double
ino( double x )
{
    const double eps = 1e-8; // required accuracy
    double       y = x / 2.0;
    double       sum = 1.0;
    double       de = 1.0;
    double       sde = 1.0;
    for ( int i = 1; i < 25 && sum * eps < sde; i++ )
    {
        de *= y / double( i );
        sde = de * de;
        sum += sde;
    }
    return sum;
}

//======================================  Calculate one of three Chebyshev
//                                        window parameters
//
//    Nf = Filter length.
//    dp = Ripple
//    df = normalized transition width.
static void
chebc( int& Nf, double& dp, double& df )
{
    double c0, c1, c2;

    //----------------------------------  Calculate Nf
    if ( Nf < 3 )
    {
        double x =
            1.0 + acosh( ( 1.0 + dp ) / dp ) / acosh( 1.0 / cos( pi * df ) );
        Nf = int( x + 1.0 );

        //----------------------------------  Calculate df (interband gap)
    }
    else if ( df == 0 )
    {
        c1 = acosh( ( 1.0 + dp ) / dp );
        c2 = cosh( c1 / ( Nf - 1 ) );
        df = acos( 1. / c2 ) / pi;

        //----------------------------------  Calculate dp (ripple)
    }
    else
    {
        c0 = cos( pi * df );
        c1 = ( Nf - 1 ) * acosh( 1. / c0 );
        dp = 1. / ( cosh( c1 ) - 1. );
    }
}

//======================================  Calculate Chebyshev window
//
//   Nf = Filter length
//   w  = window array
//   dp = ripple
//   df = transition width.
static void
cheby( int Nf, double* w, double dp, double df )
{
    double* pr = new double[ Nf ];
    double* pim = new double[ Nf ];
    double  fNf = Nf;
    double  x0 = ( 3.0 - cos( twopi * df ) ) / ( 1.0 + cos( twopi * df ) );
    double  alpha = ( x0 + 1.0 ) / 2.0;
    double  beta = alpha - 1.0;
    double  c2 = ( fNf - 1.0 ) / 2.0;
    int     n = ( Nf + 1 ) / 2;
    double  x, p, f;
    for ( int i = 0; i < Nf; i++ )
    {
        f = double( i ) * pi / fNf;
        x = alpha * cos( 2.0 * f ) + beta;
        if ( x <= 1.0 || x >= -1.0 )
        {
            p = dp * cos( c2 * acos( x ) );
        }
        else
        {
            p = dp * cosh( c2 * acosh( x ) );
        }
        if ( Nf % 2 != 0 )
        {
            pim[ i ] = 0;
            pr[ i ] = p;
        }
        else
        {
            //--------------------------  Even length filters have 1/2 sample
            //                            delay, antisymmetric freq. response.
            pr[ i ] = p * cos( f );
            pim[ i ] = -p * sin( f );
            if ( i >= ( Nf / 2 + 1 ) )
            {
                pr[ i ] = -pr[ i ];
                pim[ i ] = -pim[ i ];
            }
        }
    }

    //----------------------------------  Fourier transform.
    double twn = twopi / fNf;
    double norm = pr[ 0 ];
    for ( int j = 1; j < Nf; j++ )
        norm += pr[ j ];
    w[ 0 ] = 1.0;
    for ( int i = 1; i < n; i++ )
    {
        double sum = pr[ 0 ];
        for ( int j = 1; j < Nf; j++ )
        {
            double arg = twn * i * j;
            sum += pr[ j ] * cos( arg ) + pim[ j ] * sin( arg );
        }
        w[ i ] = sum / norm;
    }
    delete[] pr;
    delete[] pim;
}
