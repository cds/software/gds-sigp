/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef FIRDFT_HH
#define FIRDFT_HH

#include "Complex.hh"
#include "TSeries.hh"
#include "Pipe.hh"
#include <iosfwd>
#include <memory>

namespace containers
{
    class DFT;
}

class DVector;
class FSeries;
class FIRFilter;

/**  The %FIRdft class implements a digital FIR filter that filters time 
  *  series (TSeries) data using f-domain convolution. %FIRdft instances 
  *  carry the signal history as well as the filter coefficients to prevent
  *  the introduction of edge effects when filtering a long series in serveral 
  *  pieces. Each signal (channel) to be filtered must use a separate Filter 
  *  instance. The timing of the filtered output time-series may be set to
  *  implement either a causal filter or a zero phase delay filter.
  *  The %FIRdft class is much faster than the time domain 
  *  convolution technique (FIRFilter) in cases where the filter order (M) 
  *  is large. 
  *
  *  \brief Digital FIR filter implemented with f-domain convolution.
  *  \author  John G. Zweizig
  *  \version 1.0; Last modified: April 8, 2008
  */
class FIRdft : public Pipe
{
public:
    /**  The filter mode flag affects the timing of the filtered time series.
      *  The two options are:
      *  <ul>
      *  <li> <b>Causal:</b> The time of each output sample is equal to 
      *                      the time of the corresponding input sample. 
      *                      For a filter of order M, this results in an 
      *                      effective filter delay of M/2 samples</li>
      *  <li> <b>Zero phase:</b> The filtered time series is delayed by M/2
      *                      samples. This results in zero apparent delay
      *                      and zero phase shift for symmetric filters.</li>
      *  </ul>
      *  \brief FIR filter mode.
      */
    enum fir_mode
    {
        fm_causal, ///< Causal filter mode.
        fm_zero_phase, ///< Zero phase delay filter mode.
        fm_drop_start ///< Zero phase, but drop data before start time.
    };

public:
    using Pipe::apply;
    using Pipe::dataCheck;

    /**  Default constructor. No coefficient or history storage is allocated.
      *  \brief Default constructor.
      */
    FIRdft( void );

    /**  A real-valued Order 'length'-1 digital FIR filter is constructed
      *  which will operate on data sampled with specified frequency. 
      *  The filter coefficients remain uninitialized and the history data 
      *  is cleared.
      *  \brief  Construct an empty filter instance.
      *  \param length Number of terms (zeros) in the filter.
      *  \param Sample Nominal sampling rate.
      */
    FIRdft( int length, double Sample );

    /**  A filter is constructed with the same length, frequency and 
      *  coefficients as the argument Filter. The history is zeroed.
      *  \brief  Copy constructor.
      *  \param model Filter to be copied.
      */
    FIRdft( const FIRdft& model );

    /**  A filter is constructed with the same length, frequency and 
      *  coefficients as the argument Filter. The history is zeroed.
      *  \brief  Copy constructor.
      *  \param model Filter to be copied.
      */
    FIRdft( const FIRFilter& model );

    /**  Destroy the filter object and release any memory allocated for
      *  filter coefficients or history information.
      *  \brief Filter destructor.
      */
    virtual ~FIRdft( void );

    /**  Create an identical FIR filter and return a pointer to the new
      *  filter. The history is zeroed.
      *  \brief Clone a FIR filter.
      *  @return pointer to an identical filter.
      */
    virtual FIRdft*
    clone( void ) const
    {
        return new FIRdft( *this );
    }

    /**  Test whether a %TSeries is valid as input data for the filter.
      *  a runtime_error exception is thrown if the data are invalid.
      *  \brief   Check input data validity.
      *  \param ts %Time series to be checked for consistency with the current
      *            filter state.
      */
    void dataCheck( const TSeries& ts ) const;

    /**  Get the expected start time of the next input %TSeries to be filtered.
      *  \brief  Get the current time.
      *  \return Expected time for the next sample to be processed.
      */
    Time getCurrentTime( void ) const;

    /**  Get the filter length. The filter state is not affected.
      *  \brief   Get the filter length.
      *  @return The filter length (Order+1)
      */
    int getLength( void ) const;

    /**  Get the sample rate in Hz. The filter state is not affected.
      *  \brief   Get the sample rate.
      *  @return The nominal sample rate in Hz.
      */
    double getRate( void ) const;

    /**  Get the start time of this filter run. This is set by the first 
      *  filter operation after the filter has been created or reset.
      *  \brief   Get the start time.
      *  @return The start time.
      */
    Time getStartTime( void ) const;

    /**  Tests whether the previous filter operation used valid history data.
      *  History data are considered to be valid if the history buffer is full 
      *  and the start time of the input data is equal to the time stamp on the 
      *  history data. The filter state is not affected.
      *  \brief   Test the history status.
      *  @return true if the filter history data was valid for the last 
      *               operation.
      */
    bool getStat( void ) const;

    /**  Get the time delay imposed by the Filter. The time delay is only
      *  non-zero for filters that have a well defined delay i.e. FIR filters.
      *  A positive value indicates that the filter delays a signal by the 
      *  specified time.
      *  \brief Get the time delay.
      *  \return Signal time delay.
      */
    Interval getTimeDelay( void ) const;

    /**  Tests whether the filter is in use. The filter is considered to be 
      *  in use if a valid, non-empty time series was has been processed by 
      *  the filter since the instance was constructed or reset.
      *  \brief Test the filter acitivity status.
      *  @return true if the filter is being used.
      */
    bool inUse( void ) const;

    /**  All filter coefficients and history data are formatted and written 
      *  to the specified ostream.
      *  \brief  Print the filter status.
      *  \param ostr Output stream to receive the Filter info.
      */
    void dump( std::ostream& ostr ) const;

    /**  The %TSeries is filtered and the result placed in a new %TSeries. 
      *  The resulting %TSeries is left on the stack and must be copied to 
      *  be made permanent. The filter status flag is set if there are 
      *  insufficient history entries (less than the Filter order) or if 
      *  the %TSeries start time isn't contiguous with the previous data.
      *  The input samples are appended to the filter history as the filter
      *  response is calculated.
      *  \brief   Filter a time series.
      *  \param  in %Time series to be filtered.
      *  @return A new %TSeries containing the filter response to the input
      *          series.
      */
    virtual TSeries apply( const TSeries& in );

    /**  The %TSeries is filtered and the result stored in a specified output 
      *  %TSeries. The output %TSeries must already exist. The filter status 
      *  flag is set if there are insufficient history entries (less than the
      *  filter order) or if the %TSeries start time isn't contiguous with the
      *  previous data.
      *  The input samples are appended to the filter history as the filter
      *  response is calculated. The output time-series may overlap the 
      *  input series (i.e. you may filter in place).
      *  \brief  Filter a %TSeries into an output object.
      *  \param  in  %Time series to be filtered.
      *  \param  out %Time series into which the filter response will be stored.
      *  @return reference to the %TSeries containing the filter response.
      */
    virtual TSeries& apply( const TSeries& in, TSeries& out );

    /**  Reset the start time current time and history.
      *  \brief Reset state information.
      */
    void reset( void );

    /**  A filter is constructed with the same length, sample rate and 
      *  coefficients as the argument filter. The history is zeroed.
      *  \brief  Assignment operator.
      *  \param model Filter to be copied.
      *  \return Reference to the current (modified) %FIRdft instance.
      */
    FIRdft& operator=( const FIRdft& model );

    /**  A filter is constructed with the same length, sample rate and 
      *  coefficients as the argument filter. The history is zeroed.
      *  \brief  Assignment operator.
      *  \param model Filter to be copied.
      *  \return Reference to the current (modified) %FIRdft instance.
      */
    FIRdft& operator=( const FIRFilter& model );

    /**  A filter is generated by cross-correlating the coefficients of this 
      *  filter with those of the argument filter. The result is a filter 
      *  that has the same effect as applying the original two filters 
      *  sequentially. In most cases the combined filter will be nearly as 
      *  fast as the original filter. The argument filter must have defined
      *  coefficients and the same sample rate.
      *  \brief Combine %FIRdft filters.
      *  \exception std::invalid_argument Argument filter has no coefficients 
      *  or a sample rate that differs from this filter.
      *  \param model Filter to be combined.
      *  \return Reference to the current (modified) %FORdft instance.
      */
    FIRdft& operator*=( const FIRdft& model );

    /**  Set the filter length and coefficients. The filter history buffer
      *  is cleared.
      *  \brief   Set filter coefficients.
      *  \param  N     Number of coefficients to set.
      *  \param  Coefs A list of filter coefficients (filter impulse response).
      */
    void setCoefs( int N, const double* Coefs );

    /**  Modify the filter coefficients without clearing the filter history
      *  buffer.
      *  \brief   Modify filter coefficients.
      *  \param  Coefs A list of filter coefficients (filter impulse response).
      */
    void setCoefs( const double* Coefs );

    /**  The specified samples are copied to the Filter history vector and 
      *  current time of the filter is set to the end-time of the history
      *  series. If more data are provided than needed for the filter, the
      *  last sample are stored.
      *  \brief  Set history.
      *  \param hist Filter history %TSeries.
      */
    void setHistory( const TSeries& hist );

    /**  The specified samples are copied to the Filter history vector. If 
      *  the number of samples specified is greater than the filter size, 
      *  only the last Norder samples are used. If the data vector argument 
      *  is omitted the number of history terms is set as specified and any 
      *  current data are prefixed with zeroes as needed to fill out the 
      *  requested number of terms. The history time represents
      *  the time of the first sample.
      *  \brief  Set history.
      *  \param N    Number of history elements to preset.
      *  \param Hist Filter history vector.
      *  \param t    %Time of the last sample in the history list.
      */
    void setHistory( int N, const float* Hist, Time t );

    /**  The specified samples are copied to the Filter history vector. If 
      *  the number of samples specified is greater than the filter size, 
      *  only the last Norder samples are used. If the data vector argument 
      *  is omitted the number of history terms is set as specified and any 
      *  current data are prefixed with zeroes as needed to fill out the 
      *  requested number of terms. The history time represents
      *  the time of the first sample.
      *  \brief  Set history.
      *  \param N    Number of history elements to preset.
      *  \param Hist Filter history vector.
      *  \param t    %Time of the first sample in the history list.
      */
    void setHistory( int N, const double* Hist, Time t );

    /**  The specified samples are copied to the Filter history vector. If 
      *  the number of samples specified is greater than the filter size, 
      *  only the last Norder samples are used. If the data vector argument 
      *  is omitted the number of history terms is set as specified and any 
      *  current data are prefixed with zeroes as needed to fill out the 
      *  requested number of terms. The history time represents
      *  the time of the first sample.
      *  \brief  Set history.
      *  \param N    Number of history elements to preset.
      *  \param Hist Filter history vector.
      *  \param t    %Time of the last sample in the history list.
      */
    void setHistory( int N, const fComplex* Hist, Time t );

    /**  Set the filter length (order+1). The coefficients and history
      *  of the filter are cleared.
      *  \brief  Set Length.
      *  \param N new filter length.
      */
    void setLength( int N );

    /**  Set the filter mode. If the mode is set to \c fm_causal the time 
      *  of the samples in the output series are the same as those in the 
      *  input series. If the fiter is in \c fm_zero_phase mode, the start 
      *  time of the filtered series is shifted by \c order*sample/2.
      *  \brief Set the filter mode.
      *  \param mode New mode flag value
      */
    void setMode( fir_mode mode );

    /**  Set the filter sample rate.
      *  \brief Set the filter sample Rate.
      *  \param F New sample frequency.
      */
    void setRate( double F );

    /** The transfer coefficient of the filter at the specified 
      * frequency is calculated and returned as a complex number. 
      * Filters that support a fast way to compute a transfer coefficient 
      * should override xfer rather than this method.
      * \brief Get a transfer coefficent of a Filter.
      * \param coeff a complex number representing the Filter response 
      *              at the specified frequency (return)
      * \param f Frequency at which to sample the transfer function.
      * @return true if successful       
      */
    virtual bool Xfer( fComplex& coeff, double f ) const;

    /** The transfer function of the filter at the specified frequency 
      * points is calculated and returned as a complex array. 
      * The frequency points are user supplied. The return array
      * must be at least of length points.
      * \brief Get the transfer function of a Filter.
      * \param freqs Frequency points
      * \param points Number of points.
      * \param tf Transfer function (return)
      * @return true if successful       
      */
    bool Xfer( fComplex* tf, const float* freqs, int points ) const;

    /**  The transfer function of the filter in the specified frequency 
      *  interval is calculated and returned as a complex frequency series. 
      *  \brief   Get the transfer function of a Filter.
      *  \param  Fmin Minimum transfer function sample frequency.
      *  \param  Fmax Maximum transfer function sample frequency.
      *  \param  dF   Frequency step.
      *  @return a complex FSeries containing the Filter response at each 
      *          frequency step.
      */
    FSeries Xfer( float Fmin = 0.0, float Fmax = 0.0, float dF = 1.0 ) const;
    bool    Xfer( FSeries& Fs,
                  float    Fmin = 0.0,
                  float    Fmax = 0.0,
                  float    dF = 1.0 ) const;

protected:
    /** The transfer coefficient of the filter at the specified 
      * frequency is calculated and returned as a complex number. 
      * \brief Get a transfer coefficent of a Filter.
      * \param coeff a complex number representing the Filter response 
      *              at the specified frequency (return)
      * \param f Frequency at which to sample the transfer function.
      * @return true if successful       
      */
    bool xfer( fComplex& coeff, double f ) const;

private:
    void deleteHist( void );

    bool no_coefs( void ) const;

private:
    /**  The number of filter coefficients - 1.
      *  \brief Order of filter.
      */
    int mOrder;

    /**  Coefficients.
      */
    std::unique_ptr< DVector > mCoefs;

    /**  DFT of padded coeficients 
      */
    std::unique_ptr< containers::DFT > mCoefDFT;

    /** Design sample rate.
      */
    double mSample;

    /**  Last terms.
      */
    TSeries mHistory;

    /**  %Time of next expected filtered sample.
      *  \brief Current sample time.
      */
    Time mCurTime;

    /**  %Time of first sample processed since creation or resetting of
      *  the filter.
      *  \brief Start time.
      */
    Time mStartTime;

    /** Filter mode is either causal or linear-phase.
      */
    fir_mode mFIRmode;
};

#ifndef __CINT__

//======================================  Get the filter length
inline int
FIRdft::getLength( void ) const
{
    return mOrder + 1;
}

//======================================  Get the sample rate
inline double
FIRdft::getRate( void ) const
{
    return mSample;
}

//======================================  Get the processing start time
inline Time
FIRdft::getStartTime( void ) const
{
    return mStartTime;
}

//======================================  Get the next expected sample time
inline Time
FIRdft::getCurrentTime( void ) const
{
    return mCurTime;
}

//======================================  Get the next expected sample time
inline Interval
FIRdft::getTimeDelay( void ) const
{
    return Interval( 0.5 * mOrder / mSample );
}

//======================================  Get the history status
inline bool
FIRdft::getStat( void ) const
{
    return !mHistory.empty( );
}

//======================================  Is the filter in use?
inline bool
FIRdft::inUse( void ) const
{
    return mCurTime != Time( 0 );
}

//======================================  Test if coefs defined
inline bool
FIRdft::no_coefs( void ) const
{
    return !mCoefs;
}

//======================================  Set the sample rate
inline void
FIRdft::setRate( double F )
{
    mSample = F;
}
#endif // __CINT__

#endif // FIRDFT_HH
