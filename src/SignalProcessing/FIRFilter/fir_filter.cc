/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "fir_filter.hh"
#include "DVecType.hh"
#include "lcl_array.hh"
#include "FSeries.hh"
#include "constant.hh"

using namespace std;

//======================================  Default constructor
fir_filter::fir_filter( void )
    : mOrder( 0 ), mType( kGeneral ), mSample( 0 ), mTerms( 0 ), mCurTime( 0 ),
      mStartTime( 0 ), mHistOK( false ), mFIRmode( fm_zero_phase )
{
}

//======================================  Sample-rate constructor
fir_filter::fir_filter( double Sample )
    : mOrder( 0 ), mType( kGeneral ), mSample( Sample ), mTerms( 0 ),
      mCurTime( 0 ), mStartTime( 0 ), mHistOK( false ),
      mFIRmode( fm_zero_phase )
{
}

//======================================  Design constructor
fir_filter::fir_filter( double Sample, const DVector& dv )
    : mOrder( 0 ), mType( kGeneral ), mSample( Sample ), mTerms( 0 ),
      mCurTime( 0 ), mStartTime( 0 ), mHistOK( false ),
      mFIRmode( fm_zero_phase )
{
    setCoefs( dv );
}

//======================================  Copy constructor
fir_filter::fir_filter( const fir_filter& model )
{
    *this = model;
}

//======================================  Destructor
fir_filter::~fir_filter( void )
{
    //-----------------------------------  Nothing to do. Arrays are all
    //                                     DVectors in unique_ptrs
}

//======================================  Check data is appropriate
void
fir_filter::dataCheck( const TSeries& ts ) const
{

    //----------------------------------  Check frequency.
    double one = mSample * ts.getTStep( );
    if ( one > 1.0001 || one < 0.9999 )
    {
        throw std::invalid_argument( "fir_filter: Invalid sample rate." );
    }

    //----------------------------------  Check history data valid.
    if ( mCurTime != Time( 0 ) && ts.getStartTime( ) != mCurTime )
    {
        throw std::invalid_argument( "fir_filter: Invalid start time." );
    }
}

//======================================  Dump the current filter status
void
fir_filter::dump( std::ostream& ostr ) const
{
    ostr << "FIR Filter status:" << std::endl;
    ostr << "    Order: " << mOrder << "  Sample Rate " << mSample
         << "  Start time: " << mStartTime << "  Current time: " << mCurTime
         << std::endl;
    if ( no_coefs( ) )
        return;
    const DVectD& cf = dynamic_cast< const DVectD& >( *mCoefs );

    for ( int i = 0; i <= mOrder; i++ )
    {
        if ( i % 8 == 0 )
            ostr << "    Coefs:";
        ostr << " " << cf[ i ];
        if ( i % 8 == 7 || i == mOrder )
            ostr << std::endl;
    }
}

//======================================  Filter a time series
TSeries
fir_filter::apply( const TSeries& in )
{
    TSeries out;
    apply( in, out );
    return out;
}

//======================================  Filter a time series
TSeries&
fir_filter::apply( const TSeries& in, TSeries& out )
{
    //-------------------------------------  Check valid coefficients.
    if ( no_coefs( ) )
    {
        throw runtime_error( "fir_filter: Coefficiens are not defined" );
    }

    //------------------------------------  Check start time and sample rate
    dataCheck( in );

    //------------------------------------  Check start time and sample rate
    if ( !inUse( ) )
    {
        mStartTime = in.getStartTime( );
        mCurTime = mStartTime;
    }

    //------------------------------------  Check data type
    unique_ptr< DVector > data( in.refDVect( )->clone( ) );
    if ( !mLastTerms )
    {
        setHistory( in );
    }
    else if ( mLastTerms->getType( ) != data->getType( ) )
    {
        throw runtime_error(
            "fir_filter: input series type differs from history." );
    }

    //------------------------------------  Perform the convolution
    convolve( *data );

    //------------------------------------  Set the data as appropriate
    Time start = in.getStartTime( );
    if ( mFIRmode == fm_zero_phase )
    {
        start -= getTimeDelay( );
    }

    //-------------------------------------  build the output series.
    out.setData( start, in.getTStep( ), data.release( ) );
    return out;
}

//=======================================  Calculate a convolution
void
fir_filter::convolve( DVector& dv )
{
    throw logic_error( "fir_filter: convolution finction not implemented" );
}

//=======================================  Reset the filter
void
fir_filter::reset( void )
{
    mStartTime = Time( 0 );
    mCurTime = Time( 0 );
    mLastTerms.reset( );
    mTerms = 0;
    mHistOK = false;
}

//=======================================  Reset the filter
fir_filter&
fir_filter::operator=( const fir_filter& model )
{
    reset( );
    mOrder = 0;
    mType = model.mType;
    mFIRmode = model.mFIRmode;
    mSample = model.mSample;
    if ( model.mCoefs )
        setCoefs( *model.mCoefs );
    return *this;
}

//=======================================  Combine filters
fir_filter&
fir_filter::operator*=( const fir_filter& model )
{
    //----------------------------------  Check valid argument filter.
    if ( mSample != model.mSample )
    {
        throw invalid_argument( "Invalid sample rate" );
    }
    if ( !model.no_coefs( ) )
    {
        throw invalid_argument( "Product with empty filter!" );
    }

    //----------------------------------  Make sure this is valid.
    if ( no_coefs( ) )
    {
        setCoefs( *model.mCoefs );
    }
    else
    {
        const DVectD& dv2 = dynamic_cast< DVectD& >( *model.mCoefs );
        //------------------------------  Make room for more coefficients
        int n1 = mOrder;
        int n2 = model.mOrder + 1;
        mOrder = n1 + n2 - 1;
        mCoefs->Extend( mOrder + 1 );

        //------------------------------  Store C1[n] * C2[i] in C[n+i]
        double* p1 =
            dynamic_cast< DVecType< double >& >( *mCoefs ).refTData( ) + n1;
        const double* p2 = dv2.refTData( );
        double        Cn = *p1;
        for ( int i = 0; i < n2; ++i )
            p1[ i ] = Cn * p2[ i ];

        //------------------------------  Loop over n1-1 ... 0
        while ( n1-- > 0 )
        {
            double* p = --p1;
            Cn = *p;
            *p++ *= *p2;
            for ( int i = 1; i < n2; ++i )
                *p++ += Cn * p2[ i ];
        }
    }
    return *this;
}

//======================================  Set coefficient vector
void
fir_filter::setCoefs( const DVector& coefs )
{
    if ( coefs.getType( ) != DVector::t_double )
        throw runtime_error( "fir_filter: coefficient vector not double" );
    mCoefs.reset( coefs.clone( ) );
    if ( no_coefs( ) )
        throw runtime_error( "fir_filter: empty coefficient vector" );
    if ( !mOrder )
    {
        mOrder = mCoefs->size( ) - 1;
    }
    else if ( mCoefs->size( ) != mOrder + 1 )
    {
        throw runtime_error(
            "fir_filter: Number of coefficients != order + 1" );
    }
    //------------------------------------  Check the filter symmetry
    symmetry      sym = kGeneral;
    size_t        N2 = ( mOrder + 1 ) / 2;
    const DVectD& cf = dynamic_cast< const DVectD& >( coefs );
    if ( cf[ 0 ] == cf[ mOrder ] )
    {
        sym = kSymm;
        for ( size_t i = 1; i < N2; i++ )
        {
            if ( cf[ i ] != cf[ mOrder - i ] )
            {
                sym = kGeneral;
                break;
            }
        }
    }
    else if ( cf[ 0 ] == -cf[ mOrder ] )
    {
        sym = kAnti;
        for ( size_t i = 1; i < N2; i++ )
        {
            if ( cf[ i ] != -cf[ mOrder - i ] )
            {
                sym = kGeneral;
                break;
            }
        }
    }
    mType = sym;
}

//======================================  Set time delay mode
void
fir_filter::setMode( fir_mode mode )
{
    mFIRmode = mode;
}

//======================================  Set history data
void
fir_filter::setHistory( const TSeries& hist )
{
    size_t N = hist.getNSample( );
    size_t inx = 0;
    size_t nCpy = N;
    if ( mOrder < N )
    {
        inx = N - mOrder;
        nCpy = mOrder;
    }
    mLastTerms.reset( hist.refDVect( )->Extract( inx, nCpy ) );
    if ( nCpy < mOrder )
        mLastTerms->ReSize( mOrder );
    mTerms = nCpy;
}

//======================================  Set the sample rate
void
fir_filter::setRate( double F )
{
    mSample = F;
}

//======================================  FIR Filter transfer function
FSeries
fir_filter::Xfer( float Fmin, float Fmax, float dF ) const
{
    FSeries r;
    if ( mOrder < 0 || !mSample || no_coefs( ) )
        return r;
    float Fny = getRate( ) / 2.0;
    if ( Fmin < 0 )
        Fmin = 0.0;
    if ( Fmax == 0.0 || Fmax > Fny )
        Fmax = Fny;
    if ( Fmin >= Fmax )
        return r;
    if ( dF <= 0.0 )
        dF = 1.0;
    int nBins = int( ( Fmax - Fmin ) / dF + 0.5 );

    //----------------------------------  Fill a response curve
    lcl_array< fComplex > CVec( nBins );
    fComplex              temp;
    for ( int j = 0; j < nBins; j++ )
    {
        float Freq = Fmin + double( j ) * dF;
        xfer( CVec[ j ], Freq );
    }
    r = FSeries( Fmin, dF, Time( 0 ), nBins / mSample, nBins, CVec );
    r.setName( "Filter Response" );
    return r;
}

//======================================  Compute the filter transfer function
bool
fir_filter::xfer( fComplex& coeff, double f ) const
{
    coeff = fComplex( 0.0 );
    if ( no_coefs( ) )
        return false;
    const DVectD& cf = dynamic_cast< const DVectD& >( *mCoefs );
    fComplex      temp;
    float         dPhi = twopi * f / mSample;
    float         Phi0 = dPhi * mOrder / 2;
    for ( int k = 0; k <= mOrder; k++ )
    {
        temp = polar< double >( cf[ k ], Phi0 - k * dPhi );
        coeff += temp;
    }
    return true;
}
