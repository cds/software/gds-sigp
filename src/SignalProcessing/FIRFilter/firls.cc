/* -*- mode: c++; c-basic-offset: 4; -*- */
// Copyright (C) 2006 Quentin Spencer
// Ported to C++/DMT  by J. Zweizig, LIGO Caltech, 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; If not, see <http://www.gnu.org/licenses/>.

//   firls(N, nBand, F, A, W, b);
//
//  FIR filter design using least squares method. Returns a length N+1
//  linear phase filter such that the integral of the weighted mean
//  squared error in the specified bands is minimized.
//
//  F specifies the frequencies of the band edges, normalized so that
//  half the sample frequency is equal to 1.  Each band is specified by
//  two frequencies, to the vector must have an even length.
//
//  A specifies the amplitude of the desired response at each band edge.
//
//  W is an optional weighting function that contains one value for each
//  band that weights the mean squared error in that band. A must be the
//  same length as F, and W must be half the length of F.

// The least squares optimization algorithm for computing FIR filter
// coefficients is derived in detail in:
//
// I. Selesnick, "Linear-Phase FIR Filter Design by Least Squares,"
// http://cnx.org/content/m10577
//
#include "PConfig.h" // JCB
#include "constant.hh"
#include "LTMatrix.hh"
#include "lcl_array.hh"
// #include <iostream>
#include <cmath>

using namespace std;

//--------------------------------------  Temporary internal storage allocated
//                                        using local array template.
typedef lcl_array< double > dble_vect;

void
firls( size_t        N,
       size_t        nBand,
       const double* frequencies,
       const double* pass,
       const double* weight,
       double*       coef )
{

    size_t nF = 2 * nBand;
    size_t M = N / 2;

    // Generate the matrix Q and vector b to solve for a in Q a = b
    // As illustrated in the above-cited reference, the matrix can be
    // expressed as the sum of a Hankel and Toeplitz matrix. A factor of
    // 1/2 has been dropped and the final filter coefficients multiplied
    // by 2 to compensate.

    // The vector b is derived from solving the integral:
    //
    //           _ w
    //          /   2
    //  b  =   /       W(w) D(w) cos(kw) dw
    //   k    /    w
    //       -      1
    //
    // Since we assume that W(w) is constant over each band (if not, the
    // computation of Q above would be considerably more complex), but
    // D(w) is allowed to be a linear function, in general the function
    // W(w) D(w) is linear. The computations below are derived from the
    // fact that:
    //     _
    //    /                          a              ax + b
    //   /   (ax + b) cos(nx) dx =  --- cos (nx) +  ------ sin(nx)
    //  /                             2                n
    // -                             n
    //

    dble_vect omega( nF );
    dble_vect q( N + 1 ), b( M + 1 ), wpass( nF ), wband( nF );

    q[ 0 ] = 0.0;
    b[ 0 ] = 0.0;
    for ( size_t i = 0; i < nF; i += 2 )
    {
        double wb = 1.0;
        if ( weight )
            wb = weight[ i / 2 ];
        wpass[ i ] = wb * pass[ i ];
        wpass[ i + 1 ] = wb * pass[ i + 1 ];
        wband[ i ] = wb;
        wband[ i + 1 ] = wb;
        omega[ i ] = pi * frequencies[ i ];
        omega[ i + 1 ] = pi * frequencies[ i + 1 ];

        b[ 0 ] += 0.5 * ( omega[ i ] + omega[ i + 1 ] ) *
                ( wpass[ i + 1 ] - wpass[ i ] ) +
            omega[ i + 1 ] * wpass[ i + 1 ] - omega[ i ] * wpass[ i ];
        q[ 0 ] += wb * ( omega[ i + 1 ] - omega[ i ] );
    }

    for ( size_t i = 1; i <= M; i++ )
    {
        double eye = double( i );
        double sumq = 0.0;
        double sumb = 0.0;
        for ( size_t j = 0; j < nF; j += 2 )
        {
            double wj1 = eye * frequencies[ j ];
            wj1 -= 2.0 * floor( wj1 / 2.0 );
            double wj2 = eye * frequencies[ j + 1 ];
            wj2 -= 2.0 * floor( wj2 / 2.0 );
            double cj1, cj2, sj1, sj2;
#ifndef USE_SINCOS
            sj1 = sin( pi * wj1 );
            cj1 = cos( pi * wj1 );
            sj2 = sin( pi * wj2 );
            cj2 = cos( pi * wj2 );
#else
            sincos( pi * wj1, &sj1, &cj1 );
            sincos( pi * wj2, &sj2, &cj2 );
#endif
            sumq += ( sj2 - sj1 ) * wband[ j ];
            double ci2 =
                ( cj2 - cj1 ) / ( eye * ( omega[ j + 1 ] - omega[ j ] ) );
            sumb += wpass[ j + 1 ] * ( ci2 + sj2 ) - wpass[ j ] * ( ci2 + sj1 );
        }
        q[ i ] = sumq / eye;
        b[ i ] = sumb / eye;

        //----------------------------------  do the second half of the q-loop
        sumq = 0.0;
        eye = double( i + M );
        for ( size_t j = 0; j < nF; j += 2 )
        {
            double wj1 = eye * frequencies[ j ];
            wj1 -= 2.0 * floor( wj1 / 2.0 );
            double wj2 = eye * frequencies[ j + 1 ];
            wj2 -= 2.0 * floor( wj2 / 2.0 );
            double sj1 = sin( pi * wj1 );
            double sj2 = sin( pi * wj2 );
            sumq += ( sj2 - sj1 ) * wband[ j ];
        }
        q[ i + M ] = sumq / eye;
    }

    //------------------------------------  Build symmetric matrix Q from a
    //                                      toeplitz + hankel
    LTMatrix Q( M + 1 );
    Q.toeplitz( M + 1, q );
    LTMatrix h;
    h.hankel( M + 1, q, q + M );
    Q += h;

    //------------------------------------  Solve Qa = b for the filter coeffs.
    Q.solve( b, coef + M );

    //------------------------------------  Build symmetric coefficient vector.
    for ( size_t i = 0; i < M; i++ )
        coef[ i ] = coef[ N - i ];
    coef[ M ] *= 2;

    //for (size_t i=0; i<=N; i++) cout << "coef[" << i << "] = " << coef[i]
    //                                 << endl;
}
