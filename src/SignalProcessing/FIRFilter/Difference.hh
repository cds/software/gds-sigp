#ifndef DIFFERENCE_HH
#define DIFFERENCE_HH
#include "FIRFilter.hh"

/**  Differential filter takes the difference between each successive 
  *  pair of elements.
  *  \brief Didderence filter
  */
class Difference : public FIRFilter
{
public:
    /**  Construct a difference filter for the specified sample rate.
    *  \brief Difference filter constructor.
    *  \param Hz Input signal sample rate.
    */
    explicit Difference( double Hz );

    /**  Destruy a differecne filter. ALl allocates s coefficient and history 
    *  storage is freed.
    *  \brief Difference filter destructor.
    */
    ~Difference( void );

    /**  Create an identical opy of the current filter instance.
    *  \brief Clone the filter.
    *  \return Pointer to the newly created copy of the difference filter.
    */
    Difference* clone( void ) const;

private:
};
#endif // !defined(DIFFERENCE_HH)
