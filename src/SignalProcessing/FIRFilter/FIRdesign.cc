/* -*- mode: c++; c-basic-offset: 4; -*- */
//
//    Filter class FIR filter implementation.
//
#include "FIRFilter.hh"
#include "FIRdesign.hh"
#include "window_api.hh"
#include "DVecType.hh"
#include "constant.hh"
#include "lcl_array.hh"
#include <iostream>
#include <cstring>
#include <cctype>
#include <cmath>

using namespace std;

typedef lcl_array< double > dble_vect;

//======================================  Filter design from poles, zeroes.
FIRFilter
design( double Hz, int nZeros, dComplex* Zeros )
{
    FIRFilter r;
    cerr << "Filter::design() has not been implemented. " << endl;
    cerr << "Please use dFirW(), dFirLS or dRemez() instead." << endl;
    return r;
}

//======================================  Filter design from poles, zeroes.
FIRFilter
dFirLS( size_t        N,
        double        Hz,
        size_t        nBand,
        const double* Bands,
        const double* Func,
        const double* Weight )
{
    if ( Hz <= 0 )
        throw std::invalid_argument( "Sampling Rate <= 0" );
    if ( nBand <= 0 )
        throw std::invalid_argument( "nBand <= 0" );

    FIRFilter r( N, Hz );
    dble_vect fBand( 2 * nBand ); // normalized bands

    for ( size_t i = 0; i < 2 * nBand; i++ )
    {
        fBand[ i ] = 2.0 * Bands[ i ] / Hz;
        if ( fBand[ i ] < 0.0 || fBand[ i ] > 1.0 )
        {
            throw std::invalid_argument( "Band limits not in the range 0-Fny" );
        }
    }

    dble_vect pCoefs( N + 1 );
    firls( N, nBand, fBand, Func, Weight, pCoefs );
    r.setCoefs( N, pCoefs );
    return r;
}

//======================================  Filter design from poles, zeroes.
FIRFilter
dRemez( int           N,
        double        Hz,
        int           nBand,
        const double* Bands,
        const double* Func,
        const double* Weight )
{
    if ( Hz <= 0 )
        throw std::invalid_argument( "Sampling Rate <= 0" );
    if ( nBand <= 0 )
        throw std::invalid_argument( "nBand <= 0" );

    FIRFilter r( N, Hz );
    dble_vect fBand( 2 * nBand ); // normalized bands

    for ( int i = 0; i < 2 * nBand; i++ )
    {
        fBand[ i ] = Bands[ i ] / Hz;
        if ( fBand[ i ] < 0.0 || fBand[ i ] > 0.5 )
        {
            throw std::invalid_argument( "Band limits not in the range 0-Fny" );
        }
    }

    int       jtype = 1;
    dble_vect pCoefs( N + 1 );
    remez( N + 1, jtype, nBand, 0, fBand, Func, Weight, pCoefs, 0, 0 );
    r.setCoefs( N, pCoefs );
    return r;
}

//======================================  Window and filter type translation
#define N_WMODE 7
#define N_FMODE 4
const char* wmode[ N_WMODE ] = { "rectangle", "triangle", "hamming", "ghamming",
                                 "hanning",   "kaiser",   "cheby" };
const char* fmode[ N_FMODE ] = {
    "lowpass", "highpass", "bandpass", "bandstop"
};

//======================================  Windowed sinc design method
FIRFilter
dFirW( int         N,
       double      Hz,
       const char* window,
       const char* type,
       double      Flow,
       double      Fhigh,
       double      Ripple,
       double      dF )
{
    if ( Hz <= 0 )
    {
        throw std::invalid_argument( "dFirW: Invalid sampling frequency." );
    }

    //----------------------------------  Get the window mode
    int wm = -1;
    for ( int i = 0; i < N_WMODE; i++ )
    {
        if ( !strcasecmp( wmode[ i ], window ) )
        {
            wm = i + 1;
            break;
        }
    }
    if ( wm < 0 )
    {
        if ( !strcasecmp( "hann", window ) )
        {
            wm = 5; // "Hann" == "Hanning"
        }
        else
        {
            throw std::invalid_argument( "dFirW: Invalid window type." );
        }
    }

    //----------------------------------  Get the filter mode
    int fm = -1;
    for ( int i = 0; i < N_FMODE; i++ )
    {
        if ( !strcasecmp( fmode[ i ], type ) )
        {
            fm = i + 1;
            break;
        }
    }
    if ( fm < 0 )
        throw std::invalid_argument( "Invalid filter mode." );

    //----------------------------------  Calculate the filter
    int     Nf = N;
    double* h = new double[ Nf ];
    int rc = FirW( Nf, wm, fm, Flow / Hz, Fhigh / Hz, 2 * dF / Hz, Ripple, h );
    if ( rc > 0 )
    { // if (rc==1) Nf was recalculated.
        delete[] h;
        h = new double[ Nf ];
        cerr << "Number of coefficients reset to " << Nf << endl;
        rc = FirW( Nf, wm, fm, Flow / Hz, Fhigh / Hz, 2 * dF / Hz, Ripple, h );
    }
    if ( rc )
        throw std::runtime_error( "dFirW: Unable to construct filter" );

    //----------------------------------  Fill in the filter
    FIRFilter r;
    r.setRate( Hz );
    r.setCoefs( Nf, h );
    delete[] h;
    return r;
}

//======================================  Estimate low-pass filter length
int
estLPLen( double Hz, double f1, double f2, double ripple, double atten )
{
    double Fny = 0.5 * Hz;
    if ( f1 < 0 || f2 < 0 || f1 > Fny || f2 > Fny )
    {
        std::cerr << "Error: Band limits must be in the range 0.0-" << Fny
                  << " Hz" << std::endl;
        return -1;
    }
    if ( ripple <= 0 || atten <= 0 )
    {
        std::cerr << "Filter ripple, attenuation (" << ripple << "," << atten
                  << ") must be >0" << std::endl;
        return -1;
    }
    double dr = log10( ripple );
    double da = log10( atten );
    double T = -0.4278 - 0.47610 * da - 0.5941 * dr + 0.07114 * dr * da -
        0.00266 * dr * dr + 0.005309 * dr * dr * da;
    double Kf = 11.01217 + 0.51244 * ( dr - da );
    double df = ( f2 - f1 ) / Hz;
    if ( df < 0.0 )
        df = -df;
    double order = T / df - Kf * df;
    return int( ceil( order ) ) + 1;
}

//======================================  Estimate multi-band filter length.
int
estLen( double Hz, int nBand, double* Band, double* Rip )
{
    int l, lmax = 0;
    for ( int j = 1; j < nBand; j++ )
    {
        l = estLPLen(
            Hz, Band[ 2 * j - 1 ], Band[ 2 * j ], Rip[ j - 1 ], Rip[ j ] );
        if ( l <= 0 )
            return -1;
        if ( l > lmax )
            lmax = l;
        l = estLPLen(
            Hz, Band[ 2 * j - 1 ], Band[ 2 * j ], Rip[ j ], Rip[ j - 1 ] );
        if ( l <= 0 )
            return -1;
        if ( l > lmax )
            lmax = l;
    }
    return lmax;
}

//======================================  Sinc Function.
static DVectD
SincVect( double f, long N, double t0, double dT )
{
    double omega = twopi * f;
    double dPhi = dT * omega;
    double Phi0 = t0 * omega;
    DVectD dv( N );
    for ( long i = 0; i < N; ++i )
    {
        double phi = Phi0 + double( i ) * dPhi;
        if ( fabs( phi ) < 1e-4 )
            dv[ i ] = 1 - phi * phi / 6.0;
        else
            dv[ i ] = sin( phi ) / phi;
    }
    dv *= 2.0 * f * dT; //  normalize by f/fNy
    return dv;
}

//======================================  Windowed sinc filter from Window.
FIRFilter
dFirW( int                N,
       double             Hz,
       const window_api&  win,
       const std::string& type,
       double             fLow,
       double             fHigh )
{
    string lctype = type;
    for ( size_t i = 0; i < type.size( ); i++ )
        lctype[ i ] = tolower( lctype[ i ] );

    if ( N <= 0 )
        throw runtime_error( "dFirW: invalid filter length." );
    if ( Hz <= 0 )
        throw runtime_error( "dFirW: invalid sample rate." );

    //----------------------------------
    double fNy = 0.5 * Hz;
    if ( fLow < 0 || fLow > fNy )
        throw runtime_error( "dFirW: Invalid low frequency (fLow)" );
    int    N2 = N / 2;
    double dT = 1.0 / Hz;
    double t0 = -double( N2 ) * dT;
    DVectD dv;

    //------------------------------  Lowpass: sinc(wLow)
    if ( lctype == "lowpass" )
    {
        dv = SincVect( fLow, N, t0, dT );
    }

    //------------------------------  Highpass: delta(t)-sinc(wHigh)
    else if ( lctype == "highpass" )
    {
        dv = SincVect( fLow, N, t0, dT );
        dv *= -1;
        dv[ N2 ] += 1.0;
    }

    //------------------------------  BandPass: sinc(wHigh)-sinc(wLow)
    else if ( lctype == "bandpass" )
    {
        if ( fHigh < 0 || fHigh > fNy )
            throw runtime_error( "dFirW: Invalid high frequency (fHigh)" );
        dv = SincVect( fHigh, N, t0, dT );
        dv -= SincVect( fLow, N, t0, dT );
    }

    //------------------------------  BandStop: delta(t)+sinc(wLow)-sinc(wHigh)
    else if ( lctype == "bandstop" )
    {
        if ( fHigh < 0 || fHigh > fNy )
            throw runtime_error( "dFirW: Invalid high frequency (fHigh)" );
        dv = SincVect( fLow, N, t0, dT );
        dv -= SincVect( fHigh, N, t0, dT );
        dv[ N2 ] += 1.0;
    }

    //------------------------------  Invalid filter type
    else
    {
        throw runtime_error( string( "dFirW: Invalid filter type: " ) + type );
    }

    //------------------------------  Window the filter coeffs.
    if ( win.getLength( ) == N && !win.is_periodic( ) )
    {
        dv *= win.refDVect( );
        dv *= win.getRMS( );
    }
    else
    {
        window_api* w = win.clone( );
        w->set_periodic( false );
        w->setWindow( N );
        dv *= w->refDVect( );
        dv *= w->getRMS( );
        delete w;
    }

    //-------------------------------  Construct the filter.
    FIRFilter fir( N - 1, Hz );
    fir.setCoefs( dv.refTData( ) );
    return fir;
}
