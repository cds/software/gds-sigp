#ifndef SLOPE_HH
#define SLOPE_HH
#include "FIRFilter.hh"

/**  The slope filter fits a line to a specified number of successive samples
  *  and returns the slope of that line.
  *  \brief %Slope filter.
  *  \author John Zweizig
  *  \version 1.0; Last modified September 9, 2003
  */
class Slope : public FIRFilter
{
public:
    /**  Construct a %Slope filter with the specified sample rate and line 
    *  length.
    *  \brief Data constructor.
    *  \param Hz Input data sample rate.
    *  \param nSample Number of samples in fit.
    */
    explicit Slope( double Hz, int nSample );

    /**  Destroy a %Slope filter.
    *  \brief destructor.
    */
    ~Slope( void );

    /**  Create an exact copy of the current instance.
    *  \brief Clone the current instance.
    *  \return Pointer to newly created %Slope filter instance.
    */
    Slope* clone( void ) const;

private:
};

#endif // !defined(SLOPE_HH)
