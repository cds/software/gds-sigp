/* -*- mode: c++; c-basic-offset: 4; -*- */
//
//    FIRFilter class FIR filter implementation.
//
#include "DVecType.hh"
#include "FIRFilter.hh"
#include "FSeries.hh"
#include "Interval.hh"
#include "TSeries.hh"
#include "lcl_array.hh"
#include "constant.hh"
#include <iostream>
#include <cmath>

using namespace std;

//======================================  Default constructor.
FIRFilter::FIRFilter( void )
    : mOrder( -1 ), mCoefs( 0 ), mSample( 0 ), mLastTerms( 0 ), mTerms( 0 ),
      mFIRmode( fm_causal )
{
    reset( );
}

//======================================  Construct an N-pole filter.
FIRFilter::FIRFilter( int Order, double Sample )
    : mCoefs( 0 ), mSample( Sample ), mLastTerms( 0 ), mTerms( 0 ),
      mFIRmode( fm_causal )
{
    setLength( Order + 1 );
}

//======================================  Construct an N-pole filter.
FIRFilter::FIRFilter( const FIRFilter& model )
    : mCoefs( 0 ), mLastTerms( 0 ), mTerms( 0 ), mFIRmode( fm_causal )
{
    *this = model;
}

//======================================  Delete history data.
void
FIRFilter::deleteHist( void ) NOEXCEPT
{
    if ( mLastTerms )
    {
        switch ( mDType )
        {
        case kReal:
            delete[] static_cast< double* >( mLastTerms );
            break;
        case kComplex:
            delete[] static_cast< fComplex* >( mLastTerms );
            break;
        }
        mLastTerms = 0;
    }
    mTerms = 0;
}

//======================================  FIRFilter destructor.
FIRFilter::~FIRFilter( void )
{
    if ( mCoefs )
        delete[] mCoefs;
    mCoefs = 0;
    deleteHist( );
}

//======================================  Reset time stamps and history
void
FIRFilter::reset( void )
{
    mCurTime = Time( 0 ); // Mark series inactive
    mStartTime = mCurTime;
    mTerms = 0; // Delete history
    mHistOK = false;
}

//======================================  See if data series is appropriate
void
FIRFilter::dataCheck( const TSeries& ts ) const
{

    //----------------------------------  Check frequency.
    double one = mSample * ts.getTStep( );
    if ( one > 1.0001 || one < 0.9999 )
    {
        throw std::invalid_argument( "Wrong frequency" );
    }

    //----------------------------------  Check history data valid.
    if ( mCurTime != Time( 0 ) && ts.getStartTime( ) != mCurTime )
    {
        throw std::invalid_argument( "Wrong start time" );
    }
}

//======================================  Filter a TSeries
TSeries
FIRFilter::apply( const TSeries& ts )
{
    TSeries r;
    apply( ts, r );
    return r;
}

//======================================  Filter a TSeries
TSeries&
FIRFilter::apply( const TSeries& in, TSeries& out )
{
    dataCheck( in );
    if ( &in == &out )
        throw invalid_argument( "Input overlaps output" );

    int nBins = in.getNSample( );
    out.Clear( );
    if ( nBins <= 0 )
        return out;

    //----------------------------------  Copy data to float TSeries
    Interval tStep = in.getTStep( );
    Time     start = in.getStartTime( );
    if ( mFIRmode == fm_zero_phase || mFIRmode == fm_drop_start )
    {
        start -= getTimeDelay( );
    }
    if ( in.refDVect( )->C_data( ) )
    {
        DVecType< fComplex > cDv( *in.refDVect( ) );
        apply( nBins, cDv.refTData( ), cDv.refTData( ) );
        out = TSeries( start, tStep, cDv );
    }
    else if ( in.refDVect( )->W_data( ) )
    {
        DVecType< dComplex > wDv( *in.refDVect( ) );
        apply( nBins, wDv.refTData( ), wDv.refTData( ) );
        out = TSeries( start, tStep, wDv );
    }
    else if ( in.refDVect( )->D_data( ) )
    {
        DVecType< double > dDv( *in.refDVect( ) );
        apply( nBins, dDv.refTData( ), dDv.refTData( ) );
        out = TSeries( start, tStep, dDv );
    }
    else
    {
        DVecType< float > fDv( *in.refDVect( ) );
        apply( nBins, fDv.refTData( ), fDv.refTData( ) );
        out = TSeries( start, tStep, fDv );
    }
    mCurTime = in.getEndTime( );
    if ( mStartTime == Time( 0 ) )
    {
        mStartTime = in.getStartTime( );
    }
    if ( mFIRmode == fm_drop_start && start < mStartTime )
    {
        out.eraseStart( mStartTime - start );
    }
    out.setF0( in.getF0( ) );
    out.setSigmaW( in.getSigmaW( ) );
    out.setName( in.getName( ) );
    out.appName( " (Filtered)" );
    return out;
}

//======================================  Filter and copy float vector
void
FIRFilter::apply( int nBins, const float* in, float* out )
{
    //----------------------------------  Set history OK if all terms stored
    if ( !mLastTerms || mDType != kReal )
        setHistory( 0, (float*)0 );
    mHistOK = ( mTerms >= mOrder );

    //----------------------------------  Perform the filter convolution
    double* Term = (double*)mLastTerms;
    for ( int j = 0; j < nBins; j++ )
    {
        double last = *in++;
        double sum = last * mCoefs[ 0 ];
        for ( int k = 0; k < mTerms; k++ )
        {
            double next = Term[ k ];
            Term[ k ] = last;
            sum += next * mCoefs[ k + 1 ];
            last = next;
        }
        if ( mTerms < mOrder )
            Term[ mTerms++ ] = last;
        *out++ = sum;
    }
}

//======================================  Filter and copy double vector
void
FIRFilter::apply( int nBins, const double* in, double* out )
{
    //----------------------------------  Set history OK if all terms stored
    if ( !mLastTerms || mDType != kReal )
        setHistory( 0, (double*)0 );
    mHistOK = ( mTerms >= mOrder );

    //----------------------------------  Perform the filter convolution
    double* Term = (double*)mLastTerms;
    for ( int j = 0; j < nBins; j++ )
    {
        double last = *in++;
        double sum = last * mCoefs[ 0 ];
        for ( int k = 0; k < mTerms; k++ )
        {
            double next = Term[ k ];
            Term[ k ] = last;
            sum += next * mCoefs[ k + 1 ];
            last = next;
        }
        if ( mTerms < mOrder )
            Term[ mTerms++ ] = last;
        *out++ = sum;
    }
}

void
FIRFilter::apply( int nBins, const fComplex* in, fComplex* out )
{
    //----------------------------------  Set history OK if all terms stored
    if ( !mLastTerms || mDType != kComplex )
        setHistory( 0, (fComplex*)0 );
    mHistOK = ( mTerms >= mOrder );

    //----------------------------------  Perform the filter convolution
    dComplex* Term = (dComplex*)mLastTerms;
    for ( int j = 0; j < nBins; j++ )
    {
        dComplex last = *in++;
        dComplex sum = last * mCoefs[ 0 ];
        for ( int k = 0; k < mTerms; k++ )
        {
            dComplex next = Term[ k ];
            Term[ k ] = last;
            sum += next * mCoefs[ k + 1 ];
            last = next;
        }
        if ( mTerms < mOrder )
            Term[ mTerms++ ] = last;
        *out++ = sum;
    }
}

void
FIRFilter::apply( int nBins, const dComplex* in, dComplex* out )
{
    //----------------------------------  Set history OK if all terms stored
    if ( !mLastTerms || mDType != kComplex )
        setHistory( 0, (dComplex*)0 );
    mHistOK = ( mTerms >= mOrder );

    //----------------------------------  Perform the filter convolution
    dComplex* Term = (dComplex*)mLastTerms;
    for ( int j = 0; j < nBins; j++ )
    {
        dComplex last = *in++;
        dComplex sum = last * mCoefs[ 0 ];
        for ( int k = 0; k < mTerms; k++ )
        {
            dComplex next = Term[ k ];
            Term[ k ] = last;
            sum += next * mCoefs[ k + 1 ];
            last = next;
        }
        if ( mTerms < mOrder )
            Term[ mTerms++ ] = last;
        *out++ = sum;
    }
}

//======================================  Copy an N-pole filter.
FIRFilter&
FIRFilter::operator=( const FIRFilter& model ) NOEXCEPT
{
    mSample = model.getRate( );
    mFIRmode = model.mFIRmode;
    setCoefs( model.getLength( ), model.mCoefs );
    return *this;
}

//======================================  Sum of two FIR Filters
FIRFilter&
FIRFilter::operator+=( const FIRFilter& fir )
{
    if ( no_coefs( ) || fir.no_coefs( ) )
    {
        throw runtime_error( "FIRFilter: sum with empty filter" );
    }
    else if ( mOrder != fir.mOrder || mSample != fir.mSample )
    {
        throw runtime_error( "FIRFilter: sum of incompatible filters" );
    }
    else
    {
        for ( int i = 0; i <= mOrder; i++ )
            mCoefs[ i ] += fir.mCoefs[ i ];
    }
    return *this;
}

//======================================  Difference of two FIR Filters
FIRFilter&
FIRFilter::operator-=( const FIRFilter& fir )
{
    if ( no_coefs( ) || fir.no_coefs( ) )
    {
        throw runtime_error( "FIRFilter: difference with empty filter" );
    }
    else if ( mOrder != fir.mOrder || mSample != fir.mSample )
    {
        throw runtime_error( "FIRFilter: difference of incompatible filters" );
    }
    else
    {
        for ( int i = 0; i <= mOrder; i++ )
            mCoefs[ i ] -= fir.mCoefs[ i ];
    }
    return *this;
}

//======================================  Product of two FIR Filters
FIRFilter&
FIRFilter::operator*=( const FIRFilter& model )
{
    //----------------------------------  Check argument is valid.
    if ( mSample != model.mSample )
    {
        throw invalid_argument( "Invalid sample rate" );
    }
    else if ( model.no_coefs( ) )
    {
        throw invalid_argument( "Product with empty filter!" );
    }

    const double* p2 = model.mCoefs;
    int           n2 = model.mOrder + 1;
    if ( no_coefs( ) )
    {
        setCoefs( n2, p2 );
    }
    else
    {
        //-------------------------------  Expand coefs list.
        int     n1 = mOrder;
        double* p1 = mCoefs;
        mOrder = n1 + n2 - 1;
        mCoefs = new double[ mOrder + 1 ];
        memcpy( mCoefs, p1, ( n1 + 1 ) * sizeof( double ) );
        delete[] p1;
        reset( );

        //-------------------------------  Start at end of combined coef list
        p1 = mCoefs + n1;
        double Cn = *p1;
        for ( int i = 0; i < n2; ++i )
            p1[ i ] = Cn * p2[ i ];

        //-------------------------------  add more coefficients
        while ( n1-- > 0 )
        {
            double* p = --p1;
            Cn = *p;
            *p++ *= *p2;
            for ( int i = 1; i < n2; ++i )
                *p++ = Cn * p2[ i ];
        }
    }
    return *this;
}

//======================================  Set the filter coefficients.
void
FIRFilter::setCoefs( int N, const double* Coefs ) NOEXCEPT
{
    setLength( N );
    setCoefs( Coefs );
}

//======================================  Modify the filter coefficients.
void
FIRFilter::setCoefs( const double* Coefs ) NOEXCEPT
{
    int  N = getLength( );
    bool Symm = true, ASym = true;
    for ( int i = 0; i < N; i++ )
    {
        mCoefs[ i ] = Coefs[ i ];
        if ( Coefs[ i ] != Coefs[ N - 1 - i ] )
            Symm = false;
        if ( Coefs[ i ] != -Coefs[ N - 1 - i ] )
            ASym = false;
    }
    if ( Symm )
        mType = kSymm;
    else if ( ASym )
        mType = kAnti;
    else
        mType = kGeneral;
}

//======================================  Set the history of the filter.
void
FIRFilter::setHistory( const TSeries& hist ) NOEXCEPT
{
    int  N = hist.getNSample( );
    Time t = hist.getEndTime( );
    if ( hist.refDVect( )->F_data( ) )
    {
        setHistory( N, reinterpret_cast< const float* >( hist.refData( ) ), t );
    }
    else if ( hist.refDVect( )->D_data( ) )
    {
        setHistory(
            N, reinterpret_cast< const double* >( hist.refData( ) ), t );
    }
    else if ( hist.refDVect( )->C_data( ) )
    {
        setHistory(
            N, reinterpret_cast< const fComplex* >( hist.refData( ) ), t );
    }
    else
    {
        lcl_array< float > data( N );
        hist.getData( N, data.get( ) );
        setHistory( N, data.get( ), t );
    }
}

//======================================  Set the history of the filter.
void
FIRFilter::setHistory( int N, const float* Hist, Time t ) NOEXCEPT
{
    if ( mOrder > 0 )
    {
        if ( !mLastTerms || mDType != kReal )
        {
            if ( mLastTerms )
                deleteHist( );
            mLastTerms = new double[ mOrder ];
            mDType = kReal;
        }
        if ( N > 0 )
        {
            int first = N - mOrder;
            if ( first < 0 )
                first = 0;
            double* Terms = (double*)mLastTerms;
            if ( Hist )
                for ( int i = first; i < N; i++ )
                    Terms[ N - i - 1 ] = Hist[ i ];
            else
                for ( int i = mTerms; i < ( N - first ); i++ )
                    Terms[ i ] = 0;
            mCurTime = t;
            mTerms = N - first;
        }
        else
        {
            mTerms = 0;
        }
    }
    mHistOK = false;
}

//======================================  Set the history of the filter.
void
FIRFilter::setHistory( int N, const double* Hist, Time t ) NOEXCEPT
{
    if ( mOrder > 0 )
    {
        if ( !mLastTerms || mDType != kReal )
        {
            if ( mLastTerms )
                deleteHist( );
            mLastTerms = new double[ mOrder ];
            mDType = kReal;
        }
        if ( N > 0 )
        {
            int first = N - mOrder;
            if ( first < 0 )
                first = 0;
            double* Terms = (double*)mLastTerms;
            if ( Hist )
                for ( int i = first; i < N; i++ )
                    Terms[ N - i - 1 ] = Hist[ i ];
            else
                for ( int i = mTerms; i < ( N - first ); i++ )
                    Terms[ i ] = 0;
            mCurTime = t;
            mTerms = N - first;
        }
        else
        {
            mTerms = 0;
        }
    }
    mHistOK = false;
}

//======================================  Set the history of the filter.
void
FIRFilter::setHistory( int N, const fComplex* Hist, Time t ) NOEXCEPT
{
    if ( mOrder > 0 )
    {
        if ( !mLastTerms || mDType != kComplex )
        {
            if ( mLastTerms )
                deleteHist( );
            mLastTerms = new dComplex[ mOrder ];
            mDType = kComplex;
        }
        if ( N > 0 )
        {
            int first = N - mOrder;
            if ( first < 0 )
                first = 0;
            dComplex* Terms = (dComplex*)mLastTerms;
            if ( Hist )
                for ( int i = first; i < N; i++ )
                    Terms[ N - i - 1 ] = Hist[ i ];
            else
                for ( int i = mTerms; i < ( N - first ); i++ )
                    Terms[ i ] = 0;
            mCurTime = t;
            mTerms = N - first;
        }
        else
        {
            mTerms = 0;
        }
    }
    mHistOK = false;
}

//======================================  Set the history of the filter.
void
FIRFilter::setHistory( int N, const dComplex* Hist, Time t ) NOEXCEPT
{
    if ( mOrder > 0 )
    {
        if ( !mLastTerms || mDType != kComplex )
        {
            if ( mLastTerms )
                deleteHist( );
            mLastTerms = new dComplex[ mOrder ];
            mDType = kComplex;
        }
        if ( N > 0 )
        {
            int first = N - mOrder;
            if ( first < 0 )
                first = 0;
            dComplex* Terms = (dComplex*)mLastTerms;
            if ( Hist )
                for ( int i = first; i < N; i++ )
                    Terms[ N - i - 1 ] = Hist[ i ];
            else
                for ( int i = mTerms; i < ( N - first ); i++ )
                    Terms[ i ] = 0;
            mCurTime = t;
            mTerms = N - first;
        }
        else
        {
            mTerms = 0;
        }
    }
    mHistOK = false;
}

//======================================  Set filter length (& reset history).
void
FIRFilter::setLength( int N ) NOEXCEPT
{
    deleteHist( );
    if ( mCoefs )
        delete[] mCoefs;
    mCoefs = 0;
    mOrder = N - 1;
    if ( mOrder >= 0 )
        mCoefs = new double[ mOrder + 1 ];
    reset( );
}

//======================================  Set time delay mode
void
FIRFilter::setMode( fir_mode mode )
{
    mFIRmode = mode;
}

//======================================  FIR Filter transfer function
bool
FIRFilter::Xfer( fComplex& coeff, double f ) const NOEXCEPT
{
    return Pipe::Xfer( coeff, f );
}

//======================================  FIR Filter transfer function
bool
FIRFilter::Xfer( fComplex* tf, const float* freqs, int points ) const NOEXCEPT
{
    return Pipe::Xfer( tf, freqs, points );
}

//======================================  FIR Filter transfer function
bool
FIRFilter::Xfer( FSeries& Fs, float Fmin, float Fmax, float dF ) const NOEXCEPT
{
    return Pipe::Xfer( Fs, Fmin, Fmax, dF );
}

//======================================  FIR Filter transfer function
FSeries
FIRFilter::Xfer( float Fmin, float Fmax, float dF ) const NOEXCEPT
{
    FSeries r;
    if ( mOrder < 0 || !mSample || !mCoefs )
        return r;
    float Fny = getRate( ) / 2.0;
    if ( Fmin < 0 )
        Fmin = 0.0;
    if ( Fmax == 0.0 || Fmax > Fny )
        Fmax = Fny;
    if ( Fmin >= Fmax )
        return r;
    if ( dF <= 0.0 )
        dF = 1.0;
    int nBins = int( ( Fmax - Fmin ) / dF + 0.5 );

    //----------------------------------  Fill a response curve
    fComplex* CVec = new fComplex[ nBins ];
    for ( int j = 0; j < nBins; j++ )
    {
        double   Freq = Fmin + double( j ) * dF;
        double   dPhi = twopi * Freq / mSample;
        double   Phi0 = twopi * fmod( Freq * mOrder / ( 2 * mSample ), 1.0 );
        dComplex Cj;
        Cj = polar( mCoefs[ 0 ], Phi0 );
        for ( int k = 1; k <= mOrder; k++ )
        {
            dComplex temp;
            temp = polar( mCoefs[ k ], Phi0 - k * dPhi );
            Cj += temp;
        }
        CVec[ j ] = fComplex( Cj );
    }
    r = FSeries( Fmin, dF, Time( 0 ), nBins / mSample, nBins, CVec );
    r.setName( "Filter Response" );
    delete[] CVec;
    return r;
}

//======================================  FIR Filter transfer function
bool
FIRFilter::xfer( fComplex& coeff, double f ) const NOEXCEPT
{
    dComplex temp;
    dComplex dcoeff( 0.0 );
    double   dPhi = twopi * f / mSample;
    double   Phi0 = dPhi * mOrder / 2;
    for ( int k = 0; k <= mOrder; k++ )
    {
        temp = polar( mCoefs[ k ], Phi0 - k * dPhi );
        dcoeff += temp;
    }
    coeff = fComplex( dcoeff );
    return true;
}

//======================================  Dump the filter coeffcients
void
FIRFilter::dump( std::ostream& ostr ) const NOEXCEPT
{
    ostr << "FIR Filter status:" << std::endl;
    ostr << "    Order: " << mOrder << "  Sample Rate " << mSample
         << "  Start time: " << mStartTime << "  Current time: " << mCurTime
         << std::endl;
    for ( int i = 0; i <= mOrder; i++ )
    {
        if ( i % 8 == 0 )
            ostr << "    Coefs:";
        ostr << " " << mCoefs[ i ];
        if ( i % 8 == 7 || i == mOrder )
            ostr << std::endl;
    }
}
