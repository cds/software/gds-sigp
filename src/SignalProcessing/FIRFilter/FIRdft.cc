/* -*- mode: c++; c-basic-offset: 4; -*- */
//
//    FIRdft class FIR filter implementation.
//
#include "DVecType.hh"
#include "FIRdft.hh"
#include "FIRFilter.hh"
#include "FSeries.hh"
#include "fSeries/DFT.hh"
#include "fft.hh"
#include "gds_functions.hh"
#include "constant.hh"
#include <iostream>

using namespace std;
using containers::DFT;

//======================================  Default constructor.
FIRdft::FIRdft( void ) : mOrder( -1 ), mSample( 0 ), mFIRmode( fm_causal )
{
    reset( );
}

//======================================  Construct an N-tap FIR filter.
FIRdft::FIRdft( int Order, double Sample )
    : mSample( Sample ), mFIRmode( fm_causal )
{
    setLength( Order + 1 );
}

//======================================  Copy constructor
FIRdft::FIRdft( const FIRdft& model )
{
    *this = model;
}

//======================================  Copy an FIRFilter
FIRdft::FIRdft( const FIRFilter& model )
{
    *this = model;
}

//======================================  Delete FIRdft history time series.
void
FIRdft::deleteHist( void )
{
    mHistory.Clear( );
}

//======================================  FIRdft destructor.
FIRdft::~FIRdft( void )
{
    reset( );
}

//======================================  FIRdft destructor.
void
FIRdft::reset( void )
{
    mCurTime = Time( 0 ); // Mark series inactive
    mStartTime = mCurTime;
    deleteHist( );
}

//======================================  See if data series is appropriate
void
FIRdft::dataCheck( const TSeries& ts ) const
{

    //----------------------------------  Check frequency.
    if ( fabs( double( mSample * ts.getTStep( ) ) - 1.0 ) > 0.0001 )
    {
        throw std::invalid_argument( "FIRdft: Wrong frequency" );
    }

    //----------------------------------  Check history data valid.
    if ( mCurTime != Time( 0 ) && ts.getStartTime( ) != mCurTime )
    {
        cerr << "FIRdft: Wrong start time. expected = " << mCurTime
             << " input start = " << ts.getStartTime( ) << endl;
        throw std::invalid_argument( "FIRdft: Wrong start time" );
    }
}

//======================================  Filter a TSeries
TSeries
FIRdft::apply( const TSeries& ts )
{
    TSeries r;
    apply( ts, r );
    return r;
}

//======================================  Filter a TSeries
TSeries&
FIRdft::apply( const TSeries& in, TSeries& out )
{
    dataCheck( in );

    size_t nBins = in.getNSample( );
    if ( nBins <= 0 || no_coefs( ) )
    {
        out.Clear( );
        return out;
    }

    //----------------------------------  Pick an optimal fft length
    size_t nTotal = wfft_pick_length( nBins + mOrder );
    size_t M = nTotal - nBins;

    //----------------------------------  Get Alignment pad
    Interval tStep = in.getTStep( );
    long     in_rate = long( 1.0 / tStep + 0.5 );
    long     aFact = in_rate / gcd( 1000000000L, in_rate );
    long     nAlign = M % aFact;
    if ( nAlign )
        nAlign = aFact - nAlign;

    //----------------------------------  Copy data to float TSeries
    Interval totalDt = double( nTotal ) * tStep;
    Interval tPad = double( M ) * tStep;
    Time     start = in.getStartTime( );
    Time     vStart = start - tPad;
    Interval totPad = double( M + nAlign ) * tStep;
    Time     histStart = start - totPad;

    //----------------------------------  Get an FFT of the coeficients.
    if ( !mCoefDFT || mCoefDFT->series_length( ) != nTotal )
    {
        DVecType< double >* dv = new DVecType< double >( nTotal );
        dv->replace( 0, nTotal, *mCoefs, 0, mOrder + 1 );
        dv->scale( 0, 1. / tStep, mOrder + 1 ); // Undo the DFT normalization.
        dv->Extend( nTotal );
        TSeries ts_coef( vStart, tStep, dv );
        mCoefDFT.reset( new DFT( ts_coef ) );
    }

    //----------------------------------  Build TSeries from history and input
    size_t nHist = mHistory.getNSample( );
    if ( nHist < M + nAlign )
    {
        DVector* dV = 0;
        if ( in.refDVect( )->D_data( ) )
        {
            dV = new DVecType< double >( nAlign + nTotal );
        }
        else if ( in.refDVect( )->C_data( ) )
        {
            dV = new DVecType< fComplex >( nAlign + nTotal );
        }
        else
        {
            dV = new DVecType< float >( nAlign + nTotal );
        }
        dV->ReSize( 0 );
        dV->Extend( M + nAlign - nHist );
        if ( nHist )
            dV->Append( *mHistory.refDVect( ) );
        else
            mStartTime = start;
        mHistory.setData( histStart, tStep, dV );
    }
    else if ( nHist > M + nAlign )
    {
        mHistory.eraseStart( double( nHist - M - nAlign ) * tStep );
    }
    if ( mHistory.Append( in ) )
        throw logic_error( "Time resolution error" );

    //-----------------------------------  Perform convolution in the f-domain
    DFT tempDFT( mHistory.extract( vStart, totalDt ) );
    tempDFT *= *mCoefDFT;

    //----------------------------------  Inverse DFT, remove padding...
    tempDFT.iFFT( out );
    out.eraseStart( tPad );

    //----------------------------------  Set start time as appropriate
    if ( mFIRmode == fm_zero_phase || mFIRmode == fm_drop_start )
    {
        Time tStart = start - tStep * 0.5 * double( mOrder );
        out.setData( tStart, tStep, out.refDVect( ) );
        if ( mFIRmode == fm_drop_start && tStart < mStartTime )
        {
            out.eraseStart( mStartTime - tStart );
        }
    }

    //-----------------------------------  Set all the other meta-data
    out.setF0( in.getF0( ) );
    out.setSigmaW( in.getSigmaW( ) );
    out.setName( in.getName( ) );
    out.appName( " (Filtered)" );
    mCurTime = mHistory.getEndTime( );
    return out;
}

//======================================  Copy an N-pole filter.
FIRdft&
FIRdft::operator=( const FIRdft& model )
{
    mSample = model.mSample;
    mOrder = model.mOrder;
    mFIRmode = model.mFIRmode;
    if ( model.mCoefs )
        mCoefs.reset( model.mCoefs->clone( ) );
    mCoefDFT.reset( );
    reset( );
    return *this;
}

//======================================  Copy an N-pole filter.
FIRdft&
FIRdft::operator=( const FIRFilter& model )
{
    mSample = model.getRate( );
    setCoefs( model.getLength( ), model.ref_coefs( ) );
    mFIRmode = static_cast< fir_mode >( model.getMode( ) );
    mCoefDFT.reset( );
    // reset(); part of SetCoefs(N, Coefs)
    return *this;
}

//======================================  Product of two FIR Filters
FIRdft&
FIRdft::operator*=( const FIRdft& model )
{
    //----------------------------------  Check valid argument filter.
    if ( mSample != model.mSample )
    {
        throw invalid_argument( "Invalid sample rate" );
    }
    if ( !model.no_coefs( ) )
    {
        throw invalid_argument( "Product with empty filter!" );
    }

    //----------------------------------  Make sure this is valid.
    const DVecType< double >& dv2 =
        dynamic_cast< DVecType< double >& >( *model.mCoefs );
    if ( no_coefs( ) )
    {
        setCoefs( model.getLength( ), dv2.refTData( ) );
    }
    else
    {
        //------------------------------  Make room for more coefficients
        int n1 = mOrder;
        int n2 = model.mOrder + 1;
        mOrder = n1 + n2 - 1;
        mCoefs->Extend( mOrder + 1 );

        //------------------------------  Store C1[n] * C2[i] in C[n+i]
        double* p1 =
            dynamic_cast< DVecType< double >& >( *mCoefs ).refTData( ) + n1;
        const double* p2 = dv2.refTData( );
        double        Cn = *p1;
        for ( int i = 0; i < n2; ++i )
            p1[ i ] = Cn * p2[ i ];

        //------------------------------  Loop over n1-1 ... 0
        while ( n1-- > 0 )
        {
            double* p = --p1;
            Cn = *p;
            *p++ *= *p2;
            for ( int i = 1; i < n2; ++i )
                *p++ += Cn * p2[ i ];
        }

        //-------------------------------  Reset the history and coef. DFT
        mCoefDFT.reset( );
        reset( );
    }
    return *this;
}

//======================================  Set the filter coefficients.
void
FIRdft::setCoefs( int N, const double* Coefs )
{
    setLength( N );
    setCoefs( Coefs );
}

//======================================  Modify the filter coefficients.
void
FIRdft::setCoefs( const double* Coefs )
{
    mCoefs.reset( new DVecType< double >( mOrder + 1, Coefs ) );
    mCoefDFT.reset( );
}

//======================================  Set the history of the filter.
void
FIRdft::setHistory( const TSeries& hist )
{
    mCurTime = Time( 0 );
    dataCheck( hist );
    mHistory = hist;
    mCurTime = hist.getEndTime( );
    mStartTime = mCurTime;
}

//======================================  Set the history of the filter.
void
FIRdft::setHistory( int N, const float* Hist, Time t )
{
    setHistory( TSeries( t, Interval( 1. / mSample ), N, Hist ) );
}

//======================================  Set the history of the filter.
void
FIRdft::setHistory( int N, const double* Hist, Time t )
{
    setHistory( TSeries( t, Interval( 1. / mSample ), N, Hist ) );
}

//======================================  Set the history of the filter.
void
FIRdft::setHistory( int N, const fComplex* Hist, Time t )
{
    setHistory( TSeries( t, Interval( 1. / mSample ), N, Hist ) );
}

//======================================  Set filter length (& reset history).
void
FIRdft::setLength( int N )
{
    mOrder = N - 1;
    mCoefs.reset( );
    mCoefDFT.reset( );
    reset( );
}

//======================================  Set the filter mode flag
void
FIRdft::setMode( fir_mode mode )
{
    mFIRmode = mode;
}

//======================================  FIR Filter transfer function
bool
FIRdft::Xfer( fComplex& coeff, double f ) const
{
    return Pipe::Xfer( coeff, f );
}

//======================================  FIR Filter transfer function
bool
FIRdft::Xfer( fComplex* tf, const float* freqs, int points ) const
{
    return Pipe::Xfer( tf, freqs, points );
}

//======================================  FIR Filter transfer function
bool
FIRdft::Xfer( FSeries& Fs, float Fmin, float Fmax, float dF ) const
{
    return Pipe::Xfer( Fs, Fmin, Fmax, dF );
}

//======================================  FIR Filter transfer function
FSeries
FIRdft::Xfer( float Fmin, float Fmax, float dF ) const
{
    FSeries r;
    if ( mOrder < 0 || !mSample || no_coefs( ) )
        return r;
    float Fny = getRate( ) / 2.0;
    if ( Fmin < 0 )
        Fmin = 0.0;
    if ( Fmax == 0.0 || Fmax > Fny )
        Fmax = Fny;
    if ( Fmin >= Fmax )
        return r;
    if ( dF <= 0.0 )
        dF = 1.0;
    int nBins = int( ( Fmax - Fmin ) / dF + 0.5 );

    //----------------------------------  Fill a response curve
    fComplex* CVec = new fComplex[ nBins ];
    fComplex  temp;
    for ( int j = 0; j < nBins; j++ )
    {
        CVec[ j ] = fComplex( 0.0 );
        float Freq = Fmin + double( j ) * dF;
        float dPhi = twopi * Freq / mSample;
        float Phi0 = dPhi * mOrder / 2;
        for ( int k = 0; k <= mOrder; k++ )
        {
            temp = polar< double >( mCoefs->getDouble( k ), Phi0 - k * dPhi );
            CVec[ j ] += temp;
        }
    }
    r = FSeries( Fmin, dF, Time( 0 ), nBins / mSample, nBins, CVec );
    r.setName( "Filter Response" );
    delete[] CVec;
    return r;
}

//======================================  FIR Filter transfer function
bool
FIRdft::xfer( fComplex& coeff, double f ) const
{
    fComplex temp;
    coeff = fComplex( 0.0 );
    float dPhi = twopi * f / mSample;
    float Phi0 = dPhi * mOrder / 2;
    for ( int k = 0; k <= mOrder; k++ )
    {
        temp = polar< double >( mCoefs->getDouble( k ), Phi0 - k * dPhi );
        coeff += temp;
    }
    return true;
}

//======================================  Dump the filter coeffcients
void
FIRdft::dump( std::ostream& ostr ) const
{
    ostr << "FIR Filter status:" << std::endl;
    ostr << "    Order: " << mOrder << "  Sample Rate " << mSample
         << "  Start time: " << mStartTime << "  Current time: " << mCurTime
         << std::endl;
    for ( int i = 0; i <= mOrder; i++ )
    {
        if ( i % 8 == 0 )
            ostr << "    Coefs:";
        ostr << " " << mCoefs->getDouble( i );
        if ( i % 8 == 7 || i == mOrder )
            ostr << std::endl;
    }
}
