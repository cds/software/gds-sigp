/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef RECOLOR_HH
#define RECOLOR_HH
#include "autopipe.hh"
#include "fSeries/PSD.hh"

/**  Recolor an input t-series using an FIR filter. The %recolor instance
  *  has a specified output spectrum. The FIRFilter implements the transfer 
  *  function from a specified input spectrum to the recolored output. If the
  *  input %PSD is empty or not specified, the input spectrum is assumed to 
  *  be white
  *  \note I intend to implement a prewhitening step to allow recoloring from
  *  an arbitrary input spectrum to the specified output.
  *  \brief Recoloring filter.
  *  \author John Zweizig (john.zweizig@ligo.org)
  *  \version $Id$
  */
class recolor : public Pipe
{
    using Pipe::apply;
    using Pipe::dataCheck;

public:
    /**  Construct a %recolor instance to color a whitened input to the 
      *  specified output spectrum.
      *  \brief Construct a whitened %recolor instance.
      *  \param out Required output spectrum.
      */
    recolor( const containers::PSD& out );

    /**  Construct a %recolor instance for a known input spectrum and known
      *  output spectrum.
      *  \brief Construct s %recolor instance for a knowninput spectrum.
      *  \param in  Known power spectrum of input data.
      *  \param out Requred spectrum of output data.
      */
    recolor( const containers::PSD& in, const containers::PSD& out );

    /**  Destroy a %recolor instance.
      *  \brief Destructor.
      */
    ~recolor( void );

    /**  Return a pointer to a new identical %recolor filter. 
      *  \brief Clone a recolor filter.
      *  \return pionter to the new filter.
      */
    recolor* clone( void ) const;

    /**  Recolor time series data. The time series in \a ts is recolored and 
      *  returned as a time series. The returned series length and sample 
      *  rate will be the same as the input series, but start time will (not?) 
      *  be advanced by one half the filter length.
      *  \brief Recolor a time series.
      *  \param ts Time series to be recolored.
      *  \return Recolored time series.
      */
    TSeries apply( const TSeries& ts );

    /**  Check that the argument time series is valid input for thus filter
      *  in its current state.
      *  \brief Check input data.
      *  \param ts %Time series to be tested.
      *  \exception std::runtime_error if the specified time series is
      *  not valid for this filter.
      */
    void dataCheck( const TSeries& ts ) const;

    /**  Get the GPS time of the next sample to be processed by the filter.
      *  If no data have been processed since the filter was constructed or, 
      *  reset, getCurrentTime returns zero.
      *  \brief Current time of data stream.
      *  \return Time of the next expected sample in the data stream or zero.
      */
    Time getCurrentTime( void ) const;

    /**  Get the GPS time of the first sample processed after the filter was 
      *  constructed or reset. If no data have been processed, getStartTime 
      *  returns zero.
      *  \brief Start of data stream.
      *  \return GPS time of start of data stream or zero.
      */
    Time getStartTime( void ) const;

    /**  Test whether the filter is in use.
      *  \brief Test if in use
      *  \return True if the filter is in use.
      */
    bool inUse( void ) const;

    /**  Make the filter kernel from a (squared) transfer function described 
      *  by the input PSD. The filter is calculated by taking the square root 
      *  of the PSD and performing an inverse fft on the result. The resulting 
      *  time series is then windowed by a 50\% Tukey window.
      *  \brief Calculate the filter kernel.
      *  \param psd Transfer function %PSD.
      */
    void mkFilter( const containers::PSD& psd );

    /**  Remake the filter kernel as necessary. The filter length defaults to
      *  4-seconds, but may be over-ridden by the SetFilterLength method.
      *  \brief Remake or update the filter kernel as appropriate.
      *  \param ts %Time series to be processed by the remade filter.
      */
    void remake( const TSeries& ts );

    /**  Reset the filter.
     */
    void reset( void );

    /**  Set the filter length in seconds. This can also be thought of as
      *  the filter frequency resolution. If the filter length is not set
      *  explicitly, it defaults to the input series length in seconds.
      *  \brief set the filter length.
      *  \param flen Filter length.
      */
    void setFilterLength( Interval flen );

private:
    containers::PSD _psdOut; ///<  Recolored spectrum
    containers::PSD _psdIn; ///<  Input spectrum
    Interval        _filterLength; ///<  Filter length in seconds.
    Interval        _inputSample; ///<  Input sample rate.
    //Whitener      _psdIn;         ///<  Input PSD estimator.
    auto_pipe _filter; ///<  Current FIR filter.
    Time      _current; ///<  Current time
    Time      _start; ///<  Start tim e of strea.
};

//======================================  inline functions.
inline Time
recolor::getCurrentTime( void ) const
{
    return _current;
}

inline Time
recolor::getStartTime( void ) const
{
    return _start;
}

inline bool
recolor::inUse( void ) const
{
    return ( _start != Time( 0 ) );
}

#endif // !defined(RECOLOR_HH)
