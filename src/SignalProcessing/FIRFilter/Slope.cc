/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "Slope.hh"

//======================================  Filter constructor
Slope::Slope( double Hz, int nSample ) : FIRFilter( nSample, Hz )
{
    if ( nSample <= 1 || Hz <= 0.0 )
        return;
    double SumX( 0.0 ), SumX2( 0.0 );
    for ( int i = 0; i < nSample; i++ )
    {
        double x = double( i ) / Hz;
        SumX += double( x );
        SumX2 += double( x * x );
    }
    double D = double( nSample ) * SumX2 - SumX * SumX;
    if ( D == 0 )
        return;
    double* Ci = new double[ nSample ];
    double  fac = double( nSample ) / ( D * Hz );
    double  off = SumX / D;
    for ( int i = 0; i < nSample; i++ )
    {
        Ci[ i ] = fac * double( nSample - i - 1 ) - off;
    }
    setCoefs( nSample, Ci );
    delete[] Ci;
}

//======================================  Destructor
Slope::~Slope( void )
{
}

//======================================  Clone method
Slope*
Slope::clone( void ) const
{
    return new Slope( *this );
}
