/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef FIRFILTER_HH
#define FIRFILTER_HH

#include "Complex.hh"
#include "Interval.hh"
#include "Pipe.hh"
#include <iosfwd>

class FSeries;

/**  The Filter class object implements a digital FIR filter that can be 
  *  used to filter time series (TSeries) data. Filter class instances 
  *  carry the signal history as well as the filter coefficients to allow
  *  data streaming without the introduction of edge effects. Each signal 
  *  (channel) to be filtered must use a separate Filter instance.
  *  @memo Digital FIR filter implementation API.
  *  @author  John G. Zweizig
  *  @version 1.2; Last modified: June 24, 2003
  */
class FIRFilter : public Pipe
{
public:
    /**  The filter mode flag affects the timing of the filtered time series.
     *  The two options are:
     *  <ul>
     *  <li> <b>Causal:</b> The time of each output sample is equal to 
     *                      the time of the corresponding input sample. 
     *                      For a filter of order M, this results in an 
     *                      effective filter delay of M/2 samples</li>
     *  <li> <b>Zero phase:</b> The filtered time series is delayed by M/2
     *                      samples. This results in zero apparent delay
     *                      and zero phase shift for symmetric filters.</li>
     *  <li> <b>Drop start:</b> The filtered time series is delayed as for
     *                      zero phase but all output samples before the 
     *                      start time are dropped. This results in a first
     *                      output series that is shorter than the input.</li>
     *  </ul>
     *  \brief FIR filter mode.
     */
    enum fir_mode
    {
        fm_causal, ///< Causal filter mode.
        fm_zero_phase, ///< Zero phase delay filter mode.
        fm_drop_start ///< Zero phase delay and remove samples before start time
    };

public:
    using Pipe::apply;
    using Pipe::dataCheck;

    /**  Default constructor. No coefficient or history storage is allocated.
    *  @memo Default constructor.
    */
    FIRFilter( void );

    /**  A real-valued Order 'length'-1 digital FIR filter is constructed
    *  which will operate on data sampled with specified frequency. 
    *  The filter coefficients remain uninitialized and the history data 
    *  is cleared.
    *  @memo  Construct an empty filter instance.
    *  @param length Number of terms (zeros) in the filter.
    *  @param Sample Nominal sampling rate.
    */
    FIRFilter( int length, double Sample );

    /**  A filter is constructed with the same length, frequency and 
    *  coefficients as the argument Filter. The history is zeroed.
    *  @memo  Copy constructor.
    *  @param model Filter to be copied.
    */
    FIRFilter( const FIRFilter& model );

    /**  Destroy the filter object and release any memory allocated for
    *  filter coefficients or history information.
    *  @memo Filter destructor.
    */
    virtual ~FIRFilter( void );

    /**  Create an identical FIR filter and return a pointer to the new
    *  filter. The history is zeroed.
    *  @memo Clone a FIR filter.
    *  @return pointer to an identical filter.
    */
    virtual FIRFilter*
    clone( void ) const
    {
        return new FIRFilter( *this );
    }

    /**  Test whether the TSeries is valid as input data for the filter.
    *  a runtime_error exception is thrown if the data are invalid.
    *  \brief  Check input data validity.
    *  \param ts %Time series to be checked for consistency with the expected 
    *            input data time and sample rate.
    */
    void dataCheck( const TSeries& ts ) const;

    /**  Get the expected start time of the next TSeries to be filtered.
    *  @memo   Get the current time.
    *  @return Expected time of the next input sample.
    */
    Time getCurrentTime( void ) const;

    /**  Get the filter length. The filter state is not affected.
    *  @memo   Get the filter length.
    *  @return The filter length (Order+1)
    */
    int getLength( void ) const NOEXCEPT;

    /**  Get the time mode value.
    *  @memo   Get the filter mode.
    *  @return The filter mode
    */
    fir_mode getMode( void ) const;

    /**  Get the sample rate. The filter state is not affected.
    *  @memo   Get the sample rate.
    *  @return The nominal sample rate in Hz.
    */
    double getRate( void ) const NOEXCEPT;

    /**  Get the start time of this filter run. This is set by the first 
    *  filter operation after the filter has been created or reset.
    *  @memo   Get the start time.
    *  @return true The start time.
    */
    Time getStartTime( void ) const;

    /**  Tests whether the previous filter operation used valid history data.
    *  History data is considered to be valid if the history buffer is full 
    *  and the start time of the input data is equal to the time stamp on the 
    *  history data. The filter state is not affected.
    *  @memo   Test the history status.
    *  @return true if the filter history data was valid for the last 
    *  operation.
    */
    bool getStat( void ) const NOEXCEPT;

    /**  Get the time delay imposed by the Filter. The time delay is only
    *  non-zero for filters that have a well defined delay i.e. FIR filters.
    *  A positive value indicates that the filter delays a signal by the 
    *  specified time.
    *  @memo Get the time delay.
    *  @return Signal time delay.
    */
    Interval getTimeDelay( void ) const;

    /**  Tests whether the filter is in use.
    *  @memo   Test the filter acitivity status.
    *  @return true if the filter is being used.
    */
    bool inUse( void ) const;

    /**  All filter coefficients and history data are formatted and written 
    *  to the specified ostream.
    *  @memo  Print the Filter status.
    *  @param ostr Output stream to receive the Filter info.
    */
    void dump( std::ostream& ostr ) const NOEXCEPT;

    /**  The TSeries is filtered and the result placed in a new TSeries. 
    *  The resulting TSeries is left on the stack and must be copied to 
    *  be made permanent. The filter status flag is set if there are 
    *  insufficient history entries (less than the Filter order) or if 
    *  the TSeries start time isn't contiguous with the previous data.
    *  The input samples are appended to the filter history as the filter
    *  response is calculated.
    *  @memo   Filter a Time Series.
    *  @param  in Time series to be filtered.
    *  @return A new TSeries containing the filter response to the input
    *          series.
    */
    TSeries apply( const TSeries& in );

    /**  The TSeries is filtered and the result stored in a specified output 
    *  TSeries. The output TSeries must already exist. The filter status 
    *  flag is set if there are insufficient history entries (less than the
    *  Filter order) or if the TSeries start time isn't contiguous with the
    *  previous data.
    *  The input samples are appended to the filter history as the filter
    *  response is calculated.
    *  @memo   Filter a TSeries into an output object.
    *  @param  in  Time series to be filtered.
    *  @param  out Time series into which the filter response will be stored.
    *  @return reference to the TSeries containing the filter response.
    */
    TSeries& apply( const TSeries& in, TSeries& out );

    /**  An N-point float series is filtered from in and stored in out. 
    *  in may overlap out. The input samples are appended to the filter 
    *  history as the filter response is calculated.
    *  @memo   Filter a float array.
    *  @param  N   Number of elements in the input series.
    *  @param  in  Float array containing the input series.
    *  @param  out Float array to contain the filter response. the end of 
    *              out may overlap the start of in.
    */
    void apply( int N, const float* in, float* out );
    /**  An N-point double series is filtered from in and stored in out. 
    *  in may overlap out. The input samples are appended to the filter 
    *  history as the filter response is calculated.
    *  @memo   Filter a float array.
    *  @param  N   Number of elements in the input series.
    *  @param  in  Float array containing the input series.
    *  @param  out Float array to contain the filter response. the end of 
    *              out may overlap the start of in.
    */
    void apply( int N, const double* in, double* out );

    /**  An N-point complex series is filtered from in and stored in out. 
    *  in may overlap out. The input samples are appended to the filter 
    *  history as the filter response is calculated.
    *  @memo   Filter a float array.
    *  @param  N   Number of elements in the input series.
    *  @param  in  Float array containing the input series.
    *  @param  out Float array to contain the filter response. the end of 
    *              out may overlap the start of in.
    */
    void apply( int N, const fComplex* in, fComplex* out );

    /**  An N-point complex series is filtered from in and stored in out. 
    *  in may overlap out. The input samples are appended to the filter 
    *  history as the filter response is calculated.
    *  @memo   Filter a float array.
    *  @param  N   Number of elements in the input series.
    *  @param  in  Float array containing the input series.
    *  @param  out Float array to contain the filter response. the end of 
    *              out may overlap the start of in.
    */
    void apply( int N, const dComplex* in, dComplex* out );

    /**  Zero the start time and current time and clear the history.
    *  @memo Reset the current time and history.
    */
    void reset( void );

    /**  A filter is constructed with the same length, frequency and 
    *  coefficients as the argument Filter. The history is zeroed.
    *  \brief Assignment operator.
    *  \param model Filter to be copied.
    *  \return Refernce to the current (modified) %FIRFilter instance.
    */
    FIRFilter& operator=( const FIRFilter& model ) NOEXCEPT;

    /**  The filter is combined with the argument filter resulting in a single 
    *  filter that performs the operations of the two input filters 
    *  simultaneously. This methos causes any history data to be reset.
    *  \brief Combine two filters into one.
    *  \exception std::invalid_argument is throw if the argument coefficients
    *  don't exist or if the sample rates differ.
    *  \param model Filter to be combined.
    *  \return Refernce to the current (modified) %FIRFilter instance.
    */
    FIRFilter& operator*=( const FIRFilter& model );

    /**  return a pointer to the coefficient list.
     */
    const double* ref_coefs( void ) const;

    /**  Set the filter length and coefficients. The filter history buffer
    *  is cleared.
    *  \brief  Set filter coefficients.
    *  @param  N     Number of coefficients to set.
    *  @param  Coefs A list of filter coefficients (filter impulse response).
    */
    void setCoefs( int N, const double* Coefs ) NOEXCEPT;

    /**  Modify the filter coefficients without clearing the filter history
    *  buffer.
    *  @memo   Modify filter coefficients.
    *  @param  Coefs A list of filter coefficients (filter impulse response).
    */
    void setCoefs( const double* Coefs ) NOEXCEPT;

    /**  The specified samples are copied to the Filter history vector. If 
    *  the number of samples specified is greater than the filter size, 
    *  only the last Norder samples are used. The filter current time is 
    *  set to the end-time of the input series.
    *  @memo  Set history.
    *  @param hist Filter history data.
    */
    void setHistory( const TSeries& hist ) NOEXCEPT;

    /**  The specified samples are copied to the Filter history vector. If 
    *  the number of samples specified is greater than the filter size, 
    *  only the last Norder samples are used. If the data vector argument 
    *  is omitted the number of history terms is set as specified and any 
    *  current data are prefixed with zeroes as needed to fill out the 
    *  requested number of terms. The history time is optional and represents
    *  the time of the latest sample.
    *  @memo  Set history.
    *  @param N    Number of history elements to preset.
    *  @param Hist Filter history vector.
    *  @param t    Time of the last sample in the history list.
    */
    void
    setHistory( int N, const float* Hist = 0, Time t = Time( 0 ) ) NOEXCEPT;

    /**  The specified samples are copied to the Filter history vector. If 
    *  the number of samples specified is greater than the filter size, 
    *  only the last Norder samples are used. If the data vector argument 
    *  is omitted the number of history terms is set as specified and any 
    *  current data are prefixed with zeroes as needed to fill out the 
    *  requested number of terms. The history time is optional and represents
    *  the time of the latest sample.
    *  @memo  Set history.
    *  @param N    Number of history elements to preset.
    *  @param Hist Filter history vector.
    *  @param t    Time of the last sample in the history list.
    */
    void setHistory( int N, const double* Hist, Time t = Time( 0 ) ) NOEXCEPT;

    /**  The specified samples are copied to the Filter history vector. If 
    *  the number of samples specified is greater than the filter size, 
    *  only the last Norder samples are used. If the data vector argument 
    *  is omitted the number of history terms is set as specified and any 
    *  current data are prefixed with zeroes as needed to fill out the 
    *  requested number of terms. The history time is optional and represents
    *  the time of the latest sample.
    *  @memo  Set history.
    *  @param N    Number of history elements to preset.
    *  @param Hist Filter history vector.
    *  @param t    Time of the last sample in the history list.
    */
    void setHistory( int N, const fComplex* Hist, Time t = Time( 0 ) ) NOEXCEPT;

    /**  The specified samples are copied to the Filter history vector. If 
    *  the number of samples specified is greater than the filter size, 
    *  only the last Norder samples are used. If the data vector argument 
    *  is omitted the number of history terms is set as specified and any 
    *  current data are prefixed with zeroes as needed to fill out the 
    *  requested number of terms. The history time is optional and represents
    *  the time of the latest sample.
    *  @memo  Set history.
    *  @param N    Number of history elements to preset.
    *  @param Hist Filter history vector.
    *  @param t    Time of the last sample in the history list.
    */
    void setHistory( int N, const dComplex* Hist, Time t = Time( 0 ) ) NOEXCEPT;

    /**  Set the filter length (order+1). The coefficients and history
    *  of the filter are cleared.
    *  @memo  Set Length.
    *  @param N new filter length.
    */
    void setLength( int N ) NOEXCEPT;

    /**  Set the filter mode. If the mode is set to \c fm_causal the time 
     *  of the samples in the output series are the same as those in the 
     *  input series. If the fiter is in \c fm_zero_phase mode, the start 
     *  time of the filtered series is shifted by \c order*sample/2.
     *  \brief Set the filter mode.
     *  \param mode New mode flag value
     */
    void setMode( fir_mode mode );

    /**  Set the filter sample rate.
    *  @memo Set the filter sample Rate.
    *  @param F New sample frequency.
    */
    void setRate( double F ) NOEXCEPT;

    /**  Add coefficients of an argument fir filter with the same sample
      *  frequency and length less than or equal to that of this filter.
      *  \brief Add the coefficients of an argument filter
      *  \param fir  fir filter to be subtracted.
      */
    FIRFilter& operator+=( const FIRFilter& fir );

    /**  Subtract coefficients of an argument fir filter with the same sample
      *  frequency and length less than or equal to that of this filter.
      *  \brief Subtract the coefficients of an argument filter
      *  \param fir  fir filter to be subtracted.
      */
    FIRFilter& operator-=( const FIRFilter& fir );

    /** The transfer coefficient of the filter at the specified 
    * frequency is calculated and returned as a complex number. 
    * Filters that support a fast way to compute a transfer coefficient 
    * should override xfer rather than this method.
    * @memo Get a transfer coefficent of a Filter.
    * @param coeff a complex number representing the Filter response 
    *              at the specified frequency (return)
    * @param f Frequency at which to sample the transfer function.
    * @return true if successful       
    */
    virtual bool Xfer( fComplex& coeff, double f ) const NOEXCEPT;

    /** The transfer function of the filter at the specified frequency 
    * points is calculated and returned as a complex array. 
    * The frequency points are user supplied. The return array
    * must be at least of length points.
    * @memo Get the transfer function of a Filter.
    * @param freqs Frequency points
    * @param points Number of points.
    * @param tf Transfer function (return)
    * @return true if successful       
    */
    bool Xfer( fComplex* tf, const float* freqs, int points ) const NOEXCEPT;

    /**  The transfer function of the filter in the specified frequency 
    *  interval is calculated and returned as a complex frequency series. 
    *  @memo   Get the transfer function of a Filter.
    *  @param  Fmin Minimum frequency at which to sample the transfer function.
    *  @param  Fmax Maximum frequency at which to sample the transfer function.
    *  @param  dF   Frequency step.
    *  @return a complex FSeries containing the Filter response at each 
    *          frequency step.
    */
    FSeries
    Xfer( float Fmin = 0.0, float Fmax = 0.0, float dF = 1.0 ) const NOEXCEPT;
    bool Xfer( FSeries& Fs,
               float    Fmin = 0.0,
               float    Fmax = 0.0,
               float    dF = 1.0 ) const NOEXCEPT;

protected:
    /** The transfer coefficient of the filter at the specified 
    * frequency is calculated and returned as a complex number. 
    * @memo Get a transfer coefficent of a Filter.
    * @param coeff a complex number representing the Filter response 
    *              at the specified frequency (return)
    * @param f Frequency at which to sample the transfer function.
    * @return true if successful       
    */
    bool xfer( fComplex& coeff, double f ) const NOEXCEPT;

private:
    void deleteHist( void ) NOEXCEPT;

    bool no_coefs( void ) const NOEXCEPT;

private:
    /**  The number of filter coefficients - 1.
    *  @memo Order of filter.
    */
    int mOrder;

    /**  Symmetry properties of the filter, none (kGeneral), symmetric (kSymm),
    *  or anti-symmetric (kAnti).
    *  @memo Filter type
    */
    enum
    {
        kGeneral, // No known symmetry
        kSymm, // Symmetric coefficients.
        kAnti // Anti-symmetric coefficients.
    } mType;

    /**  Type of data saved in the history.
    *  @memo History data type
    */
    enum
    {
        kReal, // float or double data
        kComplex // Complex data
    } mDType;

    /**  Coefficients.
    */
    double* mCoefs;

    /**  Design sample rate.
    */
    double mSample;

    /**  Last terms.
    */
    void* mLastTerms;

    /**  Number of terms processed.
    */
    int mTerms;

    /**  Time of next expected filtered sample.
    *  @memo Current sample time.
    */
    Time mCurTime;

    /**  Time of First Sample processed since creation or resetting of
    *  the filter.
    *  @memo Start time.
    */
    Time mStartTime;

    /**  Status of history before last filter.
    */
    bool mHistOK;

    fir_mode mFIRmode; ///  time mode.
};

#ifndef __CINT__
//--------------------------------------  Get the filter length
inline int
FIRFilter::getLength( void ) const NOEXCEPT
{
    return mOrder + 1;
}

//--------------------------------------  Get the filter mode
inline FIRFilter::fir_mode
FIRFilter::getMode( void ) const
{
    return mFIRmode;
}

//--------------------------------------  Get the sample rate
inline double
FIRFilter::getRate( void ) const NOEXCEPT
{
    return mSample;
}

//--------------------------------------  Get the processing start time
inline Time
FIRFilter::getStartTime( void ) const
{
    return mStartTime;
}

//--------------------------------------  Get the next expected sample time
inline Time
FIRFilter::getCurrentTime( void ) const
{
    return mCurTime;
}

//--------------------------------------  Get the next expected sample time
inline Interval
FIRFilter::getTimeDelay( void ) const
{
    return Interval( 0.5 * mOrder / mSample );
}

//--------------------------------------  Get the history status
inline bool
FIRFilter::getStat( void ) const NOEXCEPT
{
    return mHistOK;
}

//--------------------------------------  Is the filter in use?
inline bool
FIRFilter::inUse( void ) const
{
    return mCurTime != Time( 0 );
}

inline bool
FIRFilter::no_coefs( void ) const NOEXCEPT
{
    return !mCoefs;
}

//--------------------------------------  Pointer to coefficient vector
inline const double*
FIRFilter::ref_coefs( void ) const
{
    return mCoefs;
}

//--------------------------------------  Set the sample rate
inline void
FIRFilter::setRate( double F ) NOEXCEPT
{
    mSample = F;
}
#endif // __CINT__

#endif // FIRFILTER_HH
