/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef FIR_DESIGN_HH
#define FIR_DESIGN_HH

#include "FIRFilter.hh"

#ifdef __CINT__
namespace std
{
    class runtime_error;
    class invalid_argument;
} // namespace std
#else
#include <stdexcept>
#endif

#include <string>
class window_api;

/** \ingroup sigp_fir_design
  * \{
  */

/**  Estimate the number of terms needed for a multi-band filter.
  *  The desired filter response is described as a series of bands with 
  *  corresponding limits on the ripple. The filter state is not affected.
  *  @memo   Estimate the length of a multi-band filter.
  *  @param  nBand The number of pass or stop bands in the filter response.
  *  @param  Bands A list of band frequency edges.
  *  @param  Ripple The desired ripple in a pass-band or attenuation in a 
  *          stop-band for each frequency band.
  *  @return The estimated number of terms needed for a filter with the
  *          specified response.
  */
int estLen( double Hz, int nBand, double* Bands, double* Ripple );

/**  Estimate the number of terms needed for a low-pass filter. The 
  *  filter is specified by two frequencies (the upper limit of the
  *  pass band and the lower limit of the stop band), the maximum desired
  *  ripple and the minimum desired attenuation. The frequencies must
  *  be in the range \f$0 < Fpass < Fstop < Fnyquist = Rate/2\f$.
  *  @memo   Estimate the number of terms needed for a low-pass filter.
  *  @return The estimated number of terms needed for a filter with the
  *          specified response.
  *  @param  Fpass Upper edge frequency in Hz of the pass band.
  *  @param  FStop Lower edge frequency in Hz of the stop band.
  *  @param  rip   Desired maximum pass band ripple (as a fraction).
  *  @param  atten Desired stop band attenuation (as a fraction).
  */
int estLPLen( double Hz, double Fpass, double Fstop, double rip, double atten );

/**  Design a filter from lists of poles and zeros (Not yet implemented).
  *  @memo Filter design from poles and zeroes.
  */
FIRFilter design( double Hz, int nZeros = 0, dComplex* Zeros = 0 );

/**  Design a linear phase filter using the least squares method. The 
  *  user specifies a sample frequency, a list of band start- and stop 
  *  frequencies and the desired response at each band end-point. Weights
  *  may also be specified to reflect the relative importance of specific
  *  bands. The resulting filter coefficients are used to initialize the 
  *  FIRFilter returned by the function. The filter history is left unmodified.
  *  \brief  FIR filter design using the least squares method.
  *  \param N      Order (number of coefficients-1) of the filter.
  *  \param Hz     Sample frequency (i.e. twice the Nyquist frequency).
  *  \param nBand  Number of bands to be fitted.
  *  \param Bands  Band limit frequencies in Hz.
  *  \param Pass   Desired response in each band.
  *  \param Weight Weighting factor for each band. If not specified, Weight
  *         is assumed to be 1.0 for each band.
  *  \return The FIR filter.
  */
FIRFilter dFirLS( size_t        N,
                  double        Hz,
                  size_t        nBand,
                  const double* Bands,
                  const double* Pass,
                  const double* Weight = 0 );

/**  Design a optimal equiripple linear phase FIR filter from a list of 
  *  bands and the desired amplitudes in each band. nBand defines the 
  *  number of bands in which the response will be specified. 'Bands'
  *  contains a minimum and a maximum frequency for each band. 'Func'
  *  contains the desired amplitude for each band and 'Weight' contains
  *  an optional weighting factor for each band. The resulting
  *  filter coefficients are stored in the Filter object. The filter
  *  history is left unmodified.
  *  @memo  FIR filter design using the McClellan-Parks algorithm.
  *  @param nBand  Number of bands.
  *  @param Bands  Band limit frequencies in Hz.
  *  @param Func   Desired response in each band.
  *  @param Weight Weighting factor for each band. If not specified, Weight
  *         is assumed to be 1.0 for each band.
  */
FIRFilter dRemez( int           N,
                  double        Hz,
                  int           nBand,
                  const double* Bands,
                  const double* Func,
                  const double* Weight = 0 );

/**  Design a linear phase FIR filter using windowed Sinc functions. This 
  *  makes use of the fact that Sinc is the inverse fourier transform of the 
  *  theta function. The basic filter types generated from a sum of Sinc 
  *  functions (and constants) are: 
  *  - "LowPass"
  *  - "HighPass"
  *  - "BandPass"
  *  - "BandStop"
  *
  *  Windowing improves the finite approximation of the Sinc functions. The 
  *  windowing functions that may be used are 
  *  - \b Rectangle: 
  *  - \b %Triangle: (\e a.k.a. %Bartlett)
  *  - \b %Hamming:
  *  - \b Hann[ing]: \f$ 0.5 (1 - \cos{2 \pi i / (n - 1)} ) \f$
  *  - \b Kaiser:  Optimal window, tailored attenuation and transition width.
  *  - \b Cheby:
  * 
  *  \a N or \a dF will be inferred from the other parameters if omitted or 
  *  set to zero in Kaiser and Chebyshev designs. The same is true of 
  *  \a Ripple if it is omitted or set to zero in Chebyshev designs. The 
  *  designed filter coefficients replace the current coefficients. The filter 
  *  history is not affected.
  *  @memo  Filter design from windowed Sinc functions.
  *  @param N      Number of filter coefficients.
  *  @param Hz     Filter sampling rate.
  *  @param window Type of window on which to base the design.
  *  @param type   Type of filter.
  *  @param Flow   Transition frequency of a low-pass or high-pass filter,
  *                or lower edge of a band-pass or band-stop filter.
  *  @param Fhigh  Upper edge of a band-pass or band-stop filter.
  *  @param Ripple Desired attenuation for Kaiser filters or the desired 
  *                pass-band ripple for Chebyshev filters (in dB).
  *  @param dF     Transition band width in Hz for Chebyshev and Kaiser filters.
  */
FIRFilter dFirW( int         N,
                 double      Hz,
                 const char* window,
                 const char* type,
                 double      Flow,
                 double      Fhigh = 0,
                 double      Ripple = 0,
                 double      dF = 0 );

/**  Design a linear phase FIR filter using windowed Sinc functions. This 
  *  makes use of the fact that Sinc is the inverse fourier transform of the 
  *  theta function. The basic filter types generated from a sum of Sinc 
  *  functions (and constants) are: 
  *  - "LowPass"
  *  - "HighPass"
  *  - "BandPass"
  *  - "BandStop"
  *
  *  Windowing improves the finite approximation of the Sinc functions. Any
  *  Window-based class may be used.
  *  @memo  Filter design from windowed Sinc functions.
  *  @param N      Number of filter coefficients.
  *  @param Hz     Filter sampling rate.
  *  @param window Type of window on which to base the design.
  *  @param type   Type of filter.
  *  @param Flow   Transition frequency of a low-pass or high-pass filter,
  *                or lower edge of a band-pass or band-stop filter.
  *  @param Fhigh  Upper edge of a band-pass or band-stop filter.
  */
FIRFilter dFirW( int                N,
                 double             Hz,
                 const window_api&  win,
                 const std::string& type,
                 double             Flow,
                 double             Fhigh = 0 );

//======================================  Internal entry points.
int remez( int           nfilt,
           int           jtype,
           int           nbands,
           int           lgrid,
           const double* edge,
           const double* fx,
           const double* wtx,
           double*       h,
           double*       deviat = 0,
           double*       extrm = 0 );

/**  Calculate the coefficients for a linear phase FIR filter using the least 
  *  squares method. The user specifies a filter order, the number of bands
  *  to constrain, a list of band start- and stop frequencies and the desired 
  *  response at each band end-point. Weights may also be specified to reflect 
  *  the relative importance of specific bands.
  *  \brief FIR filter design using the least squares method.
  *  \param N      Order (number of coefficients-1) of the filter.
  *  \param nB     Number of bands to be fitted.
  *  \param frequencies Normalized band limit frequencies (\c f/f_ny\ [2nB] ).
  *  \param Pass   Desired response at each band end-point (\c [2nB] ).
  *  \param Weight Weighting factor for each band. If not specified, Weight
  *         is assumed to be 1.0 for each band (\c [nB] ).
  *  \return 0 on failure 1 on success.
  */
int firls( size_t       N,
           size_t       nB,
           const double frequencies[],
           const double pass[],
           const double weight[],
           double       coefs[] );

/**  Design a linear phase FIR filter using windowed Sinc functions. This 
  *  makes use of the fact that Sinc is the inverse fourier transform of the 
  *  theta function. The basic filter types generated from a sum of Sinc 
  *  functions (and constants) are: 
  *  - 1: LowPass
  *  - 2: HighPass
  *  - 3: BandPass
  *  - 4: BandStop
  *
  *  Windows are specified as follows:
  *
  *  - 1: Rectangle
  *  - 2: %Triangle
  *  - 3: %Hamming
  *  - 4: Generalized Hamming. The form of this window is 
  *       \f$ Wm = A + B*\cos{2*pi*m/(Nf-1)} \f$ where: A is specified by
  *       the \a df argument and B is set to (1-A).
  *  - 5: Hann[ing]
  *  - 6: Kaiser - 
  *  - 7: Cheby
  *  
  *  \brief FIR Filter design from windowed Sinc functions.
  *  \param Nf     Number of filter coefficients. Can be set to 0 to receive 
  *                the appropriate number of coefficients needed for a Kaiser 
  *                or Kaiser design with specified df, ripple parameters.
  *  \param wtype  Type of window on which to base the design.
  *  \param ftype  Type of filter.
  *  \param f1     Transition frequency of a low-pass or high-pass filter,
  *                or lower edge of a band-pass or band-stop filter (as a 
  *                fraction of the sample frequency).
  *  \param f2     Upper edge of a band-pass or band-stop filter (as a 
  *                fraction of the sample frequency).
  *  \param df     width of transition region for Cheby or Kaiser filters 
  *                (specified as a fraction of fNy).
  *  \param ripple Cheby filter ripple or Kaiser filter attenuation (in dB).
  *  \param g      Coefficient output vector [Nf].
  *  \return       Status codes:
  *                 - -1: failure
  *                 - 0: success
  *                 - 1: call again with returned 
  */
int FirW( int&    Nf,
          int     ftype,
          int     wtype,
          double  f1,
          double  f2,
          double  df,
          double  ripple,
          double* g );

//@}

#endif
