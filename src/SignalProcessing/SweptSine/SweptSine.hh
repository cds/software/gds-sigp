#ifndef _LIGO_SWEPTSINE_H
#define _LIGO_SWEPTSINE_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: SweptSine						*/
/*                                                         		*/
/* Module Description: swept sine analysis				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 10Jul02  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: SweptSine.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include <string>
#include "Complex.hh"

class window_api;
class Pipe;

/** Swept sine analysis. A sweep will go through the specified 
    frequency range, generate a sine waveform of the specified 
    measurement time, send it through a filter, window the result,
    down-convert it by the sine frequency, take the complex
    average of output and input and form the transfer coefficient.
   
    @memo Swept sine analysis
    @author Written July 2002 by Daniel Sigg
    @version 1.0
 ************************************************************************/
class SweptSine
{

protected:
    /// Sample frequency
    double fFsample;
    /// Start frequency
    double fFstart;
    /// Stop frequency
    double fFstop;
    /// Number of points
    int fPoints;
    /// Sweept type: Linear or Logarithmic
    std::string fType;
    /// Number of cycles to measure
    double fCycles;
    /// Minimum time to measure
    double fMinTime;
    /// Settling time (relative to the measurement time)
    double fSettlingTime;
    /// Window function
    window_api* fWindow;

public:
    /** Constructs a swept sine class with default parameters.
          @memo Default constructor.
       ******************************************************************/
    SweptSine( );
    /** Constructs a swept sine class with default parameters.
          @memo Default constructor.
       ******************************************************************/
    SweptSine( double            fsample,
               double            fstart,
               double            fstop,
               int               points = 101,
               const char*       type = "log",
               double            cycles = 10,
               double            mintime = 0.1,
               double            settle = 0.3,
               const window_api* window = 0 );
    /** Copy constructor.
          @memo Copy constructor.
       ******************************************************************/
    SweptSine( const SweptSine& prm );
    /** Destructor.
          @memo Destructor.
       ******************************************************************/
    ~SweptSine( );
    /** Assignment operator.
          @memo Assignment operator.
       ******************************************************************/
    SweptSine& operator=( const SweptSine& prm );

    /** Sweep a filter. The return arrays f and tf must hold at
          least fPoints data values.
          @memo Sweep.
          @param filter Filter
          @param f frequency array (return)
          @param tf transfer function (return)
          @return true if successful
       ******************************************************************/
    bool Sweep( const Pipe& filter, float* f, fComplex* tf ) const;

    /** Computes the filter response at a given frequency.
          @memo Sine response.
          @param filter Filter
          @param f frequency 
          @return transfer coefficient at frequency f
       ******************************************************************/
    fComplex operator( )( const Pipe& filter, float f ) const;

    /** Computes the filter response at a given frequency.
          @memo Sine response.
          @param filter Filter
          @param f frequency 
          @return transfer coefficient at frequency f
       ******************************************************************/
    fComplex
    Response( const Pipe& filter, float f ) const
    {
        return ( *this )( filter, f );
    }

    /** Get sampling rate.
          @memo Get sampling rate.
          @return sampling rate
       ******************************************************************/
    double
    GetSampling( ) const
    {
        return fFsample;
    }
    /** Set sampling rate.
          @memo Set sampling rate.
          @param fsample sampling rate
          @return void
       ******************************************************************/
    void
    SetSampling( double fsample )
    {
        fFsample = fsample;
    }
    /** Get start frequency.
          @memo Get start frequency.
          @return start frequency
       ******************************************************************/
    double
    GetFStart( ) const
    {
        return fFstart;
    }
    /** Set start frequency.
          @memo Set start frequency.
          @param fstart start frequency
          @return void
       ******************************************************************/
    void
    SetFStart( double fstart )
    {
        fFstart = fstart;
    }
    /** Get stop frequency.
          @memo Get stop frequency.
          @return stop frequency
       ******************************************************************/
    double
    GetFStop( ) const
    {
        return fFstop;
    }
    /** Set stop frequency.
          @memo Set stop frequency.
          @param fstop stop frequency
          @return void
       ******************************************************************/
    void
    SetFStop( double fstop )
    {
        fFstop = fstop;
    }
    /** Get number of points.
          @memo Get number of points.
          @return number of points
       ******************************************************************/
    int
    GetPoints( ) const
    {
        return fPoints;
    }
    /** Set number of points.
          @memo Set number of points.
          @param points number of points
          @return void
       ******************************************************************/
    void
    SetPoints( int points )
    {
        fPoints = points;
    }
    /** Get sweep type (linear or log).
          @memo Get sweep type.
          @return sweep type
       ******************************************************************/
    const char*
    GetSweepType( ) const
    {
        return fType.c_str( );
    }
    /** Set sweep type  (linear or log).
          @memo Set sweep type.
          @param type sweep type
          @return void
       ******************************************************************/
    void
    SetSweepType( const char* type )
    {
        fType = type ? type : "log";
    }
    /** Get number of measurement cycles.
          @memo Get number of measurement cycles.
          @return number of measurement cycles
       ******************************************************************/
    double
    GetCycles( ) const
    {
        return fCycles;
    }
    /** Set number of measurement cycles.
          @memo Set number of measurement cycles.
          @param cycles number of measurement cycles
          @return void
       ******************************************************************/
    void
    SetCycles( double cycles )
    {
        fCycles = cycles;
    }
    /** Get minimum measurement time (s).
          @memo Get minimum measurement time.
          @return minimum measurement time
       ******************************************************************/
    double
    GetMinTime( ) const
    {
        return fMinTime;
    }
    /** Set minimum measurement time (s).
          @memo Set minimum measurement time.
          @param mintime minimum measurement time
          @return void
       ******************************************************************/
    void
    SetMinTime( double mintime )
    {
        fMinTime = mintime;
    }
    /** Get settling time (relative to measurement time).
          @memo Get settling time.
          @return settling time
       ******************************************************************/
    double
    GetSettlingTime( ) const
    {
        return fSettlingTime;
    }
    /** Set settling time (relative to measurement time).
          @memo Set settling time.
          @param settle settling time
          @return void
       ******************************************************************/
    void
    SetSettlingTime( double settle )
    {
        fSettlingTime = settle;
    }
    /** Get window. The window is never 0. Default is Hanning.
          @memo Get window.
          @return window
       ******************************************************************/
    window_api*
    GetWindow( ) const
    {
        return fWindow;
    }
    /** Set window. Window is not adopted! If set to 0, Hanning is
          used.
          @memo Set window.
          @param w window
          @return void
       ******************************************************************/
    void SetWindow( const window_api* w );

protected:
    /// Compute a transfer coefficient
    bool ComputeCoeff( Pipe& f1, float f, fComplex& tf, window_api& win ) const;
};

#endif // _LIGO_SWEPTSINE_H
