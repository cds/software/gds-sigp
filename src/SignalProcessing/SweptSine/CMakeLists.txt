
add_library(SweptSine OBJECT
    SweptSine.cc
)

target_include_directories(SweptSine PRIVATE
    ${CMAKE_SOURCE_DIR}/src/SignalProcessing/FilterBase
    ${CMAKE_SOURCE_DIR}/src/SignalProcessing/Window

    ${CMAKE_SOURCE_DIR}/src/Base/complex
    ${CMAKE_SOURCE_DIR}/src/Base/time

    ${CMAKE_SOURCE_DIR}/src/Containers/DVector
    ${CMAKE_SOURCE_DIR}/src/Containers/FilterIO
    ${CMAKE_SOURCE_DIR}/src/Containers/TSeries

    ${CMAKE_SOURCE_DIR}/src/Math/wave
)

install(FILES
    SweptSine.hh
    DESTINATION include/gds-sigp
)
