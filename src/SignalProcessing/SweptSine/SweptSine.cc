#include <time.h>
#include <cmath>
#include <cstring>
#include "SweptSine.hh"
#include "Pipe.hh"
#include "Hanning.hh"
#include "Sine.hh"
#include "TSeries.hh"
#include "DVector.hh"

//______________________________________________________________________________
inline double
power( double x, double y )
{
    return exp( log( x ) * y );
}

//______________________________________________________________________________
SweptSine::SweptSine( )
    : fFsample( 16384. ), fFstart( 1.0 ), fFstop( 7000.0 ), fPoints( 101 ),
      fType( "log" ), fCycles( 5. ), fMinTime( 0.1 ), fSettlingTime( 0.3 ),
      fWindow( 0 )
{
    SetWindow( 0 );
}

//______________________________________________________________________________
SweptSine::SweptSine( double            fsample,
                      double            fstart,
                      double            fstop,
                      int               points,
                      const char*       type,
                      double            cycles,
                      double            mintime,
                      double            settle,
                      const window_api* window )
    : fFsample( fsample ), fFstart( fstart ), fFstop( fstop ),
      fPoints( points ), fType( type ), fCycles( cycles ), fMinTime( mintime ),
      fSettlingTime( settle ), fWindow( 0 )
{
    SetWindow( window );
}

//______________________________________________________________________________
SweptSine::SweptSine( const SweptSine& prm ) : fWindow( 0 )
{
    *this = prm;
}

//______________________________________________________________________________
SweptSine::~SweptSine( )
{
    if ( fWindow )
        delete fWindow;
}

//______________________________________________________________________________
SweptSine&
SweptSine::operator=( const SweptSine& prm )
{
    if ( this != &prm )
    {
        fFsample = prm.fFsample;
        fFstart = prm.fFstart;
        fFstop = prm.fFstop;
        fPoints = prm.fPoints;
        fType = prm.fType;
        fCycles = prm.fCycles;
        fMinTime = prm.fMinTime;
        fSettlingTime = prm.fSettlingTime;
        SetWindow( prm.fWindow );
    }
    return *this;
}

//______________________________________________________________________________
void
SweptSine::SetWindow( const window_api* w )
{
    if ( fWindow )
        delete fWindow;
    fWindow = w ? w->clone( ) : new Hanning( );
}

//______________________________________________________________________________
bool
SweptSine::Sweep( const Pipe& f1, float* f, fComplex* transfer_func ) const
{
    // check parameters
    if ( ( fPoints < 2 ) || ( fFstart <= 0 ) || ( fFstop <= 0 ) ||
         ( fMinTime < 0 ) || ( fCycles < 0 ) ||
         ( ( fCycles == 0 ) && ( fMinTime == 0 ) ) || ( fSettlingTime < 0 ) ||
         !fWindow )
    {
        return false;
    }
    // set frequencies
    double fstart = fFstart;
    double fstop = fFstop;
    if ( fstart > fstop )
    {
        double tmp = fstart;
        fstart = fstop;
        fstop = tmp;
    }
    if ( strncasecmp( fType.c_str( ), "lin", 3 ) == 0 )
    {
        if ( fPoints == 1 )
        {
            f[ 0 ] = ( fstart + fstop ) / 2.;
        }
        else
        {
            for ( int i = 0; i < fPoints; ++i )
            {
                f[ i ] =
                    fstart + (double)i / ( fPoints - 1.0 ) * ( fstop - fstart );
            }
        }
    }
    else
    {
        if ( fPoints == 1 )
        {
            f[ 0 ] = sqrt( fstart * fstop );
        }
        else
        {
            for ( int i = 0; i < fPoints; ++i )
            {
                f[ i ] = fstart *
                    power( fstop / fstart, (double)i / ( fPoints - 1.0 ) );
            }
        }
    }
    // intialize result
    for ( int i = 0; i < fPoints; ++i )
    {
        transfer_func[ i ] = fComplex( 0, 0 );
    }
    // loop over frequencies
    Pipe*       filter = f1.clone( );
    window_api* win = fWindow->clone( );
    for ( int i = 0; i < fPoints; ++i )
    {
        ComputeCoeff( *filter, f[ i ], transfer_func[ i ], *win );
    }
    delete filter;
    delete win;

    return true;
}

//______________________________________________________________________________
fComplex
SweptSine::operator( )( const Pipe& f1, float f ) const
{
    fComplex    tf;
    Pipe*       filter = f1.clone( );
    window_api* win = fWindow->clone( );
    if ( !ComputeCoeff( *filter, f, tf, *win ) )
    {
        tf = fComplex( 0, 0 );
    }
    delete filter;
    delete win;
    return tf;
}

//______________________________________________________________________________
bool
SweptSine::ComputeCoeff( Pipe&       filter,
                         float       f,
                         fComplex&   tf,
                         window_api& win ) const
{
    // int test = 0;
    // static int i = 0;
    // Number of points in time series
    int N1 = (int)( fFsample * fCycles / f + 0.5 );
    int N2 = (int)( fFsample * fMinTime + 0.5 );
    int N = N2 > N1 ? N2 : N1;
    int NN = (int)( (double)N * ( 1. + fSettlingTime ) + 0.5 );
    if ( N < 1 )
    {
        return false;
    }
    // Time t0 = Now();
    // fill time series
    TSeries in( Time( 1 ), Interval( 1. / fFsample ), NN, Sine( f, 1.0 ) );
    // apply filter
    TSeries out;
    // Time t1 = Now();
    filter.reset( );
    out = filter( in );
    // Time t2 = Now();
    // if (i == test) {
    // in.setName ("Sine Excitation");
    // out.setName ("Filtered Sine");
    // Plot (in, out);
    // }
    // throw away settling time
    in.eraseStart( Interval( (double)( NN - N ) / fFsample ) );
    out.eraseStart( Interval( (double)( NN - N ) / fFsample ) );
    // apply window function
    in = win( in );
    out = win( out );
    //Time t3 = Now();
    // if (i == test) {
    // in.setName ("Windowed Sine Excitation");
    // out.setName ("Windowed Filtered Sine");
    // Plot (in, out);
    // }
    // Mix down
    in.Convert( DVector::t_complex ); // needed because the assignment
    out.Convert( DVector::t_complex ); // operator is broken
    in = in.fShift( -f );
    out = out.fShift( -f );
    // Time t4 = Now();
    // if (i == test) {
    // in.setName ("Mixed Down Sine Excitation");
    // out.setName ("Mixed Down Filtered Sine");
    // Plot (in, out);
    // }
    // compute transfer coefficient
    // std::complex<double> a (0.0, 0.0);
    // std::complex<double> b (0.0, 0.0);
    // fComplex val;
    // for (int j = 0; j < (int)in.getNSample(); ++j) {
    // val = ((const TSeries&)in).refDVect()->getCplx (j);
    // a += (fComplex&)val;
    // val = ((const TSeries&)out).refDVect()->getCplx (j);
    // b += (fComplex&)val;
    // }
    // std::complex<double> c (b / a);
    // tf = fComplex (c.real(), c.imag());
    dComplex c = out.getComplexAverage( ) / in.getComplexAverage( );
    tf = fComplex( c.real( ), c.imag( ) );
    // Time t5 = Now();
    // cout << "sin:    " << t1 - t0 << endl;
    // cout << "filter: " << t2 - t1 << endl;
    // cout << "window: " << t3 - t2 << endl;
    // cout << "mix:    " << t4 - t3 << endl;
    // cout << "avrg:   " << t5 - t4 << endl;
    // ++i;
    return true;
}
