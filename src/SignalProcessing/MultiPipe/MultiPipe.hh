/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef MULTIPIPE_HH
#define MULTIPIPE_HH

#include "autopipe.hh"
#include <vector>

/**  %MultiPipe is an arbitrary compound filter composed of one or more
  *  Pipe segments. 
  *  @memo Compound filter class
  *  @author John G. Zweizig
  *  @version 1.2; Last modified June 24, 2007
  */
class MultiPipe : public Pipe
{
public:
    using Pipe::apply;
    using Pipe::dataCheck;

    /// Pipe array type
    typedef std::vector< auto_pipe > PipeConfig;

    /// Size data type
    typedef PipeConfig::size_type size_type;

    /**  Construct an empty MultiPipe.
      *  @memo Default constructor
      */
    MultiPipe( void );

    /**  Construct a %MultiPipe identical to the existing argument
      *  %MultiPipe. Each filter in the argument %MultiPipe is cloned
      *  and added to the current %MultiPipe in the same order as the 
      *  original pipe. The original contents of the current instance
      *  are deleted.
      *  @memo Assignment operator
      *  @param p Constant reference to the %MultiPipe to be copied.
      *  @return Reference to the updated %MultiPipe
      */
    MultiPipe( const MultiPipe& p );

    /**  Destroy a MultiPipe.
      *  @memo Destructor
      */
    ~MultiPipe( void );

    /**  Create an identical copy of the current %MultiPipe.
      *  @memo Clone a %MultiPipe.
      *  @return Pointer to the new %MultiPipe.
      */
    MultiPipe* clone( void ) const;

    /**  Remove all filters from the pipe vector and reset the gain to 1.
      *  @memo Clear all filters in the %MultiPipe.
      */
    void clear( void );

    /**  Assign the contents of the argument operator to the current 
      *  %MultiPipe. Each filter in the argument %MultiPipe is cloned
      *  and added to the current %MultiPipe in the same order as the 
      *  original pipe. The original contents of the current instance
      *  are deleted.
      *  @memo Assignment operator
      *  @param p Constant reference to the %MultiPipe to be copied.
      *  @return Reference to the updated %MultiPipe
      */
    MultiPipe& operator=( const MultiPipe& p );

    /**  Add a clone of a filter to the tail of the %MultiPipe. The specified 
      *  Pipe is clone using the Pipe::clone() method and added to the
      *  end of the %MultiPipe. Ownership of the argument filter is retained
      *  by the calling function.
      *  @memo Add a filter.
      *  @param p Constant reference to the Pipe to be added.
      *  @return Pointer to the cloned filter added to the %MultiPipe.
      */
    Pipe* addPipe( const Pipe& p );

    /**  Add a filter to the tail of the %MultiPipe. Ownership of the 
      *  specified filter is taken by the %MultiPipe.
      *  @memo Add a filter to the %MultiPipe.
      *  @param p Pointer to the Pipe to be added.
      *  @return pointer to the added Pipe.
      */
    Pipe* addPipe( Pipe* p );

    /**  Return a constant reference to the filter array. 
      *  @memo Reference the filter array.
      *  @return Constant reference to a vector of pipe pointers.
      */
    const PipeConfig&
    pipe( void ) const
    {
        return mPipe;
    }

    /**  Return a reference to the filter array. 
      *  @memo Reference the filter array.
      *  @return Reference to a vector of pipe pointers.
      */
    PipeConfig&
    pipe( void )
    {
        return mPipe;
    }

    /**  Apply the compound filter to a time series. Each filter is applied 
      *  to the time series in the order that it was added to the %MultiPipe.
      *  @memo Filter a time series.
      *  @param ts Input time series.
      *  @return Filtered time series.
      */
    TSeries apply( const TSeries& ts );

    /**  Check that the specified time series is valid input for the 
      *  compound filter. An exception is thrown if the %MultiPipe can not
      *  be applied to the specified data.
      *  @memo Check data is valid for filtering.
      *  @param ts Time series to be checked for validity. 
      */
    void dataCheck( const TSeries& ts ) const;

    /**  Test whether the %MultiPipe s empty.
      *  \brief test for empty.
      *  \return true if empty.
      */
    bool empty( void ) const;

    /**  Test whether the compound filter is in use. The compound filter is
      *  considered to be in use if any of the component filters is in use.
      *  @memo Test whether filter is in use.
      *  @return True if compound filter is in use.
      */
    bool inUse( void ) const;

    /**  Get start time of the data segment currently in progress.
      *  @memo Get start time of epoch.
      *  @return Start time.
      */
    Time getStartTime( void ) const;

    /**  Get the Time of the next expected sample to be processed.
      *  @memo Get current time.
      *  @return Current time.
      */
    Time getCurrentTime( void ) const;

    /**  Get the additional gain.
      *  @memo Get gain.
      *  @return gain.
      */
    double getGain( void ) const;

    /**  Get the delay time of the combined filters.
      *  @memo Get time delay.
      *  @return Interval delay.
      */
    Interval getTimeDelay( void ) const;

    /**  Reset all filters.
      *  @memo Reset
      */
    void reset( );

    /**  Set the additional gain.
      *  @memo Set gain.
      *  @param gain New gain value.
      */
    void setGain( double gain );

    /**  Return the number of filters in the pipe.
      *  @memo Number of filters.
      *  @returns Number of filters.
      */
    size_type size( void ) const;

protected:
    /** The transfer coefficient of the filter at the specified 
      * frequency is calculated and returned as a complex number. 
      * @memo Get a transfer coefficient of a Filter.
      * @param coeff a complex number representing the Filter response 
      *              at the specified frequency (return)
      * @param f Frequency at which to sample the transfer function.
      * @return true if successful       
      */
    bool xfer( fComplex& coeff, double f ) const NOEXCEPT;

private:
    /// Pipe array
    PipeConfig mPipe;
    /// Additional Gain factor
    double mGain;
};

//======================================  Inline methods
inline MultiPipe::size_type
MultiPipe::size( void ) const
{
    return mPipe.size( );
}

inline bool
MultiPipe::empty( void ) const
{
    return mPipe.empty( );
}

#endif // MULTIFILTER_HH
