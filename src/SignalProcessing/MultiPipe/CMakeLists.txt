
add_library(MultiPipe OBJECT
    MultiPipe.cc
)

target_include_directories(MultiPipe PRIVATE
    ${CMAKE_SOURCE_DIR}/src/SignalProcessing/FilterBase

    ${CMAKE_SOURCE_DIR}/src/Base/complex
    ${CMAKE_SOURCE_DIR}/src/Base/time

    ${CMAKE_SOURCE_DIR}/src/Containers/FilterIO
    ${CMAKE_SOURCE_DIR}/src/Containers/TSeries
)

install(FILES
    MultiPipe.hh
    DESTINATION include/gds-sigp
)
