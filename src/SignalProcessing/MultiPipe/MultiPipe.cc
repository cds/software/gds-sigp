/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "MultiPipe.hh"

using namespace std;

//======================================  Default constructor
MultiPipe::MultiPipe( void ) : mGain( 1 )
{
}

//======================================  Copy constructor
MultiPipe::MultiPipe( const MultiPipe& p )
    : mPipe( p.size( ) ), mGain( p.mGain )
{
    size_type N = p.size( );
    for ( size_type i = 0; i < N; ++i )
    {
        mPipe[ i ] = p.mPipe[ i ]->clone( );
    }
}

//======================================  Destructor
MultiPipe::~MultiPipe( void )
{
    clear( );
}

//======================================  Clone
void
MultiPipe::clear( void )
{
    mPipe.clear( );
    mGain = 1;
}

//======================================  Clone
MultiPipe*
MultiPipe::clone( void ) const
{
    return new MultiPipe( *this );
}

//======================================  Assignment
MultiPipe&
MultiPipe::operator=( const MultiPipe& p )
{
    if ( this != &p )
    {
        clear( );
        mGain = p.mGain;
        size_type N = p.size( );
        mPipe.resize( N );
        for ( size_type i = 0; i < N; ++i )
        {
            mPipe[ i ] = p.mPipe[ i ]->clone( );
        }
    }
    return *this;
}

//======================================  Add pipe
Pipe*
MultiPipe::addPipe( Pipe* p )
{
    mPipe.push_back( p );
    return p;
}

Pipe*
MultiPipe::addPipe( const Pipe& p )
{
    return addPipe( p.clone( ) );
}

//======================================  Apply
TSeries
MultiPipe::apply( const TSeries& ts )
{
    TSeries   x = ts;
    size_type N = size( );
    for ( size_type i = 0; i < N; ++i )
    {
        x = mPipe[ i ]->apply( x );
    }
    if ( mGain != 1 )
        x *= mGain;
    x.setUnits( ts.getUnits( ) );
    return x;
}

//======================================  Data check
void
MultiPipe::dataCheck( const TSeries& ts ) const
{
    if ( !mPipe.empty( ) )
        mPipe[ 0 ]->dataCheck( ts );
}

//======================================  in use
bool
MultiPipe::inUse( void ) const
{
    size_type N = size( );
    for ( size_type i = 0; i < N; i++ )
    {
        if ( mPipe[ i ]->inUse( ) )
            return true;
    }
    return false;
}

//======================================  Start time
Time
MultiPipe::getStartTime( void ) const
{
    if ( mPipe.empty( ) )
        return Time( 0 );
    return mPipe[ 0 ]->getStartTime( );
}

//======================================  current time
Time
MultiPipe::getCurrentTime( void ) const
{
    if ( mPipe.empty( ) )
        return Time( 0 );
    return mPipe[ 0 ]->getCurrentTime( );
}

//======================================  get gain
double
MultiPipe::getGain( void ) const
{
    return mGain;
}

//======================================  get Time delay
Interval
MultiPipe::getTimeDelay( void ) const
{
    Interval  delay( 0.0 );
    size_type N = size( );
    for ( size_type i = 0; i < N; i++ )
    {
        delay += mPipe[ i ]->getTimeDelay( );
    }
    return delay;
}

//======================================  set gain
void
MultiPipe::setGain( double gain )
{
    mGain = gain;
}

//======================================  Reset
void
MultiPipe::reset( )
{
    size_type N = size( );
    for ( size_type i = 0; i < N; ++i )
    {
        mPipe[ i ]->reset( );
    }
}

//======================================  Xfer
bool
MultiPipe::xfer( fComplex& coeff, double f ) const NOEXCEPT
{
    coeff = fComplex( mGain, 0 );
    fComplex  temp;
    size_type N = size( );
    for ( size_type i = 0; i < N; ++i )
    {
        if ( !mPipe[ i ]->Xfer( temp, f ) )
        {
            return false;
        }
        coeff *= temp;
    }
    return true;
}
