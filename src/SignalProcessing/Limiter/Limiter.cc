#include <time.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#include <strings.h>
#include <stdio.h>
#include "Limiter.hh"
#include "constant.hh"
#include "DVector.hh"
#include "FSeries.hh"
#include <string>
#include <iostream>
#include <stdexcept>

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Limiter                                                              //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
Limiter::Limiter( )
    : fSample( 1. ), fType( kNoLimits ), fLow( -1. ), fHigh( 1. ),
      fSlewRate( 1. ), fLast( 0. )
{
    init( 1.0, kNoLimits, 0 );
}

//______________________________________________________________________________
Limiter::Limiter( double lim )
    : fSample( 1. ), fType( kNoLimits ), fLow( -1. ), fHigh( 1. ),
      fSlewRate( 1. ), fLast( 0. )
{
    init( 1.0, kValues, -lim, lim );
}

//______________________________________________________________________________
Limiter::Limiter( double low, double high )
    : fSample( 1. ), fType( kNoLimits ), fLow( -1. ), fHigh( 1. ),
      fSlewRate( 1. ), fLast( 0. )
{
    init( 1.0, kValues, low, high );
}

//______________________________________________________________________________
Limiter::Limiter(
    double fsample, limiter_type type, double l1, double l2, double l3 )
    : fSample( 1. ), fType( kNoLimits ), fLow( -1. ), fHigh( 1. ),
      fSlewRate( 1. ), fLast( 0. )
{
    init( fsample, type, l1, l2, l3 );
}

//______________________________________________________________________________
void
Limiter::dataCheck( const TSeries& ts ) const
{
    // Check frequency
    if ( ( fType == kSlewRate ) || ( fType == kBoth ) )
    {
        double one = fSample * ts.getTStep( );
        if ( one > 1.0001 || one < 0.9999 )
        {
            throw std::invalid_argument( "Wrong frequency" );
        }
    }
    // Check history data valid.
    if ( ( fType == kSlewRate ) || ( fType == kBoth ) )
    {
        if ( fCurTime != Time( 0 ) && ts.getStartTime( ) != fCurTime )
        {
            throw std::invalid_argument( "Wrong start time" );
        }
    }
}

//______________________________________________________________________________
TSeries
Limiter::apply( const TSeries& in )
{
    TSeries out;
    dataCheck( in );
    out.Clear( );
    out = in;
    const TSeries& constout( out );
    int            nBins = in.getNSample( );
    if ( nBins <= 0 )
        return out;

    // Copy data to float TSeries
    if ( constout.isComplex( ) )
    {
        fComplex* cd = (fComplex*)out.refData( );
        apply( nBins, cd, cd );
    }
    else
    {
        out.Convert( DVector::t_float );
        float* d = (float*)out.refData( );
        apply( nBins, d, d );
    }
    // set data type and time
    fCurTime = constout.getEndTime( );
    return out;
}

//______________________________________________________________________________
void
Limiter::apply( int N, const float* in, float* out )
{
    // setup
    bool  slew = ( fType == kSlewRate ) || ( fType == kBoth );
    bool  limit = ( fType == kValues ) || ( fType == kBoth );
    float maxstep = slew ? fSlewRate / fSample : 0;
    float next;
    // loop over values
    for ( int i = 0; i < N; ++i )
    {
        next = in[ i ];
        if ( slew )
        {
            if ( fabs( next - fLast ) > maxstep )
            {
                next = next < fLast ? fLast - maxstep : fLast + maxstep;
            }
        }
        if ( limit )
        {
            if ( next < fLow )
                next = fLow;
            if ( next > fHigh )
                next = fHigh;
        }
        out[ i ] = next;
        fLast = next;
    }
}

//______________________________________________________________________________
void
Limiter::apply( int N, const fComplex* in, fComplex* out )
{
    // setup
    bool  slew = ( fType == kSlewRate ) || ( fType == kBoth );
    bool  limit = ( fType == kValues ) || ( fType == kBoth );
    float maxstep = slew ? fSlewRate / fSample : 0;
    float nextr;
    float nexti;
    // loop over values
    for ( int i = 0; i < N; ++i )
    {
        nextr = in[ i ].real( );
        nexti = in[ i ].imag( );
        if ( slew )
        {
            if ( fabs( nextr - fLastC.real( ) ) > maxstep )
            {
                nextr = nextr < fLastC.real( ) ? fLastC.real( ) - maxstep
                                               : fLastC.real( ) + maxstep;
            }
            if ( fabs( nexti - fLastC.imag( ) ) > maxstep )
            {
                nexti = nexti < fLastC.imag( ) ? fLastC.imag( ) - maxstep
                                               : fLastC.imag( ) + maxstep;
            }
        }
        if ( limit )
        {
            if ( nextr < fLow )
                nextr = fLow;
            if ( nextr > fHigh )
                nextr = fHigh;
            if ( nexti < fLow )
                nexti = fLow;
            if ( nexti > fHigh )
                nexti = fHigh;
        }
        fLastC = fComplex( nextr, nexti );
        out[ i ] = fLastC;
    }
}

//______________________________________________________________________________
void
Limiter::init(
    double fsample, limiter_type type, double l1, double l2, double l3 )
{
    fSample = fsample;
    fType = type;
    switch ( fType )
    {
    case kNoLimits:
        break;
    case kValues:
        fLow = l1;
        fHigh = l2;
        break;
    case kSlewRate:
        fSlewRate = l1;
        break;
    case kBoth:
        fLow = l1;
        fHigh = l2;
        fSlewRate = l3;
        break;
    }
    if ( fLow > fHigh )
    {
        double temp = fLow;
        fLow = fHigh;
        fHigh = temp;
    }
}

//______________________________________________________________________________
void
Limiter::reset( )
{
    fLast = 0;
    fLastC = fComplex( 0, 0 );
    // reset time
    fCurTime = Time( 0, 0 );
    fStartTime = Time( 0, 0 );
}

//______________________________________________________________________________
bool
Limiter::xfer( fComplex& tf, double f ) const
{
    tf = fComplex( 1.0 );
    return true;
}

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// limiter                                                              //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
Limiter
limiter( const char* type, double fsample, double l1, double l2, double l3 )
{
    std::string t( type ? type : "" );
    for ( std::string::iterator p = t.begin( ); p != t.end( ); ++p )
    {
        *p = tolower( *p );
    }
    if ( t == "val" )
    {
        return Limiter( fsample, Limiter::kValues, l1, l2 );
    }
    else if ( t == "sym" )
    {
        return Limiter( fsample, Limiter::kValues, -l1, l1 );
    }
    else if ( t == "slew" )
    {
        return Limiter( fsample, Limiter::kSlewRate, l1 );
    }
    else if ( t == "val/slew" )
    {
        return Limiter( fsample, Limiter::kBoth, l1, l2, l3 );
    }
    else if ( t == "sym/slew" )
    {
        return Limiter( fsample, Limiter::kBoth, -l1, l1, l2 );
    }
    else if ( t.empty( ) )
    {
        return Limiter( fsample, Limiter::kNoLimits, 0, 0 );
    }
    else
    {
        throw std::invalid_argument( "Unknown limiter type" );
    }
}

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// limiter2str                                                          //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
std::string
limiter2str( const Limiter& lim )
{
    char buf[ 1024 ];
    switch ( lim.getType( ) )
    {
    case Limiter::kNoLimits: {
        sprintf( buf, "limiter(\"\",1)" );
        break;
    }
    case Limiter::kValues: {
        if ( fabs( lim.getLow( ) + lim.getHigh( ) ) < 1E-8 )
        {
            sprintf( buf, "limiter(\"sym\",%g)", lim.getHigh( ) );
        }
        else
        {
            sprintf(
                buf, "limiter(\"val\",%g,%g)", lim.getLow( ), lim.getHigh( ) );
        }
        break;
    }
    case Limiter::kSlewRate: {
        sprintf( buf, "limiter(\"slew\",%g)", lim.getSlewRate( ) );
        break;
    }
    case Limiter::kBoth: {
        if ( fabs( lim.getLow( ) + lim.getHigh( ) ) < 1E-8 )
        {
            sprintf( buf,
                     "limiter(\"sym/slew\",%g,%g)",
                     lim.getHigh( ),
                     lim.getSlewRate( ) );
        }
        else
        {
            sprintf( buf,
                     "limiter(\"val/slew\",%g,%g,%g)",
                     lim.getLow( ),
                     lim.getHigh( ),
                     lim.getSlewRate( ) );
        }
        break;
    }
    }
    return buf;
}
