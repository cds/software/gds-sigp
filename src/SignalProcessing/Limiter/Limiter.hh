/* Version $Id$ */
#ifndef _LIGO_LIMITER_H
#define _LIGO_LIMITER_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: Limiter							*/
/*                                                         		*/
/* Module Description: Limits the time series				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 20Jul02  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: Limiter.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include "Complex.hh"
#include "Pipe.hh"
#include <string>
#include <vector>
#include <iosfwd>

/** Limiter filter. A limit is applied to the time series. Limits
    can be applied to the values themselves and/or to the slew rate.
   
    @memo Filter to limit signal values and/or slew rate.
    @author Written July 2002 by Daniel Sigg
    @version 1.0
 ************************************************************************/
class Limiter : public Pipe
{
public:
    using Pipe::apply;
    using Pipe::dataCheck;

    /// Limiter type
    enum limiter_type
    {
        /// No limits
        kNoLimits = 0,
        /// Bounds
        kValues,
        /// Slew rate
        kSlewRate,
        /// Bounds and slew rate
        kBoth
    };

    /** Constructs a limiter filter.
          @memo Default constructor.
       ******************************************************************/
    Limiter( );
    /** Constructs a limiter filter.
          @memo Constructor.
          @param lim Symmetric limit
       ******************************************************************/
    explicit Limiter( double lim );
    /** Constructs a limiter filter.
          @memo Constructor.
          @param low Low limit
          @param high High limit
       ******************************************************************/
    Limiter( double low, double high );
    /** Constructs a limiter filter. If the limiter type is kValues,
          l1 is the lower limit and l2 is the upper limit. If it is
          kSlewRate, l1 is the slew rate limit in counts/s. If it is
          kBoth, l1, l2 and l3 are the lower bound, the upper bound
          and the slew rate limit.
          @memo Constructor.
          @param fsample Sampling rate (Hz)
          @param type Limiter type
          @param l1 First limit
          @param l2 Second limit
          @param l3 Third limit
       ******************************************************************/
    Limiter( double       fsample,
             limiter_type type,
             double       l1,
             double       l2 = 0,
             double       l3 = 0 );
    /** Create an identical Multirate filter and return a pointer 
          to the new filter. The history is zeroed.
          @memo Clone a MultiRate filter.
          @return pointer to an identical filter.
       ******************************************************************/
    virtual Limiter*
    clone( ) const
    {
        return new Limiter( *this );
    }

    /** Test whether the TSeries is valid as input data for the filter.
          a runtime_error exception is thrown if the data are invalid.
          @memo Check input data validity.
       ******************************************************************/
    virtual void dataCheck( const TSeries& ts ) const;

    /** The TSeries is filtered and the result placed in a new TSeries. 
          The resulting TSeries is left on the stack and must be copied  
          to be made permanent. The filter status flag is set if there 
          are insufficient history entries (less than the Filter order) 
          or if the TSeries start time isn't contiguous with the previous 
          data. The input samples are appended to the filter history as 
          the filter response is calculated.
          @memo   Filter a Time Series.
          @param  in Time series to be filtered.
          @return A new TSeries containing the filter response to the
                  input series.
       ******************************************************************/
    virtual TSeries apply( const TSeries& in );
    /** An N-point float series is filtered from in and stored in out. 
          in may overlap out. The input samples are appended to the 
          filter history as the filter response is calculated.
          @memo   Filter a float array.
          @param  N   Number of elements in the input series.
          @param  in  Float array containing the input series.
          @param  out Float array to contain the filter response. the end  
                      of out may overlap the start of in.
          @return void
       ******************************************************************/
    void apply( int N, const float* in, float* out );
    /** An N-point complex series is filtered from in and stored in out. 
          in may overlap out. The input samples are appended to the 
          filter history as the filter response is calculated.
          @memo   Filter a float array.
          @param  N   Number of elements in the input series.
          @param  in  Float array containing the input series.
          @param  out Float array to contain the filter response. the end 
                      of out may overlap the start of in.
          @return void
       ******************************************************************/
    void apply( int N, const fComplex* in, fComplex* out );

    /** Set the filter to new values. If the limiter type is kValues,
          l1 is the lower limit and l2 is the upper limit. If it is
          kSlewRate, l1 is the slew rate limit in counts/s. If it is
          kBoth, l1, l2 and l3 are the lower bound, the upper bound
          and the slew rate limit.
          @memo Set filter to new values.
          @param fsample Smapling rate (Hz)
          @param type Limiter type
          @param l1 First limit
          @param l2 Second limit
          @param l3 Third limit
          @return void
       ******************************************************************/
    virtual void init( double       fsample,
                       limiter_type type,
                       double       l1,
                       double       l2 = 0,
                       double       l3 = 0 );
    /** The filter history buffer is cleared.
          @memo   Set filter coefficients and reset history.
       ******************************************************************/
    virtual void reset( );
    /** Tests whether the filter is in use.
          @memo   Test the filter acitivity status.
          @return true if the filter is being used.
       ******************************************************************/
    virtual bool
    inUse( void ) const
    {
        return fCurTime != Time( 0 );
    }
    /** Get the expected start time of the next TSeries to be filtered.
          @memo   Get the current time.
          @return true The current time.
       ******************************************************************/
    virtual Time
    getCurrentTime( ) const
    {
        return fCurTime;
    }
    /** Get the start time of this filter run. This is set by the first 
          filter operation after the filter has been created or reset.
          @memo   Get the start time.
          @return true The start time.
       ******************************************************************/
    virtual Time
    getStartTime( ) const
    {
        return fStartTime;
    }
    /** Get the limiter type.
          @memo Get the limiter type.
          @return limiter type.
       ******************************************************************/
    virtual limiter_type
    getType( ) const
    {
        return fType;
    }
    /** Get the sampling frequency.
          @memo Get the sampling frequency.
          @return sampling frequency.
       ******************************************************************/
    virtual double
    getSample( ) const
    {
        return fSample;
    }
    /** Get the lower bound.
          @memo Get the lower bound.
          @return lower bound.
       ******************************************************************/
    virtual double
    getLow( ) const
    {
        return fLow;
    }
    /** Get the upper bound.
          @memo Get the upper bound.
          @return lower bound.
       ******************************************************************/
    virtual double
    getHigh( ) const
    {
        return fHigh;
    }
    /** Get the slew rate limit.
          @memo Get the slew rate limit.
          @return slew rate limit.
       ******************************************************************/
    virtual double
    getSlewRate( ) const
    {
        return fSlewRate;
    }

protected:
    /** The transfer coefficient of the filter at the specified 
          frequency is calculated and returned as a complex number. 
          @memo Get a transfer coefficent of a Filter.
          @param coeff a complex number representing the Filter response 
                       at the specified frequency (return)
          @param F Frequency at which to sample the transfer function.
          @return true if successful       
       ******************************************************************/
    virtual bool xfer( fComplex& coeff, double f ) const;

private:
    ///  Design sample rate
    double fSample;
    /// Limiter type
    limiter_type fType;
    /// Lower bound
    double fLow;
    /// Upper bound
    double fHigh;
    /// Slew rate
    double fSlewRate;
    /// Last value
    float fLast;
    /// Last complex value
    fComplex fLastC;
    ///  Time of next expected filtered sample
    Time fCurTime;
    /// Time of First Sample processed since initialization of filter
    Time fStartTime;
};

/** Designer function for a limiter. The type can be "" for no
    limits, "val" for high/low limits on the sampled values,
    "sym" for a symmetric limits on the values, "slew" for a slew 
    rate limit and "val/slew" or "sym/slew" for both. 
    If the limiter type is "val", l1 is the lower limit and l2 
    is the upper limit, for "sym" l1 is the upper limit and -l1
    the lower limit, for "slew" l1 is the slew rate limit 
    in counts/s, for "val/slew" l1, l2 and l3 are the lower 
    bound, the upper bound and the slew rate limit, respectively,
    and for "sym/slew" l1 and l2 are the symmetric limit and the
    slew rate limit, respectively.

    Throws an invalid_argument exception, if an illegal argument is
    specified.

    @memo Limiter designer function
    @param type Limiter type
    @param fsample Smapling rate (Hz)
    @param l1 First limit
    @param l2 Second limit
    @param l3 Third limit
    @return limiter filter
 ************************************************************************/
Limiter limiter(
    const char* type, double fsample, double l1, double l2 = 0, double l3 = 0 );

/// returns a string describing the limiter
std::string limiter2str( const Limiter& lim );

#endif // _LIGO_LIMITER_H
