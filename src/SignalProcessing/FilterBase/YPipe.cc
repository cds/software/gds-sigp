/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "YPipe.hh"
#include "Interval.hh"
#include <stdexcept>

using namespace std;

//======================================  Constructor
YPipe::YPipe( void ) : mStartTime( 0 )
{
}

//======================================  Destructor
YPipe::~YPipe( void )
{
}

//======================================Check data
void
YPipe::dataCheck( const TSeries& x, const TSeries& y ) const
{
    if ( !inUse( ) )
        return;
    bool synchOK = !( x.empty( ) || y.empty( ) ) &&
        ( x.getStartTime( ) == mCurrentTime ) &&
        ( y.getStartTime( ) == mCurrentTime ) &&
        ( x.getInterval( ) == y.getInterval( ) ) &&
        ( x.getTStep( ) == y.getTStep( ) );
    if ( !synchOK )
    {
        throw runtime_error( "YPipe: unmatched argument series" );
    }
    if ( inUse( ) && x.getStartTime( ) != mCurrentTime )
    {
        throw runtime_error( "YPipe: Gap in input data" );
    }
}

//======================================  Get time delay
Interval
YPipe::getTimeDelay( void ) const
{
    return Interval( 0.0 );
}

//======================================  Test in use
bool
YPipe::inUse( void ) const
{
    return mStartTime != Time( 0 );
}
