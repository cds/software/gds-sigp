#include "Pipe.hh"
#include "FSeries.hh"
#include "Interval.hh"

//_____________________________________________________________________________
Interval
Pipe::getTimeDelay( void ) const
{
    return Interval( 0.0 );
}

//_____________________________________________________________________________
bool
Pipe::Xfer( fComplex& coeff, double f ) const
{
    return xfer( coeff, f );
}

//_____________________________________________________________________________
bool
Pipe::Xfer( fComplex* tf, const float* freqs, int points ) const
{
    if ( points <= 0 )
        return true;
    if ( !tf || !freqs )
        return false;
    for ( int i = 0; i < points; ++i )
    {
        if ( !xfer( tf[ i ], freqs[ i ] ) )
        {
            return false;
        }
    }
    return true;
}

//_____________________________________________________________________________
bool
Pipe::Xfer( FSeries& Fs, float Fmin, float Fmax, float dF ) const
{
    if ( Fmin < 0 )
        Fmin = 0.0;
    if ( Fmax <= 0 )
        Fmax = 1000;
    if ( Fmin >= Fmax )
    {
        return false;
    }
    if ( dF <= 0.0 )
        dF = 1.0;
    int nBins = int( ( Fmax - Fmin ) / dF + 0.5 );

    // Fill a response curve
    float*    freqs = new float[ nBins ];
    fComplex* CVec = new fComplex[ nBins ];
    for ( int j = 0; j < nBins; j++ )
    {
        freqs[ j ] = Fmin + double( j ) * dF;
    }
    bool succ = Xfer( CVec, freqs, nBins );
    if ( succ )
    {
        Fs = FSeries( Fmin, dF, Time( 0 ), nBins / ( 2 * Fmax ), nBins, CVec );
        Fs.setName( "Filter Response" );
    }
    delete[] CVec;
    delete[] freqs;
    return succ;
}
