/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef FILTER_DELAY_HH
#define FILTER_DELAY_HH

#include "autopipe.hh"

/**  The filter_delay class modifies the TSeries output of a filter pipe 
  *  cancel the time delay imposed by the filter. This only works for 
  *  filters with a well defined time-delay returned by the getTimeDelay()
  *  method. If the time-delay is not well defined, the Pipe will specify
  *  a zero time delat and the output of the filter will not be affected.
  *  @memo filter delay cancellation operator.
  *  @author John Zweizig
  *  @version $Id$ 
  */
class filter_delay : public Pipe
{
public:
    using Pipe::apply;
    using Pipe::dataCheck;

    /**  A filter_delay object is constructed from a cloned filter specified
      *  Filter_Delay constructor.
      *  @memo filter_delay constructor.
      *  @param p A pipe to be cloned.
      */
    filter_delay( const Pipe& p );

    /**  Destroy the filter delay object and its target pipe.
      */
    ~filter_delay( void );

    /** Create an identical filter and return the pointer to the cloned 
      *  object or zero if the object is unclonable..
      *  @memo Clone a filter.
      *  @return Pointer to the cloned object,
      */
    filter_delay* clone( void ) const;

    /** Process the input returning the output in an output container. 
      *  Synonymous with operator() method.
      *  @memo The basic processing method. 
      *  @param in    The TSeries being acted on.
      */
    TSeries apply( const TSeries& in );

    /**  Validate filter input. The input container is checked for 
      *  completeness, continuity, consistency and other criteria 
      *  specific to a given instance. Throws an appropriate exception 
      *  on invalid input. For an exception-less version, see isDataValid(). 
      *  @memo Validate filter input. 
      *  @param in    TSeries to validate
      */
    void dataCheck( const TSeries& in ) const;

    /** Reset the filter history.
      * @memo Reset the filter history.
      */
    void reset( void );

    /**  Test whether the filter is being used. A filter is defined as being 
      *  in use if the filter has been applied to one or more time series 
      *  since the filter was constructed or reset. The filter state generally 
      *  reflects the application of the filters but need not.
      *  @memo Tes if the filter is being used.
      *  @return true if the filter is in use.
      */
    bool inUse( void ) const;

    /**  Get the time of the first sample processed after the most recent
      *  reset.
      *  @memo Start time.
      */
    Time getStartTime( void ) const;

    /**  Get the time of the expected next sample to be processed.
      *  @memo Get the current time.
      *  @return Time of next expected sample.
      */
    Time getCurrentTime( void ) const;

    /**  Get a pointer to the target pipe. The filter_delay class retains
      *  ownership of the pipe;
      */
    const Pipe& refPipe( void ) const;
    Pipe&       refPipe( void );

    /**  Replace the target pipe with a clone of the argument pipe.
     */
    void setPipe( const Pipe& p );

    /**  Replace the target pipe with the argument pipe. The filter_delay 
      *  class takes ownership of the specified pipe, and deltes it when
      *  the target is set again or when the filter_delay object is destroyed.
      */
    void setPipe( Pipe* p );

private:
    auto_pipe mFilter;
};

//======================================  inline methods
inline const Pipe&
filter_delay::refPipe( void ) const
{
    return *mFilter;
}

inline Pipe&
filter_delay::refPipe( void )
{
    return *mFilter;
}

#endif // !defined(FILTER_DELAY_HH)
