/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "filter_delay.hh"

//======================================  filter_delay constructor
filter_delay::filter_delay( const Pipe& p ) : mFilter( p )
{
}

//======================================  filter_delay destructor
filter_delay::~filter_delay( void )
{
}

//======================================  Clone a delay filter
filter_delay*
filter_delay::clone( void ) const
{
    return new filter_delay( *this );
}

//======================================  Apply the filter to the time series
TSeries
filter_delay::apply( const TSeries& in )
{
    TSeries  f = mFilter( in );
    Interval dt = mFilter->getTimeDelay( );
    if ( dt == Interval( 0 ) )
        return f;

    Time    t0 = f.getStartTime( ) - dt;
    TSeries r( t0, f.getTStep( ), *( f.refDVect( ) ) );
    r.setF0( f.getF0( ) );
    r.setName( f.getName( ) );
    r.setSigmaW( f.getSigmaW( ) );
    r.setStatus( f.getStatus( ) );
    r.setFNyquist( f.getFNyquist( ) );
    return r;
}

//======================================  Check data is ok for filter
void
filter_delay::dataCheck( const TSeries& in ) const
{
    mFilter.dataCheck( in );
}

//======================================  Reset filter history
void
filter_delay::reset( void )
{
    mFilter.reset( );
}

//======================================  Test if filter is in use.
bool
filter_delay::inUse( void ) const
{
    if ( mFilter.null( ) )
        return false;
    return mFilter->inUse( );
}

//======================================  Get data start time
Time
filter_delay::getStartTime( void ) const
{
    if ( mFilter.null( ) )
        return Time( 0 );
    return mFilter->getStartTime( );
}

//======================================  Get Time of next data to be processed
Time
filter_delay::getCurrentTime( void ) const
{
    if ( mFilter.null( ) )
        return Time( 0 );
    return mFilter->getCurrentTime( );
}

//======================================  Set the target Pipe
void
filter_delay::setPipe( const Pipe& p )
{
    setPipe( p.clone( ) );
}

//======================================  Set the target Pipe
void
filter_delay::setPipe( Pipe* p )
{
    mFilter.set( p );
}
