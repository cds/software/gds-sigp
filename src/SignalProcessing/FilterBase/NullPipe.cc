#include "NullPipe.hh"
#include <stdexcept>

using namespace std;

//======================================  Constructor
NullPipe::NullPipe( void ) : _step( 0 )
{
}

//======================================  Apply (Null)
TSeries
NullPipe::apply( const TSeries& ts )
{
    prep( ts );
    return ts;
}

//======================================  Clone a NullPipe
NullPipe*
NullPipe::clone( void ) const
{
    return new NullPipe( *this );
}

//======================================  Check continuous data
void
NullPipe::dataCheck( const TSeries& ts ) const
{
    if ( inUse( ) )
    {
        if ( !Almost( _current, ts.getStartTime( ) ) )
        {
            cout << "NullPipe: Start time error, Current: " << _current
                 << " ts:mT0 " << ts.getStartTime( ) << endl;
            throw runtime_error( "NullPipe: TSeries start time not current" );
        }
        if ( ts.getTStep( ) != _step )
        {
            cout << "NullPipe: Step size error, Step: " << _step << " ts::mDt "
                 << ts.getTStep( ) << endl;
            throw runtime_error( "NullPipe: Inconsistent time series step" );
        }
    }
}

//======================================  Get current time
Time
NullPipe::getCurrentTime( void ) const
{
    return _current;
}

//======================================  Get start time
Time
NullPipe::getStartTime( void ) const
{
    return _start;
}

//======================================  Test whether filtering in progress
bool
NullPipe::inUse( void ) const
{
    return _start != Time( 0 );
}

//======================================  Prepare for filtering
void
NullPipe::prep( const TSeries& ts )
{
    if ( inUse( ) )
    {
        dataCheck( ts );
    }
    else
    {
        _start = ts.getStartTime( );
        _current = _start;
        if ( !_step )
            _step = ts.getTStep( );
    }
    _current = ts.getEndTime( );
}

//======================================  Reset start time.
void
NullPipe::reset( void )
{
    _start = Time( 0 );
    _current = _start;
}
