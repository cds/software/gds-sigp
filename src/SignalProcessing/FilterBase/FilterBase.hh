#ifndef FilterBASE_HH
#define FilterBASE_HH
/*
 * $Id$
 * $Log$
 * Revision 1.4  2002/07/18 05:19:02  sigg
 * added filter type enum
 *
 * Revision 1.3  2001/12/14 20:44:45  jzweizig
 * Add clone method to FilterBase and Pipe
 *
 * Revision 1.2  2001/10/07 06:05:58  sigg
 * fixed SUN C++ problems
 *
 * Revision 1.1  2001/10/01 22:56:37  jzweizig
 * Original Version
 *
 * Revision 1.2  2000/06/22 00:31:40  jzweizig
 * Remove throw clauses from all FilterBase and Pipe methods
 *
 * Revision 1.1  1999/10/27 23:31:37  jzweizig
 * New filter base class definitions
 * 
 */

//=====================================  c++17 compatibility
#ifndef NOEXCEPT
#if __cplusplus > 201100
#define NOEXCEPT noexcept
#else
#define NOEXCEPT throw( )
#endif
#endif /* NOEXCEPT */

#include "Time.hh"
#include "FilterIO.hh"

// Forward references
#ifndef __CINT__
#include <stdexcept>
#else
namespace std
{
    class runtime_error;
}
#endif

class FilterIO;
class KeyChain;
class TSeries;

/// \file FilterBase.hh

/**  \enum Filter_Type
  *  Filter type enumerator for use in design methods. Conversion to and
  *  from name strings is available from the getFilterString() and 
  *  getFilterType() functions defined in "zp2zp.hh".
  *  \brief Filter type enumerator.
  */
enum Filter_Type
{
    /// Low pass filter
    kLowPass = 0,
    /// High pass filter
    kHighPass = 1,
    /// Band pass filter
    kBandPass = 2,
    /// Band stop filter
    kBandStop = 3
};

/** Abstract base class for filters that act on (multiple) TSeries, 
 *  returning (multiple) TSeries.
 *  @memo Filter API
 *  @author Lee Samuel Finn <LSF5@PSU.Edu>
 *  @version $Id$ 
 */
class FilterBase
{
public:
    // Lifecycle

    /** Destroy a filter and release all its allocated storage.
    * @memo Filter destructor
    */
    virtual ~FilterBase( )
    {
    }

    /** Create an identical filter and return a pointer.
   *  @memo Clone the filter.
   *  @return Pointer to the cloned filter.
   */
    virtual FilterBase* clone( void ) const = 0;

    // Operators

    /** Process the input, returning result in an output container.
   *  Synonym for apply() method.
   *  @memo The basic processing method. 
   *  @param in    Container holding the TSeries being acted on.
   *  @return Reference to output FilterIO container.
   */
    virtual FilterIO& operator( )( const FilterIO& in );

    // Operations

    /** Process the input returning the output in an output container. 
   *  Synonymous with operator() method.
   *  @memo The basic processing method. 
   *  @param in    Container holding the TSeries being acted on.
   *  @return Reference to output FilterIO container.
   */
    virtual FilterIO& apply( const FilterIO& in ) = 0;

    /** Validate filter input. The input container is checked for
   *  completeness, continuity, consistency and other criteria
   *  specific to a given instance. Throws an appropriate exception on
   *  invalid input. Assumed to throw only exceptions derived from
   *  standard c++ exception class. For an exception-less version, see
   *  isDataValid(). 
   *  @memo Validate filter input.  
   *  @param in  Container holding input to validate */
    virtual void dataCheck( const FilterIO& in ) const = 0;

    /** Reset filter to a pristine state. Following application of this method
   *  object should be indistinguishable from a new instance generated from 
   *  default constructor. 
   *  @memo Reset the filter to a pristine state. 
   */
    virtual void reset( void ) = 0;

    /** Return a KeyChain for input TSeries. Use these keys to 
   *  fill a FilterIO container with TSeries, optionally validates the 
   *  container and contents with dataCheck(), and act on the input with 
   *  apply(). 
   *  @memo Return a KeyChain with Keys for input TSeries.  
   *  @note Currently unimplemented. 
   *  @return KeyChain with Keys for input TSeries.
   */
    virtual const KeyChain& getInputKeys( void ) const = 0;

    /** Return a KeyChain for output TSeries. Use the keys on this chain
   *  to retrieve output TSeries from output FilterIO container. 
   *  @memo Return a KeyChain with Keys for output TSeries.  
   *  @note Currently unimplemented. 
   *  @return  KeyChain with Keys for output TSeries.
   */
    virtual const KeyChain& getOutputKeys( void ) const = 0;

    // Access

    /** Get the moment of the first sample processed. 
   *  For a root-safe method see rootGetStartTime().
   *  @memo Get the moment of the first sample processed.
   *  @exception std::exception if the filter has not yet been used.
   *  @return Time of first sample processed after construction or reset.
   */
    virtual Time getStartTime( void ) const = 0;

    /** Get the moment of the first sample processed. Root-safe.
   *  @memo Get the moment of the first sample processed. Root-safe.
   *  @param t  start time if returned value is true. 
   *  @return false if filter has not yet been used. 
   */
    virtual bool rootGetStartTime( Time& t ) const NOEXCEPT;

    /** Get the moment of the next expected sample. Assumed to throw only 
   *  exceptions derived from standard c++ exception class. 
   *  For a root-safe method see rootGetCurrentTime().
   *  @memo Get the moment of the next expected sample.
   *  @exception std::exception if the filter has not yet been used.
   *  @return Next expected sample time.
   */
    virtual Time getCurrentTime( void ) const = 0;

    /** Get the moment of the next expected sample. Root-safe version of
   *  getCurrentTime()
   *  @memo Get the moment of the next expected sample. Root-safe. 
   *  @param t reference to Time class object to receive the current time.
   *  @return false if the filter has not yet been used. 
   */
    virtual bool rootGetCurrentTime( Time& t ) const NOEXCEPT;

    // Inquiry

    /** Validate filter input without throwing exceptions. The input 
   *  container is checked for completeness, continuity, consistency 
   *  and other criteria specific to a given instance. Does not throw 
   *  an exception, so safe for use in root. 
   *  @memo Validate filter input without throwing exceptions.
   *  @param in    Container holding the input to validate
   *  @return true if container contains valid input data.
   */
    virtual bool isDataValid( const FilterIO& in ) const;

    /** Returns true if filter is ``in use.'' A filter is ``in use'' if it 
   *  has acted on input since it was created or last reset. 
   *  \return true if filter is ``in use.'' 
   */
    virtual bool inUse( void ) const = 0;

protected:
private:
};

//--------------------------------------  Inline functions
#ifndef __CINT__
inline bool
FilterBase::rootGetStartTime( Time& t ) const NOEXCEPT
{
    bool status( true );
    try
    {
        t = getStartTime( );
    }
    catch ( std::exception& r )
    {
        status = false;
    }
    return status;
}

inline bool
FilterBase::rootGetCurrentTime( Time& t ) const NOEXCEPT
{
    bool status( true );
    try
    {
        t = getCurrentTime( );
    }
    catch ( std::exception& r )
    {
        status = false;
    }
    return status;
}

inline FilterIO&
FilterBase::operator( )( const FilterIO& in )
{
    return apply( in );
}

inline bool
FilterBase::isDataValid( const FilterIO& in ) const
{
    bool status( true );
    try
    {
        dataCheck( in );
    }
    catch ( std::exception& r )
    {
        status = false;
    }
    return status;
}
#endif

#endif // FILTERBASE_HH
