/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "YSynch.hh"
#include <stdexcept>
#include "DVector.hh"

using namespace std;

//======================================  YSynch constructor
YSynch::YSynch( const YPipe& yp, synch_mode mode ) : _mode( mode )
{
    _pipe.reset( yp.clone( ) );
}

//======================================  YSynch destructor
YSynch::~YSynch( void )
{
}

YSynch*
YSynch::clone( void ) const
{
    return new YSynch( *_pipe, _mode );
}

//======================================  Synchronize the data and apply filter.
TSeries
YSynch::apply( const TSeries& x, const TSeries& y )
{
    if ( !_pipe )
        throw logic_error( "YSynch: No YPipe specified" );
    TSeries ts_out;

    dataCheck( x, y );

    if ( !_xcurrent && !x.empty( ) )
    {
        _xcurrent = x.getStartTime( );
    }

    if ( !_ycurrent && !y.empty( ) )
    {
        _ycurrent = y.getStartTime( );
    }

    //-----------------------------------  Let the YPipe synch itself
    bool mtxy = x.empty( ) || y.empty( );
    bool synchOK = !mtxy && ( _xcurrent == _ycurrent ) &&
        ( x.getInterval( ) == y.getInterval( ) );
    if ( _mode == m_except || synchOK )
    {
        if ( !inUse( ) )
        {
            mStartTime = _xcurrent;
        }
        _xcurrent = x.getEndTime( );
        _ycurrent = y.getEndTime( );
        return _pipe->apply( x, y );
    }

    //-----------------------------------  Update queues
    int fail = 0;
    if ( !x.empty( ) )
    {
        fail = _xqueue.Append( x );
        if ( fail )
            throw runtime_error( "YSynch: x input data are not contiguous" );
        _xcurrent = _xqueue.getEndTime( );
    }

    if ( !y.empty( ) )
    {
        fail = _yqueue.Append( y );
        if ( fail )
            throw runtime_error( "YSynch: y input data are not contiguous" );
        _ycurrent = _yqueue.getEndTime( );
    }
    if ( _xqueue.empty( ) || _yqueue.empty( ) )
        return ts_out;

    //-----------------------------------  Synchronize start times.
    if ( !inUse( ) )
    {
        Interval tDiff = _xqueue.getStartTime( ) - _yqueue.getStartTime( );
        if ( _mode == m_pad )
        {
            if ( double( tDiff ) > 0.0 )
            {
                Interval dT = _xqueue.getTStep( );
                size_t   nInsert = size_t( tDiff / dT + 0.5 );
                Time tStartx = _xqueue.getStartTime( ) - dT * double( nInsert );
                if ( !Almost( tStartx, _yqueue.getStartTime( ) ) )
                {
                    throw runtime_error(
                        "YSynch: Unable to synchronize start times" );
                }
                if ( nInsert )
                {
                    DVector* dv = _xqueue.refDVect( )->clone( );
                    dv->replace_with_zeros( 0, 0, nInsert );
                    _xqueue.setData( tStartx, dT, dv );
                }
            }
            else if ( double( tDiff ) < 0.0 )
            {
                Interval dT = _yqueue.getTStep( );
                size_t   nInsert = size_t( -tDiff / dT + 0.5 );
                Time tStarty = _yqueue.getStartTime( ) - dT * double( nInsert );
                if ( !Almost( tStarty, _xqueue.getStartTime( ) ) )
                {
                    throw runtime_error(
                        "YSynch: Unable to synchronize start times" );
                }
                if ( nInsert )
                {
                    DVector* dv = _yqueue.refDVect( )->clone( );
                    dv->replace_with_zeros( 0, 0, nInsert );
                    _yqueue.setData( tStarty, dT, dv );
                }
            }
        }

        //--------------------------------  Drop unmatched data
        else if ( _mode == m_trash )
        {
            Interval tDiff = _xqueue.getStartTime( ) - _yqueue.getStartTime( );
            if ( double( tDiff ) > 0.0 )
            {
                _yqueue.eraseStart( tDiff );
            }
            else if ( double( tDiff ) != 0 )
            {
                _xqueue.eraseStart( -tDiff );
            }
        }

        //---------------------------------  Check synchronization
        if ( !Almost( _xqueue.getStartTime( ), _yqueue.getStartTime( ) ) )
        {
            cerr << "YSynch error: x{start: " << _xqueue.getStartTime( )
                 << ", dt: " << _xqueue.getTStep( ) << "}"
                 << " y{start: " << _yqueue.getStartTime( )
                 << ", dt: " << _yqueue.getTStep( ) << "}" << endl;
            throw runtime_error( "YSynch: Unable to synchronize start times" );
        }

        mStartTime = _xqueue.getStartTime( );
    }

    //-----------------------------------  Pull out fractional data.
    Interval len = _xqueue.getInterval( );
    if ( _yqueue.getInterval( ) < len )
        len = _yqueue.getInterval( );
    if ( len == Interval( 0.0 ) )
        return ts_out;
    Time tStart = _xqueue.getStartTime( );
    { // rescope tx, ty to avoid a copy.
        TSeries tx = _xqueue.extract( tStart, len );
        TSeries ty = _yqueue.extract( tStart, len );
        if ( !Almost( tx.getEndTime( ), ty.getEndTime( ) ) )
        {
            throw runtime_error( "YSynch: Data lengths don't match" );
        }
        ts_out = _pipe->apply( tx, ty );
    }
    _xqueue.eraseStart( len );
    _yqueue.eraseStart( len );
    return ts_out;
}

//======================================  Get the time delay from the filter
void
YSynch::dataCheck( const TSeries& x, const TSeries& y ) const
{
    if ( !( !_xcurrent || x.empty( ) ) && x.getStartTime( ) != _xcurrent )
    {
        throw runtime_error( "YSynch: Gap found in x input" );
    }

    if ( !( !_ycurrent || y.empty( ) ) && y.getStartTime( ) != _ycurrent )
    {
        throw runtime_error( "YSynch: Gap found in y input" );
    }

    bool synchOK = !x.empty( ) && !y.empty( ) &&
        x.getStartTime( ) == y.getStartTime( ) &&
        x.getInterval( ) == y.getInterval( );
    if ( _mode == m_except || synchOK )
    {
        _pipe->dataCheck( x, y );
    }
}

//======================================  Get the time delay from the filter
Interval
YSynch::getTimeDelay( void ) const
{
    return _pipe->getTimeDelay( );
}

//======================================  Reset the filter and synchronization
//                                        queue states
void
YSynch::reset( void )
{
    mStartTime = Time( 0 );
    _xcurrent = mStartTime;
    _ycurrent = mStartTime;
    _pipe->reset( );
    _xqueue.Clear( );
    _yqueue.Clear( );
}
