/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef AUTOPIPE_HH
#define AUTOPIPE_HH

#include "Pipe.hh"
#include <memory>

/**  auto_pipe maintains a pipe pointer and allows easy access to the 
  *  basic functionality of the Pipe class. If the Pipe class pointer 
  *  is null the pipe functions behave as a unit pipe, i.e. ts == unit(ts).
  *  The %auto_pipe class takes ownership of the pipe and deletes the
  *  pipe when the %auto_pipe is deleted.
  *  @memo Automatic pipe pointer class.
  *  @version 2.0; Last modified July 15, 2021.
  *  @author J. Zweizig
  */
class auto_pipe
{
public:
    /**  Construct a null pipe pointer.
     *  \brief Null constructor.
     */
    auto_pipe( void ) = default;

    /**  Construct an auto_pipe with a clone of the argument pipe.
     *  \brief Initialized constructor.
     *  \param p Reference to pipe to be cloned and owned.
     */
    auto_pipe( const Pipe& p );

    /**  Construct an %auto_pipe using the specified pipe pointer. The
     *  %auto_pipe takes ownership of the pipe and deletes it when 
     *  appropriate.
     *  \brief Adoptive constructor.
     *  \param p Pointer of a Pipe to be taken by the autopipe.
     */
    auto_pipe( Pipe* p );

    /**  Construct a pipe with a clone of the target pipe of the argument
     *  %auto_pipe.
     *  \note This differs from the auto_ptr class in that the argument
     *  %auto_pipe retains its target pointer while the current instance
     *  is initialized by a cloned filter.
     *  \brief Cloning  constructor.
     *  \param p Constant reference to the %auto_pipe to be copied.
     */
    auto_pipe( const auto_pipe& p );

    /**  Move syntax constructor. The pipe pointer is moved rather than
     *  being cloned. The argument auto_pipe is nullified.
     *  \brief Move syntax constructor.
     *  \param p Move syntax reference to the %auto_pipe to be copied.
     */
    auto_pipe( auto_pipe&& p ) = default;

    /**  Destroy an auto_pipe. The unique pipe handle the destroys any 
     *  referenced pipe.
     *  \brief Destructor.
     */
    virtual ~auto_pipe( void ) = default;

    /**  Copy the argument %auto_pipe by cloning the argument target %Pipe.
     *  If the current target pointer is not NULL, the target %Pipe is
     *  deleted.
     *  @memo Assignment operator.
     *  @param ap Reference to autopipe to be copied.
     *  @return Reference to to this object
     */
    auto_pipe& operator=( const auto_pipe& ap );

    /**  Move syntax assignment operator. The pipe pointer is moved rather than
     *  being cloned. The argument auto_pipe is nullified. 
     *  \brief Move syntax constructor.
     *  \param p Move syntax reference to the %auto_pipe to be copied.
     */
    auto_pipe& operator=( auto_pipe&& p ) = default;

    /**  Filter the argument time series using the referenced pipe. If the 
     *  pipe pointer is NULL the returned TSeries is identical to the 
     *  argument time series.
     *  @memo Filter a TSeries
     *  @param ts TSeries to be filtered.
     *  @return Filtered time series.
     */
    TSeries operator( )( const TSeries& ts );

    /**  Dereference the Pipe pointer.
     *  \brief Dereference operator.
     *  \return Pointer to the target %Pipe object.
     */
    Pipe* operator->( void );

    /**  Dereference the Pipe pointer.
     *  \brief Dereference operator.
     *  \return Pointer to the target %Pipe object.
     */
    Pipe& operator*( void );

    /**  Dereference a constant %auto_pipe pointer.
     *  \brief Dereference operator.
     *  \return Constant pointer to the target %Pipe object.
     */
    const Pipe* operator->( void ) const;

    /**  Dereference a constant %auto_pipe pointer.
     *  \brief Dereference operator.
     *  \return Constant reference to the target %Pipe object.
     */
    const Pipe& operator*( void ) const;

    /**  Test for not null
    */
    operator bool( void ) const;

    /**  Run the dataCheck method of the referenced Pipe on the argument 
     *  time series. No operation is performed if the pointer is null.
     *  @memo Pipe input data check.
     *  @param ts Time series to be checked.
     */
    void dataCheck( const TSeries& ts ) const;

    /**  Get the pipe pointer
     *  \brief Get the pointer.
     *  @return pointer to the pipe.
     */
    Pipe*
    get( void )
    {
        return mPipe.get( );
    }

    /**  Get the pipe pointer
     *  \brief Get the constant pointer.
     *  @return pointer to the pipe.
     */
    const Pipe*
    get( void ) const
    {
        return mPipe.get( );
    }

    /**  Run the isDataValid method of the referenced Pipe on the argument 
     *  time series. isDataValid returns true if the pointer is null.
     *  @memo Pipe input data check.
     *  @param ts Time series to be checked.
     *  \return True if data are valid
     */
    bool isDataValid( const TSeries& ts ) const;

    /**  Test for a null pipe pointer.
     *  @memo Test for null pipe.
     *  \return True if target %Pipe pointer is NULL.
     */
    bool null( void ) const;

    /**  Reset the referenced Pipe using the Pipe::reset method. If the 
     *  Pipe pointer is null no operation is performed.
     *  \brief Reset pipe. 
     */
    void reset( void );

    /**  Release the referenced pipe. The pipe pointer value is returned and 
     *  the internal pointer is set to NULL. The referenced pipe is not 
     *  deleted and will not be deleted when the %auto_pipe is destroyed.
     *  \brief Release the current %Pipe.
     *  \return Pointer to target  pipe.
     */
    Pipe* release( void );

    /**  Set the %auto_pipe pointer to a clone of the argument %Pipe. If 
     *  the %auto_pipe is currently set to a pipe the current target is
     *  deleted.
     *  \brief Set pointer to a clone of the specified %Pipe.
     *  \param p Reference to the pipeto be cloned and adopted.
     */
    void set( const Pipe& p );

    /**  Set the pipe pointer to the argument object. The %auto_pipe class
     *  acquires ownership of the argument Pipe object. If the %auto_pipe 
     *  pointer is already set to a different pointer, It deletes the 
     *  referenced %Pipe before setting the pointer to the new %Pipe. 
     *  If the argument pointer is null, the target pipe is deleted and the 
     *  pointer is set to NULL.
     *  \brief adopt a specified pipe
     *  \param p Pointer to the pipe to be adopted.
     */
    void set( Pipe* p );

private:
    std::unique_ptr< Pipe > mPipe;
};

//======================================  Inline definitions
inline void
auto_pipe::set( Pipe* p )
{
    mPipe.reset( p );
}

inline bool
auto_pipe::null( void ) const
{
    return !mPipe;
}

inline auto_pipe::auto_pipe( Pipe* p ) : mPipe( p )
{
}

inline void
auto_pipe::set( const Pipe& p )
{
    set( p.clone( ) );
}

inline auto_pipe::auto_pipe( const Pipe& p )
{
    set( p );
}

inline const Pipe&
auto_pipe::operator*( void ) const
{
    return *mPipe;
}

inline Pipe&
auto_pipe::operator*( void )
{
    return *mPipe;
}

inline auto_pipe&
auto_pipe::operator=( const auto_pipe& ap )
{
    if ( ap.null( ) )
        set( nullptr );
    else
        set( *ap );
    return *this;
}

inline auto_pipe::auto_pipe( const auto_pipe& ap )
{
    *this = ap;
}

inline TSeries
auto_pipe::operator( )( const TSeries& ts )
{
    if ( null( ) )
        return ts;
    return mPipe->operator( )( ts );
}

inline Pipe*
auto_pipe::operator->( void )
{
    return mPipe.get( );
}

inline const Pipe*
auto_pipe::operator->( void ) const
{
    return mPipe.get( );
}

inline auto_pipe::operator bool( void ) const
{
    return !null( );
}

inline void
auto_pipe::dataCheck( const TSeries& ts ) const
{
    if ( !null( ) )
        mPipe->dataCheck( ts );
}

inline bool
auto_pipe::isDataValid( const TSeries& ts ) const
{
    if ( null( ) )
        return true;
    return mPipe->isDataValid( ts );
}

inline void
auto_pipe::reset( void )
{
    if ( !null( ) )
        mPipe->reset( );
}

inline Pipe*
auto_pipe::release( void )
{
    return mPipe.release( );
}

#endif // AUTOPIPE_HH
