/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef Pipe_HH
#define Pipe_HH
/*
 * $Id$
 * $Log$
 * Revision 1.7  2004/06/16 20:17:32  jzweizig
 * Add getTimeDelay() method
 *
 * Revision 1.6  2002/07/25 04:28:13  sigg
 * fixed xfer
 *
 * Revision 1.5  2002/07/23 22:12:33  sigg
 * added Xfer
 *
 * Revision 1.4  2001/12/18 07:46:11  jzweizig
 * New baseline tracker filter
 *
 * Revision 1.3  2001/12/14 20:44:45  jzweizig
 * Add clone method to FilterBase and Pipe
 *
 * Revision 1.2  2001/10/07 06:05:58  sigg
 * fixed SUN C++ problems
 *
 * Revision 1.1  2001/10/01 22:56:37  jzweizig
 * Original Version
 *
 * Revision 1.4  2000/06/22 00:31:41  jzweizig
 * Remove throw clauses from all FilterBase and Pipe methods
 *
 * Revision 1.3  1999/11/09 00:33:50  jzweizig
 * add Mixer compilation
 *
 * Revision 1.2  1999/11/05 03:19:14  jzweizig
 * Implement FilterBase methods involving FilterIO objects
 *
 * Revision 1.1  1999/10/27 23:31:37  jzweizig
 * New filter base class definitions
 * 
 */

#include "FilterBase.hh"
#include "TSeries.hh"

/** Abstract base class for Pipes: filters that act on and return a 
 *  single TSeries.
 *  @memo Pipe API
 *  @author Lee Samuel Finn <LSF5@PSU.Edu>
 *  @version $Id$ 
 */
class Pipe : public FilterBase
{
public:
    using FilterBase::isDataValid;
    // Lifecycle

    /** Destructor
   * @memo Filter destructor
   */
    virtual ~Pipe( )
    {
    }

    /** Create an identical filter and return the pointer to the cloned 
   *  object or zero if the object is unclonable..
   *  @memo Clone a filter.
   *  @return Pointer to the cloned object,
   */
    virtual Pipe* clone( void ) const = 0;

    // Operators

    /** Process the input, returning result in an output container.
   *  Synonym for apply() method.
   *  @memo The basic processing method. 
   *  @param in    TSeries being acted on.
   *  @return Filtered TSeries.
   */
    virtual TSeries operator( )( const TSeries& in );

    /** Process the input, returning result in an output container.
   *  Synonym for apply() method.
   *  @memo The basic processing method. 
   *  @param in    TSeries being acted on.
   *  @return Reference to FilterIO containing results.
   */
    FilterIO& operator( )( const FilterIO& in );

    // Operations

    /** Process the input returning the output in an output container. 
   *  Synonymous with operator() method.
   *  @memo The basic processing method. 
   *  @param in    The TSeries being acted on.
   *  \return TSeries containing filtered data.
   */
    virtual TSeries apply( const TSeries& in ) = 0;

    /** Process the input returning the output in an output container. 
   *  Synonymous with operator() method.
   *  @memo The basic processing method. 
   *  @param in    The TSeries being acted on.
   *  \return FilterIO containing filtered time series.
   */
    FilterIO& apply( const FilterIO& in );

    /** Validate filter input. The input container is checked for 
   *  completeness, continuity, consistency and other criteria 
   *  specific to a given instance. Throws an appropriate exception 
   *  on invalid input. For an exception-less version, see isDataValid(). 
   *  @memo Validate filter input. 
   *  @param in    TSeries to validate
   */
    virtual void dataCheck( const TSeries& in ) const = 0;

    /** Validate filter input. The input container is checked for 
   *  completeness, continuity, consistency and other criteria 
   *  specific to a given instance. Throws an appropriate exception 
   *  on invalid input. For an exception-less version, see isDataValid(). 
   *  @memo Validate filter input. 
   *  @param in    TSeries to validate
   */
    void dataCheck( const FilterIO& in ) const;

    /** Validate filter input without throwing exceptions. The input 
   *  container is checked for completeness, continuity, consistency 
   *  and other criteria specific to a given instance. Does not throw 
   *  an exception, so safe for use in root. 
   *  @memo Validate filter input without throwing exceptions.
   *  @param in    TSeries to validate
   *  \return True if incorrect data are found.
   */
    virtual bool isDataValid( const TSeries& in ) const;

    /** Reset the filter history.
    * @memo Reset the filter history.
    */
    virtual void reset( void ) = 0;

    /**  Test whether the filter is being used. A filter is defined as being 
    *  in use if the filter has been applied to one or more time series 
    *  since the filter was constructed or reset. The filter state generally 
    *  reflects the application of the filters but need not.
    *  @memo Tes if the filter is being used.
    *  @return true if the filter is in use.
    */
    virtual bool inUse( void ) const = 0;

    /** Return a KeyChain for input TSeries. Use these keys to 
   *  fill a FilterIO container with TSeries, optionally validates the 
   *  container and contents with dataCheck(), and act on the input with 
   *  apply(). 
   *  @memo Return a KeyChain with Keys for input TSeries.  Currently 
   *  unimplemented. 
   *  \return Reference to input keychain.
   */
    virtual const KeyChain& getInputKeys( void ) const;

    /** Return a KeyChain for output TSeries. Use the keys on this chain
   *  to retrieve output TSeries from output FilterIO container. 
   *  @memo Return a KeyChain with Keys for output TSeries.  Currently 
   *  unimplemented. 
   *  \return Reference to output keychain.
   */
    virtual const KeyChain& getOutputKeys( void ) const;

    /**  Get the time of the first sample processed after the most recent
    *  reset.
    *  @memo Start time.
    *  \return Output keychain.
    */
    virtual Time getStartTime( void ) const = 0;

    /**  Get the time of the expected next sample to be processed.
    *  @memo Get the current time.
    *  @return Time of next expected sample.
    */
    virtual Time getCurrentTime( void ) const = 0;

    /**  Get the time delay imposed by the Filter. The time delay is only
    *  non-zero for filters that have a well defined delay i.e. FIR filters.
    *  A positive value indicates that the filter delays a signal by the 
    *  specified time.
    *  @memo Get the time delay.
    *  @return Signal time delay.
    */
    virtual Interval getTimeDelay( void ) const;

    /** The transfer coefficient of the filter at the specified 
    * frequency is calculated and returned as a complex number. 
    * Filters that support a fast way to compute a transfer coefficient 
    * should override xfer rather than this method.
    * @memo Get a transfer coefficent of a Filter.
    * @param coeff a complex number representing the Filter response 
    *              at the specified frequency (return)
    * @param f Frequency at which to sample the transfer function.
    * @return true if successful       
    */
    virtual bool Xfer( fComplex& coeff, double f ) const;

    /** The transfer function of the filter at the specified frequency 
    * points is calculated and returned as a complex array. 
    * The frequency points are user supplied. The return array
    * must be at least of length points.
    * Filters that support a fast way to compute a transfer coefficient 
    * should override xfer rather than this method.
    * @memo Get the transfer function of a Filter.
    * @param tf Transfer function (return)
    * @param freqs Frequency points
    * @param points Number of points.
    * @return true if successful       
    */
    virtual bool Xfer( fComplex* tf, const float* freqs, int points ) const;
    /** The transfer function of the filter in the specified frequency 
    * interval is calculated and returned as a complex frequency 
    * series. 
    * Filters that support a fast way to compute a transfer coefficient 
    * should override xfer rather than this method.
    * @memo   Get the transfer function of a Filter.
    * @param  Fs a complex FSeries containing the Filter response at 
    *              each frequency step.
    * @param  Fmin Minimum frequency at which to sample the 
    *              transfer function.
    * @param  Fmax Maximum frequency at which to sample the 
    *              transfer function.
    * @param  dF   Frequency step.
    * @return true if successful 
    */
    virtual bool Xfer( FSeries& Fs,
                       float    Fmin = 0.0,
                       float    Fmax = 1000.0,
                       float    dF = 1.0 ) const;

protected:
    /** The transfer coefficient of the filter at the specified 
    * frequency is calculated and returned as a complex number. 
    * Filters that support a fast way to compute a transfer coefficient 
    * should implement this method and return true.
    * @memo Get a transfer coefficent of a Filter.
    * @param coeff a complex number representing the Filter response 
    *              at the specified frequency (return)
    * @param f Frequency at which to sample the transfer function.
    * @return true if successful       
    */
    virtual bool xfer( fComplex& coeff, double f ) const;

private:
};

//--------------------------------------  Inline methods
#ifndef __CINT__

inline TSeries
Pipe::operator( )( const TSeries& in )
{
    return apply( in );
}

inline void
Pipe::dataCheck( const FilterIO& in ) const
{
    throw std::runtime_error( "not implemented" );
}

inline FilterIO&
Pipe::operator( )( const FilterIO& in )
{
    throw std::runtime_error( "not implemented" );
}

inline FilterIO&
Pipe::apply( const FilterIO& in )
{
    throw std::runtime_error( "not implemented" );
}

inline bool
Pipe::isDataValid( const TSeries& in ) const
{
    bool status( true );
    try
    {
        dataCheck( in );
    }
    catch ( std::exception& r )
    {
        status = false;
    }
    return status;
}

inline const KeyChain&
Pipe::getInputKeys( void ) const
{
    throw std::runtime_error( "not_implemented" );
}

inline const KeyChain&
Pipe::getOutputKeys( void ) const
{
    throw std::runtime_error( "not_implemented" );
}

inline bool
Pipe::xfer( fComplex& coeff, double f ) const
{
    return false;
}

#endif // def(__CINT__)

#endif //  def(Pipe_HH)
