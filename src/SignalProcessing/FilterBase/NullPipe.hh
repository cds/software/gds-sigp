/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef NULL_PIPE_HH
#define NULL_PIPE_HH

#include "Pipe.hh"
#include "TSeries.hh"

/** The Null pipe performs no filtering on its own, but it may be used to
 *  simplify construction of other pipes. It provides common start time,
 *  current time and time step recording and access mechanisms, and uses
 *  these to implement basic dataCheck, inUse and reset methods. Default
 *  (null) apply and clone methods are provided, but these are expected
 *  to be replaced by functional methods.
 */
class NullPipe : public Pipe
{
public:
    using Pipe::apply;
    using Pipe::dataCheck;

    /** Construct a NullPipe with empty start time and current time and
    *  invalid (zero) time step.
    *  \brief Default constructor.
    */
    NullPipe( void );

    /** Default destructor
    *  \brief Destructor.
    */
    virtual ~NullPipe( void ) = default;

    /** Apply a null filter, \e i.e. return the time series passed as input
    *  to the filter.
    *  \brief Apply a null filter.
    *  \param ts Input time series.
    *  \return Identical time series to the argument
    */
    virtual TSeries apply( const TSeries& ts );

    /** Construct a copy of this NullPipe instance and return a pointer to
    *  the copy.
    *  \brief Clone a NullPipe instance
    *  \return Poiner to the copy instance.
    */
    virtual NullPipe* clone( void ) const;

    /** Check that the argument series is continuous with the end of any
    *  previously processed data.
    */
    virtual void dataCheck( const TSeries& ts ) const;

    /** Get the next expected data sample time.
    *  \brief Get the current time.
    *  \return Expected start time of the next apply() argument time series.
    */
    virtual Time getCurrentTime( void ) const;

    /** Return the start time of the first apply() argument series.
    *  \brief Get start time.
    *  \return Start time of the first apply() argument series.
    */
    virtual Time getStartTime( void ) const;

    /** Test whether data have been filtered by this instance since construction
    *  or the latest reset.
    *  \brief Test whether the filter is in use.
    *  \return true if data have been filtered by this instance.
    */
    virtual bool inUse( void ) const;

    /** Reset the start time and current time. \note this resets the inUse
    *  status. The sample time is unchanged, although it will be reset when
    *  the new start time is set.
    *  \brief Reset the filter instance.
    */
    virtual void reset( void );

protected:
    /** Check that the input time series is continuous with any previously
    *  processed data and keep track of start, current times
    */
    void prep( const TSeries& ts );

protected:
    Time     _start; ///< Time of first data filtered
    Time     _current; ///< time of most recent data processed
    Interval _step; ///< Sample time increment.
};

#endif // !defined(NULL_PIPE_HH)
