/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef YPipe_HH
#define YPipe_HH
/*
 * $Id: YPipe.hh 3276 2005-01-31 01:50:59Z daniel.sigg $
 * $Log$
 * 
 */

#include "FilterBase.hh"
#include "TSeries.hh"

/**  Abstract base class for Y-Pipes: filters that act on two input series
  *  and return a single TSeries. The two input series are referred to 
  *  herein as the X input and Y input.
  *  \brief Y-Pipe API
  *  \author John Zweizig <john.zweizig@ligo.org>
  *  \version $Id: YPipe.hh 3276 2005-01-31 01:50:59Z daniel.sigg $ 
  */
class YPipe : public FilterBase
{
public:
    using FilterBase::apply;
    using FilterBase::dataCheck;
    using FilterBase::isDataValid;

    /** Constructor
    */
    YPipe( void );

    /**  Destructor
    *  \brief Filter destructor
    */
    virtual ~YPipe( void );

    /**  Create an identical filter and return the pointer to the cloned 
    *  object or zero if the object is unclonable.
    *  \brief Clone a filter.
    *  \return Pointer to the cloned object,
    */
    virtual YPipe* clone( void ) const = 0;

    // Operators

    /**  Process the input, returning result in an output container.
    *  Synonym for apply() method.
    *  \brief The basic processing method. 
    *  \param x First  X input TSeries.
    *  \param y Second Y input TSeries.
    *  \return Result TSeries.
    */
    virtual TSeries operator( )( const TSeries& x, const TSeries& y );

    /**  Process the input, returning result in an output container.
    *  Synonym for apply() method.
    *  \brief The basic processing method. 
    *  \param in Input filter I/O container.
    *  \return Output filter I/O container.
    */
    FilterIO& operator( )( const FilterIO& in );

    // Operations

    /**  Process the input returning the output in an output container. 
    *  Synonymous with operator() method.
    *  \brief The basic processing method. 
    *  \param x X input TSeries.
    *  \param y Y input TSeries.
    *  \return Result TSeries
    */
    virtual TSeries apply( const TSeries& x, const TSeries& y ) = 0;

    /**  Process the input returning the output in an output container. 
    *  Synonymous with operator() method.
    *  \brief The basic processing method. 
    *  \param in  Input filter I/O container.
    *  \return  Output filter I/O container.
    */
    virtual FilterIO& apply( const FilterIO& in );

    /** Validate filter input. The input container is checked for 
    *  completeness, continuity, consistency and other criteria 
    *  specific to a given instance. Throws an appropriate exception 
    *  on invalid input. For an exception-less version, see isDataValid(). 
    *  \brief Validate filter input. 
    *  \param x X input TSeries to validate
    *  \param y Y input TSeries to validate
    */
    virtual void dataCheck( const TSeries& x, const TSeries& y ) const = 0;

    /**  The input container is checked for completeness, continuity, 
    *  consistency and other criteria specific to a given instance.
    *  \exception runtime_error exception is thrown on invalid input. 
    *  \note For an exception-less version, see isDataValid(). 
    *  \brief Validate filter input. 
    *  \param in Input container to validate
    */
    void dataCheck( const FilterIO& in ) const;

    /**  Validate filter input without throwing exceptions. The input time 
    *  series are checked for completeness, continuity, consistency 
    *  and other criteria specific to a given instance. Does not throw 
    *  an exception, so safe for use in root. 
    *  \brief Validate filter input without throwing exceptions.
    *  \param x X input TSeries to validate.
    *  \param y Y input TSeries to validate.
    *  \return True if data is not valid.
    */
    virtual bool isDataValid( const TSeries& x, const TSeries& y ) const;

    /** Reset the filter history.
    * \brief Reset the filter history.
    */
    virtual void reset( void ) = 0;

    /**  Test whether the filter is being used. A filter is defined as being 
    *  in use if the filter has been applied to one or more time series 
    *  since the filter was constructed or reset. The filter state generally 
    *  reflects the application of the filters but need not.
    *  \brief Test if the filter is being used.
    *  \return true if the filter is in use.
    */
    virtual bool inUse( void ) const;

    /**  Return a KeyChain for input TSeries. Use these keys to 
    *  fill a FilterIO container with TSeries, optionally validates the 
    *  container and contents with dataCheck(), and act on the input with 
    *  apply(). 
    *  \note Currently unimplemented.
    *  \brief Input keychain reference.
    *  \return Reference to a KeyChain with Keys for input TSeries.
    */
    virtual const KeyChain& getInputKeys( void ) const;

    /**  Return a KeyChain for output TSeries. Use the keys on this chain
    *  to retrieve output TSeries from output FilterIO container. 
    *  \brief Return a KeyChain with Keys for output TSeries.  Currently 
    *  unimplemented. 
    *  \return Reference to the output key chain.
    */
    virtual const KeyChain& getOutputKeys( void ) const;

    /**  Get the time of the first sample processed after the most recent
    *  reset.
    *  \brief Start time.
    *  \return Time stamp of first sample processed.
    */
    virtual Time getStartTime( void ) const;

    /**  Get the time of the expected next sample to be processed.
    *  \brief Get the current time.
    *  \return Time of next expected sample.
    */
    virtual Time getCurrentTime( void ) const;

    /**  Get the time delay imposed by the Filter. The time delay is only
    *  non-zero for filters that have a well defined delay i.e. FIR filters.
    *  A positive value indicates that the filter delays a signal by the 
    *  specified time.
    *  \brief Get the time delay.
    *  \return Signal time delay.
    */
    virtual Interval getTimeDelay( void ) const;

protected:
    Time     mStartTime; ///< Start time.
    Time     mCurrentTime; ///< Current time.
    Interval mSample; ///< Sample time
};

//--------------------------------------  Inline methods
#ifndef __CINT__

inline TSeries
YPipe::operator( )( const TSeries& x, const TSeries& y )
{
    return apply( x, y );
}

inline void
YPipe::dataCheck( const FilterIO& in ) const
{
    throw std::runtime_error( "not implemented" );
}

inline FilterIO&
YPipe::operator( )( const FilterIO& in )
{
    throw std::runtime_error( "not implemented" );
}

inline FilterIO&
YPipe::apply( const FilterIO& in )
{
    throw std::runtime_error( "not implemented" );
}

inline bool
YPipe::isDataValid( const TSeries& x, const TSeries& y ) const
{
    bool status( true );
    try
    {
        dataCheck( x, y );
    }
    catch ( std::exception& r )
    {
        status = false;
    }
    return status;
}

inline const KeyChain&
YPipe::getInputKeys( void ) const
{
    throw std::runtime_error( "not_implemented" );
}

inline const KeyChain&
YPipe::getOutputKeys( void ) const
{
    throw std::runtime_error( "not_implemented" );
}

inline Time
YPipe::getCurrentTime( void ) const
{
    return mCurrentTime;
}

inline Time
YPipe::getStartTime( void ) const
{
    return mStartTime;
}

#endif // def(__CINT__)

#endif //  def(YPipe_HH)
