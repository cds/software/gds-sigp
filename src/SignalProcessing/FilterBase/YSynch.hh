/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef YSynch_HH
#define YSynch_HH
/*
 * $Id: YSynch.hh 3276 2005-01-31 01:50:59Z daniel.sigg $
 * $Log$
 * 
 */

#include "YPipe.hh"
#include <memory>

/**  %YSynch is a Y-pipe that passes synchronized data to a referenced 
  *  YPipe filter. The synchronization can take several forms:
  *   - m_except: Throws an exception if the inputs are not synchronized.
  *   - m_buffer: buffer data to synchronize arrival times. throw exception if
  *               data are not synchronous.
  *   - m_pad: Pad the start of the later time series with zeroes to make
  *            the start times identical.
  *   - m_drop: drop samples of the earlier pipeline.
  *  %YSynch can not synchronize pipes unless
  *  \brief  Class to synchronize Y-Pipe filters.
  *  \author John Zweizig <john.zweizig@ligo.org>
  *  \version $Id: YSynch.hh 3276 2005-01-31 01:50:59Z daniel.sigg $ 
  */
class YSynch : public YPipe
{
public:
    /**  Synchronization mode enumerator specifies the rules for buffering 
     *  and constraints on the input series time segments.
     */
    enum synch_mode
    {
        m_except, ///< throw an exception if start times or data interval differ.
        m_buffer, ///< buffer inputs. Throw an exception if buffered data start
        ///< times differ overlapping data length is passed to filter.
        m_pad, ///< Pad the start of later time series
        m_trash ///< throw away data to align input series.
    };

public:
    /** Constructor
     */
    YSynch( const YPipe& yp, synch_mode = m_pad );

    /**  Destructor
     *  \brief Filter destructor
     */
    virtual ~YSynch( void );

    /**  Create an identical filter and return the pointer to the cloned 
     *  object or zero if the object is unclonable.
     *  \brief Clone a filter.
     *  \return Pointer to the cloned object,
     */
    virtual YSynch* clone( void ) const;

    // Operators

    /**  Process the input, returning result in an output container.
     *  Synonym for apply() method.
     *  \brief The basic processing method. 
     *  \param x First  X input TSeries.
     *  \param y Second Y input TSeries.
     *  \return Result TSeries.
     */
    virtual TSeries operator( )( const TSeries& x, const TSeries& y );

    // Operations

    /**  Process the input returning the output in an output container. 
     *  Synonymous with operator() method.
     *  \brief The basic processing method. 
     *  \param x X input TSeries.
     *  \param y Y input TSeries.
     *  \return Result TSeries
     */
    virtual TSeries apply( const TSeries& x, const TSeries& y );

    /** Validate filter input. The input container is checked for 
     *  completeness, continuity, consistency and other criteria 
     *  specific to a given instance. Throws an appropriate exception 
     *  on invalid input. For an exception-less version, see isDataValid(). 
     *  \brief Validate filter input. 
     *  \param x X input TSeries to validate
     *  \param y Y input TSeries to validate
     */
    virtual void dataCheck( const TSeries& x, const TSeries& y ) const;

    /**  Validate filter input without throwing exceptions. The input time 
     *  series are checked for completeness, continuity, consistency 
     *  and other criteria specific to a given instance. Does not throw 
     *  an exception, so safe for use in root. 
     *  \brief Validate filter input without throwing exceptions.
     *  \param x X input TSeries to validate.
     *  \param y Y input TSeries to validate.
     *  \return True if data is not valid.
     */
    virtual bool isDataValid( const TSeries& x, const TSeries& y ) const;

    /** Reset the filter history.
     * \brief Reset the filter history.
     */
    virtual void reset( void );

    /**  Return a KeyChain for input TSeries. Use these keys to 
     *  fill a FilterIO container with TSeries, optionally validates the 
     *  container and contents with dataCheck(), and act on the input with 
     *  apply(). 
     *  \note Currently unimplemented.
     *  \brief Input keychain reference.
     *  \return Reference to a KeyChain with Keys for input TSeries.
     */
    virtual const KeyChain& getInputKeys( void ) const;

    /**  Return a KeyChain for output TSeries. Use the keys on this chain
     *  to retrieve output TSeries from output FilterIO container. 
     *  \brief Return a KeyChain with Keys for output TSeries.  Currently 
     *  unimplemented. 
     *  \return Reference to the output key chain.
     */
    virtual const KeyChain& getOutputKeys( void ) const;

    /**  Get the time of the first sample processed after the most recent
     *  reset.
     *  \brief Start time.
     *  \return Time stamp of first sample processed.
     */
    virtual Time getStartTime( void ) const;

    /**  Get the time of the expected next sample to be processed.
     *  \brief Get the current time.
     *  \return Time of next expected sample.
     */
    virtual Time getCurrentTime( void ) const;

    /**  Get the time delay imposed by the Filter. The time delay is only
     *  non-zero for filters that have a well defined delay i.e. FIR filters.
     *  A positive value indicates that the filter delays a signal by the 
     *  specified time.
     *  \brief Get the time delay.
     *  \return Signal time delay.
     */
    virtual Interval getTimeDelay( void ) const;

protected:
    synch_mode               _mode;
    std::unique_ptr< YPipe > _pipe;
    TSeries                  _xqueue;
    TSeries                  _yqueue;
    Time                     _xcurrent;
    Time                     _ycurrent;
};

//--------------------------------------  Inline methods
inline TSeries
YSynch::operator( )( const TSeries& x, const TSeries& y )
{
    return apply( x, y );
}

inline bool
YSynch::isDataValid( const TSeries& x, const TSeries& y ) const
{
    bool status( true );
    try
    {
        dataCheck( x, y );
    }
    catch ( std::exception& r )
    {
        status = false;
    }
    return status;
}

inline Time
YSynch::getCurrentTime( void ) const
{
    if ( !_xcurrent || !_ycurrent )
    {
        return Time( 0 );
    }
    else if ( _xcurrent < _ycurrent )
    {
        return _xcurrent;
    }
    else
    {
        return _ycurrent;
    }
}

inline const KeyChain&
YSynch::getInputKeys( void ) const
{
    throw std::runtime_error( "not_implemented" );
}

inline const KeyChain&
YSynch::getOutputKeys( void ) const
{
    throw std::runtime_error( "not_implemented" );
}

inline Time
YSynch::getStartTime( void ) const
{
    return mStartTime;
}

#endif //  def(YSynch_HH)
