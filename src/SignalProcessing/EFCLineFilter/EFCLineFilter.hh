/************************************************************/
/*                                                          */
/* Module Name: EFCLineFilter                               */
/*                                                          */
/* Module Description: Wrapper class for linewatch module   */
/*                     in dttalgo subdirectory.             */
/*                                                          */
/* Ed Daw, 12th November 2008                               */
/* e.daw@shef.ac.uk                                         */
/* +44 114 222 4353                                         */
/*                                                          */
/* Comments: This header file should generate its own       */
/* documentation on typing doc++ < header file name >       */
/* See gds tree build instructions on how to download doc++ */
/*                                                          */
/* Further comments: A paper summarizing the operation of   */
/* this algorithm is available from arXiv:0808.2573 [gr-qc] */
/*                                                          */
/************************************************************/

#ifndef EFCLineFilter_HH
#define EFCLineFilter_HH

#include "Pipe.hh"
#include "linewatch.h"
#include "TSeries.hh"
#include "Time.hh"
#include "Interval.hh"
#include "FSeries.hh"

class FSeries;

/** @name EFCLineFilter
    @memo These algorithms inherit the base class Pipe, and allow
          the construction of an acausal line monitoring and removal
          code based on the EFC technique. The theory behind this
          technique is described at \URL{http://arxiv.org/pdf/0808.2573}.
          The class is a wrapper for the c implementation of EFC contained
          in the linewatch.c and linewatch.h code under the dttalgo
	  subdirectory of the SignalProcessing directory. Documentation
          for the linewatch C code can be found at
          \URL{http://www.hep.shef.ac.uk/research/lsc/restricted/rtx/doc/linewatch}, or by typing doc++ -dir <destination directory> linewatch.h in the appropriate includes directory.
    @author Ed Daw, 12th November 2008
************************************************************************/

class EFCLineFilter : public Pipe
{

public:
    // using statement used to specify which declaration in the
    // inheritance tree EFCLineFilter::apply should inherit from.
    // There is one declaration in FilterBase and one in Pipe.
    using Pipe::apply;
    using Pipe::dataCheck;

    /** Default constructor. Creates an empty linewatch structure.
    * @memo Default constructor.
    */
    EFCLineFilter( void );

    /** Constructor from arrays of line frequencies and their widths.
    * @param sampling_rate The sampling rate in Hz.
    * @param responsetime The target response time to changes in
    *        the line parameters, in seconds.
    * @param nfreq The number of lines.
    * @param freqlist A pointer to the first of an array of line
    *        frequencies in Hz.
    * @param widthlist A pointer to the first of an array of line
    *        widths in Hz.
    * @memo Constructor from line properties
    */
    EFCLineFilter( double  sampling_rate,
                   double  responsetime,
                   int     nfreq,
                   double* freqlist,
                   double* widthlist );

    /** Default destructor.
    * Frees allocated memory for internal storage of the filter state,
    * resets the number of lines to zero and the internal flag 
    * indicating that the filter is in use to false.
    * @memo Default destructor.
    */
    ~EFCLineFilter( void );

    /** Apply the filter to a timeseries
    * @param data_in The timeseries to be filtered.
    * @return Filtered timeseries.
    */
    TSeries apply( const TSeries& data_in );

    /** Assign filter characteristics from one EFCLineFilter
    * to another. Only works on filters that have not yet been
    * initialized. For initialized filters the filter properties
    * are not changed. For uninitialized filters, all filter 
    * properties are duplicated to the target filter, except for
    * the timeseries memory buffer, which is dynamically allocated
    * as a new separate buffer.
    */
    EFCLineFilter& operator=( const EFCLineFilter& f );

    /** Copy constructor
    * Equivalent functionality to the assignment operator.
    * @return The copied filter.
    */
    EFCLineFilter( const EFCLineFilter& model );

    /** Clone the filter. Equivalent functionality to the copy
    * constructor.
    * @return The copied filter.
    */
    EFCLineFilter* clone( void ) const;

    /** Cneck the data. Inherited from Pipe. Not implemented.
    */
    void dataCheck( const TSeries& data_in ) const;

    /** Reset the filter by deallocating all internally stored memory
    * buffers, setting the number of lines to zero and the internal
    * data flag to false.
    */
    void reset( void );

    /** Check for filter initialization
    * @return true (filter initialized) or false (filter uninitialized).
    */
    bool inUse( void ) const;
    /** Get the timestamp on the first sample of data fed to the filter.
    * @return Start time.
    */
    Time getStartTime( void ) const;

    /** Get the timestamp on the last sample of data fed to the filter.
    * @return End time.
    */
    Time getCurrentTime( void ) const;

    /** Get the group delay introduced by the filter. This is time delay
    * between the ingestion of a data point and the availability of a 
    * subtraction element to remove the line content at that point.
    * @return Group delay time interval.
    */
    Interval getTimeDelay( void ) const;

private:
    bool         _inuse;
    int          _nlines;
    linedata*    _ld;
    double       _sampling_period;
    unsigned int _tsbuffersize;
    double*      _ptsbuffer;
    bool         _first_data_processed;
    Time         _time_first_used;
    Time         _time_last_used;
};

#endif // EFCLineFilter_HH
