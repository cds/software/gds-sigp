// EFC line filter declarations
//
// Ed Daw, 17th September 2008

// standard C++ includes
#include <iostream>
// dmt includes
#include "TSeries.hh"
#include "Time.hh"
#include "Interval.hh"
#include "DVector.hh"
#include "EFCLineFilter.hh"
// dmt linewatch includes
#include "linewatch.h"

// default constructor
EFCLineFilter::EFCLineFilter( void ) : _inuse( false ), _nlines( 0 ), _ld( 0x0 )
{
}

// constructor from line parameters
EFCLineFilter::EFCLineFilter( double  sampling_rate,
                              double  responsetime,
                              int     nfreq,
                              double* freqlist,
                              double* widthlist )
    : _inuse( false ), _nlines( 0 ), _ld( 0x0 )
{

    // calculate the number of elements of a timeseries buffer sufficient
    // to give at least the requested response time.
    _tsbuffersize = linewatch_gettsbuffersize( sampling_rate, responsetime );
    // allocate memory for timeseries buffer
    _ptsbuffer = new double[ _tsbuffersize ];
    // store sampling period
    _sampling_period = 1 / sampling_rate;
    // allocate memory for line data structures
    _nlines = nfreq;
    _ld = new linedata[ _nlines ];
    // fill line data structures
    for ( int linecount = 0; linecount < _nlines; ++linecount )
    {
        linewatch_constructor( _ld + linecount,
                               freqlist[ linecount ],
                               widthlist[ linecount ],
                               sampling_rate,
                               responsetime,
                               _ptsbuffer );
    }
    _inuse = true;
}

// destructor
EFCLineFilter::~EFCLineFilter( void )
{
    for ( int linecount = 0; linecount < _nlines; ++linecount )
    {
        linewatch_destructor( _ld + linecount );
    }
    delete[] _ld;
    delete[] _ptsbuffer;
    _nlines = 0;
    _inuse = false;
}

// function that returns true if filter initialized
bool
EFCLineFilter::inUse( void ) const
{
    return _inuse;
}

// apply the filter
TSeries
EFCLineFilter::apply( const TSeries& tsin )
{
    if ( _inuse && !tsin.isEmpty( ) )
    {
        // if this is the first data, update _first_data_processed flag
        // and _time_first_used flag
        if ( !_first_data_processed )
        {
            _time_first_used = tsin.getStartTime( );
            _first_data_processed = true;
        }
        _time_last_used = tsin.getEndTime( );
        // process data
        // copy input data into a buffer
        TSeries databuffer( tsin );
        // extract number of samples from timeseries
        size_t nsamples( databuffer.getNSample( ) );
        // convert data to double precision if necessary
        if ( databuffer.refDVect( )->getType( ) == DVector::t_float )
        {
            databuffer.Convert( DVector::t_double );
        }
        // filter the array of data with each line remover
        double* data_array = (double*)databuffer.refData( );
        // filter the data
        double  real;
        double  imag;
        double  correction;
        double* correctionelement;
        for ( int dc = 0; dc < (int)nsamples; ++dc )
        {
            correction = 0;
            double indata = data_array[ dc ];
            for ( int lc = 0; lc < _nlines; ++lc )
            {
                correction += linewatch_increment( _ld + lc,
                                                   data_array[ dc ],
                                                   &real,
                                                   &imag,
                                                   &correctionelement );
            }
            data_array[ dc ] = ( *correctionelement ) - correction;
            /*data_array[dc]=correction;*/
            for ( int lc = 0; lc < _nlines; ++lc )
            {
                linewatch_nextsample( _ld + lc, indata );
            }
        }
        return databuffer;
    }
    else
    {
        return tsin;
    }
}

// assignment operator
EFCLineFilter&
EFCLineFilter::operator=( const EFCLineFilter& f )
{
    if ( this != &f )
    {
        _inuse = f._inuse;
        _ld = new linedata[ f._nlines ];
        _nlines = f._nlines;
        _ptsbuffer = new double[ _tsbuffersize ];
        _tsbuffersize = f._tsbuffersize;
        // copy contents of timeseries buffers from
        // f to this.
        for ( int lc = 0; lc < f._nlines; ++lc )
        {
            linewatch_duplicate( _ptsbuffer, _ld + lc, f._ld + lc );
        }
    }
    return *this;
}

// copy constructor
EFCLineFilter::EFCLineFilter( const EFCLineFilter& f )
{
    *this = f;
}

EFCLineFilter*
EFCLineFilter::clone( ) const
{
    return new EFCLineFilter( *this );
}

void
EFCLineFilter::dataCheck( const TSeries& data_in ) const
{
    std::cerr << "Not implemented." << std::endl;
    return;
}

void
EFCLineFilter::reset( )
{
    if ( _inuse )
    {
        for ( int lc = 0; lc < _nlines; ++lc )
        {
            linewatch_destructor( _ld + lc );
        }
        delete[] _ptsbuffer;
        _nlines = 0;
        _tsbuffersize = 0;
        _inuse = false;
    }
    return;
}

Time
EFCLineFilter::getStartTime( void ) const
{
    if ( !_inuse )
    {
        std::cerr
            << "WARNING: request for first used time on uninitialized filter"
            << std::endl;
    }
    return _time_first_used;
}

Time
EFCLineFilter::getCurrentTime( void ) const
{
    if ( !_inuse )
    {
        std::cerr
            << "WARNING: request for last data time on uninitialized filter"
            << std::endl;
    }
    return _time_last_used;
}

Interval
EFCLineFilter::getTimeDelay( void ) const
{
    // time delay is half the number of samples in the
    // timeseries buffer
    Interval time_delay( _sampling_period * _tsbuffersize );
    return time_delay;
}
