/* version $Id$ */
#ifndef IIR_DESIGN_HH
#define IIR_DESIGN_HH

#include "IIRFilter.hh"

/** \ingroup sigp_iir_design
  * \{
  */

/** Design a Butterworth filter of the specified type and order.
  * @memo Make a Butterworth filter.
  * @exception std::invalid_argument \c fs<=0.
  * @exception std::runtime_error filter construction fails.
  * @param type Filter type (Filter_Type enumerator)
  * @param order Filter order.
  * @param fs Sampling frequency (Hz).
  * @param f1 Pass band edge (Hz).
  * @param f2 Another pass band edge (Hz).
  * @param prewarp True for prewarping frequencies.
  * @return Designed IIRFilter
  */
IIRFilter butter( Filter_Type type,
                  int         order,
                  double      fs,
                  double      f1,
                  double      f2 = 0.0,
                  bool        prewarp = true );

/** Design a type 1 Chebyshev filter with a specified order and maximum
  * pass-band ripple.
  * @memo Make an Chebyshev filter of type 1.
  * @exception std::invalid_argument \c fs<=0.
  * @exception std::runtime_error filter construction fails.
  * @param type Filter type (/sa Filter_Type)
  * @param order Filter order.
  * @param rp Pass-band ripple in dB.
  * @param fs Sampling frequency (Hz).
  * @param f1 Pass band edge (Hz).
  * @param f2 Another pass band edge (Hz).
  * @param prewarp True for prewarping frequencies.
  * @return Designed IIRFilter
  */
IIRFilter cheby1( Filter_Type type,
                  int         order,
                  double      rp,
                  double      fs,
                  double      f1,
                  double      f2 = 0.0,
                  bool        prewarp = true );

/** Design a type 2 Chebyshev filter (constant stop-band ripple) with a 
  * specified order and stop-band ripple.
  * @memo Make a type 2 Chebyshev filter.
  * @exception std::invalid_argument \c fs<=0.
  * @exception std::runtime_error filter construction fails.
  * @param type Filter type (Filter_Type enumerator)
  * @param order Filter order.
  * @param rs Stop-band ripple (in dB).
  * @param fs Sampling frequency (Hz).
  * @param f1 Pass band edge (Hz).
  * @param f2 Another pass band edge (Hz).
  * @param prewarp True for prewarping frequencies.
  * @return Designed Chebyshev type-2 IIRFilter
  */
IIRFilter cheby2( Filter_Type type,
                  int         order,
                  double      rs,
                  double      fs,
                  double      f1,
                  double      f2 = 0.0,
                  bool        prewarp = true );

/** Make an elliptic filter.
  * @memo Make an elliptic filter.
  * @exception std::invalid_argument \c fs<=0.
  * @exception std::runtime_error filter construction fails.
  * @param type Filter type (Filter_Type enumerator)
  * @param order Filter order.
  * @param rp Pass-band ripple in dB.
  * @param as Stop-band attenuation in dB.
  * @param fs Sampling frequency (Hz).
  * @param f1 Pass band edge (Hz).
  * @param f2 Another pass band edge (Hz).
  * @param prewarp True for prewarping frequencies.
  * @return Designed elliptic IIRFilter.
  */
IIRFilter ellip( Filter_Type type,
                 int         order,
                 double      rp,
                 double      as,
                 double      fs,
                 double      f1,
                 double      f2 = 0.0,
                 bool        prewarp = true );

/** Make a notch filter.
  * Throws a runtime_error or invalid_argument exception, if an illegal 
  * argument is specified.
  * @memo Make a notch filter.
  * @exception std::invalid_argument \c fs<=0.
  * @exception std::runtime_error filter construction fails.
  * @param fs Sampling frequency
  * @param f0 Center frequency.
  * @param Q Quality factor (\f$ Q = f_0 / <width> \f$ ).
  * @param depth Depth (dB down) of the notch (dB).
  * @param prewarp True for prewarping frequencies.
  * @return IIRFilter
  */
IIRFilter notch(
    double fs, double f0, double Q, double depth = 0.0, bool prewarp = true );

/** Make a resonant gain filter.
  * Throws a runtime_error or invalid_argument exception, if an illegal 
  * argument is specified.
  * @memo Make a resonant gain filter.
  * @exception std::invalid_argument \c fs<=0.
  * @exception std::runtime_error filter construction fails.
  * @param fs Sampling frequency
  * @param f0 Center frequency.
  * @param Q Quality factor (\f$ Q = f_0 / <width> \f$ ).
  * @param height Height (dB up) of the peak (dB).
  * @param prewarp True for prewarping frequencies.
  * @return IIRFilter
  */
IIRFilter resgain(
    double fs, double f0, double Q, double height = 30.0, bool prewarp = true );

/** Design a comb filter.
  * @memo Make a comb filter.
  * @exception std::invalid_argument \c fs<=0.
  * @exception std::runtime_error filter construction fails.
  * @param fs Sampling frequency
  * @param f0 Fundamental frequency.
  * @param Q Quality factor (\f$ Q = f_0 / <width> \f$ ).
  * @param amp Depth/height of notches/peaks (dB). 
  * @param N Number of harmonics.
  * @param prewarp True for prewarping frequencies.
  * @return Designed comb filter
  */
IIRFilter comb( double fs,
                double f0,
                double Q,
                double amp = 0.0,
                int    N = 0,
                bool   prewarp = true );

/** Make a simple pole.
  * Poles and zero can be specified in the s-plane using units of rad/s (set 
  * plane to "s") or units of Hz ("f"). In both cases the frequency 
  * is expected to be POSITIVE. Alternately, one can also specify normalized
  * poles and zeros ("n"). Normalized poles and zeros are expected to have
  * POSITIVE frequencies and their respective low and high frequency gains 
  * are set to 1---unless they are located a 0Hz. The default location is 
  * the s-plane.
  * For the s-plane the formula is ["s"]: \f[ pole(f_0) = \frac{1}{s + w_0}\f]
  * or ["f"]: \f[ pole(f_0) = \frac{1}{s + 2 \pi f_0}. \f]
  * For a normalized pole ["n"] the formulae are:
  * \f[ pole(f_0) = \frac{1}{1 + i f/f_0}; \, f_0 > 0\f] 
  * and 
  * \f[ pole(f_0) = \frac{1}{i f};  f_0 = \, 0\f].
  *
  * Throws a runtime_error or invalid_argument exception, if an illegal 
  * argument is specified.
  *
  * @memo Make a simple pole
  * @exception std::invalid_argument Invalid plane.
  * @param fs Sampling frequency
  * @param f0 Pole frequency.
  * @param gain Gain.
  * @param plane location where poles/zeros are specified
  * @param prewarp True for prewarping frequencies.
  * @return IIRFilter
  */
IIRFilter pole( double      fs,
                double      f0,
                double      gain = 1.0,
                const char* plane = "s",
                bool        prewarp = true );

/** Poles and zero can be specified in the s-plane using units of rad/s (set 
  * plane to "s") or units of Hz ("f"). In both cases the frequency 
  * is expected to be POSITIVE. Alternately, one can also specify normalized
  * poles and zeros ("n"). Normalized poles and zeros are expected to have
  * POSITIVE frequencies and their respective low and high frequency gains 
  * are set to 1---unless they are located a 0Hz. The default location is 
  * the s-plane.
  * For the s-plane the formula is ["s"]: \f[zero(f_0) = (s + w_0)\f] or 
  * ["f"]: \f[ zero(f_0) = (s + 2 \pi f_0). \f] For a normalized zero ["n"]
  * the formula is \f[ zero(f_0) = (1 + i f/f_0)\f]; \f$f_0 > 0\f$ and 
  * \f$zero(f0) = (i f); \, f_0 = 0\f$.
  *
  * @memo Make a simple zero
  * @exception std::invalid_argument Invalid plane.
  * @param fs Sampling frequency
  * @param f0 Zero frequency.
  * @param gain gain.
  * @param plane location where poles/zeros are specified
  * @param prewarp True for prewarping frequencies.
  * @return IIRFilter
  */
IIRFilter zero( double      fs,
                double      f0,
                double      gain = 1.0,
                const char* plane = "s",
                bool        prewarp = true );

/** Make a complex pole pair. 
  * Poles and zero can be specified in the s-plane using units of rad/s (set 
  * plane to "s") or units of Hz ("f"). In both cases the frequency 
  * is expected to be POSITIVE. Alternately, one can also specify normalized
  * poles and zeros ("n"). Normalized poles and zeros are expected to have
  * POSITIVE frequencies and their respective low and high frequency gains 
  * are set to 1---unless they are located a 0Hz. The default location is 
  * the s-plane.
  * For the s-plane the formula is ["s"]:
  * \f[ pole2(f_0, Q) = \frac{1}{s^2 + w_0 s / Q + w_0^2} \f] 
  * or ["f"]: 
  * \f[ pole2(f_0, Q) = \frac{1}{s^2 + 2 \pi f_0 s / Q + (2 \pi f_0)^2}. \f]
  * For  a normalized complex pole pair ["n"] the  formula is 
  * \f[ pole2(f_0) = \frac{1}{1 + i f/f_0 + (f/f_0)^2}\f]
  * with \f$f_0 > 0\f$.
  *
  * @memo Make a complex pole pair
  * @exception std::invalid_argument Invalid plane.
  * @param fs Sampling frequency
  * @param f0 Pole frequency.
  * @param Q quality factor
  * @param gain gain
  * @param plane location where poles/zeros are specified
  * @param prewarp True for prewarping frequencies.
  * @return IIRFilter
  */
IIRFilter pole2( double      fs,
                 double      f0,
                 double      Q,
                 double      gain = 1.0,
                 const char* plane = "s",
                 bool        prewarp = true );

/** Poles and zero can be specified in the s-plane using units of rad/s (set 
  * plane to "s") or units of Hz ("f"). In both cases the frequency 
  * is expected to be POSITIVE. Alternately, one can also specify normalized
  * poles and zeros ("n"). Normalized poles and zeros are expected to have
  * POSITIVE frequencies and their respective low and high frequency gains 
  * are set to 1---unless they are located a 0Hz. The default location is 
  * the s-plane.
  * For the s-plane the formula is ["s"]:
  * \f[zero2(f_0,Q)=(s^2 + w_0 s / Q + w_0^2)\f] 
  * or ["f"]: 
  * \f[ zero2(f_0, Q)=(s^2 + 2 \pi f_0 s / Q + (2 \pi f_0)^2).\f]
  * For  a normalized complex pole pair ["n"] the  formula is 
  * \f[ pole2(f_0) = (1 + i f/f_0 + (f/f_0)^2)\f] with \f$f0 > 0\f$.
  *
  * Throws a runtime_error or invalid_argument exception, if an illegal 
  * argument is specified.
  *
  * @memo Make a complex zero pair
  * @exception std::invalid_argument Invalid plane.
  * @param fs Sampling frequency
  * @param f0 Zero frequency.
  * @param Q quality factor
  * @param gain gain.
  * @param plane location where poles/zeros are specified
  * @param prewarp True for prewarping frequencies.
  * @return IIRFilter
  */
IIRFilter zero2( double      fs,
                 double      f0,
                 double      Q,
                 double      gain = 1.0,
                 const char* plane = "s",
                 bool        prewarp = true );

/*! Make an IIR filter from zeros and poles.
  * Poles and zero can be specified in the s-plane using units of rad/s (set 
  * plane to "s") or units of Hz ("f"). In both cases the real part of
  * the roots are expected to be NEGATIVE. Alternately, one can specify 
  * normalized poles and zeros ("n"). Normalized poles and zeros are expected 
  * to have POSITIVE real parts and their respective low and high frequency 
  * gains are set to 1 - unless they are located a 0Hz. The default location 
  * is the s-plane.
  * For the s-plane the formula is:
  * \f[ zpk(s) = k \frac{(s - s_1)(s - s_2) \cdots}
                        {(s - p_1)(s - p_2) \cdots} \f]
  * with \f$ s_1, s_2, \dots \f$ the locations of the zeros and 
  * \f$ p_1, p_2, \dots \f$ the location of the poles. By replacing 
  * \f$ s_1 \rightarrow 2 \pi fz_1 \f$ and \f$ p_1 \rightarrow 2 \pi fp_1 \f$
  * one obtains the "f" representation. For normalize poles and zeros ["n"] 
  * one uses poles of the form 
  * \f[ pole(f0) = \frac{1}{1 + i f/f0}; \, f_0 > 0 \f] 
  * and \f[ pole(f0) = \frac{1}{i f} ; \, f_0 = 0 \f]. 
  * The zeros are then of the form 
  * \f[ zero(f_0) = (1 + i f/f_0); \, f_0 > 0 \f] 
  * and \f[ zero(f0) = (i f); \, f_0 = 0 \f] .
  * Poles and zeros with a non-zero imaginary part must come in pairs 
  * of complex conjugates.
  *
  * @memo Make a filter from zeros, poles and gain (zpk)
  * @exception std::invalid_argument negative nzeros, npoles or fs.
  * @param fs Sampling frequency
  * @param nzeros Number of zeros
  * @param zeros Array of zeros
  * @param npoles Number of poles
  * @param poles Array of poles
  * @param gain Gain
  * @param plane location where poles/zeros are specified
  * @param prewarp True for prewarping frequencies.
  * @return IIRFilter
  */
IIRFilter zpk( double          fs,
               int             nzeros,
               const dComplex* zeros,
               int             npoles,
               const dComplex* poles,
               double          gain = 1.0,
               const char*     plane = "s",
               bool            prewarp = true );

/*! Make an IIR filter from zeros and poles. This function works as the one 
  * above, but the takes single precision complex poles and zeros as arguments.
  * @memo Make a filter from zeros, poles and gain (zpk notation)
  * @exception std::invalid_argument negative \a nzeros, \a npoles or \a fs.
  * @param fs Sampling frequency
  * @param nzeros Number of zeros
  * @param zeros Array of zeros in the specified plane
  * @param npoles Number of poles
  * @param poles Array of poles in the specified.
  * @param gain Gain
  * @param plane Plane in which poles and zeros are specified.
  * @param prewarp Prewarp frequencies if true.
  * @return IIRFilter with the specified poles and zeros.
  */
IIRFilter zpk( double          fs,
               int             nzeros,
               const fComplex* zeros,
               int             npoles,
               const fComplex* poles,
               double          gain = 1.0,
               const char*     plane = "s",
               bool            prewarp = true );

/** Make a filter from a rational polynomial in s.
  * A rational polynomial in s is specified by the polynomial 
  * coefficients in the numerator and the denominator in descending 
  * order of s. Fails if for some reasons the roots can not be
  * determined.
  * 
  * The formula is:
  * \f[ zpk(s) = k \frac{a_n s^{n_z} + a_{n-1} s^{n_z - 1} \cdots}
                     {b_n s^{n_p} + b_{n-1} s^{n_p - 1} \cdots} \f]
  * where \f$ a_n , a_{n-1} , \dots, a_0 \f$ are the coefficients of
  * the numerator polynomial and \f$ b_n, b_{n-1}, \dots, b_0 \f$ are the 
  * coefficients of the denominator polynomial.
  * The polynomial coefficients must be real.
  * 
  * @memo Add a rational polynomial (rpoly)
  * @exception std::invalid_argument sampling rate is negative or zero, a
  * number of coefficients is zero or one of the first coefficients is zero.
  * @param fs Sampling frequency
  * @param nnumer Number of coefficients in numerator
  * @param numer Array of numerator coefficients
  * @param ndenom Number of coefficients in denominator
  * @param denom Array of denominator coefficients
  * @param gain Gain
  * @param prewarp True for prewarping frequencies.
  * @return IIRFilter
  */
IIRFilter rpoly( double        fs,
                 int           nnumer,
                 const double* numer,
                 int           ndenom,
                 const double* denom,
                 double        gain,
                 bool          prewarp = true );

/** Make a filter from a rational polynomial in s. This function works as the
  * one above, but the takes single precision coefficients as arguments.
  * @memo Add a rational polynomial (rpoly)
  * @exception std::invalid_argument sampling rate is negative or zero, a
  *                                  number of coefficients is zero or one of
  *                                  the first coefficients is zero.
  * @param fs Sampling frequency
  * @param nnumer Number of coefficients in numerator
  * @param numer Array of numerator coefficients
  * @param ndenom Number of coefficients in denominator
  * @param denom Array of denominator coefficients
  * @param gain Gain
  * @param prewarp True for prewarping frequencies.
  * @return IIRFilter
  */
IIRFilter rpoly( double       fs,
                 int          nnumer,
                 const float* numer,
                 int          ndenom,
                 const float* denom,
                 double       gain,
                 bool         prewarp = true );

/** Make a IIR filter from a second order section.
  *
  * @memo Make a filter from b0, b1, b2, a1 and a2
  * @exception std::invalid_argument sampling frequency is not positive,
  *            the z pole is not in the unit circle, or b0 is zero.
  * @param fs Sampling frequency
  * @param b0 b0
  * @param b1 b1
  * @param b2 b2
  * @param a1 a1
  * @param a2 a2
  * @return IIRFilter
  */
IIRFilter
biquad( double fs, double b0, double b1, double b2, double a1, double a2 );

/** Make a IIR filter from a list of second order sections. If the format is
  * 's' (standard), the coefficients are ordered like:
  * \f[ { gain, b1_1, b2_1, a1_1, a2_1, b1_2, b2_2, a1_2, a2_2,... } \f]
  * whereas for the the format 'o' (online) the order is
  * \f[ { gain, a1_1, a2_1, b1_1, b2_1, a1_2, a2_2, b1_2, b2_2,... } \f]
  * The number of coefficients must be 4 times the number of second order
  * sections plus one.
  *
  * @memo Make a filter from second order sections
  * @exception std::invalid_argument sampling frequency is not positive,
  *            the number of coefficients is not a positive multiple of 4,
  *            the format code is not valid or the z-pole is outside the
  *            unit circle.
  * @param fs Sampling frequency
  * @param nba Number of coefficients
  * @param ba List of coefficients
  * @param format Coefficient format
  * @return IIRFilter
  */
IIRFilter sos( double fs, int nba, const double* ba, const char* format = "s" );

/** Make an IIR filter from a list of double complex poles and zeros in the 
  * z-plane.
  * To be stable the z-plane poles must lie within the unity circle.
  *
  * @memo Make a filter from z-plane roots
  * @exception std::invalid_argument sampling frequency is not positive,
  *            the number of pole/zeros are negative, the poles are not 
  *            in the unit circle or the roots are not in complex pairs.
  * @param fs Sampling frequency
  * @param nzeros Number of zeros
  * @param zero Array of zeros
  * @param npoles Number of poles
  * @param pole Array of poles
  * @param gain Gain
  * @return IIRFilter
  */
IIRFilter zroots( double          fs,
                  int             nzeros,
                  const dComplex* zero,
                  int             npoles,
                  const dComplex* pole,
                  double          gain = 1.0 );

/** Make an IIR filter from the list of poles and zeros in the z-plane.
  * To be stable the z-plane poles must lie within the unity circle.
  * @memo Construct a filter from z-plane roots
  * @exception std::invalid_argument Sampling frequency is not positive,
  *            the number of poles or zeros is negative, the poles are not 
  *            in the unit circle or the roots are not in complex pairs.
  * @param fs Sampling frequency
  * @param nzeros Number of zeros
  * @param zero Array of zeros
  * @param npoles Number of poles
  * @param pole Array of poles
  * @param gain Gain
  * @return IIRFilter constructed from z-plane roots.
  */
IIRFilter zroots( double          fs,
                  int             nzeros,
                  const fComplex* zero,
                  int             npoles,
                  const fComplex* pole,
                  double          gain = 1.0 );

/** Make a filter from the direct form. 
  * The direct form can be written as
  * \f[
    H(z)= \frac{\sum_{k=0}^{nb}b_k z^{-k}}{1 - \sum_{k=1}^{na}a_k z^{-k}}
    \f]
  * 
  * Cascaded second order sections are formed by finding the roots
  * of the direct form. The specified coefficients are \f$b_0\f$, \f$b_1\f$,...,
  * \f$b_{nb}\f$ for the numerator and \f$a_1\f$, \f$a_2\f$,..., \f$a_{na}\f$ 
  * for the denominator. 
  * The argument \a nb specifies the number of \a b coefficients
  * supplied to the function minus 1, whereas \a na is exactly the number
  * of coefficients since \f$a_0\f$ is always 1 and omitted from the list.
  * 
  * Avoid the direct form since even fairly simple filters will run into 
  * precision problems.
  *
  * @memo Make a filter from the direct form
  * @exception std::invalid_argument sampling frequency is not positive, the
  *            number of a (b) coefficients is negative (not positive), the
  *            first b coefficient is zero or the algorithm is unable to
  *            find a cascaded form.
  * @param fs Sampling frequency
  * @param nb Number of coefficients in numerator
  * @param b Array of b coefficients
  * @param na Number of coefficients in denominator
  * @param a Array of denominator coefficients exclusive a0
  * @return IIRFilter
  */
IIRFilter direct( double fs, int nb, const double* b, int na, const double* a );

/** Returns the closed loop response.
  * The returned filter is the closed loop response of the specified
  * filter. This function will compute the closed loop poles and zeros
  * from the open loop transfer function.
  *
  * @memo Closed loop transfer function
  * @exception std::invalid_argument filter is not IIR or the function is 
  *            unable to obtain the polynomial form.
  * @see FilterDesign.hh
  * @param filter Open loop transfer function.
  * @param k Additional gain
  * @return Closed loop transfer function.
  */
IIRFilter closeloop( const Pipe& filter, double k );

/**
 * \}
 */
#endif
