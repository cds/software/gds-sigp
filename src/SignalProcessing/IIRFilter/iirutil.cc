/* -*- mode: c++; c-basic-offset: 3; -*- */
#include <time.h>
#include <cmath>
#include <cstring>
#include <stdio.h>
#include "iirutil.hh"
#include "MultiPipe.hh"
#include "constant.hh"
#include "polyroot.hh"
#include <algorithm>
#include <exception>
#include <stdexcept>
#include <iostream>

using namespace std;

const int    kMaxTestSteps = 1000;
const double kTestStep = 0.988553095; // 0.1dB
const double kTestGainTolerance = 1E-8;
const double kTestPhaseTolerance = 1E-8;
const double kEpsilon = 1E-10;
const double kEpsilonSqr = 1E-6;

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// sort, quadroots, chop					        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
// class cmplx_sort {
// public:
// cmplx_sort() {
// }
// bool operator() (const dComplex& c1, const dComplex& c2) const {
// double m1 = abs(c1);
// double m2 = abs(c2);
// return (fabs (m2-m1) > kEpsilonSqr) ? m1 < m2 :
// c1.imag() > c2.imag();
// }
// };

//____________________________________________________________________________
class root_sort
{
private:
    dComplex fOfs;

public:
    root_sort( bool splane = true ) : fOfs( splane ? 0 : 1, 0 )
    {
    }
    bool operator( )( const dComplex& c1, const dComplex& c2 ) const;
};

//____________________________________________________________________________
bool
root_sort::operator( )( const dComplex& c1, const dComplex& c2 ) const
{
    if ( fabs( c1.imag( ) ) < kEpsilon )
    {
        if ( fabs( c2.imag( ) ) < kEpsilon )
        {
            return fabs( ( c1 - fOfs ).real( ) ) <
                fabs( ( c2 - fOfs ).real( ) ); // both real
        }
        else
        {
            return false; // c1 real
        }
    }
    else
    {
        if ( fabs( c2.imag( ) ) < kEpsilon )
        {
            return true; // c2 real
        }
        else
        { // both complex
            double m1 = abs( c1 - fOfs );
            double m2 = abs( c2 - fOfs );
            return ( fabs( m2 - m1 ) > kEpsilonSqr )
                ? m1 < m2
                : fabs( c1.imag( ) ) < fabs( c2.imag( ) );
        }
    }
}

//____________________________________________________________________________
bool
sort_roots( dComplex* c, int num, bool splane )
{
    // sort by magnitude and angle
    sort( c, c + num, root_sort( splane ) );
    // make sure that multiple identical roots are in complex conj. pairs
    for ( int i = 0; i < num; )
    {
        // count number of identical roots
        int j = i + 1;
        while ( ( j < num ) &&
                ( ( abs( c[ j ] - c[ i ] ) < kEpsilonSqr ) ||
                  ( abs( c[ j ] - ~c[ i ] ) < kEpsilonSqr ) ) )
        {
            ++j;
        }
        // pair them up
        for ( int k = i + 1; k < j; ++k )
        {
            dComplex x = ( ( k - i ) % 2 == 0 ) ? c[ i ] : ~c[ i ];
            // check if pairing is ok
            if ( abs( c[ k ] - x ) < kEpsilonSqr )
            {
                continue;
            }
            // if not, look for a available root in the list
            for ( int l = k + 1; l < j; ++l )
            {
                if ( abs( c[ l ] - x ) < kEpsilonSqr )
                {
                    swap( c[ k ], c[ l ] );
                    break;
                }
            }
        }
        i = j;
    }
    // now check if all complex roots have a conjugate
    for ( int i = 0; i + 1 < num; i += 2 )
    {
        if ( ( fabs( c[ i ].imag( ) ) > kEpsilon ) &&
             ( abs( c[ i ] - ~c[ i + 1 ] ) > kEpsilonSqr ) )
        {
            return false;
        }
    }
    if ( ( num % 2 == 1 ) && ( num > 0 ) &&
         ( fabs( c[ num - 1 ].imag( ) ) > kEpsilon ) )
    {
        return false;
    }
    return true;
}

//____________________________________________________________________________
static void
quadroots( double a, double b, double c, dComplex* res )
{
    dComplex q;
    dComplex D( b * b - 4 * a * c, 0 );
    if ( b < 0 )
    {
        q = -( dComplex( b, 0 ) - sqrt( D ) ) / 2.0;
    }
    else
    {
        q = -( dComplex( b, 0 ) + sqrt( D ) ) / 2.0;
    }
    res[ 0 ] = q / a;
    res[ 1 ] = dComplex( c, 0 ) / q;
}

//____________________________________________________________________________
inline double
chop( double x, double epsilon = kEpsilon )
{
    return ( fabs( x ) < epsilon ) ? 0 : x;
}

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// s-plane/-z-plane transform					        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
double
bilinear( double fs, dComplex& root, bool prewarp )
{
    // prewarp
    double g = 1.;
    if ( prewarp )
    {
        double mag = abs( root );
        if ( mag > 0 )
        {
            g = ( ( 2. * fs ) / mag ) * tan( mag / ( 2. * fs ) );
            root *= g;
        }
    }
    // transform
    dComplex twofs( 2. * fs, 0 );
    dComplex denom = dComplex( 1., 0. ) / ( twofs - root );
    root = ( twofs + root ) * denom;
    return abs( denom ) * g;
}

dComplex
bilinear_root( double fs, dComplex root, bool prewarp )
{
    bilinear( fs, root, prewarp );
    return root;
}

//____________________________________________________________________________
double
inverse_bilinear( double fs, dComplex& root, bool unwarp )
{
    // inverse transform
    dComplex denom = dComplex( 2. * fs, 0 ) / ( root + dComplex( 1., 0. ) );
    root = ( root - dComplex( 1., 0. ) ) * denom;
    // unwarp
    double g = 1.;
    if ( unwarp )
    {
        double mag = abs( root );
        if ( mag > 0 )
        {
            g = ( 2. * fs / mag ) * atan( mag / ( 2. * fs ) );
            root *= g;
        }
    }
    return 2. * abs( denom ) * g;
}

dComplex
inverse_bilinear_root( double fs, dComplex root, bool unwarp )
{
    inverse_bilinear( fs, root, unwarp );
    return root;
}

//____________________________________________________________________________
bool
s2z( double      fs,
     int         nzeros,
     dComplex*   zero,
     int         npoles,
     dComplex*   pole,
     double&     gain,
     const char* plane,
     bool        prewarp )
{
    // zeros
    for ( int i = 0; i < nzeros; ++i )
    {
        // location?
        if ( plane[ 0 ] == 'n' )
        {
            double f0 = abs( zero[ i ] );
            zero[ i ] = -~zero[ i ] * 2 * pi;
            gain /= ( fabs( f0 ) > kEpsilon ) ? 2 * pi * f0 : 2 * pi;
        }
        else if ( plane[ 0 ] == 'f' )
        {
            zero[ i ] *= 2 * pi;
        }
        else
        {
            // nothing
        }
    }
    // poles
    for ( int i = 0; i < npoles; ++i )
    {
        // location?
        if ( plane[ 0 ] == 'n' )
        {
            double f0 = abs( pole[ i ] );
            pole[ i ] = -~pole[ i ] * 2 * pi;
            gain *= ( fabs( f0 ) > kEpsilon ) ? 2 * pi * f0 : 2 * pi;
        }
        else if ( plane[ 0 ] == 'f' )
        {
            pole[ i ] *= 2 * pi;
        }
        else
        {
            // nothing
        }
    }
    // need to make sure we have complex conjugate pairs
    if ( !sort_roots( zero, nzeros ) )
    {
        cerr << "zeros must come in complex conjugates pairs" << endl;
        return false;
    }
    if ( !sort_roots( pole, npoles ) )
    {
        cerr << "poles must come in complex conjugates pairs" << endl;
        return false;
    }
    //       sort_roots (zero, nzeros);
    //       sort_roots (pole, npoles);
    //       for (int i = 0; i + 1 < nzeros; i += 2) {
    //          if ((fabs (zero[i].imag()) > kEpsilon) &&
    //             (abs(zero[i] - ~zero[i+1]) > kEpsilonSqr)) {
    //             cerr << "zeros must come in complex conjugates pairs" << endl;
    //             return false;
    //          }
    //       }
    //       for (int i = 0; i + 1 < npoles; i += 2) {
    //          if ((fabs (pole[i].imag()) > kEpsilon) &&
    //             (abs(pole[i] - ~pole[i+1]) > kEpsilonSqr)) {
    //             cerr << "poles must come in complex conjugates pairs" << endl;
    //             return false;
    //          }
    //       }
    // test that poles are in the left hand plane
#ifndef RIGHT_HALF_PLANE_ALLOWED
    for ( int i = 0; i < npoles; ++i )
    {
        if ( pole[ i ].real( ) > 0 )
        {
            cerr << "poles must be in the left half plane" << endl;
            return false;
        }
    }
#endif
    // transform
    for ( int i = 0; i < nzeros; ++i )
    {
        // bilinear
        gain /= bilinear( fs, zero[ i ], prewarp );
    }
    for ( int i = 0; i < npoles; ++i )
    {
        // bilinear
        gain *= bilinear( fs, pole[ i ], prewarp );
    }
    sort_roots( zero, nzeros, false );
    sort_roots( pole, npoles, false );
    return true;
}

//____________________________________________________________________________
bool
s2z( double          fs,
     int             nzeros,
     const dComplex* zero,
     int             npoles,
     const dComplex* pole,
     double          gain,
     int&            nba,
     double*         ba,
     const char*     plane,
     const char*     format,
     bool            prewarp )
{
    if ( !format || ( strlen( format ) != 1 ) ||
         ( strchr( "so", format[ 0 ] ) == 0 ) )
    {
        return false;
    }
    if ( !plane || ( strlen( plane ) != 1 ) ||
         ( strchr( "sfn", plane[ 0 ] ) == 0 ) )
    {
        return false;
    }
    double    g = gain;
    dComplex* z = new dComplex[ nzeros + 1 ];
    dComplex* p = new dComplex[ npoles + 1 ];
    // copy zeros
    for ( int i = 0; i < nzeros; ++i )
    {
        z[ i ] = zero[ i ];
    }
    // copy poles
    for ( int i = 0; i < npoles; ++i )
    {
        p[ i ] = pole[ i ];
    }
    bool succ = s2z( fs, nzeros, z, npoles, p, g, plane, prewarp ) &&
        z2z( nzeros, z, npoles, p, g, nba, ba, format );
    delete[] z;
    delete[] p;
    return succ;
}

//____________________________________________________________________________
bool
z2s( double      fs,
     int         nzeros,
     dComplex*   zero,
     int         npoles,
     dComplex*   pole,
     double&     gain,
     const char* plane,
     bool        unwarp )
{
    // zeros
    for ( int i = 0; i < nzeros; ++i )
    {
        // inverse bilinear
        gain /= inverse_bilinear( fs, zero[ i ], unwarp );
        // location?
        if ( plane[ 0 ] == 'n' )
        {
            zero[ i ] = -~zero[ i ] / ( 2 * pi );
            double f0 = abs( zero[ i ] );
            gain *= ( fabs( f0 ) > kEpsilon ) ? 2 * pi * f0 : 2 * pi;
        }
        else if ( plane[ 0 ] == 'f' )
        {
            zero[ i ] /= 2 * pi;
        }
        else
        {
            // nothing
        }
    }
    // poles
    for ( int i = 0; i < npoles; ++i )
    {
        // inverse bilinear
        gain *= inverse_bilinear( fs, pole[ i ], unwarp );
        // location?
        if ( plane[ 0 ] == 'n' )
        {
            pole[ i ] = -~pole[ i ] / ( 2 * pi );
            double f0 = abs( pole[ i ] );
            gain /= ( fabs( f0 ) > kEpsilon ) ? 2 * pi * f0 : 2 * pi;
        }
        else if ( plane[ 0 ] == 'f' )
        {
            pole[ i ] /= 2 * pi;
        }
        else
        {
            // nothing
        }
    }
    sort_roots( zero, nzeros );
    sort_roots( pole, npoles );
    return true;
}

//____________________________________________________________________________
bool
z2s( double        fs,
     int           nba,
     const double* ba,
     int&          nzeros,
     dComplex*     zero,
     int&          npoles,
     dComplex*     pole,
     double&       gain,
     const char*   format,
     const char*   plane,
     bool          unwarp )
{
    return z2z( nba, ba, nzeros, zero, npoles, pole, gain, format ) &&
        z2s( fs, nzeros, zero, npoles, pole, gain, plane, unwarp );
}

//____________________________________________________________________________
bool
z2z( int           nba,
     const double* ba,
     int&          nzeros,
     dComplex*     zero,
     int&          npoles,
     dComplex*     pole,
     double&       gain,
     const char*   format )
{
    if ( !format || ( strlen( format ) != 1 ) ||
         ( strchr( "so", format[ 0 ] ) == 0 ) )
    {
        return false;
    }
    if ( ( nba < 1 ) || ( ( nba - 1 ) % 4 != 0 ) )
    {
        return false;
    }
    int soscount = ( nba - 1 ) / 4;
    gain = ba[ 0 ];
    nzeros = 0;
    npoles = 0;
    for ( int i = 0; i < soscount; ++i )
    {
        double b1 = ba[ 4 * i + 1 ];
        double b2 = ba[ 4 * i + 2 ];
        double a1 = ba[ 4 * i + 3 ];
        double a2 = ba[ 4 * i + 4 ];
        if ( format[ 0 ] == 'o' )
        {
            swap( b1, a1 );
            swap( b2, a2 );
        }
        int order = 2;
        if ( ( fabs( b2 ) < kEpsilon ) && ( fabs( a2 ) < kEpsilon ) )
            order = 1;
        if ( ( order == 1 ) && ( fabs( b1 ) < kEpsilon ) &&
             ( fabs( a1 ) < kEpsilon ) )
            order = 0;
        if ( order == 0 )
        {
            continue;
        }
        // first order
        if ( order == 1 )
        {
            // don't count zeros and poles at Nyqist (infinity)
            if ( fabs( b1 - 1 ) > kEpsilon )
            {
                zero[ nzeros++ ] = dComplex( -b1, 0 );
            }
            if ( fabs( a1 - 1 ) > kEpsilon )
            {
                pole[ npoles++ ] = dComplex( -a1, 0 );
            }
        }
        // second order
        else
        {
            // don't count zeros at Nyqist (infinity)
            if ( ( fabs( b1 - 2 ) < kEpsilon ) &&
                 ( fabs( b2 - 1 ) < kEpsilon ) )
            {
                // 2 zeros at Nyquist
            }
            else if ( fabs( b1 - b2 - 1 ) < kEpsilon )
            {
                // 1 zero at Nyquist
                zero[ nzeros++ ] = dComplex( -b2, 0 );
            }
            else
            {
                // 0 zeros at Nyquist
                quadroots( 1, b1, b2, zero + nzeros );
                nzeros += 2;
            }
            if ( ( fabs( a1 - 2 ) < kEpsilon ) &&
                 ( fabs( a2 - 1 ) < kEpsilon ) )
            {
                // 2 poles at Nyquist
            }
            else if ( fabs( a1 - a2 - 1 ) < kEpsilon )
            {
                // 1 pole at Nyquist
                pole[ npoles++ ] = dComplex( -a2, 0 );
            }
            else
            {
                // 0 poles at Nyquist
                quadroots( 1, a1, a2, pole + npoles );
                npoles += 2;
            }
        }
    }
    sort_roots( zero, nzeros, false );
    sort_roots( pole, npoles, false );
    return true;
}

//____________________________________________________________________________
bool
z2z( int             nzeros,
     const dComplex* zero,
     int             npoles,
     const dComplex* pole,
     double          gain,
     int&            nba,
     double*         ba,
     const char*     format )
{
    if ( !format || ( strlen( format ) != 1 ) ||
         ( strchr( "so", format[ 0 ] ) == 0 ) )
    {
        return false;
    }
    int       order = max( nzeros, npoles );
    dComplex* z = new dComplex[ order ];
    dComplex* p = new dComplex[ order ];
    // copy zeros
    for ( int i = 0; i < nzeros; ++i )
    {
        z[ i ] = zero[ i ];
    }
    // add zeros at -1, if necessary
    for ( int i = nzeros; i < order; ++i )
    {
        z[ i ] = dComplex( -1, 0 );
    }
    // copy poles
    for ( int i = 0; i < npoles; ++i )
    {
        p[ i ] = pole[ i ];
    }
    // add zeros at f = fSample/pi, if necessary
    for ( int i = npoles; i < order; ++i )
    {
        // if we add two poles, make them butterworth
        if ( ( i % 2 == 0 ) && ( i + 1 < order ) )
        {
            p[ i ] = polar( 2., 3. / 4. * pi );
            p[ i + 1 ] = ~p[ i ];
            gain *= 4 * bilinear( 1, p[ i ] ) * bilinear( 1, p[ i + 1 ] );
            ++i;
        }
        // otherwise just one real pole
        else
        {
            p[ i ] = dComplex( -2., 0 );
            gain *= -2 * bilinear( 1, p[ i ] );
        }
    }
    // need to make sure we have complex conjugate pairs
    if ( !sort_roots( z, nzeros, false ) || !sort_roots( p, npoles, false ) )
    {
        cerr << "roots must come in complex conjugates pairs" << endl;
        delete[] z;
        delete[] p;
        return false;
    }
    //       sort_roots (z, nzeros);
    //       sort_roots (p, npoles);
    //       for (int i = 0; i + 1 < order; i += 2) {
    //          if ((fabs (z[i].imag()) > kEpsilon) &&
    //             (abs(z[i] - ~z[i+1]) > kEpsilonSqr)) {
    //             cerr << "roots must come in complex conjugates pairs" << endl;
    //             delete [] z;
    //             delete [] p;
    //             return false;
    //          }
    //          if ((fabs (p[i].imag()) > kEpsilon) &&
    //             (abs(p[i] - ~p[i+1]) > kEpsilonSqr)) {
    //             cerr << "roots must come in complex conjugates pairs" << endl;
    //             delete [] z;
    //             delete [] p;
    //             return false;
    //          }
    //       }
    // test that poles are within unit circle
#ifndef RIGHT_HALF_PLANE_ALLOWED
    for ( int i = 0; i < order; ++i )
    {
        if ( abs( p[ i ] ) > 1 + kEpsilon )
        {
            cerr << "poles must be in the left half plane" << endl;
            delete[] z;
            delete[] p;
            return false;
        }
    }
#endif

    // now make the SOS
    ba[ 0 ] = gain;
    int soscount = 0;
    for ( int i = 0; i + 1 < order; i += 2, ++soscount )
    {
        ba[ 4 * soscount + 1 ] = -( z[ i ] + z[ i + 1 ] ).real( );
        ba[ 4 * soscount + 2 ] = ( z[ i ] * z[ i + 1 ] ).real( );
        ba[ 4 * soscount + 3 ] = -( p[ i ] + p[ i + 1 ] ).real( );
        ba[ 4 * soscount + 4 ] = ( p[ i ] * p[ i + 1 ] ).real( );
        if ( format[ 0 ] == 'o' )
        {
            swap( ba[ 4 * soscount + 1 ], ba[ 4 * soscount + 3 ] );
            swap( ba[ 4 * soscount + 2 ], ba[ 4 * soscount + 4 ] );
        }
    }
    // left over one?
    if ( order % 2 == 1 )
    {
        ba[ 4 * soscount + 1 ] = -z[ order - 1 ].real( );
        ba[ 4 * soscount + 2 ] = 0;
        ba[ 4 * soscount + 3 ] = -p[ order - 1 ].real( );
        ba[ 4 * soscount + 4 ] = 0;
        if ( format[ 0 ] == 'o' )
        {
            swap( ba[ 4 * soscount + 1 ], ba[ 4 * soscount + 3 ] );
            swap( ba[ 4 * soscount + 2 ], ba[ 4 * soscount + 4 ] );
        }
        ++soscount;
    }
    nba = 1 + 4 * soscount;
    delete[] z;
    delete[] p;
    return true;
}

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// IIR filter utilities						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
bool
isiir( const Pipe& filter )
{
    if ( dynamic_cast< const IIRFilter* >( &filter ) )
    {
        return true;
    }
    const MultiPipe* mp = dynamic_cast< const MultiPipe* >( &filter );
    if ( !mp )
    {
        return false;
    }
    for ( MultiPipe::PipeConfig::const_iterator i = mp->pipe( ).begin( );
          i != mp->pipe( ).end( );
          ++i )
    {
        if ( !isiir( **i ) )
        {
            return false;
        }
    }
    return true;
}

//____________________________________________________________________________
int
iirsoscount( const Pipe& filter )
{
    const IIRFilter* iir = dynamic_cast< const IIRFilter* >( &filter );
    if ( iir )
    {
        return iir->getSOS( ).size( );
    }
    const MultiPipe* mp = dynamic_cast< const MultiPipe* >( &filter );
    if ( !mp )
    {
        return -1;
    }
    int ord = 0;
    for ( MultiPipe::PipeConfig::const_iterator i = mp->pipe( ).begin( );
          i != mp->pipe( ).end( );
          ++i )
    {
        int o = iirsoscount( **i );
        if ( o < 0 )
        {
            return -1;
        }
        ord += o;
    }
    return ord;
}

//____________________________________________________________________________
int
iirpolecount( const Pipe& filter )
{
    int npoles;
    int nzeros;
    if ( !iirpolezerocount( filter, npoles, nzeros ) )
    {
        return -1;
    }
    else
    {
        return npoles;
    }
}

//____________________________________________________________________________
int
iirzerocount( const Pipe& filter )
{
    int npoles;
    int nzeros;
    if ( !iirpolezerocount( filter, npoles, nzeros ) )
    {
        return -1;
    }
    else
    {
        return nzeros;
    }
}

//____________________________________________________________________________
bool
iirpolezerocount( const Pipe& filter, int& npoles, int& nzeros )
{
    npoles = 0;
    nzeros = 0;
    const IIRFilter* iir = dynamic_cast< const IIRFilter* >( &filter );
    if ( iir )
    {
        for ( IIRFilter::const_sos_iter i = iir->getSOS( ).begin( );
              i != iir->getSOS( ).end( );
              ++i )
        {
            double b1 = i->B1( ) / i->B0( );
            double b2 = i->B2( ) / i->B0( );
            double a1 = i->A1( );
            double a2 = i->A2( );
            int    order = 2;
            if ( fabs( b2 ) < kEpsilon && fabs( a2 ) < kEpsilon )
            {
                order = 1;
                if ( fabs( b1 ) < kEpsilon && fabs( a1 ) < kEpsilon )
                {
                    order = 0;
                    continue;
                }
            }
            // first order
            if ( order == 1 )
            {
                // don't count zeros at Nyqist (infinity)
                if ( fabs( b1 - 1 ) > kEpsilon )
                {
                    ++nzeros;
                }
                if ( fabs( a1 - 1 ) > kEpsilon )
                {
                    ++npoles;
                }
            }
            // second order
            else
            {
                // don't count zeros at Nyqist (infinity)
                if ( ( fabs( b1 - 2 ) < kEpsilon ) &&
                     ( fabs( b2 - 1 ) < kEpsilon ) )
                {
                }
                else if ( fabs( b1 - b2 - 1 ) < kEpsilon )
                {
                    nzeros += 1;
                }
                else
                {
                    nzeros += 2;
                }
                if ( ( fabs( a1 - 2 ) < kEpsilon ) &&
                     ( fabs( a2 - 1 ) < kEpsilon ) )
                {
                }
                else if ( fabs( a1 - a2 - 1 ) < kEpsilon )
                {
                    npoles += 1;
                }
                else
                {
                    npoles += 2;
                }
            }
        }
        return true;
    }
    const MultiPipe* mp = dynamic_cast< const MultiPipe* >( &filter );
    if ( !mp )
    {
        return false;
    }
    for ( MultiPipe::PipeConfig::const_iterator i = mp->pipe( ).begin( );
          i != mp->pipe( ).end( );
          ++i )
    {
        int np;
        int nz;
        if ( !iirpolezerocount( **i, np, nz ) )
        {
            return false;
        }
        npoles += np;
        nzeros += nz;
    }
    return true;
}

//____________________________________________________________________________
int
iirorder( const Pipe& filter )
{
    int npoles = 0;
    int nzeros = 0;
    if ( !iirpolezerocount( filter, npoles, nzeros ) )
    {
        return -1;
    }
    else
    {
        return max( nzeros, npoles );
    }
}

//____________________________________________________________________________
bool
iircmp( const Pipe& f1, const Pipe& f2 )
{
    int soscount = iirsoscount( f1 );
    if ( ( soscount < 0 ) || ( soscount != iirsoscount( f2 ) ) )
    {
        return false;
    }
    int       nz1, nz2, np1, np2;
    double    g1, g2;
    dComplex* z1 = new dComplex[ 2 * soscount ];
    dComplex* z2 = new dComplex[ 2 * soscount ];
    dComplex* p1 = new dComplex[ 2 * soscount ];
    dComplex* p2 = new dComplex[ 2 * soscount ];
    // transform into z-plane roots
    if ( !iir2z( f1, nz1, z1, np1, p1, g1 ) ||
         !iir2z( f2, nz2, z2, np2, p2, g2 ) || ( nz1 != nz2 ) ||
         ( np1 != np2 ) || ( fabs( g2 - g1 ) > kEpsilonSqr ) )
    {
        delete[] p1;
        delete[] p2;
        delete[] z1;
        delete[] z2;
        return false;
    }
    // compare zeros
    for ( int i = 0; i < nz1; ++i )
    {
        bool foundit = false;
        for ( int j = 0; j < nz2; ++j )
        {
            dComplex diff = z2[ j ] - z1[ i ];
            if ( abs( diff ) < kEpsilonSqr )
            {
                foundit = true;
                for ( int k = j; k < nz2 - 1; ++k )
                    z2[ k ] = z2[ k + 1 ];
                --nz2;
                break;
            }
        }
        if ( !foundit )
        {
            delete[] p1;
            delete[] p2;
            delete[] z1;
            delete[] z2;
            return false;
        }
    }
    // compare poles
    for ( int i = 0; i < np1; ++i )
    {
        bool foundit = false;
        for ( int j = 0; j < np2; ++j )
        {
            dComplex diff = p2[ j ] - p1[ i ];
            if ( abs( diff ) < kEpsilonSqr )
            {
                foundit = true;
                for ( int k = j; k < np2 - 1; ++k )
                    p2[ k ] = p2[ k + 1 ];
                --nz2;
                break;
            }
        }
        if ( !foundit )
        {
            delete[] p1;
            delete[] p2;
            delete[] z1;
            delete[] z2;
            return false;
        }
    }
    delete[] p1;
    delete[] p2;
    delete[] z1;
    delete[] z2;
    return true;
#if 0
      IIRFilter iir1, iir2;
      try {
         iir1 = iir2iir (f1);
         iir2 = iir2iir (f2);
      }
         catch (...) {
            return false;
         }
      if (iir1.getFSample() != iir2.getFSample()) {
         return false;
      }
      // Poor man's test for equality: test xfer
      double f = iir1.getFSample() / 2;
      fComplex c1;
      fComplex c2;
      for (int i = 0; i < kMaxTestSteps; ++i) {
         f *= kTestStep;
         if (!iir1.Xfer (c1, f) || !iir2.Xfer (c2, f) ||
            (fabs (abs(c1) - abs(c2)) > kTestGainTolerance) ||
            (fabs (arg(c1) - arg(c2)) > kTestPhaseTolerance)) {
            return false; 
         }
      }
      return true;
#endif
}

//____________________________________________________________________________
IIRFilter
iir2iir( const Pipe& filter )
{
    const IIRFilter* iir = dynamic_cast< const IIRFilter* >( &filter );
    if ( iir )
    {
        return *iir;
    }
    const MultiPipe* mp = dynamic_cast< const MultiPipe* >( &filter );
    if ( !mp )
    {
        throw std::invalid_argument( "Not an IIR filter" );
    }
    // determine sampling
    double    fs = 0;
    IIRFilter IIR;
    IIR *= mp->getGain( );
    for ( MultiPipe::PipeConfig::const_iterator i = mp->pipe( ).begin( );
          i != mp->pipe( ).end( );
          ++i )
    {
        IIRFilter tmp = iir2iir( **i );
        IIR *= tmp;
        if ( ( fs == 0 ) && ( tmp.getFSample( ) > 0 ) )
            fs = tmp.getFSample( );
    }
    IIRFilter ret( fs );
    ret *= IIR;
    return ret;
}

//____________________________________________________________________________
bool
iir2zpk( const Pipe& filter,
         int&        nzeros,
         dComplex*   zero,
         int&        npoles,
         dComplex*   pole,
         double&     gain,
         const char* plane,
         bool        unwarp )
{
    if ( !plane || ( strlen( plane ) != 1 ) ||
         ( strchr( "sfn", plane[ 0 ] ) == 0 ) )
    {
        return false;
    }
    IIRFilter iir;
    try
    {
        iir = iir2iir( filter );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    if ( !iir2z( iir, nzeros, zero, npoles, pole, gain ) ||
         !z2s( iir.getFSample( ),
               nzeros,
               zero,
               npoles,
               pole,
               gain,
               plane,
               unwarp ) )
    {
        return false;
    }
    return true;
}

//____________________________________________________________________________
bool
iir2poly( const Pipe& filter,
          int&        nnumer,
          double*     numer,
          int&        ndenom,
          double*     denom,
          double&     gain,
          bool        unwarp )
{
    int soscount = iirsoscount( filter );
    if ( soscount < 0 )
    {
        return false;
    }
    int       nzero;
    int       npole;
    dComplex* zero = new dComplex[ 2 * soscount ];
    dComplex* pole = new dComplex[ 2 * soscount ];
    if ( !iir2zpk( filter, nzero, zero, npole, pole, gain, "s", unwarp ) )
    {
        delete[] zero;
        delete[] pole;
        return false;
    }
    nnumer = polyexpand( (std::complex< double >*)zero, nzero, numer );
    if ( nnumer < 0 )
    {
        delete[] zero;
        delete[] pole;
        return false;
    }
    ++nnumer;
    ndenom = polyexpand( (std::complex< double >*)pole, npole, denom );
    if ( ndenom < 0 )
    {
        delete[] zero;
        delete[] pole;
        return false;
    }
    ++ndenom;

    delete[] zero;
    delete[] pole;
    return true;
}

//____________________________________________________________________________
bool
iir2zpk( const Pipe& filter, std::string& zpk, const char* plane, bool unwarp )
{
    int soscount = iirsoscount( filter );
    if ( soscount < 0 )
    {
        return false;
    }
    if ( plane && ( plane[ 0 ] == 'p' ) )
    {
        double* numer = new double[ 2 * soscount + 1 ];
        double* denom = new double[ 2 * soscount + 1 ];
        int     nnumer;
        int     ndenom;
        double  gain;
        if ( !iir2poly( filter, nnumer, numer, ndenom, denom, gain, unwarp ) )
        {
            delete[] numer;
            delete[] denom;
            return false;
        }
        zpk = "rpoly([";
        char buf[ 1024 ];
        for ( int i = 0; i < nnumer; ++i )
        {
            sprintf( buf, "%.16g", chop( numer[ i ] ) );
            if ( i > 0 )
                zpk += ";";
            zpk += buf;
        }
        zpk += "],[";
        for ( int i = 0; i < ndenom; ++i )
        {
            sprintf( buf, "%.16g", chop( denom[ i ] ) );
            if ( i > 0 )
                zpk += ";";
            zpk += buf;
        }
        zpk += "],";
        sprintf( buf, "%.16g", gain );
        zpk += buf;
        zpk += ")";
        delete[] numer;
        delete[] denom;
        return true;
    }
    else
    {
        dComplex* pole = new dComplex[ 2 * soscount ];
        dComplex* zero = new dComplex[ 2 * soscount ];
        int       npoles;
        int       nzeros;
        double    gain;
        if ( !iir2zpk(
                 filter, nzeros, zero, npoles, pole, gain, plane, unwarp ) )
        {
            delete[] pole;
            delete[] zero;
            return false;
        }
        zpk = "zpk([";
        char buf[ 1024 ];
        for ( int i = 0; i < nzeros; ++i )
        {
            double x = chop( zero[ i ].real( ) );
            double y = chop( zero[ i ].imag( ) );
            //cerr << "cz["<<i<<"]="<<x<<" "<<y<<endl;
            if ( y > 0 )
            {
                sprintf( buf, "%.16g+i*%.16g", x, y );
            }
            else if ( y < 0 )
            {
                sprintf( buf, "%.16g-i*%.16g", x, -y );
            }
            else
            {
                sprintf( buf, "%.16g", x );
            }
            //cerr<<"buf="<<buf<<endl;
            if ( i > 0 )
                zpk += ";";
            zpk += buf;
        }
        zpk += "],[";
        for ( int i = 0; i < npoles; ++i )
        {
            double x = chop( pole[ i ].real( ) );
            double y = chop( pole[ i ].imag( ) );
            //cerr << "cp["<<i<<"]="<<x<<" "<<y<<endl;
            if ( y > 0 )
            {
                sprintf( buf, "%.16g+i*%.16g", x, y );
            }
            else if ( y < 0 )
            {
                sprintf( buf, "%.16g-i*%.16g", x, -y );
            }
            else
            {
                sprintf( buf, "%.16g", x );
            }
            //cerr<<"buf="<<buf<<endl;
            if ( i > 0 )
                zpk += ";";
            zpk += buf;
        }
        zpk += "],";
        sprintf( buf, "%.16g", gain );
        zpk += buf;
        if ( plane[ 0 ] != 's' )
            zpk += string( ",\"" ) + plane[ 0 ] + "\"";
        zpk += ")";
        delete[] pole;
        delete[] zero;
        return true;
    }
}

//____________________________________________________________________________
bool
iir2z( const Pipe& filter, int& nba, double* ba, const char* format )
{
    if ( !format || ( strlen( format ) != 1 ) ||
         ( strchr( "so", format[ 0 ] ) == 0 ) )
    {
        return false;
    }

    IIRFilter iir;
    try
    {
        iir = iir2iir( filter );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }

    ba[ 0 ] = iir.getGain( );
    nba = 1;
    for ( IIRFilter::const_sos_iter i = iir.getSOS( ).begin( );
          i != iir.getSOS( ).end( );
          ++i )
    {
        ba[ 0 ] *= i->B0( );
        ba[ nba + 0 ] = i->B1( ) / i->B0( );
        ba[ nba + 1 ] = i->B2( ) / i->B0( );
        ba[ nba + 2 ] = i->A1( );
        ba[ nba + 3 ] = i->A2( );
        if ( format[ 0 ] == 'o' )
        {
            swap( ba[ nba + 0 ], ba[ nba + 2 ] );
            swap( ba[ nba + 1 ], ba[ nba + 3 ] );
        }
        //cerr << "ba[" << nba+0 << "]=" << ba[nba+0] << endl;
        //cerr << "ba[" << nba+1 << "]=" << ba[nba+1] << endl;
        //cerr << "ba[" << nba+2 << "]=" << ba[nba+2] << endl;
        //cerr << "ba[" << nba+3 << "]=" << ba[nba+3] << endl;
        nba += 4;
    }
    return true;
}

//____________________________________________________________________________
bool
iir2z( const Pipe& filter,
       int&        nzeros,
       dComplex*   zero,
       int&        npoles,
       dComplex*   pole,
       double&     gain )
{
    int soscount = iirsoscount( filter );
    if ( soscount < 0 )
    {
        return false;
    }
    int     nba;
    double* ba = new double[ 4 * soscount + 1 ];
    bool    succ = iir2z( filter, nba, ba ) &&
        z2z( nba, ba, nzeros, zero, npoles, pole, gain );
    //for (int i = 0; i < nzeros; ++i) cerr << "zz["<<i<<"]="<<zero[i]<<endl;
    //for (int i = 0; i < npoles; ++i) cerr << "zp["<<i<<"]="<<pole[i]<<endl;
    delete[] ba;
    return succ;
}

//____________________________________________________________________________
bool
iir2direct( const Pipe& filter, int& nb, double* b, int& na, double* a )
{
    int soscount = iirsoscount( filter );
    if ( soscount < 0 )
    {
        return false;
    }
    int       nzero;
    int       npole;
    dComplex* zero = new dComplex[ 2 * soscount ];
    dComplex* pole = new dComplex[ 2 * soscount ];
    double    gain;
    if ( !iir2z( filter, nzero, zero, npole, pole, gain ) )
    {
        delete[] zero;
        delete[] pole;
        return false;
    }
    nb = polyexpand( (std::complex< double >*)zero, nzero, b );
    if ( nb < 0 )
    {
        delete[] zero;
        delete[] pole;
        return false;
    }
    for ( int i = 0; i <= nb; ++i )
    {
        b[ i ] *= gain;
    }
    double* aa = new double[ 2 * soscount + 1 ];
    na = polyexpand( (std::complex< double >*)pole, npole, aa );
    if ( na < 0 )
    {
        delete[] zero;
        delete[] pole;
        delete[] aa;
        return false;
    }
    for ( int i = 1; i <= na; ++i )
    {
        a[ i - 1 ] = -aa[ i ];
    }

    delete[] zero;
    delete[] pole;
    delete[] aa;
    return true;
}

//____________________________________________________________________________
bool
iir2z( const Pipe& filter, std::string& z, const char* format )
{
    int soscount = iirsoscount( filter );
    if ( soscount < 0 )
    {
        return false;
    }
    if ( !format || ( strlen( format ) != 1 ) ||
         ( strchr( "rsod", format[ 0 ] ) == 0 ) )
    {
        return false;
    }
    // direct form
    if ( format[ 0 ] == 'd' )
    {
        double* a = new double[ 2 * soscount + 1 ];
        double* b = new double[ 2 * soscount + 1 ];
        int     na;
        int     nb;
        if ( !iir2direct( filter, nb, b, na, a ) )
        {
            delete[] a;
            delete[] b;
            return false;
        }
        z = "direct([";
        char buf[ 1024 ];
        for ( int i = 0; i <= nb; ++i )
        {
            sprintf( buf, "%.16g", b[ i ] );
            if ( i > 0 )
                z += ";";
            z += buf;
        }
        z += "],[";
        for ( int i = 0; i < na; ++i )
        {
            sprintf( buf, "%.16g", a[ i ] );
            if ( i > 0 )
                z += ";";
            z += buf;
        }
        z += "])";
        delete[] a;
        delete[] b;
    }

    // z-plane roots
    else if ( format[ 0 ] == 'r' )
    {
        dComplex* pole = new dComplex[ 2 * soscount ];
        dComplex* zero = new dComplex[ 2 * soscount ];
        int       npoles;
        int       nzeros;
        double    gain;
        if ( !iir2z( filter, nzeros, zero, npoles, pole, gain ) )
        {
            delete[] pole;
            delete[] zero;
            return false;
        }
        z = "zroots([";
        char buf[ 1024 ];
        for ( int i = 0; i < nzeros; ++i )
        {
            double x = chop( zero[ i ].real( ) );
            double y = chop( zero[ i ].imag( ) );
            if ( y > 0 )
            {
                sprintf( buf, "%.16g+i*%.16g", x, y );
            }
            else if ( y < 0 )
            {
                sprintf( buf, "%.16g-i*%.16g", x, -y );
            }
            else
            {
                sprintf( buf, "%.16g", x );
            }
            if ( i > 0 )
                z += ";";
            z += buf;
        }
        z += "],[";
        for ( int i = 0; i < npoles; ++i )
        {
            double x = chop( pole[ i ].real( ) );
            double y = chop( pole[ i ].imag( ) );
            if ( y > 0 )
            {
                sprintf( buf, "%.16g+i*%.16g", x, y );
            }
            else if ( y < 0 )
            {
                sprintf( buf, "%.16g-i*%.16g", x, -y );
            }
            else
            {
                sprintf( buf, "%.16g", x );
            }
            if ( i > 0 )
                z += ";";
            z += buf;
        }
        z += "],";
        sprintf( buf, "%.16g", gain );
        z += buf;
        z += ")";
        delete[] pole;
        delete[] zero;
    }

    // biquad sections
    else
    {
        double* ba = new double[ 4 * soscount + 1 ];
        int     nba;
        if ( !iir2z( filter, nba, ba, format ) )
        {
            delete[] ba;
            return false;
        }
        z = "sos(";
        char buf[ 1024 ];
        sprintf( buf, "%.16g", ba[ 0 ] );
        z += buf;
        z += ",[";
        double x = 0;
        for ( int i = 1; i < nba; ++i )
        {
            x = chop( ba[ i ] );
            x = chop( x ); // ::COMPILER:: need this! optimizer bug!!!
            //cerr << "ba[" << i << "]=" << ba[i] << " " << x << endl;
            sprintf( buf, "%.16g", x );
            if ( i > 1 )
                z += ";";
            z += buf;
        }
        z += "]";
        if ( format[ 0 ] == 'o' )
            z += ",\"o\"";
        z += ")";
        delete[] ba;
    }
    return true;
}
