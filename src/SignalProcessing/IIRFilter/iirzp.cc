/* -*- mode: c++; c-basic-offset: 3; -*- */
#include <vector>
#include <cmath>
#include <iostream>
#include "iirzp.hh"
#include "zp2zp.hh"
#include "constant.hh"

inline double
dB2gain( double x )
{
    return sqrt( exp( M_LN10 * 0.1 * x ) - 1.0 );
}

const static double MACHEP = 1.11022302462515654042E-16; /* 2**-56 */

//--------------------------------------
// butterworth prototype
//--------------------------------------
bool
buttap( int order, int& np, dComplex* pole )
{
    // calc poles
    std::vector< dComplex > ptmp;
    for ( int i = 0; i < ( order + 1 ) / 2; ++i )
    {
        double ang = (double)( 2 * i + 1 ) / ( 2 * order );
        if ( ang == 0.5 )
        {
            ptmp.push_back( dComplex( -1.0 ) );
        }
        else
        {
            double re = -sin( ang * pi );
            double im = cos( ang * pi );
            ptmp.push_back( dComplex( re, im ) );
            ptmp.push_back( dComplex( re, -im ) );
        }
    }

    // copy poles to output array
    int indx = 0;
    for ( std::vector< dComplex >::iterator itr = ptmp.begin( );
          itr != ptmp.end( );
          ++itr )
    {
        pole[ indx ] = *itr;
        ++indx;
    }
    np = ptmp.size( );

    return true;
}

//--------------------------------------
// butterworth
//--------------------------------------
bool
butterzp( Filter_Type type,
          int         order,
          double      f1,
          double      f2,
          int&        nz,
          dComplex*   zero,
          int&        np,
          dComplex*   pole,
          double&     gain )
{

    // get zeros & poles of prototype analog low pass.
    if ( !buttap( order, np, pole ) )
    {
        return false;
    }
    nz = 0; // butterworh prototype has no zeros.
    gain = 1; // always 1 for the butterworth prototype lowpass.

    return zp2zp( type, f1, f2, nz, zero, np, pole, gain );
}

//--------------------------------------
// chebyshev type-I prototype
//--------------------------------------
bool
cheb1ap( int order, double& rp, int& np, dComplex* pole, double& gain )
{

    double eps = dB2gain( rp );

    // calculate poles
    std::vector< dComplex > ptmp;
    double                  sh = sinh( asinh( 1 / eps ) / order );
    double                  ch = cosh( asinh( 1 / eps ) / order );
    for ( int i = 0; i < ( order + 1 ) / 2; ++i )
    {
        double ang = (double)( 2 * i + 1 ) / ( 2 * order );
        if ( ang == 0.5 )
        {
            ptmp.push_back( dComplex( -sh ) );
        }
        else
        {
            double re = -sh * sin( ang * pi );
            double im = ch * cos( ang * pi );
            ptmp.push_back( dComplex( re, im ) );
            ptmp.push_back( dComplex( re, -im ) );
        }
    }

    // copy poles to output array
    int indx = 0;
    for ( std::vector< dComplex >::iterator itr = ptmp.begin( );
          itr != ptmp.end( );
          ++itr )
    {
        pole[ indx ] = *itr;
        ++indx;
    }
    np = ptmp.size( );

    // calc gain
    dComplex gtmp( 1.0 );
    for ( std::vector< dComplex >::iterator itr = ptmp.begin( );
          itr != ptmp.end( );
          ++itr )
    {
        gtmp *= -*itr;
    }
    gain = gtmp.real( );
    if ( !( np & 1 ) )
    {
        gain /= sqrt( 1 + eps * eps );
    }

    return true;
}

//--------------------------------------
// chebyshev type-I
//--------------------------------------
bool
cheby1zp( Filter_Type type,
          int         order,
          double      rp,
          double      f1,
          double      f2,
          int&        nz,
          dComplex*   zero,
          int&        np,
          dComplex*   pole,
          double&     gain )
{

    // get zeros & poles of prototype analog low pass.
    if ( !cheb1ap( order, rp, np, pole, gain ) )
    {
        return false;
    }
    nz = 0;

    return zp2zp( type, f1, f2, nz, zero, np, pole, gain );
}

//--------------------------------------
// chebyshev type-II prototype
//--------------------------------------
bool
cheb2ap( int       order,
         double&   rs,
         int&      nz,
         dComplex* zero,
         int&      np,
         dComplex* pole,
         double&   gain )
{
    double eps = 1 / dB2gain( rs );

    // calc zeros
    std::vector< dComplex > ztmp;
    for ( int i = 0; i < order / 2; ++i )
    {
        double im = 1 / cos( ( 2 * i + 1 ) * pi / ( 2 * order ) );
        ztmp.push_back( dComplex( 0.0, im ) );
        ztmp.push_back( dComplex( 0.0, -im ) );
    }
    nz = ztmp.size( );

    // calc poles
    std::vector< dComplex > ptmp;
    double                  sh = sinh( asinh( 1 / eps ) / order );
    double                  ch = cosh( asinh( 1 / eps ) / order );
    for ( int i = 0; i < order / 2; ++i )
    {
        double ang = double( 2 * i + 1 ) / ( 2 * order );
        double s = -sh * sin( ang * pi );
        double w = ch * cos( ang * pi );
        double re = s / ( s * s + w * w );
        double im = w / ( s * s + w * w );
        ptmp.push_back( dComplex( re, im ) );
        ptmp.push_back( dComplex( re, -im ) );
    }
    if ( order & 1 )
        ptmp.push_back( dComplex( -1 / sh ) );
    np = ptmp.size( );

    // copy zeros to output array
    int      indx = 0;
    dComplex gtmp( 1.0 );
    for ( std::vector< dComplex >::iterator itr = ztmp.begin( );
          itr != ztmp.end( );
          ++itr )
    {
        gtmp /= -*itr;
        zero[ indx ] = *itr;
        ++indx;
    }

    // copy poles to output array
    indx = 0;
    for ( std::vector< dComplex >::iterator itr = ptmp.begin( );
          itr != ptmp.end( );
          ++itr )
    {
        gtmp *= -*itr;
        pole[ indx ] = *itr;
        ++indx;
    }

    gain = real( gtmp );

    return true;
}

//--------------------------------------
// chebyshev type-II
//--------------------------------------
bool
cheby2zp( Filter_Type type,
          int         order,
          double      rs,
          double      f1,
          double      f2,
          int&        nz,
          dComplex*   zero,
          int&        np,
          dComplex*   pole,
          double&     gain )
{

    // get zeros & poles of prototype analog low pass.
    if ( !cheb2ap( order, rs, nz, zero, np, pole, gain ) )
    {
        return false;
    }

    return zp2zp( type, f1, f2, nz, zero, np, pole, gain );
}

//--------------------------------------
// complete elliptic integral of the first kind
//--------------------------------------
double
ellipk( double m )
{

    if ( m < 0.0 || m >= 1.0 || !( m == m ) )
    {
        std::cerr << "ellipk: m=" << m << " out of domain ( 0.0 <= m < 1.0 ) "
                  << std::endl;
        return 0.0;
    }

    // special case;
    if ( m == 0 )
    {
        return piovr2;
    }

    double a0 = 1, a1 = 1;
    double b0 = sqrt( 1 - m );
    double s0 = m;
    double mm = 1;
    int    i = 0;

    while ( mm > MACHEP )
    {
        a1 = ( a0 + b0 ) / 2;
        double b1 = sqrt( a0 * b0 );
        double c1 = ( a0 - b0 ) / 2;
        ++i;
        double w1 = pow( 2.0, i ) * c1 * c1;
        mm = w1;

        s0 += w1;
        a0 = a1;
        b0 = b1;
    }

    return piovr2 / a1;
}

//--------------------------------------
// jacobian elliptic function
//--------------------------------------
bool
ellipj( double u, double m, double& sn, double& cn, double& dn, double& ph )
{
    //--------------------------------  Make sure m in range and m, u not NaN
    if ( m < 0.0 || m > 1.0 || !( m == m && u == u ) )
    {
        std::cerr << "ellipj(u=" << u << ", m=" << m << ") not defined."
                  << std::endl;
        sn = 0.0;
        cn = 0.0;
        ph = 0.0;
        dn = 0.0;
        return false;
    }

    if ( m < 1.0e-9 )
    {
        double t = sin( u );
        double b = cos( u );
        double ai = 0.25 * m * ( u - t * b );
        sn = t - ai * b;
        cn = b + ai * t;
        ph = u - ai;
        dn = 1.0 - 0.5 * m * t * t;
        return true;
    }

    if ( m >= 0.9999999999 )
    {
        double ai = 0.25 * ( 1.0 - m );
        double b = cosh( u );
        double t = tanh( u );
        double phi = 1.0 / b;
        double twon = b * sinh( u );
        sn = t + ai * ( twon - u ) / ( b * b );
        ph = 2.0 * atan( exp( u ) ) - piovr2 + ai * ( twon - u ) / b;
        ai *= t * phi;
        cn = phi - ai * ( twon - u );
        dn = phi + ai * ( twon + u );
        return true;
    }

    /*	A. G. M. scale		*/
    const int max_iter( 10 );
    double    a[ max_iter ], c[ max_iter ];

    a[ 0 ] = 1.0;
    double b = sqrt( 1.0 - m );
    c[ 0 ] = sqrt( m );
    double twon = 1.0;
    int    i = 0;

    while ( fabs( c[ i ] / a[ i ] ) > MACHEP )
    {
        if ( i >= max_iter - 1 )
        {
            break;
        }
        double ai = a[ i ];
        ++i;
        c[ i ] = ( ai - b ) / 2.0;
        double t = sqrt( ai * b );
        a[ i ] = ( ai + b ) / 2.0;
        b = t;
        twon *= 2.0;
    }

    /* backward recurrence */
    double phi = twon * a[ i ] * u;
    do
    {
        double t = c[ i ] * sin( phi ) / a[ i ];
        phi = ( asin( t ) + phi ) / 2.0;
    } while ( --i );

    sn = sin( phi );
    cn = cos( phi );
    dn = sqrt( 1 - m * sn * sn );
    ph = phi;
    return true;
}

//--------------------------------------
//	Incomplete elliptic integral of the first kind
//--------------------------------------
double
ellipf( double phi, double m )
{

    if ( m == 0.0 )
        return ( phi );

    // special case ( m == 1 )
    if ( m == 1.0 )
    {
        if ( fabs( phi ) >= piovr2 )
        {
            std::cerr << "ellipf: singularity error" << std::endl;
            return 0;
        }
        return ( log( tan( ( piovr2 + phi ) / 2.0 ) ) );
    }

    int npio2 = (int)floor( phi / piovr2 );
    if ( npio2 & 1 )
        ++npio2;

    double K = ( npio2 ) ? ellipk( 1.0 - m ) : 0.0;

    phi -= npio2 * piovr2;
    int sign = ( phi < 0.0 ) ? -1 : 1;
    phi = fabs( phi );

    double t = tan( phi );
    if ( fabs( t ) > 10.0 )
    {
        /* Transform the amplitude */
        double e = 1.0 / ( sqrt( 1.0 - m ) * t );
        /* ... but avoid multiple recursions.  */
        if ( fabs( e ) < 10.0 )
        {
            if ( !npio2 )
                K = ellipk( 1.0 - m );
            double ret = K - ellipf( atan( e ), m );
            return sign * ret + npio2 * K;
        }
    }

    double a = 1.0;
    double b = sqrt( 1.0 - m );
    double c = sqrt( m );
    int    d = 1;
    int    mod = 0;
    while ( fabs( c / a ) > MACHEP )
    {
        double tmp = b / a;
        phi = phi + ( atan( t * tmp ) + mod * pi );
        mod = int( ( phi + piovr2 ) / pi );
        t = t * ( 1.0 + tmp ) / ( 1.0 - tmp * t * t );
        c = ( a - b ) / 2.0;
        tmp = sqrt( a * b );
        a = ( a + b ) / 2.0;
        b = tmp;
        d += d;
    }

    return sign * ( atan( t ) + mod * pi ) / ( d * a ) + npio2 * K;
}

/*		cay()
 *
 * Find parameter corresponding to given nome by expansion
 * in theta functions:
 * AMS55 #16.38.5, 16.38.7
 *
 *       1/2
 * ( 2K )                   4     9
 * ( -- )     =  1 + 2q + 2q  + 2q  + ...  =  Theta (0,q)
 * ( pi )                                          3
 *
 *
 *       1/2
 * ( 2K )     1/4       1/4        2    6    12    20
 * ( -- )    m     =  2q    ( 1 + q  + q  + q   + q   + ...) = Theta (0,q)
 * ( pi )                                                           2
 *
 * The nome q(m) = exp( - pi K(1-m)/K(m) ).
 *
 *                                1/2
 * Given q, this program returns m   .
 */
double
cay( double q )
{
    double t1, t2;

    double a = 1.0; // Theta3(0,q)
    double b = 1.0; // Theta2(0,q)/(2 q^(1/4))
    double r = 1.0;
    double p = q;

    do
    {
        r *= p;
        a += 2.0 * r;
        t1 = fabs( r / a );

        r *= p;
        b += r;
        p *= q;
        t2 = fabs( r / b );

    } while ( t1 > MACHEP || t2 > MACHEP );

    return 4.0 * sqrt( q ) *
        ( b * b / ( a * a ) ); /* see above formulas, solved for m */
}

//--------------------------------------
// elliptic analog lowpass filter prototype.
//--------------------------------------
bool
ellipap( int       order,
         double    rp,
         double    rs,
         int&      nz,
         dComplex* zero,
         int&      np,
         dComplex* pole,
         double&   gain )
{

    // special case ( order = 1 )
    if ( order == 1 )
    {
        zero[ 0 ] = dComplex( 0.0, 0.0 );
        pole[ 0 ] = dComplex( -1 / dB2gain( rs ), 0.0 );
        gain = -pole[ 0 ].real( );
        return true;
    }

    double eps = dB2gain( rp );
    double k1 = eps / dB2gain( rs );
    double k1p = sqrt( 1 - k1 * k1 );
    if ( k1p == 1 || k1 == 1 || rs <= 0 )
    {
        std::cerr << "ellipap: Ripple (" << rp << "), Attenuation (" << rs
                  << ") specifications too strict." << std::endl;
        std::cerr << "eps, k1, k1p = " << eps << ", " << k1 << ", " << k1p
                  << std::endl;
        return false;
    }

    // find elliptic paramter, m = k^2.
    double Kk1 = ellipk( k1 * k1 );
    double Kk1p = ellipk( k1p * k1p );
    double k = cay( exp( -pi * Kk1p / ( order * Kk1 ) ) );
    double m = k * k;
    double Kk = ellipk( m );

    // calc zeros
    std::vector< dComplex > ztmp;
    for ( int i = 0; i < order / 2; ++i )
    {
        double s, c, d, p;
        ellipj( ( order - 1 - 2 * i ) * Kk / order, m, s, c, d, p );
        ztmp.push_back( dComplex( 0.0, 1 / ( k * s ) ) );
        ztmp.push_back( dComplex( 0.0, -1 / ( k * s ) ) );
    }
    nz = ztmp.size( );

    // calc poles
    std::vector< dComplex > ptmp;
    double v = ellipf( atan( 1 / eps ), k1p * k1p ) * Kk / ( order * Kk1 );
    double sp, cp, dp, pp;
    ellipj( v, 1 - m, sp, cp, dp, pp );
    for ( int i = 0; i < ( order + 1 ) / 2; ++i )
    {
        double s, c, d, p;
        ellipj( ( order - 1 - 2 * i ) * Kk / order, m, s, c, d, p );
        double denom = 1 - d * d * sp * sp;
        double re = -c * d * sp * cp / denom;
        double im = -s * dp / denom;
        // check real pole
        if ( fabs( im ) < MACHEP * sqrt( re * re + im * im ) )
        {
            ptmp.push_back( dComplex( re, 0.0 ) );
        }
        else
        {
            ptmp.push_back( dComplex( re, im ) );
            ptmp.push_back( dComplex( re, -im ) );
        }
    }
    np = ptmp.size( );

    // copy zeros & poles to output array and calc gain
    int      indx = 0;
    dComplex prodz( 1.0, 0.0 );
    for ( std::vector< dComplex >::iterator itr = ztmp.begin( );
          itr != ztmp.end( );
          ++itr )
    {
        zero[ indx ] = *itr;
        ++indx;
        prodz *= -*itr;
    }

    indx = 0;
    dComplex prodp( 1.0, 0.0 );
    for ( std::vector< dComplex >::iterator itr = ptmp.begin( );
          itr != ptmp.end( );
          ++itr )
    {
        pole[ indx ] = *itr;
        ++indx;
        prodp *= -*itr;
    }

    gain = real( prodp / prodz );
    if ( !( order & 1 ) )
    {
        gain = gain / sqrt( 1 + eps * eps );
    }

    return true;
}

//--------------------------------------
// ellip : generate zeros and poles in s-plane.
//--------------------------------------
bool
ellipzp( Filter_Type type,
         int         order,
         double      rp,
         double      rs,
         double      f1,
         double      f2,
         int&        nz,
         dComplex*   zero,
         int&        np,
         dComplex*   pole,
         double&     gain )
{
    if ( !ellipap( order, rp, rs, nz, zero, np, pole, gain ) )
    {
        return false;
    }
    return zp2zp( type, f1, f2, nz, zero, np, pole, gain );
}

//--------------------------------------
// notchzp : zeros and poles in s-plane for a notch filter.
//--------------------------------------
bool
notchzp( double    f0,
         double    Q,
         double    depth,
         int&      nz,
         dComplex* zero,
         int&      np,
         dComplex* pole )
{

    double d;
    if ( depth )
    {
        d = exp( -log( 10.0 ) * 0.1 * depth );
        if ( d >= 0.5 )
        {
            std::cerr << "notchzp: depth too small ( depth > 3dB )"
                      << std::endl;
            return false;
        }
    }
    else
        d = 0.0;

    if ( Q < 1 )
    {
        std::cerr << "Q too small. ( Q > 1/sqrt( 1 - 2 10^(-depth/10) ) )"
                  << std::endl;
        return false;
    }
    double k2 =
        ( 8 * Q * Q - 1 ) / ( 2 * Q * ( 4 * Q * Q - 1 ) * sqrt( 1 - 2 * d ) );
    if ( k2 > 1 )
    {
        std::cerr << "notchzp: Q > 1/sqrt( 1 - 2 10^(-depth/10) )" << std::endl;
        return false;
    }
    double k1 = sqrt( d ) * k2;

    zero[ 0 ] = dComplex( f0 * -k1, f0 * sqrt( 1 - k1 * k1 ) );
    zero[ 1 ] = dComplex( f0 * -k1, f0 * -sqrt( 1 - k1 * k1 ) );
    pole[ 0 ] = dComplex( f0 * -k2, f0 * sqrt( 1 - k2 * k2 ) );
    pole[ 1 ] = dComplex( f0 * -k2, f0 * -sqrt( 1 - k2 * k2 ) );

    nz = 2;
    np = 2;

    return true;
}

//--------------------------------------
// resgainzp : zeros and poles in s-plane for a resonant gain filter.
//--------------------------------------
bool
resgainzp( double    f0,
           double    Q,
           double    height,
           int&      nz,
           dComplex* zero,
           int&      np,
           dComplex* pole )
{

    double h = exp( log( 10.0 ) * 0.1 * height );
    if ( h <= 2 )
    {
        std::cerr << "resgainzp: height too small ( height > 3dB )"
                  << std::endl;
        return false;
    }
    if ( Q < 1 )
    {
        std::cerr << "resgainzp: Q too small. Q > sqrt( 10^(height/10) - 2 )"
                  << std::endl;
        return false;
    }
    //double k2 = ( 8*Q*Q - 1 ) / ( 2 * Q * ( 4*Q*Q - 1 ) ) * sqrt( 1 - 2/h );
    double k2 =
        ( 8 * Q * Q - 1 ) / ( 2 * Q * ( 4 * Q * Q - 1 ) * sqrt( h - 2 ) );
    double k1 = sqrt( h ) * k2;
    if ( k1 > 1 )
    {
        std::cerr << "resgainzp: Q > sqrt( 10^(height/10) - 2 )" << std::endl;
        return false;
    }

    zero[ 0 ] = dComplex( f0 * -k1, f0 * sqrt( 1 - k1 * k1 ) );
    zero[ 1 ] = dComplex( f0 * -k1, f0 * -sqrt( 1 - k1 * k1 ) );
    pole[ 0 ] = dComplex( f0 * -k2, f0 * sqrt( 1 - k2 * k2 ) );
    pole[ 1 ] = dComplex( f0 * -k2, f0 * -sqrt( 1 - k2 * k2 ) );

    nz = 2;
    np = 2;

    return true;
}
