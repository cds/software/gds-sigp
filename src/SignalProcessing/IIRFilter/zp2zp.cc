#include <cmath>
#include <vector>
#include <strings.h>
#include <iostream>
#include "zp2zp.hh"

// analog prototype low pass to low pass
// w0 : pass band edge
void
lp2lp(
    double w0, int& nz, dComplex* zero, int& np, dComplex* pole, double& gain )
{
    // convert gain
    gain *= pow( w0, np - nz );

    // convert zero
    for ( int i = 0; i < nz; ++i )
    {
        zero[ i ] = w0 * zero[ i ];
    }

    // convert pole
    for ( int i = 0; i < np; ++i )
    {
        pole[ i ] = w0 * pole[ i ];
    }
}

// analog prototype low pass to high pass
// w0 : pass band edge
void
lp2hp(
    double w0, int& nz, dComplex* zero, int& np, dComplex* pole, double& gain )
{

    // convert gain
    dComplex prodz( 1.0, 0.0 );
    dComplex prodp( 1.0, 0.0 );
    for ( int i = 0; i < nz; ++i )
    {
        if ( abs( zero[ i ] ) )
            prodz *= -zero[ i ];
    }
    for ( int i = 0; i < np; ++i )
    {
        if ( abs( pole[ i ] ) )
            prodp *= -pole[ i ];
    }
    gain *= real( prodz / prodp );

    // convert zero
    for ( int i = 0; i < nz; ++i )
    {
        if ( abs( zero[ i ] ) )
            zero[ i ] = dComplex( w0 ) / zero[ i ];
    }

    // convert pole
    for ( int i = 0; i < np; ++i )
    {
        if ( abs( pole[ i ] ) )
            pole[ i ] = dComplex( w0 ) / pole[ i ];
    }

    // process additional zeros or poles
    if ( np > nz )
    {
        for ( int i = nz; i < np; ++i )
        {
            zero[ i ] = 0;
        }
        nz = np;
    }
    else if ( nz > np )
    {
        for ( int i = np; i < nz; ++i )
        {
            zero[ i ] = 0;
        }
        np = nz;
    }
}

// analog prototype low pass to band pass
// w0 : center frequency ( sqrt( w1 w2 ) )
// bw : band width ( w2 - w1 ) where w2 > w1
void
lp2bp( double    w0,
       double    bw,
       int&      nz,
       dComplex* zero,
       int&      np,
       dComplex* pole,
       double&   gain )
{

    // convert gain
    gain *= pow( bw, np - nz );

    // convert zero
    std::vector< dComplex > ztmp;
    for ( int i = 0; i < nz; ++i )
    {
        if ( abs( zero[ i ] ) )
        {
            dComplex sol1 = 0.5 * zero[ i ] * bw;
            dComplex sol2 = 0.5 *
                sqrt( zero[ i ] * zero[ i ] * bw * bw -
                      dComplex( 4 * w0 * w0 ) );
            ztmp.push_back( sol1 + sol2 );
            ztmp.push_back( sol1 - sol2 );
        }
        else
        {
            ztmp.push_back( 0.0 );
        }
    }

    // convert pole
    std::vector< dComplex > ptmp;
    for ( int i = 0; i < np; ++i )
    {
        if ( abs( pole[ i ] ) )
        {
            dComplex sol1 = 0.5 * pole[ i ] * bw;
            dComplex sol2 = 0.5 *
                sqrt( pole[ i ] * pole[ i ] * bw * bw -
                      dComplex( 4 * w0 * w0 ) );
            ptmp.push_back( sol1 + sol2 );
            ptmp.push_back( sol1 - sol2 );
        }
        else
        {
            ptmp.push_back( 0.0 );
        }
    }

    // process additional zeros or poles
    if ( np > nz )
    {
        for ( int i = 0; i < np - nz; ++i )
        {
            ztmp.push_back( 0.0 );
        }
    }
    else if ( nz > np )
    {
        for ( int i = 0; i < nz - np; ++i )
        {
            ptmp.push_back( 0.0 );
        }
    }

    // copy converted zeros to output array
    int indx = 0;
    for ( std::vector< dComplex >::iterator itr = ztmp.begin( );
          itr != ztmp.end( );
          ++itr )
    {
        zero[ indx ] = *itr;
        ++indx;
    }
    nz = ztmp.size( );

    // copy converted poles to output array
    indx = 0;
    for ( std::vector< dComplex >::iterator itr = ptmp.begin( );
          itr != ptmp.end( );
          ++itr )
    {
        pole[ indx ] = *itr;
        ++indx;
    }
    np = ptmp.size( );
}

// analog prototype low pass to band stop
// w0 : center frequency ( sqrt( w1 w2 ) )
// bw : band width ( w2 - w1 ) where w2 > w1
void
lp2bs( double    w0,
       double    bw,
       int&      nz,
       dComplex* zero,
       int&      np,
       dComplex* pole,
       double&   gain )
{

    // convert gain
    dComplex prodz( 1.0, 0.0 );
    dComplex prodp( 1.0, 0.0 );
    for ( int i = 0; i < nz; ++i )
    {
        if ( abs( zero[ i ] ) )
            prodz *= -zero[ i ];
    }
    for ( int i = 0; i < np; ++i )
    {
        if ( abs( pole[ i ] ) )
            prodp *= -pole[ i ];
    }
    gain *= real( prodz / prodp );

    // convert zero
    std::vector< dComplex > ztmp;
    for ( int i = 0; i < nz; ++i )
    {
        if ( abs( zero[ i ] ) )
        {
            dComplex sol1 = dComplex( 0.5 * bw ) / zero[ i ];
            dComplex sol2 = 0.5 *
                sqrt( dComplex( bw * bw ) / ( zero[ i ] * zero[ i ] ) -
                      dComplex( 4 * w0 * w0 ) );
            ztmp.push_back( sol1 + sol2 );
            ztmp.push_back( sol1 - sol2 );
        }
        else
        {
            ztmp.push_back( 0.0 );
        }
    }

    // convert pole
    std::vector< dComplex > ptmp;
    for ( int i = 0; i < np; ++i )
    {
        if ( abs( pole[ i ] ) )
        {
            dComplex sol1 = dComplex( 0.5 * bw ) / pole[ i ];
            dComplex sol2 = 0.5 *
                sqrt( dComplex( bw * bw ) / ( pole[ i ] * pole[ i ] ) -
                      dComplex( 4 * w0 * w0 ) );
            ptmp.push_back( sol1 + sol2 );
            ptmp.push_back( sol1 - sol2 );
        }
        else
        {
            ptmp.push_back( 0.0 );
        }
    }

    // process additional zeros or poles
    if ( np > nz )
    {
        for ( int i = 0; i < np - nz; ++i )
        {
            ztmp.push_back( dComplex( 0.0, w0 ) );
            ztmp.push_back( dComplex( 0.0, -w0 ) );
        }
    }
    else if ( nz > np )
    {
        for ( int i = 0; i < nz - np; ++i )
        {
            ptmp.push_back( dComplex( 0.0, w0 ) );
            ptmp.push_back( dComplex( 0.0, -w0 ) );
        }
    }

    // copy converted zeros to output array
    int indx = 0;
    for ( std::vector< dComplex >::iterator itr = ztmp.begin( );
          itr != ztmp.end( );
          ++itr )
    {
        zero[ indx ] = *itr;
        ++indx;
    }
    nz = ztmp.size( );

    // copy converted poles to output array
    indx = 0;
    for ( std::vector< dComplex >::iterator itr = ptmp.begin( );
          itr != ptmp.end( );
          ++itr )
    {
        pole[ indx ] = *itr;
        ++indx;
    }
    np = ptmp.size( );
}

bool
zp2zp( Filter_Type type,
       double      f1,
       double      f2,
       int&        nz,
       dComplex*   zero,
       int&        np,
       dComplex*   pole,
       double&     gain )
{
    if ( f1 > f2 )
    {
        double tmp = f2;
        f2 = f1;
        f1 = tmp;
    }

    double w0 = 0; // analog frequency
    double bw = 0; // band width
    switch ( type )
    {
    case kLowPass:
    case kHighPass: {
        w0 = f2;
        break;
    }
    case kBandPass:
    case kBandStop: {
        bw = f2 - f1;
        w0 = sqrt( f1 * f2 );
        break;
    }
    default: {
        std::cerr << "Filter Type Error." << std::endl;
        return false;
    }
    }

    // convert prototype zeros & poles of analog low pass
    // to selected type analog zeros & poles.
    switch ( type )
    {
    case kLowPass: {
        lp2lp( w0, nz, zero, np, pole, gain );
        break;
    }
    case kHighPass: {
        lp2hp( w0, nz, zero, np, pole, gain );
        break;
    }
    case kBandPass: {
        lp2bp( w0, bw, nz, zero, np, pole, gain );
        break;
    }
    case kBandStop: {
        lp2bs( w0, bw, nz, zero, np, pole, gain );
        break;
    }
    default: {
        std::cerr << "Filter Type Error." << std::endl;
        return false;
    }
    }

    return true;
}

bool
getFilterType( const char* type_name, Filter_Type& type )
{

    if ( strcasecmp( type_name, "lowpass" ) == 0 )
    {
        type = kLowPass;
    }
    else if ( strcasecmp( type_name, "highpass" ) == 0 )
    {
        type = kHighPass;
    }
    else if ( strcasecmp( type_name, "bandpass" ) == 0 )
    {
        type = kBandPass;
    }
    else if ( strcasecmp( type_name, "bandstop" ) == 0 )
    {
        type = kBandStop;
    }
    else
    {
        return false;
    }

    return true;
}

std::string
getFilterString( Filter_Type type )
{
    switch ( type )
    {
    case kLowPass:
        return "LowPass";
    case kHighPass:
        return "HighPass";
    case kBandPass:
        return "BandPass";
    case kBandStop:
        return "BandStop";
    }
    return "";
}
