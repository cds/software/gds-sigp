/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "IIRFilter.hh"
#include "DVecType.hh"
#include <cmath>
//#include <iostream>

//using namespace std;

//======================================  Filter settling time.
Interval
settle_time( const IIRFilter& p, double f )
{
    IIRFilter* filt = p.clone( );
    filt->reset( );
    double fSample = filt->getFSample( );
    size_t nSample = 2 * fSample;
    DVectD dvd;
    dvd.Extend( nSample );
    Time   t0( 0 );
    double rMax = 0.0;
    double rTest = 0;
    Time   rLast( 0 );
    while ( rLast + Interval( 1.0 ) > t0 )
    {
        if ( rMax == 0.0 )
            dvd[ 0 ] = 1.0;
        else
            dvd[ 0 ] = 0.0;
        TSeries tin( t0, Interval( 1.0 / fSample ), dvd );
        // tin.Dump(cout);
        t0 += Interval( 2 );
        TSeries tout = ( *filt )( tin );
        //tout.Dump(cout);
        const DVectD& repv = dynamic_cast< const DVectD& >( *tout.refDVect( ) );
        for ( size_t i = 0; i < nSample; i++ )
        {
            double rabs = fabs( repv[ i ] );
            if ( rabs > rMax )
            {
                rMax = rabs;
                rTest = rabs * f;
            }
            if ( rabs >= rTest )
                rLast = tout.getBinT( i );
        }
    }
    return rLast.totalS( );
}
