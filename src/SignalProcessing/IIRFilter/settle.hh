/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef SETTLE_HH
#define SETTLE_HH
class IIRFilter;
class Interval;

/**  settle_samples measures the time before the response of an IIR filter  
  *  to a delta function drops below a specified fraction (1e-6 by default) 
  *  of the maximum filter response.
  *  \brief Filter settling time.
  */
Interval settle_time( const IIRFilter& p, double f = 1e-6 );

#endif // !defined(SETTLE_HH)
