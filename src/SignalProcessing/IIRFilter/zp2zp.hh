//
// Reference : Digital Filter Design (T.W. Parks & C.S. Burrus)
//
// 7/17/2002
// Masahiro Ito
// Univerisity of Oregon
// masahiro@darkwing.uoregon.edu
//

#ifndef zp2zp_HH
#define zp2zp_HH

#include "FilterBase.hh"
#include "Complex.hh"
#include <string>

/** \ingroup sigp_iir_design
  * \{
  */

/** Convert a prototype low pass filter with 1 rad/sec cutoff to 
  * low pass with cut off at w0 rad/sec.
  * @memo Convert a prototype low pass filter to low pass.
  * @param w0 Cut off frequency (rad/sec).
  * @param nz Number of poles.
  * @param zero Array for poles (rad/sec).
  * @param np Number of poles.
  * @param pole Array for poles (rad/sec).
  * @param gain Filter gain.
  */
void lp2lp(
    double w0, int& nz, dComplex* zero, int& np, dComplex* pole, double& gain );

/** Convert a prototype low pass filter with 1 rad/sec cutoff to 
  * high pass with cut off at w0 rad/sec.
  * @memo Convert a prototype low pass filter to high pass.
  * @param w0 Cut off frequency (rad/sec).
  * @param nz Number of poles.
  * @param zero Array for poles (rad/sec).
  * @param np Number of poles.
  * @param pole Array for poles (rad/sec).
  * @param gain Filter gain.
  */
void lp2hp(
    double w0, int& nz, dComplex* zero, int& np, dComplex* pole, double& gain );

/** Convert a prototype low pass filter with 1 rad/sec cutoff to 
  * band pass with center frequency at w0 rad/sec and band width, 
  * bw (rad/sec).
  * @memo Convert a prototype low pass filter to band pass.
  * @param w0 Center frequency (rad/sec).
  * @param bw Band width
  * @param nz Number of poles.
  * @param zero Array for poles (rad/sec).
  * @param np Number of poles.
  * @param pole Array for poles (rad/sec).
  * @param gain Filter gain.
  */
void lp2bp( double    w0,
            double    bw,
            int&      nz,
            dComplex* zero,
            int&      np,
            dComplex* pole,
            double&   gain );

/** Convert a prototype low pass filter with 1 rad/sec cutoff to 
  * band stop with specified center frequency and band width.
  * @memo Convert a prototype low pass filter to band stop.
  * @param w0 Center frequency (rad/sec).
  * @param bw Band width
  * @param nz Number of poles.
  * @param zero Array for poles (rad/sec).
  * @param np Number of poles.
  * @param pole Array for poles (rad/sec).
  * @param gain Filter gain.
  */
void lp2bs( double    w0,
            double    bw,
            int&      nz,
            dComplex* zero,
            int&      np,
            dComplex* pole,
            double&   gain );

/** Convert prototype low pass filter to the specified filter type.
  * @memo Convert prototype filter to the specified filter type.
  * @param type Filter type (#Filter_Type enumerator)
  * @param f1 Pass band edge (Hz).
  * @param f2 Another pass band edge (Hz).
  * @param nz Number of poles.
  * @param zero Array for poles (rad/sec).
  * @param np Number of poles.
  * @param pole Array for poles (rad/sec).
  * @param gain Filter gain.
  * @return True if successful.
  */
bool zp2zp( Filter_Type type,
            double      f1,
            double      f2,
            int&        nz,
            dComplex*   zero,
            int&        np,
            dComplex*   pole,
            double&     gain );

/**  Convert case insensitive filter type name string to enumerated 
  *  Filter_Type. Valid filter type name strings are:
  *  - "LowPass"
  *  - "HighPass"
  *  - "BandPass"
  *  - "BandStop"
  *
  *  @memo Convert filter type string to enumerated filter type.
  *  @param type_name Case insensitive type name string.
  *  @param type Filter type (returned as #Filter_Type enumerator)
  *  @return True if successful.
  */
bool getFilterType( const char* type_name, Filter_Type& type );

/** Convert an enumerated filter type to a filter type name string.
  * @memo Convert an enumerated filter type to a name string.
  * @param type Filter type (Filter_Type enumerator)
  * @return Filter type name string.
  */
std::string getFilterString( Filter_Type type );

/**
 * \}
 */
#endif
