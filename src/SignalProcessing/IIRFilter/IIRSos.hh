/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef IIRSos_HH
#define IIRSos_HH

#include "Complex.hh"

/**  The IIRSos class is for initializing and manipulating 
  *  IIR filters implemented as second order sections. THe constructor (and 
  *  initializers allow the user to specify most combinations of 0 - 2 real 
  *  or complex poles and zeros.Scond order sectons can be constructed 
  *  individually and added to an IIRFilter or they may be constructed 
  *  automatically using the IIRFilter design utilities (see the FilterDesign 
  *  and IIRdesign documentation for further details).
  *
  * A second order section is define as:
  *
  * \f[
     H(z) = \frac{b_0 + b_1z^{-1} + b_2z^{-2}}
                 {1 + a_1z^{-1} + a_2z^{-2}}
     \f]
  *
  * @memo Second order IIR filter section.
  * @author  Edward Daw
  * @version 20th May 2001; 
  */
class IIRSos
{
public:
    enum ev_mode
    { ///< Select the mode for speed and precision
        sos_df2, ///< Direct form II
        sos_lnf, ///< Low-noise form (Transposed direct form II)
        sos_lnf2, ///< Low-noise form II
        sos_hpc ///< High precision calculations.
    };

public:
    /**  Empty constructor
      *  @memo Default constructor
      */
    IIRSos( void );

    /**  Constructor using same conventions as CDS filter files.
      *  @memo CDS coefficient constructor.
      *  @param gain Overall gain factor.
      *  @param c1   1st coefficient.
      *  @param c2   2nd coefficient.
      *  @param c3   3rd coefficient.
      *  @param c4   4th coefficient.
      */
    IIRSos( double gain, double c1, double c2, double c3, double c4 );

    /**  Constructor from second order section coefficients compatible
      *  with matlab (Note that the gain is not factored out so \c a0!=1 ).
      *  @memo matlab coefficient constructor.
      *  @param b0  b0 coefficient (see equation above)
      *  @param b1  b1 coefficient
      *  @param b2  b2 coefficient
      *  @param a0  a0 coefficient
      *  @param a1  a1 coefficient
      *  @param a2  a2 coefficient
      */
    IIRSos( double b0, double b1, double b2, double a0, double a1, double a2 );

    /**  Constructor from complex conjugate zero pair and complex conjugate
      *  pole pair
      *  @memo Complex zero pair + complex pole pair constructor.
      *  @param zero Complex zero
      *  @param pole Complex pole
      *  @param fs   Sampling frequency.
      */
    IIRSos( dComplex zero, dComplex pole, double fs );

    /**  Constructor from two real zeros and a complex conjugate pole pair
      *  @memo Two zeros + complex pole pair constructor.
      *  @param z0    First real zero
      *  @param z1    Second real zero
      *  @param poles Complex pole
      *  @param fs    Sampling frequency.
      */
    IIRSos( double z0, double z1, dComplex poles, double fs );

    /**  Constructor from a single real zero and a complex conjugate pole pair.
      *  @memo Single real zero + complex pole pair constructor.
      *  @param z     Real zero
      *  @param poles Complex poles
      *  @param fs    Sampling frequency.
      */
    IIRSos( double z, dComplex poles, double fs );

    /**  Constructor from a complex conjugate pair of roots
      *  @memo Nzeros + Complex pair of roots constructor
      *  @param nzeros Number f zeros
      *  @param roots  Complex roots
      *  @param fs     Sampling frequency.
      */
    IIRSos( int nzeros, dComplex roots, double fs );

    /**  Constructor from a complex conjugate zero pair and two real poles
      *  @memo Complex zero pair + two real poles
      *  @param zeros Complex zero (conjugate pair)
      *  @param p0    First real pole
      *  @param p1    Second real pole
      *  @param fs    Sample rate (Hz)
      */
    IIRSos( dComplex zeros, double p0, double p1, double fs );

    /**  Constructor from a complex conjugate zero pair and a single real zero
      *  @memo Complex zero pair + one real pole
      *  @param zeros Complex zero (conjugate pair)
      *  @param p     Real pole
      *  @param fs    Sample rate (Hz)
      */
    IIRSos( dComplex zeros, double p, double fs );

    /**  Constructor from two real poles and two real zeros
      *  @memo Two real zeros + two real poles constructor.
      *  @param nzeros Number of zeros.
      *  @param z0     First real zero
      *  @param z1     Second real zero
      *  @param p0     First real pole
      *  @param p1     Second real pole
      *  @param fs     Sample rate (Hz)
      */
    IIRSos( int nzeros, double z0, double z1, double p0, double p1, double fs );

    /**  Constructor from 3 real roots, 1 or 2 zeros and 2 or 1 poles
      *  @memo Constructor from 3 real roots
      *  @param nzeros Number of zeros.
      *  @param r0     First root
      *  @param r1     Second root
      *  @param r2     Third root
      *  @param fs     Sample rate (Hz)
      */
    IIRSos( int nzeros, double r0, double r1, double r2, double fs );

    /**  Constructor from 2 real roots
      *  @memo Constructor from 2 real roots
      *  @param nzeros Number of zeros.
      *  @param r0     First root
      *  @param r1     Second root
      *  @param fs     Sample rate (Hz)
      */
    IIRSos( int nzeros, double r0, double r1, double fs );

    /**  Constructor from a single real root
      *  @memo Constructor from 1 real roots
      *  @param nzeros Number of zeros.
      *  @param r      Real root
      *  @param fs     Sample rate (Hz)
      */
    IIRSos( int nzeros, double r, double fs );

    /**  Destroy and IIRSos object and free any allocated storage.
      *  @memo IIRSos destructor
      */
    virtual ~IIRSos( void );

    /**  Initialize with cds compatible coefficients
      *  @memo CDS coefficient initializer.
      *  @param gain Overall gain factor.
      *  @param c1   1st coefficient.
      *  @param c2   2nd coefficient.
      *  @param c3   3rd coefficient.
      *  @param c4   4th coefficient.
      *  @return Zero on success, -1 on error
      */
    int init( double gain, double c1, double c2, double c3, double c4 );

    /**  Initialize with matlab compatible second order section coefficients
      *  @memo matlab coefficient initializer.
      *  @param b0  b0 coefficient (see equation above)
      *  @param b1  b1 coefficient
      *  @param b2  b2 coefficient
      *  @param a0  a0 coefficient
      *  @param a1  a1 coefficient
      *  @param a2  a2 coefficient
      *  @return Zero on success, -1 on error
      */
    int
    init( double b0, double b1, double b2, double a0, double a1, double a2 );

    /**  Initialize section with complex conjugate pole pair and
      *  complex conjugate zero pair. 
      *  @memo Complex zero pair + complex pole pair initializer.
      *  @param zeros Complex zero
      *  @param poles Complex pole
      *  @param fs    Sampling frequency.
      *  @return Zero on success, -1 on error
      */
    int init( dComplex zeros, dComplex poles, double fs );

    /**  Initialize from a complex conjugate zero pair and two real poles
      *  @memo Complex zero pair + two real poles initializer
      *  @param zero Complex zero (conjugate pair)
      *  @param p0   First real pole
      *  @param p1   Second real pole
      *  @param fs   Sample rate (Hz)
      *  @return Zero on success, -1 on error
      */
    int init( dComplex zero, double p0, double p1, double fs );

    /**  Initialize from a complex conjugate zero pair and one real pole
      *  @memo Complex zero pair + one real pole initializer
      *  @param zero Complex zero (conjugate pair)
      *  @param p    Real pole
      *  @param fs   Sample rate (Hz)
      *  @return Zero on success, -1 on error
      */
    int init( dComplex zero, double p, double fs );

    /**  Initialize from a complex conjugate pair of zeros or a complex
      *  conjugate pair of poles. If \a nzeros is 1, \a rootpair is assumed 
      *  to be a complex zero pair and a pair of poles is added at the 
      *  Nyquist frequency. If \a nzeros is 0, \a rootpair is assumed to be 
      *  a complex pole pair. Any other value of \a nzeros results in an error.
      *  @memo Nzeros + Complex pair of roots initializer
      *  @param nzeros   Number of zeros
      *  @param rootpair Complex root pair.
      *  @param fs       Sampling frequency.
      *  @return Zero on success, -1 on error
      */
    int init( int nzeros, dComplex rootpair, double fs );

    /**  Initialize from a single real zero and a complex conjugate pole pair
      *  @memo Single real zero + complex pole pair initializer
      *  @param z    Real zero
      *  @param pole Complex poles
      *  @param fs   Sampling frequency.
      *  @return Zero on success, -1 on error
      */
    int init( double z, dComplex pole, double fs );

    /**  Initialize from a pair of real zeros and a complex conjugate pole pair
      *  @memo Two real zeros + complex pole pair initializer
      *  @param z0   First real zero
      *  @param z1   Second real zero
      *  @param pole Complex pole
      *  @param fs   Sampling frequency.
      *  @return Zero on success, -1 on error
      */
    int init( double z0, double z1, dComplex pole, double fs );

    /**  Initialize from a pair of real zeros and a pair of real poles. 
      *  \a nzeros must be set to 2.
      *  @memo Two real zeros + two real poles initializer.
      *  @param nzeros Number of zeros (must be 2).
      *  @param z0     First real zero
      *  @param z1     Second real zero
      *  @param p0     First real pole
      *  @param p1     Second real pole
      *  @param fs     Sample rate (Hz)
      *  @return Zero on success, -1 on error
      */
    int
    init( int nzeros, double z0, double z1, double p0, double p1, double fs );

    /**  Initialize from three real roots. The number of zeros (\a nzeros) 
      *  determines the assignment of \a r1 and \a r2. The permitted values 
      *  are:
      *  <ul>
      *    <li>\c nzeros=1: \a r0 is a real zero and \a r1 and \a r2 are real 
      *           poles.</li>
      *    <li>\c nzeros=2: \a r0 and \a r1 are real zeros and \a r2 is real 
      *           poles. An additional real pole is inserted at fNyquist.</li>
      *  </ul>
      *  @memo Initialize from 3 real roots
      *  @param nzeros Number of zeros.
      *  @param r0     First root
      *  @param r1     Second root
      *  @param r2     Third root
      *  @param fs     Sample rate (Hz)
      *  @return Zero on success, -1 on error
      */
    int init( int nzeros, double r0, double r1, double r2, double fs );

    /**  Initialize from two real roots. The number of zeros (\a nzeros) 
      *  determines the assignment of \a r0 and \a r1. The permitted values 
      *  are:
      *  <ul>
      *    <li>\c nzeros=0: \a r0 and \a r1 are real poles.</li>
      *    <li>\c nzeros=1: \a r0 is a real zero and \a r1 is a real pole </li>
      *    <li>\c nzeros=2: \a r0 and \a r1 are real zeros. Two real poles  
      *           are inserted at fNyquist to avoid a divergent transfer 
      *           function.</li>
      *  </ul>
      *  @memo Initialize from 2 real roots
      *  @param nzeros Number of zeros.
      *  @param r0     First root
      *  @param r1     Second root
      *  @param fs     Sample rate (Hz)
      *  @return Zero on success, -1 on error
      */
    int init( int nzeros, double r0, double r1, double fs );

    /**  Initialize from a single real root, either zero or pole. The number 
      *  of zeros (\a nzeros) determines the assignment of \a r. The permitted 
      *  values for \a nzeros are:
      *  <ul>
      *    <li>\c nzeros=0: \a r is a real pole.</li>
      *    <li>\c nzeros=1: \a r is a real zero and a real pole is inserted
      *           at fNyquist to avoid a divergent transfer function.</li>
      *  </ul>

      *  @memo Initialize from 1 real root
      *  @param nzeros Number of zeros.
      *  @param r      Real root
      *  @param fs     Sample rate (Hz)
      *  @return Zero on success, -1 on error
      */
    int init( int nzeros, double r, double fs );

    /**  Dump filter coefficients, history buffers, buffer counter
      *  @memo Dump SOS state
      *  @param output Output stream
      *  @return 0 on success, otherwise -1
      */
    int dump( std::ostream& output ) const;

    /**  Apply the filter to a single double precision sample.
      *  @memo Apply filter to a single sample
      *  @param data_in  Input sample
      *  @param data_out Pointer to output data buffer.
      *  @return Zero on success otherwise -1.
      */
    int apply( double data_in, double* data_out );

    /**  Apply filter to an array of doubles in place
      *  @memo Apply filter to a double array.
      *  @param pdata Pointer to input (and output) data buffer
      *  @param ndata Number of samples to filter.
      *  @return Zero on success otherwise -1.
      */
    int apply( double* pdata, int ndata );

    /**  Apply filter to an array of floats in place
      *  @memo Apply filter to a float array.
      *  @param pdata Pointer to input (and output) data buffer
      *  @param ndata Number of samples to filter.
      *  @return Zero on success otherwise -1.
      */
    int apply( float* pdata, int ndata );

    /**  Reset history buffers to zero.
      *  @memo Reset operator.
      */
    void reset( void );

    /**  Select the evaluation mode for this sos. At present, the implemented 
      *  modes are Direct Form II (sos_df2) and the Low noise form. By default 
      *  the Direct Form II evaluation function is used. This is the 
      *  traditional mode, although it tends to result in a noisier output 
      *  signal in many cases. 
      *  \brief Setlect evaluation mode.
      *  \param mode SOS ealuation mode.
      */
    void select_eval( ev_mode mode );

    /**  Test history state for infinite / NaN values. Once the history is
      *  non-finite, the Sos output will remain invalid until it is reset.
      *  @memo Test for infinite/NaN state.
      *  @return True if the state is invalid.
      */
    bool test( void ) const;

    /**  Evaluate H(z^{-1})
      *  @memo Evaluate H
      *  @param iz Inverse of z.
      *  @return Comples value of H(iz)
      */
    dComplex H( const dComplex& iz ) const;

    /**  Get b0 coefficient
      *  @memo b0 coefficient/
      *  @return b0 coefficient.
      */
    double
    B0( void ) const
    {
        return b0;
    }

    /**  Get b1 coefficient
      *  @memo b1 coefficient/
      *  @return b1 coefficient.
      */
    double
    B1( void ) const
    {
        return b1;
    }

    /**  Get b2 coefficient
      *  @memo b2 coefficient/
      *  @return b2 coefficient.
      */
    double
    B2( void ) const
    {
        return b2;
    }

    /**  Get a1 coefficient
      *  @memo a1 coefficient/
      *  @return a1 coefficient.
      */
    double
    A1( void ) const
    {
        return a1;
    }

    /**  Get a2 coefficient
      *  @memo a2 coefficient/
      *  @return a2 coefficient.
      */
    double
    A2( void ) const
    {
        return a2;
    }

private:
    // true if the coefficients are loaded
    bool _section_loaded;
    // Evaliation mode
    ev_mode _evmode;
    // design sampling period
    bool   _period_known;
    double _period;
    // history buffers
    double _hzminusone;
    double _hzminustwo;
    // filter coefficients
    double a1, a2, b1, b2, b0;
    // number of s plane poles, zeros represented in second order section.
    unsigned int _npoles;
    unsigned int _nzeros;
};

#endif // define IIRSos_HH
