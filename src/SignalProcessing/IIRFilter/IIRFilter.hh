/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef IIRFilter_HH
#define IIRFilter_HH

#include "Pipe.hh"
#include "Complex.hh"
#include "IIRSos.hh"
#include <vector>
#include <iosfwd>

class FSeries;

/** IIR Filter implements digital IIR (Infinite Impulse Response) filters.
  * An object of class IIRFilter is constructed by specifying the 
  * positions of the filter poles and zeros in the S plane and the sampling
  * rate of the data. Any TSeries object can be filtered. Note that 
  * error trapping to spot gaps between successive TSeries inputs applied
  * to the same filter is not yet implemented.
  * @memo Digital IIR filter implementation API
  * @author  Edward J. Daw  (edaw@lsuligo.phys.lsu.edu)
  * @version 1.0; Last modified: June 9, 2001
  */
class IIRFilter : public Pipe
{

public:
    /// Complex vector data type definition
    typedef std::vector< dComplex > cplx_vect;

    /// Complex vector iterator definition
    typedef cplx_vect::iterator cplx_iter;

    /// Complex vector constant iterator definition
    typedef cplx_vect::const_iterator const_cplx_iter;

    /// Real vector data type  definition
    typedef std::vector< double > dble_vect;

    /// Real vector iterator definition
    typedef dble_vect::const_iterator const_dble_iter;

    ///  Second order section vector data type definition
    typedef std::vector< IIRSos > sos_vect;

    ///  Second order section vector iterator definition
    typedef sos_vect::iterator sos_iter;

    ///  Second order section vector constant iterator definition
    typedef sos_vect::const_iterator const_sos_iter;

public:
    using Pipe::apply;
    using Pipe::dataCheck;

    /** Default constructor. No coefficient or history storage is allocated.
      * @memo Default constructor.
      */
    IIRFilter( void );

    /** Constructor. Makes a unity operator.
      * @param design_sample_rate The sampling rate of the channel to be
      * filtered, in Hz.
      * @memo Default constructor.
      */
    explicit IIRFilter( double design_sample_rate );

    /** Constructor from S plane poles and zeros. The pole and zero positions
      * are written to two arrays of type dComplex. For example, to initialize
      * a filter with two poles at 10Hz and no zeros, you could type:
      * \verbatim
        dComplex pole0(-10,0), pole1(-10,0); 
	dComplex poles[2] = {pole0, pole1};
        IIRFilter myfilter(2, poles, 0, NULL, 256);
	\endverbatim
      * This IIRFilter myfilter could be applied to any channel with a 256
      * Hz sampling rate. The gain of the filter is set to 1, so that
      * the transfer function is
      * \f[
        H(s) = \frac{(s - s_1)(s - s_2) \cdots}
	            {(s - p_1)(s - p_2) \cdots}
	\f]
      * @param npoles The number of poles. 
      * @param pole An array of type dComplex containing the list of poles.
      * Note that where a complex pole is in the list, its complex conjugate
      * must also be in the list. The convention for root specification is
      * the same as for Matlab, except that matlab expresses s plane values
      * in \f$\mathrm{rad s^{-1}}\f$ whereas here s plane values are in Hz. 
      * So to enter
      * Matlab generated poles and zeros with this software, divide real and
      * imaginary parts by \f$2 \pi\f$.
      * @param nzeros The number of zeros.
      * @param zero An array of type dComplex containing the list of zeros. 
      * See comment above for pole specification.
      * @param design_sample_rate The sampling rate of the channel to be
      * filtered, in Hz.
      * @memo Constructor from poles and zeros
      */
    IIRFilter( unsigned int    npoles,
               const dComplex* pole,
               unsigned int    nzeros,
               const dComplex* zero,
               double          design_sample_rate );

    /** Constructor from S plane poles and zeros. The pole and zero positions
      * are written to two arrays of type dComplex. For example, to initialize
      * a filter with two poles at 10Hz and no zeros, you could type:
      * \verbatim
        dComplex pole0(-10,0), pole1(-10,0);
        dComplex poles[2] = {pole1, pole2};
        IIRFilter myfilter(2, poles, 0, NULL, 256);
	\endverbatim
      * This IIRFilter myfilter could be applied to any channel with a 256
      * Hz sampling rate. 
      * @memo Constructor from poles and zeros
      * @param npoles The number of poles. 
      * @param pole An array of type dComplex containing the list of poles.
      * Note that where a complex pole is in the list, its complex conjugate
      * must also be in the list. The convention for root specification is
      * the same as for Matlab, except that matlab expresses s plane values
      * in \f$\mathrm{rad s^{-1}}\f$ whereas here s plane values are in Hz. 
      * So to enter
      * Matlab generated poles and zeros with this software, divide real and
      * imaginary parts by \f$2 \pi\f$.
      * @param nzeros The number of zeros.
      * @param zero An array of type dComplex containing the list of zeros. 
      * See comment above for pole specification.
      * @param gain The gain of the filter. 
      * @param design_sample_rate The sampling rate of the channel to be
      * filtered, in Hz.
      */
    IIRFilter( unsigned int    npoles,
               const dComplex* pole,
               unsigned int    nzeros,
               const dComplex* zero,
               double          design_sample_rate,
               double          gain );

    /**  Add another IIRFilter to this one
      *  @memo Combine filters
      *  @param f IIR filter to be combined with this.
      *  @return Reference to this filter.
      */
    IIRFilter& operator*=( const IIRFilter& f );

    /**  Add a single second order section to the IIR filter
      *  @memo Add a second order section.
      *  @param sos Second order section to add to this filter.
      *  @return Reference to this filter.
      */
    IIRFilter& operator*=( const IIRSos& sos );

    /**  Multiply filter gain by a numerical factor
      *  @memo Scale filter gain.
      *  @param gainfactor Fator to multiply into the current filter gain.
      *  @return Reference to this filter.
      */
    IIRFilter& operator*=( double gainfactor );

    /**  This function sets the pole and zero frequencies of the filter and
      *  the filter gain. All values are in Hz. To input compute poles and 
      *  zeros from s plane parameters, divide the s plane parameters (poles 
      *  and zeros) by \f$2\pi\f$. See documentation for constructors for 
      *  details on how to initialize filters.
      *  @memo Initialize filter from lists of poles/zeros
      *  @param npoles Number of poles
      *  @param pole   Pointer to an array of poles. 
      *  @param nzeros Number of zeros
      *  @param zero   Pointer to an array of zeros. 
      *  @param design_sample_rate Sample rate
      *  @param gain   Gain factor.
      */
    void init( unsigned int    npoles,
               const dComplex* pole,
               unsigned int    nzeros,
               const dComplex* zero,
               double          design_sample_rate,
               double          gain );

    /**  Print the values of the second order section coefficients.
      *  @memo Print Sos coefficients.
      *  @param output Output stream
      *  @return Zero on success. 
      */
    int dumpSosData( std::ostream& output ) const;

    /**  Print the positions of the S plane poles and zeros in rad s^-1.
      *  @memo Print the root values
      *  @param output Output stream
      *  @return Zero on success. 
      */
    int dumpSPlaneRoots( std::ostream& output ) const;

    /**  Destroy the IIRFilter and free the history buffers.
      *  @memo Destructor.
      */
    virtual ~IIRFilter( void );

    /**  Clone an IIR Filter
      *  @memo Clone the filter.
      *  @returns Pointer to the newly created copy.
      */
    IIRFilter* clone( void ) const;

    /**  Apply filter to a timeseries. No error trapping for gaps between
      *  successive timeseries filtered. Note that a filter should always be
      *  applied to the same channel.
      *  @return Filtered TSeries.
      *  @memo Apply filter to a timeseries.
      *  @param data_in Input time series.
      */
    TSeries apply( const TSeries& data_in );

    /**  Check data before filtering. The start time must be equal to the 
      *  last processed time, the sample rates must be equal and the input
      *  data must be finite. If the data do not satisfy these criteria
      *  an exception is thrown.
      *  @memo Check data integrity
      *  @param data_in Input time series.
      */
    void dataCheck( const TSeries& data_in ) const;

    /** Clear history buffers, clear error flag.
      * @memo Reset the filter.
      */
    void reset( void );

    /**  Test history state of all second-order sections. Return true if 
      *  one or more is invalid (i.e. not finite);
      *  @memo Test the filter state.
      *  @return true If the filter state is not finite.
      */
    bool test( void ) const;

    /** See if filter coefficients have been set and history buffers created.
      * @return 1 (true) if yes, 0 (false) if no
      * @memo Check filter for coefficients, history buffers.
      */
    bool inUse( void ) const;

    /** Get data start time
      * @return Start time for first data set on which IIR filter applied.
      * @memo Get data start time.
      */
    Time getStartTime( void ) const;

    /** Get data current time
      * @return Current time for first data set on which IIR filter applied.
      * @memo Get start time for current data.
      */
    Time getCurrentTime( void ) const;

    /** Get filter order. Implemented for pole/zero designs.
      * The filter order is defined as max(number of zeros,number of poles)
      * @memo Filter order.
      * @return Order of filter
      */
    unsigned getOrder( void ) const;

    /** Check whether the filter has an s-plane representation.
      * @memo Does filter have an s-plane representation.
      * @returns True if the filter has an s-plane representation.
      */
    bool
    hasSRepresentation( void ) const
    {
        return _have_s_representation;
    }

    /** Get real pole list.
      * @memo Real pole list.
      * @return Real pole list
      */
    const dble_vect&
    getRealPoles( void ) const
    {
        return _real_pole;
    }

    /** Get complex pole list.
      * @memo Complex pole list.
      * @return Complex pole list
      */
    const cplx_vect&
    getComplexPoles( void ) const
    {
        return _pole_pair;
    }

    /** Get real zero list.
      * @memo Real zero list.
      * @return Real zero list
      */
    const dble_vect&
    getRealZeros( void ) const
    {
        return _real_zero;
    }

    /** Get complex zero list.
      * @memo Complex zero list.
      * @return Complex zero list
      */
    const cplx_vect&
    getComplexZeros( void ) const
    {
        return _zero_pair;
    }

    /** Get gain.
      * @memo Gain.
      * @return gain
      */
    double
    getGain( void ) const
    {
        return _filter_gain;
    }

    /** Get sampling rate.
      * @memo Sampling rate .
      * @return Sampling rate
      */
    double
    getFSample( void ) const
    {
        return _sample_rate;
    }

    /** Check if filter is invertible.
      * @memo Check if filter is invertible.
      * @return True if the filter is invertible.
      */
    bool
    isInvertible( void ) const
    {
        return _filter_invertible;
    }

    /** Get SOS list.
      * @memo SOS list.
      * @return SOS list
      */
    const sos_vect& getSOS( void ) const;

    /**  Select the evaluation mode for all second order sections in this 
      *  filter. The default mode is sos_df2 (direct form II) which is 
      *  relatively noisy for low frequencies and high sample rates. The
      *  sos_lnf (lower noise form or transposed DF2) has better noise 
      *  characteristics when filtering low frequencies with high sample rates.
      *  @memo Set sos evaluation 
      *  @param mode Evaluation mode
      */
    void setSosMode( IIRSos::ev_mode mode );

    /**  Set the s-Plane representation.
     */
    void setSPlaneRep( unsigned int   nZero,
                       const dComplex zeros[],
                       unsigned int   nPole,
                       const dComplex poles[] );

protected:
    /** The transfer coefficient of the filter at the specified 
      * frequency is calculated and returned as a complex number. 
      * Filters that support a fast way to compute a transfer coefficient 
      * should implement this method and return true.
      * @memo Get a transfer coefficent of a Filter.
      * @param coeff a complex number representing the Filter response 
      *              at the specified frequency (return)
      * @param f Frequency at which to sample the transfer function.
      * @return true if successful       
      */
    bool xfer( fComplex& coeff, double f ) const;

private:
    // whether the filter has been initialized with poles, zeros
    bool _in_use;
    bool _have_s_representation;

    // the order of the filter
    unsigned _order;

    // vectors of poles sorted into real and imaginary sets
    cplx_vect _pole_pair;
    dble_vect _real_pole;
    // vectors of zeros sorted into real and imaginary sets
    cplx_vect _zero_pair;
    dble_vect _real_zero;

    // the overall filter gain
    double _filter_gain;
    // design sample rate for the filter
    double _sample_rate;
    // true if filter can be inverted by interchanging poles and zeros,
    // which it can if there are no zeros with positive real parts
    bool _filter_invertible;

    // a vector containing the second order sections
    sos_vect _sos;

    // time when filter first applied to data. Flag that is set to
    // true after _time_first_used has been assigned a value
    Time _time_first_used;
    bool _first_data_processed;
    // end time of last data set to which filter was applied
    Time _time_last_used;

    // this function sorts the roots into real and imaginary sets, checks that
    // for every complex root there is a complex conjugate root, and checks that
    // all poles have a negative real part. Also, labels filters with zeros
    // having a positive real part as invertible. Finally, multiples all
    // pole and zero values by 2*pi, converting from Hz to rad s^-1.
    int sortRoots( char type, unsigned int nroots, const dComplex* root );

    // this function makes a vector of second order sections from the vectors
    // of real and non-real poles and zeros
    int initializeSOS( void );
};

#ifndef __CINT__

inline unsigned
IIRFilter::getOrder( void ) const
{
    return _order;
}

inline const IIRFilter::sos_vect&
IIRFilter::getSOS( void ) const
{
    return _sos;
}

inline bool
IIRFilter::inUse( void ) const
{
    return _in_use;
}

#endif // define __CINT__
#endif // define IIRFilter_HH
