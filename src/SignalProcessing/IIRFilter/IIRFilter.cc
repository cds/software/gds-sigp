/* -*- mode: c++; c-basic-offset: 4; -*- */
#include <time.h>
#include <iostream>
#include <sstream>
#include <algorithm>
#include "DVector.hh"
#include "IIRSos.hh"
#include "IIRFilter.hh"
#include "FSeries.hh"
#include "Interval.hh"
#include "TSeries.hh"
#include "constant.hh"

using namespace std;

//======================================  Constructors
IIRFilter::IIRFilter( void )
    : _in_use( false ), _have_s_representation( false ), _order( 0 ),
      _filter_gain( 1 ), _sample_rate( 0 ), _filter_invertible( false ),
      _time_first_used( 0 ), _first_data_processed( false ),
      _time_last_used( 0 )
{
}

IIRFilter::IIRFilter( double design_sample_rate )
    : _in_use( true ), _have_s_representation( true ), _order( 0 ),
      _filter_gain( 1 ), _sample_rate( design_sample_rate ),
      _filter_invertible( true ), _time_first_used( 0 ),
      _first_data_processed( false ), _time_last_used( 0 )
{
}

IIRFilter::IIRFilter( unsigned int    npoles,
                      const dComplex* pole,
                      unsigned int    nzeros,
                      const dComplex* zero,
                      double          design_sample_rate )
    : _in_use( false ), _have_s_representation( false ), _order( 0 ),
      _filter_gain( 1 ), _sample_rate( 0 ), _filter_invertible( false ),
      _time_first_used( 0 ), _first_data_processed( false ),
      _time_last_used( 0 )
{
    double gain( 1.0 );
    init( npoles, pole, nzeros, zero, design_sample_rate, gain );
}

IIRFilter::IIRFilter( unsigned int    npoles,
                      const dComplex* pole,
                      unsigned int    nzeros,
                      const dComplex* zero,
                      double          design_sample_rate,
                      double          gain )
    : _in_use( false ), _have_s_representation( false ), _order( 0 ),
      _filter_gain( 1 ), _sample_rate( 0 ), _filter_invertible( false ),
      _time_first_used( 0 ), _first_data_processed( false ),
      _time_last_used( 0 )
{
    init( npoles, pole, nzeros, zero, design_sample_rate, gain );
}

//======================================  Initializers
void
IIRFilter::init( unsigned int    npoles,
                 const dComplex* pole,
                 unsigned int    nzeros,
                 const dComplex* zero,
                 double          design_sample_rate,
                 double          gain )
{
    // note that the sortRoots function now multiplies all pole and
    // zero values by 2 pi.

    // set filter gain
    _filter_gain = gain;

    // sort poles and zeros, check for unphysicals
    bool poles_ok( false );
    if ( sortRoots( 'p', npoles, pole ) < 0 )
    {
        std::cerr << " -while sorting pole list with sortRoots." << std::endl;
    }
    else
    {
        poles_ok = true;
    }

    bool zeros_ok( false );
    if ( sortRoots( 'z', nzeros, zero ) < 0 )
    {
        std::cerr << " -while sorting zero list with sortRoots." << std::endl;
    }
    else
    {
        zeros_ok = true;
    }

    _in_use = poles_ok && zeros_ok;

    // make second order sections
    _sample_rate = design_sample_rate;

    if ( _in_use )
    {
        _filter_invertible = true;
        _first_data_processed = false;
        if ( ( initializeSOS( ) ) < 0 )
        {
            std::cerr << " -while initializing second"
                      << " order sections with initializeSOS." << std::endl;
            _in_use = false;
        }
        _order = npoles > nzeros ? npoles : nzeros;
    }

    return;
}

IIRFilter&
IIRFilter::operator*=( const IIRFilter& f )
{
    if ( !f._in_use )
    {
        return *this;
    }
    if ( !_in_use )
    {
        _in_use = true;
        _filter_gain = 1;
        _sample_rate = f._sample_rate;
        _have_s_representation = f._have_s_representation;
    }
    //------------  Note that if gain is set on an unused filter, the _in_use
    //              status is set but the sample rate is left as zero.
    else if ( _sample_rate == 0 )
    {
        _sample_rate = f._sample_rate;
    }

    if ( f._sample_rate == 0.0 )
    {
        _filter_gain *= f._filter_gain;
        return *this;
    }

    if ( _sample_rate != f._sample_rate )
    {
        ostringstream msg;
        msg << "IIRFilter: Can not combine filters with unequal sample rates ("
            << _sample_rate << ", " << f._sample_rate << ")." << endl;
        throw runtime_error( msg.str( ) );
    }
    if ( _have_s_representation && f._have_s_representation )
    {
        copy( f._pole_pair.begin( ),
              f._pole_pair.end( ),
              back_inserter( _pole_pair ) );
        copy( f._real_pole.begin( ),
              f._real_pole.end( ),
              back_inserter( _real_pole ) );
        copy( f._zero_pair.begin( ),
              f._zero_pair.end( ),
              back_inserter( _zero_pair ) );
        copy( f._real_zero.begin( ),
              f._real_zero.end( ),
              back_inserter( _real_zero ) );
        _order = max( _real_pole.size( ) + 2 * _pole_pair.size( ),
                      _real_zero.size( ) + 2 * _zero_pair.size( ) );
    }
    else
    {
        _have_s_representation = false;
    }
    _filter_gain *= f._filter_gain;
    _filter_invertible = ( _filter_invertible && f._filter_invertible );

    for ( const_sos_iter i = f._sos.begin( ); i != f._sos.end( ); ++i )
    {
        _sos.push_back( *i );
    }
    // reset all history buffers on new second order section
    reset( );
    return *this;
}

IIRFilter&
IIRFilter::operator*=( const IIRSos& sos )
{
    if ( !_in_use )
    {
        _in_use = true;
        _filter_gain = 1;
    }
    _have_s_representation = false;
    _sos.push_back( sos );
    // reset all history buffers on new second order section
    reset( );
    return *this;
}

IIRFilter&
IIRFilter::operator*=( double gainfactor )
{
    if ( !_in_use )
    {
        _in_use = true;
        _filter_gain = 1;
        _have_s_representation = true;
    }
    _filter_gain *= gainfactor;
    return *this;
}

IIRFilter*
IIRFilter::clone( ) const
{
    return new IIRFilter( *this );
}

int
IIRFilter::dumpSosData( std::ostream& output ) const
{
    for ( const_sos_iter sec = _sos.begin( ); sec < _sos.end( ); ++sec )
    {
        sec->dump( output );
    }
    output << "total of " << _sos.size( ) << " second order sections."
           << std::endl;
    return 0;
}

int
IIRFilter::dumpSPlaneRoots( std::ostream& output ) const
{

    output << "gain: " << _filter_gain << std::endl;
    if ( _have_s_representation )
    {
        for ( const_cplx_iter cpole = _pole_pair.begin( );
              cpole != _pole_pair.end( );
              ++cpole )
        {
            output << "complex pole " << (int)( cpole - _pole_pair.begin( ) )
                   << " : " << *cpole << " , " << conj( *cpole ) << std::endl;
        }
        for ( const_dble_iter rpole = _real_pole.begin( );
              rpole != _real_pole.end( );
              ++rpole )
        {
            output << "real pole " << (int)( rpole - _real_pole.begin( ) )
                   << " : " << *rpole << std::endl;
        }
        for ( const_cplx_iter czero = _zero_pair.begin( );
              czero != _zero_pair.end( );
              ++czero )
        {
            output << "complex zero " << (int)( czero - _zero_pair.begin( ) )
                   << " : " << *czero << " , " << conj( *czero ) << std::endl;
        }
        for ( const_dble_iter rzero = _real_zero.begin( );
              rzero != _real_zero.end( );
              ++rzero )
        {
            output << "real zero " << (int)( rzero - _real_zero.begin( ) )
                   << " : " << *rzero << std::endl;
        }
    }
    else
    {
        std::cout << "No s-plane representation available for filter."
                  << std::endl;
    }
    return 0;
}

IIRFilter::~IIRFilter( void )
{
    _in_use = false;
    _first_data_processed = false;
}

// function to apply IIR filter

TSeries
IIRFilter::apply( const TSeries& data_in )
{
    if ( _in_use && !data_in.isEmpty( ) )
    {

        // Check data
        dataCheck( data_in );

        // update timestamps
        if ( !_first_data_processed )
        {
            _time_first_used = data_in.getStartTime( );
            _first_data_processed = true;
        }
        _time_last_used = data_in.getEndTime( );

        // copy data into an in container
        TSeries databuffer( data_in );

        // extract the number of samples
        size_t nsamples( databuffer.getNSample( ) );

        if ( data_in.refDVect( )->getType( ) == DVector::t_float )
        {
            // make a pointer to the data array
            float* data_array = (float*)databuffer.refData( );

            // filter data in place
            for ( sos_iter sos = _sos.begin( ); sos != _sos.end( ); ++sos )
            {
                sos->apply( data_array, (int)nsamples );
            }
        }
        else
        {
            // make a pointer to the data array
            databuffer.Convert( DVector::t_double );
            double* data_array = (double*)databuffer.refData( );

            // filter data in place
            for ( sos_iter sos = _sos.begin( ); sos != _sos.end( ); ++sos )
            {
                sos->apply( data_array, (int)nsamples );
            }
        }

        // multiply by overall filter gain
        if ( _filter_gain != 1.0 )
            databuffer *= _filter_gain;

        if ( test( ) )
            throw runtime_error( "IIRFilter: Invalid (infinite) state." );
        return databuffer;
    }

    //----------------------------------  Return input for null filter.
    else
    {
        return data_in;
    }
}

//======================================  Check data.
void
IIRFilter::dataCheck( const TSeries& data_in ) const
{
    if ( _time_last_used != Time( 0 ) )
    {
        if ( _time_last_used != data_in.getStartTime( ) )
        {
            throw runtime_error( "IIRFilter::dataCheck: Invalid start time." );
        }
        if ( fabs( _sample_rate * double( data_in.getTStep( ) ) - 1.0 ) > 1e-6 )
        {
            throw runtime_error( "IIRFilter::dataCheck: Invalid sample rate." );
        }
    }
}

//======================================  Reset the filter state
void
IIRFilter::reset( void )
{
    for ( sos_iter sos = _sos.begin( ); sos != _sos.end( ); ++sos )
    {
        sos->reset( );
    }
    _time_first_used = Time( 0 );
    _first_data_processed = false;
    _time_last_used = Time( 0 );
}

bool
IIRFilter::test( void ) const
{
    for ( const_sos_iter sos = _sos.begin( ); sos != _sos.end( ); ++sos )
    {
        if ( sos->test( ) )
            return true;
    }
    return false;
}

Time
IIRFilter::getStartTime( void ) const
{
    if ( !_in_use )
    {
        cerr << "WARNING: request for first used time on uninitialized filter"
             << endl;
    }
    return _time_first_used;
}

Time
IIRFilter::getCurrentTime( void ) const
{
    if ( !_in_use )
    {
        cerr << "WARNING: last data time requested for uninitialized filter"
             << endl;
    }
    return _time_last_used;
}

bool
IIRFilter::xfer( fComplex& coeff, double f ) const
{
    dComplex iz;
    iz = polar< double >( 1, -twopi * f / _sample_rate );
    dComplex temp( _filter_gain, 0 );

    for ( const_sos_iter sos = _sos.begin( ); sos != _sos.end( ); ++sos )
    {
        temp *= sos->H( iz );
    }

    coeff = temp;
    return true;
}

int
IIRFilter::sortRoots( char type, unsigned int nroots, const dComplex* root )
{

    // get roots with a non-zero imaginary part first

    for ( unsigned int root_count = 0; root_count < nroots; ++root_count )
    {

        if ( root[ root_count ].imag( ) > 0 )
        {

            // check the root hasn't already been added to the root sorted list
            bool root_preexists( false );

            if ( type == 'z' )
            {
                for ( cplx_iter zpair = _zero_pair.begin( );
                      zpair < _zero_pair.end( );
                      ++zpair )
                {
                    if ( ( *zpair ) == root[ root_count ] )
                    {
                        root_preexists = true;
                    }
                }
            }
            else if ( type == 'p' )
            {
                for ( cplx_iter ppair = _pole_pair.begin( );
                      ppair < _pole_pair.end( );
                      ++ppair )
                {
                    if ( ( *ppair ) == root[ root_count ] )
                    {
                        root_preexists = true;
                    }
                }
            }
            else
            {
                std::cerr << "ERROR: improper root category" << std::endl;
                return -1;
            }

            if ( !root_preexists )
            {

                if ( ( real( root[ root_count ] ) ) > 0 )
                {
                    if ( type == 'p' )
                    {
                        cerr << "ERROR: pole cannot have positive real part"
                             << std::endl;
                        return -1;
                    }
                    else if ( type == 'z' )
                    {
                        std::cout << "WARNING: noninvertible filter"
                                  << std::endl;
                        _filter_invertible = false;
                    }
                    else
                    {
                        cerr << "ERROR: improper root category" << std::endl;
                        return -1;
                    }
                }

                unsigned int n_instances_root( 0 );
                unsigned int n_instances_conjugate( 0 );

                for ( unsigned int j = 0; j < nroots; ++j )
                {
                    if ( root[ j ] == root[ root_count ] )
                    {
                        ++n_instances_root;
                    }
                    if ( root[ j ] == conj( root[ root_count ] ) )
                    {
                        ++n_instances_conjugate;
                    }
                }

                if ( n_instances_root == n_instances_conjugate )
                {
                    if ( type == 'z' )
                    {
                        for ( unsigned int k = 0; k < n_instances_root; ++k )
                        {
                            _zero_pair.push_back( 2 * pi * root[ root_count ] );
                        }
                    }
                    else if ( type == 'p' )
                    {
                        for ( unsigned int k = 0; k < n_instances_root; ++k )
                        {
                            _pole_pair.push_back( 2 * pi * root[ root_count ] );
                        }
                    }
                    else
                    {
                        cerr << "ERROR: improper root category" << endl;
                        return -1;
                    }
                }
                else
                {
                    cerr << "ERROR: filter maps real input to complex output"
                         << endl;
                    return -1;
                }
            }
        }

        // for real roots
        else if ( imag( root[ root_count ] ) == 0 )
        {
            if ( type == 'z' )
            {
                if ( real( root[ root_count ] ) > 0 )
                {
                    std::cout << "WARNING: noninvertable filter" << std::endl;
                    _filter_invertible = false;
                }
                _real_zero.push_back( 2 * pi * real( root[ root_count ] ) );
            }
            else if ( type == 'p' )
            {
                if ( real( root[ root_count ] ) > 0 )
                {
                    std::cerr << "ERROR: pole cannot have positive real part"
                              << std::endl;
                    return -1;
                }
                _real_pole.push_back( 2 * pi * real( root[ root_count ] ) );
            }
            else
            {
                std::cerr << "ERROR: impropor root category" << std::endl;
                return -1;
            }
        }
    }
    _have_s_representation = true;
    return 0;
}

int
IIRFilter::initializeSOS( void )
{

    // make internal holders for poles and zeros that it is OK
    // to empty
    cplx_vect polepair( _pole_pair );
    cplx_vect zeropair( _zero_pair );
    dble_vect realpole( _real_pole );
    dble_vect realzero( _real_zero );

    // this conditional loops until the maximum number of second order
    // sections consisting of a pole pair and a zero pair which both
    // have nonzero imaginary parts has been made.

    while ( !( polepair.empty( ) || zeropair.empty( ) ) )
    {

        // make a second order section with a a complex conjugate
        // pair of poles and a complex conjugate pair of zeros

        const IIRSos buffer( zeropair.back( ), polepair.back( ), _sample_rate );
        _sos.push_back( buffer );
        zeropair.pop_back( );
        polepair.pop_back( );
    }

    // this conditional executes if after the last while loop has
    // made all the second order sections it can, there are still
    // remaining complex poles

    while ( !polepair.empty( ) )
    {

        // since the while loop above exited, and there are remaining
        // non-real pole pairs, there must be no remaining non-real
        // zero pairs. Therefore start making second order sections
        // using the real zeros. First case is when there are 2 or more
        // zeros, then  make a second order section out of a remaining
        // pole pair and two of the real zeros.

        if ( realzero.size( ) >= 2 )
        {

            // make a second order section with a complex conjugate
            // pair of poles and two real zeros

            double z1( realzero.back( ) );
            realzero.pop_back( );
            const IIRSos buffer(
                z1, realzero.back( ), polepair.back( ), _sample_rate );
            _sos.push_back( buffer );
            realzero.pop_back( );
            polepair.pop_back( );
        }

        else if ( realzero.size( ) == 1 )
        {

            // make a second order section out of a complex conjugate
            // pair of poles and one real zero

            const IIRSos buffer(
                realzero.back( ), polepair.back( ), _sample_rate );
            _sos.push_back( buffer );
            realzero.pop_back( );
            polepair.pop_back( );
        }

        else if ( realzero.empty( ) )
        {

            // make a second order section out of a complex conjugate
            // pair of poles only

            const IIRSos buffer( (int)0, polepair.back( ), _sample_rate );
            _sos.push_back( buffer );
            polepair.pop_back( );
        }
    }

    // this conditional executes if there are residual non real
    // complex conjugate zero pairs

    while ( !zeropair.empty( ) )
    {

        // make a second order section out of a complex conjugate
        // pair of zeros and two real poles

        if ( realpole.size( ) >= 2 )
        {

            // make a second order section with a complex conjugate
            // pair of poles and two real zeros

            double p1( realpole.back( ) );
            realpole.pop_back( );
            const IIRSos buffer(
                zeropair.back( ), p1, realpole.back( ), _sample_rate );
            _sos.push_back( buffer );
            realpole.pop_back( );
            zeropair.pop_back( );
        }

        else if ( realpole.size( ) == 1 )
        {

            // make a second order section out of a complex conjugate
            // pair of poles and one real zero

            const IIRSos buffer(
                zeropair.back( ), realpole.back( ), _sample_rate );
            _sos.push_back( buffer );
            zeropair.pop_back( );
            realpole.pop_back( );
        }

        else if ( realpole.empty( ) )
        {

            // make a second order section out of a complex conjugate
            // pair of poles only

            const IIRSos buffer( (int)1, zeropair.back( ), _sample_rate );
            _sos.push_back( buffer );
            zeropair.pop_back( );
        }
    }

    // this conditional executes until all remaining real zeros
    // and real poles have been exhausted

    while ( !( realpole.empty( ) && realzero.empty( ) ) )
    {

        if ( ( realpole.size( ) >= 2 ) && ( realzero.size( ) >= 2 ) )
        {

            // make a second order section out of two real poles and
            // two real zeros

            double p1( realpole.back( ) );
            realpole.pop_back( );
            double z1( realzero.back( ) );
            realzero.pop_back( );
            const IIRSos buffer( (int)2,
                                 z1,
                                 realzero.back( ),
                                 p1,
                                 realpole.back( ),
                                 _sample_rate );
            _sos.push_back( buffer );
            realpole.pop_back( );
            realzero.pop_back( );
        }

        else if ( ( realpole.size( ) >= 2 ) && ( realzero.size( ) == 1 ) )
        {

            // make a second order section out of two real poles and
            // the last remaining real zero

            double p1( realpole.back( ) );
            realpole.pop_back( );
            const IIRSos buffer(
                (int)1, realzero.back( ), p1, realpole.back( ), _sample_rate );
            _sos.push_back( buffer );
            realzero.pop_back( );
            realpole.pop_back( );
        }

        else if ( ( realpole.size( ) >= 2 ) && realzero.empty( ) )
        {

            // make a second order section out of two real poles

            double p1( realpole.back( ) );
            realpole.pop_back( );
            const IIRSos buffer( (int)0, p1, realpole.back( ), _sample_rate );
            _sos.push_back( buffer );
            realpole.pop_back( );
        }

        else if ( ( realpole.size( ) == 1 ) && ( realzero.size( ) >= 2 ) )
        {

            // make a second order section out of one real pole
            // and two real zeros

            double z1( realzero.back( ) );
            realzero.pop_back( );
            const IIRSos buffer(
                (int)2, z1, realzero.back( ), realpole.back( ), _sample_rate );
            _sos.push_back( buffer );
            realzero.pop_back( );
            realpole.pop_back( );
        }

        else if ( realpole.empty( ) && ( realzero.size( ) >= 2 ) )
        {

            // make a second order section out of two real zeros

            double z1( realzero.back( ) );
            realzero.pop_back( );
            const IIRSos buffer( (int)2, z1, realzero.back( ), _sample_rate );
            _sos.push_back( buffer );
            realzero.pop_back( );
        }

        else if ( ( realpole.size( ) == 1 ) && ( realzero.size( ) == 1 ) )
        {

            // make a second order section out of one real pole and
            // one real zero

            const IIRSos buffer(
                (int)1, realzero.back( ), realpole.back( ), _sample_rate );
            _sos.push_back( buffer );
            realzero.pop_back( );
            realpole.pop_back( );
        }

        else if ( ( realpole.size( ) == 1 ) && realzero.empty( ) )
        {

            // make a second order section out of one real pole

            const IIRSos buffer( (int)0, realpole.back( ), _sample_rate );
            _sos.push_back( buffer );
            realpole.pop_back( );
        }

        else if ( realpole.empty( ) && ( realzero.size( ) == 1 ) )
        {

            // make a second order section out of one real zero

            const IIRSos buffer( (int)1, realzero.back( ), _sample_rate );
            _sos.push_back( buffer );
            realzero.pop_back( );
        }
    }
    return 0;
}

//======================================  Set evaluation mode
void
IIRFilter::setSosMode( IIRSos::ev_mode mode )
{
    size_t N = _sos.size( );
    for ( size_t i = 0; i < N; i++ )
    {
        _sos[ i ].select_eval( mode );
    }
}

void
IIRFilter::setSPlaneRep( unsigned int   nZero,
                         const dComplex zeros[],
                         unsigned int   nPole,
                         const dComplex poles[] )
{
    sortRoots( 'p', nPole, poles );
    sortRoots( 'z', nZero, zeros );
}
