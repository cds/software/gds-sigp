/* -*- mode: c++; c-basic-offset: 3; -*- */
#include <cmath>
#include "lcl_array.hh"
#include "IIRdesign.hh"
#include "iirutil.hh"
#include "iirzp.hh"
#include "zp2zp.hh"
#include "polyroot.hh"
#include "constant.hh"
#include <string.h>
#include <vector>
#include <algorithm>
#include <stdexcept>
#include <iostream>

inline double
fwarp( double f, double fs )
{
    double fsop = fs / pi;
    return fsop * tan( f / fsop );
}

IIRFilter
butter(
    Filter_Type type, int order, double fs, double f1, double f2, bool prewarp )
{
    if ( fs <= 0 )
    {
        throw std::invalid_argument( "Sampling frequency must be positive" );
    }
    // pre-warping
    if ( prewarp )
    {
        f1 = fwarp( f1, fs );
        f2 = fwarp( f2, fs );
    }

    lcl_array< dComplex > zero( 2 * order );
    lcl_array< dComplex > pole( 2 * order );
    int                   nz, np;
    double                gain;
    if ( !butterzp( type, order, f1, f2, nz, zero, np, pole, gain ) )
    {
        throw std::runtime_error( "Unable to construct butterworth filter" );
    }

    // correct gain ( Hz to rad/s )
    gain *= pow( twopi, np - nz );

    return zpk( fs, nz, zero, np, pole, gain, "f", false );
}

IIRFilter
cheby1( Filter_Type type,
        int         order,
        double      rp,
        double      fs,
        double      f1,
        double      f2,
        bool        prewarp )
{
    if ( fs <= 0 )
    {
        throw std::invalid_argument( "Sampling frequency must be positive" );
    }
    // pre-warping
    if ( prewarp )
    {
        f1 = fwarp( f1, fs );
        f2 = fwarp( f2, fs );
    }

    lcl_array< dComplex > zero( 2 * order );
    lcl_array< dComplex > pole( 2 * order );
    int                   nz, np;
    double                gain;
    if ( !cheby1zp( type, order, rp, f1, f2, nz, zero, np, pole, gain ) )
    {
        throw std::runtime_error( "Unable to construct cheby1 filter" );
    }

    // correct gain ( Hz to rad/s )
    gain *= pow( twopi, np - nz );
    return zpk( fs, nz, zero, np, pole, gain, "f", false );
}

IIRFilter
cheby2( Filter_Type type,
        int         order,
        double      rs,
        double      fs,
        double      f1,
        double      f2,
        bool        prewarp )
{
    if ( fs <= 0 )
    {
        throw std::invalid_argument( "Sampling frequency must be positive" );
    }
    // pre-warping
    if ( prewarp )
    {
        f1 = fwarp( f1, fs );
        f2 = fwarp( f2, fs );
    }

    lcl_array< dComplex > zero( 2 * order );
    lcl_array< dComplex > pole( 2 * order );
    int                   nz, np;
    double                gain;
    if ( !cheby2zp( type, order, rs, f1, f2, nz, zero, np, pole, gain ) )
    {
        throw std::runtime_error( "Unable to construct cheby2 filter" );
    }

    // correct gain ( Hz to rad/s )
    gain *= pow( twopi, np - nz );

    return zpk( fs, nz, zero, np, pole, gain, "f", false );
}

IIRFilter
ellip( Filter_Type type,
       int         order,
       double      rp,
       double      rs,
       double      fs,
       double      f1,
       double      f2,
       bool        prewarp )
{
    if ( fs <= 0 )
    {
        throw std::invalid_argument( "Sampling frequency must be positive" );
    }
    // pre-warping
    if ( prewarp )
    {
        f1 = fwarp( f1, fs );
        f2 = fwarp( f2, fs );
    }

    lcl_array< dComplex > zero( 2 * order );
    lcl_array< dComplex > pole( 2 * order );
    int                   nz, np;
    double                gain;
    if ( !ellipzp( type, order, rp, rs, f1, f2, nz, zero, np, pole, gain ) )
    {
        throw std::runtime_error(
            "ellip: Unable to construct elliptic filter" );
    }

    // correct gain ( Hz to rad/s )
    gain *= pow( twopi, np - nz );

    return zpk( fs, nz, zero, np, pole, gain, "f", false );
}

IIRFilter
notch( double fs, double f0, double Q, double depth, bool prewarp )
{
    if ( fs <= 0 )
    {
        throw std::invalid_argument( "Sampling frequency must be positive" );
    }
    // pre-warping
    if ( prewarp )
    {
        f0 = fwarp( f0, fs );
    }

    dComplex zero[ 2 ];
    dComplex pole[ 2 ];
    int      nz, np;
    if ( !notchzp( f0, Q, depth, nz, zero, np, pole ) )
    {
        throw std::runtime_error( "notch: Unable to construct notch filter" );
    }

    return zpk( fs, nz, zero, np, pole, 1, "f", false );
}

IIRFilter
resgain( double fs, double f0, double Q, double height, bool prewarp )
{
    if ( fs <= 0 )
    {
        throw std::invalid_argument( "Sampling frequency must be positive" );
    }
    // pre-warping
    if ( prewarp )
    {
        f0 = fwarp( f0, fs );
    }

    dComplex zero[ 2 ];
    dComplex pole[ 2 ];
    int      nz, np;
    if ( !resgainzp( f0, Q, height, nz, zero, np, pole ) )
    {
        throw std::runtime_error( "Unable to construct resgain filter" );
    }

    return zpk( fs, nz, zero, np, pole, 1, "f", false );
}

IIRFilter
comb( double fs, double f0, double Q, double amp, int N, bool prewarp )
{
    if ( fs <= 0 )
    {
        throw std::invalid_argument( "Sampling frequency must be positive" );
    }
    if ( !N )
    {
        N = int( fs / ( 2 * f0 ) );
        if ( N && f0 * N >= fs / 2 )
            --N;
    }
    else if ( f0 * N >= fs / 2 )
    {
        throw std::invalid_argument(
            "Max frequency greater than Nyquist frequency" );
    }

    lcl_array< dComplex >   zero( 2 * N );
    lcl_array< dComplex >   pole( 2 * N );
    std::vector< dComplex > ztmp;
    std::vector< dComplex > ptmp;
    ztmp.reserve( 2 * N );
    ptmp.reserve( 2 * N );
    int nz, np;
    for ( int i = 0; i < N; ++i )
    {

        double f = f0 * ( i + 1 );
        // pre-warping
        if ( prewarp )
        {
            f = fwarp( f, fs );
        }

        if ( amp <= 0.0 )
        {
            if ( notchzp( f, Q, -amp, nz, zero, np, pole ) )
            {

                ztmp.push_back( zero[ 0 ] );
                ztmp.push_back( zero[ 1 ] );
                ptmp.push_back( pole[ 0 ] );
                ptmp.push_back( pole[ 1 ] );
            }
            else
            {
                throw std::runtime_error( "Unable to construct comb filter" );
            }
        }
        else
        {
            if ( resgainzp( f, Q, amp, nz, zero, np, pole ) )
            {

                ztmp.push_back( zero[ 0 ] );
                ztmp.push_back( zero[ 1 ] );
                ptmp.push_back( pole[ 0 ] );
                ptmp.push_back( pole[ 1 ] );
            }
            else
            {
                throw std::runtime_error( "Unable to construct comb filter" );
            }
        }
    }

    int indx = 0;
    for ( std::vector< dComplex >::iterator itr = ztmp.begin( );
          itr != ztmp.end( );
          ++itr )
    {
        zero[ indx ] = *itr;
        ++indx;
    }
    nz = ztmp.size( );

    indx = 0;
    for ( std::vector< dComplex >::iterator itr = ptmp.begin( );
          itr != ptmp.end( );
          ++itr )
    {
        pole[ indx ] = *itr;
        ++indx;
    }
    np = ptmp.size( );

    return zpk( fs, nz, zero, np, pole, 1, "f", false );
}

IIRFilter
pole( double fs, double f0, double gain, const char* plane, bool prewarp )
{
    if ( !plane || ( strlen( plane ) != 1 ) ||
         ( strchr( "sfn", plane[ 0 ] ) == 0 ) )
    {
        throw std::invalid_argument( "Invalid plane location" );
    }
    dComplex pole( plane[ 0 ] == 'n' ? f0 : -f0, 0 );
    return zpk( fs, 0, 0, 1, &pole, gain, plane, prewarp );
}

IIRFilter
zero( double fs, double f0, double gain, const char* plane, bool prewarp )
{
    if ( !plane || ( strlen( plane ) != 1 ) ||
         ( strchr( "sfn", plane[ 0 ] ) == 0 ) )
    {
        throw std::invalid_argument( "Invalid plane location" );
    }
    dComplex zero( plane[ 0 ] == 'n' ? f0 : -f0, 0 );
    return zpk( fs, 1, &zero, 0, 0, gain, plane, prewarp );
}

IIRFilter
pole2( double      fs,
       double      f0,
       double      Q,
       double      gain,
       const char* plane,
       bool        prewarp )
{
    if ( !plane || ( strlen( plane ) != 1 ) ||
         ( strchr( "sfn", plane[ 0 ] ) == 0 ) )
    {
        throw std::invalid_argument( "Invalid plane location" );
    }
    dComplex pole[ 2 ];
    double   twoQ = 2. * fabs( Q );
    if ( twoQ <= 1 )
    {
        pole[ 0 ] = dComplex( plane[ 0 ] == 'n' ? f0 : -f0, 0 );
    }
    else
    {
        pole[ 0 ] = dComplex( plane[ 0 ] == 'n' ? f0 / twoQ : -f0 / twoQ,
                              f0 * sqrt( 1 - 1. / ( twoQ * twoQ ) ) );
    }
    pole[ 1 ] = ~pole[ 0 ];
    return zpk( fs, 0, 0, 2, pole, gain, plane, prewarp );
}

IIRFilter
zero2( double      fs,
       double      f0,
       double      Q,
       double      gain,
       const char* plane,
       bool        prewarp )
{
    if ( !plane || ( strlen( plane ) != 1 ) ||
         ( strchr( "sfn", plane[ 0 ] ) == 0 ) )
    {
        throw std::invalid_argument( "Invalid plane location" );
    }
    dComplex zero[ 2 ];
    double   twoQ = 2. * fabs( Q );
    if ( twoQ <= 1 )
    {
        zero[ 0 ] = dComplex( plane[ 0 ] == 'n' ? f0 : -f0, 0 );
    }
    else
    {
        zero[ 0 ] = dComplex( plane[ 0 ] == 'n' ? f0 / twoQ : -f0 / twoQ,
                              f0 * sqrt( 1 - 1. / ( twoQ * twoQ ) ) );
    }
    zero[ 1 ] = ~zero[ 0 ];
    return zpk( fs, 2, zero, 0, 0, gain, plane, prewarp );
}

IIRFilter
zpk( double          fs,
     int             nzeros,
     const dComplex* zero,
     int             npoles,
     const dComplex* pole,
     double          gain,
     const char*     plane,
     bool            prewarp )
{
    if ( fs <= 0 )
    {
        throw std::invalid_argument( "Sampling frequency must be positive" );
    }
    if ( ( npoles < 0 ) || ( ( npoles > 0 ) && !pole ) )
    {
        throw std::invalid_argument( "Number of poles must be non-negative" );
    }
    if ( ( nzeros < 0 ) || ( ( nzeros > 0 ) && !zero ) )
    {
        throw std::invalid_argument( "Number of zeros must be non-negative" );
    }
    if ( !plane || ( strlen( plane ) != 1 ) ||
         ( strchr( "sfn", plane[ 0 ] ) == 0 ) )
    {
        throw std::invalid_argument( "Invalid plane location" );
    }

    int                 nba = 0;
    lcl_array< double > ba( 2 * std::max( npoles, nzeros ) + 5 );
    if ( !s2z( fs,
               nzeros,
               zero,
               npoles,
               pole,
               gain,
               nba,
               ba,
               plane,
               "s",
               prewarp ) )
    {
        throw std::invalid_argument( "Complex conjugates and poles must "
                                     "be in the left half plane" );
    }
    IIRFilter iir( fs );
    iir *= ba[ 0 ];
    for ( int i = 0; i < ( nba - 1 ) / 4; ++i )
    {
        iir *= IIRSos( 1.,
                       ba[ 4 * i + 1 ],
                       ba[ 4 * i + 2 ],
                       1.,
                       ba[ 4 * i + 3 ],
                       ba[ 4 * i + 4 ] );
    }
    //iir.setSPlaneRep(nzeros, zero, npoles, pole);

    return iir;
}

IIRFilter
zpk( double          fs,
     int             nzeros,
     const fComplex* zero,
     int             npoles,
     const fComplex* pole,
     double          gain,
     const char*     plane,
     bool            prewarp )
{
    if ( fs <= 0 )
    {
        throw std::invalid_argument( "Sampling frequency must be positive" );
    }
    if ( ( npoles < 0 ) || ( ( npoles > 0 ) && !pole ) )
    {
        throw std::invalid_argument( "Number of poles must be non-negative" );
    }
    if ( ( nzeros < 0 ) || ( ( nzeros > 0 ) && !zero ) )
    {
        throw std::invalid_argument( "Number of zeros must be non-negative" );
    }
    if ( !plane || ( strlen( plane ) != 1 ) ||
         ( strchr( "sfn", plane[ 0 ] ) == 0 ) )
    {
        throw std::invalid_argument( "Invalid plane location" );
    }

    lcl_array< dComplex > z( nzeros + 1 );
    lcl_array< dComplex > p( npoles + 1 );
    for ( int i = 0; i < nzeros; ++i )
    {
        z[ i ] = zero[ i ];
    }
    for ( int i = 0; i < npoles; ++i )
    {
        p[ i ] = pole[ i ];
    }

    return zpk( fs, nzeros, z, npoles, p, gain, plane, prewarp );
}

IIRFilter
rpoly( double        fs,
       int           nnumer,
       const double* numer,
       int           ndenom,
       const double* denom,
       double        gain,
       bool          prewarp )
{
    if ( fs <= 0 )
    {
        throw std::invalid_argument( "Sampling frequency must be positive" );
    }
    if ( ( nnumer < 1 ) || !numer )
    {
        throw std::invalid_argument(
            "Number of numerator coefficients must be positive" );
    }
    if ( ( ndenom < 1 ) || !denom )
    {
        throw std::invalid_argument(
            "Number of denominator coefficients must be positive" );
    }
    if ( numer[ 0 ] == 0 )
    {
        throw std::invalid_argument(
            "First numerator coefficients cannot be zero" );
    }
    if ( denom[ 0 ] == 0 )
    {
        throw std::invalid_argument(
            "First denominator coefficients cannot be zero" );
    }
    lcl_array< dComplex > z( nnumer );
    lcl_array< dComplex > p( ndenom );
    int                   nzeros =
        polyroot( numer, nnumer - 1, (std::complex< double >*)(dComplex*)z );
    if ( nzeros != nnumer - 1 )
    {
        throw std::invalid_argument( "Unable to find numerator roots" );
    }
    int npoles =
        polyroot( denom, ndenom - 1, (std::complex< double >*)(dComplex*)p );
    if ( npoles != ndenom - 1 )
    {
        throw std::invalid_argument( "Unable to find denominator roots" );
    }
    gain *= numer[ 0 ] / denom[ 0 ];

    return zpk( fs, nzeros, z, npoles, p, gain, "s", prewarp );
}

IIRFilter
rpoly( double       fs,
       int          nnumer,
       const float* numer,
       int          ndenom,
       const float* denom,
       double       gain,
       bool         prewarp )
{
    if ( fs <= 0 )
    {
        throw std::invalid_argument( "Sampling frequency must be positive" );
    }
    if ( ( nnumer < 1 ) || !numer )
    {
        throw std::invalid_argument(
            "Number of numerator coefficients must be positive" );
    }
    if ( ( ndenom < 1 ) || !denom )
    {
        throw std::invalid_argument(
            "Number of denominator coefficients must be positive" );
    }
    if ( numer[ 0 ] == 0 )
    {
        throw std::invalid_argument(
            "First numerator coefficients cannot be zero" );
    }
    if ( denom[ 0 ] == 0 )
    {
        throw std::invalid_argument(
            "First denominator coefficients cannot be zero" );
    }
    lcl_array< double > nm( nnumer + 1 );
    lcl_array< double > dn( ndenom + 1 );
    for ( int i = 0; i < nnumer; ++i )
    {
        nm[ i ] = numer[ i ];
    }
    for ( int i = 0; i < ndenom; ++i )
    {
        dn[ i ] = denom[ i ];
    }

    return rpoly( fs, nnumer, numer, ndenom, denom, gain, prewarp );
}

IIRFilter
biquad( double fs, double b0, double b1, double b2, double a1, double a2 )
{
    if ( fs <= 0 )
    {
        throw std::invalid_argument( "Sampling frequency must be positive" );
    }
    if ( b0 == 0 )
    {
        throw std::invalid_argument( "b0 cannot be zero" );
    }
    if ( fabs( b2 ) < 1E-12 )
        b2 = 0;
    if ( fabs( a2 ) < 1E-12 )
        a2 = 0;
    // check that poles are within the unit circle
    if ( a2 < 1E-12 )
    {
        if ( fabs( a1 ) > 1 )
        {
            std::cerr << "fabs (a1) > 1" << a1 << std::endl;
            throw std::invalid_argument(
                "biquad: z pole must be within the unit circle" );
        }
    }
    else if ( fabs( a1 + a2 + 1 ) < 1E-12 )
    { // at least one pole at 0Hz
        if ( fabs( a2 - 1 ) < 1E-12 )
            a2 = 1; // second pole at 0Hz too
        a1 = -a2 - 1; // set to exact 0Hz
        if ( fabs( a2 ) > 1 )
        { // make sure second pole is stable
            std::cerr << "fabs (a2) > 1 " << a2 << std::endl;
            throw std::invalid_argument(
                "biquad: z pole must be within the unit circle" );
        }
    }
    else
    {
        double D = a1 * a1 - 4 * a2;
        if ( ( ( D < 0 ) && ( a2 > 1 ) ) ||
             ( ( D >= 0 ) && ( fabs( a1 ) + sqrt( D ) > 2 ) ) )
        {
            std::cerr << "D = " << D << " " << a1 << " " << a2 << std::endl;
            throw std::invalid_argument(
                "biquad: z pole must be within the unit circle" );
        }
    }
    IIRFilter filter( fs );
    filter *= IIRSos( b0, b1, b2, 1, a1, a2 );
    return filter;
}

IIRFilter
sos( double fs, int nba, const double* ba, const char* format )
{
    if ( fs <= 0 )
    {
        throw std::invalid_argument( "Sampling frequency must be positive" );
    }
    if ( !format || ( strlen( format ) != 1 ) ||
         ( strchr( "so", format[ 0 ] ) == 0 ) )
    {
        throw std::invalid_argument( "Invalid coefficient format" );
    }
    if ( ( nba < 1 ) || ( ( nba - 1 ) % 4 != 0 ) )
    {
        throw std::invalid_argument( "Invalid number of coefficients" );
    }
    IIRFilter filter( fs );
    filter *= ba[ 0 ];
    for ( int i = 0; i < ( nba - 1 ) / 4; ++i )
    {
        double b1 = ba[ 4 * i + 1 ];
        double b2 = ba[ 4 * i + 2 ];
        double a1 = ba[ 4 * i + 3 ];
        double a2 = ba[ 4 * i + 4 ];
        if ( format[ 0 ] == 'o' )
        {
            std::swap( a1, b1 );
            std::swap( a2, b2 );
        }
        if ( fabs( b2 ) < 1E-12 )
            b2 = 0;
        if ( fabs( a2 ) < 1E-12 )
            a2 = 0;
        // check that poles are within the unit circle
        if ( a2 < 1E-12 )
        {
            if ( fabs( a1 ) > 1 )
            {
                std::cerr << "fabs (a1) > 1" << a1 << std::endl;
                throw std::invalid_argument(
                    "sos: z pole must be within the unit circle" );
            }
        }
        else if ( fabs( a1 + a2 + 1 ) < 1E-12 )
        { // at least one pole at 0Hz
            if ( fabs( a2 - 1 ) < 1E-12 )
                a2 = 1; // second pole at 0Hz too
            a1 = -a2 - 1; // set to exact 0Hz
            if ( fabs( a2 ) > 1 )
            { // make sure second pole is stable
                std::cerr << "fabs (a2) > 1 " << a2 << std::endl;
                throw std::invalid_argument(
                    "sos: z pole must be within the unit circle" );
            }
        }
        else
        {
            double D = a1 * a1 - 4 * a2;
            if ( ( ( D < 0 ) && ( a2 > 1 ) ) ||
                 ( ( D >= 0 ) && ( fabs( a1 ) + sqrt( D ) > 2 ) ) )
            {
                std::cerr << "D = " << D << " " << a1 << " " << a2 << std::endl;
                throw std::invalid_argument(
                    "sos: z pole must be within the unit circle" );
            }
        }
        filter *= IIRSos( 1., b1, b2, 1., a1, a2 );
    }
    return filter;
}

IIRFilter
zroots( double          fs,
        int             nzeros,
        const dComplex* zero,
        int             npoles,
        const dComplex* pole,
        double          gain )
{
    if ( fs <= 0 )
    {
        throw std::invalid_argument( "Sampling frequency must be positive" );
    }
    if ( ( npoles < 0 ) || ( ( npoles > 0 ) && !pole ) )
    {
        throw std::invalid_argument( "Number of poles must be non-negative" );
    }
    if ( ( nzeros < 0 ) || ( ( nzeros > 0 ) && !zero ) )
    {
        throw std::invalid_argument( "Number of zeros must be non-negative" );
    }
    int nba = 0;
    int order = npoles;
    if ( nzeros > npoles )
        order = nzeros;
    lcl_array< double > ba( 2 * order + 5 );
    if ( !z2z( nzeros, zero, npoles, pole, gain, nba, ba, "s" ) )
    {
        throw std::invalid_argument( "Poles must be within unit circle "
                                     "and roots must come in complex pairs" );
    }
    return sos( fs, nba, ba, "s" );
}

IIRFilter
zroots( double          fs,
        int             nzeros,
        const fComplex* zero,
        int             npoles,
        const fComplex* pole,
        double          gain )
{
    if ( fs <= 0 )
    {
        throw std::invalid_argument( "Sampling frequency must be positive" );
    }
    if ( ( npoles < 0 ) || ( ( npoles > 0 ) && !pole ) )
    {
        throw std::invalid_argument( "Number of poles must be non-negative" );
    }
    if ( ( nzeros < 0 ) || ( ( nzeros > 0 ) && !zero ) )
    {
        throw std::invalid_argument( "Number of zeros must be non-negative" );
    }
    lcl_array< dComplex > z( nzeros + 1 );
    lcl_array< dComplex > p( npoles + 1 );
    for ( int i = 0; i < nzeros; ++i )
    {
        z[ i ] = zero[ i ];
    }
    for ( int i = 0; i < npoles; ++i )
    {
        p[ i ] = pole[ i ];
    }

    return zroots( fs, nzeros, z, npoles, p, gain );
}

IIRFilter
direct( double fs, int nb, const double* b, int na, const double* a )
{
    if ( fs <= 0 )
    {
        throw std::invalid_argument( "Sampling frequency must be positive" );
    }
    if ( ( nb < 0 ) || !b )
    {
        throw std::invalid_argument(
            "Number of b coefficients must be positive" );
    }
    if ( ( na < 0 ) || ( ( na > 0 ) && !a ) )
    {
        throw std::invalid_argument(
            "Number of a coefficients must be non-negative" );
    }
    if ( b[ 0 ] == 0 )
    {
        throw std::invalid_argument( "First b coefficients cannot be zero" );
    }
    lcl_array< dComplex > z( nb );
    lcl_array< dComplex > p( na );
    lcl_array< double >   aa( na + 1 );
    aa[ 0 ] = 1.0;
    for ( int i = 1; i < na + 1; ++i )
    {
        aa[ i ] = -a[ i - 1 ];
    }
    int nzeros = polyroot( b, nb, (std::complex< double >*)(dComplex*)z );
    if ( nzeros != nb )
    {
        throw std::invalid_argument(
            "Unable to find cascaded form (numerator)" );
    }
    int npoles = polyroot( aa, na, (std::complex< double >*)(dComplex*)p );
    if ( npoles != na )
    {
        throw std::invalid_argument(
            "Unable to find cascaded form (denominator)" );
    }
    double gain = b[ 0 ];
    return zroots( fs, nzeros, z, npoles, p, gain );
}

IIRFilter
closeloop( const Pipe& filter, double k )
{
    // make IIR filter
    IIRFilter iir;
    try
    {
        iir = iir2iir( filter );
    }
    catch ( ... )
    {
        throw std::invalid_argument( "Trying to close loop on non IIR Filter" );
    }
    int soscount = iirsoscount( iir );
    if ( soscount < 0 )
    {
        throw std::invalid_argument( "Trying to close loop on non IIR Filter" );
    }

    // get polynomial form
    int                 nnumer;
    int                 ndenom;
    lcl_array< double > numer( 2 * soscount + 1 );
    lcl_array< double > denom( 2 * soscount + 1 );
    double              gain;
    if ( !iir2poly( iir, nnumer, numer, ndenom, denom, gain ) )
    {
        throw std::runtime_error( "Unable to obtain polynomial form" );
    }
    // close loop math
    lcl_array< double > dn( 2 * soscount + 1 );
    int                 ndn = ndenom;
    gain *= k;
    if ( ndenom < nnumer )
    {
        int shift = nnumer - ndenom;
        for ( int j = nnumer - 1; j >= shift; --j )
        {
            dn[ j ] = denom[ j - shift ] + gain * numer[ j ];
        }
        for ( int j = shift - 1; j >= 0; --j )
        {
            dn[ j ] = gain * numer[ j ];
        }
        ndn = nnumer;
    }
    else
    {
        int shift = ndenom - nnumer;
        for ( int j = ndenom - 1; j >= shift; --j )
        {
            dn[ j ] = denom[ j ] + gain * numer[ j - shift ];
        }
        for ( int j = shift - 1; j >= 0; --j )
        {
            dn[ j ] = denom[ j ];
        }
        ndn = ndenom;
    }
    // make new filter with closed loop polynomial form
    return rpoly(
        iir.getFSample( ), ndenom, denom, ndn, dn, denom[ 0 ] / dn[ 0 ] );
}
