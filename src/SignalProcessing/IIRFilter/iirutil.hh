#ifndef _LIGO_IIRUTIL_H
#define _LIGO_IIRUTIL_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: iirutil							*/
/*                                                         		*/
/* Module Description: Utility functions for IIR filters		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 4Aug02   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: iirutil.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include <string>
#include "Complex.hh"
#include "IIRFilter.hh"

/** \ingroup sigp_iir_design IIR Filter Design
  * \{
  */

/** Bilinear transform. The transform will take a an s-plane pole
    or zero (\f$s_p\f$) as the argument and return the z-plane root,
    \f$z_p\f$ and its gain. For a pole we have:

    \f[ \frac{1}{s - s_p} \rightarrow 
        g \left(\frac{1 + z^{-1}}{1 - z_p \cdot z^{-1}}\right) \f]

    with

    \f[ z_p = \frac{2 f_s + s_p}{2 f_s - s_p} ; \, g = \frac{1}{2 f_s - s_p} \f]

    The function returns the absolute value of the gain, g, if \f$s_p\f$ is 
    complex. Since complex poles must come in complex conjugate pairs,
    the overall gain will always be real. 

    For a zero we have

    \f[ s + s_z  \rightarrow  \frac{1}{g} 
        \left(\frac{ 1 - z_z \cdot z^{-1}}{ 1 + z^{-1}}\right) \f]

    where:

    \f[z_z = \frac{ 2 f_s + s_z}{2 f_s - s_z} ; \, g = \frac{1}{2 f_s - s_z}\f]

    The function returns the absolute value of g, if \f$ s_p \f$ is
    complex. There is always a root at the Nyquist frequency which
    is ignored in the return of this function.

    If prewarping is requested (default), the s-plane root is first
    scaled by
    \f[ \frac{2 f_s}{|s_p|} \tan{\frac{|s_p|}{2 f_s}} \f]
    The returned gain is also multiplied by this factor.

    @memo Bilinear transform.
    @param fs Sampling rate.
    @param root s-plane root (in) and z-plane root (out)
    @param prewarp True for prewarping frequencies.
    @return Gain adjust.
 ************************************************************************/
double bilinear( double fs, dComplex& root, bool prewarp = true );

// This function is a workaround for the fact that pybind11 can not handle return by reference.
dComplex bilinear_root( double fs, dComplex root, bool prewarp = true );

/** Inverse bilinear transform. The transform will take a an z-plane 
    root, zp, as the argument and return the s-plane pole or zero,
    sp, and its gain. For a pole we have

    \f[ \frac{1 + z^{-1}}{1 - z_p \cdot z^{-1}}
             \rightarrow g \left( \frac{1}{s - s_p}\right) \f]

    with

    \f[ s_p = 2 f_s \left( \frac{z_p - 1}{z_p + 1} \right) \f]

    and

    \f[ g = \frac{ 4 f_s }{ z_p + 1 } \f]

    The function assumes an additional zero at the Nyquist frequency. 
    It returns the absolute value of the gain, g, if \f$z_p\f$ is complex. 
    Since complex poles must come in complex conjugate pairs,
    the overall gain will always be real. For a zero we have

    \f[ \frac{1 - z_p \cdot z^{-1}}{1 + z^{-1}} 
        \rightarrow \frac{1}{g}(s-s_z) \f]

    with
   \f[ sz = 2 fs \frac{z_p - 1}{z_p + 1} \f]

   and

   \f[ g = \frac{4 f_s}{zp + 1} \f]

    Again, the function returns the absolute value of g, if \f$s_p\f$ is
    complex.

    If unwarping is requested (default), the s-plane root is further
    scaled by

    \f[ \frac{2 fs}{| s_p |} \arctan{ \frac{| s_p |}{2 f_s}} \f]

    This factor is also multiplied to the returned gain.

    @memo Bilinear transform.
    @param fs Sampling rate.
    @param root s-plane root (in) and z-plane root (out)
    @param unwarp True for prewarping frequencies.
    @return Gain adjust.
 ************************************************************************/
double inverse_bilinear( double fs, dComplex& root, bool unwarp = true );

// This function is a workaround for the fact that pybind11 can not handle return by reference.
dComplex inverse_bilinear_root( double fs, dComplex root, bool unwarp = true );

/** Sorts roots in ascending order. Makes sure complex
    roots are grouped as complex conjugate pairs. If the roots are
    int the s-plane, they are ordered in increasing magnitude and
    increasing Q values. If the roots are in the z-plane, they are
    ordered in increasing distance from z=1 and increasing imaginary
    part. Real roots always come last.
    @memo Sort roots.
    @param root Array of roots (in and return)
    @param nroots Number of roots
    @param splane true if in s-plane, false if in z-plane
    @return True if all complex roots have a complex conjugate partner.
 ************************************************************************/
bool sort_roots( dComplex* root, int nroots, bool splane = true );

/** Transforms s-plane roots into z-plane roots.
    Poles and zero can be specifed in the s-plane using units of 
    rad/s (set plane to "s") or units of Hz ("f"). In both cases 
    the real part of the roots are expected to be NEGATIVE. 
    Alternately, one can also specify normalized poles and zeros
    ("n"). Normalized poles and zeros are expected to have POSITIVE
    real parts and their respective low and high frequency gains
    are set to 1 - unless they are located a 0Hz. The default 
    location is the s-plane.
    The transformation is made in place and the returned number
    of poles and zeros are identical.
    This functions uses the bilinear transform and ignores any
    z-plane roots at the Nyquist frequency that might be added 
    by this transfrom.
    @memo Bilinear transform.
    @param fs Sampling rate.
    @param nzeros Number of zeros
    @param zero Array of zeros (in and return)
    @param npoles Number of poles
    @param pole Array of poles (in and return)
    @param gain Gain (in and return)
    @param plane location where s-plane poles/zeros are specified
    @param prewarp True for prewarping frequencies.
    @return True if transform was successful.
 ************************************************************************/
bool s2z( double      fs,
          int         nzeros,
          dComplex*   zero,
          int         npoles,
          dComplex*   pole,
          double&     gain,
          const char* plane = "s",
          bool        prewarp = true );

/** Transforms s-plane roots into z-plane second order sections.
    Poles and zero can be specifed in the s-plane using units of 
    rad/s (set plane to "s") or units of Hz ("f"). In both cases 
    the real part of the roots are expected to be NEGATIVE. 
    Alternately, one can also specify normalized poles and zeros
    ("n"). Normalized poles and zeros are expected to have POSITIVE
    real parts and their respective low and high frequency gains
    are set to 1---unless they are located a 0Hz. The default 
    location is the s-plane.
    If the format is 's' (standard), the order of the coeffcients is 
    b1, b2, a1, a2. If the format is 'o' (online), the order is 
    a1, a2, b1, b2. The length nba must be of the form
    nba = 1 + 4 * (number of second order sections). The second order
    section is defined as:
    \f[ H(z)=\frac{ b_0 + b_1 z^{-1} + b_2 z^{-2}}
                  {1 + a_1 z^{-1} + a_2 z^{-2}} \f]
    This functions uses the bilinear transform.
    @memo Bilinear transform.
    @param fs Sampling rate.
    @param nzeros Number of zeros
    @param zero Array of zeros
    @param npoles Number of poles
    @param pole Array of poles
    @param gain Gain
    @param nba Number of b's and a's (return)
    @param ba Array of b's and a's (return)
    @param plane location where s-plane poles/zeros are specified
    @param format How a's and b's are specified
    @param prewarp True for prewarping frequencies.
    @return True if transform was successful.
 ************************************************************************/
bool s2z( double          fs,
          int             nzeros,
          const dComplex* zero,
          int             npoles,
          const dComplex* pole,
          double          gain,
          int&            nba,
          double*         ba,
          const char*     plane = "s",
          const char*     format = "s",
          bool            prewarp = true );

/** Transforms z-plane roots into s-plane roots.
    Poles and zeros are specifed in the s-plane using units of 
    rad/s (set plane to "s") or units of Hz ("f"). In both cases 
    the real part of the roots are expected to be NEGATIVE. 
    Alternativly, one can also specify normalized poles and zeros
    ("n"). Normalized poles and zeros are expected to have POSITIVE
    real parts and their respective low and high frequency gains
    are set to 1---unless they are located a 0Hz. The default 
    location is the s-plane.
    The transformation is made in place and the returned number
    of poles and zeros are identical.
    This functions uses the inverse bilinear transform and assumes 
    any z-plane roots at the Nyquist frequency that might be required 
    by this transform. Hence, the input list must not contain any
    poles or zeros at the Nyquist frequency.
    @memo Inverse bilinear transform.
    @param fs Sampling rate.
    @param nzeros Number of zeros
    @param zero Array of zeros (in and return)
    @param npoles Number of poles
    @param pole Array of poles (in and return)
    @param gain Gain (in and return)
    @param plane location where s-plane poles/zeros are specified
    @param unwarp True for unwarping frequencies.
    @return True if transform was successful.
 ************************************************************************/
bool z2s( double      fs,
          int         nzeros,
          dComplex*   zero,
          int         npoles,
          dComplex*   pole,
          double&     gain,
          const char* plane = "s",
          bool        unwarp = true );

/** Transforms z-plane second order sections into s-plane roots.
    Poles and zeros are specifed in the s-plane using units of 
    rad/s (set plane to "s") or units of Hz ("f"). In both cases 
    the real part of the roots are expected to be NEGATIVE. 
    Alternativly, one can specify normalized poles and zeros
    ("n"). Normalized poles and zeros are expected to have POSITIVE
    real parts and their respective low and high frequency gains
    are set to 1---unless they are located a 0Hz. The default 
    location is the s-plane.
    If the format is 's' (standard), the order of the coeffcients is 
    b1, b2, a1, a2. If the format is 'o' (online), the order is 
    a1, a2, b1, b2. The length nba must be of the form
    nba = 1 + 4 * (number of second order sections). The second order
    section is defined as:
    \f[ H(z) = \frac{b_0 + b_1 z^{-1} + b_2 z^{-2}}
                    {1 + a_1 z^{-1} + a_2 z^{-2}} \f]
    This functions uses the inverse bilinear transform.
    @memo Inverse bilinear transform.
    @param fs Sampling rate.
    @param nba Number of b's and a's
    @param ba Array of b's and a's
    @param nzeros Number of zeros (return)
    @param zero Array of zeros (return)
    @param npoles Number of poles (return)
    @param pole Array of poles (return)
    @param gain Gain (return)
    @param format How a's and b's are specified
    @param plane location where s-plane poles/zeros are specified
    @param unwarp True for unwarping frequencies.
    @return True if transform was successful.
 ************************************************************************/
bool z2s( double        fs,
          int           nba,
          const double* ba,
          int&          nzeros,
          dComplex*     zero,
          int&          npoles,
          dComplex*     pole,
          double&       gain,
          const char*   format = "s",
          const char*   plane = "s",
          bool          unwarp = true );

/** Transforms z-plane second order sections into z-plane roots.
    Poles and zeros at the Nyquist frequency are automatically removed.
    If the format is 's' (standard), the order of the coeffcients is 
    b1, b2, a1, a2. If the format is 'o' (online), the order is 
    a1, a2, b1, b2. The length nba must be of the form
    nba = 1 + 4 * (number of second order sections). The second order
    section is defined as:
    \f[ H(z) = \frac{ b_0 + b_1 z^{-1} + b_2 z^{-2}}
                    {1 + a_1 z^{-1} + a_2 z^{-2}} \f]
    @memo z-plane conversion.
    @param nba Number of b's and a's
    @param ba Array of b's and a's
    @param nzeros Number of zeros (return)
    @param zero Array of zeros (return)
    @param npoles Number of poles (return)
    @param pole Array of poles (return)
    @param gain Gain (return)
    @param format How a's and b's are specified
    @return True if transform was successful.
 ************************************************************************/
bool z2z( int           nba,
          const double* ba,
          int&          nzeros,
          dComplex*     zero,
          int&          npoles,
          dComplex*     pole,
          double&       gain,
          const char*   format = "s" );

/** Transforms z-plane roots into z-plane second order sections.
    Poles and zeros at the Nyquist frequency are automatically added
    as necessary.
    If the format is 's' (standard), the order of the coeffcients is 
    b1, b2, a1, a2. If the format is 'o' (online), the order is 
    a1, a2, b1, b2. The length nba must be of the form
    nba = 1 + 4 * (number of second order sections). The second order
    section is defined as:
    \f[ H(z) = \frac{b_0 + b_1 z^{-1} + b_2 z^{-2}}
                    {1 + a_1 z^{-1} + a_2 z^{-2}} \f]
    @memo z-plane conversion.
    @param nzeros Number of zeros
    @param zero Array of zeros
    @param npoles Number of poles
    @param pole Array of poles
    @param gain Gain
    @param nba Number of b's and a's (return)
    @param ba Array of b's and a's (return)
    @param format How a's and b's are specified
    @return True if transform was successful.
 ************************************************************************/
bool z2z( int             nzeros,
          const dComplex* zero,
          int             npoles,
          const dComplex* pole,
          double          gain,
          int&            nba,
          double*         ba,
          const char*     format = "s" );

/** Tests if this is an IIR Filter. Returns true if filter is either
    an IIRFilter or a MultiPipe filled with IIRFilters.
    @memo Is IIR filter.
    @param filter Filter under test.
    @return True if IIR filter.
 ************************************************************************/
bool isiir( const Pipe& filter );

/** Returns the count of second order sections of an IIR filter. If 
    the specified filter is not an IIR filter it will return -1.
    @memo IIR filter SOS count.
    @param filter Filter to evaluate.
    @return IIR filter SOS count.
 ************************************************************************/
int iirsoscount( const Pipe& filter );

/** Returns the number of poles of an IIR filter. If the specified 
    filter is not an IIR filter it will return -1.
    @memo IIR filter number of poles.
    @param filter Filter to evaluate.
    @return IIR filter number of poles.
 ************************************************************************/
int iirpolecount( const Pipe& filter );

/** Returns the number of zeros of an IIR filter. If the specified 
    filter is not an IIR filter it will return -1.
    @memo IIR filter number of zeros.
    @param filter Filter to evaluate.
    @return IIR filter number of zeros.
 ************************************************************************/
int iirzerocount( const Pipe& filter );

/** Returns the number of poles and zeros of an IIR filter. If the 
    specified filter is not an IIR filter it will return false.
    @memo IIR filter number of zeros.
    @param filter Filter to evaluate.
    @param npoles Number of poles (return)
    @param nzeros Number of zeros (return)
    @return true if IIR filter.
 ************************************************************************/
bool iirpolezerocount( const Pipe& filter, int& npoles, int& nzeros );

/** Returns the order of the IIR filter. If the specified filter is
    not an IIR filter it will return -1.
    @memo IIR filter order.
    @param filter Filter to evaluate.
    @return IIR filter order.
   ************************************************************************/
int iirorder( const Pipe& filter );

/** Compare two IIR filters. Returns true if both filters are
    IIR and implement the same filter coefficients.
    @memo IIR filter compare.
    @param f1 Filter 1 to evaluate.
    @param f2 Filter 2 to evaluate.
    @return true if equal.
 ************************************************************************/
bool iircmp( const Pipe& f1, const Pipe& f2 );

/** Combines all IIR filters in MultiPipe filter into a single IIRFilter.
    @memo IIR filter join.
    @exception std::invalid_argument \a filter is not an %IIRFilter or a 
                                     MultiPipe composed of %IIRFilters.
    @param filter Filter to copy/merge.
    @return Combined %IIRFilter.
 ************************************************************************/
IIRFilter iir2iir( const Pipe& filter );

/** Extract the zeros and poles of an IIR filter.
    @memo IIR to zpk.
    @see FilterDesign.hh
    @param filter Filter to evaluate.
    @param nzeros Number of zeros (return)
    @param zero Array of zeros (return)
    @param npoles Number of poles (return)
    @param pole Array of poles (return)
    @param gain Gain (return)
    @param plane Plane in which poles and zeros are specified.
    @param unwarp Unwarp the frequency response.
    @return true if successful.
 ************************************************************************/
bool iir2zpk( const Pipe& filter,
              int&        nzeros,
              dComplex*   zero,
              int&        npoles,
              dComplex*   pole,
              double&     gain,
              const char* plane = "s",
              bool        unwarp = true );

/** Returns the rational polynomial of an IIR filter.
    @memo IIR to rational polynomial.
    @see FilterDesign.hh
    @param filter Filter to evaluate.
    @param nnumer Number of coefficients in numerator (return)
    @param numer Array of numerator coefficents (return)
    @param ndenom Number of coeffcients in denominator (return)
    @param denom Array of denominator coeffcients (return)
    @param gain Gain (return)
    @param unwarp Unwarp the frequency response.
    @return true if successful.
 ************************************************************************/
bool iir2poly( const Pipe& filter,
               int&        nnumer,
               double*     numer,
               int&        ndenom,
               double*     denom,
               double&     gain,
               bool        unwarp = true );

/** Convert an IIRFilter to a zpk definition string. 
    The returned string has the format "zpk(...)", if the plane is
    "s", "n" or "f". It is of the form "rpoly(...)", if the plane is
    "p". It can be passed to the FilterDesign class
    @memo Convert %IIRFilter to a zpk definition string.
    @see FilterDesign.hh
    @param filter Filter to evaluate.
    @param zpk Zero-pole-gain string (return)
    @param plane location where poles/zeros are specified
    @param unwarp Unwarp the frequency response.
    @return true if successful.
 ************************************************************************/
bool iir2zpk( const Pipe&  filter,
              std::string& zpk,
              const char*  plane = "s",
              bool         unwarp = true );

/** Returns the a's and b's of an IIR filter.
    The returned a's and b's are grouped in zecond order sections.
    The first returned coeffcient is the overall gain. The following
    coeffcients come in groups of 4. If the format is 's' (standard),
    the order is b1, b2, a1, a2. If the format is 'o' (online), the
    order is a1, a2, b1, b2. The returned length is always of the
    for nba = 1 + 4 * (number of second order sections). A second order
    section is defined as:
    \f[ H(z) = \frac{b_0 + b_1 z^{-1} + b_2 z^{-2}}
                    {1 + a_1 z^{-1} + a_2 z^{-2}} \f]
    @memo IIR to z.
    @see FilterDesign.hh
    @param filter Filter to evaluate.
    @param nba Number of b's and a's (return)
    @param ba Array of b's and a's (return)
    @param format How a's and b's are specified
    @return true if successful.
 ************************************************************************/
bool
iir2z( const Pipe& filter, int& nba, double* ba, const char* format = "s" );

/** Returns the roots in the z-plane of an IIR filter. Any poles or
    zeros at the Nyquist frequency are getting ignored!
    @memo IIR to z.
    @see FilterDesign.hh
    @param filter Filter to evaluate.
    @param nzeros Number of zeros (return)
    @param zero Array of zeros (return)
    @param npoles Number of poles (return)
    @param pole Array of poles (return)
    @param gain Gain (return)
    @return true if successful.
 ************************************************************************/
bool iir2z( const Pipe& filter,
            int&        nzeros,
            dComplex*   zero,
            int&        npoles,
            dComplex*   pole,
            double&     gain );

/** Returns the direct form of an IIR filter. Avoid the direct form
    since even fairly simple filters will run into precision problems.
    @memo IIR to direct form.
    @see FilterDesign.hh
    @param filter Filter to evaluate.
    @param nb Number of coefficients in numerator (return)
    @param b Array of b coefficents (return)
    @param na Number of coeffcients in denominator (return)
    @param a Array of denominator coeffcients exclusive a0 (return)
    @return true if successful.
 ************************************************************************/
bool iir2direct( const Pipe& filter, int& nb, double* b, int& na, double* a );

/** Returns the a's and b's of an IIR filter.
    The returned string has the representation "sos([c0;c1;...])" for 
    formats "s" (standard) and "o" (online), the representation 
    zroots([z1;...],[p1;...],k) for the format "r" (roots), or the
    representation direct([b0,...],[a1;...]) for the format "d". This
    string can be passed to the FilterDesign class. Avoid the direct form
    since even fairly simple filters will run into precision problems.
    @memo IIR to z.
    @see FilterDesign.hh
    @param filter Filter to evaluate.
    @param z String for a's and b's or roots (return)
    @param format How a's and b's are specified
    @return true if successful.
 ************************************************************************/
bool iir2z( const Pipe& filter, std::string& z, const char* format = "r" );

/**
 * \}
 */

#endif // _LIGO_IIRUTIL_H
