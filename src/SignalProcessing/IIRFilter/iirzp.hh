//
// Reference : Digital Filter Design (T.W. Parks & C.S. Burrus)
//             Filter generation code, ellf, at http://www.moshier.net/ellfdoc.html
//
// 7/17/2002
// Masahiro Ito
// Univerisity of Oregon
// masahiro@darkwing.uoregon.edu
//

#ifndef iirzp_HH
#define iirzp_HH

#include "Complex.hh"
#include "zp2zp.hh"

/** \ingroup sigp_iir_design
  * \{
  */

/** Generate poles for an analog prototype low-pass Butterworth 
     * filter. The Butterworth prototype has no zeros and its gain is 1.
     * @memo Poles for a low-pass Butterworth filter.
     * @param order Filter order.
     * @param np Number of poles.
     * @param pole Array for poles.
     * @return True if successful.
     */
bool buttap( int order, int& np, dComplex* pole );

/** Generate zeros and poles for a Butterworh filter.
     * @memo zeros and poles for a Butterworth filter.
     * @param type Filter type (Filter_Type enumerator)
     * @param order Filter order.
     * @param f1 Pass band edge.
     * @param f2 Another pass band edge.
     * @param nz Number of poles.
     * @param zero Array for poles.
     * @param np Number of poles.
     * @param pole Array for poles.
     * @param gain Filter gain.
     * @return True if successful.
     */
bool butterzp( Filter_Type type,
               int         order,
               double      f1,
               double      f2,
               int&        nz,
               dComplex*   zero,
               int&        np,
               dComplex*   pole,
               double&     gain );

/** Generate zeros and poles for an analog prototype low-pass Chebyshev 
     * type-I filter. The Chebyshev type-I prototype has no zeros.
     * @memo Zeros and poles for a low-pass Chebyshev type-I filter.
     * @param order Filter order.
     * @param rp Pass band ripple (dB).
     * @param np Number of poles.
     * @param pole Array for poles.
     * @param gain Filter gain.
     * @return True if successful.
     */
bool cheb1ap( int order, double& rp, int& np, dComplex* pole, double& gain );

/** Generate zeros and poles for a Chebyshev type-I filter.
     * @memo Zeros and poles for a Chebyshev type-I filter.
     * @param type Filter type (Filter_Type enumerator)
     * @param order Filter order.
     * @param rp Pass band ripple (dB).
     * @param f1 Pass band edge.
     * @param f2 Another pass band edge.
     * @param nz Number of poles.
     * @param zero Array for poles.
     * @param np Number of poles.
     * @param pole Array for poles.
     * @param gain Filter gain.
     * @return True if successful.
     */
bool cheby1zp( Filter_Type type,
               int         order,
               double      rp,
               double      f1,
               double      f2,
               int&        nz,
               dComplex*   zero,
               int&        np,
               dComplex*   pole,
               double&     gain );

/** Generate zeros and poles for an analog prototype low-pass Chebyshev 
     * type-II filter.
     * @memo Zeros and poles for a low-pass Chebyshev type-II filter.
     * @param order Filter order.
     * @param rs Stop band ripple (in dB).
     * @param nz Number of poles.
     * @param zero Array for poles.
     * @param np Number of poles.
     * @param pole Array for poles.
     * @param gain Filter gain.
     * @return True if successful.
     */
bool cheb2ap( int       order,
              double&   rs,
              int&      nz,
              dComplex* zero,
              int&      np,
              dComplex* pole,
              double&   gain );

/** Generate zeros and poles for a Chebyshev type-II filter.
     * @memo zeros and poles for a Chebyshev type-II filter.
     * @param type Filter type (Filter_Type enumerator)
     * @param order Filter order.
     * @param rs Stop band ripple (dB).
     * @param f1 Pass band edge.
     * @param f2 Another pass band edge.
     * @param nz Number of poles.
     * @param zero Array for poles.
     * @param np Number of poles.
     * @param pole Array for poles.
     * @param gain Filter gain.
     * @return True if successful.
     */
bool cheby2zp( Filter_Type type,
               int         order,
               double      rs,
               double      f1,
               double      f2,
               int&        nz,
               dComplex*   zero,
               int&        np,
               dComplex*   pole,
               double&     gain );

/** Jacobian elliptic function.
     *
     * \f[ u = \int_0^{\phi} \frac{1}{\sqrt{1 - m \sin{x}^2 }} dx \f],
     * \f[ sn = \sin{\phi}; \, sn^2 + cn^2 = 1; \, 
           k^2 sn^2 + dn^2 = 1; \, k = \sqrt{ m } \f]
     * @memo Jacobian elliptic function.
     * @param u
     * @param m
     * @param sn (returned) 
     * @param cn (returned)
     * @param dn (returned)
     * @param ph phi (returned)
     * @return true if calculated successfully.
     */
bool
ellipj( double u, double m, double& sn, double& cn, double& dn, double& ph );

/** Complete elliptic integral of the first kind.
     * \f[ K(m) = \int_0^{\pi/2} \frac{1}{\sqrt{1 - m \sin{x}^2 }} dx \f]
     * @memo Complete elliptic integral of the first kind.
     * @param m
     * @return K(m)
     */
double ellipk( double m );

/** Incomplete elliptic integral of the first kind.
     * \f[ F(\phi | m) = \int_0^{\phi} \frac{1}{\sqrt{1 - m \sin{x}^2}} dx \f]
     * @memo Incomplete elliptic integral of the first kind.
     * @param phi
     * @param m
     * @return F(phi|m)
     */
double ellipf( double phi, double m );

/** Generate zeros and poles for an analog prototype low-pass elliptic 
     * filter.
     * @memo Zeros and poles for an analog prototype low-pass elliptic filter.
     * @param order Filter order.
     * @param rp pass band ripple (dB).
     * @param rs Stop band attenuation (dB).
     * @param nz Number of zeros.
     * @param zero Array for zeros.
     * @param np Number of poles.
     * @param pole Array for poles.
     * @param gain Filter gain.
     * @return True if successful.
     */
bool ellipap( int       order,
              double    rp,
              double    rs,
              int&      nz,
              dComplex* zero,
              int&      np,
              dComplex* pole,
              double&   gain );

/** Generate zeros and poles for an elliptic filter.
     * @memo zeros and poles for an elliptic filter.
     * @param type Filter type (Filter_Type enumerator)
     * @param order Filter order.
     * @param rp Pass band ripple (dB).
     * @param rs Stop band attenuation (dB).
     * @param f1 Pass band edge.
     * @param f2 Another pass band edge.
     * @param nz Number of zeros.
     * @param zero Array for zeros.
     * @param np Number of poles.
     * @param pole Array for poles.
     * @param gain Filter gain.
     * @return True if successful.
     */
bool ellipzp( Filter_Type type,
              int         order,
              double      rp,
              double      rs,
              double      f1,
              double      f2,
              int&        nz,
              dComplex*   zero,
              int&        np,
              dComplex*   pole,
              double&     gain );

/** Generate zeros and poles for a notch filter.
   * @memo zeros and poles for a notch filter.
   * @param f0 Center frequency.
   * @param Q Quality factor ( \f$ Q = f_0 / <width> \f$ ).
   * @param depth Depth of the notch (dB).
   * @param nz Number of zeros.
   * @param zero Array for zeros.
   * @param np Number of poles.
   * @param pole Array for poles.
   * @return True if successful.
   */
bool notchzp( double    f0,
              double    Q,
              double    depth,
              int&      nz,
              dComplex* zero,
              int&      np,
              dComplex* pole );

/** Generate zeros and poles for a resonant gain filter.
   * @memo zeros and poles for a resonant gain filter.
   * @param f0 Center frequency.
   * @param Q Quality factor ( \f$ Q = f_0 / <width> \f$ ).
   * @param height Height of the peak (dB).
   * @param nz Number of zeros.
   * @param zero Array for zeros.
   * @param np Number of poles.
   * @param pole Array for poles.
   * @return True if successful.
   */
bool resgainzp( double    f0,
                double    Q,
                double    height,
                int&      nz,
                dComplex* zero,
                int&      np,
                dComplex* pole );

/**
 * \}
 */
#endif
