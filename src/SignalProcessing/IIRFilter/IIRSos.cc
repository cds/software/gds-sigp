/* -*- mode: c++; c-basic-offset: 3; -*- */
// this class is for initializing and manipulating
// IIR filters implemented as second order sections
//
// Edward Daw, 20th May 2001
//   updated for greater efficiency and cds constructor 5th July 2002

#include "IIRSos.hh"
#include "Complex.hh"
#include <iostream>
// #include <cmath>
#include "gds_fpclass.hh"
#include "constant.hh"
#include <stdexcept>

using namespace std;

//======================================  Evaluate biquad with different methods
//
//--------------------------------------  Direct form II
template < typename T >
inline T
step_df2( T       in,
          double& m1,
          double& m2,
          double  a1,
          double  a2,
          double  b0,
          double  b1,
          double  b2 )
{
    double m0 = double( in ) - m1 * a1 - m2 * a2;
    T      out = T( m0 * b0 + m1 * b1 + m2 * b2 );
    m2 = m1;
    m1 = m0;
    return out;
}

//--------------------------------------  Transposed DF2
template < typename T >
inline T
step_lnf( T       in,
          double& m1,
          double& m2,
          double  a1,
          double  a2,
          double  b0,
          double  b1,
          double  b2 )
{
    double m0 = in * b0 + m1;
    m1 = in * b1 + m2 - a1 * m0;
    m2 = in * b2 - a2 * m0;
    return T( m0 );
}

//---------------------------------------  Low noise form (modified parameters)
template < typename T >
inline T
step_lnf2( T       in,
           double& m1,
           double& m2,
           double  a1,
           double  a2,
           double  g,
           double  d1,
           double  d2 )
{
    double gx = double( in ) * g;
    double m0 = m1;
    m1 = gx * d1 + m2 - a1 * m0;
    m2 = gx * d2 - a2 * m0;
    return T( m0 + gx );
}

//---------------------------------------  Low noise form (modified parameters)
typedef long double hpc_type;
template < typename T >
inline T
step_hpc( T       in,
          double& m1,
          double& m2,
          double  a1,
          double  a2,
          double  b0,
          double  b1,
          double  b2 )
{
    hpc_type din = hpc_type( in );
    hpc_type m0 = din * b0 + hpc_type( m1 );
    m1 = double( din * b1 + hpc_type( m2 ) - a1 * m0 );
    m2 = double( din * b2 - a2 * m0 );
    return T( m0 );
}

//========================================  IIRSos constructors
IIRSos::IIRSos( void ) : _evmode( sos_lnf2 )
{
    _npoles = 0;
    _nzeros = 0;
    reset( );
    _section_loaded = 0;
}

IIRSos::IIRSos( double gain, double c1, double c2, double c3, double c4 )
    : _evmode( sos_lnf2 )
{
    init( gain, c1, c2, c3, c4 );
}

IIRSos::IIRSos( double b0in,
                double b1in,
                double b2in,
                double a0in,
                double a1in,
                double a2in )
    : _evmode( sos_lnf2 )
{
    init( b0in, b1in, b2in, a0in, a1in, a2in );
}

IIRSos::IIRSos( dComplex zeros, dComplex poles, double fs )
    : _evmode( sos_lnf2 )
{
    init( zeros, poles, fs );
}

IIRSos::IIRSos( double z0, double z1, dComplex poles, double fs )
    : _evmode( sos_lnf2 )
{
    init( z0, z1, poles, fs );
}

IIRSos::IIRSos( double z, dComplex poles, double fs ) : _evmode( sos_lnf2 )
{
    init( z, poles, fs );
}

IIRSos::IIRSos( int nzeros, dComplex roots, double fs ) : _evmode( sos_lnf2 )
{
    init( nzeros, roots, fs );
}

IIRSos::IIRSos( dComplex zeros, double p0, double p1, double fs )
    : _evmode( sos_lnf2 )
{
    init( zeros, p0, p1, fs );
}

IIRSos::IIRSos( dComplex zeros, double p, double fs )
{
    init( zeros, p, fs );
}

IIRSos::IIRSos(
    int nzeros, double z0, double z1, double p0, double p1, double fs )
{
    init( nzeros, z0, z1, p0, p1, fs );
}

IIRSos::IIRSos( int nzeros, double r0, double r1, double r2, double fs )
    : _evmode( sos_lnf2 )
{
    init( nzeros, r0, r1, r2, fs );
}

IIRSos::IIRSos( int nzeros, double r0, double r1, double fs )
    : _evmode( sos_lnf2 )
{
    init( nzeros, r0, r1, fs );
}

IIRSos::IIRSos( int nzeros, double r, double fs ) : _evmode( sos_lnf2 )
{
    init( nzeros, r, fs );
}

IIRSos::~IIRSos( void )
{
}

int
IIRSos::init( double gain, double c1, double c2, double c3, double c4 )
{
    a1 = c1;
    a2 = c2;
    b0 = gain;
    b1 = c3 * gain;
    b2 = c4 * gain;
    reset( );
    _npoles = 0;
    _nzeros = 0;
    _section_loaded = 1;
    _period = 0;
    _period_known = false;
    return 0;
}

int
IIRSos::init( double b0in,
              double b1in,
              double b2in,
              double a0in,
              double a1in,
              double a2in )
{
    b0 = b0in / a0in;
    b1 = b1in / a0in;
    b2 = b2in / a0in;
    a1 = a1in / a0in;
    a2 = a2in / a0in;
    reset( );
    _npoles = 0;
    _nzeros = 0;
    _section_loaded = 1;
    _period = 0;
    _period_known = false;
    return 0;
}

int
IIRSos::init( dComplex zero, dComplex pole, double fs )
{
    double alpha( -real( pole ) );
    double gamma( -real( zero ) );
    _period = 1 / fs;
    _period_known = true;
    double fs2 = fs * 2.0;
    // set filter coefficients
    double ssz( norm( zero ) );
    double ssp( norm( pole ) );
    double nm( 4 * fs * ( fs + alpha ) + ssp );
    a1 = 2 * ( ssp - fs2 * fs2 ) / nm;
    a2 = ( 4 * fs * ( fs - alpha ) + ssp ) / nm;
    b1 = 2 * ( ssz - fs2 * fs2 ) / nm;
    b2 = ( 4 * fs * ( fs - gamma ) + ssz ) / nm;
    b0 = ( 4 * fs * ( fs + gamma ) + ssz ) / nm;
    _npoles = 2;
    _nzeros = 2;
    // set history buffers
    reset( );
    // set filter loaded bit
    _section_loaded = 1;
    return 0;
}

int
IIRSos::init( dComplex zeros, double p0, double p1, double fs )
{
    double gamma( -real( zeros ) );
    double ssz( norm( zeros ) );
    _period = 1 / fs;
    _period_known = true;
    _nzeros = 2;
    double fs2 = fs * 2.0;
    double epsilon( -p0 );
    double phi( -p1 );
    _npoles = 2;
    double nm = ( fs2 + epsilon ) * ( fs2 + phi );
    a1 = 2 * ( epsilon * phi - fs2 * fs2 ) / nm;
    a2 = ( fs2 - epsilon ) * ( fs2 - phi ) / nm;
    b0 = ( 4 * fs * ( fs + gamma ) + ssz ) / nm;
    b1 = 2 * ( ssz - fs2 * fs2 ) / nm;
    b2 = ( 4 * fs * ( fs - gamma ) + ssz ) / nm;
    // set history buffers
    reset( );
    // set filter loaded bit
    _section_loaded = 1;
    return 0;
}

int
IIRSos::init( dComplex zeros, double p, double fs )
{
    double gamma( -real( zeros ) );
    double ssz( norm( zeros ) );
    _period = 1 / fs;
    _period_known = true;
    _nzeros = 2;
    double fs2 = fs * 2.0;
    double epsilon( -p );
    cout << "warning: adding a pole at the Nyquist frequency to " << endl
         << "         avoid divergence of transfer function magnitude." << endl;
    double phi = pi * fs;
    _npoles = 2;
    double nm = ( fs2 + epsilon ) * ( fs2 + phi );
    a1 = 2 * ( epsilon * phi - fs2 * fs2 ) / nm;
    a2 = ( fs2 - epsilon ) * ( fs2 - phi ) / nm;
    double nm2 = 0.5 * fs; // = phi / (2*pi)
    b0 = nm2 * ( 4 * fs * ( fs + gamma ) + ssz ) / nm;
    b1 = nm2 * 2 * ( ssz - fs2 * fs2 ) / nm;
    b2 = nm2 * ( 4 * fs * ( fs - gamma ) + ssz ) / nm;
    // set history buffers
    reset( );
    // set filter loaded bit
    _section_loaded = 1;
    return 0;
}

int
IIRSos::init( int nzeros, dComplex rootpair, double fs )
{
    _period = 1 / fs;
    _period_known = true;
    double fs2 = 2.0 * fs;

    if ( nzeros == 1 )
    {
        double gamma( -real( rootpair ) );
        double ssz( norm( rootpair ) );
        _nzeros = 2;
        cout << "warning: Adding two poles at Nyquist frequency to " << endl
             << "         avoid divergence of transfer function magnitude."
             << endl;
        double epsilon = pi * fs;
        double phi = pi * fs;
        _npoles = 2;
        double nm = ( fs2 + epsilon ) * ( fs2 + phi );
        a1 = 2 * ( epsilon * phi - fs2 * fs2 ) / nm;
        a2 = ( fs2 - epsilon ) * ( fs2 - phi ) / nm;
        double nm2 = 0.25 * fs * fs; // = epsilon*phi/(2*pi)^2
        b0 = nm2 * ( 4 * fs * ( fs + gamma ) + ssz ) / nm;
        b1 = nm2 * 2 * ( ssz - fs2 * fs2 ) / nm;
        b2 = nm2 * ( 4 * fs * ( fs - gamma ) + ssz ) / nm;
    }
    else if ( nzeros == 0 )
    {
        double alpha = ( -real( rootpair ) );
        double ssp( norm( rootpair ) );
        _nzeros = 0;
        double nm = 4 * fs * ( fs + alpha ) + ssp;
        _npoles = 2;
        a1 = 2 * ( ssp - fs2 * fs2 ) / nm;
        a2 = ( 4 * fs * ( fs - alpha ) + ssp ) / nm;
        b0 = 1 / nm;
        b1 = 2 / nm;
        b2 = 1 / nm;
    }
    else
    {
        cerr << "ERROR: invalid initialization with 1 complex root pair"
             << endl;
        _section_loaded = 0;
        return -1;
    }

    // set history buffers
    reset( );

    // set filter loaded bit
    _section_loaded = 1;
    return 0;
}

int
IIRSos::init( double z, dComplex pole, double fs )
{
    double alpha = ( -real( pole ) );
    double ssp( norm( pole ) );
    double mu( -z );
    _period = 1 / fs;
    _period_known = true;
    _nzeros = 1;
    _npoles = 2;
    double fs2 = 2.0 * fs;
    double nm = 4 * fs * ( fs + alpha ) + ssp;
    a1 = 2 * ( ssp - fs2 * fs2 ) / nm;
    a2 = ( 4 * fs * ( fs - alpha ) + ssp ) / nm;
    b0 = -( fs2 + mu ) / nm;
    b1 = -2 * mu / nm;
    b2 = ( fs2 - mu ) / nm;
    // set history buffers
    reset( );
    // set filter loaded bit
    _section_loaded = 1;
    return 0;
}

int
IIRSos::init( double z0, double z1, dComplex pole, double fs )
{
    double alpha = ( -real( pole ) );
    double ssz( norm( pole ) );
    _period = 1 / fs;
    _period_known = true;
    _nzeros = 2;
    double nm = 4 * fs * ( fs + alpha ) + ssz;
    _npoles = 2;
    double fs2 = fs * 2.0;
    double mu( -z0 );
    double nu( -z1 );
    a1 = 2 * ( ssz - fs2 * fs2 ) / nm;
    a2 = ( 4 * fs * ( fs - alpha ) + ssz ) / nm;
    b0 = ( fs2 + mu ) * ( fs2 + nu ) / nm;
    b1 = 2 * ( nu * mu - fs2 * fs2 ) / nm;
    b2 = ( fs2 - mu ) * ( fs2 - nu ) / nm;
    // set history buffers
    reset( );
    // set filter loaded bit
    _section_loaded = 1;
    return 0;
}

int
IIRSos::init(
    int nzeros, double z0, double z1, double p0, double p1, double fs )
{
    if ( nzeros != 2 )
    {
        cerr << "ERROR: init function requires two zeros and two poles."
             << endl;
        _section_loaded = 0;
        return -1;
    }
    _npoles = 2;
    _nzeros = 2;
    double fs2 = fs * 2.0;
    double epsilon( -p0 );
    double phi( -p1 );
    double mu( -z0 );
    double nu( -z1 );
    double nm = ( fs2 + epsilon ) * ( fs2 + phi );
    a1 = 2 * ( epsilon * phi - fs2 * fs2 ) / nm;
    a2 = ( fs2 - epsilon ) * ( fs2 - phi ) / nm;
    b0 = ( fs2 + mu ) * ( fs2 + nu ) / nm;
    b1 = 2 * ( mu * nu - fs2 * fs2 ) / nm;
    b2 = ( fs2 - mu ) * ( fs2 - nu ) / nm;
    // set period
    _period = 1 / fs;
    _period_known = true;
    // set history buffers
    reset( );
    // set filter loaded bit
    _section_loaded = 1;
    return 0;
}

int
IIRSos::init( int nzeros, double r0, double r1, double r2, double fs )
{
    double fs2 = fs * 2.0;
    if ( nzeros == 2 )
    {
        _npoles = 2;
        _nzeros = 2;
        double epsilon( -r2 ); // real pole
        cout << "WARNING: adding a real pole at the Nyquist frequency" << endl;
        double phi( pi * fs ); // pole inserted at fNy
        double mu( -r0 ); // first real zero
        double nu( -r1 ); // second zero
        double nm = ( fs2 + epsilon ) * ( fs2 + phi );
        a1 = 2 * ( epsilon * phi - fs2 * fs2 ) / nm;
        a2 = ( fs2 - epsilon ) * ( fs2 - phi ) / nm;
        double nm2 = fs * 0.5; // = phi/(2*pi)
        b0 = nm2 * ( fs2 + mu ) * ( fs2 + nu ) / nm;
        b1 = nm2 * 2 * ( mu * nu - fs2 * fs2 ) / nm;
        b2 = nm2 * ( fs2 - mu ) * ( fs2 - nu ) / nm;
    }
    else if ( nzeros == 1 )
    {
        _npoles = 2;
        _nzeros = 1;
        double mu( -r0 ); // real zero
        double epsilon( -r1 ); // first real pole
        double phi( -r2 ); // second real pole
        double nm = ( fs2 + epsilon ) * ( fs2 + phi );
        a1 = 2 * ( epsilon * phi - fs2 * fs2 ) / nm;
        a2 = ( fs2 - epsilon ) * ( fs2 - phi ) / nm;
        b0 = -( fs2 + mu ) / nm;
        b1 = -2 * mu / nm;
        b2 = ( fs2 - mu ) / nm;
    }
    else
    {
        cerr << "ERROR: three real root init function inappropriate." << endl;
        _section_loaded = 0;
        return -1;
    }
    // set period
    _period = 1 / fs;
    _period_known = true;
    // set history buffers
    reset( );
    // set filter loaded bit
    _section_loaded = 1;
    return 0;
}

int
IIRSos::init( int nzeros, double r0, double r1, double fs )
{
    double fs2 = fs * 2.0;
    if ( nzeros == 2 )
    {
        _npoles = 2;
        _nzeros = 2;
        cout << "warning: adding two real poles at the Nyquist frequency"
             << endl;
        double epsilon = pi * fs;
        double phi = pi * fs;
        double mu( -r0 );
        double nu( -r1 );
        double nm = ( fs2 + epsilon ) * ( fs2 + phi );
        a1 = 2 * ( epsilon * phi - fs2 * fs2 ) / nm;
        a2 = ( fs2 - epsilon ) * ( fs2 - phi ) / nm;
        double nm2 = fs * fs * 0.25; // epsilon * phi / (2*pi)^2
        b0 = nm2 * ( fs2 + mu ) * ( fs2 + nu ) / nm;
        b1 = nm2 * 2 * ( mu * nu - fs2 * fs2 ) / nm;
        b2 = nm2 * ( fs2 - mu ) * ( fs2 - nu ) / nm;
    }
    else if ( nzeros == 1 )
    {
        _npoles = 1;
        _nzeros = 1;
        double epsilon( -r1 );
        double mu( -r0 );
        double nm = -( fs2 + epsilon );
        a1 = ( fs2 - epsilon ) / nm;
        a2 = 0;
        b0 = -( fs2 + mu ) / nm;
        b1 = ( fs2 - mu ) / nm;
        b2 = 0;
    }
    else if ( nzeros == 0 )
    {
        _npoles = 2;
        _nzeros = 0;
        double epsilon( -r0 );
        double phi( -r1 );
        double nm = ( fs2 + epsilon ) * ( fs2 + phi );
        a1 = 2 * ( epsilon * phi - fs2 * fs2 ) / nm;
        a2 = ( fs2 - epsilon ) * ( fs2 - phi ) / nm;
        b0 = 1.0 / nm;
        b1 = 2.0 / nm;
        b2 = 1.0 / nm;
    }
    else
    {
        cerr << "ERROR: two real root init function inappropriate" << endl;
        _section_loaded = 0;
        return -1;
    }
    // set period
    _period = 1 / fs;
    _period_known = true;
    // set history buffers
    reset( );
    // set filter loaded bit
    _section_loaded = 1;
    return 0;
}

int
IIRSos::init( int nzeros, double r, double fs )
{
    double fs2 = fs * 2.0;
    if ( nzeros == 1 )
    {
        _npoles = 1;
        _nzeros = 1;
        cout << "warning: inserting extra pole at Nyquist frequency" << endl;
        double epsilon( pi * fs ); // inserted pole at fs
        double mu( -r ); //real zero
        double nm( -( fs2 + epsilon ) );
        a1 = ( fs2 - epsilon ) / nm;
        a2 = 0;
        double nm2 = fs * 0.5; //  = epsilon / (2*pi)
        b0 = nm2 * -( fs2 + mu ) / nm;
        b1 = nm2 * ( fs2 - mu ) / nm;
        b2 = 0;
    }
    else if ( nzeros == 0 )
    {
        _npoles = 1;
        _nzeros = 0;
        double epsilon( -r ); // real pole
        double nm( -( fs2 + epsilon ) );
        a1 = ( fs2 - epsilon ) / nm;
        a2 = 0;
        b0 = 1 / nm;
        b1 = 1 / nm;
        b2 = 0;
    }
    else
    {
        cerr << "ERROR: one real root init fuction inappropriate" << endl;
        _section_loaded = 0;
        return -1;
    }
    // set period
    _period = 1 / fs;
    _period_known = true;
    // set history buffers
    reset( );
    // set filter loaded bit
    _section_loaded = 1;
    return 0;
}

int
IIRSos::dump( ostream& output ) const
{
    output << "b0 = " << b0 << endl
           << "b1 = " << b1 << endl
           << "b2 = " << b2 << endl
           << "a1 = " << a1 << endl
           << "a2 = " << a2 << endl;
    return 0;
}

int
IIRSos::apply( double data_in, double* data_out )
{
    if ( !_section_loaded )
        throw runtime_error( "IIRSOS::apply: Not initialized" );

    //---------------------------------  Calculate using selected mode.
    switch ( _evmode )
    {
    case sos_df2:
        //------------------------------  apply single second order section
        //                                direct form II
        *data_out =
            step_df2( data_in, _hzminusone, _hzminustwo, a1, a2, b0, b1, b2 );
        break;

    case sos_lnf:
        //-------------------------------  Transposed direct form II (LNF)
        *data_out =
            step_lnf( data_in, _hzminusone, _hzminustwo, a1, a2, b0, b1, b2 );
        break;

    case sos_lnf2:
        //-------------------------------  Low noise form II (LNF2)
        *data_out = step_lnf2(
            data_in, _hzminusone, _hzminustwo, a1, a2, b0, b1 - a1, b2 - a2 );
        break;

    case sos_hpc:
        //-------------------------------  Transposed direct form II (LNF)
        *data_out =
            step_hpc( data_in, _hzminusone, _hzminustwo, a1, a2, b0, b1, b2 );
    }

    // return
    return 0;
}

int
IIRSos::apply( double* pdata, int ndata )
{
    if ( !_section_loaded )
        throw runtime_error( "IIRSOS::apply: Not initialized" );

    double hzm1( _hzminusone ), hzm2( _hzminustwo );
    switch ( _evmode )
    {
    case sos_df2:
        //------------------------------  apply single second order section
        //                                direct form II
        for ( int i = 0; i < ndata; ++i )
        {
            pdata[ i ] = step_df2( pdata[ i ], hzm1, hzm2, a1, a2, b0, b1, b2 );
        }
        break;

    case sos_lnf:
        //-------------------------------  Transposed direct form II (LNF)
        for ( int i = 0; i < ndata; ++i )
        {
            pdata[ i ] = step_lnf( pdata[ i ], hzm1, hzm2, a1, a2, b0, b1, b2 );
        }
        break;

    case sos_lnf2:
        //-------------------------------  Low noise form II (LNF2)
        {
            double d1 = b1 / b0 - a1;
            double d2 = b2 / b0 - a2;
            for ( int i = 0; i < ndata; ++i )
            {
                pdata[ i ] =
                    step_lnf2( pdata[ i ], hzm1, hzm2, a1, a2, b0, d1, d2 );
            }
        }
        break;

    case sos_hpc:
        //-------------------------------  High precision DF2.
        for ( int i = 0; i < ndata; ++i )
        {
            pdata[ i ] = step_hpc( pdata[ i ], hzm1, hzm2, a1, a2, b0, b1, b2 );
        }
    }
    _hzminusone = hzm1;
    _hzminustwo = hzm2;
    return 0;
}

int
IIRSos::apply( float* pdata, int ndata )
{
    if ( !_section_loaded )
        throw runtime_error( "IIRSOS::apply: Not initialized" );

    double hzm1( _hzminusone ), hzm2( _hzminustwo );

    switch ( _evmode )
    {
    case sos_df2:
        //------------------------------  apply single second order section
        //                                direct form II
        for ( int i = 0; i < ndata; ++i )
        {
            pdata[ i ] = step_df2( pdata[ i ], hzm1, hzm2, a1, a2, b0, b1, b2 );
        }
        break;

    case sos_lnf:
        //-------------------------------  Transposed direct form II (LNF)
        for ( int i = 0; i < ndata; ++i )
        {
            pdata[ i ] = step_lnf( pdata[ i ], hzm1, hzm2, a1, a2, b0, b1, b2 );
        }
        break;

    case sos_lnf2:
        //-------------------------------  Low noise form II (LNF2)
        {
            double d1 = b1 - a1;
            double d2 = b2 - a2;
            for ( int i = 0; i < ndata; ++i )
            {
                pdata[ i ] =
                    step_lnf2( pdata[ i ], hzm1, hzm2, a1, a2, b0, d1, d2 );
            }
        }
        break;

    case sos_hpc:
        //-------------------------------  High precision DF2
        for ( int i = 0; i < ndata; ++i )
        {
            pdata[ i ] = step_hpc( pdata[ i ], hzm1, hzm2, a1, a2, b0, b1, b2 );
        }
    }
    _hzminusone = hzm1;
    _hzminustwo = hzm2;
    return 0;
}

void
IIRSos::reset( void )
{
    _hzminusone = 0;
    _hzminustwo = 0;
}

void
IIRSos::select_eval( ev_mode mode )
{
    _evmode = mode;
    reset( );
}

bool
IIRSos::test( void ) const
{
    return !isfinite( _hzminusone ) || !isfinite( _hzminustwo );
}

dComplex
IIRSos::H( const dComplex& iz ) const
{
    dComplex iz2 = iz * iz;
    return ( dComplex( b0, 0 ) + b1 * iz + b2 * iz2 ) /
        ( dComplex( 1, 0 ) + a1 * iz + a2 * iz2 );
    //return ((iz*b2 + dComplex(b1))*iz + dComplex(b0)) /
    //       ((iz*a2 + dComplex(a1))*iz + dComplex(1.0));
}
