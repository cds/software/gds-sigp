/************************************************************/
/*                                                          */
/* Module Name: RTXCorr                                     */
/*                                                          */
/* Module Description, wrapper for iterative cross          */
/*                     correlation and line subtraction     */
/*                                                          */
/* Ed Daw, 17th November 2008                               */
/* e.daw@shef.ac.uk                                         */
/* +44 114 222 4353                                         */
/*                                                          */
/* Comments: This header file should generate its own       */
/* documentation on typing doc++ < header file name >       */
/* See gds tree build instructions on how to download doc++ */
/*                                                          */
/************************************************************/

#ifndef _RTXCOR_H
#define _RTXCOR_H

#include "FilterBase.hh"
#include "rtcor.h"
#include "TSeries.hh"
#include "Time.hh"
#include "Interval.hh"
#include "DVector.hh"
#include "FSeries.hh"
#include "FilterIO.hh"

/** @name Enumerated type for correlator
 */

/*@{*/

/** Enumerated type 
    @memo Selects the cross correlation algorithm
 */
enum rtxtype
{
    /** wrapped - lags are cyclic permutations */
    kWrapped = 0,
    /** unwrapped - lags are sample shifts */
    kUnwrapped = 1,
    /** unset - type not set yet */
    kNotSet = 2
};

/*@}*/

/** @name RTXCor
    @memo Wrapper for C based real time cross correlation algorithms
    @author Ed Daw
*/

/*@{*/

class RTXCorr : public FilterBase
{

public:
    using FilterBase::isDataValid;

    /** Default constructor
   * @memo Default constructor
   */
    RTXCorr( void );

    /** Constructor from initial data streams, the required number
   * of lags and the type of cross correlation.
   */
    RTXCorr( int nlags, double* initdatax, double* initdatay, rtxtype type );

    /** Constructor from initial data streams, the required number of
   * lags, the length of data segments to be used in the cross 
   * correlation in number of samples, and the type of cross
   * correlation.
   */
    RTXCorr( int     nlags,
             int     ncor,
             double* initdatax,
             double* initdatay,
             rtxtype type );

    /** Constructor from initial time series, the required number
   * of lags, and the type of cross correlation to be performed.
   * this constructor sets up a wrapped cross correlation.
   */
    RTXCorr( const TSeries& xin, const TSeries& xout, int nlags );

    /** Copy constructor.
   */
    RTXCorr( const RTXCorr& f );

    /** Destructor.
   */
    ~RTXCorr( void );

    /** Get number of samples required for initialization
   */
    int GetInitNsamplesRequired( void ) const;

    /** Duplicate an RTX Correlator in all but the contents of the
   * internal buffers and the memory locations used to store them.
   */
    RTXCorr* clone( void ) const;

    /** Reset the filter, emptying memory buffers and 
   */
    void reset( void );

    /** Returns true if filter is in use, meaning that
   * the filter type and internal data buffers have
   * been defined.
   */
    bool inUse( void ) const;

    /** Apply method with TSeries objects at input. 
   */
    int apply( const TSeries& xin, const TSeries& yin, double* out );

    /** Apply method to two doubles. Return a pointer to an array
   *  of correlation lag outputs.
   */
    int apply( double xin, double yin );

    /** Get a pointer to the correlation output data
   */
    double* getCorrPtr( void );

    /** Apply method with FilterIO objects. Not implemented!
   */
    FilterIO& apply( const FilterIO& in );

    /** Operator for applying the filter. Not Implemented !
   */
    FilterIO& operator( )( const FilterIO& in );

    /** Operator for validating input data. Not implemented!
   */
    void dataCheck( const FilterIO& in ) const;

    /** Return a KeyChain for the input timeseries. Not implemented!
   */
    virtual const KeyChain& getInputKeys( void ) const;

    /** Return a KeyChain for the output timeseries. Not implemented!
   */
    virtual const KeyChain& getOutputKeys( void ) const;

    /** Get the time of the first sample to be processed. Not implemented!
   */
    Time getStartTime( void ) const;

    /** Get the moment of the next expected sample. Throws
   * an exception if the filter has yet to be used. Not implemented!
   */
    Time getCurrentTime( void ) const;

    /** Validate input data without throwing exceptions
   */
    //bool isDataValid(const FilterIO& in) const;

    /*@}*/

private:
    bool      _in_use;
    rtxtype   _type;
    rtcordata _rtdata;
};

#ifndef __CINT__

// unimplemented items from FilterBase
inline FilterIO&
RTXCorr::apply( const FilterIO& in )
{
    throw std::runtime_error( "not implemented" );
}
inline FilterIO&
RTXCorr::operator( )( const FilterIO& in )
{
    throw std::runtime_error( "not implemented" );
}
inline void
RTXCorr::dataCheck( const FilterIO& in ) const
{
    throw std::runtime_error( "not implemented" );
}
inline const KeyChain&
RTXCorr::getInputKeys( void ) const
{
    throw std::runtime_error( "not implemented" );
}
inline const KeyChain&
RTXCorr::getOutputKeys( void ) const
{
    throw std::runtime_error( "not implemented" );
}
inline Time
RTXCorr::getStartTime( void ) const
{
    throw std::runtime_error( "not implemented" );
}

inline Time
RTXCorr::getCurrentTime( void ) const
{
    throw std::runtime_error( "not implemented" );
}

#endif // __CINT__

#endif // _RTCOR_H
