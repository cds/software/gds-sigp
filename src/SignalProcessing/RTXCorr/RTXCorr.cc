// RTXCorr correlation class definitions
// Ed Daw, 18th November 2008

// standard C++ includes
#include <iostream>
// dmt includes
#include "RTXCorr.hh"
// dmt correlation includes
#include "rtcor.h"

// default constructor
RTXCorr::RTXCorr( void ) : _in_use( false ), _type( kNotSet )
{
}

// Constructor where number of lags equals length of data segment.
// Can be used for both wrapped and unwrapped correlations.
RTXCorr::RTXCorr( int     nlags,
                  double* initdatax,
                  double* initdatay,
                  rtxtype xcorrtype )
{
    if ( xcorrtype == kWrapped )
    {
        _type = kWrapped;
        rtcor_constructor( nlags, initdatax, initdatay, &_rtdata );
        _in_use = true;
    }
    else if ( xcorrtype == kUnwrapped )
    {
        _type = kUnwrapped;
        int ncor = nlags;
        urtcor_constructor( ncor, nlags, initdatax, initdatay, &_rtdata );
        _in_use = true;
    }
    else
    {
        _type = kNotSet;
        _in_use = false;
    }
}

// Constructor where the number of lags does not equal the length
// of the data segment. Only applicable to unwrapped correlations.
RTXCorr::RTXCorr( int     nlags,
                  int     ncor,
                  double* initdatax,
                  double* initdatay,
                  rtxtype xcorrtype )
{
    if ( xcorrtype == kWrapped )
    {
        _type = kNotSet;
        _in_use = false;
    }
    else if ( xcorrtype == kUnwrapped )
    {
        _type = kUnwrapped;
        urtcor_constructor( ncor, nlags, initdatax, initdatay, &_rtdata );
        _in_use = true;
    }
    else
    {
        _type = kNotSet;
        _in_use = false;
    }
}

// Copy constructor
RTXCorr::RTXCorr( const RTXCorr& f )
{
    _in_use = f._in_use;
    _type = f._type;
    if ( _type == kUnwrapped )
    {
        urtcor_copystruct( &_rtdata, f._rtdata );
    }
    if ( _type == kWrapped )
    {
        rtcor_copystruct( &_rtdata, f._rtdata );
    }
}

// Constructor from timeseries inputs
RTXCorr::RTXCorr( const TSeries& tsx, const TSeries& tsy, int nlags )
{
    if ( ( nlags != (int)tsx.getNSample( ) ) ||
         ( nlags != (int)tsy.getNSample( ) ) )
    {
        _type = kNotSet;
        _in_use = false;
    }
    else
    {
        TSeries* xconv;
        TSeries* yconv;
        bool     xset;
        bool     yset;
        if ( tsx.refDVect( )->getType( ) == DVector::t_float )
        {
            TSeries databuffer( tsx );
            databuffer.Convert( DVector::t_double );
            xconv = new TSeries( databuffer );
            xset = true;
        }
        else
        {
            xconv = new TSeries( tsx );
            xset = true;
        }
        if ( tsy.refDVect( )->getType( ) == DVector::t_float )
        {
            TSeries databuffer( tsy );
            databuffer.Convert( DVector::t_double );
            yconv = new TSeries( databuffer );
            yset = true;
        }
        else
        {
            yconv = new TSeries( tsy );
            yset = true;
        }
        rtcor_constructor( nlags,
                           (double*)xconv->refData( ),
                           (double*)yconv->refData( ),
                           &_rtdata );
        _type = kWrapped;
        _in_use = true;
        if ( xset )
        {
            delete xconv;
        }
        if ( yset )
        {
            delete yconv;
        }
    }
}

// Destructor
RTXCorr::~RTXCorr( void )
{
    reset( );
}

// Get the number of samples required for initialization
int
RTXCorr::GetInitNsamplesRequired( void ) const
{
    switch ( _type )
    {
    case kUnwrapped:
        return _rtdata.nlags + _rtdata.ncor - 1;
    case kWrapped:
        return _rtdata.nlags;
    default:
        return -1;
    }
}

// Clone an RTXCorr filter
RTXCorr*
RTXCorr::clone( void ) const
{
    return new RTXCorr( *this );
}

// Reset an RTXCorr filter
void
RTXCorr::reset( void )
{
    if ( _type == kWrapped )
    {
        rtcor_destructor( &_rtdata );
    }
    if ( _type == kUnwrapped )
    {
        urtcor_destructor( &_rtdata );
    }
    _in_use = false;
    _type = kNotSet;
}

// In use check
bool
RTXCorr::inUse( void ) const
{
    return _in_use;
}

// Apply the filter to a timeseries of a large number of data samples.
int
RTXCorr::apply( const TSeries& xin, const TSeries& yin, double* out )
{
    if ( xin.getNSample( ) != yin.getNSample( ) )
    {
        std::cout << "X and Y timeseries should be of equal length"
                  << std::endl;
        return -1;
    }
    else
    {
        double* xref = (double*)xin.refData( );
        double* yref = (double*)yin.refData( );
        for ( int i = 0; i < (int)xin.getNSample( ); ++i )
        {
            rtcor_iterate( xref[ i ], yref[ i ], &_rtdata );
            rtcor_copy( out + i * ( _rtdata.nlags ), &_rtdata );
        }
        return 0;
    }
}

// Apply the filter to a single data sample from each channel.
int
RTXCorr::apply( double xin, double yin )
{
    rtcor_iterate( xin, yin, &_rtdata );
    return 0;
}

// return a pointer to the correlation results
double*
RTXCorr::getCorrPtr( void )
{
    return _rtdata.corrmem;
}
