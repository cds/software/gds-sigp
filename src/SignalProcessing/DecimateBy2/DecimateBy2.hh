#ifndef DECIMATEBY2_HH
#define DECIMATEBY2_HH

#include "Pipe.hh"
#include "Time.hh"
#include "Interval.hh"
#include "TSeries.hh"

/**  The DecimateBy2 class decimates a TSeries by 2^N. The TSeries 
  *  data are filtered before decimation to remove aliasing. This class
  *  is essentially a wrapper of the C code written for LIGO GDS by Peter
  *  Fritschel.
  *
  *  DecimateBy2 is based on the DMT Pipe class and as such is can
  *  be treated as a generic DMT Filter. The decimation may be applied to
  *  the TSeries with either the apply method or the parentheses operator.
  *  These methods return a TSeries of the same type as the input series 
  *  for any supported data type (float, double, single-precieion complex or 
  *  double-precicion complex) and a single precision floating point TSeries 
  *  for all other input series data types.
  *
  *  Four anti-aliasing low-pass FIR filters are available, each of which
  *  cuts off at \f$0.9 F_{Nyquist}\f$. The options are tabulated below:
  *  <table>
  *  <tr><td>ID</td><td>Order</td>
  *      <td> Comments </td><td>
  *  <tr><td>1</td><td>42</td>
  *      <td>Least-Squares design, ripple ~.015 (-0.2 dB)</td></tr>
  *  <tr><td>2</td><td>42</td>
  *      <td>Equiripple design, ripple ~0.06 dB</td></tr>
  *  <tr><td>3</td><td>22</td>
  *      <td>Least-squares design, ripple ~0.1 (-1 dB)</td></tr>
  *  <tr><td>4</td><td>82</td>
  *      <td>Least-squares design, ripple ~0.0006 (-0.01 dB)</td></tr>
  *  </table>
  *  The antialiasing filters add an effective delay of \f$N*O_F/2\f$, where
  *  N is the number of stages (the decimation factor exponent) and \f$O_F\f$
  *  is the filter order. This delay is NOT added to the TSeries start time.
  *  @memo Decimation by \f$2^N\f$.
  *  @version 1.2 ; Modified May 14, 2004
  *  @author John G. Zweizig, based on C function written by P. Fritschel
  */
class DecimateBy2 : public Pipe
{
public:
    using Pipe::apply;
    using Pipe::dataCheck;
    using Pipe::isDataValid;

    /**  Build an empty Decimator.
    *  @memo Default constructor.
    */
    DecimateBy2( void );

    /**  Construct a decimator and specify the decimation factor (the radix-2
    *  logaritm, i.e. the number of times the input series is to be decimated)
    *  and the anti-aliasing filter selection.
    *  @memo Constructor.
    *  @param N Number of stages.
    *  @param FilterID Type code of anti-aliasing filter.
    */
    explicit DecimateBy2( int N, int FilterID = 1 );

    /**  Build a Decimator as specified identical to the argument.
    *  @memo Copy constructor.
    *  @param x Decimation filter to be copied.
    */
    DecimateBy2( const DecimateBy2& x );

    /**  Build a Decimator as specified identical to the argument.
    *  @memo Copy constructor.
    *  @param x Decimation filter to be copied.
    *  @return Reference to the modified decimation filter.
    */
    DecimateBy2& operator=( const DecimateBy2& x );

    /**  Destroy the DecimateBy2 object and release the function storage.
    *  @memo Virtual destructor.
    */
    ~DecimateBy2( void );

    /**  Clone a decimation filter.
    *  @memo Clone the filter.
    *  @return Pointer to the cloned object.
    */
    DecimateBy2* clone( void ) const;

    /**  The argument time series is filtered to remove aliasing, and 
    *  decimated by 2^N. The argument TSeries is left unchanged.
    *  @memo Return a Decimated TSeries.
    *  @param ts Time series to be decimated.
    *  @return Decimated time series.
    */
    TSeries apply( const TSeries& ts );

    /**  Check the data for validity. If the data are not applicable for 
    *  this DecimateBy2, an exception is thrown.
    *  @param ts Time series to be checked as imput to the filter.
    *  @exception runtime_error Data start time or sample rate is incorrect.
    */
    void dataCheck( const TSeries& ts ) const;

    /**  Check the data for validity. Performs the same data checks as 
    *  dataCheck() but returns a boolean status instead f throwing an
    *  exception.
    *  @param ts Time series to be checked as imput to the filter.
    *  @return True if data start time and sample rate are correct.
    */
    bool isDataValid( const TSeries& ts ) const;

    /**  The Decimate by 2 filter may be used to decimate by an arbitrary 
    *  power of 2. This specifes the decimation factor (as the radix-2
    *  logaritmm, i.e. the number of times the input series is to be 
    *  decimated) and the anti-aliasing filter selection. Note that if 
    *  number of stages is set to a number less than 1, a single stage is
    *  employed.
    *  @memo Set the number of decimate-by-2 steps to perform on input series.
    *  @param N Number of stages (\c \>\ 0 ).
    *  @param FilterID Type code of anti-aliasing filter.
    */
    void setDecimation( int N, int FilterID = 1 );

    /**  Clear/release the internal history vector and reset the current 
    *  time. The start time is reaset to zero and the InUse() flag is reset.
    *  @memo Reset the filter state and history.
    */
    void reset( void );

    /**  Get time of first processed data.
    *  @memo Get start time
    *  @return Time of first sample processeed after filter was constructed 
    *          or reset.
    */
    Time getStartTime( void ) const;

    /**  Get time of next input sample expected to be processed by this filter.
    *  @memo get next sample time.
    *  @return Time of next input sample expected to be processed.
    */
    Time getCurrentTime( void ) const;

    /**  Test whether filter is in use. The filter is considered to be in 
    *  use if at leas one sample has been processed since the filter was 
    *  constructed or reset.
    *  @memo test if in use.
    *  @return True if the filter is in use.
    */
    bool inUse( void ) const;

    /**  Return the phase difference resulting from the FIR antialiasing 
    *  filters \e i.e. \f$2 \pi \Delta t\f$.
    *  @return The difference between the input- and output- signal phase.
    */
    double getPhase( void ) const;

    /**  Return the time difference resulting from the FIR antialiasing 
    *  filters
    *  @return The time difference between the input- and output- signal phase.
    */
    Interval getTimeDelay( void ) const;

private:
    /**  List of data types that can be used. Any type other than 
    *  those enumerated are converted to float.
    */
    enum mode_id
    {
        t_none,
        t_float,
        t_double,
        t_fComplex,
        t_dComplex
    };

private:
    void        rmTemp( void );
    void        setMode( mode_id mode, int len );
    const char* testData( const TSeries& ts ) const;

private:
    int mPower2;
    int mFilterID;
    union
    {
        fComplex* c;
        dComplex* w;
        double*   d;
        float*    f;
        void*     v;
    } mFilterState;
    Time     mCurTime;
    Time     mStartTime;
    Interval mSample;
    mode_id  mMode;
    void*    mTemp;
    int      mLTemp;
};

inline Time
DecimateBy2::getStartTime( void ) const
{
    return mStartTime;
}

inline Time
DecimateBy2::getCurrentTime( void ) const
{
    return mCurTime;
}

inline bool
DecimateBy2::inUse( void ) const
{
    return ( mStartTime != Time( 0 ) );
}

#endif // DecimateBy2_HH
