/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "DecimateBy2.hh"
#include "TSeries.hh"
#include "DVector.hh"
#include "decimate.hh"
#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif
#include <cmath>

//======================================  Constructors
DecimateBy2::DecimateBy2( void )
    : mPower2( 1 ), mFilterID( 1 ), mCurTime( 0 ), mStartTime( 0 ),
      mSample( 0.0 ), mMode( t_none ), mTemp( 0 ), mLTemp( 0 )
{
    mFilterState.v = 0;
}

DecimateBy2::DecimateBy2( int N, int FilterID )
    : mCurTime( 0 ), mStartTime( 0 ), mSample( 0.0 ), mMode( t_none ),
      mTemp( 0 ), mLTemp( 0 )
{
    mFilterState.v = 0;
    setDecimation( N, FilterID );
}

DecimateBy2::DecimateBy2( const DecimateBy2& x ) : mMode( t_none )
{
    mFilterState.v = 0;
    *this = x;
}

DecimateBy2&
DecimateBy2::operator=( const DecimateBy2& x )
{
    if ( this != &x )
    {
        reset( );
        mPower2 = x.mPower2;
        mFilterID = x.mFilterID;
        mSample = x.mSample;
    }
    return *this;
}

//======================================  Clone a filter
DecimateBy2*
DecimateBy2::clone( void ) const
{
    return new DecimateBy2( *this );
}

//======================================  Release temporary storage
void
DecimateBy2::rmTemp( void )
{
    switch ( mMode )
    {
    case t_none:
        break;
    case t_float:
        delete[]( float* ) mTemp;
        break;
    case t_double:
        delete[]( double* ) mTemp;
        break;
    case t_fComplex:
        delete[]( fComplex* ) mTemp;
        break;
    case t_dComplex:
        delete[]( dComplex* ) mTemp;
        break;
    }
    mMode = t_none;
    mTemp = 0;
    mLTemp = 0;
}

//======================================  Set the data type and length
void
DecimateBy2::setMode( mode_id Mode, int Length )
{
    if ( mMode != Mode || mLTemp < Length )
    {
        rmTemp( );
        switch ( Mode )
        {
        case t_float:
            mTemp = new float[ Length ];
            break;
        case t_double:
            mTemp = new double[ Length ];
            break;
        case t_fComplex:
            mTemp = new fComplex[ Length ];
            break;
        case t_dComplex:
            mTemp = new dComplex[ Length ];
            break;
        default:
            break;
        }
        mMode = Mode;
        if ( mTemp )
            mLTemp = Length;
    }
}

//======================================  Destructors
DecimateBy2::~DecimateBy2( )
{
    reset( );
}

//======================================  Decimate a time series
TSeries
DecimateBy2::apply( const TSeries& ts )
{
    //----------------------------------  Check the input data.
    unsigned int Ninput = ts.getNSample( );
    if ( !Ninput )
        return TSeries( mCurTime, mSample );

    //----------------------------------  Check the input data.
    dataCheck( ts );
    mSample = ts.getTStep( );
    if ( !mStartTime )
        mStartTime = ts.getStartTime( );

    //----------------------------------  Do the decimation.
    unsigned long nDec = Ninput >> mPower2;
    unsigned long dec_Factor = 1 << mPower2;
    Time          tStart = ts.getStartTime( );
    TSeries       r;
    if ( ts.refDVect( )->C_data( ) )
    {
        setMode( t_fComplex, Ninput );
        fComplex* Xc = (fComplex*)mTemp;
        ts.getData( Ninput, Xc );
        decimate< fComplex >( mFilterID,
                              Xc,
                              Xc,
                              Ninput,
                              mPower2,
                              mFilterState.c,
                              &mFilterState.c );
        r.setData( tStart, mSample * double( dec_Factor ), Xc, nDec );
    }
    else if ( ts.refDVect( )->W_data( ) )
    {
        setMode( t_dComplex, Ninput );
        dComplex* Xc = (dComplex*)mTemp;
        ts.getData( Ninput, Xc );
        decimate< dComplex >( mFilterID,
                              Xc,
                              Xc,
                              Ninput,
                              mPower2,
                              mFilterState.w,
                              &mFilterState.w );
        r.setData( tStart, mSample * double( dec_Factor ), Xc, nDec );
    }
    else if ( ts.refDVect( )->D_data( ) )
    {
        setMode( t_double, Ninput );
        double* Xd = (double*)mTemp;
        ts.getData( Ninput, Xd );
        decimate< double >( mFilterID,
                            Xd,
                            Xd,
                            Ninput,
                            mPower2,
                            mFilterState.d,
                            &mFilterState.d );
        r.setData( tStart, mSample * double( dec_Factor ), Xd, nDec );
    }
    else
    {
        setMode( t_float, Ninput );
        float* Xf = (float*)mTemp;
        ts.getData( Ninput, Xf );
        decimate< float >( mFilterID,
                           Xf,
                           Xf,
                           Ninput,
                           mPower2,
                           mFilterState.f,
                           &mFilterState.f );
        r.setData( tStart, mSample * double( dec_Factor ), Xf, nDec );
    }
    r.setName( ts.getName( ) );
    r.setF0( ts.getF0( ) );

    //----------------------------------  Set the final state;
    mCurTime = ts.getEndTime( );
    return r;
}

const char*
DecimateBy2::testData( const TSeries& ts ) const
{

    //----------------------------------  Check data type match
    bool tOK = false;
    switch ( mMode )
    {
    case t_none:
        tOK = true;
        break;
    case t_float:
        tOK = !ts.refDVect( )->C_data( ) && !ts.refDVect( )->D_data( ) &&
            !ts.refDVect( )->W_data( );
        break;
    case t_double:
        tOK = ts.refDVect( )->D_data( );
        break;
    case t_fComplex:
        tOK = ts.refDVect( )->C_data( );
        break;
    case t_dComplex:
        tOK = ts.refDVect( )->W_data( );
        break;
    }
    if ( !tOK )
        return "Wrong data type!";

    //----------------------------------  Check frequency.
    if ( mSample != Interval( 0.0 ) && mSample != ts.getTStep( ) )
    {
        return "Wrong frequency";
    }

    //----------------------------------  Check history data valid.
    if ( mCurTime != Time( 0 ) && ts.getStartTime( ) != mCurTime )
    {
        return "Wrong start time";
    }
    return 0;
}

void
DecimateBy2::dataCheck( const TSeries& ts ) const
{
    const char* msg = testData( ts );
    if ( msg )
        throw std::invalid_argument( msg );
}

bool
DecimateBy2::isDataValid( const TSeries& ts ) const
{
    return !testData( ts );
}

double
DecimateBy2::getPhase( void ) const
{
    return firphase( mFilterID, 1 << mPower2 );
}

Interval
DecimateBy2::getTimeDelay( void ) const
{
    return mSample * getPhase( ) / ( 2.0 * M_PI );
}

void
DecimateBy2::setDecimation( int N, int FilterID )
{
    reset( );
    if ( N <= 0 )
        N = 1;
    mPower2 = N;
    mFilterID = FilterID;
}

void
DecimateBy2::reset( void )
{
    if ( mFilterState.v )
    {
        switch ( mMode )
        {
        case t_float: {
            float* zF( 0 );
            decimate< float >(
                mFilterID, zF, zF, 0, mPower2, mFilterState.f, (float**)0 );
            break;
        }
        case t_double: {
            double* zD( 0 );
            decimate< double >(
                mFilterID, zD, zD, 0, mPower2, mFilterState.d, (double**)0 );
            break;
        }
        case t_fComplex: {
            fComplex* zC( 0 );
            decimate< fComplex >(
                mFilterID, zC, zC, 0, mPower2, mFilterState.c, (fComplex**)0 );
            break;
        }
        case t_dComplex: {
            dComplex* zW( 0 );
            decimate< dComplex >(
                mFilterID, zW, zW, 0, mPower2, mFilterState.w, (dComplex**)0 );
            break;
        }
        default:
            break;
        }
    }
    mFilterState.v = 0;
    mCurTime = Time( 0 );
    mStartTime = Time( 0 );
    mSample = 0.0;
    rmTemp( );
}
