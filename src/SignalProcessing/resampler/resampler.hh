/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef RESAMPLER_HH
#define RESAMPLER_HH

#include "NullPipe.hh"
#include "autopipe.hh"
#include "FIRdft.hh"
#include "Time.hh"

/**  Resample a data stream with FIR anti-aliasing filter. The resampled data 
  *  are constrained to have the same phase as the original data and the time
  *  delay will be an integer number of output samples. 
  *
  *  The resampling algorithm proceeds as follows:
  *  1) the resampling factor is decomposed into integer upsampling and
  *     downsampling factors.
  *  2) The signal is upsampled using the selected interpolation method.
  *  3) an antialiasing filter is applied to the upsampled time series.
  *  4) signal is down-sampled by the specified decimation factor.
  *  \brief Resample a time series.
  *  \author John Zweizig <john.zweizig@ligo.org>
  *  \version 0.1
  */
class resampler : public NullPipe
{
public:
    /**  Enumerate the resampling modes
    */
    enum upsample_mode
    {
        kup_spline, ///< cubic spline interpolation
        kup_sparse, ///< Sparse upsampling
        kup_repeat ///< upsample by repetition.
    };

    /**  AA filter parameter structure.
    */
    struct filter_pars
    {
        size_t m_order;
        double m_fmax;
        double m_bw;
        double m_att_dB;
    };

public:
    /**  Default resampler constructor.
     *  \brief Default constructor.
     */
    resampler( void );

    /**  Initializing resampler constructor.
     *  \brief Default constructor.
     *  \param to_rate Sample rate of resampled series.
     *  \param aafilt  Use a kaiser FIR anitaliasing filter after upsampling
     */
    resampler( double to_rate, bool aafilt = true );

    /**  Construct an exact copy of the specified resampler instance.
     *  \brief copy constructor
     *  \param x resampler to be copied.
     */
    resampler( const resampler& x ) = default;

    /**  Construct an exact copy of the specified resampler instance.
     *  \brief copy constructor
     *  \param x resampler to be copied.
     */
    resampler( resampler&& x ) = default;

    /**  Resampler destructor.
     *  \brief Destructor.
     */
    virtual ~resampler( void ) = default;

    /**  Construct an exact copy of the specified resampler instance.
     *  \brief copy constructor
     *  \param x resampler to be copied.
     */
    resampler& operator=( const resampler& x ) = default;

    /**  Clone a resampler instance.
     *  \brief Clone clone this resampler.
     *  \returns Pointer to resampler clone.
     */
    resampler* clone( void ) const;

    /**  Resample the input time series. 
     *  Synonymous with operator() method.
     *  \brief Resample an 
     *  \param in The TSeries being acted on.
     *  \return Resample time series.
     */
    virtual TSeries apply( const TSeries& in );

    /**  Calculate the time delay imposed by this filter.
     *  \brief delay time.
     *  \return Delay time.
     */
    Interval getTimeDelay( void ) const;

    /** Construct a filter as defined by the filter parameters.
    */
    void make_filter( void );

    /** Reset the filter history.
     * \brief Reset the filter history.
     */
    virtual void reset( void );

    /**  Set antialiasing filter parameters. The edge argument specifies the
     *  width of the antialiasing filter transition zone in units of the input
     *  signal Nyquist frequency. The atten_dB argument specifies the
     *  attenuation in dB.
     *  \brief Set the antialiasing filter parameters.
     *  \param f    knee frequency.
     *  \param N    number of filter samples.
     *  \param edge Edge width in nyquist units (0.02).
     *  \param dB   Stop band attenualtion (60.0dB).
     */
    void set_filter( double f, size_t N, double edge, double dB );

    /**  Set a filter as described by the specified filter parameters and/or
     *  default parameters parameters.
     *  \brief set a filter.
     */
    void set_filter( bool auto_pars );

    /** Use the argument filter for anti-aliasing/smoothing.
    *  \brief Specify an antialiasing filter.
    *  \param p Anti-aliasing filter. 
    */
    void set_filter( const Pipe& p );

    /** Set the resampling up- and down-factors.
    *  \brief Set the sample factors.
    *  \param to_rate Target sample rate.
    */
    void set_sample_factors( long upfactor, long downfactor = 1 );

    /** Set the target sample rate. The interpolation and decimation stages 
    *  will be set up when the input sample rate is determined.
    *  \brief set the sample rate.
    *  \param to_rate Target sample rate. 
    */
    void set_sample_rate( double to_rate );

    /**  Set the FIR delay calculation mode. The default is to create a
     *  zero-phase-shift filter, but the mode may also be set for the other
     *  fir_mode options
     *  \brief Set the anti-aliasing delay mode.
     *  \param fmode FIR filter delay mode (as int(FIRdft::fir_mode))
     */
    void setMode( int fmode );

    /**  Set the upsampling calculation mode. The default is to use a cubic
     *  spline fit (kup_spline), but the mode may also be set for the other
     *  upsample_mode options
     *  \brief Set the upsample mode.
     *  \param umode Upsample mode enum
     */
    void setUpsampleMode( upsample_mode umode );

    /**  Set the interpolation and antialiasing filters.
    */
    void setup( void );

private:
    /** perform interpolation of the signal by the specified upsampling factor.
    */
    TSeries upsample( const TSeries& ts );
    TSeries downsample( void );

private:
    double        m_sample_rate; ///< target sample rate
    filter_pars   m_filter_pars; ///< anti-aliasing/smoothing filter parameters
    upsample_mode m_upmode; ///< Interpolator mode
    int           m_firmode; ///< Filter delay mode [fm_zero_phase]
    long          m_upfactor; ///< Upsample ratio
    long          m_downfactor; ///< downsample ratio
    auto_pipe     m_interpolate; ///< interpolation pipe
    TSeries       m_aabuffer; ///< Antialiasing buffer.
    auto_pipe     m_antiAlias; ///< antialiasing/smoothing filter.
};

#endif // !defined(RESAMPLER_HH)
