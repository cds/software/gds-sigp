/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "resampler.hh"
#include "DVecType.hh"
#include "gcd.hh"
#include "FIRdesign.hh"
#include "FIRdft.hh"
#include "pipe_tools.hh"
#include "cubic_interpolate.hh"
#include <stdexcept>
#include <iostream>

using namespace std;

//=======================================  Resample default constructor
resampler::resampler( void )
    : m_sample_rate( 0 ), m_upmode( kup_spline ),
      m_firmode( FIRdft::fm_zero_phase ), m_upfactor( 0 ), m_downfactor( 0 )
{
    set_filter( false );
}

//=======================================  Resample initializing constructor
resampler::resampler( double to_rate, bool nyquist )
    : m_upmode( kup_spline ), m_firmode( FIRdft::fm_zero_phase ),
      m_upfactor( 0 ), m_downfactor( 0 )
{
    set_sample_rate( to_rate );
    set_filter( nyquist );
}

//=======================================  Clone this resampler
resampler*
resampler::clone( void ) const
{
    return new resampler( *this );
}

//=======================================  Resample a specified time series
TSeries
resampler::apply( const TSeries& in )
{
    //------------------------------------  set up on first use
    if ( !inUse( ) )
    {
        prep( in );
        setup( );
    }

    //------------------------------------  Otherwise, check for valid input
    else
    {
        prep( in );
    }

    //------------------------------------  start with interpolation
    TSeries ts_upsample = upsample( in );

    //------------------------------------  Apply antialiasing
    if ( !m_antiAlias.null( ) )
    {
        TSeries aaSeries( m_antiAlias( ts_upsample ) );
        if ( m_aabuffer.Append( aaSeries ) )
        {
            cerr << "resampler: aaSeries start: " << aaSeries.getStartTime( )
                 << " aaBuffer end: " << m_aabuffer.getEndTime( )
                 << " input series start: " << ts_upsample.getStartTime( )
                 << endl;
            throw runtime_error(
                "resampler: antialiasing filter alignment error" );
        }
    }
    else
    {
        m_aabuffer.Append( ts_upsample );
    }

    //------------------------------------  Perform decimation
    return downsample( );
}

//====================================  Down-sample
TSeries
resampler::downsample( void )
{
    int      ndecim = m_aabuffer.getNSample( ) / m_downfactor;
    Interval out_len = m_aabuffer.getTStep( ) * double( ndecim * m_downfactor );
    TSeries  r( m_aabuffer.decimate( m_downfactor ) );
    m_aabuffer.eraseStart( out_len );
    return r;
}

//======================================  Time delay
Interval
resampler::getTimeDelay( void ) const
{
    Interval dt( 0.0 );
    if ( !m_interpolate.null( ) )
        dt += m_interpolate->getTimeDelay( );
    if ( !m_antiAlias.null( ) )
        dt += m_antiAlias->getTimeDelay( );
    return dt;
}

//======================================  Construct the requested filter
void
resampler::make_filter( void )
{
    if ( !_step || !m_filter_pars.m_fmax || !m_antiAlias.null( ) )
        return;

    //-----------------------------------  filter parameters
    double Hz = double( m_upfactor ) / _step; //<<< sample rate

    double f = m_filter_pars.m_fmax; //<<< edge frequency
    if ( f < 0 )
    {
        f = 0.5 / double( _step );
        if ( m_downfactor > m_upfactor )
            f *= double( m_upfactor ) / m_downfactor;
    }

    double dB = m_filter_pars.m_att_dB; //<<< Attenuation
    if ( !dB )
        dB = 60.0;

    int    N = m_filter_pars.m_order; //<<< bandwidth
    double bw = m_filter_pars.m_bw;
    if ( !N && !bw )
        bw = 0.02 * f;

    if ( !N )
    { //<<< Calculate filter length
        int stat = FirW( N, 6, 1, f / Hz, 0.0, bw / Hz, dB, NULL );
        if ( stat > 0 )
        {
            //-----------------------------  Preserve the decimated signal phase.
            //   To maintain the sample phasing, the delay must be an integer
            //   multiple of the decimated sample rate, so the filter order
            //   should <filter-order> = 2 * <multiplier> * <decimation-factor>
            //   Include a factor to force delay to an even number of nanoseconds.
            long inHz( 1.0 / double( _step ) + 0.5 );
            long nsFact = inHz / gcd( 1000000000, inHz );
            long dtFact = nsFact * m_downfactor / gcd( nsFact, m_downfactor );
            if ( ( N - 1 ) % ( 2 * dtFact ) != 0 )
            {
                int mult = ( N - 1 ) / ( 2 * dtFact ) + 1;
                N = mult * 2 * dtFact + 1;
            }
        }
    }

    //-----------------------------------  Allocate the coeff. array and design
    DVectD  filter_coefs( N );
    double* h = filter_coefs.refTData( );
    if ( FirW( N, 6, 1, f / Hz, 0.0, bw / Hz, dB, h ) != 0 )
    {
        throw runtime_error( "resampler::filter FirW failed" );
    }
    cout << "resampler: Design FirW, order: " << N - 1 << endl;
    FIRdft* filt = new FIRdft( N - 1, 1.0 / double( _step ) );
    filt->setCoefs( N, filter_coefs.refTData( ) );
    filt->setMode( FIRdft::fir_mode( m_firmode ) );
    m_antiAlias.set( filt );
}

//====================================  reset the filter
void
resampler::reset( void )
{
    NullPipe::reset( );
    m_interpolate.reset( );
    m_antiAlias.reset( );
    m_aabuffer.Clear( );
}

//======================================  Set anti-aliasing filter parameters
void
resampler::set_filter( double f, size_t N, double bw, double dB )
{
    m_filter_pars.m_order = N;
    m_filter_pars.m_fmax = f;
    m_filter_pars.m_bw = bw;
    m_filter_pars.m_att_dB = dB;
}

void
resampler::set_filter( bool nyquist )
{
    if ( nyquist )
        set_filter( -1, 0, 0, 0 );
    else
        set_filter( 0, 0, 0, 0 );
}

//======================================  Set anti-aliasing filter parameters
void
resampler::setMode( int fmode )
{
    m_firmode = fmode;
    if ( m_antiAlias )
        set_fir_mode( m_antiAlias.get( ), m_firmode );
}

//======================================  set target sample rate
void
resampler::set_sample_rate( double to_rate )
{
    m_sample_rate = to_rate;
    if ( double( _step ) > 0 )
    {
        double frac = m_sample_rate * double( _step );
        double tol = 1. / ( m_sample_rate * m_sample_rate * frac );
        if ( tol > 1e-7 )
            tol = 1e-7;
        tol = rat( frac, m_upfactor, m_downfactor, tol );
    }
}

//======================================  set target sample rate
void
resampler::set_sample_factors( long upfactor, long downfactor )
{
    m_upfactor = upfactor;
    m_downfactor = downfactor;
    int den = gcd( upfactor, downfactor );
    if ( den > 1 )
    {
        m_upfactor /= den;
        m_downfactor /= den;
        cout << "resampler: common factor removed from up/down factors."
             << endl;
    }
}

//======================================  Design the antialiasing filter.
void
resampler::setup( void )
{
    if ( !m_upfactor || !m_downfactor )
    {
        if ( !m_sample_rate )
        {
            throw runtime_error( "resampler: target rate not set." );
        }
        else if ( !_step )
        {
            throw runtime_error( "resampler: input sample rate unknown." );
        }
        else
        {
            set_sample_rate( m_sample_rate );
        }
    }

    if ( m_upfactor > 1 && m_upmode == kup_spline )
    {
        m_interpolate.set( new cubic_interpolate( m_upfactor ) );
    }

    //--------------------------------  Design an anti-aliasing filter.
    make_filter( );
}

//======================================  Perform interpolation.
TSeries
resampler::upsample( const TSeries& ts )
{
    if ( m_upfactor <= 1 || ts.empty( ) )
        return ts;

    TSeries r;
    size_t  Nin = ts.getNSample( );
    size_t  Nout = Nin * m_upfactor;
    size_t  Ninx = Nout - Nin;
    Time    t0 = ts.getStartTime( );

    //-------------------------------------  Upsample a real TSeries
    if ( !ts.isComplex( ) )
    {
        DVectD dvd( Nout );
        switch ( m_upmode )
        {

        //----------------------------------  Cubic spline.
        case kup_spline:
            r = m_interpolate( ts );
            break;

        //----------------------------------  Sparse fill
        case kup_sparse:
            dvd.replace_with_zeros( 0, Ninx, Ninx );
            dvd.replace( Ninx, Nin, *ts.refDVect( ), 0, Nin );
            for ( size_t i = 0; i < Nin; i++ )
            {
                double t = dvd[ Ninx + i ] * m_upfactor;
                dvd[ Ninx + i ] = 0.0;
                dvd[ i * m_upfactor ] = t;
            }
            r.setData(
                t0, ts.getTStep( ) / double( m_upfactor ), dvd.clone( ) );
            break;

        //----------------------------------  Repeated fill.
        case kup_repeat:
            dvd.replace( Ninx, Nin, *ts.refDVect( ), 0, Nin );
            for ( size_t i = 0; i < Nin; i++ )
            {
                dvd.replace(
                    i * m_upfactor, m_upfactor, dvd[ Ninx + i ], m_upfactor );
            }
            r.setData(
                t0, ts.getTStep( ) / double( m_upfactor ), dvd.clone( ) );
            break;
        }
    }

    //-----------------------------------  upsample a complex TSeries
    else
    {
        DVectW dvw( Nout );
        switch ( m_upmode )
        {

        //--------------------------------  Cubic spline.
        case kup_spline:
            r = m_interpolate( ts );
            break;

        //--------------------------------  Sparse fill
        case kup_sparse:
            dvw.replace_with_zeros( 0, Ninx, Ninx );
            dvw.replace( Ninx, Nin, *ts.refDVect( ), 0, Nin );
            for ( size_t i = 0; i < Nin; i++ )
            {
                dComplex t = dvw[ Ninx + i ] * m_upfactor;
                dvw[ Ninx + i ] = dComplex( 0.0 );
                dvw[ i * m_upfactor ] = t;
            }
            r.setData(
                t0, ts.getTStep( ) / double( m_upfactor ), dvw.clone( ) );
            break;

        //--------------------------------  Repeated fill.
        case kup_repeat:
            dvw.replace( Ninx, Nin, *ts.refDVect( ), 0, Nin );
            for ( size_t i = 0; i < Nin; i++ )
            {
                dvw.replace(
                    i * m_upfactor, m_upfactor, dvw[ Ninx + i ], m_upfactor );
            }
            r.setData(
                t0, ts.getTStep( ) / double( m_upfactor ), dvw.clone( ) );
            break;
        }
    }
    return r;
}
