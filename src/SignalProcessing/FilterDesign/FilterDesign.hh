/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef _LIGO_FILTERDESIGN_H
#define _LIGO_FILTERDESIGN_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: FilterDesign						*/
/*                                                         		*/
/* Module Description: design filters					*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 17Jul02  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: FilterDesign.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include "Complex.hh"
#include "Pipe.hh"
#include "TSeries.hh"
#include "FSeries.hh"
#include "FilterParse.hh"
#include <string>

class Chirp;
class SweptSine;
namespace ligogui
{
    class TLGMultiPad;
}

/** The filter design class allows to design complex filters. It is
  * not a filter class by itself but rather a "factory" class that
  * produces filters depending on a user specification.
  * It supports:
  *  - \b IIR: elliptic, chebyshev, butterworth, zero/pole/gain, notches,
  *    resonant gain, pendulum and bi-quad coefficients.
  *  - \b FIR: window and McClellan-Parks algorithms.
  *  - \b Others: line tracker, decimation and resampling.
  *
  * Any number of filters can be added in series. Filters can be
  * designed by calling their corresponding creation methods (such
  * as ellip), by parsing a string formula or by reading the 
  * coefficients of a configuration file used by the front-end code.
  *
  * The filter design class also supports a configuration line 
  * parser which follows the matlab syntax. This way a filter can 
  * be written by a product of designer functions. For example,
  * "zpk([100;100],[1;1],1) /. setgain(0,1)" represents a filter
  * consisting of two poles at 100Hz, two zeros at 1Hz and a 
  * dc gain of 1. For a list of supported designer functions
  * see below.
  *
  * For filters that support it such as IIR, FIR and combinations
  * of them the filter design class can also calculate the 
  * transfer function and generate a Bode plot. All filters can
  * tested for step, impulse and ramp response.
  *
  * By default the filter design class will return a unity gain 
  * filter (identity operator), if no filter has been added.
  *
  * <b>Example 1:</b> Make an elliptic high pass filter
  * \verbatim
    FilterDesign fdesign (16384); // specify sampling frequency
    fdesign.ellip (kLowPass,8,1,60,100); // make elliptic low pass
           // 8th order, 1dB ripple, 60dB attenuation, 100Hz corner
    Pipe* pipe = fdesign.release(); // get filter
    \endverbatim
  *
  * <b>Example 2:</b> Make a whitening filter with a resonant gain, generate 
  *             a bode plot and investigate its step response
  * \verbatim
    FilterDesign fdesign ("zpk([1;1],[100;100],1)*resgain(12,30)", 16384); 
       // specify filter string and sampling frequency
    fdesign.bode (0.1, 1000, 1001); // make a bode plot
    fdesign.response ("step", 10.0) // plot step response
    \endverbatim
  *
  * <b>Example 3:</b> Read DARM filter from online system, add a 60Hz comb 
  *             and test it on random noise
  * \verbatim
    FilterDesign fdesign (16384); 
    fdesign.frontend ("H2LSC.txt", "DARM");
    fdesign.comb (60, 100); // add Q=100 comb
    TSeries test (Time (0), Interval (1./fdesign.getFSample()), 
                  10*16384, GaussNoise (fdesign.getFSample())); 
            // make some random noise
    fdesign.response (test) // plot filter response to random noise
    \endverbatim
  *
  * The filter types are: kLowPass for low pass, kHighPass for high
  * pass, kBandPass for band pass and kBandStop for a band stop
  * filter, respectively. When using a string specification, the 
  * corresponding filter types are "LowPass", "HighPass", "BandPass"
  * and "BandStop".
  *
  * \section FILTER FORMULA SPECIFICATION
  *
  * Filter formulas are written as a product of real numbers and
  * designer functions. Optionally one can add a gain condition
  * at the end. Designer functions are of the form "f(a1,a2,...aN)"
  * where "f" must be a supported function and "a1", .. "aN" must
  * be the corresponding arguments. A complex number argument is 
  * written as "real+i*imag" or "real+imag*i". Number arguments
  * must be constants and expressions are not allowed. A vector
  * of numbers is written as "[e1;e2;...eN]". The gain condition
  * is introduce by the "/." symbol and it must be followed by the 
  * "setgain(f,g)" where g is the desired gain and f is the 
  * frequency at which it should be true. The multiplication 
  * operator "*" is optional and can be omitted or replaced by a
  * space.
  *
  * List of designer functions:
  * <table>
  * <tr><td>gain(g,gfmt='scalar')</td>
  *     <td> multiply by g </td></tr>
  * <tr><td>pole(f,g=1,pl='s')</td>
  *     <td>a simple pole</td></tr>
  * <tr><td>pole(f,pl='s') </td>
  *     <td>a simple pole</td></tr>
  * <tr><td>zero(f,g=1,pl='s')</td>
  *     <td>a simple zero</td></tr>
  * <tr><td>zero(f,pl='s')</td>
  *     <td>a simple zero</td></tr>
  * <tr><td>pole2(f,Q,g=1,pl='s')</td>
  *     <td>a complex pole pair</td></tr>
  * <tr><td>pole2(f,Q,pl='s')</td>
  *     <td>a complex pole pair</td></tr>
  * <tr><td>zero2(f,Q,g=1,pl='s')</td>
  *     <td>a complex zero pair</td></tr>
  * <tr><td>zero2(f,Q,pl='s')</td>
  *     <td>a complex zero pair</td></tr>
  * <tr><td>zpk([z1;...zN],[p1;...pM],g=1,pl)</td>
  *     <td>a set of s-plane poles and zeros</td></tr>
  * <tr><td>rpoly([z1;...zN],[p1;...pM],g=1)</td>
  *     <td>a rational polynomial</td></tr>
  * <tr><td>biquad(b0,b1,b2,a1,a2)</td>
  *     <td>second order section</td></tr>
  * <tr><td>sos(g,[b1,b2,a1,a2,...],fmt='s')</td>
  *     <td>list of second order sections</td></tr>
  * <tr><td>zroots([z1;...zN],[p1;...pM],g=1)</td>
  *     <td>a set of z-plane poles and zeros</td></tr>
  * <tr><td>direct([b0;...bN],[a1;...aM])</td>
  *     <td>direct form</td></tr>
  * <tr><td>ellip(type,order,rp,as,f1,f2=0)</td>
  *     <td>elliptic filter</td></tr>
  * <tr><td>butter(type,order,f1,f2=0)</td>
  *     <td>butterworth filter</td></tr>
  * <tr><td>cheby1(type,order,rp,f1,f2=0)</td>
  *     <td>chebyshev type 1 filter</td></tr>
  * <tr><td>cheby2(type,order,as,f1,f2=0)</td>
  *     <td>chebyshev type 2 filter</td></tr>
  * <tr><td>notch(f,Q,depth=0) </td>
  *     <td>notch filter</td></tr>
  * <tr><td>resgain(f,q,height=0)</td>
  *     <td>resonant gain filter</td></tr>
  * <tr><td>comb(f,Q,depth=0,hN=0)</td>
  *     <td>comb filter </td></tr>
  * <tr><td>remez(order,[...],[...],[...])</td>
  *     <td>FIR filter according to McClellan-Parks</td></tr>
  * <tr><td>firls(order,[f1...fN],[bp1...pbN],[w1...wN/2])</td>
  *     <td>FIR filter using least-squares method</td></tr>
  * <tr><td>firw(order,type,window,f1,f2=0,as=0,df=0) </td>
  *     <td>FIR filter with window method
  * <tr><td>fircoef([c1...cN],firm="causal") </td>
  *     <td>FIR filter from a coefficient list
  * <tr><td>%difference() </td>
  *     <td>difference filter</td></tr>
  * <tr><td>limiter(type,l1,l2=0,l3=0)</td>
  *     <td>limiter</td></tr>
  * <tr><td>decimateBy2(stages,id=1)</td>
  *     <td>decimate by factors of 2 (FIR)</td></tr>
  * <tr><td>multirate(type,m1,m2,attn)</td>
  *     <td>resample the time series</td></tr>
  * <tr><td>mixer(fmix,phase)</td>
  *     <td>mixer</td></tr>
  * <tr><td>linefilter(...)</td>
  *     <td>line removal</td></tr>
  * <tr><td>frontend(file,module,sect=-1)</td>
  *     <td>read front-end configuration</td></tr>
  * </table>
  *
  * where: 
  * <table>
  * <tr><td>f        </td><td> corner frequency</td></tr>
  * <tr><td>g        </td><td> gain</td></tr>
  * <tr><td>gfmt     </td><td> gain format</td></tr>
  * <tr><td>Q        </td><td> quality factor</td></tr>
  * <tr><td>z        </td><td> zero location</td></tr>
  * <tr><td>p        </td><td> pole location</td></tr>
  * <tr><td>pl       </td><td> pole/zero location, either "s" for radians/s,
  *                            "f" for frequencies in Hz or "n" for normalized
  *                            poles/zeroes. </td></tr>
  * <tr><td>a1,a2    </td><td> denominator coefficients</td></tr>
  * <tr><td>b0,b1,b2 </td><td> numerator coefficients</td></tr>
  * <tr><td>fmt      </td><td> format of second order sections</td></tr>
  * <tr><td>type     </td><td> filter type ("LowPass", "HighPass", "BandPass"
  * or "BandStop")</td></tr>
  * <tr><td>order    </td><td> filter order</td></tr>
  * <tr><td>rp       </td><td> pass-band ripple (in dB)</td></tr>
  * <tr><td>as       </td><td> stop-band attenuation</td></tr>
  * <tr><td>f1       </td><td> lower corner frequency</td></tr>
  * <tr><td>f2       </td><td> upper corner frequency</td></tr>
  * <tr><td>depth    </td><td> depth in dB</td></tr>
  * <tr><td>height   </td><td> depth in dB</td></tr>
  * <tr><td>hN       </td><td> Number of harmonics</td></tr>
  * <tr><td>l1,l2,l3 </td><td> limits (bounds and/or slew rate)</td></tr>
  * <tr><td>stages   </td><td> number of stages</td></tr>
  * <tr><td>id       </td><td> filter id</td></tr>
  * <tr><td>m1,m2    </td><td> resampling parameters (abs or rel)</td></tr>
  * <tr><td>attn     </td><td> attenuation in dB</td></tr>
  * <tr><td>fmix     </td><td> heterodyne frequency</td></tr>
  * <tr><td>phase    </td><td> phase</td></tr>
  * <tr><td>file     </td><td> file name</td></tr>
  * <tr><td>firm     </td><td> FIR mode ("causal" or "zero_phase")</td></tr>
  * <tr><td>module   </td><td> filter module name</td></tr>
  * <tr><td>sect     </td><td> filter section</td></tr>
  * <tr><td>bp1,...,bpN </td><td> band pass at corresponding frequency</td></tr>
  * <tr><td>w1,...,wN </td><td> band weights</td></tr>
  * </table>
  * For a more detailed description of the designer functions see
  * the documentation of the corresponding method below.
  *
  * @memo Filter design
  * @author Written July 2002 by Ed Daw, Masahiro Ito, Daniel Sigg
  * @version 1.0
  ************************************************************************/
class FilterDesign : public FilterParse
{
public:
    /** Constructs a filter design class. The filter design class
          keeps track of the sampling frequency and the current 
          filter.
          @memo Constructor.
          @param fsample Sampling rate
          @param name Name of filter
       ******************************************************************/
    explicit FilterDesign( double fsample = 1.0, const char* name = "filter" );
    /** Constructs a filter design class. The current filter is 
          initialized with the specified string---see also 
          AddFilter(const char*).
          @memo Constructor.
          @param spec Filter design specification
          @param fsample Sampling rate
          @param name Name of filter
       ******************************************************************/
    explicit FilterDesign( const char* spec,
                           double      fsample = 1.0,
                           const char* name = "filter" );

    /** Copy constructor.
          @memo Copy constructor.
	  @param design Constant reference to FilterDesign to be copied.
       ******************************************************************/
    FilterDesign( const FilterDesign& design );

    /** Destructs the filter design class.
          @memo Destructor.
       ******************************************************************/
    virtual ~FilterDesign( );

    /** Assignment operator.
          @memo Assignment operator.
	  @param design FilterDesign to be copied to this.
	  @return Reference to the modified FilterDesign
       ******************************************************************/
    FilterDesign& operator=( const FilterDesign& design );

    /** Initializes the filter design class with a new sampling rate.
          The output rate is set equal to the input rate and the
          current filter is reset to NULL. The prewarp flag is left
          unchanged.
          @memo Initialization.
          @param fsample Sampling rate
       ******************************************************************/
    void init( double fsample = 1.0 );

    /** Check if we have a trivial unity gain filter.
          @memo Check for unity gain filter.
          @return true if unity gain
       ******************************************************************/
    bool isUnityGain( ) const;

    /** Set the filter name.
          @memo Set name.
          @param name Name of filter
          @return void
       ******************************************************************/
    void
    setName( const char* name )
    {
        fName = name ? name : "filter";
    }
    /** Get the filter name.
          @memo Get name.
          @return filter name
       ******************************************************************/
    const char*
    getName( ) const
    {
        return fName.c_str( );
    }
    /** Set the sampling frequency. The current filter gets reset.
          @memo Set sampling frequency.
          @param fsample Sampling rate
          @return void
       ******************************************************************/
    void
    setFSample( double fsample )
    {
        init( fsample );
    }
    /** Get the (input) sampling frequency.
          @memo Get sampling frequency.
          @return Sampling rate
       ******************************************************************/
    double
    getFSample( ) const
    {
        return fFSample;
    }
    /** Get the output sampling frequency.
          @memo Get output sampling frequency.
          @return Sampling rate
       ******************************************************************/
    double
    getFOut( ) const
    {
        return fFOut;
    }
    /** Get the heterodyne flag. This flag is set to false initially
          and will be set to true if one of the filters is a mixer.
          This is then an indication that the output time series will
          be complex for a real input time series.
          @memo Get heterodyne flag.
          @return Sampling rate
       ******************************************************************/
    bool
    getHeterodyne( ) const
    {
        return fHeterodyne;
    }

    /** Set the prewarping flag
          @memo Set prewarping flag
          @param set prewarping flag
       ******************************************************************/
    void
    setPrewarp( bool set = true )
    {
        fPrewarp = set;
    }

    /** Is prewarping enabled?
          @memo Get prewarping flag
          @return true if prewarping is enabled
       ******************************************************************/
    bool
    getPrewarp( ) const
    {
        return fPrewarp;
    }
    /** Get the filter specification.
          @memo Get filter specification.
          @return Filter specification
       ******************************************************************/
    const char*
    getFilterSpec( ) const
    {
        return fFilterSpec.c_str( );
    }
    /** Get pad returned by the last call to a plotting function.
          @memo Get plot pad.
          @return plot pad
       ******************************************************************/
    ligogui::TLGMultiPad*
    getPad( ) const
    {
        return fPad;
    }

    /** Get the filter. Returns the identity filter, if no filter 
          has been added.
          @memo Get the filter
          @return current filter
       ******************************************************************/
    Pipe&
    get( )
    {
        return *fFilter;
    }
    /** Get the filter. Returns the identity filter, if no filter 
          has been added.
          @memo Get the filter (const)
          @return current filter
       ******************************************************************/
    const Pipe&
    get( ) const
    {
        return *fFilter;
    }
    /** Get the filter. Returns the identity filter, if no filter 
          has been added.
          @memo Get the filter
          @return current filter
       ******************************************************************/
    Pipe&
    operator( )( )
    {
        return get( );
    }
    /** Get the filter. Returns the identity filter, if no filter 
          has been added.
          @memo Get the filter (const)
          @return current filter
       ******************************************************************/
    const Pipe&
    operator( )( ) const
    {
        return get( );
    }
    /** Get a copy of the filter. User must delete it! Returns the 
          identity filter, if no filter has been added.
          @memo Get a copy of the filter
          @return copy of current filter
       ******************************************************************/
    Pipe* copy( ) const;
    /** Release the current filter and return it. Returns the identity 
          filter, if no filter has been added. After this call the 
          current filter is set back to an identity filter.
          @memo Get the current filter
          @return current filter
       ******************************************************************/
    Pipe* release( );
    /** Resets the current filter to identity.
          @memo Reset the current filter
          @return void
       ******************************************************************/
    void reset( );
    /** Set the filter.
          @memo Set the filter
          @param filter New current filter
          @param resampling Resampling factor
          @param heterodyne Filter is heterodyning
       ******************************************************************/
    void
    set( const Pipe& filter, double resampling = 1.0, bool heterodyne = false );

    /** Add a filter to the current one.
          @memo Add a filter
          @param filter Filter to be added
          @param resampling Resampling factor.
          @param heterodyne Filter is heterodyning
          @return pointer to new current filter (owned by design class)
       ******************************************************************/
    bool
    add( const Pipe& filter, double resampling = 1.0, bool heterodyne = false );

    /** Make a filter from a descriptive string and add it to the
          current one.
          @memo Add a filter
          @param formula New filter specification
          @return true if successful
       ******************************************************************/
    bool filter( const char* formula );

    /** Multiply the filter by the specified gain. The gain can be 
          specified as a scalar or in dB. The default is scalar.
          @memo Add a gain
          @param g gain.
          @param format scalar or dB
          @return true if successful
       ******************************************************************/
    bool gain( double g, const char* format = "scalar" );

    using FilterParse::pole;
    /** Poles and zero can be specified in the s-plane using units of 
          rad/s (set plane to "s") or units of Hz ("f"). In both cases 
          the frequency is expected to be POSITIVE. Alternatively, one can 
          also specify normalized poles and zeros ("n"). Normalized poles 
          and zeros are expected to have POSITIVE frequencies and their 
          respective low and high frequency gains are set to 1 unless 
          they are located a 0Hz. The default location is the s-plane.
          For the s-plane the formula is \f$pole(f0) = 1/(s + w0)\f$ ["s"] 
          or \f$pole(f0) = 1/(s + 2 pi f0)\f$ ["f"]. For a normalized pole 
          ["n"] the formula is \f$pole(f0) = 1/(1 + i f/f0)\f$; \f$f0 > 0\f$
          and \f$pole(f0) = 1/(i f)\f$; \f$f0 = 0\f$. 
          @memo Add pole
          @see IIRFilter.hh
          @param f Pole frequency.
          @param gain Gain.
          @param plane location where poles/zeros are specified
          @return true if successful
       ******************************************************************/
    bool pole( double f, double gain, const char* plane = "s" );

    using FilterParse::zero;
    /** Poles and zero can be specified in the s-plane using units of 
          rad/s (set plane to "s") or units of Hz ("f"). In both cases 
          the frequency is expected to be POSITIVE. Alternatively, one can
          also specify normalized poles and zeros ("n"). Normalized poles 
          and zeros are expected to have POSITIVE frequencies and their 
          respective low and high frequency gains are set to 1---unless 
          they are located a 0Hz. The default location is the s-plane.
          For the s-plane the formula is \f$zero(f0) = (s + w0)\f$ ["s"] 
          or \f$zero(f0) = (s + 2 pi f0)\f$ ["f"]. For a normalized zero 
          ["n"] the formula is \f$zero(f0) = (1 + i f/f0)\f$; \f$f0 > 0\f$ and
          \f$zero(f0) = (i f)\f$; \f$f0 = 0\f$.
          @memo Add zero
          @see IIRFilter.hh
          @param f Zero frequency.
          @param gain Gain.
          @param plane location where poles/zeros are specified
          @return true if successful
       ******************************************************************/
    bool zero( double f, double gain, const char* plane = "s" );

    using FilterParse::pole2;
    /** Poles and zero can be specified in the s-plane using units of 
          rad/s (set plane to "s") or units of Hz ("f"). In both cases 
          the frequency is expected to be POSITIVE. Alternatively, one can
          also specify normalized poles and zeros ("n"). Normalized poles 
          and zeros are expected to have POSITIVE frequencies and their 
          respective low and high frequency gains are set to 1---unless 
          they are located a 0Hz. The default location is the s-plane.
          For the s-plane the formula is 
          \f$pole2(f0,Q)=1/(s^2 + w0 s / Q + w0^2)\f$ ["s"] or 
          \f$pole2(f0,Q)=1/(s^2 + 2 pi f0 s / Q + (2 pi f0)^2)\f$ ["f"]. 
          For a normalized complex pole pair ["n"] the formula is 
          \f$pole2(f0) = 1/(1 + i f/f0 + (f/f0)^2)\f$ with \f$f0 > 0\f$.
          The quality factor Q must be greater than 0.5.
          @memo Add a complex pole pair
          @see IIRFilter.hh
          @param f Pole frequency.
          @param Q Quality factor.
          @param gain Gain.
          @param plane location where poles/zeros are specified
          @return true if successful
       ******************************************************************/
    bool pole2( double f, double Q, double gain, const char* plane = "s" );

    using FilterParse::zero2;
    /** Poles and zero can be specified in the s-plane using units of 
          rad/s (set plane to "s") or units of Hz ("f"). In both cases 
          the frequency is expected to be POSITIVE. Alternatively, one can
          also specify normalized poles and zeros ("n"). Normalized poles 
          and zeros are expected to have POSITIVE frequencies and their 
          respective low and high frequency gains are set to 1 unless 
          they are located a 0Hz. The default location is the s-plane.
          For the s-plane the formula is 
          \f$zero2(f0,Q)=(s^2 + w0 s / Q + w0^2)\f$ ["s"] or 
          \f$zero2(f0,Q)=(s^2 + 2 \pi f0 s / Q + (2 \pi f0)^2)\f$ ["f"]. 
          For a normalized complex pole pair ["n"] the  formula is 
          \f$pole2(f0) = (1 + i f/f0 + (f/f0)^2)\f$ with \f$f0 > 0\f$.
          The quality factor Q must be greater than 0.5.
          @memo Add a complex zero pair
          @see IIRFilter.hh
          @param f Pole frequency.
          @param Q Quality factor.
          @param gain Gain.
          @param plane location where poles/zeros are specified
          @return true if successful
       ******************************************************************/
    bool zero2( double f, double Q, double gain, const char* plane = "s" );

    /** Make an filter from zeros and poles. 
          Poles and zero can be specified in the s-plane using units of 
          rad/s (set plane to "s") or units of Hz ("f"). In both cases 
          the real part of the roots are expected to be NEGATIVE. 
          Alternatively, one can also specify normalized poles and zeros
          ("n"). Normalized poles and zeros are expected to have POSITIVE
          real parts and their respective low and high frequency gains
          are set to 1 unless they are located a 0Hz. The default 
          location is the s-plane. 
   
          For the s-plane the formula is:
          \f[ zpk(s) = k \frac{(s - s_1)(s - s_2) \cdots}
	                      {(s - p_1)(s - p_2) \cdots}
	  \f]
          with \f$s_1, s_2,...\f$ the locations of the zeros and 
          \f$p_1, p_2,...\f$ the location of the poles. By replacing 
          \f$s_1->2 \pi fz_1\f$ and \f$p_1 -> 2 \pi fp_1\f$ one obtains the 
          "f" representation. For normalize poles and zeros ["n"] one uses 
          poles of the form \f$pole(f0) = 1/(1 + i f/f0)\f$;\f$f0 > 0\f$ and 
          \f$pole(f0) = 1/(i f)\f$; \f$f0 = 0\f$. The zeros are then of the 
	  form \f$zero(f0) = (1 + i f/f0)\f$; \f$f0 > 0\f$ and 
	  \f$zero(f0) = (i f)\f$; \f$f0 = 0\f$. Poles and zeros with a 
	  non-zero imaginary part must come in pairs of complex conjugates.
   
          @memo Add zero-pole-gain (zpk)
          @see IIRFilter.hh
          @param nzeros Number of zeros
          @param zero Array of zeros
          @param npoles Number of poles
          @param pole Array of poles
          @param gain Gain
          @param plane location where poles/zeros are specified
          @return true if successful
       ******************************************************************/
    bool zpk( int             nzeros,
              const dComplex* zero,
              int             npoles,
              const dComplex* pole,
              double          gain,
              const char*     plane = "s" );

    /** Make an filter from zeros and poles. 
          Poles and zero can be specified in the s-plane using units of 
          rad/s (set plane to "s") or units of Hz ("f"). In both cases 
          the real part of the roots are expected to be NEGATIVE. 
          Alternatively, one can also specify normalized poles and zeros
          ("n"). Normalized poles and zeros are expected to have POSITIVE
          real parts and their respective low and high frequency gains
          are set to 1 unless they are located a 0Hz. The default 
          location is the s-plane. 
   
          For the s-plane the formula is:
          \f[ zpk(s) = k \frac{(s - s_1)(s - s_2) \cdots}
	                      {(s - p_1)(s - p_2) \cdots}
	  \f]
          with \f$s_1, s_2,...\f$ the locations of the zeros and 
          \f$p_1, p_2,...\f$ the location of the poles. By replacing 
          \f$s_1->2 \pi fz_1\f$ and \f$p_1 -> 2 \pi fp_1\f$ one obtains the 
          "f" representation. For normalize poles and zeros ["n"] one uses 
          poles of the form \f$pole(f0) = 1/(1 + i f/f0)\f$;\f$f0 > 0\f$ and 
          \f$pole(f0) = 1/(i f)\f$; \f$f0 = 0\f$. The zeros are then of the 
	  form \f$zero(f0) = (1 + i f/f0)\f$; \f$f0 > 0\f$ and 
	  \f$zero(f0) = (i f)\f$; \f$f0 = 0\f$. Poles and zeros with a 
	  non-zero imaginary part must come in pairs of complex conjugates.
   
          @memo Add zero-pole-gain (zpk)
          @see IIRFilter.hh
          @param nzeros Number of zeros
          @param zero Array of zeros
          @param npoles Number of poles
          @param pole Array of poles
          @param gain Gain
          @param plane location where poles/zeros are specified
          @return true if successful
       ******************************************************************/
    bool zpk( int             nzeros,
              const fComplex* zero,
              int             npoles,
              const fComplex* pole,
              double          gain,
              const char*     plane = "s" );

    /** Make a filter from a rational polynomial in s. 
          A rational polynomial in s is specified by the polynomial 
          coefficients in the numerator and the denominator in descending 
          order of s. 
   
          The formula is
          \f[ zpk(s) = k \frac{a_n s^{n_z} + a_{n-1} s^{n_z - 1} \cdots}
                            {b_n s^{n_p} + b_{n-1} s^{n_p - 1} \cdots}
	  \f]
          where \f$a_n,  a_{n-1},..., a_0\f$ are the coefficients 
	  of the polynomial in the numerator and \f$ b_n, b_{n-1},..., b_0\f$ 
	  are the coefficients of the polynomial in the denominator.
          The polynomial coefficients have to be real.
   
          @memo Add a rational polynomial (rpoly)
          @see IIRFilter.hh
          @param nnumer Number of coefficients in numerator
          @param numer Array of numerator coefficients
          @param ndenom Number of coefficients in denominator
          @param denom Array of denominator coefficients
          @param gain Gain
          @return true if successful
       ******************************************************************/
    bool rpoly( int           nnumer,
                const double* numer,
                int           ndenom,
                const double* denom,
                double        gain );

    /** Make a filter from a rational polynomial in s. 
          A rational polynomial in s is specified by the polynomial 
          coefficients in the numerator and the denominator in descending 
          order of s. 
   
          The formula is
          \f[ zpk(s) = k \frac{a_n s^{n_z} + a_{n-1} s^{n_z - 1} \cdots}
                            {b_n s^{n_p} + b_{n-1} s^{n_p - 1} \cdots}
	  \f]
          where \f$a_n,  a_{n-1},..., a_0\f$ are the coefficients 
	  of the polynomial in the numerator and \f$ b_n, b_{n-1},..., b_0\f$ 
	  are the coefficients of the polynomial in the denominator.
          The polynomial coefficients must be real.
   
          @memo Add a rational polynomial (rpoly)
          @see IIRFilter.hh
          @param nnumer Number of coefficients in numerator
          @param numer Array of numerator coefficients
          @param ndenom Number of coefficients in denominator
          @param denom Array of denominator coefficients
          @param gain Gain
          @return true if successful
       ******************************************************************/
    bool rpoly( int          nnumer,
                const float* numer,
                int          ndenom,
                const float* denom,
                double       gain );

    /** Make an filter from the bi-quad coefficients and add it to the 
          current one.
          @memo Add a second order section
          @see IIRdesign.hh
          @param b0 b0
          @param b1 b1
          @param b2 b2
          @param a1 a1
          @param a2 a2
          @return true if successful
       ******************************************************************/
    bool biquad( double b0, double b1, double b2, double a1, double a2 );

    /** Make a IIR filter from a list of second order sections. If the
          format is 's' (standard), the coefficients are ordered like:
          \f$ gain, b1_1, b2_1, a1_1, a2_1, b1_2, b2_2, a1_2, a2_2,... \f$
          whereas for the the format 'o' (online) the order is
          \f$ gain, a1_1, a2_1, b1_1, b2_1, a1_2, a2_2, b1_2, b2_2,... \f$
          The number of coefficients must be 4 times the number of second 
          order sections plus one.
          @memo Add a list of second order sections
          @see IIRdesign.hh
          @param nba Number of coefficients
          @param ba List of coefficients
          @param format Coefficient format
          @return true if successful
       ******************************************************************/
    bool sos( int nba, const double* ba, const char* format = "s" );

    /** Make a IIR filter from the list of poles and zeros in the 
          z-plane. To be stable the z-plane poles must lie within the 
          unity circle.
          @memo Add a filter from z-plane roots
          @see IIRdesign.hh
          @param nzeros Number of zeros
          @param zero Array of zeros
          @param npoles Number of poles
          @param pole Array of poles
          @param gain Gain
          @return true if successful
       ******************************************************************/
    bool zroots( int             nzeros,
                 const dComplex* zero,
                 int             npoles,
                 const dComplex* pole,
                 double          gain = 1.0 );

    /** Make a IIR filter from the list of poles and zeros in the 
          z-plane. To be stable the z-plane poles must lie within the 
          unity circle.
          @memo Add a filter from z-plane roots
          @see IIRdesign.hh
          @param nzeros Number of zeros
          @param zero Array of zeros
          @param npoles Number of poles
          @param pole Array of poles
          @param gain Gain
          @return true if successful
      ******************************************************************/
    bool zroots( int             nzeros,
                 const fComplex* zero,
                 int             npoles,
                 const fComplex* pole,
                 double          gain = 1.0 );

    /** Make a filter from the direct form. 
          The direct form can be written as:
          \f[
	  H(z) = \frac{\sum_{k=0}^{nb} b_k z^{-k}}
	              {1-\sum_{k=1}^{na} a_k z^{-k}}
	  \f]
    
          Cascaded second order sections are formed by finding the roots
          of the direct form. The specified coefficients are 
          \f$b_0, b_1, \dots, b_{nb}\f$ for the numerator and 
	  \f$a_1, a_2, \dots, a_{na}\f$ for the denominator. The argument 
	  \a nb specifies the number of \a b coefficients supplied to the 
	  function minus 1, whereas \a na is exactly the number of \a a 
	  coefficients since \f$a_0\f$ is always 1 and omitted from the list.
    
          Avoid the direct form since even fairly simple filters will 
          run into precision problems.
   
          @memo Add a filter from the direct form
          @param nb Number of coefficients in numerator minus 1
          @param b Array of numerator coefficients
          @param na Number of coefficients in denominator
          @param a Array of denominator coefficients exclusive of a0
          @return true if successful
       ******************************************************************/
    bool direct( int nb, const double* b, int na, const double* a );

    /** Make an elliptic filter and add it to the current one.
          @memo Add an elliptic filter
          @see IIRdesign.hh
          @param type Filter type
          @param order Filter order
          @param rp Pass band ripple (dB)
          @param as Stop band attenuation (dB)
          @param f1 Pass band edge (Hz)
          @param f2 Another pass band edge (Hz)
          @return true if successful
       ******************************************************************/
    bool ellip( Filter_Type type,
                int         order,
                double      rp,
                double      as,
                double      f1,
                double      f2 = 0.0 );
    /** Make an chebyshev filter of type 1 and add it to the current 
          one.
          @memo Add a chebyshev filter
          @see IIRdesign.hh
          @param type Filter type
          @param order Filter order
          @param rp Pass band ripple (dB)
          @param f1 Pass band edge (Hz)
          @param f2 Another pass band edge (Hz)
          @return true if successful
       ******************************************************************/
    bool cheby1(
        Filter_Type type, int order, double rp, double f1, double f2 = 0.0 );
    /** Make an chebyshev filter of type 2 and add it to the current 
          one.
          @memo Add a chebyshev filter
          @see IIRdesign.hh
          @param type Filter type
          @param order Filter order
          @param as Stop band attenuation (dB)
          @param f1 Pass band edge (Hz)
          @param f2 Another pass band edge (Hz)
          @return true if successful
       ******************************************************************/
    bool cheby2(
        Filter_Type type, int order, double as, double f1, double f2 = 0.0 );
    /** Make an butterworth filter and add it to the current one.
          @memo Add a butterworth filter
          @see IIRdesign.hh
          @param type Filter type
          @param order Filter order
          @param f1 Pass band edge (Hz)
          @param f2 Another pass band edge (Hz)
          @return true if successful
       ******************************************************************/
    bool butter( Filter_Type type, int order, double f1, double f2 = 0.0 );
    /** Make an notch filter and add it to the current one.
          @memo Add a notch filter
          @see IIRdesign.hh
          @param f0 Center frequency.
          @param Q Quality factor ( Q = (Center freq)/(Width) ).
          @param depth Depth of the notch (dB).
          @return true if successful
       ******************************************************************/
    bool notch( double f0, double Q, double depth = 0.0 );
    /** Make a resonant gain filter and add it to the current one.
          @memo Add a resonant gain filter
          @see IIRdesign.hh
          @param f0 Center frequency.
          @param Q Quality factor ( Q = (Center freq)/(Width) ).
          @param height Height of the peak (dB).
          @return true if successful
       ******************************************************************/
    bool resgain( double f0, double Q, double height = 30.0 );

    /** Make an comb filter and add it to the current one.
          @memo Add a comb filter
          @see IIRdesign.hh
          @param f0 Fundamental frequency.
          @param Q Quality factor ( Q = \<Center-freq\>/Width ).
          @param amp Depth/height of notches/peaks (dB).
          @param N Number of harmonics.
          @return true if successful
       ******************************************************************/
    bool comb( double f0, double Q, double amp = 0.0, int N = 0 );

    /** Specify how FIR filters will be evaluated. The filter mode applies
	  to both to any filters already defined and to filters to be set later.
	  The integer argument is a  bit mask with the following bit values:
	   - bit 0: subtract out the delay
	   - bit 1: suppress output before start time.
	   - bit 2: Use fast DFT calculation.

	  \brief Set the FIR filter modifiers.
	  \param fir_type modifier mask.
      *******************************************************************/
    void setFirType( int fir_type );

    /** Design an FIR filter using the McClellan-Parks algorithm
          and add it to the current one.
          Design a optimal equiripple linear phase FIR filter from a 
          list of bands and the desired amplitudes in each band. 
          nBand defines the number of bands in which the response will
          be specified. 'Bands' contains a minimum and a maximum 
          frequency for each band. \a Func contains the desired amplitude 
          for each band and \a Weight contains an optional weighting 
          factor for each band. The resulting filter coefficients are 
          stored in the Filter object. The filter history is left 
          unmodified. 
          @memo  FIR filter design using the McClellan-Parks algorithm.
          @see FIRdesign.hh
          @param N      Number of filter coefficients.
          @param nBand  Number of bands.
          @param Bands  Band limit frequencies in Hz.
          @param Func   Desired response in each band.
          @param Weight Weighting factor for each band. If not specified, 
                        Weight is assumed to be 1.0 for each band.
          @return true if successful
       ******************************************************************/
    bool remez( int           N,
                int           nBand,
                const double* Bands,
                const double* Func,
                const double* Weight = 0 );

    /** Design a linear phase FIR filter using the least squares method. 
	  The inputs are substantially the same as the remez function.
          The designed Filter coefficients replace the current 
          coefficients. The Filter history is not affected.
          @memo  Filter design by least squares fit..
          @see FIRdesign.hh
          @param N      Number of filter coefficients.
          @param nBand  Number of bands.
          @param bands  Band start and end frequencies (2*nBand entries)
          @param pass   Desired filter response at each frequency in \a bands.
          @param weight Weight of each band.
          @return true if successful
       ******************************************************************/
    bool firls( int           N,
                int           nBand,
                const double* bands,
                const double* pass,
                const double* weight = 0 );

    /** Design a linear phase FIR filter using the window function 
          technique. The windowing functions that may be used are 
          "Rectangle", "Triangle", "Hamming", "Hanning", "Kaiser" or 
          "Cheby". Filter types that may be designed are "LowPass", 
          "HighPass", "BandPass" or "BandStop". \a N or \a dF will be 
          inferred from the other parameters if omitted or set 
          to zero in Kaiser and Chebyshev designs. The same is true of 
          Ripple if it is omitted or set to zero in Chebyshev designs.
	  The Kaiser window Beta value is set from the Stop-band ripple
	  value (specified as attenuation, \i.e. \f$ -20 log(f_o / f_i) \f$).
          The designed Filter coefficients replace the current 
          coefficients. The Filter history is not affected.
          @memo  FIR Filter design from window function.
          @see FIRdesign.hh
          @param N      Number of filter coefficients.
          @param window Type of window on which to base the design.
          @param type   Type of filter.
          @param Flow   Transition frequency of a low-pass or 
                        high-pass filter, or lower edge of a band-pass 
                        or band-stop filter.
          @param Fhigh  Upper edge of a band-pass or band-stop filter.
          @param Ripple Desired attenuation for Kaiser filters or the 
                        desired pass-band ripple for Chebyshev filters 
                        (in dB).
          @param dF     Transition band width in Kaiser and Chebyshev filters.
          @return true if successful
       ******************************************************************/
    bool firw( int         N,
               Filter_Type type,
               const char* window,
               double      Flow,
               double      Fhigh = 0,
               double      Ripple = 0,
               double      dF = 0 );

    /**  Design an FIR filter using listed coefficients.
        *  @memo Add an FIR filter.
	*  @param N      Number of coefficients in the filter.
	*  @param coef   Pointer to coefficient list.
	*  @param mode   Timing mode false = "causal", true = "zero_phase"
	*  @return True if filter design is successful.
	*/
    bool fircoefs( int N, const double* coef, bool mode );

    /** Make a difference filter and add it to the current one.
          Differential filter takes the difference between each 
          successive pair of elements.
          @see Difference.hh
          @memo Add a difference filter
          @return true if successful
       ******************************************************************/
    bool difference( );

    /** Make a decimate-by-2 filter and add it to the current one.
          The DecimateBy2 class decimates a TSeries by \f$2^N\f$. The 
          TSeries data are filtered before decimation to remove 
          aliasing. This class is essentially a wrapper of the C 
          code written for LIGO GDS by Peter Fritschel.
   
          DecimateBy2 is based on the DMT FilterBase class and as 
          such is can be treated as a generic DMT Filter. The 
          decimation may be applied to the TSeries with either the 
          apply method or the parentheses operator. These methods 
          return a complex TSeries if the input series is complex,
          and a single precision floating point TSeries for all other 
          input series data types.
   
          Four anti-aliasing low-pass FIR filters are available, 
          each of which cuts off at \f$0.9 F_{Nyquist}\f$. The options are 
          tabulated below:
          <table>
          <tr><td>ID </td><td> Order </td><td> Comments </td></tr>
          <tr><td>1 </td><td> 42 </td>
	      <td> Least-Squares design, ripple ~.015 (-0.2 dB) </td></tr>
          <tr><td>2 </td><td> 42 </td>
	      <td> Equiripple design, ripple ~0.06 dB </td></tr>
          <tr><td>3 </td><td> 22 </td>
	      <td> Least-squares design, ripple ~0.1 (-1 dB) </td></tr>
          <tr><td>4 </td><td> 82 </td>
	      <td> Least-squares design, ripple ~0.0006 (-0.01 dB) </td></tr>
          </table>
          The anti-aliasing filters add an effective delay of \f$N*O_F/2\f$,
          where N is the number of stages (the decimation factor 
          exponent) and \f$O_F\f$ is the filter order. This delay is NOT 
          added to the TSeries start time.
          @memo Add a decimation by \f$2^N\f$.
          @see DecimateBy2
          @param N number of stages
          @param FilterID filter type
          @return true if successful
       ******************************************************************/
    bool decimateBy2( int N, int FilterID = 1 );

    /** Make a line removal filter and add it to the current one.
          The LineFilter class contains methods to track and remove 
          quasi-monochromatic lines. A filter id of 0 corresponds
          to a pure "comb" filter, a filter if of 1 corresponds to 
          an optimal Wiener filter with 1-st method of noise estimation 
          (default), and a filter id of -1 
          @memo Add a line removal filter
          @see LineFilter.hh
          @param f Approximate line frequency
          @param T time interval to remove lines.
          @param fid Select filter ID (0/1), 1 by default
          @param nT number of subdivisions of time interval w
          @return true if successful
       ******************************************************************/
    bool linefilter( double f, double T = 0.0, int fid = 1, int nT = 1 );

    /** Make a limiter filter and add it to the current one.
          The type can be "" for no limits, "val" for high/low limits on
          the sampled values, "sym" for a symmetric limits on the values, 
          "slew" for a slew rate limit and "val/slew" or "sym/slew" for 
          both. If the limiter type is "val", l1 is the lower bound and 
          l2 is the upper bound, for "sym" l1 is the upper bound and -l1
          is the lower bound, for "slew" l1 is the slew rate limit in 
          counts/s, for "val/slew" l1, l2 and l3 are the lower bound, 
          the upper bound and the slew rate limit, respectively, and for 
          "sym/slew" l1 and l2 are the symmetric limit and the slew rate 
          limit, respectively.
          @memo Add a limiter
          @see Limiter.hh
          @param type Limiter type
          @param l1 First limit
          @param l2 Second limit
          @param l3 Third limit
          @return true if successful
       ******************************************************************/
    bool limiter( const char* type, double l1, double l2 = 0, double l3 = 0 );

    /** Make a resampling filter and add it to the current one.
          The filter type is either "abs" or "rel" depending if the 
          required resampling rate is specified relative or absolute.
          For absolute rate specifications the first parameter is the 
          desired sampling rate and the second parameter is the maximally 
          allowed error. For a relative rate specification the first 
          parameter is the interpolation factor and the second parameter
          is the decimation factor (both are integer parameters). The 
          third parameter is in both cases the required stop band 
          attenuation.
   
          @memo Add a resampling filter
          @see MultiRate.hh
          @param type Multirate type
          @param m1 First parameter (desired fS or interpolation factor)
          @param m2 Second parameter (error or decimation factor)
          @param atten Stop-band attenuation
          @return true if successful
       ******************************************************************/
    bool multirate( const char* type,
                    double      m1,
                    double      m2 = 1E-3,
                    double      atten = 80 );

    /** Make a mixer and add it to the current filter. This will 
          transform a real time series into a complex one. The mixer
          will heterodyne (multiply) a time series by 
          \f[ e^{ i (2 \pi f_c t + \phi)}.\f]
          @memo Add a mixer
          @see Mixer.hh
          @param fmix Mixer frequency (Hz)
          @param phase Initial phase (\f$ 0 <= \phi < 2 \pi \f$).
          @return true if successful
       ******************************************************************/
    bool mixer( double fmix, double phase = 0 );

    /** Read filter coefficients from front-end file, make filter 
          and add it to the current one.
          @memo Add a filter from a configuration file
          @param file Front-end configuration file
          @param module Module name or number
          @param section Filter section or -1 for all
          @return true if successful
       ******************************************************************/
    bool frontend( const char* file, const char* module, int section = -1 );

    /** Set the filter gain at specified frequency.
          @memo Set filter gain
          @param f Frequency 
          @param gain Set point
          @return true if successful
       ******************************************************************/
    bool setgain( double f, double gain );

    /** Form the closed loop response of the current filter.
          @memo Closed loop response
          @param k Additional gain: 1/(1+k*G(f)) 
          @return true if successful
       ******************************************************************/
    bool closeloop( double k = 1.0 );

    /** The transfer coefficient of the filter at the specified 
          frequency is calculated and returned as a complex number. 
          @memo Get a transfer coefficient of a Filter.
          @param coeff a complex number representing the Filter response 
                       at the specified frequency (return)
          @param f Frequency at which to sample the transfer function.
          @return true if successful       
       ******************************************************************/
    bool Xfer( fComplex& coeff, double f ) const;

    /** The transfer function of the filter at the specified frequency 
          points is calculated and returned as a complex array. 
          The frequency points are user supplied. The return array
          must be at least of length points.
          @memo Get the transfer function of a Filter.
          @param tf Transfer function (return)
          @param freqs Frequency points
          @param points Number of points.
          @return true if successful       
       ******************************************************************/
    bool Xfer( fComplex* tf, const float* freqs, int points ) const;

    /** The transfer function of the filter in the specified frequency 
          interval is calculated and returned as a complex frequency 
          series. 
          @memo Get the transfer function of a Filter.
          @param fseries a complex FSeries containing the Filter response 
                         at each frequency step (return)
          @param Fmin Minimum frequency at which to sample the 
                      transfer function.
          @param Fmax Maximum frequency at which to sample the 
                      transfer function.
          @param dF Frequency step.
          @return true if successful       
       ******************************************************************/
    bool Xfer( FSeries& fseries,
               double   Fmin = 0.0,
               double   Fmax = 0.0,
               double   dF = 1.0 ) const;
    /** The transfer function of the filter at the specified frequency 
          points is calculated and returned as a complex array. 
          The 'sweep type' can be "log" for logarithmically spaced
          frequency points or "linear" for linear spacing. The specified 
          arrays must be at least of length points.
          @memo Get the transfer function of a Filter.
          @param freqs Frequency points (return)
          @param tf Transfer function (return)
          @param fstart Minimum frequency at which to sample the 
                        transfer function.
          @param fstop Maximum frequency at which to sample the 
                       transfer function.
          @param points Number of points.
          @param type Type of frequency spacing.
          @return true if successful       
       ******************************************************************/
    bool Xfer( float*      freqs,
               fComplex*   tf,
               double      fstart,
               double      fstop = 0.0,
               int         points = 101,
               const char* type = "log" ) const;
    /** The transfer function of the filter in the specified frequency 
          interval is calculated using a swept sine. This method works
          with all filters but may take significantly longer than the
          direct method implemented for FIR and IIR filters.
          @memo Get the transfer function of a Filter.
          @param freqs Frequency points (return)
          @param tf Transfer function (return)
          @param param Swept sine parameters
          @return true if successful       
       ******************************************************************/
    bool Xfer( float* freqs, fComplex* tf, const SweptSine& param ) const;

    /** Compute the response of the filter. The requested response type
          must be either "step" for a step response, "impulse" for
          an impulse response, or "ramp" for a ramp response.
          @memo Compute filter response.
          @param output Time series (return)
          @param type Response type 
          @param duration Length of returned time series.
          @return time series of response       
       ******************************************************************/
    bool response( TSeries&        output,
                   const char*     type = "step",
                   const Interval& duration = 1.0 ) const;
    /** Compute the response of the filter.
          @memo Compute filter response.
          @param output Time series (return)
          @param func Signal function 
          @param duration Length of returned time series.
          @return time series of response       
       ******************************************************************/
    bool response( TSeries&        output,
                   const Chirp&    func,
                   const Interval& duration = 1.0 ) const;
    /** Compute the response of the filter. 
          @memo Compute filter response.
          @param output Time series (return)
          @param input Input time series 
          @return time series of response       
       ******************************************************************/
    bool response( TSeries& output, const TSeries& input ) const;

    /** Make a bode plot of the filter plot it on a Root canvas. The plot 
          contains two windows with amplitude in dB vs. frequency and phase
          in degrees ve. frequency. The frequency axis is the same on the two 
	  plots and may be linear or logarithmic, as specified by the \a type 
	  argument.
          @memo Bode plot of filter.
          @param fstart Minimum frequency at which to sample the 
                        transfer function.
          @param fstop Maximum frequency at which to sample the 
                       transfer function.
          @param points Number of points.
          @param type Type of frequency spacing, either linear ("lin") or
		      logarithmic ("log").
          @return true if successful
       ******************************************************************/
    bool bode( double      fstart,
               double      fstop = 0.0,
               int         points = 101,
               const char* type = "log" ) const;

    /** Make a bode plot of the filter plot it on a Root canvas. The plot 
	  contains two windows with amplitude in dB vs. frequency and phase
	  in degrees ve. frequency. The frequency axis is logarithmic.
          @memo Bode ploti of filter.
          @param freqs List of frequencies.
          @param points Number of points.
          @return true if successful
       ******************************************************************/
    bool bode( const float* freqs, int points = 101 ) const;

    /** Bode plot of filter. Make a bode plot of the filter using a
          swept sine method. This method works with all filters but may 
          take significantly longer than the direct method implemented 
          for FIR and IIR filters.
          @memo Bode plot.
          @param param Swept sine parameters
          @return true if successful
       ******************************************************************/
    bool bode( const SweptSine& param ) const;

    /** Plot the response of the filter. The requested response type
          must be either "step" for a step response, "impulse" for
          an impulse response, or "ramp" for a ramp response.
          @memo Plot filter response.
          @param type Response type.
          @param duration Length of returned time series.
          @return true if successful
       ******************************************************************/
    bool resp( const char*     type = "step",
               const Interval& duration = 1.0 ) const;

    /** Plot the response of the filter.
          @memo Plot filter response.
          @param func Signal function.
          @param duration Length of returned time series.
          @return true if successful
       ******************************************************************/
    bool resp( const Chirp& func, const Interval& duration = 1.0 ) const;

    /** Plot the response of the filter.
          @memo Plot filter response.
          @param input Input time series
          @return true if successful
       ******************************************************************/
    bool resp( const TSeries& input ) const;

    /** Interactive design of a filter. Not yet implemented.
          @memo Filter wizard.
          @return true if a new filter was designed       
       ******************************************************************/
    bool wizard( );

private:
    /// Input sampling frequency
    double fFSample;
    /// Output sampling frequency
    double fFOut;
    /// Heterodyne flag
    bool fHeterodyne;
    /// Current filter
    Pipe* fFilter;
    /// Use prewarping
    bool fPrewarp;
    /// Name
    std::string fName;
    /// Pad returned by plot functions
    mutable ligogui::TLGMultiPad* fPad;
    /// filter specifiction
    std::string fFilterSpec;
    /// FIR Filter type
    int fFirType;

protected:
    /**  Make a Bode plot.
       *  @memo Bode plot.
       *  @param freqs List of frequencies.
       *  @param tf    Complex transfer function.
       *  @param points Number of frequency points.
       *  @return True if successful.
       */
    bool plotbode( const float* freqs, const fComplex* tf, int points ) const;
    /**  Plot a time series.
        *  @memo Plot time series.
	*  @param ts Time series reference.
	*  @return True if successful.
	*/
    bool plotts( const TSeries& ts ) const;
};

#endif // _LIGO_FILTERDESIGN_H
