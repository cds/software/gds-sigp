/* -*- mode: c++; c-basic-offset: 3; -*- */
/* version $Id$ */
#include "PConfig.h"
#include <string.h>
#include <strings.h>
#include <stdio.h>
#include <dlfcn.h>
#include <string>
#include <iostream>
#include <exception>
#include "FilterDesign.hh"
#include "MultiPipe.hh"
#include "IIRFilter.hh"
#include "FIRFilter.hh"
#include "FIRdft.hh"
#include "Difference.hh"
#include "DecimateBy2.hh"
#include "MultiRate.hh"
#include "Limiter.hh"
#include "Mixer.hh"
#include "LineFilter.hh"
#include "IIRdesign.hh"
#include "FIRdesign.hh"
#include "SweptSine.hh"
// #include "SignalGen.hh"
#include "Offset.hh"
#include "Ramp.hh"
#include "Impulse.hh"
//
#include "zp2zp.hh"
#include "iirutil.hh"

#ifdef P__WIN32
#define RTLD_LOCAL 0
#endif

using namespace std;

//______________________________________________________________________________
extern "C" {
typedef void ( *func_t )( void );
typedef ligogui::TLGMultiPad* ( *bodefunc_t )( const float*,
                                               const float*,
                                               int,
                                               const char* );
typedef ligogui::TLGMultiPad* ( *tsfunc_t )( const TSeries& );
typedef bool ( *wizfunc_t )( const string& name, string& filter );
}

//______________________________________________________________________________
#ifdef P__WIN32
const char* const libname = "libgdsplot.dll";
#else
#ifdef P__DARWIN
const char* const libname = "libgdsplot.dylib";
#else
const char* const libname = "libgdsplot.so";
#endif
#endif
const char* const fn_bode = "bodeplot___dynamic";
const char* const fn_ts = "tsplot___dynamic";
const char* const fn_wiz = "wizard___dynamic";

static bool              libloaded = false; // library loaded
static void*             handle = 0; // library handle
static func_t            dispatch[ 10 ]; // dispatch table
static const char* const funcname[] = // list of functions
    { fn_bode, fn_ts, fn_wiz, 0 };

//______________________________________________________________________________
static func_t
getFunc( int num = 0 )
{
    // dynamically load plot library
    if ( !libloaded )
    {
        // load librray
        handle = ::dlopen( libname, RTLD_NOW | RTLD_LOCAL );
        if ( handle == 0 )
        {
            cerr << "Unable to load library " << libname << endl;
            return 0;
        }
        // resolve dispatch table
        for ( int i = 0; funcname[ i ]; ++i )
        {
            dispatch[ i ] = (func_t)dlsym( handle, funcname[ i ] );
            if ( dispatch[ i ] == 0 )
            {
                cerr << "load failed for " << funcname[ i ] << endl;
                return 0;
            }
        }
        libloaded = true;
    }
    return dispatch[ num ];
}

//______________________________________________________________________________
inline static double
power( double x, double y )
{
    return exp( log( x ) * y );
}

//______________________________________________________________________________
FilterDesign::FilterDesign( double fsample, const char* name )
    : fFilter( 0 ), fPrewarp( true ), fName( name ? name : "filter" ),
      fPad( 0 ), fFirType( 0 )
{
    init( fsample );
}

//______________________________________________________________________________
FilterDesign::FilterDesign( const char* spec, double fsample, const char* name )
    : fFilter( 0 ), fPrewarp( true ), fName( name ? name : "filter" ),
      fPad( 0 ), fFirType( 0 )
{
    init( fsample );
    if ( spec )
        filter( spec );
}

//______________________________________________________________________________
FilterDesign::FilterDesign( const FilterDesign& design )
    : fFilter( 0 ), fFirType( 0 )
{
    *this = design;
}

//______________________________________________________________________________
FilterDesign::~FilterDesign( )
{
    if ( fFilter )
        delete fFilter;
}

//______________________________________________________________________________
FilterDesign&
FilterDesign::operator=( const FilterDesign& design )
{
    if ( this != &design )
    {
        if ( fFilter )
            delete fFilter;
        fFilter = 0;
        fFSample = design.fFSample;
        fFOut = design.fFOut;
        fHeterodyne = design.fHeterodyne;
        fPrewarp = design.fPrewarp;
        fName = design.fName;
        fPad = design.fPad;
        fFilterSpec = design.fFilterSpec;
        if ( design.fFilter )
        {
            fFilter = design.fFilter->clone( );
        }
        else
        {
            gain( 1.0 );
        }
        fFirType = design.fFirType;
    }
    return *this;
}

//______________________________________________________________________________
bool
FilterDesign::isUnityGain( ) const
{
    MultiPipe* mp = dynamic_cast< MultiPipe* >( fFilter );
    return ( mp && ( mp->getGain( ) == 1.0 ) && mp->pipe( ).empty( ) );
}

//______________________________________________________________________________
void
FilterDesign::init( double fsample )
{
    if ( fFilter )
        delete fFilter;
    fFilter = 0;
    fFSample = fsample;
    fFOut = fsample;
    fHeterodyne = false;
    gain( 1.0 ); // make sure we have at least an identity filter
    fFilterSpec = "";
    fFirType = 0;
}

//______________________________________________________________________________
Pipe*
FilterDesign::copy( ) const
{
    return fFilter ? fFilter->clone( ) : 0;
}

//______________________________________________________________________________
Pipe*
FilterDesign::release( )
{
    Pipe* tmp = fFilter;
    fFilter = 0;
    init( fFSample );
    return tmp;
}

//______________________________________________________________________________
void
FilterDesign::reset( )
{
    if ( fFilter )
        delete fFilter;
    fFilter = 0;
    fFOut = fFSample;
    fHeterodyne = false;
    gain( 1.0 ); // make sure we have at least an identity filter
    fFilterSpec = "";
}

//______________________________________________________________________________
void
FilterDesign::set( const Pipe& filter, double resampling, bool heterodyne )
{
    init( fFSample );
    fFilter = filter.clone( );
    fFOut = resampling * fFSample;
    if ( heterodyne )
        fHeterodyne = true;
}

//______________________________________________________________________________
bool
FilterDesign::add( const Pipe& filter, double resampling, bool heterodyne )
{
    // any filter at all?
    if ( !fFilter )
    {
        fFilter = filter.clone( );
        fFOut = resampling * fFSample;
        if ( heterodyne )
            fHeterodyne = true;
        return true;
    }
    // make sure we have a multi pipe
    MultiPipe* mp = dynamic_cast< MultiPipe* >( fFilter );
    if ( !mp )
    {
        mp = new MultiPipe;
        mp->addPipe( *fFilter );
        delete fFilter;
        fFilter = mp;
    }
    mp->addPipe( filter );
    fFOut = resampling * fFOut;
    if ( heterodyne )
        fHeterodyne = true;
    return true;
}

//______________________________________________________________________________
bool
FilterDesign::filter( const char* formula )
{
    return FilterParse::filter( formula );
}

//______________________________________________________________________________
bool
FilterDesign::gain( double g, const char* format )
{
    double gg = g;
    if ( format && ( strcasecmp( format, "dB" ) != 0 ) &&
         ( strcasecmp( format, "scalar" ) != 0 ) )
    {
        cerr << "Illegal gain format." << endl;
        return false;
    }
    if ( format && ( strcasecmp( format, "dB" ) == 0 ) )
    {
        gg = power( 10., g / 20. );
    }
    // make sure we have a multi pipe
    MultiPipe* mp = dynamic_cast< MultiPipe* >( fFilter );
    if ( !mp )
    {
        mp = new MultiPipe;
        if ( fFilter )
        {
            mp->addPipe( *fFilter );
            delete fFilter;
        }
        fFilter = mp;
    }
    mp->setGain( gg * mp->getGain( ) );
    // update filter spec
    char buf[ 1024 ];
    sprintf( buf, "gain(%g", g );
    fFilterSpec += buf;
    if ( format && ( strcasecmp( format, "\"dB\"" ) == 0 ) )
    {
        fFilterSpec += ",dB";
    }
    fFilterSpec += ")";
    return true;
}

//______________________________________________________________________________
bool
FilterDesign::pole( double f, double gain, const char* plane )
{
    bool succ;
    try
    {
        succ = add( ::pole( fFOut, f, gain, plane, fPrewarp ) );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        char buf[ 1024 ];
        sprintf( buf, "pole(%g", f );
        fFilterSpec += buf;
        if ( fabs( gain - 1 ) > 1E-12 )
        {
            sprintf( buf, ",%g", gain );
            fFilterSpec += buf;
        }
        if ( plane && ( strcasecmp( plane, "s" ) != 0 ) )
        {
            fFilterSpec += string( ",\"" ) + plane + "\"";
        }
        fFilterSpec += ")";
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::zero( double f, double gain, const char* plane )
{
    bool succ;
    try
    {
        succ = add( ::zero( fFOut, f, gain, plane, fPrewarp ) );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        char buf[ 1024 ];
        sprintf( buf, "zero(%g", f );
        fFilterSpec += buf;
        if ( fabs( gain - 1 ) > 1E-12 )
        {
            sprintf( buf, ",%g", gain );
            fFilterSpec += buf;
        }
        if ( plane && ( strcasecmp( plane, "s" ) != 0 ) )
        {
            fFilterSpec += string( ",\"" ) + plane + "\"";
        }
        fFilterSpec += ")";
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::pole2( double f, double Q, double gain, const char* plane )
{
    bool succ;
    try
    {
        succ = add( ::pole2( fFOut, f, Q, gain, plane, fPrewarp ) );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        char buf[ 1024 ];
        sprintf( buf, "pole2(%g,%g", f, Q );
        fFilterSpec += buf;
        if ( fabs( gain - 1 ) > 1E-12 )
        {
            sprintf( buf, ",%g", gain );
            fFilterSpec += buf;
        }
        if ( plane && ( strcasecmp( plane, "s" ) != 0 ) )
        {
            fFilterSpec += string( ",\"" ) + plane + "\"";
        }
        fFilterSpec += ")";
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::zero2( double f, double Q, double gain, const char* plane )
{
    bool succ;
    try
    {
        succ = add( ::zero2( fFOut, f, Q, gain, plane, fPrewarp ) );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        char buf[ 1024 ];
        sprintf( buf, "zero2(%g,%g", f, Q );
        fFilterSpec += buf;
        if ( fabs( gain - 1 ) > 1E-12 )
        {
            sprintf( buf, ",%g", gain );
            fFilterSpec += buf;
        }
        if ( plane && ( strcasecmp( plane, "s" ) != 0 ) )
        {
            fFilterSpec += string( ",\"" ) + plane + "\"";
        }
        fFilterSpec += ")";
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::zpk( int             nzeros,
                   const dComplex* zero,
                   int             npoles,
                   const dComplex* pole,
                   double          gain,
                   const char*     plane )
{
    bool      succ;
    IIRFilter iir;
    try
    {
        iir = ::zpk( fFOut, nzeros, zero, npoles, pole, gain, plane, fPrewarp );
        succ = add( iir );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        string s;
        iir2zpk( iir, s, plane, fPrewarp );
        fFilterSpec += s;
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::zpk( int             nzeros,
                   const fComplex* zero,
                   int             npoles,
                   const fComplex* pole,
                   double          gain,
                   const char*     plane )
{
    bool      succ;
    IIRFilter iir;
    try
    {
        iir = ::zpk( fFOut, nzeros, zero, npoles, pole, gain, plane, fPrewarp );
        succ = add( iir );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        string s;
        iir2zpk( iir, s, plane, fPrewarp );
        fFilterSpec += s;
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::rpoly( int           nnumer,
                     const double* numer,
                     int           ndenom,
                     const double* denom,
                     double        gain )
{
    bool      succ;
    IIRFilter iir;
    try
    {
        iir = ::rpoly( fFOut, nnumer, numer, ndenom, denom, gain, fPrewarp );
        succ = add( iir );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        string s;
        iir2zpk( iir, s, "p", fPrewarp );
        fFilterSpec += s;
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::rpoly( int          nnumer,
                     const float* numer,
                     int          ndenom,
                     const float* denom,
                     double       gain )
{
    bool      succ;
    IIRFilter iir;
    try
    {
        iir = ::rpoly( fFOut, nnumer, numer, ndenom, denom, gain, fPrewarp );
        succ = add( iir );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        string s;
        iir2zpk( iir, s, "p", fPrewarp );
        fFilterSpec += s;
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::biquad( double b0, double b1, double b2, double a1, double a2 )
{
    bool succ;
    try
    {
        succ = add( ::biquad( fFOut, b0, b1, b2, a1, a2 ) );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        char buf[ 1024 ];
        sprintf( buf, "biquad(%g,%g,%g,%g,%g)", b0, b1, b2, a1, a2 );
        fFilterSpec += buf;
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::sos( int nba, const double* ba, const char* format )
{
    bool      succ;
    IIRFilter iir;
    try
    {
        iir = ::sos( fFOut, nba, ba, format );
        succ = add( iir );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        string s;
        iir2z( iir, s, format );
        fFilterSpec += s;
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::zroots( int             nzeros,
                      const dComplex* zero,
                      int             npoles,
                      const dComplex* pole,
                      double          gain )
{
    bool      succ;
    IIRFilter iir;
    try
    {
        iir = ::zroots( fFOut, nzeros, zero, npoles, pole, gain );
        succ = add( iir );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        string s;
        iir2z( iir, s, "r" );
        fFilterSpec += s;
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::zroots( int             nzeros,
                      const fComplex* zero,
                      int             npoles,
                      const fComplex* pole,
                      double          gain )
{
    bool      succ;
    IIRFilter iir;
    try
    {
        iir = ::zroots( fFOut, nzeros, zero, npoles, pole, gain );
        succ = add( iir );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        string s;
        iir2z( iir, s, "r" );
        fFilterSpec += s;
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::direct( int nb, const double* b, int na, const double* a )
{
    bool      succ;
    IIRFilter iir;
    try
    {
        iir = ::direct( fFOut, nb, b, na, a );
        succ = add( iir );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        string s;
        iir2z( iir, s, "d" );
        fFilterSpec += s;
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::ellip(
    Filter_Type type, int order, double rp, double rs, double f1, double f2 )
{
    bool succ;
    try
    {
        succ = add( ::ellip( type, order, rp, rs, fFOut, f1, f2, fPrewarp ) );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        char buf[ 1024 ];
        sprintf( buf,
                 "ellip(\"%s\",%i,%g,%g,%g",
                 getFilterString( type ).c_str( ),
                 order,
                 rp,
                 rs,
                 f1 );
        fFilterSpec += buf;
        if ( ( type == kBandPass ) || ( type == kBandStop ) )
        {
            sprintf( buf, ",%g", f2 );
            fFilterSpec += buf;
        }
        fFilterSpec += ")";
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::cheby1(
    Filter_Type type, int order, double rp, double f1, double f2 )
{
    bool succ;
    try
    {
        succ = add( ::cheby1( type, order, rp, fFOut, f1, f2, fPrewarp ) );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        char buf[ 1024 ];
        sprintf( buf,
                 "cheby1(\"%s\",%i,%g,%g",
                 getFilterString( type ).c_str( ),
                 order,
                 rp,
                 f1 );
        fFilterSpec += buf;
        if ( ( type == kBandPass ) || ( type == kBandStop ) )
        {
            sprintf( buf, ",%g", f2 );
            fFilterSpec += buf;
        }
        fFilterSpec += ")";
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::cheby2(
    Filter_Type type, int order, double rs, double f1, double f2 )
{
    bool succ;
    try
    {
        succ = add( ::cheby2( type, order, rs, fFOut, f1, f2, fPrewarp ) );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        char buf[ 1024 ];
        sprintf( buf,
                 "cheby2(\"%s\",%i,%g,%g",
                 getFilterString( type ).c_str( ),
                 order,
                 rs,
                 f1 );
        fFilterSpec += buf;
        if ( ( type == kBandPass ) || ( type == kBandStop ) )
        {
            sprintf( buf, ",%g", f2 );
            fFilterSpec += buf;
        }
        fFilterSpec += ")";
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::butter( Filter_Type type, int order, double f1, double f2 )
{
    bool succ;
    try
    {
        succ = add( ::butter( type, order, fFOut, f1, f2, fPrewarp ) );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        char buf[ 1024 ];
        sprintf( buf,
                 "butter(\"%s\",%i,%g",
                 getFilterString( type ).c_str( ),
                 order,
                 f1 );
        fFilterSpec += buf;
        if ( ( type == kBandPass ) || ( type == kBandStop ) )
        {
            sprintf( buf, ",%g", f2 );
            fFilterSpec += buf;
        }
        fFilterSpec += ")";
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::notch( double f0, double Q, double depth )
{
    bool succ;
    try
    {
        succ = add( ::notch( fFOut, f0, Q, depth, fPrewarp ) );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        char buf[ 1024 ];
        sprintf( buf, "notch(%g,%g,%g)", f0, Q, depth );
        fFilterSpec += buf;
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::resgain( double f0, double Q, double height )
{
    bool succ;
    try
    {
        succ = add( ::resgain( fFOut, f0, Q, height, fPrewarp ) );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        char buf[ 1024 ];
        sprintf( buf, "resgain(%g,%g,%g)", f0, Q, height );
        fFilterSpec += buf;
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::comb( double f0, double Q, double amp, int N )
{
    bool succ;
    try
    {
        succ = add( ::comb( fFOut, f0, Q, amp, N, fPrewarp ) );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        char buf[ 1024 ];
        sprintf( buf, "comb(%g,%g,%g", f0, Q, amp );
        fFilterSpec += buf;
        if ( N > 0 )
        {
            sprintf( buf, ",%i", N );
            fFilterSpec += buf;
        }
        fFilterSpec += ")";
    }
    return succ;
}

//______________________________________________________________________________
void
FilterDesign::setFirType( int fir_type )
{
    fFirType = fir_type;
    if ( !fFirType || !fFilter )
        return;

    //--------------------------------  Deal with the content of MultiPipes
    MultiPipe* mp = dynamic_cast< MultiPipe* >( fFilter );
    if ( mp )
    {
        for ( auto p = mp->pipe( ).begin( ); p != mp->pipe( ).end( ); p++ )
        {
            FIRFilter* firf = dynamic_cast< FIRFilter* >( p->get( ) );
            if ( firf )
            {
                if ( ( fFirType & 2 ) != 0 )
                {
                    firf->setMode( FIRFilter::fm_drop_start );
                }
                else if ( ( fFirType & 1 ) != 0 )
                {
                    firf->setMode( FIRFilter::fm_zero_phase );
                }
                if ( ( fFirType & 4 ) != 0 )
                {
                    p->set( new FIRdft( *firf ) );
                    delete firf;
                }
            }
        }
    }

    //--------------------------------  Not a MultiPipe - is it a FIRFilter?
    else
    {
        FIRFilter* firf = dynamic_cast< FIRFilter* >( fFilter );
        if ( firf )
        {
            if ( ( fFirType & 2 ) != 0 )
            {
                firf->setMode( FIRFilter::fm_drop_start );
            }
            else if ( ( fFirType & 1 ) != 0 )
            {
                firf->setMode( FIRFilter::fm_zero_phase );
            }
            if ( ( fFirType & 4 ) != 0 )
            {
                fFilter = new FIRdft( *firf );
                delete firf;
            }
        }
    }
}

//______________________________________________________________________________
bool
FilterDesign::remez( int           N,
                     int           nBand,
                     const double* Bands,
                     const double* Func,
                     const double* Weight )
{
    bool succ = false;
    try
    {
        FIRFilter filt = dRemez( N, fFOut, nBand, Bands, Func, Weight );
        if ( ( fFirType & 2 ) != 0 )
            filt.setMode( FIRFilter::fm_drop_start );
        else if ( ( fFirType & 1 ) != 0 )
            filt.setMode( FIRFilter::fm_zero_phase );
        if ( ( fFirType & 4 ) != 0 )
            succ = add( FIRdft( filt ) );
        else
            succ = add( filt );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        cerr << "add remez filter" << endl;
        char buf[ 1024 ];
        sprintf( buf, "remez(%i", N );
        fFilterSpec += buf;
        const double* prm[] = { Bands, Func, Weight, 0 };
        for ( const double** x = prm; *x; ++x )
        {
            fFilterSpec += ",[";
            for ( int i = 0; i < ( ( x == prm ) ? 2 * nBand : nBand ); ++i )
            {
                if ( i > 0 )
                    fFilterSpec += ";";
                sprintf( buf, "%g", ( *x )[ i ] );
                fFilterSpec += buf;
            }
            fFilterSpec += "]";
        }
        fFilterSpec += ")";
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::firls( int           N,
                     int           nBand,
                     const double* Bands,
                     const double* Func,
                     const double* Weight )
{
    bool succ;
    try
    {
        FIRFilter filt = dFirLS( N, fFOut, nBand, Bands, Func, Weight );
        if ( ( fFirType & 2 ) != 0 )
            filt.setMode( FIRFilter::fm_drop_start );
        else if ( ( fFirType & 1 ) != 0 )
            filt.setMode( FIRFilter::fm_zero_phase );
        if ( ( fFirType & 4 ) != 0 )
            succ = add( FIRdft( filt ) );
        else
            succ = add( filt );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }

    // update filter spec
    if ( succ )
    {
        char buf[ 1024 ];
        sprintf( buf, "firls(%i", N );
        fFilterSpec += buf;
        const double* prm[] = { Bands, Func, Weight, 0 };
        const int     len[] = { 2 * nBand, 2 * nBand, nBand, 0 };
        for ( int j = 0; j < 3; ++j )
        {
            const double* x = prm[ j ];
            if ( !x[ j ] )
                break;
            fFilterSpec += ",[";
            for ( int i = 0; i < len[ j ]; ++i )
            {
                if ( i > 0 )
                    fFilterSpec += ";";
                sprintf( buf, "%g", x[ i ] );
                fFilterSpec += buf;
            }
            fFilterSpec += "]";
        }
        fFilterSpec += ")";
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::firw( int         N,
                    Filter_Type type,
                    const char* window,
                    double      Flow,
                    double      Fhigh,
                    double      Ripple,
                    double      dF )
{
    bool succ = false;
    try
    {
        const char* ftype = "LowPass";
        switch ( type )
        {
        case kLowPass:
            ftype = "LowPass";
            break;
        case kHighPass:
            ftype = "HighPass";
            break;
        case kBandPass:
            ftype = "BandPass";
            break;
        case kBandStop:
            ftype = "BandStop";
            break;
        }
        FIRFilter filt =
            dFirW( N, fFOut, window, ftype, Flow, Fhigh, Ripple, dF );
        if ( ( fFirType & 2 ) != 0 )
            filt.setMode( FIRFilter::fm_drop_start );
        else if ( ( fFirType & 1 ) != 0 )
            filt.setMode( FIRFilter::fm_zero_phase );
        if ( ( fFirType & 4 ) != 0 )
            succ = add( FIRdft( filt ) );
        else
            succ = add( filt );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        char buf[ 1024 ];
        sprintf( buf,
                 "firw(%i,\"%s\",\"%s\",%g,%g,%g,%g)",
                 N,
                 getFilterString( type ).c_str( ),
                 window ? window : "",
                 Flow,
                 Fhigh,
                 Ripple,
                 dF );
        fFilterSpec += buf;
    }
    return succ;
}

//=============================================================================
bool
FilterDesign::fircoefs( int N, const double* coefs, bool mode )
{
    FIRdft fir( N - 1, fFOut );
    fir.setCoefs( coefs );
    if ( mode )
        fir.setMode( FIRdft::fm_zero_phase );
    bool succ = add( fir );

    // update filter spec
    if ( succ )
    {
        char buf[ 1024 ];
        sprintf( buf, "fircoef(%i", N );
        fFilterSpec += buf;
        fFilterSpec += ",[";
        for ( int i = 0; i < N; ++i )
        {
            if ( i > 0 )
                fFilterSpec += ";";
            sprintf( buf, "%g", coefs[ i ] );
            fFilterSpec += buf;
        }
        fFilterSpec += "]";
        if ( mode )
            fFilterSpec += ",\"zero_phase\"";
        fFilterSpec += ")";
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::difference( )
{
    Difference d( fFOut );
    bool       succ = add( d );
    // update filter spec
    if ( succ )
    {
        fFilterSpec += "difference()";
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::decimateBy2( int N, int FilterID )
{
    if ( N < 1 )
    {
        return false;
    }
    int  decfactor = 1 << N;
    bool succ = add( DecimateBy2( N, FilterID ), 1. / (double)decfactor );
    // update filter spec
    if ( succ )
    {
        char buf[ 1024 ];
        sprintf( buf, "decimateBy2(%i,%i)", N, FilterID );
        fFilterSpec += buf;
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::linefilter( double f, double T, int fid, int nT )
{
    bool succ = add( LineFilter( f, T, fid, nT ) );
    // update filter spec
    if ( succ )
    {
        char buf[ 1024 ];
        sprintf( buf, "linefilter(%g,%g,%i,%i)", f, T, fid, nT );
        fFilterSpec += buf;
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::limiter( const char* type, double l1, double l2, double l3 )
{
    bool    succ;
    Limiter lim;
    try
    {
        lim = ::limiter( type, fFOut, l1, l2, l3 );
        succ = add( lim );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    // update filter spec
    if ( succ )
    {
        fFilterSpec += limiter2str( lim );
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::multirate( const char* type, double m1, double m2, double atten )
{
    bool succ;
    try
    {
        succ = add( ::multirate( type, m1, m2, atten ) );
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
    if ( succ )
    {
        char buf[ 1024 ];
        sprintf( buf,
                 "multirate(\"%s\",%g,%g,%g)",
                 type ? type : "",
                 m1,
                 m2,
                 atten );
        fFilterSpec += buf;
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::mixer( double fmix, double phase )
{
    Mixer mix;
    mix.setFcHz( fmix );
    mix.setPhase( phase );
    bool succ = add( mix, 1.0, true );
    if ( succ )
    {
        char buf[ 1024 ];
        sprintf( buf, "mixer(%g,%g)", fmix, phase );
        fFilterSpec += buf;
    }
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::frontend( const char* file, const char* module, int section )
{
    try
    {
        return false;
    }
    catch ( exception& err )
    {
        cerr << err.what( ) << endl;
        return false;
    }
}

//______________________________________________________________________________
bool
FilterDesign::setgain( double f, double g )
{
    // Calculate scaling
    fComplex c;
    if ( !Xfer( c, f ) )
    {
        return false;
    }
    double scale = g;
    if ( abs( c ) > 0 )
    {
        scale /= abs( c );
    }
    else
    {
        scale *= 1E20;
    }
    return gain( scale );
}

//______________________________________________________________________________
bool
FilterDesign::closeloop( double k )
{
    IIRFilter cl;
    bool      iir = true;
    try
    {
        cl = ::closeloop( get( ), k );
    }
    catch ( exception& err )
    {
        iir = false;
    }

    if ( !iir )
    { // Not yet supported
        cerr << "Not an IIR filter" << endl;
        bool succ = false;
        if ( succ )
        {
            char buf[ 1024 ];
            sprintf( buf, "closeloop(%g)", k );
            fFilterSpec += buf;
        }
        return succ;
    }
    else
    {
        reset( );
        bool succ = add( cl );
        // update filter spec
        if ( succ )
        {
            string s;
            iir2zpk( cl, s, "n", fPrewarp );
            fFilterSpec += s;
        }
        return succ;
    }
}

//______________________________________________________________________________
bool
FilterDesign::Xfer( fComplex& coeff, double f ) const
{
    if ( !fFilter )
    {
        return false;
    }
    return fFilter->Xfer( coeff, f );
}

//______________________________________________________________________________
bool
FilterDesign::Xfer( fComplex* tf, const float* freqs, int points ) const
{
    if ( !fFilter )
    {
        return false;
    }
    return fFilter->Xfer( tf, freqs, points );
}

//______________________________________________________________________________
bool
FilterDesign::Xfer( FSeries& fseries,
                    double   Fmin,
                    double   Fmax,
                    double   dF ) const
{
    fseries = FSeries( );
    float Fny = fFOut / 2.0;
    if ( Fmin < 0 )
        Fmin = 0.0;
    if ( Fmax == 0.0 || Fmax > Fny )
        Fmax = Fny;
    if ( Fmin >= Fmax )
    {
        return false;
    }
    if ( dF <= 0.0 )
        dF = 1.0;
    int nBins = int( ( Fmax - Fmin ) / dF + 0.5 );

    // Fill a response curve
    float*    freqs = new float[ nBins ];
    fComplex* CVec = new fComplex[ nBins ];
    for ( int j = 0; j < nBins; j++ )
    {
        freqs[ j ] = Fmin + double( j ) * dF;
    }
    bool succ = Xfer( CVec, freqs, nBins );
    if ( succ )
    {
        fseries = FSeries( Fmin, dF, Time( 0 ), nBins / fFOut, nBins, CVec );
        fseries.setName( "Filter Response" );
    }
    delete[] CVec;
    delete[] freqs;
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::Xfer( float*      freqs,
                    fComplex*   tf,
                    double      fstart,
                    double      fstop,
                    int         points,
                    const char* type ) const
{
    if ( points <= 0 )
    {
        return true;
    }
    if ( !fFilter || !freqs || !tf )
    {
        cerr << "FilterDesign::Xfer(): fFilter, freqs, or tf is NULL" << endl;
        return false;
    }
    // set frequencies
    if ( fstart > fstop )
    {
        double tmp = fstart;
        fstart = fstop;
        fstop = tmp;
    }
    if ( type && ( strncasecmp( type, "lin", 3 ) == 0 ) )
    {
        if ( points == 1 )
        {
            freqs[ 0 ] = ( fstart + fstop ) / 2.;
        }
        else
        {
            for ( int i = 0; i < points; ++i )
            {
                freqs[ i ] =
                    fstart + (double)i / ( points - 1.0 ) * ( fstop - fstart );
            }
        }
    }
    else
    {
        if ( points == 1 )
        {
            freqs[ 0 ] = sqrt( fstart * fstop );
        }
        else
        {
            for ( int i = 0; i < points; ++i )
            {
                freqs[ i ] = fstart *
                    power( (double)fstop / fstart,
                           (double)i / ( points - 1.0 ) );
            }
        }
    }
    return Xfer( tf, freqs, points );
}

//______________________________________________________________________________
bool
FilterDesign::Xfer( float* freqs, fComplex* tf, const SweptSine& param ) const
{
    if ( !fFilter )
    {
        return false;
    }
    SweptSine ss = param;
    ss.SetSampling( fFSample );
    return ss.Sweep( *fFilter, freqs, tf );
}

//______________________________________________________________________________
bool
FilterDesign::response( TSeries&        output,
                        const char*     type,
                        const Interval& duration ) const
{
    if ( !fFilter )
    {
        cerr << "Invalid filter" << endl;
        return false;
    }
    // determine waveform
    string t( type ? type : "" );
    for ( string::iterator p = t.begin( ); p != t.end( ); ++p )
    {
        *p = tolower( *p );
    }
    Chirp* func = 0;
    if ( t == "step" )
    {
        func = new Offset( 1. );
    }
    else if ( t == "ramp" )
    {
        func = new Ramp( 1. / duration );
    }
    else if ( t == "impulse" )
    {
        func = new Impulse( 0.999 / fFSample );
    }
    else
    {
        cerr << "Unknown waveform specifiaction " << t << endl;
        return false;
    }
    // calculate response
    bool succ = response( output, *func, duration );
    delete func;
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::response( TSeries&        output,
                        const Chirp&    func,
                        const Interval& duration ) const
{
    if ( !fFilter )
    {
        cerr << "Invalid filter" << endl;
        return false;
    }
    TSeries input(
        Time( 0, 0 ), Interval( 1. / fFSample ), duration * fFSample, func );
    return response( output, input );
}

//______________________________________________________________________________
bool
FilterDesign::response( TSeries& output, const TSeries& input ) const
{
    Pipe* filter = copy( );
    if ( !filter )
    {
        cerr << "Invalid filter" << endl;
        return false;
    }
    bool succ = true;
    try
    {
        output = ( *filter )( input );
    }
    catch ( ... )
    {
        succ = false;
    }
    delete filter;
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::bode( double      fstart,
                    double      fstop,
                    int         points,
                    const char* type ) const
{
    if ( points <= 0 )
    {
        return false;
    }
    float*    freqs = new float[ points ];
    fComplex* tf = new fComplex[ points ];
    bool      succ = Xfer( freqs, tf, fstart, fstop, points, type );
    if ( succ )
    {
        succ = plotbode( freqs, tf, points );
    }
    delete[] freqs;
    delete[] tf;
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::bode( const float* freqs, int points ) const
{
    if ( points <= 0 )
    {
        return false;
    }
    fComplex* tf = new fComplex[ points ];
    bool      succ = Xfer( tf, freqs, points );
    if ( succ )
    {
        succ = plotbode( freqs, tf, points );
    }
    delete[] tf;
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::bode( const SweptSine& param ) const
{
    SweptSine ss = param;
    int       points = ss.GetPoints( );
    if ( points <= 0 )
    {
        return false;
    }
    float*    freqs = new float[ points ];
    fComplex* tf = new fComplex[ points ];
    bool      succ = Xfer( freqs, tf, ss );
    if ( succ )
    {
        succ = plotbode( freqs, tf, points );
    }
    delete[] freqs;
    delete[] tf;
    return succ;
}

//______________________________________________________________________________
bool
FilterDesign::plotbode( const float*    freqs,
                        const fComplex* tf,
                        int             points ) const
{
    // get function pointer
    bodefunc_t bodef = 0;
    bodef = (bodefunc_t)getFunc( 0 );
    if ( !bodef )
    {
        return false;
    }
    // call function
    const char* p = fName.empty( ) ? "filter" : fName.c_str( );
    fPad = bodef( freqs, (const float*)tf, points, p );
    return fPad != 0;
}

//______________________________________________________________________________
bool
FilterDesign::resp( const char* type, const Interval& duration ) const
{
    TSeries out;
    if ( !response( out, type, duration ) )
    {
        return false;
    }
    string n = string( type ) + " response of " + fName;
    out.setName( n.c_str( ) );
    return plotts( out );
}

//______________________________________________________________________________
bool
FilterDesign::resp( const Chirp& func, const Interval& duration ) const
{
    TSeries out;
    if ( !response( out, func, duration ) )
    {
        return false;
    }
    string n = "response of " + fName;
    out.setName( n.c_str( ) );
    return plotts( out );
}

//______________________________________________________________________________
bool
FilterDesign::resp( const TSeries& input ) const
{
    TSeries out;
    if ( !response( out, input ) )
    {
        return false;
    }
    string n = "response of " + fName;
    out.setName( n.c_str( ) );
    return plotts( out );
}

//______________________________________________________________________________
bool
FilterDesign::plotts( const TSeries& ts ) const
{
    // get function pointer
    tsfunc_t tsf = 0;
    tsf = (tsfunc_t)getFunc( 1 );
    if ( !tsf )
    {
        return false;
    }
    // call function
    fPad = tsf( ts );
    return fPad != 0;
}

//______________________________________________________________________________
bool
FilterDesign::wizard( )
{
    // get function pointer
    wizfunc_t wizf = 0;
    wizf = (wizfunc_t)getFunc( 2 );
    if ( !wizf )
    {
        return false;
    }

    // call function
    string filterspec = fFilterSpec;
    if ( wizf( fName, filterspec ) )
    {
        reset( );
        return filter( filterspec.c_str( ) );
    }
    else
    {
        return false;
    }
}
