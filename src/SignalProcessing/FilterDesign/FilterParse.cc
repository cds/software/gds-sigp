#include <cstring>
#include <cstdio>
#include <string>
#include <cstdlib>
#include <cctype>
#include <iostream>
#include "FilterParse.hh"
#include "zp2zp.hh"

using namespace std;

//======================================  Utility functions
void
FilterParse::splitLine( const string& str_in, StrList& strlist, char delim )
{
    if ( str_in.size( ) )
    {

        string            line = str_in + delim;
        string::size_type pos = 0, prev_pos = 0;
        string            value;
        while ( ( pos = line.find_first_of( delim, pos ) ) != string::npos )
        {
            value = line.substr( prev_pos, pos - prev_pos );

            // remove extra space from value
            while ( !value.empty( ) && isspace( value[ 0 ] ) )
            {
                value.erase( 0, 1 );
            }
            while ( !value.empty( ) && isspace( value[ value.size( ) - 1 ] ) )
            {
                value.erase( value.size( ) - 1, 1 );
            }

            if ( value.size( ) )
                strlist.push_back( value );

            prev_pos = ++pos;
        }
    }
}

//______________________________________________________________________________
bool
FilterParse::removequote( const string& str_in, string& str_out )
{
    if ( ( str_in.size( ) >= 2 ) &&
         ( ( str_in[ 0 ] == '"' && str_in[ str_in.size( ) - 1 ] == '"' ) ||
           ( str_in[ 0 ] == '\'' && str_in[ str_in.size( ) - 1 ] == '\'' ) ||
           ( str_in[ 0 ] == '[' && str_in[ str_in.size( ) - 1 ] == ']' ) ) )
    {

        str_out = str_in;
        str_out.erase( 0, 1 );
        str_out.erase( str_out.size( ) - 1, 1 );
    }
    else
    {
        cerr << "Quote/Bracket mismatch. " << str_in << endl;
        return false;
    }

    return true;
}

//______________________________________________________________________________
bool
FilterParse::str2cmplx( const string& str, dComplex& value )
{
    if ( str.empty( ) )
    {
        return false;
    }

    string line;

    // copy str to line except for spaces
    for ( string::const_iterator itr = str.begin( ); itr != str.end( ); ++itr )
    {
        if ( !isspace( *itr ) )
        {
            line += *itr;
        }
    }

    // look for +/- operator which separates Re and Im.
    string::size_type pos = 0;
    pos = line.find_first_of( "+-", 1 );
    // check for exponent
    if ( ( pos != string::npos ) && ( pos > 0 ) &&
         ( toupper( line[ pos - 1 ] ) == 'E' ) )
    {
        pos = line.find_first_of( "+-", pos + 1 );
    }

    // separate line into real and imaginary parts.
    string reline, imline;
    if ( pos != string::npos )
    {
        reline = line.substr( 0, pos );
        imline = line.substr( pos );
    }
    else if ( line.find_first_of( 'i' ) != string::npos )
    {
        imline = line;
    }
    else
    {
        reline = line;
    }

    // process real part
    int resign = 1;
    if ( !reline.empty( ) )
    {
        while ( !reline.empty( ) && !isdigit( reline[ 0 ] ) )
        {

            if ( reline[ 0 ] == '-' )
            {
                if ( resign == 1 )
                {
                    resign = -1;
                }
                else
                { // sign error
                    cerr << "Sign error in a Complex Number. " << str << endl;
                    value = polar( 0.0, 0.0 );
                    return false;
                }
            }

            reline.erase( 0, 1 );
        }
        while ( !reline.empty( ) && !isdigit( reline[ reline.size( ) - 1 ] ) )
        {
            reline.erase( reline.size( ) - 1, 1 );
        }
    }

    // process imaginary part
    int imsign = 1;
    if ( !imline.empty( ) )
    {
        bool isimag = false;
        while ( !imline.empty( ) && !isdigit( imline[ 0 ] ) )
        {

            if ( imline[ 0 ] == '-' )
            {
                if ( imsign == 1 )
                {
                    imsign = -1;
                }
                else
                { // sign error
                    cerr << "Sign error in a Complex Number. " << str << endl;
                    value = polar( 0.0, 0.0 );
                    return false;
                }
            }
            else if ( imline[ 0 ] == 'i' )
            {
                if ( !isimag )
                {
                    isimag = true;
                }
                else
                {
                    cerr << "Imaginary symbol error in a Complex Number. "
                         << str << endl;
                    value = polar( 0.0, 0.0 );
                    return false;
                }
            }

            imline.erase( 0, 1 );
        }
        while ( !imline.empty( ) && !isdigit( imline[ imline.size( ) - 1 ] ) )
        {

            if ( imline[ imline.size( ) - 1 ] == 'i' )
            {
                if ( !isimag )
                    isimag = true;
                else
                { // imag symbol error
                    cerr << "Imaginary symbol error in a Complex Number. "
                         << str << endl;
                    value = polar( 0.0, 0.0 );
                    return false;
                }
            }

            imline.erase( imline.size( ) - 1, 1 );
        }
        if ( imline.empty( ) )
            imline = "1"; // could i alone!

        if ( !isimag )
        {
            cerr << "Imaginary symbol error in a Complex Number. " << str
                 << endl;
            value = polar( 0.0, 0.0 );
            return false;
        }
    }

    value = dComplex( resign * atof( reline.c_str( ) ),
                      imsign * atof( imline.c_str( ) ) );

    return true;
}

//______________________________________________________________________________
bool
FilterParse::getComplexList( const string&       value,
                             vector< dComplex >& cmplxlist )
{
    string line;
    if ( !removequote( value, line ) )
    {
        return false;
    }

    StrList elements;
    splitLine( line, elements, ';' );
    for ( StrList::iterator itr = elements.begin( ); itr != elements.end( );
          ++itr )
    {
        dComplex val;
        if ( !str2cmplx( *itr, val ) )
        {
            return false;
        }
        cmplxlist.push_back( val );
    }

    return true;
}

//______________________________________________________________________________
bool
FilterParse::getDoubleList( const string& value, vector< double >& dbllist )
{
    string line;
    if ( !removequote( value, line ) )
    {
        return false;
    }

    StrList elements;
    splitLine( line, elements, ';' );
    for ( StrList::iterator itr = elements.begin( ); itr != elements.end( );
          ++itr )
    {
        dbllist.push_back( atof( itr->c_str( ) ) );
    }

    return true;
}

//______________________________________________________________________________
bool
FilterParse::getFilterType( const char* type_name, Filter_Type& type )
{
    return ::getFilterType( type_name, type );
}

//______________________________________________________________________________
std::string
FilterParse::getFilterString( Filter_Type type )
{
    return ::getFilterString( type );
}

//______________________________________________________________________________
FilterParse::FilterParse( const char* spec )
{
    if ( spec )
        filter( spec );
}

//______________________________________________________________________________
bool
FilterParse::filter( const char* formula )
{
    string line = formula;

    // replace * and /. with spaces.
    string::size_type pos = 0;
    while ( ( pos = line.find_first_of( '*', pos ) ) != string::npos )
    {
        line.replace( pos, 1, " " );
    }
    pos = 0;
    while ( ( pos = line.find( "/.", pos ) ) != string::npos )
    {
        line.replace( pos, 2, " " );
    }

    pos = 0;
    string::size_type prev_pos = 0;
    string            name, arg;
    while ( ( pos = line.find_first_of( '(', pos ) ) != string::npos )
    {

        name = line.substr( prev_pos, pos - prev_pos );
        prev_pos = ++pos;
        pos = line.find_first_of( ')', prev_pos );
        if ( pos == string::npos )
            return false;
        arg = line.substr( prev_pos, pos - prev_pos );

        // remove extra space from name
        while ( !name.empty( ) && isspace( name[ 0 ] ) )
        {
            name.erase( 0, 1 );
        }
        while ( !name.empty( ) && isspace( name[ name.size( ) - 1 ] ) )
        {
            name.erase( name.size( ) - 1, 1 );
        }
        // remove extra space from arg
        while ( !arg.empty( ) && isspace( arg[ 0 ] ) )
        {
            arg.erase( 0, 1 );
        }
        while ( !arg.empty( ) && isspace( arg[ arg.size( ) - 1 ] ) )
        {
            arg.erase( arg.size( ) - 1, 1 );
        }

        if ( name.empty( ) )
        {
            return false;
        }

        //funclist.push_back( FunctionPair( name, arg ) );
        if ( !addfilter( name, arg ) )
        {
            cerr << "Error adding " << name << endl;
            return false;
        }

        prev_pos = ++pos;
    }

    return true;
}

//______________________________________________________________________________
bool
FilterParse::addfilter( const string& name, const string& arg )
{
    StrList arglist;
    splitLine( arg, arglist );

    int size = arglist.size( );

    string tname = name;
    for ( size_t i = 0; i < name.size( ); i++ )
        tname[ i ] = tolower( name[ i ] );

    //--------------------------------  gain
    if ( tname == "gain" )
    {

        double g;
        string format = "scalar";

        switch ( size )
        {
        case 2:
            if ( !removequote( arglist[ 1 ], format ) )
            {
                return false;
            }
            // no break!
        case 1: {
            g = atof( arglist[ 0 ].c_str( ) );
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        if ( !gain( g, format.c_str( ) ) )
        {
            return false;
        }
    }
    //--------------------------------  pole
    else if ( tname == "pole" )
    {

        double f;
        double gain = 1;
        string plane = "s";

        bool findgain = false;
        switch ( size )
        {
        case 3:
            if ( !removequote( arglist[ 2 ], plane ) )
            {
                return false;
            }
            // no break!
        case 2:
            if ( isdigit( arglist[ 1 ][ arglist[ 1 ].size( ) - 1 ] ) )
            {
                gain = atof( arglist[ 1 ].c_str( ) );
                findgain = true;
            }
            else
            {
                if ( !removequote( arglist[ 1 ], plane ) )
                {
                    return false;
                }
            }
            // no break!
        case 1: {
            f = atof( arglist[ 0 ].c_str( ) );
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        if ( plane.size( ) != 1 ||
             plane.find_first_of( "sfn" ) == string::npos )
        {
            cerr << "Invalid plane option." << endl;
            return false;
        }

        if ( findgain )
        {
            if ( !pole( f, gain, plane.c_str( ) ) )
            {
                return false;
            }
        }
        else
        {
            if ( !pole( f, plane.c_str( ) ) )
            {
                return false;
            }
        }
    }
    //--------------------------------  zero
    else if ( tname == "zero" )
    {

        double f;
        double gain = 1;
        string plane = "s";

        bool findgain = false;
        switch ( size )
        {
        case 3:
            if ( !removequote( arglist[ 2 ], plane ) )
            {
                return false;
            }
            // no break!
        case 2:
            if ( isdigit( arglist[ 1 ][ arglist[ 1 ].size( ) - 1 ] ) )
            {
                gain = atof( arglist[ 1 ].c_str( ) );
                findgain = true;
            }
            else
            {
                if ( !removequote( arglist[ 1 ], plane ) )
                {
                    return false;
                }
            }
            // no break!
        case 1: {
            f = atof( arglist[ 0 ].c_str( ) );
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        if ( plane.size( ) != 1 ||
             plane.find_first_of( "sfn" ) == string::npos )
        {
            cerr << "Invalid plane option." << endl;
            return false;
        }

        if ( findgain )
        {
            if ( !zero( f, gain, plane.c_str( ) ) )
            {
                return false;
            }
        }
        else
        {
            if ( !zero( f, plane.c_str( ) ) )
            {
                return false;
            }
        }
    }
    //--------------------------------  pole2
    else if ( tname == "pole2" )
    {

        double f;
        double Q;
        double gain = 1;
        string plane = "s";

        bool findgain = false;
        switch ( size )
        {
        case 4:
            if ( !removequote( arglist[ 3 ], plane ) )
            {
                return false;
            }
            // no break!
        case 3:
            if ( isdigit( arglist[ 2 ][ arglist[ 2 ].size( ) - 1 ] ) )
            {
                gain = atof( arglist[ 2 ].c_str( ) );
                findgain = true;
            }
            else
            {
                if ( !removequote( arglist[ 2 ], plane ) )
                {
                    return false;
                }
            }
            // no break!
        case 2: {
            Q = atof( arglist[ 1 ].c_str( ) );
            f = atof( arglist[ 0 ].c_str( ) );
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        if ( plane.size( ) != 1 ||
             plane.find_first_of( "sfn" ) == string::npos )
        {
            cerr << "Invalid plane option." << endl;
            return false;
        }

        if ( findgain )
        {
            if ( !pole2( f, Q, gain, plane.c_str( ) ) )
            {
                return false;
            }
        }
        else
        {
            if ( !pole2( f, Q, plane.c_str( ) ) )
            {
                return false;
            }
        }
    }
    //--------------------------------  zero2
    else if ( tname == "zero2" )
    {

        double f;
        double Q;
        double gain = 1;
        string plane = "s";

        bool findgain = false;
        switch ( size )
        {
        case 4:
            if ( !removequote( arglist[ 3 ], plane ) )
            {
                return false;
            }
            // no break!
        case 3:
            if ( isdigit( arglist[ 2 ][ arglist[ 2 ].size( ) - 1 ] ) )
            {
                gain = atof( arglist[ 2 ].c_str( ) );
                findgain = true;
            }
            else
            {
                if ( !removequote( arglist[ 2 ], plane ) )
                {
                    return false;
                }
            }
            // no break!
        case 2: {
            Q = atof( arglist[ 1 ].c_str( ) );
            f = atof( arglist[ 0 ].c_str( ) );
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        if ( plane.size( ) != 1 ||
             plane.find_first_of( "sfn" ) == string::npos )
        {
            cerr << "Invalid plane option." << endl;
            return false;
        }

        if ( findgain )
        {
            if ( !zero2( f, Q, gain, plane.c_str( ) ) )
            {
                return false;
            }
        }
        else
        {
            if ( !zero2( f, Q, plane.c_str( ) ) )
            {
                return false;
            }
        }
    }
    //--------------------------------  zpk
    else if ( tname == "zpk" )
    {

        int size = arglist.size( );

        string             plane = "s";
        vector< dComplex > zlist;
        vector< dComplex > plist;
        double             g = 1.0;

        switch ( size )
        {
        case 4: {
            if ( !removequote( arglist[ 3 ], plane ) )
            {
                return false;
            }
            if ( plane.size( ) > 1 ||
                 plane.find_first_of( "sfn" ) == string::npos )
            {
                cerr << "Invalid plane argument." << endl;
                return false;
            }
        }
            // no break!
        case 3: {
            g = atof( arglist[ 2 ].c_str( ) );
        }
            // no break!
        case 2: {
            if ( !getComplexList( arglist[ 1 ], plist ) ||
                 !getComplexList( arglist[ 0 ], zlist ) )
            {
                cerr << "Invalid list of complex numbers." << endl;
                return false;
            }
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        dComplex* zero;
        if ( zlist.size( ) )
        {
            zero = new dComplex[ zlist.size( ) ];
            int indx = 0;
            for ( vector< dComplex >::iterator itr = zlist.begin( );
                  itr != zlist.end( );
                  ++itr )
            {
                zero[ indx ] = *itr;
                ++indx;
            }
        }
        else
        {
            zero = new dComplex[ 1 ];
        }

        dComplex* pole;
        if ( plist.size( ) )
        {
            pole = new dComplex[ plist.size( ) ];
            int indx = 0;
            for ( vector< dComplex >::iterator itr = plist.begin( );
                  itr != plist.end( );
                  ++itr )
            {
                pole[ indx ] = *itr;
                ++indx;
            }
        }
        else
        {
            pole = new dComplex[ 1 ];
        }

        bool noerr =
            zpk( zlist.size( ), zero, plist.size( ), pole, g, plane.c_str( ) );

        if ( zero )
            delete[] zero;
        if ( pole )
            delete[] pole;

        if ( !noerr )
            return false;
    }
    //--------------------------------  rpoly
    else if ( tname == "rpoly" )
    {

        if ( size != 3 )
        {
            cerr << "Too few/many arguments." << endl;
            return false;
        }

        vector< double > nmlist;
        vector< double > dnlist;
        double           g = 1.0;

        if ( !getDoubleList( arglist[ 0 ], nmlist ) )
        {
            cerr << "Invalid list of real numbers." << endl;
        }
        if ( nmlist.size( ) < 1 )
        {
            cerr << "Number of coefficients must be at "
                    "least 1."
                 << endl;
            return false;
        }
        if ( !getDoubleList( arglist[ 1 ], dnlist ) )
        {
            cerr << "Invalid list of real numbers." << endl;
        }
        if ( dnlist.size( ) < 1 )
        {
            cerr << "Number of coefficients must be at "
                    "least 1."
                 << endl;
            return false;
        }
        g = atof( arglist[ 2 ].c_str( ) );

        double* nm = new double[ nmlist.size( ) ];
        int     indx = 0;
        for ( vector< double >::iterator itr = nmlist.begin( );
              itr != nmlist.end( );
              ++itr )
        {
            nm[ indx ] = *itr;
            ++indx;
        }
        double* dn = new double[ dnlist.size( ) ];
        indx = 0;
        for ( vector< double >::iterator itr = dnlist.begin( );
              itr != dnlist.end( );
              ++itr )
        {
            dn[ indx ] = *itr;
            ++indx;
        }

        bool noerr = rpoly( nmlist.size( ), nm, dnlist.size( ), dn, g );

        delete[] nm;
        delete[] dn;

        if ( !noerr )
            return false;
    }
    //--------------------------------  biquad
    else if ( tname == "biquad" )
    {

        if ( size != 5 )
        {
            cerr << "Too few/many arguments." << endl;
            return false;
        }

        double b0 = atof( arglist[ 0 ].c_str( ) );
        double b1 = atof( arglist[ 1 ].c_str( ) );
        double b2 = atof( arglist[ 2 ].c_str( ) );
        double a1 = atof( arglist[ 3 ].c_str( ) );
        double a2 = atof( arglist[ 4 ].c_str( ) );

        if ( !biquad( b0, b1, b2, a1, a2 ) )
        {
            return false;
        }
    }
    //--------------------------------  sos
    else if ( tname == "sos" )
    {

        int              size = arglist.size( );
        string           format = "s";
        vector< double > balist;
        double           g = 1.0;

        switch ( size )
        {
        case 3: {
            if ( !removequote( arglist[ 2 ], format ) )
            {
                return false;
            }
            if ( format.size( ) > 1 ||
                 format.find_first_of( "so" ) == string::npos )
            {
                cerr << "Invalid format argument." << endl;
                return false;
            }
        }
        // no break!
        case 2: {
            if ( !getDoubleList( arglist[ 1 ], balist ) )
            {
                cerr << "Invalid list of real numbers." << endl;
                return false;
            }
            if ( balist.size( ) % 4 != 0 )
            {
                cerr << "Number of coefficients must be a "
                        "multiple of 4."
                     << endl;
                return false;
            }
            g = atof( arglist[ 0 ].c_str( ) );
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        int     indx = 1;
        double* ba = new double[ balist.size( ) + 1 ];
        ba[ 0 ] = g;
        for ( vector< double >::iterator itr = balist.begin( );
              itr != balist.end( );
              ++itr )
        {
            ba[ indx ] = *itr;
            ++indx;
        }

        bool noerr = sos( balist.size( ) + 1, ba, format.c_str( ) );

        delete[] ba;

        if ( !noerr )
            return false;
    }
    //--------------------------------  zroots
    else if ( tname == "zroots" )
    {

        int size = arglist.size( );

        vector< dComplex > zlist;
        vector< dComplex > plist;
        double             g = 1.0;

        switch ( size )
        {
        case 3: {
            g = atof( arglist[ 2 ].c_str( ) );
        }
            // no break!
        case 2: {
            if ( !getComplexList( arglist[ 1 ], plist ) ||
                 !getComplexList( arglist[ 0 ], zlist ) )
            {
                cerr << "Invalid list of complex numbers." << endl;
            }
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        dComplex* zero;
        if ( zlist.size( ) )
        {
            zero = new dComplex[ zlist.size( ) ];
            int indx = 0;
            for ( vector< dComplex >::iterator itr = zlist.begin( );
                  itr != zlist.end( );
                  ++itr )
            {
                zero[ indx ] = *itr;
                ++indx;
            }
        }
        else
        {
            zero = new dComplex[ 1 ];
        }

        dComplex* pole;
        if ( plist.size( ) )
        {
            pole = new dComplex[ plist.size( ) ];
            int indx = 0;
            for ( vector< dComplex >::iterator itr = plist.begin( );
                  itr != plist.end( );
                  ++itr )
            {
                pole[ indx ] = *itr;
                ++indx;
            }
        }
        else
        {
            pole = new dComplex[ 1 ];
        }

        bool noerr = zroots( zlist.size( ), zero, plist.size( ), pole, g );

        if ( zero )
            delete[] zero;
        if ( pole )
            delete[] pole;

        if ( !noerr )
            return false;
    }
    //--------------------------------  direct
    else if ( tname == "direct" )
    {

        if ( size != 2 )
        {
            cerr << "Too few/many arguments." << endl;
            return false;
        }

        vector< double > blist;
        vector< double > alist;

        if ( !getDoubleList( arglist[ 0 ], blist ) )
        {
            cerr << "Invalid list of real numbers." << endl;
        }
        if ( blist.size( ) < 1 )
        {
            cerr << "Number of coefficients must be at "
                    "least 1."
                 << endl;
            return false;
        }
        if ( !getDoubleList( arglist[ 1 ], alist ) )
        {
            cerr << "Invalid list of real numbers." << endl;
        }

        double* b = new double[ blist.size( ) ];
        int     indx = 0;
        for ( vector< double >::iterator itr = blist.begin( );
              itr != blist.end( );
              ++itr )
        {
            b[ indx ] = *itr;
            ++indx;
        }
        double* a = new double[ alist.size( ) ];
        indx = 0;
        for ( vector< double >::iterator itr = alist.begin( );
              itr != alist.end( );
              ++itr )
        {
            a[ indx ] = *itr;
            ++indx;
        }

        bool noerr = direct( blist.size( ) - 1, b, alist.size( ), a );

        delete[] a;
        delete[] b;

        if ( !noerr )
            return false;
    }
    //--------------------------------  elliptic
    else if ( tname == "ellip" )
    {

        Filter_Type type;
        int         order;
        double      rp, as, f1;
        double      f2 = 0.0;

        switch ( size )
        {
        case 6: {
            f2 = atof( arglist[ 5 ].c_str( ) );
        }
            // no break!
        case 5: {
            f1 = atof( arglist[ 4 ].c_str( ) );
            as = atof( arglist[ 3 ].c_str( ) );
            rp = atof( arglist[ 2 ].c_str( ) );
            order = atoi( arglist[ 1 ].c_str( ) );
            string name;
            if ( !removequote( arglist[ 0 ], name ) )
            {
                return false;
            }
            if ( !getFilterType( name.c_str( ), type ) )
            {
                cerr << "Invalid filter type." << endl;
                return false;
            }
            if ( ( type == kBandPass || type == kBandStop ) && size != 6 )
            {
                cerr << "BandPass and BandStop filters require two "
                     << "cutoff frequencies." << endl;
                return false;
            }
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        if ( !ellip( type, order, rp, as, f1, f2 ) )
        {
            return false;
        }
    }
    //--------------------------------  chebyshev type-1
    else if ( tname == "cheby1" )
    {

        Filter_Type type;
        int         order;
        double      rp, f1;
        double      f2 = 0.0;

        switch ( size )
        {
        case 5: {
            f2 = atof( arglist[ 4 ].c_str( ) );
        }
            // no break!
        case 4: {
            f1 = atof( arglist[ 3 ].c_str( ) );
            rp = atof( arglist[ 2 ].c_str( ) );
            order = atoi( arglist[ 1 ].c_str( ) );
            string name;
            if ( !removequote( arglist[ 0 ], name ) )
            {
                return false;
            }
            if ( !getFilterType( name.c_str( ), type ) )
            {
                cerr << "Invalid filter type." << endl;
                return false;
            }
            if ( ( type == kBandPass || type == kBandStop ) && size != 5 )
            {
                cerr << "BandPass and BandStop filters require two "
                     << "cutoff frequencies." << endl;
                return false;
            }
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        if ( !cheby1( type, order, rp, f1, f2 ) )
        {
            return false;
        }
    }
    //--------------------------------  chebyshev type-2
    else if ( tname == "cheby2" )
    {

        Filter_Type type;
        int         order;
        double      as, f1;
        double      f2 = 0.0;

        switch ( size )
        {
        case 5: {
            f2 = atof( arglist[ 4 ].c_str( ) );
        }
            // no break!
        case 4: {
            f1 = atof( arglist[ 3 ].c_str( ) );
            as = atof( arglist[ 2 ].c_str( ) );
            order = atoi( arglist[ 1 ].c_str( ) );
            string name;
            if ( !removequote( arglist[ 0 ], name ) )
            {
                return false;
            }
            if ( !getFilterType( name.c_str( ), type ) )
            {
                cerr << "Invalid filter type." << endl;
                return false;
            }
            if ( ( type == kBandPass || type == kBandStop ) && size != 5 )
            {
                cerr << "BandPass and BandStop filters require two "
                     << "cutoff frequencies." << endl;
                return false;
            }
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        if ( !cheby2( type, order, as, f1, f2 ) )
        {
            return false;
        }
    }
    //--------------------------------  butterworth filter
    else if ( tname == "butter" )
    {

        Filter_Type type;
        int         order;
        double      f1;
        double      f2 = 0.0;

        switch ( size )
        {
        case 4: {
            f2 = atof( arglist[ 3 ].c_str( ) );
        }
            // no break!
        case 3: {
            f1 = atof( arglist[ 2 ].c_str( ) );
            order = atoi( arglist[ 1 ].c_str( ) );
            string name;
            if ( !removequote( arglist[ 0 ], name ) )
            {
                return false;
            }
            if ( !getFilterType( name.c_str( ), type ) )
            {
                cerr << "Invalid filter type." << endl;
                return false;
            }
            if ( ( type == kBandPass || type == kBandStop ) && size != 4 )
            {
                cerr << "BandPass and BandStop filters require two "
                     << "cutoff frequencies." << endl;
                return false;
            }
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        if ( !butter( type, order, f1, f2 ) )
        {
            return false;
        }
    }
    //--------------------------------  notch
    else if ( tname == "notch" )
    {

        double f0;
        double Q;
        double depth = 0.0;

        switch ( size )
        {
        case 3: {
            depth = atof( arglist[ 2 ].c_str( ) );
        }
            // no break!
        case 2: {
            Q = atof( arglist[ 1 ].c_str( ) );
            f0 = atof( arglist[ 0 ].c_str( ) );
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        if ( !notch( f0, Q, depth ) )
        {
            return false;
        }
    }
    //--------------------------------  resgain
    else if ( tname == "resgain" )
    {

        double f0;
        double Q;
        double height = 30.0;

        switch ( size )
        {
        case 3: {
            height = atof( arglist[ 2 ].c_str( ) );
        }
            // no break!
        case 2: {
            Q = atof( arglist[ 1 ].c_str( ) );
            f0 = atof( arglist[ 0 ].c_str( ) );
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        if ( !resgain( f0, Q, height ) )
        {
            return false;
        }
    }
    //--------------------------------  comb
    else if ( tname == "comb" )
    {

        int    N = 0;
        double amp = 0.0;
        double f0, Q;

        switch ( size )
        {
        case 4: {
            N = atoi( arglist[ 3 ].c_str( ) );
        }
            // no break!
        case 3: {
            amp = atof( arglist[ 2 ].c_str( ) );
        }
            // no break!
        case 2: {
            Q = atof( arglist[ 1 ].c_str( ) );
            f0 = atof( arglist[ 0 ].c_str( ) );
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        if ( !comb( f0, Q, amp, N ) )
        {
            return false;
        }
    }
    //--------------------------------  remez
    else if ( tname == "remez" )
    {

        int              N;
        int              nBand;
        vector< double > bandlist;
        vector< double > funclist;
        vector< double > weightlist;

        switch ( size )
        {
        case 4: {
            if ( !getDoubleList( arglist[ 3 ], weightlist ) ||
                 !weightlist.size( ) )
            {
                return false;
            }
        }
            // no break!
        case 3: {
            if ( !getDoubleList( arglist[ 2 ], funclist ) || !funclist.size( ) )
            {
                return false;
            }
            if ( !getDoubleList( arglist[ 1 ], bandlist ) || !bandlist.size( ) )
            {
                return false;
            }
            N = atoi( arglist[ 0 ].c_str( ) );
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }
        if ( bandlist.size( ) != 2 * funclist.size( ) )
        {
            cerr << "Invalid length of band/func list" << endl;
            return false;
        }
        if ( weightlist.size( ) &&
             ( bandlist.size( ) != 2 * weightlist.size( ) ) )
        {
            cerr << "Invalid length of weight list" << endl;
            return false;
        }
        nBand = bandlist.size( ) / 2;
        double* Bands = new double[ 2 * nBand ];
        double* Func = new double[ nBand ];
        double* Weight = weightlist.size( ) ? new double[ nBand ] : 0;
        for ( int indx = 0; indx < nBand; ++indx )
        {
            Bands[ indx ] = bandlist[ indx ];
            Bands[ indx + nBand ] = bandlist[ indx + nBand ];
            Func[ indx ] = funclist[ indx ];
            if ( Weight )
                Weight[ indx ] = weightlist[ indx ];
        }

        bool noerr = remez( N, nBand, Bands, Func, Weight );

        delete[] Bands;
        delete[] Func;
        if ( Weight )
            delete[] Weight;

        if ( !noerr )
            return false;
    }
    //--------------------------------  firls()
    else if ( tname == "firls" )
    {

        int              N;
        int              nBand;
        vector< double > bandlist;
        vector< double > funclist;
        vector< double > weightlist;

        switch ( size )
        {
        case 4: {
            if ( !getDoubleList( arglist[ 3 ], weightlist ) ||
                 !weightlist.size( ) )
            {
                return false;
            }
        }
            // no break!
        case 3: {
            if ( !getDoubleList( arglist[ 2 ], funclist ) || !funclist.size( ) )
            {
                return false;
            }
            if ( !getDoubleList( arglist[ 1 ], bandlist ) || !bandlist.size( ) )
            {
                return false;
            }
            N = atoi( arglist[ 0 ].c_str( ) );
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }
        if ( bandlist.size( ) != funclist.size( ) )
        {
            cerr << "Invalid length of band/func list" << endl;
            return false;
        }
        if ( weightlist.size( ) &&
             ( bandlist.size( ) != 2 * weightlist.size( ) ) )
        {
            cerr << "Invalid length of weight list" << endl;
            return false;
        }
        nBand = bandlist.size( ) / 2;
        double* Bands = new double[ 2 * nBand ];
        double* Func = new double[ 2 * nBand ];
        double* Weight = weightlist.size( ) ? new double[ nBand ] : 0;
        for ( int indx = 0; indx < nBand; ++indx )
        {
            Bands[ indx ] = bandlist[ indx ];
            Bands[ indx + nBand ] = bandlist[ indx + nBand ];
            Func[ indx ] = funclist[ indx ];
            Func[ indx + nBand ] = funclist[ indx + nBand ];
            if ( Weight )
                Weight[ indx ] = weightlist[ indx ];
        }

        bool noerr = firls( N, nBand, Bands, Func, Weight );

        delete[] Bands;
        delete[] Func;
        if ( Weight )
            delete[] Weight;

        if ( !noerr )
            return false;
    }
    //--------------------------------  firw
    else if ( tname == "firw" )
    {

        int         N;
        Filter_Type type;
        std::string window;
        double      Flow;
        double      Fhigh = 0.0;
        double      Ripple = 0.0;
        double      dF = 0.0;

        switch ( size )
        {
        case 7: {
            dF = atof( arglist[ 6 ].c_str( ) );
        }
            // no break!
        case 6: {
            Ripple = atof( arglist[ 5 ].c_str( ) );
        }
            // no break!
        case 5: {
            Fhigh = atof( arglist[ 4 ].c_str( ) );
        }
            // no break!
        case 4: {
            Flow = atof( arglist[ 3 ].c_str( ) );

            if ( !removequote( arglist[ 2 ], window ) )
            {
                return false;
            }

            string name;
            if ( !removequote( arglist[ 1 ], name ) )
            {
                return false;
            }
            if ( !getFilterType( name.c_str( ), type ) )
            {
                cerr << "Invalid filter type." << endl;
                return false;
            }

            N = atoi( arglist[ 0 ].c_str( ) );
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        if ( !firw( N, type, window.c_str( ), Flow, Fhigh, Ripple, dF ) )
        {
            return false;
        }
    }

    //--------------------------------  fircoef
    else if ( tname == "fircoef" )
    {
        string           mode;
        bool             ftype = false;
        vector< double > coefs;

        switch ( size )
        {
        case 2:
            if ( removequote( arglist[ 1 ], mode ) )
            {
                if ( !strcasecmp( mode.c_str( ), "causal" ) )
                {
                    ftype = false;
                }
                else if ( !strcasecmp( mode.c_str( ), "zero_phase" ) )
                {
                    ftype = true;
                }
                else
                {
                    cerr << "Invalid fir filter mode: " << mode << endl;
                    return false;
                }
            }
            // no break!

        case 1:
            if ( !getDoubleList( arglist[ 0 ], coefs ) || coefs.empty( ) )
            {
                cerr << "Missing coefficient list" << endl;
                return false;
            }
            break;

        default:
            cerr << "Too few/many arguments." << endl;
            return false;
        }

        if ( !fircoefs( coefs.size( ), &coefs.front( ), ftype ) )
        {
            return false;
        }
    }

    //--------------------------------  difference
    else if ( tname == "difference" )
    {

        if ( !size )
        {
            if ( !difference( ) )
            {
                return false;
            }
        }
        else
        {
            cerr << "difference doesn't take arguments." << endl;
            return false;
        }
    }

    //--------------------------------  limiter
    else if ( tname == "limiter" )
    {

        double l1;
        double l2 = 0.0;
        double l3 = 0.0;

        switch ( size )
        {
        case 4: {
            l3 = atof( arglist[ 3 ].c_str( ) );
        }
            // no break!
        case 3: {
            l2 = atof( arglist[ 2 ].c_str( ) );
        }
            // no break!
        case 2: {
            l1 = atof( arglist[ 1 ].c_str( ) );
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        if ( !limiter( arglist[ 0 ].c_str( ), l1, l2, l3 ) )
        {
            return false;
        }
    }
    //--------------------------------  decimateBy2
    else if ( tname == "decimateBy2" )
    {

        int N;
        int filtid = 1;

        switch ( size )
        {
        case 2: {
            filtid = atoi( arglist[ 1 ].c_str( ) );
        }
            // no break!
        case 1: {
            N = atoi( arglist[ 0 ].c_str( ) );
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        if ( !decimateBy2( N ) )
        {
            return false;
        }
    }
    //--------------------------------  multirate
    else if ( tname == "multirate" )
    {

        double m1, m2 = 1E-3;
        double atten = 80;

        switch ( size )
        {
        case 4: {
            atten = atof( arglist[ 3 ].c_str( ) );
        }
            // no break!
        case 3: {
            m2 = atof( arglist[ 2 ].c_str( ) );
        }
            // no break!
        case 2: {
            m1 = atof( arglist[ 2 ].c_str( ) );
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        if ( !multirate( arglist[ 0 ].c_str( ), m1, m2, atten ) )
        {
            return false;
        }
    }
    //--------------------------------  mixer
    else if ( tname == "mixer" )
    {

        double fmix;
        double phase = 0.0;

        switch ( size )
        {
        case 2: {
            phase = atof( arglist[ 1 ].c_str( ) );
        }
            // no break!
        case 1: {
            fmix = atof( arglist[ 0 ].c_str( ) );
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        if ( !mixer( fmix, phase ) )
        {
            return false;
        }
    }
    //--------------------------------  linefilter
    else if ( tname == "linefilter" )
    {

        double f;
        double T = 0.0;
        int    fid = 1;
        int    nT = 1;

        switch ( size )
        {
        case 4: {
            nT = atoi( arglist[ 3 ].c_str( ) );
        }
            // no break!
        case 3: {
            fid = atoi( arglist[ 2 ].c_str( ) );
        }
            // no break!
        case 2: {
            T = atof( arglist[ 1 ].c_str( ) );
        }
            // no break!
        case 1: {
            f = atof( arglist[ 0 ].c_str( ) );
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        if ( !linefilter( f, T, fid, nT ) )
        {
            return false;
        }
    }
    //--------------------------------  frontend
    else if ( tname == "frontend" )
    {

        int section = -1;

        switch ( size )
        {
        case 3: {
            section = atoi( arglist[ 2 ].c_str( ) );
        }
            // no break!
        case 2: {
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        if ( !frontend(
                 arglist[ 0 ].c_str( ), arglist[ 1 ].c_str( ), section ) )
        {
            return false;
        }
    }
    //--------------------------------  closeloop
    else if ( tname == "closeloop" )
    {

        double g;

        switch ( size )
        {
        case 1: {
            g = atof( arglist[ 0 ].c_str( ) );
            break;
        }
        default: {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        }

        if ( !closeloop( g ) )
        {
            return false;
        }
    }
    //--------------------------------  setgain
    else if ( tname == "setgain" )
    {
        if ( size != 2 )
        {
            cerr << "Too few/many arguments." << endl;
            return false;
        }
        double f = atof( arglist[ 0 ].c_str( ) );
        double g = atof( arglist[ 1 ].c_str( ) );
        if ( !setgain( f, g ) )
        {
            return false;
        }
    }
    else
    {
        cerr << "Unknown function name. " << name << endl;
        return false;
    }

    return true;
}
