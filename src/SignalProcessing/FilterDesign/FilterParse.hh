/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef _LIGO_FILTERPARSE_H
#define _LIGO_FILTERPARSE_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: FilterParse						*/
/*                                                         		*/
/* Module Description: parse a filter					*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 17Jul02  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: FilterParse.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include "Complex.hh"
#include "FilterBase.hh"
#include <string>
#include <vector>

/** Parse a filter.
    This class parses a filter. For a detailed description see 
    the FilterDesign class.

    @memo Filter parsing
    @author Written July 2002 by Masahiro Ito
    @version 1.0
 ************************************************************************/
class FilterParse
{
public:
    ///  String container data type
    typedef std::vector< std::string > StrList;

    /** Constructs a filter parser. 
        * @memo Constructor.
	* @param spec Filter design specification
	******************************************************************/
    explicit FilterParse( const char* spec = 0 );

    /** Destroy the filter design parser class.
          @memo Destructor.
      ******************************************************************/
    virtual ~FilterParse( )
    {
    }

    /** Parse a filter and call the corresponding methods.
          @memo Parse a filter
          @param formula New filter specification
          @return true if successful
       ******************************************************************/
    virtual bool filter( const char* formula );

    /**  Set a parsed gain.
        *  @memo Parse gain
	*  @param g Gain value
	*  @param format Fomat or units od specified gain (e.g. scalar or dB)
	*  @return True if gain was set.
	*/
    virtual bool
    gain( double g, const char* format = "scalar" )
    {
        return false;
    }

    /**  Add a single parsed real pole to the filter being constructed.
        *  @memo Add a pole.
	*  @param f     Pole frequency.
	*  @param gain  Pole gain.
	*  @param plane Plan in which pole is defined.
	*  @return True if added successfully.
	*/
    virtual bool
    pole( double f, double gain, const char* plane = "s" )
    {
        return false;
    }

    /**  Add a single parsed real pole to the filter being constructed.
        *  @memo Add a pole.
	*  @param f     Pole frequency.
	*  @param plane Plan in which pole is defined.
	*  @return True if added successfully.
	*/
    virtual bool
    pole( double f, const char* plane = "s" )
    {
        return pole( f, 1.0, plane );
    }

    /**  Add a single parsed real zero to the filter being constructed.
        *  @memo Add a zero.
	*  @param f     Zero frequency.
	*  @param gain  Zero gain.
	*  @param plane Zero in which pole is defined.
	*  @return True if added successfully.
	*/
    virtual bool
    zero( double f, double gain, const char* plane = "s" )
    {
        return false;
    }

    /**  Add a single parsed real zero to the filter being constructed.
        *  @memo Add a zero.
	*  @param f     Zero frequency.
	*  @param plane Zero in which pole is defined.
	*  @return True if added successfully.
	*/
    virtual bool
    zero( double f, const char* plane = "s" )
    {
        return zero( f, 1.0, plane );
    }

    /** Add a complex conjugate pair of poles to the filter being 
        *  constructed.
        *  @memo Add a complex conjugate pair of poles.
	*  @param f     Pole frequency.
	*  @param Q     Pole Q value.
	*  @param gain  Pole gain.
	*  @param plane Plane in which poles are defined.
	*  @return True if added successfully.
	*/
    virtual bool
    pole2( double f, double Q, double gain, const char* plane = "s" )
    {
        return false;
    }

    /**  Add a complex conjugate pair of poles to the filter being 
        *  constructed.
        *  @memo Add a complex conjugate pair of poles.
	*  @param f     Pole frequency.
	*  @param Q     Pole Q value.
	*  @param plane Plane in which poles are defined.
	*  @return True if added successfully.
	*/
    virtual bool
    pole2( double f, double Q, const char* plane = "s" )
    {
        return pole2( f, Q, 1.0, plane );
    }

    /**  Add a complex conjugate pair of zeroes to the filter being 
        *  constructed.
        *  @memo Add a complex conjugate pair of zeroes.
	*  @param f     Zeri frequency.
	*  @param Q     Zero Q value.
	*  @param gain  Zero gain.
	*  @param plane Plane in which poles are defined.
	*  @return True if added successfully.
	*/
    virtual bool
    zero2( double f, double Q, double gain, const char* plane = "s" )
    {
        return false;
    }

    /**  Add a complex conjugate pair of zeroes to the filter being 
        *  constructed.
        *  @memo Add a complex conjugate pair of zeroes.
	*  @param f     Zero frequency.
	*  @param Q     Zero Q value.
	*  @param plane Plane in which poles are defined.
	*  @return True if added successfully.
	*/
    virtual bool
    zero2( double f, double Q, const char* plane = "s" )
    {
        return zero2( f, Q, 1.0, plane );
    }

    /**  Add a filter defined by lists of zeroes and poles and a constant
        *  gain in the specified plane.
	*  @memo Define filter by zeros and poles.
	*  @param nzeros Number of zeroes defined.
	*  @param zero   List of zeros.
	*  @param npoles Number of poles defined.
	*  @param pole   List of complex poles.
	*  @param gain   Gain factor.
	*  @param plane  Plane in which poles and zeroes are defined.
	*  @return True if added successfully.
	*/
    virtual bool
    zpk( int             nzeros,
         const dComplex* zero,
         int             npoles,
         const dComplex* pole,
         double          gain,
         const char*     plane = "s" )
    {
        return false;
    }

    /**  Add an IIR filter defined by a rational polynomial to the design.
        *  @memo Add a rational polynomial.
	*  @param nnumer Number of numerator coefficients.
	*  @param numer  List of numerator coefficients.
	*  @param ndenom Number of denominator coefficients.
	*  @param denom  List of denominator coefficients.
	*  @param gain   Filter gain.
	*  @return True on successful addition.
	*/
    virtual bool
    rpoly( int           nnumer,
           const double* numer,
           int           ndenom,
           const double* denom,
           double        gain )
    {
        return false;
    }

    /**  Make an IIR filter from the biquad coefficients and add it to the 
        *  current one.
        *  @memo Add a second order section
        *  @see IIRdesign.hh
        *  @param b0 b0
        *  @param b1 b1
        *  @param b2 b2
        *  @param a1 a1
        *  @param a2 a2
        *  @return true if successful
	*/
    virtual bool
    biquad( double b0, double b1, double b2, double a1, double a2 )
    {
        return false;
    }

    /**  Make an IIR filter from a list of second order sections. If the
        *  format is 's' (standard), the coefficents are ordered like:
        *  \f$ gain, b1_1, b2_1, a1_1, a2_1, b1_2, b2_2, a1_2, a2_2,... \f$
        *  whereas for the the format 'o' (online) the order is
        *  \f$ gain, a1_1, a2_1, b1_1, b2_1, a1_2, a2_2, b1_2, b2_2,... \f$
        *  The number of coefficients must be 4 times the number of second 
        *  order sections plus one.
        *  @memo Add a list of second order sections
        *  @see IIRdesign.hh
        *  @param nba Number of coefficients
        *  @param ba List of coefficients
        *  @param format Coefficient format
        *  @return true if successful
	*/
    virtual bool
    sos( int nba, const double* ba, const char* format = "s" )
    {
        return false;
    }

    /**  Make an IIR filter from the list of poles and zeros in the 
        *  z-plane. To be stable the z-plane poles must lie within the 
        *  unity cirlce.
        *  @memo Add a filter from z-plane roots
        *  @see IIRdesign.hh
        *  @param nzeros Number of zeros
        *  @param zero Array of zeros
        *  @param npoles Number of poles
        *  @param pole Array of poles
        *  @param gain Gain
        *  @return true if successful
	*/
    virtual bool
    zroots( int             nzeros,
            const dComplex* zero,
            int             npoles,
            const dComplex* pole,
            double          gain = 1.0 )
    {
        return false;
    }

    /**  Make a filter from the direct form. 
        *  The direct form can be written as:
        *  \f[
	   H(z)=\frac{\sum_{k=0}^{nb}b_k z^{-k}}
	             {1-\sum_{k=1}^{na}a_k z^{-k}}
           \f]
	*
        *  Cascaded second order sections are formed by finding the roots
        *  of the direct form. The specified coefficients are 
        *  \f$b_0, b_1,..., b_{nb}\f$ for the numerator and 
        *  \f$a_1, a_2,..., a_{na}\f$ for the denominator. The argument 
        *  \a nb specifies the number of \a b coeffcients supplied to the 
        *  function minus 1, whereas \a na is exactly the number of \a a 
        *  coeffcients since \f$a_0\f$ is always 1 and omitted from the list.
	*
        *  Avoid the direct form since even fairly simple filters will 
        *  run into precision problems.
	* 
        *  @memo Add a filter from the direct form
        *  @param nb Number of coefficients in numerator
        *   @param b Array of numerator coefficents
        *  @param na Number of coeffcients in denominator
        *  @param a Array of denominator coefficients exclusive of a0
        *  @return true if successful
	*/
    virtual bool
    direct( int nb, const double* b, int na, const double* a )
    {
        return false;
    }

    /** Add an elliptic filter to the design.
	* @memo Add an elliptic filter.
	* @param type Filter type 0: Low Pass 2: High Pass 3: Band Pass 
	*             4: Band Stop
	* @param order Filter order.
	* @param rp Pass band ripple.
	* @param as Stop band attenuation.
	* @param f1 Pass band lower edge (Hz).
	* @param f2 Pass band upper edge (Hz).
	* @return True if successful.
	*/
    virtual bool
    ellip( Filter_Type type,
           int         order,
           double      rp,
           double      as,
           double      f1,
           double      f2 = 0.0 )
    {
        return false;
    }

    /** Add a type 1 Chebyshev filter to the design.
	* @memo Make an chebyshev filter of type 1.
	* @param type Filter type 0: Low Pass 2: High Pass 3: Band Pass 
	*             4: Band Stop
	* @param order Filter order.
	* @param rp Pass band ripple.
	* @param f1 Lower pass band edge frequency (Hz).
	* @param f2 Upper pass band edge frequency (Hz).
	* @return True if successful.
	*/
    virtual bool
    cheby1( Filter_Type type, int order, double rp, double f1, double f2 = 0.0 )
    {
        return false;
    }

    /** Add a type 2 Chebyshev filter to the design.
	* @memo Add a type 2 Chebyshev filter.
	* @param type Filter type 0: Low Pass 2: High Pass 3: Band Pass 
	*             4: Band Stop
	* @param order Filter order.
	* @param as Stop band attenuation.
	* @param f1 Pass band edge (Hz).
	* @param f2 Another pass band edge (Hz).
	* @return True if successful.
	*/
    virtual bool
    cheby2( Filter_Type type, int order, double as, double f1, double f2 = 0.0 )
    {
        return false;
    }

    /** Add a Butterworth filter to the design.
	* @memo Add a Butterworth filter.
	* @param type Filter type 0: Low Pass 2: High Pass 3: Band Pass 
	*             4: Band Stop
	* @param order Filter order.
	* @param f1 Pass band edge (Hz).
	* @param f2 Another pass band edge (Hz).
	* @return True if successful.
	*/
    virtual bool
    butter( Filter_Type type, int order, double f1, double f2 = 0.0 )
    {
        return false;
    }

    /** Add a notch filter to the design.
	* @memo Add a notch filter.
	* @param f0 Notch center frquency (Hz).
	* @param Q  Notch Q value.
	* @param depth Depth of notch.
	* @return True if successful.
	*/
    virtual bool
    notch( double f0, double Q, double depth = 0.0 )
    {
        return false;
    }

    /** Add a resonant gain filter to the design.
	* @memo Add a resonant gain filter.
	* @param f0 Filter center frequency (Hz).
	* @param Q  Filter Q value.
	* @param height Filter gain.
	* @return True if successful.
	*/
    virtual bool
    resgain( double f0, double Q, double height = 30.0 )
    {
        return false;
    }

    /**  Make an comb filter and add it to the current design.
        *  @memo Add a comb filter
        *  @see IIRdesign.hh
        *  @param f0 Fundamental frequency.
        *  @param Q Quality factor ( Q = \<Center-freq\>/Width ).
        *  @param amp Depth/height of notches/peaks (dB).
        *  @param N Number of harmonics.
        *  @return true if successful
	*/
    virtual bool
    comb( double f0, double Q, double amp = 0.0, int N = 0 )
    {
        return false;
    }

    /**  Add a filter designed using the McClellan-Parks algorithm.
        *  @memo Add a FIR filter.
        *  @param N      Number of filter coefficients.
        *  @param nBand  Number of bands.
        *  @param Bands  Band limit frequencies in Hz.
	*  @param Func   Desired response in each band.
        *  @param Weight Weighting factor for each band. If not specified, 
        *                Weight is assumed to be 1.0 for each band.
	*  @return True if filter design is successful.
	*/
    virtual bool
    remez( int           N,
           int           nBand,
           const double* Bands,
           const double* Func,
           const double* Weight = 0 )
    {
        return false;
    }

    /** Design a linear phase FIR filter using the least squares method. 
	  The inputs are substantially the same as the remez function.
          The designed Filter coefficients replace the current 
          coefficients. The Filter history is not affected.
          @memo  Filter design by least squares fit..
          @see FIRdesign.hh
          @param N      Number of filter coefficients.
          @param nBand  Number of bands.
          @param bands  Band start and end frequencies (2*nBand entries)
          @param pass   Desired filter response at each frequency in \a bands.
          @param weight Weight of each band.
          @return true if successful
       ******************************************************************/
    virtual bool
    firls( int           N,
           int           nBand,
           const double* bands,
           const double* pass,
           const double* weight = 0 )
    {
        return false;
    }

    /**  Add an FIR  filter using a window-based design.
        *  @memo Add a window-based FIR filter.
	*  @param N      Number of coefficients in the filter.
	*  @param type   Filter type to be designed.
	*  @param window Window name design is to be based on.
	*  @param Flow   Low filter edge frequency (in Hz).
	*  @param Fhigh  High filter edge frequency (in Hz).
	*  @param Ripple Desired ripple in dB (Chebyshev) or stop-band 
	*                attenuation (Kaiser).
	*  @param dF     Normalized transition width for Chebyshev windows
	*  @return True if filter design is successful.
	*/
    virtual bool
    firw( int         N,
          Filter_Type type,
          const char* window,
          double      Flow,
          double      Fhigh = 0,
          double      Ripple = 0,
          double      dF = 0 )
    {
        return false;
    }

    /**  Design an FIR filter using listed coefficients.
        *  @memo Add an FIR filter.
	*  @param N      Number of coefficients in the filter.
	*  @param coef   Pointer to coefficient list.
	*  @param mode   Timing mode false = "causal", true = "zero_phase"
	*  @return True if filter design is successful.
	*/
    virtual bool
    fircoefs( int N, const double* coef, bool mode )
    {
        return false;
    }

    /**  Add a differencing filter. Take the difference between successive 
        *  samples.
        *  @memo Add difference filter.
	*  @return True if filter added successfully.
	*/
    virtual bool
    difference( void )
    {
        return false;
    }

    /**  Decimation filter decimates by powers of 2. This uses the
        *  DecimateBy2 filter class.
        *  @memo Decimate by a power of 2.
	*  @param N Factor by which to decimate (must be a power of two).
	*  @param FilterID ID of the antialiasing filter \see DecimateBy2.
	*  @return True if the filter is successfully added.
	*/
    virtual bool
    decimateBy2( int N, int FilterID = 1 )
    {
        return false;
    }

    /**  Add a filter to remove a specified line.
        *  @memo Filter a line.
	*  @param f   Line frequency (Hz).
	*  @param T   Time interval to remove line
	*  @param fid Filter type (0, 1) [1]
	*  @param nT  Number of subdivisions of the time interval.
	*  @return True if the filter is successfully added.
	*/
    virtual bool
    linefilter( double f, double T = 0.0, int fid = 1, int nT = 1 )
    {
        return false;
    }

    /**  Make a limiter filter and add it to the current design pipe.
        *  The type can be "" for no limits, "val" for high/low limits on
        *  the sampled values, "sym" for a symmetric limits on the values, 
        *  "slew" for a slew rate limit and "val/slew" or "sym/slew" for 
        *  both. If the limiter type is "val", l1 is the lower bound and 
        *  l2 is the upper bound, for "sym" l1 is the upper bound and -l1
        *  is the lower bound, for "slew" l1 is the slew rate limit in 
        *  counts/s, for "val/slew" l1, l2 and l3 are the lower bound, 
        *  the upper bound and the slew rate limit, respectively, and for 
        *  "sym/slew" l1 and l2 are the symmetric limit and the slew rate 
        *  limit, respectively.
        *  @memo Add a limiter
        *  @see Limiter.hh
        *  @param type Limiter type
        *  @param l1 First limit
        *  @param l2 Second limit
        *  @param l3 Third limit
        *  @return true if successful
	*/
    virtual bool
    limiter( const char* type, double l1, double l2 = 0, double l3 = 0 )
    {
        return false;
    }

    /**  Make a resampling filter and add it to the current design.
        *  The filter type is either "abs" or "rel" depending if the 
        *  required resampling rate is specified realtive or absolute.
        *  For absolute rate specifications the first parameter is the 
        *  desired sampling rate and the second parameter is the maximally 
        *  allowed error. For a relative rate specifiaction the first 
        *  parameter is the interpolation factor and the second parameter
        *  is the decimation factor (both are integer parameters). The 
        *  third parameter is in both cases the required stop band 
        *  attenuation.
        *  @memo Add a resampling filter
        *  @see MultiRate.hh
        *  @param type Multirate type
        *  @param m1 First parameter (desired fS or interpolation factor)
        *  @param m2 Second parameter (error or decimation factor)
        *  @param atten Stopband attenuation
        *  @return true if successful
	******************************************************************/
    virtual bool
    multirate( const char* type,
               double      m1,
               double      m2 = 1E-3,
               double      atten = 80 )
    {
        return false;
    }

    /**  Make a mixer and add it to the current filter. This will 
        *  transform a real time series into a complex one. The mixer
        *  will heterodyne (multiply) a time series by 
        *  \f$ \exp{2 \pi i f_c t + phase} \f$.
        *  @memo Add a mixer
        *  @see Mixer.hh
        *  @param fmix Mixer frequency
        *  @param phase Initial phase
        *  @return true if successful
	*/
    virtual bool
    mixer( double fmix, double phase = 0 )
    {
        return false;
    }

    /**  Read filter coefficients from front-end file, make filter 
        *  and add it to the current one.
        *  @memo Add a filter from a configuaryion file
        *  @param file Front-end configuration file
        *  @param module Module name or number
        *  @param section Filter section or -1 for all
        *  @return true if successful
	*/
    virtual bool
    frontend( const char* file, const char* module, int section = -1 )
    {
        return false;
    }

    /**  Set the filter gain at specified frequency.
        *  @memo Set filter gain
        *  @param f Frequency 
        *  @param gain Set point
        *  @return true if successful
	*/
    virtual bool
    setgain( double f, double gain )
    {
        return false;
    }

    /** Form the closed loop response of the current filter.
          @memo Closed loop response
          @param k Additional gain: 1/(1+k*G(f)) 
          @return true if successful
      ******************************************************************/
    virtual bool
    closeloop( double k = 1.0 )
    {
        return false;
    }

protected:
    /// Split a line
    static void
    splitLine( const std::string& str_in, StrList& strlist, char delim = ',' );
    /// Remove quotes
    static bool removequote( const std::string& str_in, std::string& str_out );
    /// String to complex
    static bool str2cmplx( const std::string& str, dComplex& value );

    /// get complex list
    static bool getComplexList( const std::string&       value,
                                std::vector< dComplex >& cmplxlist );
    /// get real list
    static bool getDoubleList( const std::string&     value,
                               std::vector< double >& dbllist );
    /// get filter type
    static bool getFilterType( const char* type_name, Filter_Type& type );
    /// get filter string
    static std::string getFilterString( Filter_Type type );

    /// add a filter ( name : filter name, arg : corresponding arguments ).
    virtual bool addfilter( const std::string& name, const std::string& arg );
};

#endif // _LIGO_FILTERPARSE_H
