/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "pipe_tools.hh"
#include "MultiPipe.hh"
#include "fir_filter.hh"
#include "FIRFilter.hh"
#include "FIRdft.hh"
#include "resampler.hh"

using namespace std;

//======================================  Set the filter mode for FIR filters
void
set_fir_mode( Pipe* p, int mode )
{
    if ( !p )
        return;
    if ( dynamic_cast< MultiPipe* >( p ) )
    {
        MultiPipe::PipeConfig& vec( dynamic_cast< MultiPipe* >( p )->pipe( ) );
        for ( auto i = vec.begin( ); i != vec.end( ); i++ )
        {
            set_fir_mode( i->get( ), mode );
        }
    }
    else if ( dynamic_cast< FIRdft* >( p ) )
    {
        dynamic_cast< FIRdft* >( p )->setMode( FIRdft::fir_mode( mode ) );
    }
    else if ( dynamic_cast< resampler* >( p ) )
    {
        dynamic_cast< resampler* >( p )->setMode( mode );
    }
    else if ( dynamic_cast< FIRFilter* >( p ) )
    {
        dynamic_cast< FIRFilter* >( p )->setMode( FIRFilter::fir_mode( mode ) );
    }
    else if ( dynamic_cast< fir_filter* >( p ) )
    {
        dynamic_cast< fir_filter* >( p )->setMode(
            fir_filter::fir_mode( mode ) );
    }
}
