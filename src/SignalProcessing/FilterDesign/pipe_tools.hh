/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef PIPE_TOOLS_HH
#define PIPE_TOOLS_HH

class Pipe;

/** Set the fir (delay) mode for the specified pipe for any fir-based 
 *  filter(s) in the pipe. This function iterates over all filters in
 *  a multipipe and selects filters of types FIRFilter, FIRdft, fir_filter
 *  and resampler and modifies the pipes as appropriate.
 *  \brief Set the delay mode.
 *  \param filter Pointer to pipe to be modified.
 */
void set_fir_mode( Pipe* p, int mode );

#endif // !defined(PIPE_TOOLS_HH)
