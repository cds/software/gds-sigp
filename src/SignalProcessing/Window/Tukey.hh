/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef TUKEY_HH
#define TUKEY_HH

#include "window_api.hh"

/**  Tukey is the Tukey window class. The argument, \e f, specifies the 
  *  fraction of the window function that is one. On either side of the 
  *  central plateau, the window function drops to zero as cos(|x-f|/(1-f)).
  *  for \c f=0, this window is equivalent to a Hann window, and for \c f=1
  *  the window is equal to a rectangular window. 
  *  \brief Tukey Window
  *  \author john.zweizig@ligo.org
  *  \version $Id$
  */
class Tukey : public window_api
{
public:
    /**  Construct a Tukey window. If the number of points is zero, no window
      *  function is calculated.
      *  \brief Data constructor.
      *  \param frac Fraction of window in the central plateau
      *  \param N    Length of the window to be calculated.
      */
    Tukey( double frac, int N = 0 );

    /**  Delete the window and release any allocated storage.
      *  @memo Destructor.
      */
    virtual ~Tukey( void ) = default;

    /**  Create an identical Tukey window.
      *  @memo clone the window.
      *  @return Pointer to  a new Tukey window.
      */
    Tukey* clone( void ) const;

    /**  Return the window fraction parameter used in constructing the window.
      *  \brief Window fraction parameter.
      *  \return value of fraction parameter.
      */
    double parameter( void ) const;

    /**  Return the window function at x, where x = 2*pi*i/(N-1). The Tukey
      *  window is 0.5*(1-cos(x)).
      *  \brief Window function.
      */
    double WinFc( double arg );

private:
    double mFrac;
};
#endif // TUKEY_HH
