#include "Bartlett.hh"
#include <cmath>
#include "constant.hh"

//--------------------------------------  Constructor.
Bartlett::Bartlett( int N )
{
    setWindow( N );
}

//--------------------------------------  Clone operator.
Bartlett*
Bartlett::clone( void ) const
{
    return new Bartlett( *this );
}

//--------------------------------------  Window function.
double
Bartlett::WinFc( double x )
{
    return 1.0 - fabs( x / pi - 1.0 );
}
