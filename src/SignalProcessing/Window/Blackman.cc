#include "Blackman.hh"
#include <cmath>

//--------------------------------------  Constructor.
Blackman::Blackman( int N, double Beta ) : mBeta( Beta )
{
    setWindow( N );
}

//--------------------------------------  Copy constructor.
Blackman::Blackman( const Blackman& x ) : mBeta( x.mBeta )
{
    if ( x.getLength( ) )
        setWindow( x.getLength( ) );
}

//--------------------------------------  Copy constructor.
Blackman*
Blackman::clone( void ) const
{
    return new Blackman( *this );
}

//--------------------------------------  Window function.
double
Blackman::WinFc( double x )
{
    return ( 0.5 - mBeta ) - 0.5 * cos( x ) + mBeta * cos( 2 * x );
}
