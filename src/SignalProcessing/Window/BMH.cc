#include "BMH.hh"
#include <cmath>

//--------------------------------------  Constructor.
BMH::BMH( int N )
{
    setWindow( N );
}

//--------------------------------------  Clone operator.
BMH*
BMH::clone( void ) const
{
    return new BMH( *this );
}

//--------------------------------------  Window function.
double
BMH::WinFc( double x )
{
    return 1.0 - 1.36109 * cos( x ) + 0.39381 * cos( 2 * x ) -
        0.032557 * cos( 3 * x );
}
