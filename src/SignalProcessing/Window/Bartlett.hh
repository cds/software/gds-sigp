#ifndef BARTLETT_HH
#define BARTLETT_HH

#include "window_api.hh"

/**  Bartlett window class.
  *  Bartlett is the Bartlett window class.
  */
class Bartlett : public window_api
{
public:
    /**  Construct a Bartlett window.
    *  @memo Default constructor.
    */
    Bartlett( void )
    {
    }

    /**  Construct a Bartlett window object. Initialize it for sequences of 
    *  length N.
    *  @memo Data Constructor.
    *  @param N Length of time series to be windowed.
    */
    explicit Bartlett( int N );

    /**  Delete the window and release any allocated storage.
    *  @memo Destructor.
    */
    ~Bartlett( void )
    {
    }

    /**  Create an identical Bartlett window.
    *  @memo clone the window.
    *  @return Pointer to a new Bartlett window.
    */
    Bartlett* clone( void ) const;

    /**  Window function.
    *  Return the window function at x, where x = 2*pi*i/(N-1). The Bartlett
    *  window is 1 - |x/pi-1|.
    */
    double WinFc( double arg );

private:
};
#endif // BARTLETT_HH
