#ifndef UNIFORM_HH
#define UNIFORM_HH

#include "window_api.hh"

/**  Uniform window class.
  *  Uniform is the Uniform window class.
  */
class Uniform : public window_api
{
public:
    /**  Construct a Uniform window.
    *  @memo Default constructor.
    */
    Uniform( void )
    {
    }

    /**  Construct a Uniform window object. Initialize it for sequences of 
    *  length N.
    *  @memo Data Constructor.
    *  @param N Length of time series to be windowed.
    */
    explicit Uniform( int N );

    /**  Delete the window and release any allocated storage.
    *  @memo Destructor.
    */
    ~Uniform( void )
    {
    }

    /**  Create an identical Uniform window.
    *  @memo clone the window.
    *  @return Pointer to a new Uniform window.
    */
    Uniform* clone( void ) const;

    /**  Window function.
    *  Return the window function at x, where x = 2*pi*i/(N-1). The Uniform
    *  window is 1.
    */
    double WinFc( double arg );

private:
};
#endif // UNIFORM_HH
