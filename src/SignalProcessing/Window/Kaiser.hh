/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef KAISER_HH
#define KAISER_HH

#include "window_api.hh"

/**  %Kaiser is the %Kaiser window class. The argument, \e alpha, specifies 
  *  Kaiser alpha parameter.
  *  \brief %Kaiser Window
  *  \author john.zweizig@ligo.org
  *  \version $Id$
  */
class Kaiser : public window_api
{
public:
    /**  Construct a  %Kaiser window. If the number of points is zero, the 
      *  window function calculation is deferred until a non-zero length is 
      *  requested (\e e.g. by the setWindow() function).
      *  \brief Data  constructor.
      *  \param alpha Kaiser window parameter
      *  \param N     Length of the window to be calculated.
      */
    Kaiser( double alpha, int N = 0 );

    /**  Delete the window and release any allocated storage.
      *  @memo Destructor.
      */
    virtual ~Kaiser( void );

    /**  Create an identical Kaiser window.
      *  @memo clone the window.
      *  @return Pointer to  a new Kaiser window.
      */
    Kaiser* clone( void ) const;

    /**  Return the window alpha parameter used in constructing the window.
      *  \brief Window alpha parameter.
      *  \return value of alpha parameter.
      */
    double parameter( void ) const;

    /**  Return the window function at x, where x = 2*i/(N-1). The Kaiser
      *  window is \f$ i0(\pi \alpha) \sqrt{1 - (x - 1)^2})/i0(\pi \alpha) \f$
      *  \brief Window function.
      *  \param arg 
      */
    double WinFc( double arg );

    /**  Bessel function.
     */
    static double bessel0( double );

private:
    double mAlpha;
    double mI0AlphaPi;
};
#endif // KAISER_HH
