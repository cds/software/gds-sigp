/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "Bartlett.hh"
#include "Blackman.hh"
#include "FlatTop.hh"
#include "Hamming.hh"
#include "Hanning.hh"
#include "Kaiser.hh"
#include "Nutall.hh"
#include "Tukey.hh"
#include "Uniform.hh"
#include "Welch.hh"
#include <string>
#include <cctype>
#include <memory>
#include <stdexcept>

static void
lc_str( std::string& s )
{
    size_t N = s.size( );
    for ( size_t i = 0; i < N; ++i )
        s[ i ] = tolower( s[ i ] );
}

using namespace std;

//======================================  Construct a named zero-parameter window
window_api*
window_factory( const std::string& name, size_t N )
{
    //-----------------------------------  Force lower case
    string tname = name;
    lc_str( tname );
    if ( tname == "bartlett" )
    {
        return new Bartlett( N );
    }
    else if ( tname == "blackman" )
    {
        return new Blackman( N );
    }
    else if ( tname == "flattop" )
    {
        return new FlatTop( N );
    }
    else if ( tname == "hamming" )
    {
        return new Hamming( N );
    }
    else if ( tname == "hanning" || tname == "hann" )
    {
        return new Hanning( N );
    }
    else if ( tname == "nutall" )
    {
        return new Nutall( N );
    }
    else if ( tname == "uniform" || tname == "rectangle" || tname == "square" )
    {
        return new Uniform( N );
    }
    else if ( tname == "welch" )
    {
        return new Welch( N );
    }
    else
    {
        string msg = "window_factory: Unrecognized window type requested (";
        msg += tname + ").";
        throw runtime_error( msg );
    }
}

//======================================  Construct a named one-parameter window
window_api*
window_factory_1( const std::string& name, size_t N, double par )
{
    //-----------------------------------  Force lower case
    string tname = name;
    lc_str( tname );
    if ( tname == "blackman" )
    {
        return new Blackman( N, par );
    }
    else if ( tname == "kaiser" )
    {
        return new Kaiser( par, N );
    }
    else if ( tname == "tukey" )
    {
        return new Tukey( par, N );
    }
    else
    {
        string msg = "window_factory_1: Unrecognized window type requested (";
        msg += tname + ").";
        throw runtime_error( msg );
    }
}

//======================================  Template window-type test.
template < class P >
bool
pipe_isa( const Pipe* x )
{
    return dynamic_cast< const P* >( x ) != nullptr;
}

//======================================  Type name of window_api instance.
std::string
window_type( const Pipe* p )
{
    if ( !p )
        return "null";
    if ( !pipe_isa< window_api >( p ) )
        return "none";
    if ( pipe_isa< Bartlett >( p ) )
        return "bartlett";
    if ( pipe_isa< Blackman >( p ) )
        return "blackman";
    if ( pipe_isa< FlatTop >( p ) )
        return "flattop";
    if ( pipe_isa< Hamming >( p ) )
        return "hamming";
    if ( pipe_isa< Hanning >( p ) )
        return "hanning";
    if ( pipe_isa< Kaiser >( p ) )
        return "kaiser";
    if ( pipe_isa< Nutall >( p ) )
        return "nutall";
    if ( pipe_isa< Tukey >( p ) )
        return "tukey";
    if ( pipe_isa< Uniform >( p ) )
        return "square";
    if ( pipe_isa< Welch >( p ) )
        return "welch";
    return "other";
}
