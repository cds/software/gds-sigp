/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef NUTALL_HH
#define NUTALL_HH

#include "window_api.hh"

/**  The Nutall class implements a generalized Nutall window. The 
  *  window function is: 
  *   Wn[j] = .355768 - .487396 cos(Xj) + .144232 cos(2 Xj) - .012604 cos(3 Xj)
  *  where Xj=2*pi*j/(N-1).
  *  \brief Generalized Nutall window class.
  */
class Nutall : public window_api
{
public:
    /**  Construct an uninitialized (unspecified length) Nutall window 
    *  instance
    *  \brief Default constructor.   
    */
    Nutall( void )
    {
    }

    /**  Construct a Nutall window of length \a len.
    *  \brief Full constructor.
    *  \param len Number of samples in the TSeries to be windowed.
    */
    explicit Nutall( int len );

#if __cplusplus > 201100
    /**  Construct a generalized Nutall window of length 'len'. If beta
    *  isn't specified it defalts to the true hamming window.
    *  \brief Copy constructor.
    *  \param x Nutall window to be copied.
    */
    Nutall( const Nutall& x ) = default;
#endif

    /**  Delete the object.
    *  \brief Destructor.
    */
    ~Nutall( )
    {
    }

    /**  Create an identical copy of the window instance and return a pointer.
    *  \brief Clone the window.
    *  \return Pointer to the cloned window.
    */
    Nutall* clone( void ) const;

    /**  Return the function value at Xj=2*pi*j/(N-1), where j is the sample
    *  number and N is the series length.
    *  \brief Window function.
    */
    double WinFc( double x );

private:
};
#endif // NUTALL_HH
