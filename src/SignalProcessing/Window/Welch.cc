#include "Welch.hh"
#include <cmath>
#include "constant.hh"

//--------------------------------------  Constructor.
Welch::Welch( int N )
{
    setWindow( N );
}

//--------------------------------------  Clone operator.
Welch*
Welch::clone( void ) const
{
    return new Welch( *this );
}

//--------------------------------------  Window function.
double
Welch::WinFc( double x )
{
    return 1.0 - pow( x / pi - 1.0, 2 );
}
