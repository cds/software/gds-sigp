#ifndef FLATTOP_HH
#define FLATTOP_HH

#include "window_api.hh"

/**  FlatTop is the FlatTop window class. The float top window is most useful
  *  for calculating the spectral amplitude of a peak with little spectral 
  *  energy in nearby bins and provides the best amplitude accuracy in this 
  *  case.
  *  @memo FlatTop window class.
  *  @author J. Zweizig
  *  @version 1.0; Modified April 5, 2000
  */
class FlatTop : public window_api
{
public:
    /**  Construct a flat-top window.
    *  @memo Default constructor.
    */
    FlatTop( void )
    {
    }

    /**  Construct a FlatTop window object. Initialize it for sequences of 
    *  length N.
    *  @memo Data Constructor.
    */
    FlatTop( int N );

    /**  Construct a FlatTop window object identical to an existing object.
    *  @memo Copy Constructor.
    */
    FlatTop( const FlatTop& x );

    /**  Delete the window and release any allocated storage.
    *  @memo Destructor.
    */
    ~FlatTop( )
    {
    }

    /**  Construct a FlatTop window object identical to an existing object.
    *  @memo Copy Constructor.
    */
    FlatTop* clone( void ) const;

    /**  Return the window function at x, where x = 2*pi*i/(N-1). The FlatTop
    *  window is 0.2810638602 - 0.5208971735*cos(x) + 0.1980389663*cos(2x).
    *  @memo Window function.
    */
    double WinFc( double arg );

private:
};

#endif // FLATTOP_HH
