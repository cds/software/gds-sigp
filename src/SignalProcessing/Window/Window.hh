/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef WINDOW_HH
#define WINDOW_HH

#include "window_api.hh"

#ifndef __CINT__
#warning "Window class is deprecated. It has been replaced by window_api"
typedef window_api Window;
#endif // !defined(__CLING__)

#endif // WINDOW_HH
