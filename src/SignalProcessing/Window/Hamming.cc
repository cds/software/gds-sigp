#include "Hamming.hh"
#include <cmath>

//--------------------------------------  Constructor.
Hamming::Hamming( int N, double Beta ) : mBeta( Beta )
{
    setWindow( N );
}

//--------------------------------------  Copy constructor.
Hamming::Hamming( const Hamming& x ) : mBeta( x.mBeta )
{
    if ( x.getLength( ) )
        setWindow( x.getLength( ) );
}

//--------------------------------------  Constructor.
Hamming*
Hamming::clone( void ) const
{
    return new Hamming( *this );
}

//--------------------------------------  Window function.
double
Hamming::WinFc( double x )
{
    return mBeta - ( 1.0 - mBeta ) * cos( x );
}
