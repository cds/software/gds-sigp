#ifndef WELCH_HH
#define WELCH_HH

#include "window_api.hh"

/**  Welch window class.
  *  Welch is the Welch window class.
  */
class Welch : public window_api
{
public:
    /**  Construct a Welch window.
    *  @memo Default constructor.
    */
    Welch( void )
    {
    }

    /**  Construct a Welch window object. Initialize it for sequences of 
    *  length N.
    *  @memo Data Constructor.
    *  @param N Length of time series to be windowed.
    */
    explicit Welch( int N );

    /**  Delete the window and release any allocated storage.
    *  @memo Destructor.
    */
    ~Welch( void )
    {
    }

    /**  Create an identical Welch window.
    *  @memo clone the window.
    *  @return Pointer to a new Welch window.
    */
    Welch* clone( void ) const;

    /**  Window function.
    *  Return the window function at x, where x = 2*pi*i/(N-1). The Welch
    *  window is 1 - (x/pi-1)^2.
    */
    double WinFc( double arg );

private:
};
#endif // WELCH_HH
