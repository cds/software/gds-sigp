#include "FlatTop.hh"
#include <cmath>

static const double a0 = 0.2810638602;
static const double a1 = 0.5208971735;
static const double a2 = 0.1980389663;

//--------------------------------------  Constructor.
FlatTop::FlatTop( int N )
{
    setWindow( N );
}

//--------------------------------------  Copy constructor.
FlatTop::FlatTop( const FlatTop& x )
{
    if ( x.getLength( ) )
        setWindow( x.getLength( ) );
}

//--------------------------------------  Clone operator.
FlatTop*
FlatTop::clone( void ) const
{
    return new FlatTop( *this );
}

//--------------------------------------  Window function.
double
FlatTop::WinFc( double x )
{
    return a0 - a1 * cos( x ) + a2 * cos( 2 * x );
}
