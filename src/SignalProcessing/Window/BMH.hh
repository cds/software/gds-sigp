#ifndef BMH_HH
#define BMH_HH

#include "window_api.hh"

/**  BMH window class.
  *  BMH is the Blackman-Harris window class.
  */
class BMH : public window_api
{
public:
    /**  Construct a BMH window.
    *  @memo Default constructor.
    */
    BMH( void )
    {
    }

    /**  Construct a BMH window object. Initialize it for sequences of 
    *  length N.
    *  @memo Data Constructor.
    *  @param N Length of time series to be windowed.
    */
    explicit BMH( int N );

    /**  Delete the window and release any allocated storage.
    *  @memo Destructor.
    */
    ~BMH( void )
    {
    }

    /**  Create an identical BMH window.
    *  @memo clone the window.
    *  @return Pointer to a new BMH window.
    */
    BMH* clone( void ) const;

    /**  Window function.
    *  Return the window function at x, where x = 2*pi*i/(N-1). The BMH
    *  window is 1.0 - 1.36109*cos(x) + 0.39381*cos(2x) - 0.032557*cos(3x)
    */
    double WinFc( double arg );

private:
};
#endif // BMH_HH
