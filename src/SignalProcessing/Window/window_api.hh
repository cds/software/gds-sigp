/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef WINDOW_API_HH
#define WINDOW_API_HH

#include "Pipe.hh"
#include <memory>
#include "gds_memory.hh"

class DVector;

/**  The %window_api class is an abstract base class that performs common 
  *  windowing calculations. The derived classes typically differ from one 
  *  another only in the window function and the constructors needed to 
  *  provide whatever variation is appropriate to the specific window type. 
  *  The %window_api class forces a normalization of \f$\Sigma W[i]^2 = 1.0\f$.
  *  This maintains the power of a windowed series approximately constant, 
  *  but it overrides any normalization built into the window function.
  *
  *  The window function calculation can save half of the computation 
  *  steps for windows that are symmetric about the center bin (\f$x = \pi\f$). 
  *  This symmetry is assumed by default. Derived classes representing 
  *  windows that don't exhibit this symmetry should define isPiSymmetric() 
  *  to return false.
  *  @memo Windowing base class.
  *  @version 1.2 ; Modified December 29, 2021
  *  @author John G. Zweizig
  */
class window_api : public Pipe
{
public:
    using Pipe::apply;
    using Pipe::dataCheck;
    using Pipe::isDataValid;

    /// Element type of window array
    typedef double element_type;

    /**  Build an empty window function.
    *  @memo Default constructor.
    */
    window_api( void );

    /**  Copy constructor.
    *  @memo Copy constructor.
    *  @param w %window_api to be copied.
    */
    window_api( const window_api& w );

    /**  Move semantics constructor.
    *  @memo Move constructor.
    *  @param w %window_api to be copied.
    */
    window_api( window_api&& w ) = default;

    /**  Destroy the %Window object and release the function storage.
    *  @memo Virtual destructor.
    */
    virtual ~window_api( );

    /**  Copy the contents of the argument %window_api base class into this.
    *  @memo Assignment operator.
    *  @param w %Window to be copied.
    *  @return Reference to this %Window.
    */
    window_api& operator=( const window_api& w );

    /**  The argument time series is multiplied on an element-by-element basis
    *  by the windowing function. The resulting windowed series is returned.
    *  The argument TSeries is left unchanged. The Window normalization is
    *  set to \f$\Sigma_{i=0}^{N-1} W[i]^2 = 1.0\f$, as described in the class
    *  description.
    *  @memo Return a windowed TSeries.
    *  @param ts Data to which the window is to be applied.
    *  @return Windowed time series.
    */
    TSeries apply( const TSeries& ts );

    /**  The clone method returns a pointer to a new %Window object identical 
    *  to the current one.
    *  @memo Clone a %Window.
    *  @return Pointer to the new identical %Window.
    */
    virtual window_api* clone( void ) const = 0;

    /**  Check the data for validity. If the data are not applicable for 
    *  this window, an exception is thrown.
    *  @memo Test if data are valid for input to window.
    *  @param ts Time series data to test for Window compatibility.
    */
    void dataCheck( const TSeries& ts ) const;

    /**  Check the data for validity. Performs the same data checks as 
    *  dataCheck() but returns a boolean status instead of throwing an
    *  exception.
    *  @memo Test if data are valid for input to window.
    *  @param ts Time series data to test for Window compatibility.
    *  @return True if data works for window.
    */
    bool isDataValid( const TSeries& ts ) const;

    /**  Test whether the window is periodic in matlab parlance \e i.e. if 
    *  the first element would follow the last element in the underlying   
    *  sequence. A symmetric (\e i.e. non-periodic) window has all 
    *  coefficients \f$\mathrm W[i] = W[N-i-1]\f$ for all 
    *  \f$\mathrm 0 \le i < N\f$ 
    *  in an N-element window vector. For instance a symmetric hann window would
    *  have \f$\mathrm W[0] = W[N-1] = 0\f$, whereas a periodic hann window has 
    *  \f$\mathrm W[0] = 0, W[N-1] = \sin^{2} \frac{\pi (N - 1)}{N}\f$ . The 
    *  default value of the periodic flag is true unless reset by 
    *  set_periodic().
    *  \brief Test for periodic window.
    *  \returns True indicates a periodic window.
    */
    bool is_periodic( void ) const;

    /**  Return the current window length.
    *  @memo Get the current window length.
    *  @return length of window vector in samples.
    */
    int getLength( void ) const;

    /**  Return the window function RMS. This is calculated automatically in 
    *  the setWindow() method before the window is normalized.
    *  @memo Get window function RMS.
    *  @return RMS of the original window function.
    */
    double getRMS( void ) const;

    /**  Return the window parameter for window types that require a single 
    *  shape parameter, \e e.g. Kaiser and Tukey windows. This method 
    *  returns zero for window types that do not require a parameter, and
    *  the first parameter value for window type requiring more than one. 
    *  \brief Get the window shape parameter.
    *  \returns Value of first window shape parameter.
    */
    virtual double parameter( void ) const;

    /**  Set the periodic flag for the window. The meaning of the flag is 
    *  described in the documentation for the is_periodic() method.
    *  If the window is switched from periodic to symmetric or visa-versa,
    *  and the coefficients have already been calculated, the coefficient
    *  vector is reset to the specified symmetry value.
    *  \brief Set the periodic (symmetric) mode flag.
    *  \param pflag Requested periodic flag state.
    */
    void set_periodic( bool pflag );

    /**  The Window table length is set to the specified value. A new function 
    *  array is allocated if insufficient storage is available. The function 
    *  array is then filled with the calculated window coefficients.
    *  @memo Set the length and calculate the Window function.
    *  @param N Number of entries in the current window function table.
    */
    void setWindow( int N );

    /**  The window function is calculated at the argument position 
    *  (\f$arg = 2 \pi i/(N-1)\f$ for symmetric windows or 
    *  \f$arg = 2 \pi i/N\f$, where i is the entry number and N is the window
    *  length in samples)
    *  @memo Calculate the Window function.
    *  @param arg The function parameter, x, defined in the range 
    *             \f$0 <= x <= 2 \pi \f$.
    *  @return the window function at the specified argument value.
    */
    virtual double WinFc( double arg ) = 0;

    /**  Return true if the Window function is symmetric around \f$arg = \pi\f$.
    *  Functions are by default symmetric, but the symmetry assumption 
    *  may be preempted by defining isPiSymmetric() for a derived class.
    *  @memo Test the Window function for symmetry around \f$ arg = \pi \f$.
    *  @return True if the window function is symmetric around 
    *          \f$ arg = \pi \f$.
    */
    virtual bool isPiSymmetric( void );

    /**  Get the start time of the current data epoch. The start time is 
    *  set to the time of the first filtered sample after either the
    *  filter is constructed or the filter is reset. If no data have 
    *  been windowed since the construction or reset, the start time 
    *  is given as 0.
    *  @memo Get start time.
    *  @return Current epoch start time or 0 if no data have been windowed.
    */
    Time getStartTime( void ) const;

    /**  Get the current time of the current data epoch. The current time is 
    *  set with the time of the next expected sample to be filtered.
    *  @memo Get current time.
    *  @return GPS time currently being processed.
    */
    Time getCurrentTime( void ) const;

    /**  Returns true if the filter is in use.
    *  @memo Test whether windo is in use.
    *  @return True if in use.
    */
    bool inUse( void ) const;

    /**  Get a reference to the window data Vector.
    *  @memo Get the window function Vector.
    */
    const DVector& refDVect( void ) const;

    /**  Prepare the window for a disjoint data epoch. This method is needed
    *  for the Pipe class.
    *  @memo Reset the window. 
    */
    void reset( void );

private:
    std::unique_ptr< DVector > mWindow; ///< Coefficient data vector
    double                     mRMS; ///< RMS value of window coefficients.
    bool                       mPeriodic; ///< Periodic window
};

/**  window_factory creates a window instance returned as a window_api
  *  pointer. Currently implemented window functions include {"bartlett",
  *  "blackman", "flattop", "hamming", "hanning", "uniform" and "welch"} 
  *  It is recommended that the pointer is stored in an automatic
  *  pointer (\e e.g. \c std::unique_ptr<window_api>) to prevent memory leaks.
  *  \brief window factory function.
  *  \param wname  Case insensitive window name.
  *  \param length Number of samples in the window.
  */
window_api* window_factory( const std::string& wname, size_t length = 0 );

/**  window_factory_1 creates an instance of a window with a single real 
  *  parameter and returns it as a window_api pointer. Example single 
  *  parameter window types include Kaiser and Tukey windows. It is 
  *  recommended  that the pointer is stored in an automatic pointer 
  *  (\e e.g. \c std::unique_ptr<window_api>) to prevent memory leaks.
  *  \brief Single parameter window factory function.
  *  \param wname  case insensitive window name {"kaiser" or "tukey"}
  *  \param length Number of samples in the window.
  *  \param par    Window parameter (as defined by the requested window class).
  */
window_api*
window_factory_1( const std::string& wname, size_t length, double par = 0 );

/**  Identify type of window referred to by \a p. A null pointer returns 
  *  "null", a non-window_api pointer returns "none" and an unrecognized 
  *  window_api returns "other".
  *  \brief Window type string.
  *  \param p constant pipe pointer to be identified.
  */
std::string window_type( const Pipe* p );

//======================================  Inline functions
inline double
window_api::getRMS( void ) const
{
    return mRMS;
}

inline Time
window_api::getStartTime( void ) const
{
    return Time( 0 );
}

inline Time
window_api::getCurrentTime( void ) const
{
    return Time( 0 );
}

inline bool
window_api::inUse( void ) const
{
    return true;
}

inline bool
window_api::is_periodic( void ) const
{
    return mPeriodic;
}

inline void
window_api::reset( void )
{
}

inline const DVector&
window_api::refDVect( void ) const
{
    return *mWindow;
}

#endif // !defined(WINDOW_API_HH)
