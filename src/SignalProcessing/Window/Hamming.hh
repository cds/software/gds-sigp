#ifndef HAMMING_HH
#define HAMMING_HH

#include "window_api.hh"

/**  Hamming is a generalized Hamming window class. The classic Hamming 
  *  window is Wm(x) = 0.54 - 0.46*cos(x). This is generalized to 
  *  Wm(x) = beta - (1-beta)*cos(x) where beta can take any value in the
  *  range 0<beta<1. Beta=0.5 corresponds to the Hannig window.
  *  @memo Hamming window class.
  */
class Hamming : public window_api
{
public:
    /**  Use the true hamming window parameter, beta=0.54.
    *  @memo Default constructor.
    */
    Hamming( void ) : mBeta( 0.54 )
    {
    }

    /**  Construct a generalized Hamming window of length 'len'. If beta
    *  isn't specified it defalts to the true hamming window.
    *  @memo Full constructor.
    */
    explicit Hamming( int len, double mBeta = 0.54 );

    /**  Construct a generalized Hamming window identical to the argument.
    *  @memo Copy constructor.
    */
    explicit Hamming( const Hamming& x );

    /**  Delete the object.
    *  @memo Destructor.
    */
    ~Hamming( void )
    {
    }

    /**  Create a new window object identical to he current one.
    *  @memo Clone the Hamming window.
    */
    Hamming* clone( void ) const;

    /**  Window function.
    *  Return the function value at x=2*pi*i/(N-1), where i is the sample
    *  number and N is the series length.
    */
    double WinFc( double x );

private:
    double mBeta;
};
#endif // HAMMING_HH
