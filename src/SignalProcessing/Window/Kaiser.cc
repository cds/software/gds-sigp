/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "Kaiser.hh"
#include <cmath>
#include <stdexcept>

using namespace std;

//======================================  Constructor
Kaiser::Kaiser( double alpha, int N ) : mAlpha( alpha )
{
    mI0AlphaPi = bessel0( mAlpha * M_PI );
    if ( N )
        setWindow( N );
}

//======================================  Destructor
Kaiser::~Kaiser( void )
{
}

//======================================  Clone a window
Kaiser*
Kaiser::clone( void ) const
{
    return new Kaiser( *this );
}

//======================================  alpha parameter accessor
double
Kaiser::parameter( void ) const
{
    return mAlpha;
}

//======================================  Kaiser window function.
double
Kaiser::WinFc( double arg )
{
    // Note: x = 2*pi*x/(N-1)
    //       beta = pi * alpha
    //       so Kaiser(x) = I0(beta*sqrt(1-(2x/(N-1))^2))/I0(beta)
    //                    = I0(alpha * sqrt(pi^2 - (pi - arg)^2)) / I0(pi*alpha)
    //                    = I0(alpha * sqrt((2*pi - arg) * arg) / I0(pi*alpha)
    const double twopi = 2.0 * M_PI;
    double       x = ( twopi - arg ) * arg;
    if ( x < 0 )
        throw logic_error( "Kaiser::WinFc(x) called with 0 <= x > 2pi" );
    return bessel0( mAlpha * sqrt( x ) ) / mI0AlphaPi;
}

//======================================  Modified zeroth order bessel function
double
Kaiser::bessel0( double x )
{
    const double eps = 1e-8; // required accuracy
    double       y = x / 2.0;
    double       sum = 1.0;
    double       de = 1.0;
    double       sde = 1.0;
    for ( int i = 1; i < 25 && sum * eps < sde; i++ )
    {
        de *= y / double( i );
        sde = de * de;
        sum += sde;
    }
    return sum;
}
