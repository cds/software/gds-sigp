#ifndef HANNING_HH
#define HANNING_HH

#include "window_api.hh"

/**  Hanning window class.
  *  Hanning is the Hanning window class.
  */
class Hanning : public window_api
{
public:
    /**  Construct a Hanning window.
    *  @memo Default constructor.
    */
    Hanning( void )
    {
    }

    /**  Construct a Hanning window object. Initialize it for sequences of 
    *  length N.
    *  @memo Data Constructor.
    *  @param N Length of time series to be windowed.
    */
    explicit Hanning( int N );

    /**  Construct a Hanning window object identical to an existing one.
    *  @memo Copy constructor.
    */
    explicit Hanning( const Hanning& x );

    /**  Delete the window and release any allocated storage.
    *  @memo Destructor.
    */
    ~Hanning( void )
    {
    }

    /**  Create an identical Hanning window.
    *  @memo clone the window.
    *  @return Pointer to  a new Hanning window.
    */
    Hanning* clone( void ) const;

    /**  Window function.
    *  Return the window function at x, where x = 2*pi*i/(N-1). The Hanning
    *  window is 0.5*(1-cos(x)).
    */
    double WinFc( double arg );

private:
};
#endif // HANNING_HH
