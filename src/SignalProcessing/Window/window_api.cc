/* -*- mode: c++; c-basic-offset: 3; -*- */
//
//    Methods for the Window base class.
//
//    21/07/1999  JGZ  I added the symmetric calculation method to setWindow
//                     and rescaled the window coefficients by 1/RMS to give
//                     unit power normalization. By integrating the Hanning
//                     window squared, I calculate the window RMS to be
//                     sqrt(0.375)=0.6123724. The direct RMS calculation for
//                     an 8192 point window gives 0.6123351
//    29/12/2021  JGZ  Added the Periodic flag.
//
#include "DVecType.hh"
#include "Interval.hh"
#include "Time.hh"
#include "TSeries.hh"
#include "window_api.hh"
#include "constant.hh"
#include <cmath>

//======================================  Window constructor
window_api::window_api( void ) : mRMS( 0.0 ), mPeriodic( true )
{
}

//======================================  Copy constructor
window_api::window_api( const window_api& w ) : mRMS( 0.0 )
{
    *this = w;
}

//======================================  Window destructor
window_api::~window_api( )
{
}

//======================================  Assignment operator
window_api&
window_api::operator=( const window_api& w )
{
    mRMS = w.mRMS;
    if ( mWindow.get( ) && w.mWindow.get( ) )
        *mWindow = *w.mWindow;
    else if ( w.mWindow.get( ) )
        mWindow.reset( w.mWindow->clone( ) );
    else if ( mWindow.get( ) )
        mWindow->Clear( );
    return *this;
}

//======================================  Return true if symmetric around x=pi
bool
window_api::isPiSymmetric( void )
{
    return true;
}

//======================================  Set length N window contents
void
window_api::setWindow( int N )
{
    if ( N <= 0 || getLength( ) == N )
        return;

    //----------------------------------  Allocate storage.
    if ( mWindow.get( ) )
        mWindow->ReSize( N );
    else
        mWindow.reset( new DVecType< element_type >( N ) );
    element_type* pWindow =
        reinterpret_cast< element_type* >( mWindow->refData( ) );

    //----------------------------------  Fill in the window vector.
    double dPhi( 0 );
    if ( mPeriodic )
    {
        dPhi = twopi / double( N );
    }
    else
    {
        if ( N > 1 )
            dPhi = twopi / double( N - 1 ); // symmetric => last point at 2*pi
    }

    double SumSq = 0;
    if ( isPiSymmetric( ) )
    {
        if ( mPeriodic )
        {
            int n2 = ( N - 1 ) / 2 + 1; // First index not duplicated.
            pWindow[ 0 ] = WinFc( 0.0 );
            for ( int i = 1; i < n2; i++ )
            {
                double Xn = WinFc( dPhi * double( i ) );
                pWindow[ i ] = Xn;
                pWindow[ N - i ] = Xn;
                SumSq += Xn * Xn;
            }
            SumSq *= 2.0;
            SumSq += pWindow[ 0 ] * pWindow[ 0 ];
            if ( !( N & 1 ) )
            { //  One more entry for even N.
                double Xmid = WinFc( pi );
                pWindow[ n2 ] = Xmid;
                SumSq += Xmid * Xmid;
            }
        }
        else
        {
            int n2 = N / 2;
            for ( int i = 0; i < n2; i++ )
            { // loop over independent entries
                double Xn = WinFc( dPhi * double( i ) );
                pWindow[ i ] = Xn;
                pWindow[ N - 1 - i ] = Xn;
                SumSq += Xn * Xn;
            }
            SumSq *= 2.0;
            if ( ( N & 1 ) )
            { //  One more entry for odd N.
                double Xmid = WinFc( pi );
                pWindow[ n2 ] = Xmid;
                SumSq += Xmid * Xmid;
            }
        }
    }
    else
    { //  No symmetry assumption
        for ( int i = 0; i < N; i++ )
        {
            pWindow[ i ] = WinFc( dPhi * double( i ) );
            SumSq += pWindow[ i ] * pWindow[ i ];
        }
    }

    //----------------------------------  Now scale by 1/RMS to maintain power
    mRMS = std::sqrt( SumSq / N );
    double Norm = 1.0 / mRMS;
    *mWindow *= Norm;
}

//======================================  Apply window to the specified series
TSeries
window_api::apply( const TSeries& ts )
{
    TSeries      r( ts );
    unsigned int N = ts.getNSample( );
    if ( !N )
        return r;
    setWindow( N );
    DVector* dv = r.refDVect( );
    switch ( dv->getType( ) )
    {
    case DVector::t_short:
    case DVector::t_int:
        r.Convert( DVecType< element_type >::getDataType( ) );
        dv = r.refDVect( );
        break;
    case DVector::t_long:
        r.Convert( DVector::t_double );
        dv = r.refDVect( );
        break;
    default:
        break;
    }
    dv->mpy( 0, *mWindow, 0, N );
    return r;
}

//======================================  All data are valid
void
window_api::dataCheck( const TSeries& ts ) const
{
    return;
}

//======================================  Get the current window length
int
window_api::getLength( void ) const
{
    if ( !mWindow.get( ) )
        return 0;
    return mWindow->getLength( );
}

//======================================  Default (unspecified) window parameter
double
window_api::parameter( void ) const
{
    return 0.0;
}

//======================================  Set the periodic flag
void
window_api::set_periodic( bool pflag )
{
    size_t N = getLength( );
    if ( pflag != mPeriodic && N != 0 )
    {
        mPeriodic = pflag;
        mWindow->ReSize( 1 );
        setWindow( N );
    }
    else
    {
        mPeriodic = pflag;
    }
}

//======================================  All data are valid
bool
window_api::isDataValid( const TSeries& ts ) const
{
    return true;
}
