#include "Uniform.hh"

//--------------------------------------  Constructor.
Uniform::Uniform( int N )
{
    setWindow( N );
}

//--------------------------------------  Clone operator.
Uniform*
Uniform::clone( void ) const
{
    return new Uniform( *this );
}

//--------------------------------------  Window function.
double
Uniform::WinFc( double x )
{
    return 1.0;
}
