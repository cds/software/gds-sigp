/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "Nutall.hh"
#include <cmath>

//--------------------------------------  Constructor.
Nutall::Nutall( int N )
{
    setWindow( N );
}

//--------------------------------------  Copy constructor.
Nutall*
Nutall::clone( void ) const
{
    return new Nutall( *this );
}

//--------------------------------------  Window function.
double
Nutall::WinFc( double x )
{
    return 0.355768 - 0.487396 * cos( x ) + 0.144232 * cos( 2 * x ) -
        .012604 * cos( 3 * x );
}
