#include "Hanning.hh"
#include <cmath>

//--------------------------------------  Constructor.
Hanning::Hanning( int N )
{
    setWindow( N );
}

//--------------------------------------  Copy constructor.
Hanning::Hanning( const Hanning& x )
{
    if ( x.getLength( ) )
        setWindow( x.getLength( ) );
}

//--------------------------------------  Clone operator.
Hanning*
Hanning::clone( void ) const
{
    return new Hanning( *this );
}

//--------------------------------------  Window function.
double
Hanning::WinFc( double x )
{
    return 0.5 * ( 1.0 - cos( x ) );
}
