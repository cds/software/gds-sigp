#ifndef BLACKMAN_HH
#define BLACKMAN_HH

#include "window_api.hh"

/**  The Blackman class implements a generalized Blackman window. The 
  *  window function is: Wb(x) = (0.5 - Beta) - 0.5*cos(Xj) + Beta*cos(2Xj), 
  *  where Xj=2*pi*j/(N-1). The classic Blackman window is given by Beta=0.08.
  *  A slight change in the parameters of the Blackman window (Beta=0.125) 
  *  give a 30-dB/octave rolloff. The 30dB window can be implemented using 
  *  the alternate constructor
  *  @memo Generalized Blackman window class.
  */
class Blackman : public window_api
{
public:
    /**  Default constructor.
    *  Use the true Blackman window parameter, beta=0.08.
    */
    Blackman( void ) : mBeta( 0.08 )
    {
    }

    /**  Construct a generalized Blackman window of length 'len'. If beta
    *  isn't specified it defalts to the true Blackman window.
    *  @memo Full constructor.
    *  @param len Number of samples in the TSeries to be windowed.
    *  @param Beta $cos(2x)$ coeffcient in window function.
    */
    explicit Blackman( int len, double Beta = 0.08 );

    /**  Construct a generalized Blackman window of length 'len'. If beta
    *  isn't specified it defalts to the true hamming window.
    *  @memo Copy constructor.
    *  @param x Blackman window to be copied.
    */
    explicit Blackman( const Blackman& x );

    /**  Delete the object.
    *  @memo Destructor.
    */
    ~Blackman( )
    {
    }

    /**  Create an identical copy of the windo and return a pointer.
    *  @memo Clone the window.
    *  @return Pointer to the cloned window.
    */
    Blackman* clone( void ) const;

    /**  Window function.
    *  Return the function value at Xj=2*pi*j/(N-1), where j is the sample
    *  number and N is the series length.
    */
    double WinFc( double x );

private:
    double mBeta;
};
#endif // BLACKMAN_HH
