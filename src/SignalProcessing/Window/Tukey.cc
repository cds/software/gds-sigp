/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "Tukey.hh"
#include "constant.hh"
#include <cmath>

//======================================  Constructor
Tukey::Tukey( double frac, int N ) : mFrac( frac )
{
    if ( mFrac < 0 )
        mFrac = 0.0;
    else if ( mFrac > 1.0 )
        mFrac = 1.0;
    if ( N )
        setWindow( N );
}

//======================================  Clone
Tukey*
Tukey::clone( void ) const
{
    return new Tukey( mFrac );
}

//======================================  fraction parameter accessor
double
Tukey::parameter( void ) const
{
    return mFrac;
}

//======================================  Generate Tukey
double
Tukey::WinFc( double arg )
{
    if ( mFrac == 1.0 )
        return 1.0;
    double pifrac = pi * mFrac;
    double piarg = arg - pi;
    double x;
    if ( fabs( piarg ) < pifrac )
        x = 0.0;
    else if ( piarg < 0 )
        x = ( piarg + pifrac ) / ( 1.0 - mFrac );
    else
        x = ( piarg - pifrac ) / ( 1.0 - mFrac );
    return 0.5 * ( 1 + cos( x ) );
}
