/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "psd_estimate.hh"
#include "Hamming.hh"
#include "DecimateBy2.hh"
#include "fSeries/DFT.hh"
#include "fSeries/ASD.hh"
#include "DVecType.hh"
#include "FIRdft.hh"
#include <stdexcept>
#include <iostream>

using namespace std;

//======================================  Default constructor.
psd_estimate::psd_estimate( void )
    : mStride( 0 ), mOverlap( 0 ), mStartTime( 0 ), mCurrent( 0 )
{
}

//======================================  Data constructor.
psd_estimate::psd_estimate( Interval          stride,
                            double            overlap,
                            const window_api* w )
    : mStride( stride ), mStartTime( 0 ), mCurrent( 0 )
{
    if ( overlap < 0 || overlap >= 1.0 )
    {
        if ( w )
            set_window( *w, true );
        else
            set_overlap( 0 );
    }
    else
    {
        if ( w )
            set_window( *w, false );
        set_overlap( overlap );
    }
}

//======================================  Destructor.
psd_estimate::~psd_estimate( void )
{
}

//======================================  Make a whitener Filter
Pipe*
psd_estimate::make_whitener( double sample_rate, char mode )
{
    containers::DFT dft;
    static_cast< containers::fSeries& >( dft ) = containers::ASD( get_psd( ) );
    dft /= get_psd( );
    dft.unfold( );
    TSeries       from_dft( dft.iFFT( ) );
    const DVectD& dvd( dynamic_cast< const DVectD& >( *from_dft.refDVect( ) ) );
    FIRdft        wf( 0, sample_rate );
    wf.setCoefs( dvd.size( ), &dvd[ 0 ] );
    return wf.clone( );
}

//======================================  Reset accumulators and history
void
psd_estimate::reset( void )
{
    reset_accumulators( );
    reset_history( );
    mStartTime = Time( 0 );
}

//======================================  Set-up for welch method
void
psd_estimate::set_defaults( Interval stride )
{
    set_stride( stride );
    Hamming ham;
    set_window( ham, true ); // set hamming window and 0.5 overlap
}

//======================================  Set the overlap value
void
psd_estimate::set_overlap( double ovlp )
{
    if ( ovlp < 0 || ovlp >= 1.0 )
        throw std::invalid_argument( "psd_estimate: Invalid overlap fraction" );
    mOverlap = ovlp;
}

//======================================  Set the stride length
void
psd_estimate::set_stride( Interval dt )
{
    mStride = dt;
}

//======================================  Set the stride based on the input
//                                        series length and a number of
//                                        segments.
void
psd_estimate::set_stride( const TSeries& x, double nSeg )
{
    Interval tSeg = x.getInterval( );
    if ( nSeg > 1.0 )
        tSeg /= nSeg;
    if ( !tSeg )
        throw runtime_error( "psd_estimate: Stride set to zero" );
    mStride = tSeg;
}

//======================================  Set the window
void
psd_estimate::set_window( const window_api& w, bool set_ovlp )
{
    mWindow.set( w );
    if ( !set_ovlp )
    {
        mOverlap = 0.0;
    }
    else if ( window_type( &w ) == "tukey" )
    {
        mOverlap = 0.5 * ( 1 - w.parameter( ) );
    }
    else if ( window_type( &w ) == "square" )
    {
        mOverlap = 0.0;
    }
    else
    {
        mOverlap = 0.5;
    }
}
