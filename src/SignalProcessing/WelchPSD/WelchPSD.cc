/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "WelchPSD.hh"
#include "Hamming.hh"
#include "DecimateBy2.hh"
#include "fSeries/DFT.hh"
#include <bit>
#include <stdexcept>
#include <iostream>

using namespace std;

//======================================  Default constructor.
WelchPSD::WelchPSD( void ) : mSampleRate( 0 ), mNAverage( 0 )
{
}

//======================================  Data constructor.
WelchPSD::WelchPSD( Interval          stride,
                    double            overlap,
                    const window_api* w,
                    double            sample_rate )
    : psd_estimate( stride, overlap, w ), mSampleRate( sample_rate ),
      mNAverage( 0 )
{
}

//======================================  Destructor.
WelchPSD::~WelchPSD( void )
{
}

//======================================  Clone
WelchPSD*
WelchPSD::clone( void ) const
{
    return new WelchPSD( *this );
}

//======================================  Add one or more strides to the
//                                        accumulated coherence
void
WelchPSD::add( const TSeries& x )
{

    //------------------------------------  Calculate the stride.
    if ( mStride == Interval( 0 ) )
    {
        set_stride( x, 1.0 );
    }

    //------------------------------------  Set up to resample as appropriate
    if ( mSampleRate == 0 )
    {
        if ( x.getTStep( ) == Interval( 0.0 ) )
        {
            throw runtime_error( "WelchPSD: Invalid sample rate. " );
        }
        else
        {
            mSampleRate = 1.0 / x.getTStep( );
        }
    }

    //------------------------------------ Resample the input and append
    resample( mXDecim, x, mXHistory );

    //------------------------------------  Record first data start time.
    if ( !mStartTime )
    {
        mStartTime = mXHistory.getStartTime( );
        mCurrent = mStartTime;
    }

    //------------------------------------  Loop over overlapping strides.
    while ( mXHistory.getInterval( ) >= mStride )
    {
        containers::DFT xDft(
            mWindow( mXHistory.extract( mCurrent, mStride ) ) );

        //-------------------------------- First time - set accumulator.
        if ( mXXSum.empty( ) )
        {
            mXXSum = containers::PSD( xDft );
        }

        //------------------------------  Subsequently - Add to accumulator.
        else
        {
            mXXSum += containers::PSD( xDft );
        }
        mNAverage++;

        //----------------------------------  Advance history and current time.
        Interval DtErase = mStride * ( 1.0 - mOverlap );
        mXHistory.eraseStart( DtErase );
        mCurrent += DtErase;
    }
}

//======================================  Get the PSD from the accumulator
containers::PSD
WelchPSD::get_psd( void ) const
{
    if ( !mNAverage || mNAverage == 1 )
        return mXXSum;

    //----------------------------------  fill a PSD with the CSD modsq.
    containers::PSD r( mXXSum );
    r *= 1.0 / double( mNAverage );
    return r;
}

//======================================  Resample data and append it to the
//                                        input history series.
void
WelchPSD::resample( auto_pipe& decim, const TSeries& in, TSeries& hist )
{
    //cout << "resample: rate=" << mSampleRate << " tStep=" << in.getTStep()
    // 	   << " start=" << in.getStartTime().getS() << endl;

    //------------------------------------  No resampling necessary
    if ( fabs( mSampleRate * double( in.getTStep( ) ) - 1.0 ) < 1e-6 )
    {
        if ( hist.empty( ) )
        {
            hist = in;
        }
        else
        {
            int rc = hist.Append( in );
            if ( rc )
            {
                cerr << "TSeries::Append returned rc=" << rc
                     << " tStep=" << hist.getTStep( )
                     << " end=" << hist.getEndTime( ).getS( ) << endl;
                throw runtime_error( "WelchPSD: Invalid input data." );
            }
        }
    }

    //------------------------------------  Set up resampling?
    else
    {
        if ( !mStartTime )
        {
            int resample =
                int( 1.0 / double( in.getTStep( ) * mSampleRate ) + 0.5 );
            if ( resample < 2 || !std::__has_single_bit( resample ) )
                throw runtime_error( "WelchPSD: Invalid resample request" );
            int N = 0;
            while ( resample > 1 )
            {
                resample /= 2;
                N++;
            }
            decim.set( new DecimateBy2( N, 1 ) );
        }

        //------------------------------------  Resample
        if ( decim.null( ) )
            throw runtime_error( "WelchPSD: Resampling misconfigured." );
        if ( hist.empty( ) )
        {
            hist = decim( in );
        }
        else
        {
            int rc = hist.Append( decim( in ) );
            if ( rc )
                throw runtime_error( "WelchPSD: Invalid input data." );
        }
    }
}

//======================================  Reset accumulators and history
void
WelchPSD::reset_history( void )
{
    mXHistory.Clear( Time( 0 ) );
    mXDecim.set( 0 );
}

//======================================  Reset accumulators and history
void
WelchPSD::reset_accumulators( void )
{
    mXXSum.clear( );
    mNAverage = 0;
}

//======================================  Set the sample rate
void
WelchPSD::set_rate( double rate )
{
    mSampleRate = rate;
}

//======================================  Print status information
void
WelchPSD::status( std::ostream& out ) const
{
    out << "WelchPSD status: " << endl;
    out << "  stride:        " << mStride << endl;
    out << "  overlap:       " << mOverlap << endl;
    out << "  sample rate:   " << mSampleRate << endl;
    out << "  window:        " << window_type( mWindow.get( ) ) << endl;
    out << "  start time:    " << mStartTime << endl;
    out << "  current time:  " << mCurrent << endl;
    out << "  history start: " << mXHistory.getStartTime( ) << endl;
    out << "  history end:   " << mXHistory.getEndTime( ) << endl;
    out << "  # of averages: " << mNAverage << endl;
}
