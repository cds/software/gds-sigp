/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef PSD_ESTIMATE_HH
#define PSD_ESTIMATE_HH

#include "TSeries.hh"
#include "fSeries/PSD.hh"
#include "autopipe.hh"
#include <iosfwd>

class window_api;

/**  The %psd_estimate class is an abstract base class to calculate the power 
  *  spectral density of a digitized signal. Concrete instances will use 
  *  either Welch's method, a running median method or etc. In general the 
  *  input time series is divided into one or more overlapping (as specified) 
  *  segments which are windowed and combined as specified according to the
  *  implemented method. If a desired sample rate is specified, the signal 
  *  will be down-sampled by factors of two (using DecimateBy2) to give the 
  *  requested sample rate. 
  *  If the preferred sample rate is not specified the sample rate of the
  *  input time series is used. The input signal is then divided 
  *  into segments of the specified stride length and overlap fraction.
  *  The data segments are windowed as requested and the power spectral 
  *  density is calculated and added to the accumulator. An arbitrary 
  *  number of segments may be added into the accumulator with the add() 
  *  method. The resulting PSD is calculated by the get_psd(void) method or
  *  by the operator()(void) method.
  *  \brief Calculate estimate of the power spectral density.
  *  \author John Zweizig (john.zweizig@ligo.org)
  */
class psd_estimate
{
public:
    /**  Construct an uninitialized %PSD object. If no stride, window, 
      *  overlap or sample rate are specified, these parameters will be 
      *  inferred from the first data as follows:
      *  - \e Stride: length will be the same as the input series length.
      *  - \e Window: No windowing will be performed.
      *  - \e Overlap: No Overlapping will be performed.
      *  - \e nAverage: Number of averages per estimate.
      *  - \e Sample-Rate: The input series will not be resampled..
      *  \brief Default constructor.
      */
    psd_estimate( void );

    /**  Construct a %psd_estimate and set the stride, overlap, window and
      *  sample rate parameters. If any or all of the parameters are set 
      *  to zero or left unspecified, these values will take the default
      *  values as specified in the default (psd_estimate()) constructor.
      *  \brief Construct and initialize a %psd_estimate object.
      *  \param stride  Length of an analysis stride. (see set_stride())
      *  \param overlap Fractional overlap of the  strides (see set_overlap()).
      *  \param win     Window instance (see set_window()).
      *  \param sample_rate Sample rate (see set_rate())
      */
    explicit psd_estimate( Interval          stride,
                           double            overlap = 0.0,
                           const window_api* win = 0 );

#ifndef __CINT__
    /**  Define a default copy constructor
      *  \brief  Copy constructor.
      *  \param x %psd_estimate instance to be copied 
      */
    psd_estimate( const psd_estimate& x ) = default;
#endif

    /**  Destroy the %psd_estimate object and release all current storage.
      *  \brief  Destructor.
      */
    virtual ~psd_estimate( void );

    /** Clone the psd_estimate instance.
     *  \brief Clone a psd_estimate.
     *  \return pointer to copy of a psd_estimate.
     */
    virtual psd_estimate* clone( void ) const = 0;

    /**  Accumulate all the information needed to calculate the %Welch %PSD
      *  estimate from one or more strides. The input series is resampled 
      *  as appropriate and added to the input signal history series. If 
      *  sufficient data are available to calculate one or more strides, the 
      *  data segment(s) are windowed and a discrete Fourier transform is made.
      *  The resulting DFT is squared, and added to the accumulated %PSD.
      *  The normalization is as defined for the PSD class.
      *  \brief Add one or more data strides to the %psd_estimate data 
      *         accumulator.
      *
      *  \exception std::runtime_error is thrown if the the sample rate of
      *  the input series can not be converted to the specified rate,
      *  or if the series start time does not match the end of the series
      *  history data.
      *  \param x %Time series containing the digital signal data.
      */
    virtual void add( const TSeries& x ) = 0;

    /**  Calculate the %Welch %PSD estimator from the accumulated data and 
      *  return it in a PSD class instance.
      *  \brief Calculate the %PSD from the accumulated data.
      *  \note The data accumulator is not reset. The summed data used to 
      *  calculate the psd will be included in the next get_psd() calculation 
      *  unless the accumulator is explicitly cleared with reset_accumulator() 
      *  or reset().
      *  \return %Welch power spectral density estimate in a PSD.
      */
    virtual containers::PSD get_psd( void ) const = 0;

    /**  Get the estimator type string, i.e. "welch", "mean-median" or 
      *  "rayleigh".
      *  \brief Type string
      *  \return type string
      */
    virtual const char* get_type( void ) const = 0;

    /**  Return the start time of the next overlapping stride to be 
      *  accumulated.
      *  \brief Start of next data stride.
      *  \return Constant reference to the start Time of the next stride to be 
      *  accumulated.
      */
    const Time& currentTime( void ) const;

    /**  Make an FIR whitening filter based on the current PSD estimate.
      *  \brief make a whiltening filter.
      *  \param sample_rate Sample rate for the generated whitening filter.
      *  \param mode  whitening filter mode, time-domain='t', 
      *               frequency domain='f'.
      *  \return An fir filter to whiten a series.
      */
    virtual Pipe* make_whitener( double sample_rate, char mode = 't' );

    /**  Calculate the %PSD estimate from an input time series containing 
      *  one or more data segments. The %PSD is calculated using the parameters
      *  (stride length, overlap, sample rate and window function) specified 
      *  in the constructor or with the set functions. The accumulator is 
      *  reset before the calculation, but the history data are left intact.
      *  This leaves the %psd_estimate estimator object ready to continue 
      *  processing the next stride of a data stream.
      *  \exception std::runtime_error is thrown if the sample rate of the
      *  input series can not be converted to the specified rate,
      *  or if the series start time does not match the end of the series
      *  history data.
      *  \brief Single step %PSD calculation.
      *  \param x Time series containing digitized signal data.
      *  \return Welch method power spectral density estimate returned in a PSD.
      */
    virtual containers::PSD operator( )( const TSeries& x );

    /** Get the configured overlap fraction.
     *  \brief Overlap
     *  \returns Overlap fraction.
     */
    virtual double overlap( void ) const;

    /** Calculate the number of overlapping strides in a specified time span 
     *  given the current stride and overlap fraction.
     *  \brief Number of strides for a given interval.
     *  \param dt Length of data to be processed
     *  \return number of overlapping strides.
     */
    size_t nstride( Interval dt ) const;

    /**  Get a reference to the window.
      *  \brief reference window.
      *  \return Constan reference to the window.
      */
    const Pipe* ref_window( void ) const;

    /**  Reset the accumulator, clear the input history and destroy the 
      *  resampling filters. Note that the sample rate and stride time are 
      *  retained even if they were inferred from the first data stride.
      *  The input sample rate may be changed after a reset.
      *  \brief Reset accumulators and history.
      */
    virtual void reset( void );

    /**  Reset the %psd_estimate accumulator and stride count. The history and 
      *  resampling filters are retained. This method should be used when 
      *  calculating a %PSD estimate of subsequent segments of a data stream.
      *  \brief Reset the %psd_estimate accumulator.
      */
    virtual void reset_accumulators( void ) = 0;

    /**  Reset the input signal history and signal conditioning filters. This 
      *  method should be used for a global reset (i.e. in reset()).
      *  \brief Reset the history and signal conditioning states.
      */
    virtual void reset_history( void ) = 0;

    /**  Set parameters to defaules as for a standard Welch estimate with the 
      *  given stride length. This method uses a Hamming window and 50% overlap. 
      *  The stride length considerations are discussed for set_stride(). If the
      *  stride is set to zero, the length of the first data series passed 
      *  to add() will be used instead.
      *  \brief Set parameters to default values.
      *  \param stride Data stride length.
      */
    virtual void set_defaults( Interval stride );

    /**  Set the overlap fraction parameter that indicates the portion of
      *  input data stride to be retained for use in the next data stride.
      *  The valid range for the overlap fraction is \c 0\<=x\<1.
      *  \exception std::invalid_argument \a ovlp is not a valid fraction.
      *  \brief Set overlap parameter.
      *  \param ovlp Fraction of stride to be retained.
      */
    virtual void set_overlap( double ovlp );

    /**  Set the length of a single stride in seconds. The %PSD
      *  frequency step will be the inverse of this number.
      *  \brief Set stride time.
      *  \param dt Stride time (in seconds).
      */
    virtual void set_stride( Interval dt );

    /**  Set the length of a single stride based on the length of a TSeries. 
      *  The stride length is set to the length of the TSeries divided 
      *  by the specified number of segments. If the number of segments
      *  is less than one, one is assumed. The %PSD frequency step will be 
      *  the inverse of this number.
      *  \brief Set stride time.
      *  \param x    Stride time (in seconds).
      *  \param nSeg Number of segments the series is to be divided into.
      */
    virtual void set_stride( const TSeries& x, double nSeg );

    /**  Set the windowing function to be applied to each data stride before
      *  performing the discrete Fourier transform. If no window is set, a 
      *  rectangular window is used. The \a ov argument indicates whether the 
      *  overlap is to be set to reflect the window symmetry.
      *  \brief Set the window and overlap.
      *  \param w  Window class object to be used.
      *  \param ov set overlap to reflect windo symmetry.
      */
    virtual void set_window( const window_api& w, bool ov = true );

    /**  Get the time of the first data provided to the add() method after 
      *  the most recent reset().
      *  \brief Get start of analyzed data stream.
      *  \return Start time of the current data stream.
      */
    virtual const Time& startTime( void ) const;

    /**  Print a status description to the specified stream.
      *  \brief print status
      *  \param out output stream to receive the status info.
      */
    virtual void status( std::ostream& out = std::cout ) const = 0;

    /**  Get the stride length in seconds.
      *  \brief Stride length.
      *  \return Stride interval.
      */
    Interval stride( void ) const;

protected:
    //------------------------------------  Parameters
    Interval  mStride;
    double    mOverlap;
    long      mAvgRequest;
    auto_pipe mWindow;

    //------------------------------------  State variables.
    Time mStartTime;
    Time mCurrent;
};

//======================================  In-line methods
inline const Time&
psd_estimate::currentTime( void ) const
{
    return mCurrent;
}

inline const Time&
psd_estimate::startTime( void ) const
{
    return mStartTime;
}

inline containers::PSD
psd_estimate::operator( )( const TSeries& ts )
{
    reset_accumulators( );
    add( ts );
    return get_psd( );
}

inline double
psd_estimate::overlap( void ) const
{
    return mOverlap;
}

inline size_t
psd_estimate::nstride( Interval dt ) const
{
    if ( !mStride || dt < mStride )
        return 0;
    return size_t( ( dt - mStride ) / ( mStride * ( 1.0 - mOverlap ) ) ) + 1;
}

inline const Pipe*
psd_estimate::ref_window( void ) const
{
    return mWindow.get( );
}

inline Interval
psd_estimate::stride( void ) const
{
    return mStride;
}

#endif // !defined(PSD_ESTIMATE_HH)
