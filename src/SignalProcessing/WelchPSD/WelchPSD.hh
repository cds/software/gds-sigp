/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef WELCHPSD_HH
#define WELCHPSD_HH

#include "psd_estimate.hh"

class window_api;

/**  The %WelchPSD class calculates the power spectral density of a
  *  digitized signal using Welch's method. It sums the power spectral
  *  density of windowed, overlapping strides of a signal. If a 
  *  desired sample rate is specified, the signal will be resampled by 
  *  factors of two (using DecimateBy2) to give the requested sample rate. 
  *  If the preferred sample rate is not specified the sample rate of the
  *  input time series is used. The input signal is then divided 
  *  into segments of the specified stride length and overlap fraction.
  *  The data segments are windowed as requested and the power spectral 
  *  density is calculated and added to the accumulator. An arbitrary 
  *  number of segments may be added into the accumulator with the add() 
  *  method. The resulting PSD is calculated by the get_psd() method or
  *  by the operator()() method.
  *  \brief Calculate Welch estimator of the power spectral density.
  *  \author John Zweizig (john.zweizig@ligo.org)
  */
class WelchPSD : public psd_estimate
{
public:
    /**  Construct an uninitialized %WelchPSD object. If no stride, window, 
      *  overlap or sample rate are specified, these parameters will be 
      *  inferred from the first data as follows:
      *  - \e Stride: length will be the same as the input series.length.
      *  - \e Window: No windowing will be performed.
      *  - \e Overlap: No Overlapping will be performed.
      *  - \e Sample-Rate: The input series will not be resampled..
      *  \brief Default constructor.
      */
    WelchPSD( void );

    /**  Construct a %WelchPSD and set the stride, overlap, window and
      *  sample rate parameters. If any or all of the parameters are set 
      *  to zero or left unspecified, these values will take the default
      *  values as specified in the default (WelchPSD()) constructor.
      *  \brief Construct and initialize a %WelchPSD object.
      *  \param stride  Length of an analysis stride. (see set_stride())
      *  \param overlap Fractional overlap of the  strides (see set_overlap()).
      *  \param win     Window instance (see set_window()).
      *  \param sample_rate Sample rate (see set_rate())
      */
    explicit WelchPSD( Interval          stride,
                       double            overlap = 0.0,
                       const window_api* win = 0,
                       double            sample_rate = 0 );

    /**  Destroy the %WelchPSD object and release all current storage.
      *  \brief  Destructor.
      */
    virtual ~WelchPSD( void );

    /** Clone the psd_estimate instance.
     *  \brief Clone a psd_estimate.
     *  \return pointer to copy of a psd_estimate.
     */
    WelchPSD* clone( void ) const;

    /**  Accumulate all the information needed to calculate the %Welch %PSD
      *  estimate from one or more strides. The input series is resampled 
      *  as appropriate and added to the input signal history series. If 
      *  sufficient data are available to calculate one or more strides, the 
      *  data segment(s) are windowed and a discrete Fourier transform is made.
      *  The resulting DFT is squared, and added to the accumulated %PSD.
      *  The normalization is as defined for the PSD class.
      *  \brief Add one or more data strides to the %WelchPSD data accumulator.
      *
      *  \exception std::runtime_error is thrown if the the sample rate of
      *  the input series can not be converted to the specified rate,
      *  or if the series start time does not match the end of the series
      *  history data.
      *  \param x %Time series containing the digital signal data.
      */
    void add( const TSeries& x );

    /**  Calculate the %Welch %PSD estimator from the accumulated data and 
      *  return it in a PSD class instance.
      *  \brief Calculate the %PSD from the accumulated data.
      *  \note The data accumulator is not reset. The summed data used to 
      *  calculate the psd will be included in the next get_psd() calculation 
      *  unless the accumulator is explicitly cleared with reset_accumulator() 
      *  or reset().
      *  \return %Welch power spectral density estimate in a PSD.
      */
    containers::PSD get_psd( void ) const;

    /**  Get the estimator type string, "welch")
      *  \brief Type string
      *  \return type string
      */
    const char* get_type( void ) const;

    /**  Reset the %WelchPSD accumulator and stride count. The history and 
      *  resampling filters are retained. This method should be used when 
      *  calculating a %PSD estimate of subsequent segments of a data stream.
      *  \brief Reset the %WelchPSD accumulator.
      */
    void reset_accumulators( void );

    /**  Reset the input signal history and signal conditioning filters. This 
      *  method should be used for a global reset (i.e. in reset()).
      *  \brief Reset the history and signal conditioning states.
      */
    void reset_history( void );

    /**  Set the desired sample rate. The input signal will be resampled to
      *  this rate if it has a higher sample rate.
      *  \note The sample rate is useful in setting the %PSD frequency 
      *        band (\f$ 0 <= f <= f_{Ny} = 0.5 \times rate\f$).
      *  \note The current resampling implementation will only reduce sample 
      *        rates by a power of two. If the specified sample rate differs 
      *        from the input signal sample rate by other than a power of two,
      *        the add() method will throw an exception.
      *  \brief Set the sample rate.
      *  \param rate Sample rate in Hz.
      */
    void set_rate( double rate );

    /**  Set up for a standard Welch estimate with the given stride length.
      *  This method uses a Hamming window and 50% overlap. The stride 
      *  length considerations are discussed for set_stride(). If the
      *  stride is set to zero, the length of the first data series passed 
      *  to add() will be used instead.
      *  \brief Set parameters for Welch's method.
      *  \param stride Data stride length.
      */
    void set_welch( Interval stride );

    /**  Print a status description to the specified stream.
      *  \brief print status
      *  \param out output stream to receive the status info.
      */
    void status( std::ostream& out = std::cout ) const;

    //--------------------------------------  Private data manipulation methods
private:
    /**  Resample the specified data stream and add it to the specified 
      *  history series. If the resampling has not been set up yet, the
      *  necessary decimation filter is constructed and returned to \a decim.
      *  \exception std::runtime_error The resampling filter cannot be 
      *  constructed or the resampled data can not be appended to the end 
      *  of the history series (TSeries::Append).
      *  \brief Resample input data.
      */
    void resample( auto_pipe& decim, const TSeries& in, TSeries& hist );

private:
    double          mSampleRate;
    auto_pipe       mXDecim;
    TSeries         mXHistory;
    long            mNAverage;
    containers::PSD mXXSum;
};

//======================================  In-line methods
inline void
WelchPSD::set_welch( Interval stride )
{
    set_defaults( stride );
}

inline const char*
WelchPSD::get_type( void ) const
{
    return "welch";
}

#endif // !defined(WELCHPSD_HH)
