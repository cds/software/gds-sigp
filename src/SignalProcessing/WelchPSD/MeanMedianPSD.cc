/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "MeanMedianPSD.hh"
#include "Hanning.hh"
#include "resampler.hh"
#include "fSeries/DFT.hh"
#include "DVecType.hh"
#include <stdexcept>
#include <iostream>

using namespace std;

//======================================  Default constructor.
MeanMedianPSD::MeanMedianPSD( void ) : psd_estimate( 1.0 ), mSampleRate( 0.0 )
{
    set_window( Hanning( ) );
}

//======================================  Data constructor.
MeanMedianPSD::MeanMedianPSD( Interval stride, size_t nAvg, double sample_rate )
    : psd_estimate( stride, 0 )
{
    set_window( Hanning( ) );
    if ( nAvg )
        set_averages( nAvg );
    set_rate( sample_rate );
}

//======================================  Destructor.
MeanMedianPSD::~MeanMedianPSD( void )
{
}

//======================================  Destructor.
MeanMedianPSD*
MeanMedianPSD::clone( void ) const
{
    return new MeanMedianPSD( *this );
}

//======================================  Add one or more strides to the
//                                        accumulated coherence
void
MeanMedianPSD::add( const TSeries& x )
{
    Interval tOverlap = mStride * ( 1.0 - mOverlap );

    //------------------------------------  Set up to resample as appropriate
    if ( mSampleRate == 0 )
    {
        if ( x.getTStep( ) == Interval( 0.0 ) )
        {
            throw runtime_error( "MeanMedianPSD: Invalid sample rate. " );
        }
        else
        {
            mSampleRate = 1.0 / x.getTStep( );
        }
    }

    //------------------------------------ Resample the input and append
    resample( x, mXHistory );

    //------------------------------------  Record first data start time.
    if ( !mStartTime )
    {
        mStartTime = mXHistory.getStartTime( );
        mCurrent = mStartTime;

        if ( !mEvenMedian.size( ) || !mOddMedian.size( ) )
        {
            set_averages( nstride( mXHistory.getInterval( ) ) );
        }
    }

    //------------------------------------  Loop over overlapping strides.
    while ( mXHistory.getInterval( ) >= mStride )
    {
        containers::DFT xDft(
            mWindow( mXHistory.extract( mCurrent, mStride ) ) );
        containers::PSD xPsd( xDft );
        const DVectD& dvd( dynamic_cast< const DVectD& >( xPsd.refDVect( ) ) );

        int iStride( ( mCurrent - mStartTime ) / tOverlap + 0.5 );
        if ( iStride % 2 == 0 )
            mEvenMedian.collect( dvd );
        else
            mOddMedian.collect( dvd );

        //----------------------------------  Advance history and current time.
        mXHistory.eraseStart( tOverlap );
        mCurrent += tOverlap;
    }
}

//======================================  Calculate median bias factor.
double
MeanMedianPSD::medianbiasfactor( int n )
{
    // ---- Verify that n is a positive, odd, integer scalar.
    // if (n < 0 || n%2 != 1)
    if ( n <= 0 )
    {
        throw runtime_error(
            "medianbiasfactor: argument not a positive, odd integer." );
    }

    // ---- Compute bias factor alpha.
    double alpha = 0;
    double toggle = 1.0;
    for ( int ii = 1; ii <= n; ++ii )
    {
        alpha += toggle / double( ii );
        toggle = -toggle;
    }
    return alpha;
}

//======================================  Get the PSD from the accumulator
containers::PSD
MeanMedianPSD::get_psd( void ) const
{
    DVectD dvd;
    size_t Ns_odd = mOddMedian.last( );
    size_t Ns_even = mEvenMedian.last( );
    double nTotal( Ns_even + Ns_odd );
    mOddMedian.interpolate( 0.5, dvd );
    if ( Ns_even )
    {
        DVectD dvd_even;
        mEvenMedian.interpolate( 0.5, dvd_even );
        // ----  Weighted average of two medians.
        dvd *= double( Ns_odd ) / ( medianbiasfactor( Ns_odd ) * nTotal );
        dvd_even *=
            double( Ns_even ) / ( medianbiasfactor( Ns_even ) * nTotal );
        dvd += dvd_even;
    }
    else
    {
        dvd *= 1.0 / medianbiasfactor( Ns_odd );
    }

    //-----------------------------  Construct PSD to return to caller.
    Interval        dT = 0.5 * nTotal * mStride;
    double          dF = 1.0 / double( mStride );
    containers::PSD ret_psd;
    static_cast< containers::fSeries& >( ret_psd ) =
        containers::fSeries( 0, dF, mCurrent - dT, dT, dvd );
    return ret_psd;
}

//======================================  Resample data and append it to the
//                                        input history series.
void
MeanMedianPSD::resample( const TSeries& in, TSeries& hist )
{
    TSeries resin( mXDecim( in ) );
    int     rc = hist.Append( resin );
    if ( rc )
    {
        cerr << "TSeries::Append returned rc=" << rc
             << " tStep=" << hist.getTStep( )
             << " end=" << hist.getEndTime( ).getS( ) << endl;
        throw runtime_error( "MeanMedianPSD: Invalid input data." );
    }
}

//======================================  Reset accumulators and history
void
MeanMedianPSD::reset_history( void )
{
    mXHistory.Clear( );
}

//======================================  Reset accumulators and history
void
MeanMedianPSD::reset_accumulators( void )
{
    mEvenMedian.reset( );
    mOddMedian.reset( );
}

//======================================  Set number of averages
void
MeanMedianPSD::set_averages( size_t nAvgs )
{
    if ( nAvgs < 4 )
    {
        throw runtime_error( "MeanMedianPSD: invalid number of averages" );
    }
    size_t nOdd = ( nAvgs + 1 ) / 2;
    mOddMedian.set_stride( nOdd );
    mEvenMedian.set_stride( nAvgs - nOdd );
}
//======================================  Set the sample rate
void
MeanMedianPSD::set_rate( double rate )
{
    mSampleRate = rate;
    if ( rate != 0 )
        mXDecim.set( new resampler( rate ) );
}

//======================================  Print status information
void
MeanMedianPSD::status( std::ostream& out ) const
{
    out << "MeanMedianPSD status: " << endl;
    out << "  stride:           " << mStride << endl;
    out << "  overlap:          " << mOverlap << endl;
    out << "  sample rate:      " << mSampleRate << endl;
    out << "  window:           " << window_type( mWindow.get( ) ) << endl;
    out << "  even vector size: " << mEvenMedian.size( ) << endl;
    out << "  odd vector size:  " << mOddMedian.size( ) << endl;
    out << "  start time:       " << mStartTime << endl;
    out << "  current time:     " << mCurrent << endl;
}
