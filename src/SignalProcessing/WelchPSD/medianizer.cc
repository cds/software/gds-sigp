/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "medianizer.hh"
#include "DVecType.hh"
#include <stdexcept>
#include <iostream>

using namespace std;

//======================================  Constructor
medianizer::medianizer( size_t stride, double frac )
    : _length( 0 ), _sequence( 0 )
{
    set_stride( stride );
    set_fraction( frac );
}

//======================================  Destructor
medianizer::~medianizer( void )
{
}

//======================================  Interpolate fraction of sorted values
void
medianizer::collect( const DVector& in )
{
    if ( !_sequence )
    {
        set_length( in.size( ) );
    }
    else if ( _length != in.size( ) )
    {
        throw runtime_error( "medianizer: length error" );
    }
    size_t phase = _sequence % _stride;
    if ( _sequence < _stride )
    {
        _input[ phase ].reset( in.clone( ) );
    }
    DVectD out( *_input[ phase ] );
    *_input[ phase ] = in;
    for ( size_t col = 0; col < _length; col++ )
    {
        double in_i = in.getDouble( col );
        double out_i = out.getDouble( col );
        replace( col, out_i, in_i );
    }
    _sequence++;
}

//======================================  Interpolate fraction of sorted values
void
medianizer::interpolate( double fraction, DVector& out ) const
{
    if ( fraction < 0 || fraction > 1 )
    {
        throw runtime_error( "medianizer: invalid fractional value." );
    }
    size_t N = ( _sequence < _stride ) ? _sequence : _stride;
    if ( !N || !_length )
        throw runtime_error( "medianizer: no history data." );

    DVectD dvd( _length );
    double dfrac = ( N - 1 ) * fraction;
    size_t index = dfrac;
    if ( index == dfrac )
    {
        for ( size_t col = 0; col < _length; col++ )
        {
            dvd[ col ] = _sorted[ col * _stride + index ];
        }
    }
    else
    {
        dfrac -= index;
        for ( size_t col = 0; col < _length; col++ )
        {
            dvd[ col ] = _sorted[ col * _stride + index ] * ( 1 - dfrac ) +
                _sorted[ col * _stride + index + 1 ] * dfrac;
        }
    }
    out = dvd;
}

//======================================  get it, return the median
void
medianizer::next( const DVector& in, DVector& out )
{
    collect( in );
    interpolate( _fraction, out );
}

//======================================  find the first element >= x in
//                                        a sorted vector of length N
inline size_t
find_first_ge( double x, const double* vec, size_t N )
{
    size_t lo = 0; // index most recent < x
    size_t hi = N;
    if ( x <= vec[ lo ] )
        return lo;
    for ( size_t mid = N / 2; lo + 1 < hi; mid = ( lo + hi ) / 2 )
    {
        if ( vec[ mid ] < x )
            lo = mid;
        else
            hi = mid;
    }
    return hi;
}

//======================================  replace specified values
void
medianizer::replace( size_t col, double out, double in )
{
    size_t  iout;
    double  col0 = col * _stride;
    double* sortcol = &( _sorted[ col0 ] );

    //-----------------------------------  Find the position of the "out" sample
    size_t N = ( _sequence < _stride ) ? _sequence : _stride;
    if ( !N )
    {
        sortcol[ 0 ] = in;
        return;
    }

    //-----------------------------------  Fewer samples than needed
    else if ( N < _stride )
    {
        iout = N;
    }

    //-----------------------------------  Find the sample in the list
    else
    {
        iout = find_first_ge( out, sortcol, N );
        if ( iout == N )
            iout--;
        if ( out != sortcol[ iout ] )
        {
            cerr << "No 'out' sample. out = " << out
                 << " closest = " << sortcol[ iout ] << endl;
            cerr << "search failure. col=" << col << " _stride = " << _stride
                 << " N = " << N << " _sequence = " << _sequence << endl;
            cerr << " out       = " << out << endl;
            for ( size_t i = 0; i < N; i++ )
            {
                cerr << " sorted[" << i << "] = " << sortcol[ i ] << endl;
            }
            throw runtime_error(
                "medianizer: Can not find sample to be replaced" );
        }
    }

    //------------------------------------  Find the first lesser sample
    while ( iout > 0 && sortcol[ iout - 1 ] > in )
    {
        sortcol[ iout ] = sortcol[ iout - 1 ];
        iout--;
    }

    //------------------------------------  Find the First greater
    while ( iout < N - 1 && sortcol[ iout + 1 ] < in )
    {
        sortcol[ iout ] = sortcol[ iout + 1 ];
        iout++;
    }
    sortcol[ iout ] = in;
}

//======================================  reset history.
void
medianizer::reset( void )
{
    _sequence = 0;
    _length = 0;
    _input.clear( );
    _sorted.clear( );
}

//======================================  set fraction
void
medianizer::set_fraction( double f )
{
    _fraction = f;
}

void
medianizer::set_length( size_t l )
{
    if ( !_stride )
        throw runtime_error( "medianizer: stride is zero" );
    _length = l;
    _input.clear( );
    _input.resize( _stride );
    _sorted.resize( _stride * _length );
}

void
medianizer::set_stride( size_t n )
{
    _stride = n;
}
