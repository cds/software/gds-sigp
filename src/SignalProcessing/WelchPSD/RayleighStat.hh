/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef RAYLEIGHSTAT_HH
#define RAYLEIGHSTAT_HH

#include "psd_estimate.hh"
#include <iosfwd>

class window_api;

/**  The %RayleighStat class calculates the ratio of the rms to the mean 
  *  spectral density of a digitized signal. This is normalized to the 
  *  expected value for gaussian noise, \e i.e. gaussian noise should have
  *  a ratio of 1.0 and a pure sine wave would have a ratio of zero. The 
  *  data are accumulated using the add() method and a history is maintained
  *  to allow overlapping.
  *  \brief Calculate the Rayleigh statistic for a stretch of data.
  *  \author John Zweizig (john.zweizig@ligo.org)
  */
class RayleighStat : public psd_estimate
{
public:
    /**  Construct an uninitialized %RayleighStat object. If no stride, window, 
      *  overlap or sample rate are specified, these parameters will be 
      *  inferred from the first data as follows:
      *  - \e Stride: Length of a single fft. By default the stride length 
      *               will be the same as the input series length.
      *  - \e Window: Window applied to TSeries data before the fft calculation.
      *               By default, no windowing will be performed.
      *  - \e Overlap: Overlap of successive strides. By default, no 
      *                overlapping will be performed, i.e. overlap=0.
      *  - \e Sample-Rate: Output rate of preliminary resampling step. By 
      *                default, the input series will not be resampled.
      *  \brief Default constructor.
      */
    RayleighStat( void );

    /**  Construct a %RayleighStat instance and set the stride, overlap, 
      *  window and sample rate parameters. If any or all of the parameters 
      *  are set to zero or left unspecified, these values will take the 
      *  default values as specified in the default %RayleighStat constructor.
      *  \brief Construct and initialize a %RayleighStat instance.
      *  \param stride  Length of an analysis stride. (see set_stride())
      *  \param overlap Fractional overlap of the  strides (see set_overlap()).
      *  \param win     Window instance (see set_window()).
      *  \param sample_rate Sample rate (see set_rate())
      */
    explicit RayleighStat( Interval          stride,
                           double            overlap = 0.0,
                           const window_api* win = 0,
                           double            sample_rate = 0 );

    /**  Destroy the %RayleighStat object and release all current storage.
      *  \brief  Destructor.
      */
    virtual ~RayleighStat( void );

    /** Clone the psd_estimate instance.
     *  \brief Clone a psd_estimate.
     *  \return pointer to copy of a psd_estimate.
     */
    RayleighStat* clone( void ) const;

    /**  Accumulate all the information needed to calculate the %Welch %PSD
      *  estimate from one or more strides. The input series is resampled 
      *  as appropriate and added to the input signal history series. If 
      *  sufficient data are available to calculate one or more strides, the 
      *  data segment(s) are windowed and a discrete Fourier transform is made.
      *  The resulting DFT is squared, and added to the accumulated %PSD.
      *  The normalization is as defined for the PSD class.
      *  \brief Add one or more data strides to the %RayleighStat data accumulator.
      *
      *  \exception std::runtime_error is thrown if the the sample rate of
      *  the input series can not be converted to the specified rate,
      *  or if the series start time does not match the end of the series
      *  history data.
      *  \param x %Time series containing the digital signal data.
      */
    void add( const TSeries& x );

    /**  Calculate the power spectral density (psd) estimate from the 
      *  accumulated data and return it in a PSD class instance.
      *  \brief Calculate the %PSD from the accumulated data.
      *  \note The data accumulator is not reset. The summed data used to 
      *  calculate the psd will be included in the next get_psd() calculation 
      *  unless the accumulator is explicitly cleared with reset_accumulator() 
      *  or reset().
      *  \return Power spectral density estimate in a PSD.
      */
    containers::PSD get_psd( void ) const;

    /**  Get the estimator type string, "rayleigh";
      *  \brief Type string
      *  \return type string
      */
    const char* get_type( void ) const;

    /**  Calculate the normalized ratio of the measured rms to the psd 
      *  (\e i.e. the Rayleigh statistic) from the accumulated data and 
      *  return it in a containers::PSD instance.
      *  \brief Calculate the Rayleigh statistic from the accumulated data.
      *  \note The data accumulator is not reset. The summed data used to 
      *  calculate the rayleigh statistic will be included in the next 
      *  get_rstat() calculation unless the accumulator is explicitly 
      *  cleared with reset_accumulator() or reset().
      *  \return Rayleigh statistic estimate in a PSD.
      */
    containers::PSD get_rstat( void ) const;

    /**  Calculate the Rayleigh estimate from an input time series containing 
      *  one or more data segments. The Rayleigh statistic is calculated using 
      *  the parameters (stride length, overlap, sample rate and window 
      *  function) specified in the constructor or with the set functions. The 
      *  accumulators are reset before the calculation, but the history data 
      *  are left intact.
      *  This leaves the %RayleighStat estimator object ready to continue 
      *  processing the next stride of a data stream.
      *  \exception std::runtime_error is thrown if the sample rate of the
      *  input series can not be converted to the specified rate,
      *  or if the series start time does not match the end of the series
      *  history data.
      *  \brief Single step Rayleigh Statistic calculation.
      *  \param x Time series containing digitized signal data.
      *  \return Rayleigh statu\istic returned in a PSD.
      */
    containers::PSD operator( )( const TSeries& x );

    /**  Reset the %RayleighStat accumulators and stride count. The history and 
      *  resampling filters are retained. This method should be used when 
      *  calculating a %PSD estimate of subsequent segments of a data stream.
      *  \brief Reset the %RayleighStat accumulator.
      */
    void reset_accumulators( void );

    /**  Reset the input signal history and signal conditioning filters. This 
      *  method should be used for a global reset (i.e. in reset()).
      *  \brief Reset the history and signal conditioning states.
      */
    void reset_history( void );

    /**  Set the desired sample rate. The input signal will be resampled to
      *  this rate if it has a higher sample rate.
      *  \note The sample rate is useful in setting the %PSD frequency 
      *        band (\f$ 0 <= f <= f_{Ny} = 0.5 \times rate\f$).
      *  \note The current resampling implementation will only reduce sample 
      *        rates by a power of two. If the specified sample rate differs 
      *        from the input signal sample rate by other than a power of two,
      *        the add() method will throw an exception.
      *  \brief Set the sample rate.
      *  \param rate Sample rate in Hz.
      */
    void set_rate( double rate );

    /**  Print a status description to the specified stream.
      *  \brief print status
      *  \param out output stream to receive the status info.
      */
    void status( std::ostream& out = std::cout ) const;

    //--------------------------------------  Private data manipulation methods
private:
    /**  Resample the specified data stream and add it to the specified 
      *  history series. If the resampling has not been set up yet, the
      *  necessary decimation filter is constructed and returned to \a decim.
      *  \exception std::runtime_error The resampling filter cannot be 
      *  constructed or the resampled data can not be appended to the end 
      *  of the history series (TSeries::Append).
      *  \brief Resample input data.
      */
    void resample( auto_pipe& decim, const TSeries& in, TSeries& hist );

private:
    double          mSampleRate;
    auto_pipe       mXDecim;
    TSeries         mXHistory;
    long            mNAverage;
    containers::PSD mXXSum;
    containers::PSD mXXSumSq;
};

inline const char*
RayleighStat::get_type( void ) const
{
    return "rayleigh";
}

#endif // !defined(RAYLEIGHSTAT_HH)
