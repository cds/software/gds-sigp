/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef MEANMEDIANPSD_HH
#define MEANMEDIANPSD_HH

#include "psd_estimate.hh"
#include "medianizer.hh"

class window_api;

/**  The %MeanMedianPSD class calculates the power spectral density of a
  *  digitized signal using a mean-median estomator. It finds the medians 
  *  of two sets of PSDs formed from alternating 50% overlap Hann windowed 
  *  strides of an input time series. The length of the individual strides 
  *  (the fft length) default to 1s but can be set to any length with the 
  *  set_stride() method. The number of segments used to calculate the 
  *  medians can be specified with the set_averages() method or can be 
  *  inferred from the size of the input time series. An arbitrary number 
  *  of segments may be added into the median list with the add() 
  *  method. The resulting PSD is calculated by the get_psd() method or
  *  by the operator()() method.
  *  \brief Calculate mean-median estimator of the power spectral density.
  *  \author John Zweizig (john.zweizig@ligo.org)
  */
class MeanMedianPSD : public psd_estimate
{
public:
    /**  Construct an uninitialized %MeanMedianPSD object. If no stride, 
     *  window, overlap or sample rate are specified, these parameters will
     *  be inferred from the first data as follows:
     *  - \e Stride: length will be 1s.
     *  - \e Window: a Hann window will be used.
     *  - \e Sample-Rate: The input series will not be resampled..
     *  \brief Default constructor.
     */
    MeanMedianPSD( void );

    /**  Construct a %MeanMedianPSD and set the stride, overlap, window and
     *  sample rate parameters. If any or all of the parameters are set 
     *  to zero or left unspecified, these values will take the default
     *  values as specified in the default (MeanMedianPSD()) constructor.
     *  \brief Construct and initialize a %MeanMedianPSD object.
     *  \param stride  Length of an analysis stride. (see set_stride())
     *  \param nAvg    total number of median list entries.
     *  \param sample_rate Sample rate (see set_rate())
     */
    explicit MeanMedianPSD( Interval stride,
                            size_t   nAvg = 0,
                            double   sample_rate = 0 );

    /**  Destroy the %MeanMedianPSD object and release all current storage.
     *  \brief  Destructor.
     */
    virtual ~MeanMedianPSD( void );

    /** Clone the psd_estimate instance.
     *  \brief Clone a psd_estimate.
     *  \return pointer to copy of a psd_estimate.
     */
    MeanMedianPSD* clone( void ) const;

    /**  Accumulate all the information needed to calculate the %Welch %PSD
     *  estimate from one or more strides. The input series is resampled 
     *  as appropriate and added to the input signal history series. If 
     *  sufficient data are available to calculate one or more strides, the 
     *  data segment(s) are windowed and a discrete Fourier transform is made.
     *  The resulting DFT is squared, and added to the accumulated %PSD.
     *  The normalization is as defined for the PSD class.
     *  \brief Add data stride(s) to the %MeanMedianPSD data accumulator.
     *
     *  \exception std::runtime_error is thrown if the the sample rate of
     *  the input series can not be converted to the specified rate,
     *  or if the series start time does not match the end of the series
     *  history data.
     *  \param x %Time series containing the digital signal data.
     */
    void add( const TSeries& x );

    /**  Calculate the %Welch %PSD estimator from the accumulated data and 
     *  return it in a PSD class instance.
     *  \brief Calculate the %PSD from the accumulated data.
     *  \note The data accumulator is not reset. The summed data used to 
     *  calculate the psd will be included in the next get_psd() calculation 
     *  unless the accumulator is explicitly cleared with reset_accumulator() 
     *  or reset().
     *  \return %Welch power spectral density estimate in a PSD.
     */
    containers::PSD get_psd( void ) const;

    /**  Get the estimator type string, "mean-median";
      *  \brief Type string
      *  \return type string
      */
    const char* get_type( void ) const;

    /**  Reset the %MeanMedianPSD accumulator and stride count. The history 
     *  and resampling filters are retained. This method should be used when 
     *  calculating a %PSD estimate of subsequent segments of a data stream.
     *  \brief Reset the %MeanMedianPSD accumulator.
     */
    void reset_accumulators( void );

    /**  Reset the input signal history and signal conditioning filters. This 
      *  method should be used for a global reset (i.e. in reset()).
      *  \brief Reset the history and signal conditioning states.
      */
    void reset_history( void );

    /**  Set the total number of elements in the even and odd stride lists. 
     *  The even and odd averages are set to nEven = int(nAverage/2) and 
     *  nOdd = nAverage - nEven.
     *  \brief Set total number of elements in the meadian lists. 
     *  \param nAverage Fraction of stride to be retained.
     */
    void set_averages( size_t nAverage );

    /**  Set the desired sample rate. The input signal will be resampled to
     *  this rate if it has a higher sample rate.
     *  \note The sample rate is useful in setting the %PSD frequency 
     *        band (\f$ 0 <= f <= f_{Ny} = 0.5 \times rate\f$).
     *  \note The current resampling implementation will only reduce sample 
     *        rates by a power of two. If the specified sample rate differs 
     *        from the input signal sample rate by other than a power of two,
     *        the add() method will throw an exception.
     *  \brief Set the sample rate.
     *  \param rate Sample rate in Hz.
     */
    void set_rate( double rate );

    /**  Print a status description to the specified stream.
      *  \brief print status
      *  \param out output stream to receive the status info.
      */
    void status( std::ostream& out = std::cout ) const;

    /**  Compute the factor by which median must be divided to give an
     *  unbiased estimate of the mean, assuming an exponential distribution.
     *  \brief Bias factor for median estimate of mean.
     *  \param n Number of samples used to estimate median.  Must be a
     *           positive, odd integer.  
     *  \retun Median bias factor
     */
    static double medianbiasfactor( int n );

    //--------------------------------------  Private data manipulation methods
private:
    /**  Resample the specified data stream and add it to the specified 
     *  history series. If the resampling has not been set up yet, the
     *  necessary decimation filter is constructed and returned to \a decim.
     *  \exception std::runtime_error The resampling filter cannot be 
     *  constructed or the resampled data can not be appended to the end 
     *  of the history series (TSeries::Append).
     *  \brief Resample input data.
     */
    void resample( const TSeries& in, TSeries& hist );

private:
    double     mSampleRate;
    auto_pipe  mXDecim;
    TSeries    mXHistory;
    medianizer mEvenMedian;
    medianizer mOddMedian;
};

inline const char*
MeanMedianPSD::get_type( void ) const
{
    return "mean-median";
}

#endif // !defined(MEANMEDIANPSD_HH)
