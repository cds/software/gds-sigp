/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef MEDIANIZER_HH
#define MEDIANIZER_HH
#include <vector>
#include <memory>

class DVector;

/**  The medianizer class finds the median (or specified fractional element 
 *  value) of a series of data vectors.
 */
class medianizer
{
public:
    /**  Construct a medianizer. \c stride gives the number of elements 
     *  (DVectors) to be stored averaged and cmputed.
     *  \brief Constructor.
     *  \param stride Length of the history.
     *  \param frac   Fractional number of elements (e.g. 0.5 for median)
     */
    medianizer( size_t stride = 0, double frac = 0.5 );

    /**  Destroy this medianizer instance.
     *  \brief destructor.
     */
    virtual ~medianizer( void );

    /**  Add a data vector to the history list and insert the contents of each
     *  vector element into the corresponding sorted list. No interpolation is
     *  performed if the maximum number of history vectors has already been 
     *  added, the oldest remaining vector is replaced.
     *  \brief Add a vector to the history and the sorted lists.
     *  \param in Vector to be added to the medianizer history.
     */
    void collect( const DVector& in );

    /**  Calculate the specified fractional limit for the data vectors currently
     *  in the history list.
     *  \brief find the value of a specified fractional position
     *  \param fraction position at which the vector is to be calculated.
     *  \param out %DVector reference to receive the interpolated value.
     */
    void interpolate( double fraction, DVector& out ) const;

    /**  Current number of elements in sorted lists. This number has a range 
     *  of zero to the stride.
     *  \brief Number of elements over which the median will be calculated.
     *  \return Number of elements.
     */
    size_t last( void ) const;

    /**  Replace the least recent vector in the history list with the in 
     *  vector. Then recalculate the fraction limit from the new sorted 
     *  history list.
     *  \brief Add a vector to the history list and return the fractional 
     *         limit vector.
     *  \param in  Vector to be added to the history list.
     *  \param out Vector receive the fraactional limit vector.
     */
    void next( const DVector& in, DVector& out );

    /**  Reset the history. All stored data vectors are deleted and the sequence
     *  number is reset to zero.
     *  \brief Reset the history storage. 
     */
    void reset( void );

    /**  Set the fractional limit to be used for calculations.
     *  \brief Set the fraactional limit.
     *  \param f Fraction.
     */
    void set_fraction( double f );

    /**  Set the number of vectors stored in the history list.
     */
    void set_stride( size_t n );

    /**  Get the number of vectors stored in the history list.
     */
    size_t size( void ) const;

private:
    /**  Replace a value with another one in the sorted list for the specified 
     *  column.
     *  \brief replace a value in the sorted list.
     *  \param col column number
     *  \param out Value to be replaced in sorted list
     *  \param in  New value to be enered into sorted list.
     */
    void replace( size_t col, double out, double in );

    /**  Set the length of the vectors to be stored/calculated.
     *  \brief Set vector length.
     *  \param l length.
     */
    void set_length( size_t l );

#ifndef __CINT__
private:
    size_t                                    _stride;
    double                                    _fraction;
    size_t                                    _length;
    size_t                                    _sequence;
    std::vector< std::shared_ptr< DVector > > _input;
    std::vector< double >                     _sorted;
#endif
};

//======================================  inline methods
#ifndef __CINT__
inline size_t
medianizer::last( void ) const
{
    return ( _sequence < _stride ) ? _sequence : _stride;
}

inline size_t
medianizer::size( void ) const
{
    return _stride;
}
#endif

#endif // !defined(MEDIANIZER_HH)
