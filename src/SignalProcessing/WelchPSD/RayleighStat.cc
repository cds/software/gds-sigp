/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "RayleighStat.hh"
#include "Hamming.hh"
#include "DecimateBy2.hh"
#include "fSeries/DFT.hh"
#include "DVecType.hh"
#include <bit>
#include <stdexcept>
#include <iostream>

using namespace std;

//======================================  Default constructor.
RayleighStat::RayleighStat( void ) : mSampleRate( 0 ), mNAverage( 0 )
{
}

//======================================  Data constructor.
RayleighStat::RayleighStat( Interval          stride,
                            double            overlap,
                            const window_api* w,
                            double            sample_rate )
    : psd_estimate( stride, overlap, w ), mSampleRate( sample_rate ),
      mNAverage( 0 )
{
}

//======================================  Destructor.
RayleighStat::~RayleighStat( void )
{
}

//======================================  Destructor.
RayleighStat*
RayleighStat::clone( void ) const
{
    return new RayleighStat( *this );
}

//======================================  Add one or more strides to the
//                                        accumulated coherence
void
RayleighStat::add( const TSeries& x )
{

    //------------------------------------  Calculate the stride.
    if ( mStride == Interval( 0 ) )
    {
        set_stride( x, 1.0 );
    }

    //------------------------------------  Set up to resample as appropriate
    if ( mSampleRate == 0 )
    {
        if ( x.getTStep( ) == Interval( 0.0 ) )
        {
            throw runtime_error( "RayleighStat: Invalid sample rate. " );
        }
        else
        {
            mSampleRate = 1.0 / x.getTStep( );
        }
    }

    //------------------------------------ Resample the input and append
    resample( mXDecim, x, mXHistory );

    //------------------------------------  Record first data start time.
    Time startHist = mXHistory.getStartTime( );
    if ( !mStartTime )
    {
        mStartTime = startHist;
        mCurrent = startHist;
    }

    //------------------------------------  OK if some data were skipped
    else if ( startHist > mCurrent )
    {
        mCurrent = startHist;
    }

    //------------------------------------  Loop over overlapping strides.
    while ( mXHistory.getEndTime( ) >= mCurrent + mStride )
    {
        TSeries xSeg = mXHistory.extract( mCurrent, mStride );
        xSeg.Convert( DVector::t_double );
        containers::DFT xDft( mWindow( xSeg ) );

        //-------------------------------- First time - set accumulator.
        containers::PSD psd_inc = containers::PSD( xDft );
        containers::PSD psd_ssq = psd_inc;
        psd_ssq *= psd_inc;
        if ( mXXSum.empty( ) )
        {
            mXXSum = psd_inc;
            mXXSumSq = psd_ssq;
        }

        //------------------------------  Subsequently - Add to accumulator.
        else
        {
            mXXSum += psd_inc;
            mXXSumSq += psd_ssq;
        }
        mNAverage++;

        //----------------------------------  Advance history and current time.
        Interval DtErase = mStride * ( 1.0 - mOverlap );
        mCurrent += DtErase;
        mXHistory.eraseStart( mCurrent - mXHistory.getStartTime( ) );
    }
}

//======================================  Get the PSD from the accumulator
containers::PSD
RayleighStat::get_psd( void ) const
{
    if ( !mNAverage || mNAverage == 1 )
        return mXXSum;

    //----------------------------------  fill a PSD with the CSD modsq.
    containers::PSD r( mXXSum );
    r *= 1.0 / double( mNAverage );
    return r;
}

//======================================  Get the PSD from the accumulator
containers::PSD
RayleighStat::get_rstat( void ) const
{
    if ( mNAverage < 2 )
    {
        throw runtime_error( "RayleighStat: insufficient data to "
                             "calculate Rayleigh statistic" );
    }

    //--------------------------------  Set normalization for Rayleigh
    //                                  statistic using approximations:
    double N( mNAverage );
    double logN = log10( N );
    double expected_raymean = 1.0 - 0.6 * pow( N, -0.9 );
    double expected_rayvar =
        pow( 10.0, -0.35 - 0.2125 * logN - 0.0625 * logN * logN ) /
        expected_raymean; //normalized for expected mean

    //----------------------------------  fill a PSD with the CSD modsq.
    containers::PSD var( mXXSumSq );
    var *= 1.0 / double( mNAverage );
    containers::PSD meanPSD = get_psd( );
    containers::PSD meanSq = meanPSD;
    meanSq *= meanPSD;
    var -= meanSq;
    containers::PSD r( var );
    double          normal = 1.0 / expected_raymean;
    DVectD&         rv = dynamic_cast< DVectD& >( r.refDVect( ) );
    for ( size_t i = 0; i < rv.size( ); i++ )
    {
        double x_i = rv[ i ];
        if ( x_i > 0 )
            rv[ i ] = sqrt( x_i ) * normal;
        else
            rv[ i ] = 0;
    }
    r /= meanPSD;
    return r;
}

//======================================  All in one coherence calculation.
containers::PSD
RayleighStat::operator( )( const TSeries& x )
{
    reset_accumulators( );
    add( x );
    return get_rstat( );
}

//======================================  Resample data and append it to the
//                                        input history series.
void
RayleighStat::resample( auto_pipe& decim, const TSeries& in, TSeries& hist )
{
    //cout << "resample: rate=" << mSampleRate << " tStep=" << in.getTStep()
    // 	   << " start=" << in.getStartTime().getS() << endl;

    //------------------------------------  No resampling necessary
    TSeries decim_ts;
    if ( fabs( mSampleRate * double( in.getTStep( ) ) - 1.0 ) < 1e-6 )
    {
        decim_ts = in;
    }

    //------------------------------------  Set up resampling?
    else
    {
        if ( !mStartTime )
        {
            int resample =
                int( 1.0 / double( in.getTStep( ) * mSampleRate ) + 0.5 );
            if ( resample < 2 || !std::__has_single_bit( resample ) )
                throw runtime_error( "RayleighStat: Invalid resample request" );
            int N = 0;
            while ( resample > 1 )
            {
                resample /= 2;
                N++;
            }
            decim.set( new DecimateBy2( N, 1 ) );
        }

        //------------------------------------  Resample
        if ( decim.null( ) )
            throw runtime_error( "RayleighStat: Resampling misconfigured." );
        decim_ts = decim( in );
    }

    //----------------------------------  Add decimated time series to history
    if ( hist.empty( ) || hist.getEndTime( ) < decim_ts.getStartTime( ) )
    {
        hist = decim_ts;
    }
    else
    {
        int rc = hist.Append( decim_ts );
        if ( rc )
        {
            cerr << "TSeries::Append returned rc=" << rc
                 << " tStep=" << hist.getTStep( )
                 << " end=" << hist.getEndTime( ).getS( ) << endl;
            throw runtime_error( "RayleighStat: Invalid input data." );
        }
    }
}

//======================================  Reset accumulators
void
RayleighStat::reset_accumulators( void )
{
    mXXSum.clear( );
    mXXSumSq.clear( );
    mNAverage = 0;
}

//======================================  Reset history
void
RayleighStat::reset_history( void )
{
    mXHistory.Clear( Time( 0 ) );
    mXDecim.set( 0 );
}

//======================================  Set the sample rate
void
RayleighStat::set_rate( double rate )
{
    mSampleRate = rate;
}

//======================================  Print status information
void
RayleighStat::status( std::ostream& out ) const
{
    out << "RayleighStat status: " << endl;
    out << "  stride:        " << mStride << endl;
    out << "  overlap:       " << mOverlap << endl;
    out << "  sample rate:   " << mSampleRate << endl;
    out << "  window:        " << window_type( mWindow.get( ) ) << endl;
    out << "  start time:    " << mStartTime << endl;
    out << "  current time:  " << mCurrent << endl;
    out << "  # of averages: " << mNAverage << endl;
}
