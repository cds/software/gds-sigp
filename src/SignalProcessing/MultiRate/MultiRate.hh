#ifndef _LIGO_MULTIRATE_H
#define _LIGO_MULTIRATE_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: MultiRate						*/
/*                                                         		*/
/* Module Description: Sample rate conversion				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 20Jul02  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: MultiRate.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include "Complex.hh"
#include "Pipe.hh"
#include <vector>
#include <iosfwd>

class FSeries;

/** Resampling filter. This filter can be used for data rate 
    conversion. It implements a polyphase filter using the
    following formula:
    \f[
    y(m) = \sum_{n=0}^{K-1} g(n,m-[m/u]*u) * x([m*d/u]-n)
    \f]
    The decimation/interpolation filter is a FIR filter designed
    with the Kaiser window method. Its corner frequency is at
    0.9 of the Nyquist frequency of the output series (when the
    sampling rate gets overall smaller) or of the input series
    (if the sampling rate gets larger).

    The stop band attenuation can be specified; default is 80dB. 
    Since this filter has equal stop-band and pass band ripple, the 
    pass-band ripple can be calculated from the stop-band attenuation
    with \f$ripple = 10^{-attenuation/20}\f$, where the attenuation
    is given in dB.

    If the overall down sampling factor is large, it is better to
    decimate in steps. Since this algorithm implements a set of 
    polyphase filters, a large  sampling mismatch -- like if one 
    wants to go from 16384Hz to 10000Hz -- is handled efficiently.
   
    @memo Multi rate filter
    @see J.G. Proakis and D.G. Manolakis, "Digital Signal Processing",
         chap. 10, pp 800.
    @see A.V. Oppenheim and R.W. Schafer, "Discrete-time Signal
         Processing", sect. 4.6, 4.7 and 7.2.
    @author Written July 2002 by Daniel Sigg
    @version 1.0
 ************************************************************************/
class MultiRate : public Pipe
{
public:
    using Pipe::apply;
    using Pipe::dataCheck;

    /// Data type
    enum datatype
    {
        /// float data
        kReal,
        // Complex data
        kComplex
    };

    /** Constructs a multi rate filter.
          @memo Default constructor.
       ******************************************************************/
    MultiRate( );

    /** Constructs a multi rate filter.
          @memo Constructor.
          @param fsample Input sampling frequency
          @param up Interpolation factor
          @param down Decimation factor
          @param atten Stop-band attenuation
       ******************************************************************/
    MultiRate( double fsample, int up, int down, double atten = 80 );

    /** Copy constructor.
        * @memo Copy constructor.
	* @param filter %MultiRate filter to be copied.
       ******************************************************************/
    MultiRate( const MultiRate& filter );

    /** Destructs the multi rate filter.
          @memo Destructor.
       ******************************************************************/
    virtual ~MultiRate( );

    /** Assignment operator.
        * @memo Assignment operator.
	* @param filter %MultiRate filter to be copied
	* @return Reference to modified filter.
       ******************************************************************/
    MultiRate& operator=( const MultiRate& filter );

    /** Create an identical Multirate filter and return a pointer 
          to the new filter. The history is zeroed.
          @memo Clone a MultiRate filter.
          @return pointer to an identical filter.
       ******************************************************************/
    virtual MultiRate*
    clone( ) const
    {
        return new MultiRate( *this );
    }

    /** Test whether the TSeries is valid as input data for the filter.
          a runtime_error exception is thrown if the data are invalid.
          @memo Check input data validity.
	  @param ts TSeries to be checked.
       ******************************************************************/
    virtual void dataCheck( const TSeries& ts ) const;

    /** The TSeries is filtered and the result placed in a new TSeries. 
          The resulting TSeries is left on the stack and must be copied  
          to be made permanent. The filter status flag is set if there 
          are insufficient history entries (less than the Filter order) 
          or if the TSeries start time isn't contiguous with the previous 
          data. The input samples are appended to the filter history as 
          the filter response is calculated.
          @memo   Filter a Time Series.
          @param  in Time series to be filtered.
          @return A new TSeries containing the filter response to the
                  input series.
       ******************************************************************/
    virtual TSeries apply( const TSeries& in );

    /** An N-point float series is filtered from in and stored in out. 
          in may overlap out. The input samples are appended to the 
          filter history as the filter response is calculated.
          @memo   Filter a float array.
          @param  N   Number of elements in the input series.
          @param  in  Float array containing the input series.
          @param  out Float array to contain the filter response. the end  
                      of out may overlap the start of in.
          @return Number of values written to out
       ******************************************************************/
    int apply( int N, const float* in, float* out );

    /** An N-point complex series is filtered from in and stored in out. 
          in may overlap out. The input samples are appended to the 
          filter history as the filter response is calculated.
          @memo   Filter a float array.
          @param  N   Number of elements in the input series.
          @param  in  Float array containing the input series.
          @param  out Float array to contain the filter response. the end 
                      of out may overlap the start of in.
          @return Number of values written to out
       ******************************************************************/
    int apply( int N, const fComplex* in, fComplex* out );

    /** Set the filter to new values.
          @memo Set filter to new values.
          @param fsample Input sampling frequency
          @param up Interpolation factor
          @param down Decimation factor
          @param atten Stop-band attenuation
       ******************************************************************/
    virtual void init( double fsample, int up, int down, double atten = 80 );

    /** Set the filter length and coefficients. The filter history
          buffer is cleared.
          @memo   Set filter coefficients and reset history.
       ******************************************************************/
    virtual void reset( );

    /** Set the filter history.
          @memo   Reset history.
       ******************************************************************/
    virtual void resetHist( );

    /** Tests whether the filter is in use.
          @memo   Test the filter activity status.
          @return true if the filter is being used.
       ******************************************************************/
    virtual bool
    inUse( void ) const
    {
        return fCurTime != Time( 0 );
    }

    /** Get the expected start time of the next TSeries to be filtered.
          @memo   Get the current time.
          @return true The current time.
       ******************************************************************/
    virtual Time
    getCurrentTime( ) const
    {
        return fCurTime;
    }

    /** Get the start time of this filter run. This is set by the first 
          filter operation after the filter has been created or reset.
          @memo   Get the start time.
          @return true The start time.
       ******************************************************************/
    virtual Time
    getStartTime( ) const
    {
        return fStartTime;
    }

    /** Get the interpolation factor.
          @memo Get the interpolation factor.
          @return interpolation factor.
       ******************************************************************/
    virtual int
    getUp( ) const
    {
        return fUp;
    }

    /** Get the decimation factor.
          @memo Get the decimation factor.
          @return decimation factor.
       ******************************************************************/
    virtual int
    getDown( ) const
    {
        return fDn;
    }

    /** Get the stop-band attenuation.
          @memo Get the stop-band attenuation.
          @return stop-band attenuation.
       ******************************************************************/
    virtual double
    getAtten( ) const
    {
        return fAttn;
    }

    /** Get the input sampling frequency.
          @memo Get the input sampling frequency.
          @return input sampling frequency.
       ******************************************************************/
    virtual double
    getSampleIn( ) const
    {
        return fSample;
    }

    /** Get the output sampling frequency.
          @memo Get the output sampling frequency.
          @return output sampling frequency.
       ******************************************************************/
    virtual double
    getSampleOut( ) const
    {
        return fSample * (double)fUp / (double)fDn;
    }

    /** Get the filter order.
          @memo Get the filter order.
          @return filter order.
       ******************************************************************/
    virtual int
    getOrder( ) const
    {
        return fOrder;
    }

    /** Get the history data type.
          @memo Get the history data type.
          @return history data type.
       ******************************************************************/
    virtual datatype
    getHistType( ) const
    {
        return fDType;
    }

protected:
    /** The transfer coefficient of the filter at the specified 
          frequency is calculated and returned as a complex number. 
          @memo Get a transfer coefficient of a Filter.
          @param coeff a complex number representing the Filter response 
                       at the specified frequency (return)
          @param f Frequency at which to sample the transfer function.
          @return true if successful       
       ******************************************************************/
    virtual bool xfer( fComplex& coeff, double f ) const;

    /// allocate history buffer
    void allocHist( int size );
    /// allocate coefficient buffer
    void allocCoeff( int num, int len );

private:
    /// Up-sampling factor
    int fUp;
    /// Down-sampling factor
    int fDn;
    /// Stopband attenuation
    double fAttn;
    /// Order of filter
    int fOrder;
    /// Number of filters
    int fCoeffNum;
    /// Set of polyphase filter coefficients
    double** fCoeffs;
    /// Type of data saved in the history
    datatype fDType;
    ///  Design sample rate
    double fSample;
    ///  Last terms
    void* fLastTerms;
    /// Number of terms processed
    int fTerms;
    ///  Time of next expected filtered sample
    Time fCurTime;
    /// Time of First Sample processed since initialization of filter
    Time fStartTime;
};

/** Designer function for a multi rate filter. The filter type is
    either "abs" or "rel" depending if the required resampling 
    rate is specified relative or absolute. For absolute rate
    specifications the first parameter is the desired sampling 
    rate and the second parameter is the maximally allowed error.
    For a relative rate specification the first parameter is
    the interpolation factor and the second parameter is the 
    decimation factor (both are integer parameters). The third
    parameter is in both cases the required stop band attenuation.

    Throws an invalid_argument exception, if an illegal argument is
    specified.

    @memo MultiRate designer function
    @param type Multirate type
    @param fsample Input sampling frequency
    @param m1 First parameter (desired fS or interpolation factor)
    @param m2 Second parameter (error or decimation factor)
    @param attn Stop-band attenuation
    @return multi rate filter
 ************************************************************************/
MultiRate multirate( const char* type,
                     double      fsample,
                     double      m1,
                     double      m2 = 1E-3,
                     double      atten = 80 );

/** Determine the resampling factors from two integer frequencies.
    @memo Resampling factors
    @param fin  input sampling frequency
    @param fout output sampling frequency
    @param up   interpolation factor (return)
    @param down decimation factor (return)
    @void
 ************************************************************************/
void resampling_factors_int( int fin, int fout, int& up, int& down );

/** Determine the resampling factors from two real frequencies.
    @memo Resampling factors
    @param fin  input sampling frequency
    @param fout output sampling frequency
    @param up   interpolation factor (return)
    @param down decimation factor (return)
    @param maxerr maximally allowed error
    @void
 ************************************************************************/
void resampling_factors(
    double fin, double fout, int& up, int& down, double maxerr = 1E-3 );

#endif // _LIGO_MULTIRATE_H
