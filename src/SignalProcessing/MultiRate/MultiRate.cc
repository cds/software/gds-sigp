#include <time.h>
#include <cmath>
#include <string.h>
#include <strings.h>
#include "MultiRate.hh"
#include "constant.hh"
#include "DVector.hh"
#include "FSeries.hh"
#include <stdexcept>
#include <ctype.h>
#include <inttypes.h>
#include <algorithm>

typedef uint64_t uLong;

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Utilities                                                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
inline double
power( double x, double y )
{
    return exp( log( x ) * y );
}

//______________________________________________________________________________
inline double
sinc( double x )
{
    return ( fabs( x ) < 1E-50 ? 1.0 : sin( fmod( x, twopi ) ) / x );
}

//______________________________________________________________________________
static double
bessel0( double x )
{
    const double eps = 1e-8; // required accuracy
    double       y = x / 2.0;
    double       sum = 1.0;
    double       de = 1.0;
    double       sde = 1.0;
    for ( int i = 1; i <= 25 && sum * eps <= sde; i++ )
    {
        de *= y / double( i );
        sde = de * de;
        sum += sde;
    }
    return sum;
}

//______________________________________________________________________________
static double
kaiser( double x, double beta, double M )
{
    if ( ( x < 0 ) || ( x > M ) )
    {
        return 0.0;
    }
    double alpha = M / 2;
    double s = ( x - alpha ) / alpha;
    double u = beta * sqrt( 1. - s * s );
    return bessel0( u ) / bessel0( beta );
}

//______________________________________________________________________________
static double
coefficient( int    i,
             int    j,
             int    order,
             double beta,
             int    M,
             double fsample,
             int    up,
             int    dn )
{
    double dx = fmod( j * (double)dn / (double)up, 1. );
    double x = dx + M / 2. - i;
    return 2 * fsample * sinc( twopi * fsample * x ) *
        kaiser( i - dx, beta, M );
}

//______________________________________________________________________________
static uLong
gcd( uLong n, uLong d )
{
    // greates common divisor; Euclids algorithm
    uLong r;
    if ( n < d )
    {
        int tmp = d;
        d = n;
        n = tmp;
    }
    if ( d == 0 )
    {
        return 1;
    }
    while ( d > 0 )
    {
        r = n % d;
        n = d;
        d = r;
    }
    return n;
}

//______________________________________________________________________________
// static uLong lcd (uLong n, uLong d)
// {
//    // lowest common denominator
// return n / gcd (n, d) * d;
// }

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// MultiRate                                                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
MultiRate::MultiRate( )
    : fUp( 1 ), fDn( 1 ), fAttn( 80 ), fOrder( 0 ), fCoeffNum( 0 ),
      fCoeffs( 0 ), fDType( kReal ), fSample( 1. ), fLastTerms( 0 ), fTerms( 0 )
{
    reset( );
}

//______________________________________________________________________________
MultiRate::MultiRate( double fsample, int up, int down, double atten )
    : fUp( up ), fDn( down ), fAttn( atten ), fOrder( 0 ), fCoeffNum( 0 ),
      fCoeffs( 0 ), fDType( kReal ), fSample( fsample ), fLastTerms( 0 ),
      fTerms( 0 )
{
    reset( );
}

//______________________________________________________________________________
MultiRate::MultiRate( const MultiRate& filter )
    : fUp( 1 ), fDn( 1 ), fAttn( 80 ), fOrder( 0 ), fCoeffNum( 0 ),
      fCoeffs( 0 ), fDType( kReal ), fSample( 1. ), fLastTerms( 0 ), fTerms( 0 )
{
    *this = filter;
}

//______________________________________________________________________________
MultiRate::~MultiRate( )
{
    allocCoeff( 0, 0 );
    allocHist( 0 );
}

//______________________________________________________________________________
MultiRate&
MultiRate::operator=( const MultiRate& filter )
{
    if ( this != &filter )
    {
        fUp = filter.fUp;
        fDn = filter.fDn;
        fAttn = filter.fAttn;
        fOrder = filter.fOrder;
        fDType = filter.fDType;
        fSample = filter.fSample;
        fTerms = filter.fTerms;
        fCurTime = filter.fCurTime;
        fStartTime = filter.fStartTime;
        if ( filter.fCoeffs )
        {
            allocCoeff( filter.fCoeffNum, fOrder );
            for ( int i = 0; i < fCoeffNum; ++i )
            {
                for ( int j = 0; j < fOrder; ++j )
                {
                    fCoeffs[ i ][ j ] = filter.fCoeffs[ i ][ j ];
                }
            }
        }
        else
        {
            allocCoeff( 0, 0 );
        }
        if ( filter.fLastTerms )
        {
            allocHist( fOrder + fDn + 1 );
            memcpy( fLastTerms,
                    filter.fLastTerms,
                    2 * ( fOrder + fDn + 1 ) * sizeof( float ) );
        }
        else
        {
            allocHist( 0 );
        }
    }
    return *this;
}

//______________________________________________________________________________
void
MultiRate::dataCheck( const TSeries& ts ) const
{
    // Check frequency.
    double one = fSample * ts.getTStep( );
    if ( one > 1.0001 || one < 0.9999 )
    {
        throw std::invalid_argument( "Wrong frequency" );
    }
    // Check history data valid.
    if ( fCurTime != Time( 0 ) && ts.getStartTime( ) != fCurTime )
    {
        throw std::invalid_argument( "Wrong start time" );
    }
    // Check history data type.
    if ( fCurTime != Time( 0 ) && ( ts.isComplex( ) ^ ( fDType == kComplex ) ) )
    {
        throw std::invalid_argument( "Wrong data type" );
    }
}

//______________________________________________________________________________
TSeries
MultiRate::apply( const TSeries& in )
{
    TSeries out;
    dataCheck( in );
    out.Clear( );
    out = in;
    const TSeries& constout( out );
    int            nBins = in.getNSample( );
    if ( nBins <= 0 )
        return out;

    // Copy data to float TSeries
    if ( constout.isComplex( ) )
    {
        fComplex* cd = (fComplex*)out.refData( );
        apply( nBins, cd, cd );
    }
    else
    {
        out.Convert( DVector::t_float );
        float* d = (float*)out.refData( );
        apply( nBins, d, d );
    }
    // set data type and time
    if ( fCurTime == Time( 0 ) )
    {
        fDType = constout.isComplex( ) ? kComplex : kReal;
    }
    fCurTime = constout.getEndTime( );
    return out;
}

//______________________________________________________________________________
int
MultiRate::apply( int N, const float* in, float* out )
{
    // setup
    int           idx = 0;
    int           shift;
    double        accu;
    const double* coeff;
    int           outn = 0;
    float*        lastterms = (float*)fLastTerms;
    // loop over input
    while ( idx < N )
    {
        // shift up to fDn value into history buffer
        shift = fOrder + fDn - fTerms;
        if ( idx + shift > N )
            shift = N - idx;
        memmove( lastterms, lastterms + shift, fTerms );
        for ( int j = 0; j < shift; ++j )
        {
            lastterms[ shift - j - 1 ] = in[ idx + j ];
        }
        fTerms += shift;
        idx += shift;
        if ( fTerms < fOrder + fDn )
        {
            continue; // not enough for a full U/D cycle
        }
        // compute one U/D cycle
        for ( int j = 0; j < fUp; ++j )
        {
            idx = ( j * fDn ) / fUp;
            idx = fDn - ( j * fDn ) / fUp - 1;
            accu = 0;
            coeff = fCoeffs[ j ];
            for ( int k = 0; k < fOrder + 1; ++k )
            {
                accu += coeff[ k ] * (double)lastterms[ k + idx ];
            }
            out[ outn++ ] = accu;
        }
        fTerms -= fDn;
    }
    return outn;
}

//______________________________________________________________________________
int
MultiRate::apply( int N, const fComplex* in, fComplex* out )
{
    // setup
    int           idx = 0;
    int           shift;
    dComplex      accu;
    const double* coeff;
    int           outn = 0;
    fComplex*     lastterms = (fComplex*)fLastTerms;
    // loop over input
    while ( idx < N )
    {
        // shift up to fDn values into history buffer
        shift = fOrder - 1 + fDn - fTerms;
        if ( idx + shift > N )
            shift = N - idx;
        memmove( lastterms, lastterms + shift, fTerms );
        for ( int j = 0; j < shift; ++j )
        {
            lastterms[ shift - j - 1 ] = in[ idx + j ];
        }
        fTerms += shift;
        idx += shift;
        if ( fTerms < fOrder - 1 + fDn )
        {
            continue; // not enough for a full U/D cycle
        }
        // compute one U/D cycle
        for ( int j = 0; j < fUp; ++j )
        {
            idx = fDn - ( j * fDn ) / fUp - 1;
            accu = 0;
            coeff = fCoeffs[ j ];
            for ( int k = 0; k < fOrder + 1; ++k )
            {
                accu += coeff[ k ] * dComplex( lastterms[ k + idx ] );
            }
            out[ outn++ ] = accu;
        }
        fTerms -= fDn;
    }
    return outn;
}

//______________________________________________________________________________
void
MultiRate::init( double fsample, int up, int down, double atten )
{
    fSample = fsample;
    fUp = up;
    fDn = down;
    fAttn = atten;
    reset( );
}

//______________________________________________________________________________
void
MultiRate::reset( )
{
    // check up and down
    if ( fUp < 1 )
        fUp = 1;
    if ( fDn < 1 )
        fDn = 1;
    // Kaiser window parameters
    double fDOmega;
    double fBeta;
    int    fM;
    double fOut;
    // at least 20dB attenuation
    if ( fAttn < 20 )
        fAttn = 20;
    // Output frequency
    fOut = fSample * (double)fUp / (double)fDn;
    // transition band witdh
    if ( fOut < fSample )
    {
        fDOmega = twopi * 0.1 * fOut / fSample;
    }
    else
    {
        fDOmega = twopi * 0.1;
    }
    // beta
    if ( fAttn < 21 )
    {
        fBeta = 0;
    }
    else if ( fAttn > 50 )
    {
        fBeta = 0.1102 * ( fAttn - 8.7 );
    }
    else
    {
        fBeta = 0.5842 * power( fAttn - 21., 0.4 ) + 0.07886 * ( fAttn - 21 );
    }
    // filter order
    fM = (int)( ( fAttn - 8 ) / ( 2.285 * fDOmega ) + 0.5 );
    fOrder = fM + 1;
    // allocate memory for coeffs
    allocCoeff( fUp, fOrder );
    // fill coefficients
    for ( int i = 0; i < fOrder; ++i )
    {
        for ( int j = 0; j < fUp; ++j )
        {
            fCoeffs[ j ][ i ] =
                coefficient( i, j, fOrder, fBeta, fM, fSample, fUp, fDn );
        }
    }
    // reset history buffer
    allocHist( fOrder + fDn + 1 );
    resetHist( );
    // reset time
    fCurTime = Time( 0, 0 );
    fStartTime = Time( 0, 0 );
}

//______________________________________________________________________________
void
MultiRate::resetHist( )
{
    fTerms = fOrder - 1;
    if ( fLastTerms && ( fTerms > 0 ) )
    {
        memset( fLastTerms, 0, 2 * fTerms * sizeof( float ) );
    }
}

//______________________________________________________________________________
bool
MultiRate::xfer( fComplex& tf, double f ) const
{
    tf = fComplex( 0.0 );
    if ( ( fOrder < 0 ) || !fSample || !fCoeffs || ( fCoeffNum < 1 ) )
    {
        return false;
    }
    fComplex temp;
    float    dPhi = twopi * f / fSample;
    float    Phi0 = dPhi * fOrder / 2.;
    for ( int k = 0; k <= fOrder; k++ )
    {
        temp = std::polar< double >( fCoeffs[ 0 ][ k ], Phi0 - k * dPhi );
        tf += temp;
    }
    return true;
}

//______________________________________________________________________________
void
MultiRate::allocHist( int size )
{
    if ( fLastTerms )
    {
        delete[]( float* ) fLastTerms;
        fLastTerms = 0;
    }
    if ( size > 0 )
    {
        fLastTerms = (void*)new float[ 2 * size ];
    }
}

//______________________________________________________________________________
void
MultiRate::allocCoeff( int num, int len )
{
    if ( fCoeffs )
    {
        for ( int i = 0; i < fCoeffNum; ++i )
        {
            delete[] fCoeffs[ i ];
        }
        delete[] fCoeffs;
        fCoeffs = 0;
    }
    fCoeffNum = num;
    if ( fCoeffNum > 0 )
    {
        fCoeffs = new double*[ num ];
        for ( int i = 0; i < fCoeffNum; ++i )
        {
            fCoeffs[ i ] = new double[ len ];
        }
    }
}

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// multirate                                                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
MultiRate
multirate(
    const char* type, double fsample, double m1, double m2, double atten )
{
    std::string t( type ? type : "" );
    for ( std::string::iterator p = t.begin( ); p != t.end( ); ++p )
    {
        *p = tolower( *p );
    }
    // absolute rate specification
    if ( t == "abs" )
    {
        int up;
        int down;
        resampling_factors( fsample, m1, up, down, m2 );
        return MultiRate( fsample, up, down, atten );
    }
    // relative rate specification
    else if ( t == "rel" )
    {
        int up = (int)( m1 + 0.5 );
        int down = (int)( m2 + 0.5 );
        if ( up < 1 )
            up = 1;
        if ( down < 1 )
            down = 1;
        return MultiRate( fsample, up, down, atten );
    }
    else
    {
        throw std::invalid_argument( "Unknown multi rate type" );
    }
}

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// resampling_factors                                                   //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
void
resampling_factors_int( int fin, int fout, int& up, int& down )
{
    int GCD = gcd( abs( fin ), abs( fout ) );
    up = fout / GCD;
    down = fin / GCD;
}

//______________________________________________________________________________
void
resampling_factors( double fin, double fout, int& up, int& down, double maxerr )
{
    if ( maxerr <= 0 )
        maxerr = 1E-3;
    uLong FIN = uLong( fabs( fin / maxerr ) + 0.5 );
    uLong FOUT = uLong( fabs( fout / maxerr ) + 0.5 );
    int   GCD = gcd( FIN, FOUT );
    up = FIN / GCD;
    down = FOUT / GCD;
}
