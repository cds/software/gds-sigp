/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef LOGIC_2OP_HH
#define LOGIC_2OP_HH

#include "YPipe.hh"

/**  Perform a 2-operand bitwise logical operation between the two input 
  *  series. The two input series must match one another in start time,
  *  time step and number of samples. The output series has the same time 
  *  and length parameters. The output series is assigned units of "bit-mask".
  *  \brief 2-operand logical operation.
  */
class logic_2op : public YPipe
{
public:
    /**  opcode enumerator defines the 16 2-operand bit-wise logic operations.
     *  \brief enumerate logic operations.
     */
    enum opcode
    {
        op_Zero, ///<  Zero
        op_AnorB, ///<  A nor B
        op_notAandB, ///<  ~A and B
        op_notA, ///<  ~A
        op_AandnotB, ///<  A and ~B
        op_notB, ///<  ~B
        op_AneqB, ///<  A neq B (same as xor)
        op_AnandB, ///<  A nand B
        op_AandB, ///<  A and B
        op_AeqB, ///<  A eq B
        op_B, ///<  B
        op_notAorB, ///<  ~A or B
        op_A, ///<  A
        op_AornotB, ///<  A or ~B
        op_AorB, ///<  A or B
        op_One ///<  One
    };

public:
    /**  Construct a 2-operand logic filter. The default operator is 
     *  \f$ A | B \f$.
     *  \brief Default constructor.
     */
    logic_2op( void );

    /**  Construct a 2-operand logic filter with a specified operation.
     *  \brief Operator specified constructor.
     *  \param op Operation opcode.
     */
    logic_2op( opcode op ) : _op( op )
    {
    }

    /**  Construct a 2-operand logic filter with a specified operation.
     *  \brief Operator specified constructor.
     *  \param op Operation string.
     */
    logic_2op( const std::string& op ) : _op( get_opcode( op ) )
    {
    }

    /**  2-operand logic operation destructor.
     *  \brief Destructor
     */
    virtual ~logic_2op( void )
    {
    }

    /**  Perform the indicated bit-wise operation on the two input time series 
     *  and return the result of the operaton.
     *  \brief Perform the configured operation.
     *  \param A %Time series containing the first operand.
     *  \param B %Time series containing the second operand.
     *  \return %Time series containing the result.
     */
    TSeries apply( const TSeries& A, const TSeries& B );

    /**  Create a new %logic_2op instance with the same operator.
     */
    logic_2op* clone( void ) const;

    /**  Check that the start-times and sample rates of the two argument series 
     *  are equal and and correspond to the recorded current time and sample 
     *  rate of the filter.
     *  \brief Check the operand start time sna sample rates.
     *  \param x First operand series.
     *  \param y Second operand series.
     */
    void dataCheck( const TSeries& x, const TSeries& y ) const;

    /**  Get the opcode corresponding the the argument operation symbol.
     *  \brief get the opcode.
     *  \param op operator string.
     *  \return opcode corresponding to the string.
     */
    static opcode get_opcode( const std::string& op );

    /**  Get the operation symbol corresponding to the argument opcode.
     *  \brief get the opcode.
     *  \param op operator opcode.
     *  \return string corresponding to the opcode.
     */
    static std::string get_opstring( opcode op );

    /**  Reset the status information in the filter.
    */
    void reset( void );

    /**  Set the operation code.
     *  \brief set the opcode.
     *  \param op Operation to be performed by this instance.
     */
    void set_oper( opcode op );

private:
    opcode _op;
};

#endif // LOGIC_2OP_HH
