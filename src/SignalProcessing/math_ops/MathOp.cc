/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "MathOp.hh"
#include "DVecType.hh"
#include "lcl_array.hh"

using namespace std;

//======================================  Constructor
MathOp::MathOp( const std::string& oper, double value ) : _value( value )
{
    if ( oper == "+" )
    {
        _oper = kBias;
    }
    else if ( oper == "*" )
    {
        _oper = kScale;
    }
    else if ( oper == "*" )
    {
        _oper = kScale;
    }
    else if ( oper == "&" )
    {
        _oper = kAnd;
    }
    else if ( oper == "|" )
    {
        _oper = kOr;
    }
    else if ( oper == "^" )
    {
        _oper = kXor;
    }
    else if ( oper == "<" )
    {
        _oper = kLt;
    }
    else if ( oper == "<=" )
    {
        _oper = kLEq;
    }
    else if ( oper == ">" )
    {
        _oper = kGt;
    }
    else if ( oper == ">=" )
    {
        _oper = kGEq;
    }
    else if ( oper == "=" )
    {
        _oper = kEq;
    }
    else if ( oper == "!=" )
    {
        _oper = kNEq;
    }
    else
    {
        throw runtime_error( string( "MathOp: Invalid operation name: " ) +
                             oper );
    }
}
//======================================  Destructor
MathOp::~MathOp( void )
{
}

//======================================  Clone an instance
MathOp*
MathOp::clone( void ) const
{
    return new MathOp( *this );
}

//======================================  Apply the filter.
TSeries
MathOp::apply( const TSeries& in )
{
    if ( in.empty( ) )
        return in;
    prep( in );

    TSeries rc( in );
    size_t  N = rc.getNSample( );

    switch ( _oper )
    {

    //------------------------------------  Arithemetic operations (+, *)
    case kBias:
        rc += _value;
        break;
    case kScale:
        rc *= _value;
        break;

    //-------------------------------------  Boolean operations (&, |, ^)
    case kAnd:
    case kOr:
    case kXor: {
        lcl_array< int > data( N );
        rc.getData( N, data );
        int intval = int( _value );
        if ( _oper == kAnd )
        {
            for ( size_t i = 0; i < N; ++i )
                data[ i ] &= intval;
        }
        else if ( _oper == kOr )
        {
            for ( size_t i = 0; i < N; ++i )
                data[ i ] |= intval;
        }
        else if ( _oper == kXor )
        {
            for ( size_t i = 0; i < N; ++i )
                data[ i ] ^= intval;
        }
        rc.refDVect( )->replace( 0, N, DVectI( N, data ) );
    }
    break;

    //------------------------------------  Comparisons (<, <=, >, >=, =, !=)
    case kLt:
    case kLEq:
    case kGt:
    case kGEq:
    case kEq:
    case kNEq: {
        lcl_array< double > data( N );
        rc.getData( N, data );
        if ( _oper == kLt )
        {
            for ( size_t i = 0; i < N; ++i )
                data[ i ] = ( data[ i ] < _value ) ? 1.0 : 0;
        }
        else if ( _oper == kLEq )
        {
            for ( size_t i = 0; i < N; ++i )
                data[ i ] = ( data[ i ] <= _value ) ? 1.0 : 0;
        }
        else if ( _oper == kGt )
        {
            for ( size_t i = 0; i < N; ++i )
                data[ i ] = ( data[ i ] > _value ) ? 1.0 : 0;
        }
        else if ( _oper == kGEq )
        {
            for ( size_t i = 0; i < N; ++i )
                data[ i ] = ( data[ i ] >= _value ) ? 1.0 : 0;
        }
        else if ( _oper == kEq )
        {
            for ( size_t i = 0; i < N; ++i )
                data[ i ] = ( data[ i ] == _value ) ? 1.0 : 0;
        }
        else if ( _oper == kNEq )
        {
            for ( size_t i = 0; i < N; ++i )
                data[ i ] = ( data[ i ] != _value ) ? 1.0 : 0;
        }
        rc.refDVect( )->replace( 0, N, DVectD( N, data ) );
    }
    break;
    }
    return rc;
}

//======================================  Don't bother with the data.
void
MathOp::dataCheck( const TSeries& ts ) const
{
}
