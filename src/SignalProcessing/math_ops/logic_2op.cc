/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "logic_2op.hh"
#include "DVecType.hh"
#include "lcl_array.hh"
#include <stdexcept>

using namespace std;

//=====================================================================
//
//    logic_2op class
//
//=====================================================================

logic_2op::logic_2op( void ) : _op( op_AorB )
{
}

TSeries
logic_2op::apply( const TSeries& A, const TSeries& B )
{
    if ( A.empty( ) )
        return A;
    dataCheck( A, B );
    if ( !inUse( ) )
    {
        mStartTime = A.getStartTime( );
    }

    size_t                N = A.getNSample( );
    DVectU                out( *A.refDVect( ) );
    lcl_array< uint32_t > B_data( N );
    switch ( _op )
    {
    case op_A:
    case op_notA:
    case op_Zero:
    case op_One:
        break;
    default:
        B.refDVect( )->getData( 0, N, B_data.get( ) );
    }

    //-----------------------------------  Perform the requested operation
    switch ( _op )
    {
    case op_Zero: ///  Zero
        for ( size_t i = 0; i < N; ++i )
            out[ i ] = 0;
        break;
    case op_AnorB: ///  A nor B
        for ( size_t i = 0; i < N; ++i )
            out[ i ] = ~( out[ i ] | B_data[ i ] );
        break;
    case op_notAandB: ///  ~A and B
        for ( size_t i = 0; i < N; ++i )
            out[ i ] = ~out[ i ] & B_data[ i ];
        break;
    case op_notA: ///  ~A
        for ( size_t i = 0; i < N; ++i )
            out[ i ] = ~out[ i ];
        break;
    case op_AandnotB: ///  A and ~B
        for ( size_t i = 0; i < N; ++i )
            out[ i ] &= ~B_data[ i ];
        break;
    case op_notB: ///  ~B
        for ( size_t i = 0; i < N; ++i )
            out[ i ] = ~B_data[ i ];
        break;
    case op_AneqB: ///  A neq B (same as xor)
        for ( size_t i = 0; i < N; ++i )
            out[ i ] ^= B_data[ i ];
        break;
    case op_AnandB: ///  A nand B
        for ( size_t i = 0; i < N; ++i )
            out[ i ] = ~( out[ i ] & B_data[ i ] );
        break;
    case op_AandB: ///  A and B
        for ( size_t i = 0; i < N; ++i )
            out[ i ] &= B_data[ i ];
        break;
    case op_AeqB: ///  A eq B
        for ( size_t i = 0; i < N; ++i )
            out[ i ] ^= ~B_data[ i ];
        break;
    case op_B: ///  B
        for ( size_t i = 0; i < N; ++i )
            out[ i ] = B_data[ i ];
        break;
    case op_notAorB: ///  ~A or B
        for ( size_t i = 0; i < N; ++i )
            out[ i ] = ~out[ i ] | B_data[ i ];
        break;
    case op_A: ///  A
        break;
    case op_AornotB: ///  A or ~B
        for ( size_t i = 0; i < N; ++i )
            out[ i ] |= ~B_data[ i ];
        break;
    case op_AorB: ///  A or B
        for ( size_t i = 0; i < N; ++i )
            out[ i ] |= B_data[ i ];
        break;
    case op_One: ///  One
        for ( size_t i = 0; i < N; ++i )
            out[ i ] = ~0;
        break;
    }
    TSeries ts_out( A.getStartTime( ), A.getTStep( ), out );
    ts_out.setUnits( "bit_mask" );
    mCurrentTime = A.getEndTime( );
    return ts_out;
}

logic_2op*
logic_2op::clone( void ) const
{
    return new logic_2op( *this );
}

void
logic_2op::dataCheck( const TSeries& x, const TSeries& y ) const
{
    bool mtq = x.empty( ) || y.empty( );
    bool synchOK = !mtq && ( x.getStartTime( ) == y.getStartTime( ) ) &&
        ( x.getInterval( ) == y.getInterval( ) ) &&
        ( x.getTStep( ) == y.getTStep( ) );
    if ( !synchOK )
    {
        throw runtime_error( "logic_2op: unmatched argument series" );
    }
    if ( inUse( ) && x.getStartTime( ) != mCurrentTime )
    {
        throw runtime_error( "logic_2op: Gap in input data" );
    }
}

logic_2op::opcode
logic_2op::get_opcode( const std::string& op )
{
    for ( int i = int( op_Zero ); i <= int( op_One ); i++ )
    {
        opcode iop = static_cast< opcode >( i );
        if ( get_opstring( iop ) == op )
            return iop;
    }
    throw runtime_error( "logic_2op: unknown operator string" );
}

std::string
logic_2op::get_opstring( opcode op )
{
    //-----------------------------------  Perform the requested operation
    switch ( op )
    {
    case op_Zero: ///  Zero
        return "0";
    case op_AnorB: ///  A nor B
        return "~(A|B)";
    case op_notAandB: ///  ~A and B
        return "~A&B";
    case op_notA: ///  ~A
        return "~A";
    case op_AandnotB: ///  A and ~B
        return "A&~B";
    case op_notB: ///  ~B
        return "~B";
    case op_AneqB: ///  A neq B (same as xor)
        return "A^B";
    case op_AnandB: ///  A nand B
        return "~(A&B)";
    case op_AandB: ///  A and B
        return "A&B";
    case op_AeqB: ///  A eq B
        return "~(A^B)";
    case op_B: ///  B
        return "B";
    case op_notAorB: ///  ~A or B
        return "~A|B";
    case op_A: ///  A
        return "A";
    case op_AornotB: ///  A or ~B
        return "A|~B";
    case op_AorB: ///  A or B
        return "A|B";
    case op_One: ///  One
        return "1";
    }
    throw runtime_error( "logic_2op: undefined op code" );
}

void
logic_2op::reset( void )
{
    mStartTime = Time( 0 );
}

void
logic_2op::set_oper( opcode op )
{
    _op = op;
}
