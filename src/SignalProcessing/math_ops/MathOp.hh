/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef MATHOP_HH
#define MATHOP_HH
#include "NullPipe.hh"

/**  The %MathOp filter performs a binary arithemetic or logical operation on
  *  each element of the input time series with a specified constant operand.
  *  The result time series data will be of the same type as the input time 
  *  series. The series data and the constant value are converted to integers 
  *  before performing the boolean operations (and, or and exclusive or) and
  *  the resulting value is converted back to the input data type. Logical 
  *  comparisons return 1 for true and 0 for false.
  *  \brief Arithemetic and logical operations with a constant.
  *  \author John Zweizig (john.zweizig@ligo.org)
  *  \version $Id$
  */
class MathOp : public NullPipe
{
public:
    using Pipe::apply;
    using Pipe::dataCheck;

    /**  Construct a %MathOp filter with the specified operation and constant 
      *  operand. Valid operations are: \e bias ('+'), \e scale ('*'), 
      *  \e and ('&'), \e or ('|'), \e exclusive \e or ('^'), \e less ('<'),
      *  \e less \e or \e equal ('<='), \e greater \e than ('<'),
      *  \e greater \e or \e equal ('<='), \e equal ('=') and \e not \e equal
      *  ('!=').
      */
    MathOp( const std::string& oper, double value );

    /**  Destroy a MathOp filter.
      *  \brief Destructor.
      */
    ~MathOp( void );

    /**  Create an identical copy of this instance.
      *  \brief Clone a %MathOp filter.
      *  \return pointer to the identical copy.
      */
    MathOp* clone( void ) const;

    /**  Perform the specified operation element-by-element on the input
      *  time series and return the result as a time series of the same 
      *  data type with idential start time, time step, etc.
      *  \brief Operate on the time series
      *  \param in Input time series.
      *  \return Result time series.
      */
    TSeries apply( const TSeries& in );

    /**  Check that the specified time series is compatible with the filter.
      *  \note Since the %MathOp filter works on an element by element basis,
      *  the dataCheck will never fail.
      *  \brief Check that data is appropriate.
      */
    void dataCheck( const TSeries& ts ) const;

private:
    enum op_type
    {
        kBias,
        kScale,
        kAnd,
        kOr,
        kXor,
        kLt,
        kLEq,
        kGt,
        kGEq,
        kEq,
        kNEq
    } _oper;
    double _value;
};

#endif // !defined(MATHOP_HH)
