/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef EMPTY_FLAG_HH
#define EMPTY_FLAG_HH

#include "Pipe.hh"
#include "DVector.hh"
#include <memory>

class TSeries;
class DVector;

/**  The empty_flag class behaves as a filter that returns a pre-defined time
  *  series in place of an input series. The replacement series is defined by 
  *  a sample-rate, a length (in time or samples?) and a constant value. The
  *  start time of the result vector is copied from the input vector. The 
  *  replacement series parameters (sample-rate, length, data-type) may be 
  *  copied from a template TSeries entered as an argument to the 
  */
class empty_flag : public Pipe
{
public:
    empty_flag( void );
    empty_flag( const TSeries& tsattr );
    empty_flag( const empty_flag& x );
    virtual ~empty_flag( void );
    TSeries     apply( const TSeries& in );
    empty_flag* clone( void ) const;
    void        dataCheck( const TSeries& ts ) const;
    Time        getCurrentTime( void ) const;
    Time        getStartTime( void ) const;
    bool        inUse( void ) const;
    void        reset( void );

    /**  Set the value used to indicate an empty series. THe empty value is 
     *  specified as a a double precision number, but is converted to the
     *  specified data type.
     *  \brief Set empty indication value.
     *  \param value Empty value.
     */
    void set_empty_value( double value );
    void set_sample_rate( double srate );
    void set_series_length( long nsample );
    void set_series_length( Interval dt );
    void set_series_attrs( const TSeries& ts );

    /**  Specify the series data type. The valid options are: "int_2", "int_4", 
     *  "real_4", "real_8" and "uint_4".
     *  \brief Set the vector data type.
     *  \param dv_type Data type string.
     */
    void set_vector_type( const std::string& dv_type );

private:
    /**  Resize and fill the empty series replacement vector if the vector 
     *  length and type are available.
     */
    void make_vector( void );

private:
    double                     sample_rate;
    long                       sample_count;
    double                     empty_value;
    std::unique_ptr< DVector > empty_vec;
    Time                       current;
    Time                       start;
};

//======================================  inline methods
inline Time
empty_flag::getCurrentTime( void ) const
{
    return current;
}

inline Time
empty_flag::getStartTime( void ) const
{
    return start;
}

inline bool
empty_flag::inUse( void ) const
{
    return start != Time( 0 );
}

#endif //!defined(EMPTY_FLAG_HH)
