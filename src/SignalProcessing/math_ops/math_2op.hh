/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef MATH_2OP_HH
#define MATH_2OP_HH

#include "YPipe.hh"

/**  Perform a 2-operand mathematical operation between the two input 
  *  series. The two input series must match one another in start time,
  *  time step and number of samples. The output series has the same time 
  *  and length parameters. Note that the YSynch filter may be used to 
  *  unskew input series. The output series is assigned the same units 
  *  as the A operand.
  *  \brief 2-operand logical operation.
  *  \author John Zweizig <jzweizig@ligo.org>
  */
class math_2op : public YPipe
{
public:
    /**  opcode enumerator defines the 16 2-operand mathematical operations.
     *  \brief enumerate logic operations.
     */
    enum opcode
    {
        op_AplusB, ///<  A + B
        op_AminusB, ///<  A - B
        op_AtimesB, ///<  A * B
        op_AdivbyB, ///<  A / B
        op_hypot, ///<  sqrt(A^2 +  B^2)
        op_power ///<  A^B
    };

public:
    /**  Construct a 2-operand mathematical filter. The default operator is 
     *  \f$ A + B \f$.
     *  \brief Default constructor.
     */
    math_2op( void );

    /**  Construct a 2-operand math filter with a specified operation.
     *  \brief Operator specified constructor.
     *  \param op Operation opcode
     */
    math_2op( opcode op ) : _op( op )
    {
    }

    /**  Construct a 2-operand logic filter with a specified operation.
     *  \brief Operator specified constructor.
     *  \param op Operation string
     */
    math_2op( const std::string& op ) : _op( get_opcode( op ) )
    {
    }

    /**  2-operand mathematical operation destructor.
     *  \brief Destructor
     */
    virtual ~math_2op( void )
    {
    }

    /**  Perform the indicated sample-by-sample operation on the two input time 
     *  series and return the result of the operaton.
     *  \brief Perform the configured operation.
     *  \param A %Time series containing the first operand.
     *  \param B %Time series containing the second operand.
     *  \return %Time series containing the result.
     */
    TSeries apply( const TSeries& A, const TSeries& B );

    /**  Create a new %math_2op instance with the same operator.
     */
    math_2op* clone( void ) const;

    /**  Check that the start-times and sample rates of the two argument series 
     *  are equal and and correspond to the recorded current time and sample 
     *  rate of the filter.
     *  \brief Check the operand start time sna sample rates.
     *  \param x First operand series.
     *  \param y Second operand series.
     */
    void dataCheck( const TSeries& x, const TSeries& y ) const;

    /**  Get the opcode corresponding the the argument operation symbol.
     *  \brief get the opcode.
     *  \param op operator string.
     *  \return opcode corresponding to the string.
     */
    static opcode get_opcode( const std::string& op );

    /**  Get the operation symbol corresponding to the argument opcode.
     *  \brief get the opcode.
     *  \param op operator opcode.
     *  \return string corresponding to the opcode.
     */
    static std::string get_opstring( opcode op );

    /**  Reset the status information in the filter.
    */
    void reset( void );

    /**  Set the operation code.
     *  \brief set the opcode.
     *  \param op Operation to be performed by this instance.
     */
    void set_oper( opcode op );

private:
    opcode _op;
};

#endif // MATH_2OP_HH
