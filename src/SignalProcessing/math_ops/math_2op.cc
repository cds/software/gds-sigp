/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "math_2op.hh"
#include "DVecType.hh"
#include "lcl_array.hh"
#include <cmath>
#include <stdexcept>

using namespace std;

//=====================================================================
//
//    math_2op class
//
//=====================================================================

math_2op::math_2op( void ) : _op( op_AplusB )
{
}

TSeries
math_2op::apply( const TSeries& A, const TSeries& B )
{
    if ( A.empty( ) )
        return A;
    dataCheck( A, B );
    if ( !inUse( ) )
    {
        mStartTime = A.getStartTime( );
    }

    TSeries ts_out( A );
    ts_out.Convert( DVectD::getDataType( ) );

    //-----------------------------------  Perform the requested operation
    switch ( _op )
    {
    case op_AplusB: ///<  A + B
        ts_out += B;
        break;

    case op_AminusB: ///<  A - B
        ts_out -= B;
        break;

    case op_AtimesB: ///<  A * B
        ts_out *= B;
        break;

    case op_AdivbyB: ///<  A / B
        ts_out /= B;
        break;

    case op_hypot: { ///<  sqrt(A^2 +  B^2)
        DVectD& dvd( dynamic_cast< DVectD& >( *( ts_out.refDVect( ) ) ) );
        DVectD  dvb( *( B.refDVect( ) ) );
        size_t  N = dvd.size( );
        for ( size_t i = 0; i < N; i++ )
        {
            dvd[ i ] = sqrt( dvd[ i ] * dvd[ i ] + dvb[ i ] * dvb[ i ] );
        }
        break;
    }

    case op_power: { ///<  A^B
        DVectD& dvd( dynamic_cast< DVectD& >( *( ts_out.refDVect( ) ) ) );
        DVectD  dvb( *( B.refDVect( ) ) );
        size_t  N = dvd.size( );
        for ( size_t i = 0; i < N; i++ )
        {
            dvd[ i ] = pow( dvd[ i ], dvb[ i ] );
        }
        break;
    }
    }

    //ts_out.setUnits(A.getUnits());
    mCurrentTime = A.getEndTime( );
    return ts_out;
}

math_2op*
math_2op::clone( void ) const
{
    return new math_2op( *this );
}

void
math_2op::dataCheck( const TSeries& x, const TSeries& y ) const
{
    bool mtq = x.empty( ) || y.empty( );
    bool synchOK = !mtq && ( x.getStartTime( ) == y.getStartTime( ) ) &&
        ( x.getInterval( ) == y.getInterval( ) ) &&
        ( x.getTStep( ) == y.getTStep( ) );
    if ( !synchOK )
    {
        throw runtime_error( "math_2op: unmatched argument series" );
    }
    if ( inUse( ) && x.getStartTime( ) != mCurrentTime )
    {
        throw runtime_error( "math_2op: Gap in input data" );
    }
}

math_2op::opcode
math_2op::get_opcode( const std::string& op )
{
    for ( int i = int( op_AplusB ); i <= int( op_power ); i++ )
    {
        opcode iop = static_cast< opcode >( i );
        if ( get_opstring( iop ) == op )
            return iop;
    }
    throw runtime_error( "math_2op:: unknown string" );
}

std::string
math_2op::get_opstring( opcode op )
{
    //-----------------------------------  Perform the requested operation
    switch ( op )
    {
    case op_AplusB: ///<  A + B
        return "+";
    case op_AminusB: ///<  A - B
        return "-";
    case op_AtimesB: ///<  A * B
        return "*";
    case op_AdivbyB: ///<  A / B
        return "/";
    case op_hypot: ///<  sqrt(A^2 +  B^2)
        return "hypot";
    case op_power: ///<  A^B
        return "^";
    }
    throw runtime_error( "math_2op: undefined op code" );
}

void
math_2op::reset( void )
{
    mStartTime = Time( 0 );
}

void
math_2op::set_oper( opcode op )
{
    _op = op;
}
