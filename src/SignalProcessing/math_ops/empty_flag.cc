/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "empty_flag.hh"
#include "TSeries.hh"
#include "DVecType.hh"
#include <iostream>

using namespace std;

//======================================  Default constructor
empty_flag::empty_flag( void )
    : sample_rate( 0.0 ), sample_count( 0 ), empty_value( 0.0 )
{
}

//======================================  Template time series constructor
empty_flag::empty_flag( const TSeries& tsattr ) : empty_value( 0.0 )
{
    set_series_attrs( tsattr );
}

//======================================  Template time series constructor
empty_flag::empty_flag( const empty_flag& x )
{
    sample_rate = x.sample_rate;
    sample_count = x.sample_count;
    empty_value = x.empty_value;
    if ( x.empty_vec )
        empty_vec.reset( x.empty_vec->clone( ) );
}

//======================================  empty_flag destructor
empty_flag::~empty_flag( void )
{
}

//======================================  clone an empty_flag
empty_flag*
empty_flag::clone( void ) const
{
    return new empty_flag( *this );
}

//======================================  apply empty_flag filter
TSeries
empty_flag::apply( const TSeries& in )
{
    TSeries r( in );
    Time    t0 = in.getStartTime( );
    if ( !start )
        start = t0;
    if ( r.empty( ) )
        r.setData( t0, Interval( 1.0 / sample_rate ), empty_vec->clone( ) );
    // r.dump_header(cout);
    current = r.getEndTime( );
    return r;
}

//======================================  data check
void
empty_flag::dataCheck( const TSeries& ts ) const
{
}

//======================================  make the vector
void
empty_flag::make_vector( void )
{
    if ( empty_vec && sample_count )
    {
        empty_vec->Clear( );
        empty_vec->Extend( sample_count );
        if ( empty_value != 0.0 )
            *empty_vec += empty_value;
    }
}

//=======================================  Reset the emptyflag filter.
void
empty_flag::reset( void )
{
    start = Time( 0 );
    current = Time( 0 );
}

//======================================  Set value indicating an empty series
void
empty_flag::set_empty_value( double value )
{
    empty_value = value;
    make_vector( );
}

//======================================  set the sample rate
void
empty_flag::set_sample_rate( double srate )
{
    sample_rate = srate;
}

//======================================  set the number of samples
void
empty_flag::set_series_length( long nsample )
{
    sample_count = nsample;
    make_vector( );
}

//======================================  set the series length (interval)
void
empty_flag::set_series_length( Interval dt )
{
    if ( sample_rate != 0.0 )
    {
        sample_count = long( double( dt ) * sample_rate + 0.5 );
        make_vector( );
    }
    else
    {
        cerr << "empty_flag: Series length cannot be set without a sample rate."
             << endl;
    }
}

//======================================  Copy attributes from a TSeries.
void
empty_flag::set_series_attrs( const TSeries& ts )
{
    if ( ts.getTStep( ) != Interval( 0 ) )
        sample_rate = 1. / double( ts.getTStep( ) );
    const DVector* dv = ts.refDVect( );
    if ( dv )
        empty_vec.reset( dv->clone( ) );
    if ( dv->size( ) )
        set_series_length( dv->size( ) );
    else
        make_vector( );
}

//======================================  Set vector data type
void
empty_flag::set_vector_type( const std::string& dv_type )
{
    if ( dv_type == "int_2" )
    {
        empty_vec.reset( new DVectS );
    }
    else if ( dv_type == "int_4" )
    {
        empty_vec.reset( new DVectI );
    }
    else if ( dv_type == "uint_4" )
    {
        empty_vec.reset( new DVectU );
    }
    else if ( dv_type == "real_4" || dv_type == "float" )
    {
        empty_vec.reset( new DVectF );
    }
    else if ( dv_type == "real_8" || dv_type == "double" )
    {
        empty_vec.reset( new DVectD );
    }
    else
    {
        cerr << "empty_vec: Unrecognized vector type: " << dv_type << endl;
    }
    make_vector( );
}
