#ifndef DELAY_HH
#define DELAY_HH

#include "Interval.hh"
#include "Pipe.hh"

class Delay : public Pipe
{
public:
    using Pipe::apply;
    using Pipe::dataCheck;

    Delay( void );
    Delay( Interval dT );
    TSeries  apply( const TSeries& ts );
    Delay*   clone( void ) const;
    Time     getCurrentTime( void ) const;
    Interval getDelay( void ) const;
    Time     getStartTime( void ) const;
    void     setDelay( Interval dT );
    void     reset( void );
    void     dataCheck( const TSeries& ts ) const;
    bool     inUse( void ) const;

private:
    Interval mDelay;
    Time     mStart;
    TSeries  mStore;
};

//======================================  inline methods
inline Interval
Delay::getDelay( void ) const
{
    return mDelay;
}

inline Time
Delay::getStartTime( void ) const
{
    return mStart;
}

inline bool
Delay::inUse( void ) const
{
    return mStart != Time( 0 );
}

#endif // DELAY_HH
