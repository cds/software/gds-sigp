#include "Delay.hh"
#include "DVector.hh"
#include <stdexcept>

using namespace std;

//======================================  Default constructor
Delay::Delay( void ) : mDelay( 0.0 ), mStart( 0 )
{
}

//======================================  Data constructor
Delay::Delay( Interval dT ) : mDelay( dT ), mStart( 0 )
{
}

//======================================  Apply the delay
TSeries
Delay::apply( const TSeries& ts )
{
    if ( mDelay <= Interval( 0.0 ) || ts.isEmpty( ) )
        return ts;
    Interval dT = ts.getTStep( );
    long     nSample = long( mDelay / dT + 0.5 );

    Time tStart = ts.getStartTime( );
    Time tBegin = tStart - dT * double( nSample );
    if ( !mStart )
    {
        mStart = tStart;
        float f( 0.0 );
        mStore = TSeries( tBegin, dT, 1, &f );
        mStore.Convert( ts.refDVect( )->getType( ) );
        mStore.extend( mStart );
    }

    if ( mStore.Append( ts ) )
        throw runtime_error( "Time not contiguous" );
    if ( tBegin > mStore.getStartTime( ) )
    {
        mStore.eraseStart( tBegin - mStore.getStartTime( ) );
    }

    long     N = ts.getNSample( );
    DVector* dv =
        const_cast< const TSeries& >( mStore ).refDVect( )->Extract( 0, N );
    TSeries rc( tStart, dT, *dv );
    delete dv;
    mStore.eraseStart( dT * double( N ) );
    return rc;
}

//======================================  Clone a filter
Delay*
Delay::clone( void ) const
{
    return new Delay( *this );
}

//======================================  Check data
void
Delay::dataCheck( const TSeries& ts ) const
{
    if ( mDelay <= Interval( 0.0 ) || mStore.isEmpty( ) )
    {
        return;
    }
    else if ( !Almost( mStore.getEndTime( ), ts.getStartTime( ) ) )
    {
        throw runtime_error( "Time not contiguous" );
    }
    else if ( mStore.getTStep( ) != ts.getTStep( ) )
    {
        throw runtime_error( "Different time step" );
    }
}

Time
Delay::getCurrentTime( void ) const
{
    return mStore.getEndTime( );
}

void
Delay::reset( void )
{
    mStore.Clear( );
    mStart = Time( 0 );
}

void
Delay::setDelay( Interval dT )
{
    mDelay = dT;
    reset( );
}
