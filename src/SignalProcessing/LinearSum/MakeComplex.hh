/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef MAKECOMPLEX_HH
#define MAKECOMPLEX_HH

#include "YPipe.hh"

/**  The %MakeComplex class is a YPipe based class that returns a complex
  *  %TSeries formed from two input TSeries containing the real- and 
  *  imaginary parts of the complex number. Note the the start-times, the
  *  time steps and the number of entries of the two input series must be
  *  identical. If the input data types are both either double or float,
  *  the resulting time series with have dCOmples or fComplex data type,
  *  respectively. If the data types differ or are not double or float, 
  *  both input series will be converted to double and th resultins series 
  *  will be of type dComplex.
  *  \brief Construct a comples time series.
  *  \author John Zweizig (john.zweizig@ligo.org)
  */
class MakeComplex : public YPipe
{
public:
    using YPipe::apply;
    using YPipe::dataCheck;

    /**  Default constructor.
     *  \brief Constructor.
     */
    MakeComplex( void );

    /**  Destroy the object.
     *  \brief Destructor
     */
    ~MakeComplex( void );

    /**  Make a complex time series from two input series.
     *  \brief Convert a pair of series to complex
     *  \param real Real part of the complex series.
     *  \param imag Imaginary part of the complex series.
     *  \returns Complex time series
     */
    TSeries      apply( const TSeries& real, const TSeries& imag );
    MakeComplex* clone( void ) const;
    void         dataCheck( const TSeries& real, const TSeries& imag ) const;
    Time         getCurrentTime( void ) const;
    void         reset( void );

private:
    Time mCurrentTime;
};

inline Time
MakeComplex::getCurrentTime( void ) const
{
    return mCurrentTime;
}

#endif // !defined(MAKECOMPLEX_HH)
