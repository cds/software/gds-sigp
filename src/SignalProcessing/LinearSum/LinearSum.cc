#include "LinearSum.hh"
#include <stdexcept>

using namespace std;

//======================================  Constructor
LinearSum::LinearSum( double alpha, double beta )
    : mAlpha( alpha ), mBeta( beta )
{
}

//======================================  Destructor
LinearSum::~LinearSum( void )
{
}

//======================================  Apply the filter to the input series
TSeries
LinearSum::apply( const TSeries& x, const TSeries& y )
{
    dataCheck( x, y );
    if ( !mStartTime )
        mStartTime = x.getStartTime( );
    TSeries tempA = x;
    tempA *= mAlpha;
    TSeries tempB = y;
    tempB *= mBeta;
    tempA += tempB;
    mCurrentTime = tempB.getEndTime( );
    return tempA;
}

LinearSum*
LinearSum::clone( void ) const
{
    return new LinearSum( *this );
}

//======================================  Apply the filter to the input series
void
LinearSum::dataCheck( const TSeries& x, const TSeries& y ) const
{
    if ( x.getStartTime( ) != y.getStartTime( ) )
    {
        throw runtime_error( "LinearSum: Unmatched input times" );
    }
    if ( x.getNSample( ) != y.getNSample( ) )
    {
        throw runtime_error( "LinearSum: Unmatched series lengths" );
    }
    if ( x.getTStep( ) != y.getTStep( ) )
    {
        throw runtime_error( "LinearSum: Unmatched sample rates" );
    }
    if ( inUse( ) && mCurrentTime != x.getStartTime( ) )
    {
        throw runtime_error( "LinearSum: Incorrect input time" );
    }
}

//======================================  Reset the pipe.
void
LinearSum::reset( void )
{
    mStartTime = Time( 0 );
}
