
add_library(LinearSum OBJECT
    LinearSum.cc
    MakeComplex.cc
)

target_include_directories(LinearSum PRIVATE
    ${CMAKE_SOURCE_DIR}/src/SignalProcessing/FilterBase

    ${CMAKE_SOURCE_DIR}/src/Base/complex
    ${CMAKE_SOURCE_DIR}/src/Base/thread
    ${CMAKE_SOURCE_DIR}/src/Base/time

    ${CMAKE_SOURCE_DIR}/src/Containers/CWVec
    ${CMAKE_SOURCE_DIR}/src/Containers/DVector
    ${CMAKE_SOURCE_DIR}/src/Containers/FilterIO
    ${CMAKE_SOURCE_DIR}/src/Containers/TSeries
)

install(FILES
    LinearSum.hh
    MakeComplex.hh
    DESTINATION include/gds-sigp
)
