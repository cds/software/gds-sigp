/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "MakeComplex.hh"
#include "DVecType.hh"
#include <stdexcept>

using namespace std;

//======================================  Constructor
MakeComplex::MakeComplex( void )
{
}

//======================================  Destructor
MakeComplex::~MakeComplex( void )
{
}

//======================================  Apply the filter to the input series
TSeries
MakeComplex::apply( const TSeries& x, const TSeries& y )
{
    dataCheck( x, y );
    size_t N = x.getNSample( );
    if ( !mStartTime )
        mStartTime = x.getStartTime( );
    unique_ptr< DVector > dv;
    if ( x.refDVect( )->D_data( ) && y.refDVect( )->D_data( ) )
    {
        DVectW* wv = new DVectW( N );
        dv.reset( wv );
        const DVectD& xv = *dynamic_cast< const DVectD* >( x.refDVect( ) );
        const DVectD& yv = *dynamic_cast< const DVectD* >( y.refDVect( ) );
        for ( size_t i = 0; i < N; i++ )
        {
            ( *wv )[ i ] = dComplex( xv[ i ], yv[ i ] );
        }
    }
    else if ( x.refDVect( )->F_data( ) && y.refDVect( )->F_data( ) )
    {
        DVectC* wv = new DVectC( N );
        dv.reset( wv );
        const DVectF& xv = *dynamic_cast< const DVectF* >( x.refDVect( ) );
        const DVectF& yv = *dynamic_cast< const DVectF* >( y.refDVect( ) );
        for ( size_t i = 0; i < N; i++ )
        {
            ( *wv )[ i ] = fComplex( xv[ i ], yv[ i ] );
        }
    }
    else
    {
        DVectW* wv = new DVectW( N );
        dv.reset( wv );
        DVectD xv( *x.refDVect( ) );
        DVectD yv( *y.refDVect( ) );
        for ( size_t i = 0; i < N; i++ )
        {
            ( *wv )[ i ] = dComplex( xv[ i ], yv[ i ] );
        }
    }
    TSeries ts( x.getStartTime( ), x.getTStep( ), dv.release( ) );
    mCurrentTime = ts.getEndTime( );
    return ts;
}

//======================================  Apply the filter to the input series
void
MakeComplex::dataCheck( const TSeries& x, const TSeries& y ) const
{
    if ( x.getStartTime( ) != y.getStartTime( ) )
    {
        throw runtime_error( "MakeComplex: Unmatched input times" );
    }
    if ( x.getNSample( ) != y.getNSample( ) )
    {
        throw runtime_error( "MakeComplex: Unmatched series lengths" );
    }
    if ( x.getTStep( ) != y.getTStep( ) )
    {
        throw runtime_error( "MakeComplex: Unmatched sample rates" );
    }
    if ( inUse( ) && mCurrentTime != x.getStartTime( ) )
    {
        throw runtime_error( "MakeComplex: Incorrect input time" );
    }
}

//======================================  Reset the pipe.
MakeComplex*
MakeComplex::clone( void ) const
{
    return new MakeComplex( *this );
}

//======================================  Reset the pipe.
void
MakeComplex::reset( void )
{
    mStartTime = Time( 0 );
}
