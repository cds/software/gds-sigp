#ifndef LINEARSUM_HH
#define LINEARSUM_HH

#include "YPipe.hh"

/**  The %LinearSum class is a YPipe based class that returns a  linear 
  *  combination of two input TSeries, \e i.e.
  *   \f$ Z(X, Y) = \alpha X + \beta Y \f$.
  *  \brief Linar combination of time series.
  *  \author John Zweizig (john.zweizig@ligo.org)
  */
class LinearSum : public YPipe
{
public:
    using YPipe::apply;
    using YPipe::dataCheck;

    /**  Initializing constructor.
    *  \brief Constructor.
    *  \param alpha Mutiplicative factor for first TSeries.
    *  \param beta  Mutiplicative factor for second TSeries.
    */
    LinearSum( double alpha = 1, double beta = 1 );

    /**  Destroy the object.
    *  \brief Destructor
    */
    ~LinearSum( void );
    TSeries    apply( const TSeries& x, const TSeries& y );
    LinearSum* clone( void ) const;
    void       dataCheck( const TSeries& x, const TSeries& y ) const;
    Time       getCurrentTime( void ) const;
    void       reset( void );

private:
    Time   mCurrentTime;
    double mAlpha;
    double mBeta;
};

inline Time
LinearSum::getCurrentTime( void ) const
{
    return mCurrentTime;
}

#endif // !defined(LINEARSUM_HH)
