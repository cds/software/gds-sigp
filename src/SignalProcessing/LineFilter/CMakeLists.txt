
add_library(LineFilter OBJECT
    LineFilter.cc
)

target_include_directories(LineFilter PRIVATE
    ${CMAKE_SOURCE_DIR}/src/SignalProcessing/FilterBase
    ${CMAKE_SOURCE_DIR}/src/SignalProcessing/wavelet

    ${CMAKE_SOURCE_DIR}/src/Base/complex
    ${CMAKE_SOURCE_DIR}/src/Base/time

    ${CMAKE_SOURCE_DIR}/src/Containers/DVector
    ${CMAKE_SOURCE_DIR}/src/Containers/FilterIO
    ${CMAKE_SOURCE_DIR}/src/Containers/TSeries
    ${CMAKE_SOURCE_DIR}/src/Containers/Wavelet
)

install(FILES
    LineFilter.hh
    DESTINATION include/gds-sigp
)
