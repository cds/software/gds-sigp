#include "BaseLine.hh"
#include "DVecType.hh"
#include <stdexcept>

using namespace std;

//======================================  Default constructor
BaseLine::BaseLine( double tConst, double rate )
    : mTconst( tConst ), mTStep( 0.0 )
{
    reset( );
    if ( rate )
        setRate( rate );
}

//======================================  Copy constructor
BaseLine::BaseLine( const BaseLine& x )
    : mTconst( x.mTconst ), mHistory( 0.0 ), mTStep( x.mTStep ),
      mEpsilon( x.mEpsilon ), mStartTime( 0 ), mCurrentTime( 0 )
{
}

//======================================  Destructor
BaseLine::~BaseLine( void )
{
}

//======================================  Clone method
BaseLine*
BaseLine::clone( void ) const
{
    return new BaseLine( *this );
}

//======================================  Apply the filter to a Time series
TSeries
BaseLine::apply( const TSeries& x )
{

    //----------------------------------  Set up if nothing shaking yet.
    if ( !inUse( ) )
    {
        mStartTime = x.getStartTime( );
        mCurrentTime = mStartTime;
        mTStep = x.getTStep( );
        mEpsilon = double( mTStep ) / mTconst;
        mHistory = x.getDouble( 0 );

        //----------------------------------  Check for valid input series
    }
    else if ( mTStep != x.getTStep( ) )
    {
        throw runtime_error( "Incompatible sample rate" );
    }
    else if ( !Almost( mCurrentTime, x.getStartTime( ) ) )
    {
        throw runtime_error( "Incompatible start time" );
    }

    //----------------------------------  Convert series to the correct type
    TSeries r( x );
    r.Convert( DVecType< series_type >( ).getType( ) );
    series_type* ptr = static_cast< series_type* >( r.refData( ) );

    //----------------------------------  Loop over series data and filter.
    unsigned int N = r.getNSample( );
    for ( unsigned int i = 0; i < N; i++ )
    {
        *ptr -= mHistory;
        mHistory += *ptr++ * mEpsilon;
    }
    mCurrentTime = r.getEndTime( );
    return r;
}

//======================================  Clear the history
void
BaseLine::reset( void )
{
    mStartTime = Time( 0 );
    mCurrentTime = Time( 0 );
    mHistory = 0.0;
}

//======================================  Check for valid input data.
void
BaseLine::dataCheck( const TSeries& x ) const
{
    if ( !inUse( ) )
        return;
    if ( mTStep != x.getTStep( ) )
    {
        throw runtime_error( "Incompatible sample rate" );
    }
    else if ( !Almost( mCurrentTime, x.getStartTime( ) ) )
    {
        throw runtime_error( "Incompatible start time" );
    }
}

//======================================  Check for valid input data.
void
BaseLine::setRate( double rate )
{
    mTStep = Interval( 1. / rate );
    mEpsilon = double( mTStep ) / mTconst;
}
