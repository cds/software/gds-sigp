#ifndef BASELINE_HH
#define BASELINE_HH
#include "Pipe.hh"

/**  Baseline restoration filter is essentially a (single pole?) IIR 
  *  filter that causes the average value (baseline) of the input signal 
  *  to revert to 0.0 with a specified time constant.
  *  @memo Revert signal average to 0
  *  @author J. Zweizig
  *  @version 1.0; Modified December 14, 2001
  */
class BaseLine : public Pipe
{
public:
    typedef float series_type;

public:
    explicit BaseLine( double tConst = 1.0, double mRate = 0.0 );
    explicit BaseLine( const BaseLine& x );
    ~BaseLine( void );
    TSeries apply( const TSeries& x );
    FilterIO&
    apply( const FilterIO& in )
    {
        return Pipe::apply( in );
    }
    BaseLine* clone( void ) const;
    void      dataCheck( const TSeries& x ) const;
    void
    dataCheck( const FilterIO& in ) const
    {
        Pipe::dataCheck( in );
    }
    Time   getCurrentTime( void ) const;
    double getRate( void ) const;
    Time   getStartTime( void ) const;
    double getTimeConst( void ) const;
    bool   inUse( void ) const;
    void   reset( void );
    void   setRate( double rate );

private:
    double   mTconst;
    double   mHistory;
    Interval mTStep;
    double   mEpsilon;
    Time     mStartTime;
    Time     mCurrentTime;
};

//======================================  Inline functions
#ifndef __CINT__
inline Time
BaseLine::getCurrentTime( void ) const
{
    return mCurrentTime;
}

inline double
BaseLine::getRate( void ) const
{
    return 1. / double( mTStep );
}

inline Time
BaseLine::getStartTime( void ) const
{
    return mStartTime;
}

inline double
BaseLine::getTimeConst( void ) const
{
    return mTconst;
}

inline bool
BaseLine::inUse( void ) const
{
    return ( mCurrentTime != Time( 0 ) );
}

#endif

#endif // BASELINE_HH
