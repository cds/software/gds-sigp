/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef COHERENCE_HH
#define COHERENCE_HH

#include "TSeries.hh"
#include "fSeries/CSD.hh"
#include "fSeries/PSD.hh"
#include "calc_dft.hh"
#include "autopipe.hh"

class window_api;

/**  The %Coherence class calculates the magnitude squared coherence between 
  *  two digitized signals. It sums the overlapped cross-spectral 
  *  density of the two input signals and calculates the magnitude-squared 
  *  coherence (\f$C_{XY}\f$) from 
  *  \f[ C_{XY} = \frac{|G_{XY}|^2}{G_{XX} G_{YY}}. \f]
  *  where \f$G_{IJ}(f) = \sum_{i} \tilde{I}_i(f) \tilde{J}^*_i(f)\f$ 
  *  for all segments (\e i.e. the Welch estimate of coherence). If a 
  *  desired sample rate is specified, both signals will be resampled by 
  *  factors of two (using DecimateBy2) to give the requested sample rate. 
  *  The input signals are divided into segments of the specified stride 
  *  length and overlap fraction. The data segments are windowed as requested 
  *  and the cross spectral density (\f$C_{XY}\f$) and power spectral density
  *  for each signal (\f$C_{XX}, C_{XX} \f$ ) are accumulated. An arbitrary
  *  number of segments may be added into the accumulators with the add() 
  *  method. The resulting magnitude-squared coherence is returned by 
  *  the get_coherence() method or may be calculated in a single step with 
  *  the operator()() method.
  *  \brief Calculate coherence between two signals
  *  \author John Zweizig (john.zweizig@ligo.org)
  */
class Coherence
{
public:
    /**  Construct an uninitialized %Coherence object. If no stride, window, 
      *  overlap or sample rate are specified, these parameters will be 
      *  inferred from the first data as follows:
      *  - \e Stride: length will be the same as the length of shorter input
      *    series.
      *  - \e Window: No windowing will be performed.
      *  - \e Overlap: No Overlapping will be performed.
      *  - \e Sample-Rate: The sample rate of the lower rate input series 
      *    will be used.
      *  \brief Default constructor.
      */
    Coherence( void );

    /**  Construct a %Coherence and set the stride, overlap, window and
      *  sample rate parameters. If any or all of the parameters are set 
      *  to zero or left unspecified, these values will take the default
      *  values as specified in the default (Coherence()) constructor.
      *  \brief Construct and initialize a %Coherence object.
      *  \param stride  Length of an analysis stride. (see set_stride())
      *  \param overlap Fractional overlap of the  strides (see set_overlap()).
      *  \param win     Window object (see set_window()).
      *  \param sample_rate Sample rate (see set_rate())
      */
    explicit Coherence( Interval          stride,
                        double            overlap = 0.0,
                        const window_api* win = 0,
                        double            sample_rate = 0 );

    /**  Destroy the coherence object and release all current storage.
      *  \brief  Destructor.
      */
    virtual ~Coherence( void );

    /**  Accumulate all the information needed to calculate the coherence
      *  for one or more strides. The input series are resampled as appropriate
      *  and added to the input signal history series. If sufficient data are
      *  available to calculate a stride, the data stride is windowed and 
      *  a discrete Fourier transform is made. The resulting DFTs are 
      *  combined and added to the accumulated %CSD and PSDs.
      *  \brief Add one or more data strides to the coherence data Accumulators.
      *  \exception std::runtime_error is thrown if the the sample rate of
      *  one or both input series can not be converted to the specified rate,
      *  or if the series start time does not match the end of the series
      *  history data.
      *  \param x Time series containing data from the first (x) signal.
      *  \param y Time series containing data from the second (y) signal.
      */
    void add( const TSeries& x, const TSeries& y = TSeries::null_tseries );

    /**  Accumulate all the information needed to calculate the coherence
      *  for one stride from a %TSeries and the DFT of the second (conjugated)
      *  argument. The input series is assumed to contain sufficient data for 
      *  a new stride, the data stride is resampled, windowed and a discrete 
      *  Fourier transform is calculated. The resulting DFT is combined with 
      *  the conjugated second argument and added to the accumulated %CSD and 
      *  PSDs.
      *  \brief Add one or more data strides to the coherence data Accumulators.
      *  \exception std::runtime_error is thrown if the the sample rate of
      *  one or both input series can not be converted to the specified rate,
      *  or if the series start time does not match the end of the series
      *  history data.
      *  \param x Time series containing data from the first (x) signal.
      *  \param y Conjugated DFT of the second (y) signal.
      */
    void add( const TSeries& x, const containers::DFT& y );

    /**  Get the number of strides that have been averaged together.
      *  \brief Averaged stride count.
      *  \return Number of averaged strides.
      */
    size_t average_count( void ) const;

    /**  Enable calculation of the transfer function.
      *  \brief Enable transfer function calculation.
      *  \param w Windo
      */
    void enable_xfer_fc( bool ok );

    /**  Calculate the coherence from the accumulated data and return it as
      *  a PSD object. 
      *  \brief Calculate the coherence from the accumulated data.
      *  \note The data accumulators used to calculate the coherence are not
      *  reset. They will be included in the next coherence() calculation 
      *  unless explicitly reset with reset_accumulators() or reset().
      *  \return %Coherence in a PSD.
      */
    containers::PSD get_coherence( void ) const;

    /**  Calculate the transfer function from the accumulated data and return 
      *  it as a DFT object. 
      *  \brief Calculate the transfer function from the accumulated data.
      *  \note The data accumulator used to calculate the transfer function 
      *  is not reset. It will be included in the next trasfer function 
      *  calculation unless explicitly reset with reset_accumulators() or 
      *  reset().
      *  \return %Coherence in a PSD.
      */
    containers::DFT get_xfer_func( void ) const;

    /**  Return the start time of the next overlapping stride to be 
      *  accumulated.
      *  \brief Start of next data stride.
      *  \return Time of next stride to be accumulated.
      */
    const Time& currentTime( void ) const;

    /**  Calculate the coherence from two time series. The coherence is 
      *  calculated using the parameters specified in the constructor. 
      *  If the stride length has not been set, the stride length will
      *  be one eighth of the shorter time series length to give a valid 
      *  coherence estimate. The accumulators are reset before the 
      *  calculation is performed.
      *  \note Resetting the accumulators leaves the %Coherence object
      *  ready to process the next sequential stride. If the data passed
      *  to this method do not immediately follow any previous data, an
      *  exception will be thrown. Use reset() to prevent this.
      *  \brief Single step coherence calculation.
      *  \exception std::runtime_error is thrown if the sample rate of one
      *  or both input series can not be converted to the specified rate,
      *  or if the series start time does not match the end of the series
      *  history data.
      *  \param x Time series containing data from the first (x) signal.
      *  \param y Time series containing data from the second (y) signal.
      *  \return Coherence contained in a PSD.
      */
    containers::PSD operator( )( const TSeries& x, const TSeries& y );

    /**  Reference to the CSD sum accumulator.
     *   \brief CSD reference
     *   \return constant reference to the CSD sum.
     */
    const containers::CSD& refCSD( void ) const;

    /**  Reference to the x PSD sum accumulator.
     *   \brief X PSD reference
     *   \return constant reference to the x-PSD sum.
     */
    const containers::PSD& refXPSD( void ) const;

    /**  Reference to the y PSD sum accumulator.
     *   \brief Y PSD reference
     *   \return constant reference to the y-PSD sum.
     */
    const containers::PSD& refYPSD( void ) const;

    /**  Reset the accumulators, clear the input history and destroy the 
      *  resampling filters. Note that the sample rate and stride time are 
      *  retained even if they were inferred from the first data stride.
      *  \brief Reset accumulators and history.
      */
    void reset( void );

    /**  Reset all the coherence accumulators. The history and resampling
      *  filters are retained. This method should be used when calculating 
      *  the coherence of a several sequential stretches of the same data 
      *  stream.
      *  \brief Reset the coherence accumulators.
      */
    void reset_accumulators( void );

    /**  Set the overlap fraction parameter that indicates the portion of
      *  input data stride to be retained for use in the next data stride.
      *  The valid range for the overlap fraction is \c 0\<=x\<1.
      *  \brief Set overlap parameter.
      *  \exception std::invalid_argument \a ovlp is not a valid fraction.
      *  \param ovlp Fraction of stride to be retained.
      */
    void set_overlap( double ovlp );

    /**  Set the desired sample rate. Each signal will be resampled to this
      *  rate if it has a higher sample rate.
      *  \note The sample rate is useful in setting the coherence frequency 
      *        band (\f$ 0 <= f <= f_{Ny} = 0.5 \times rate\f$).
      *  \note The current resampling implementation will only reduce sample 
      *        rates by a power of two. If the specified sample rate differs 
      *        from the signal sample rates by other than a power of two, the
      *        add() method will throw an exception.
      *  \brief Set the sample rate.
      *  \param rate Desired sample rate in Hz.
      */
    void set_rate( double rate );

    /**  Set the length of a single stride in seconds. The coherence 
      *  frequency step will be the inverse of this number.
      *  \note If the stride is not set, the length of the shorter series
      *  passed to the add method will be used as a default. The exception
      *  is if the operator()() method is called before add() in which case 
      *  the stride is set to one eight of the shorter input series length.
      *  \brief Set stride time.
      *  \param dt Stride time (in seconds).
      */
    void set_stride( Interval dt );

    /**  Set up for a standard Welch estimate with the given stride length.
      *  This method uses a Hamming window and 50% overlap. The stride 
      *  length considerations are discussed for set_stride(). If the
      *  stride is set to zero, the length of the first data series passed 
      *  to add() will be used instead.
      *  \brief Set parameters for Welch's method.
      *  \param stride Data stride length.
      */
    void set_welch( Interval stride );

    /**  Set the windowing function to be applied to each data stride before
      *  performing the discrete Fourier transform.
      *  \brief Set the window.
      *  \param w Window class object to be used.
      */
    void set_window( const window_api& w );

    /**  Get the time of the first data provided to the add() method after 
      *  the most recent reset().
      *  \brief Get start of analyzed data stream.
      *  \return Start of data stream.
      */
    const Time& startTime( void ) const;

    /** Return the stride length.
      */
    Interval stride( void ) const;

    /** Return the window type.
      */
    std::string window_type( void ) const;

    //--------------------------------------  Private data manipulation methods
private:
    /**  Resample the specified data stream and add it to the specified 
      *  history series. If the resampling has not been set up yet, the
      *  necessary decimation filter is constructed and returned to \a decim.
      *  \brief Resample input data.
      *  \exception std::runtime_error The resampling filter cannot be 
      *  constructed or the resampled data can not be appended to the end 
      *  of the history series (TSeries::Append).
      */
    void resample( auto_pipe& decim, const TSeries& in, TSeries& hist );

private:
    //------------------------------------  Parameters
    Interval  mStride; ///< Stride length in s
    double    mOverlap; ///< Overlap fraction, f:  0 <= f < 1
    double    mSampleRate; ///< Resampling frequency
    auto_pipe mWindow; ///< Window function
    bool      mXferCalc; ///< Calculate the xfer function and coherence

    //------------------------------------  State variables.
    Time            mStartTime;
    Time            mCurrent;
    Time            mStartStride;
    auto_pipe       mXDecim;
    auto_pipe       mYDecim;
    calc_dft        mXCalc;
    calc_dft        mYCalc;
    size_t          mNumAcc;
    containers::CSD mXYSum;
    containers::PSD mXXSum;
    containers::PSD mYYSum;
    containers::DFT mXferFc; ///< Transfer function accumulator.
};

//======================================  Inline Coherence methods
inline size_t
Coherence::average_count( void ) const
{
    return mNumAcc;
}

inline const Time&
Coherence::currentTime( void ) const
{
    return mCurrent;
}

inline const containers::CSD&
Coherence::refCSD( void ) const
{
    return mXYSum;
}

inline const containers::PSD&
Coherence::refXPSD( void ) const
{
    return mXXSum;
}

inline const containers::PSD&
Coherence::refYPSD( void ) const
{
    return mYYSum;
}

inline const Time&
Coherence::startTime( void ) const
{
    return mStartTime;
}

inline Interval
Coherence::stride( void ) const
{
    return mStride;
}

#endif // !defined(COHERENCE_HH)
