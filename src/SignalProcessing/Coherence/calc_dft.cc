/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "calc_dft.hh"
#include "window_api.hh"
#include <stdexcept>

using namespace std;
using namespace containers;

//======================================  calc_dft constructor.
calc_dft::calc_dft( void ) : mStride( 1.0 ), mOverlap( 0 )
{
}

//======================================  calc_dft constructor.
calc_dft::calc_dft( Interval stride, const std::string& window, double overlap )
    : mStride( stride )
{
    if ( !window.empty( ) )
        set_window( window );
    set_overlap( overlap );
}

//======================================  calc_dft destructor.
calc_dft::~calc_dft( void )
{
}

//======================================  add data to the history time series.
void
calc_dft::add( const TSeries& ts )
{
    if ( mHistory.empty( ) )
    {
        mHistory = ts;
    }
    else if ( mHistory.Append( ts ) )
    {
        throw std::runtime_error(
            "calc_dft: Unable to append to history series" );
    }
}

//======================================  Get the next DFT
DFT
calc_dft::nextDFT( void )
{
    if ( !ready( ) )
    {
        throw runtime_error( "calc_dft: Insufficient data for stride." );
    }
    containers::DFT dft;
    if ( mWindow.null( ) )
        dft = DFT( mHistory.extract( start( ), mStride ) );
    else
        dft = DFT( mWindow( mHistory.extract( start( ), mStride ) ) );
    mHistory.eraseStart( mStride * ( 1.0 - mOverlap ) );
    return dft;
}

//======================================  calculate nyquist frequency
double
calc_dft::nyquist( void ) const
{
    double tStep( mHistory.getTStep( ) );
    if ( !tStep )
        return 0;
    return 0.5 / tStep;
}

//======================================  calc_dft destructor.
void
calc_dft::reset( void )
{
    mHistory.Clear( );
}

//======================================  Set the window length.
void
calc_dft::set_stride( Interval dt )
{
    mStride = dt;
}

//======================================  Set the window instance.
void
calc_dft::set_window( const window_api& window )
{
    mWindow.set( window );
}

//======================================  Set the windoe type and.
void
calc_dft::set_window( const std::string& window, double arg )
{
    try
    {
        mWindow.set( window_factory_1( window, 0, arg ) );
    }
    catch ( exception& e )
    {
        mWindow.set( window_factory( window ) );
    }
}

//======================================  Set the overlap fraction.
void
calc_dft::set_overlap( double overlap )
{
    mOverlap = overlap;
}

//======================================  Erase data up to the spocified time
void
calc_dft::set_start( const Time& t0 )
{
    if ( !t0 )
        return;
    Interval dt = t0 - start( );
    if ( dt > Interval( 0 ) )
        mHistory.eraseStart( dt );
}

//======================================  constant reference to window.
const window_api&
calc_dft::window( void ) const
{
    if ( mWindow.null( ) )
        throw runtime_error( "calc_dft: window not defined" );
    return dynamic_cast< const window_api& >( *mWindow );
}

//======================================  window type string destructor.
std::string
calc_dft::window_type( void ) const
{
    return ::window_type( mWindow.get( ) );
}
