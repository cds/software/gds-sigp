/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "TSeries.hh"
#include "fSeries/DFT.hh"
#include "autopipe.hh"

class window_api;

/** The calc_dft class calculates a series of optionally overlapping DFTs
 *  from streaming time series inputs. The class is initialized with a 
 *  stride (window) length, a window type and an overlap fraction. Time 
 *  series data are added to an input buffer after which, for each call to 
 *  nextDFT(), the next data stride is windowed, the dft is calculated,
 *  and the data pointers are advanced.
 *  \brief Streaming dft calculation tool.
 *  \author John Zweizig (john.zweizig@ligo.org)
 *  \version 10/9/20
 */
class calc_dft
{
public:
    /** Construct a default calc_dft instance.
    *  \brief Default constructor
    */
    calc_dft( void );

    /** Construct a calc_dft instance and initialize the stride, window and
    *  overlap.
    *  \brief Initializing constructor.
    *  \param stride stride and window length of the data to be analyzed.
    *  \param window Type of window to be used.
    *  \param overlap Fractional overlap between strides.
    */
    calc_dft( Interval           stride,
              const std::string& window = "",
              double             overlap = 0 );

    /**  calc_dft instance destructor.
    */
    virtual ~calc_dft( void );

    /** Add time series data to the input buffer. The start time and sample 
    *  rate of the argument series must be identical to those of previous
    *  series data after the latest reset.
    *  \brief Add data to the input buffer (history) series.
    */
    void add( const TSeries& ts );

    /** Compute the DFT from the next data stride and remove the non-overlapped
    *  data from the front of the input series buffer.
    *  \brief Compute a DFT from the first buffered data stride.
    *  \return Resulting DFT
    */
    containers::DFT nextDFT( void );

    /** Get the Nyquist frequency based on the sample rate of the input buffer.
    *  \brief Nyquist frequency
    *  \return nyquost frequesncy or zero.
    */
    double nyquist( void ) const;

    /** Return the fraction overlap of a stride with the previous data stride.
    *  Valid valus of the overlap are \f$ 0 <= overlap < 1 \f$
    *  in practice, this means that the time difference between the start of 
    *  an analysis stride and the start of the next stride is
    *  \f$ (1 - overlap) x stride \f$.
    *  \brief Overlap fraction.
    *  \return Overlap fraction.
    */
    double overlap( void ) const;

    /** Test whether there are sufficient data in the input buffer to calculate
    *  the next DFT.
    *  \brief Test if ready to calculate next stride.
    *  \return True if sufficient data available to calculate next dft.
    */
    bool ready( void ) const;

    /** Reset the input data buffer.
    */
    void reset( void );

    /** Set the data stride length. This length is used as the window length  
    *  and length of the input series for each discrete forier transform.
    *  \brief Set stride length in seconds.
    *  \param dt Length of time series used for each dft.
    */
    void set_stride( Interval dt );

    /** Set the window to be used in the dft calculation. A clone of the 
    *  argument window is stored for lter use.
    *  \brief Set the windo instance.
    *  \param arg window to be cloned.
    */
    void set_window( const window_api& window );

    /** Set the window to be used in calculating a DFT. The window name string
    *  can be any window name as required by the window_factory() function. 
    *  The \a arg argument contains the parameter used for single parameter
    *  window types (\e e.g. tukey, kaiser. \see window_factory_1()) 
    *  \brief Set the window type.
    *  \param window Window type string.
    *  \param arg    Window argument for single parameter window types.
    */
    void set_window( const std::string& window, double arg = 0 );

    /** Set the overlap fraction. The overlap fraction is described in the
    *  comments for overlap() and may be set eith directly with set_overlap() 
    *  or indirectly by setting the window function.
    *  \brief Set overlap fraction.
    *  \param overlap Over lap fraction to be used.
    */
    void set_overlap( double overlap );

    /** Skip ahead in the input data stream to the specified time. This results 
    *  in discarding some of the data entered via the add() method without it
    *  being used for a DFT calculation. 
    *  \brief Skip input data up to the specified time.
    *  \param t0 Time up to which the data will be skipped.
    */
    void set_start( const Time& t0 );

    /** Start time for the next DFT calculation, i.e. it is the time of the 
    *  first sample in the input (history) buffer. This function returns 
    *  zero if the input buffer is empty.
    *  \brief Start time of next stride.
    *  \return Start time of the next stride.
    */
    Time start( void ) const;

    /** Get the analysis stride interval.
    *  \brief Analysis stride.
    *  \return Analysis stride in seconds.
    */
    Interval stride( void ) const;

    /** Return a reference to the cal_dft window. If the window is not defined,
    *  window() throws a sdtd::runtim_error exception.
    *  \brief Window reference.
    *  \return reference to the configures calc_dft window.
    */
    const window_api& window( void ) const;

    /** Return an stl string containing the window type. This is generated by 
    *  the ::window_type() function. calc_dft window type string.
    *  \brief Window type string.
    *  \return STL string containing the window type name.
    */
    std::string window_type( void ) const;

private:
    Interval  mStride; ///< length in seconds of time series data stride.
    double    mOverlap; ///< fraction overlap between one stride and the next
    auto_pipe mWindow; ///< pointer to the window base_class
    TSeries   mHistory; ///< Data added and available for a DFT calculation
};

//======================================  inline methods
inline Time
calc_dft::start( void ) const
{
    if ( mHistory.empty( ) )
        return Time( 0 );
    return mHistory.getStartTime( );
}

inline double
calc_dft::overlap( void ) const
{
    return mOverlap;
}

inline bool
calc_dft::ready( void ) const
{
    return mHistory.getInterval( ) >= mStride;
}

inline Interval
calc_dft::stride( void ) const
{
    return mStride;
}
