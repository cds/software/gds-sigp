/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "Coherence.hh"
#include "Hamming.hh"
#include "DecimateBy2.hh"
#include "fSeries/DFT.hh"
#include <bit>
#include <stdexcept>
#include <iostream>

using namespace std;
using namespace containers;

//======================================  Default constructor.
Coherence::Coherence( void )
    : mStride( 0 ), mOverlap( 0 ), mSampleRate( 0 ), mXferCalc( false ),
      mNumAcc( 0 )
{
}

//======================================  Data constructor.
Coherence::Coherence( Interval          stride,
                      double            overlap,
                      const window_api* w,
                      double            sample_rate )
    : mStride( stride ), mOverlap( 0 ), mSampleRate( sample_rate ),
      mXferCalc( false ), mNumAcc( 0 )
{
    set_stride( stride );
    if ( w )
        mWindow.set( *w );
    if ( overlap > 0 )
        set_overlap( overlap );
}

//======================================  Destructor.
Coherence::~Coherence( void )
{
}

//======================================  Add one or more strides to the
//                                        accumulated coherence
void
Coherence::add( const TSeries& x, const TSeries& y )
{

    //------------------------------------  Check that the stride is calculated.
    if ( !mStride )
    {
        throw runtime_error( "Coherence: stride was not specified" );
    }

    //------------------------------------  Set up sample rate
    if ( mSampleRate == 0 )
    {
        if ( x.getTStep( ) < y.getTStep( ) )
        {
            mSampleRate = 1.0 / y.getTStep( );
        }
        else if ( x.getTStep( ) == Interval( 0.0 ) )
        {
            throw runtime_error( "Coherence: Invalid sample rate. " );
        }
        else
        {
            mSampleRate = 1.0 / x.getTStep( );
        }
    }

    //-------------------------------------  Resample data, append to history
    TSeries xr, yr;
    resample( mXDecim, x, xr );
    mXCalc.add( xr );
    if ( !y.empty( ) )
    {
        resample( mYDecim, y, yr );
        mXCalc.add( yr );
    }

    //------------------------------------  Make sure start times are equal.
    if ( mXCalc.start( ) != mYCalc.start( ) )
    {
        if ( !mXCalc.start( ) || !mYCalc.start( ) )
            return;
        if ( mXCalc.start( ) > mYCalc.start( ) )
        {
            mYCalc.set_start( mXCalc.start( ) );
        }
        else
        {
            mXCalc.set_start( mYCalc.start( ) );
        }
    }

    //------------------------------------  Record first data start time.
    if ( !mStartTime )
    {
        mStartTime = mXCalc.start( );
        mCurrent = mStartTime;
    }
    if ( !mStartStride )
        mStartStride = mXCalc.start( );

    //------------------------------------  Loop over overlapping strides.
    while ( mXCalc.ready( ) && mYCalc.ready( ) )
    {
        containers::DFT xDft( mXCalc.nextDFT( ) );
        containers::DFT yDft( mYCalc.nextDFT( ) );
        if ( xDft.getHighFreq( ) != yDft.getHighFreq( ) )
        {
            if ( xDft.getHighFreq( ) > yDft.getHighFreq( ) )
            {
                xDft = xDft.extract_dft( 0, yDft.getHighFreq( ) );
            }
            else
            {
                yDft = yDft.extract_dft( 0, xDft.getHighFreq( ) );
            }
        }
        containers::CSD xyCsd( yDft, xDft );
        containers::PSD xPsd( xDft );
        containers::PSD yPsd( yDft );

        //-------------------------------- First time - set accumulators.
        if ( !mNumAcc )
        {
            mXYSum = xyCsd;
            mXXSum = xPsd;
            mYYSum = yPsd;
        }

        //------------------------------  Subsequently - Add to accumulators.
        else
        {
            mXYSum += xyCsd;
            mXXSum += xPsd;
            mYYSum += yPsd;
        }

        //------------------------------  Add a term to the transfer function
        if ( mXferCalc )
        {
            containers::DFT yovrx;
            static_cast< containers::fSeries& >( yovrx ) = xyCsd;
            yovrx /= xPsd;
            if ( !mNumAcc )
                mXferFc = yovrx;
            else
                mXferFc += yovrx;
        }

        //------------------------------  Bump the counter, advance history
        //                                and current time.
        mNumAcc++;
        mCurrent = mXCalc.start( );
    }
}

//======================================  Add one or more strides to the
//                                        accumulated coherence
void
Coherence::add( const TSeries& x, const DFT& yDftIn )
{

    //----------------------------------  Check that the stride is calculated.
    if ( !mStride )
    {
        throw runtime_error( "Coherence: stride was not specified" );
    }
    if ( double( mStride ) * yDftIn.getFStep( ) != 1.00 )
    {
        throw runtime_error(
            "Coherence::add DFT f-step conflicts with stride" );
    }

    //----------------------------------  Set up sample rate
    if ( mSampleRate == 0 )
    {
        if ( x.getTStep( ) == Interval( 0.0 ) )
        {
            throw runtime_error( "Coherence: Invalid sample rate. " );
        }
        mSampleRate = 1.0 / double( x.getTStep( ) );
        double ySample = 2.0 * yDftIn.getHighFreq( );
        if ( mSampleRate > ySample )
            mSampleRate = ySample;
    }

    //----------------------------------  Resample data, append to history
    if ( !x.empty( ) )
    {
        TSeries xr;
        resample( mXDecim, x, xr );
        if ( mXCalc.start( ) != yDftIn.getStartTime( ) )
        {
            throw runtime_error(
                "Coherence: Unsynchronized TSeries, DFT times." );
        }
        mXCalc.add( xr );
    }

    //----------------------------------  Record first data start time.
    if ( !mStartTime )
    {
        mStartTime = mXCalc.start( );
        mCurrent = mStartTime;
    }
    if ( !mStartStride )
        mStartStride = mXCalc.start( );

    //----------------------------------  Loop over overlapping strides.
    containers::DFT xDft( mXCalc.nextDFT( ) );
    containers::DFT yDft( yDftIn );
    if ( xDft.getHighFreq( ) != yDft.getHighFreq( ) )
    {
        if ( xDft.getHighFreq( ) > yDft.getHighFreq( ) )
        {
            xDft = xDft.extract_dft( 0, yDft.getHighFreq( ) );
        }
        else
        {
            yDft = yDft.extract_dft( 0, xDft.getHighFreq( ) );
        }
    }

    containers::CSD xyCsd( yDft, xDft );
    containers::PSD xPsd( xDft );
    containers::PSD yPsd( yDft );

    //----------------------------------  First time - set accumulators.
    if ( !mNumAcc )
    {
        mXYSum = xyCsd;
        mXXSum = xPsd;
        mYYSum = yPsd;
    }

    //----------------------------------  Subsequently - Add to accumulators.
    else
    {
        mXYSum += xyCsd;
        mXXSum += xPsd;
        mYYSum += yPsd;
    }

    //------------------------------  Add a term to the transfer funcetion
    if ( mXferCalc )
    {
        containers::DFT yovrx;
        static_cast< containers::fSeries& >( yovrx ) = xyCsd;
        yovrx /= xPsd;
        if ( !mNumAcc )
            mXferFc = yovrx;
        else
            mXferFc += yovrx;
    }

    //----------------------------------  Bump count and advance x history.
    mNumAcc++;
    mCurrent = mXCalc.start( );
}

//======================================  Calculate the coherence from the
//                                        accumulated CSD and PSDs
containers::PSD
Coherence::get_coherence( void ) const
{
    //----------------------------------  fill a PSD with the CSD modsq.
    containers::PSD r;
    if ( !mNumAcc )
        return r;
    static_cast< containers::fSeries& >( r ) = mXYSum.modsq( );

    r /= mXXSum;
    r /= mYYSum;
    return r;
}

//======================================  Calculate the coherence from the
//                                        accumulated CSD and PSDs
containers::DFT
Coherence::get_xfer_func( void ) const
{
    containers::DFT r;
    if ( mNumAcc )
    {
        r = mXferFc;
        r *= 1.0 / double( mNumAcc );
    }
    return r;
}

//======================================  All in one coherence calculation.
containers::PSD
Coherence::operator( )( const TSeries& x, const TSeries& y )
{
    reset_accumulators( );
    add( x, y );
    return get_coherence( );
}

//======================================  Resample data and append it to the
//                                        input history series.
void
Coherence::resample( auto_pipe& decim, const TSeries& in, TSeries& hist )
{

    //------------------------------------  No resampling necessary
    if ( fabs( mSampleRate * double( in.getTStep( ) ) - 1.0 ) < 1e-6 )
    {
        if ( hist.empty( ) )
        {
            hist = in;
        }
        else
        {
            int rc = hist.Append( in );
            if ( rc )
                throw runtime_error( "Coherence: Invalid input data." );
        }
    }

    //------------------------------------  Set up resampling?
    else
    {
        if ( !mStartTime )
        {
            int resample =
                int( 1.0 / double( in.getTStep( ) * mSampleRate ) + 0.5 );
            if ( resample < 2 || !std::__has_single_bit( resample ) )
                throw runtime_error( "Coherence: Invalid resample request" );
            int N = 0;
            while ( resample > 1 )
            {
                resample /= 2;
                N++;
            }
            decim.set( new DecimateBy2( N, 1 ) );
        }

        //------------------------------------  Resample
        if ( decim.null( ) )
            throw runtime_error( "Coherence: Resampling misconfigured." );
        if ( hist.empty( ) )
        {
            hist = decim( in );
        }
        else
        {
            int rc = hist.Append( decim( in ) );
            if ( rc )
                throw runtime_error( "Coherence: Invalid input data." );
        }
    }
}

//======================================  Reset accumulators, history and
//                                        resamplers
void
Coherence::reset( void )
{
    reset_accumulators( );
    mStartTime = Time( 0 );
    mXCalc.reset( );
    mYCalc.reset( );
    mXDecim.set( 0 );
    mYDecim.set( 0 );
}
//======================================  Reset accumulators
void
Coherence::reset_accumulators( void )
{
    mXXSum.clear( );
    mXYSum.clear( );
    mYYSum.clear( );
    mNumAcc = 0;
    mStartStride = Time( 0 );
}

//======================================  Set the overlap value
void
Coherence::set_overlap( double ovlp )
{
    if ( ovlp < 0 || ovlp >= 1.0 )
        throw std::invalid_argument( "Coherence: Invalid overlap fraction" );
    mOverlap = ovlp;
    mXCalc.set_overlap( ovlp );
    mYCalc.set_overlap( ovlp );
}

//======================================  Set the sample rate
void
Coherence::set_rate( double rate )
{
    mSampleRate = rate;
}

//======================================  Set the stride length
void
Coherence::set_stride( Interval dt )
{
    mStride = dt;
    mXCalc.set_stride( dt );
    mYCalc.set_stride( dt );
}

//======================================  Set-up for Welch method
void
Coherence::set_welch( Interval stride )
{
    set_stride( stride );
    set_overlap( 0.5 );
    Hamming ham;
    set_window( ham );
}

//======================================  Set the window
void
Coherence::set_window( const window_api& w )
{
    mWindow.set( w.clone( ) );
    mXCalc.set_window( w );
    mYCalc.set_window( w );
}

std::string
Coherence::window_type( void ) const
{
    if ( mWindow.null( ) )
        return "none";
    return ::window_type( mWindow.get( ) );
}
