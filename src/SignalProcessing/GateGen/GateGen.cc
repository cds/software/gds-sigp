/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "GateGen.hh"
#include "DVecType.hh"
#include <stdexcept>
#include <cmath>

using namespace std;

const static double* double_nullptr( NULL );

//======================================  Default constructor
GateGen::GateGen( void )
    : mSampleRate( 16384 ), mThreshold( 1.0 ), mTrigMode( tm_gt ),
      mVetoLimit( 0 ), mVetoMode( tm_null ), mWindow( "tukey" ), mIdle( 1.0 ),
      mActive( 0.0 ), mFront( 0.25 ), mTransit( 0.25 ), mMinWidth( -1 )
{
    reset( );
}

//======================================  Initializing constructor
GateGen::GateGen( double             srate,
                  Interval           transit,
                  Interval           front,
                  Interval           width,
                  const std::string& window )
    : mSampleRate( srate ), mThreshold( 1.0 ), mTrigMode( tm_gt ),
      mVetoLimit( 0 ), mVetoMode( tm_null ), mWindow( window ), mIdle( 1.0 ),
      mActive( 0.0 ), mFront( front ), mTransit( transit ), mMinWidth( -1 )
{
    reset( );
}

//======================================  Initializing constructor
GateGen::GateGen( const GateGen& gg )
{
    mSampleRate = gg.mSampleRate;
    mThreshold = gg.mThreshold;
    mTrigMode = gg.mTrigMode;
    mVetoLimit = gg.mVetoLimit;
    mVetoMode = gg.mVetoMode;
    mWindow = gg.mWindow;
    mIdle = gg.mIdle;
    mActive = gg.mActive;
    mFront = gg.mFront;
    mTransit = gg.mTransit;
    mMinWidth = gg.mMinWidth;
    reset( );
}

//======================================  Destructor
GateGen::~GateGen( void )
{
}

//====================================== cheaty way to extend TSeries with
//                                       non-zero value
inline void
extend_cheat( TSeries& out, const Time& xTime, double val )
{
    out -= val;
    out.extend( xTime );
    out += val;
}

//======================================  Generate a gate
TSeries
GateGen::apply( const TSeries& ts )
{
    if ( !mCurrentTime )
    {
        reset( );
        mCurrentTime = ts.getStartTime( );
        mStartTime = mCurrentTime;
        mWriteTime = mCurrentTime;
        mInstep = ts.getTStep( );
        set_gate( );
    }
    else
    {
        dataCheck( ts );
    }

    TSeries out( mWriteTime, mSampleTime, 0, double_nullptr );
    Time    tEnd = ts.getEndTime( );
    while ( mCurrentTime < tEnd )
    {
        Time tTrig =
            trigger_index( ts.extract( mCurrentTime, tEnd - mCurrentTime ) );
        Time tFront = tTrig - mFront;

        //--------------------------------  Take care of any gate in progress
        if ( mTriggered )
        {
            //-----------------------------  If inside of a gate, the gate must
            //                               first be extended to the stop time.
            if ( mStopTime > mWriteTime )
            {
                extend_cheat( out, mStopTime, mActive );
                mWriteTime = mStopTime;
            }

            //------------------------------  If no transition-on before the end
            //                                of gate, insert the
            //                                trailing-edge data.
            if ( tFront > mStopTime + mTransit )
            {
                out.Append( TSeries( mStopTime, mSampleTime, *mTrailingEdge ) );
                mWriteTime = mStopTime + mTransit;
                mTriggered = false;
            }
        }

        //-----------------------------  Out of gate, add idle data to the
        //                               earliest possible on transition.
        if ( !mTriggered && tFront > mWriteTime )
        {
            extend_cheat( out, tFront, mIdle );
            mWriteTime = tFront;
        }
        mCurrentTime = tTrig;

        //-----------------------------  If a new trigger was found, fill data
        //                               to the stop time.
        if ( tTrig != tEnd )
        {
            mStopTime = tFront + mTransit + mMinWidth;

            //--------------------------  Add transition-on at tFront.
            if ( !mTriggered )
            {
                TSeries lead( tFront, mSampleTime, *mLeadingEdge );
                if ( tFront < mWriteTime )
                    lead.eraseStart( tFront - mWriteTime );
                if ( out.Append( lead ) )
                    throw runtime_error( "GateGen: Append failed" );
                mWriteTime = out.getEndTime( );
                mTriggered = true;
            }

            //--------------------------  Pad active to the gate stop time
            if ( mTriggered && mStopTime > mWriteTime )
            {
                extend_cheat( out, mStopTime, mActive );
                mWriteTime = mStopTime;
            }
            mCurrentTime = tTrig + mInstep;
        }
    }
    return out;
}

//=====================================   Clone a filter
GateGen*
GateGen::clone( void ) const
{
    return new GateGen( *this );
}

//======================================  Default constructor
void
GateGen::dataCheck( const TSeries& ts ) const
{
    if ( !mCurrentTime )
        return;
    if ( mCurrentTime != ts.getStartTime( ) )
    {
        throw runtime_error( "GateGen: Input start time not current" );
    }
    else if ( mInstep != ts.getTStep( ) )
    {
        throw runtime_error( "GateGen: Input sample rate changed" );
    }
}

//======================================  Default constructor
void
GateGen::dump( std::ostream& out ) const
{
    out << "GenGate persistent data:     " << endl;
    out << "  gate sample rate:          " << mSampleRate << endl;
    out << "  selection criterion:       " << test_mode_to_str( mTrigMode )
        << " " << mThreshold << endl;
    out << "  veto criterion:            " << test_mode_to_str( mVetoMode )
        << " " << mVetoLimit << endl;
    out << "  gate waveform:             " << mWindow << endl;
    out << "  idle / active values:      " << mIdle << " / " << mActive << endl;
    out << "  front time (samples):      " << mFront << " ("
        << ( mLeadingEdge ? mLeadingEdge->size( ) : 0 ) << ")" << endl;
    out << "  transition time (samples): " << mTransit << " ("
        << ( mTrailingEdge ? mTrailingEdge->size( ) : 0 ) << ")" << endl;
    out << "  minimum gate width:        " << mMinWidth << endl;
    if ( inUse( ) )
    {
        out << "  input tstep:          " << mInstep << endl;
        out << "  Start time:           " << mStartTime << endl;
        out << "  Current time:         " << mCurrentTime << endl;
        out << "  triggered flag:       " << mTriggered << endl;
        out << "  write time:           " << mWriteTime << endl;
        out << "  gate end time:        " << mStopTime << endl;
    }
    else
    {
        out << "  -- Filter is not in use -- " << endl;
    }
}

//======================================  Default constructor
bool
GateGen::eval_trigger( const DVector& dv, int i ) const
{
    unsigned int mask( mThreshold );
    switch ( mTrigMode )
    {
    case tm_null:
        return false;
    case tm_gt:
        return dv.getDouble( i ) > mThreshold;
    case tm_ge:
        return dv.getDouble( i ) >= mThreshold;
    case tm_lt:
        return dv.getDouble( i ) < mThreshold;
    case tm_le:
        return dv.getDouble( i ) <= mThreshold;
    case tm_eq:
        return dv.getDouble( i ) == mThreshold;
    case tm_ne:
        return dv.getDouble( i ) != mThreshold;
    case tm_bset:
        return ( dv.getInt( i ) & mask ) != 0;
    case tm_bclr:
        return ( ~dv.getInt( i ) & mask ) != 0;
    default:
        break;
    }
    return false;
}

//======================================  Default constructor
bool
GateGen::eval_veto( const DVector& dv, int i ) const
{
    unsigned int mask( mVetoLimit );
    switch ( mVetoMode )
    {
    case tm_null:
        return false;
    case tm_gt:
        return dv.getDouble( i ) > mVetoLimit;
    case tm_ge:
        return dv.getDouble( i ) >= mVetoLimit;
    case tm_lt:
        return dv.getDouble( i ) < mVetoLimit;
    case tm_le:
        return dv.getDouble( i ) <= mVetoLimit;
    case tm_eq:
        return dv.getDouble( i ) == mVetoLimit;
    case tm_ne:
        return dv.getDouble( i ) != mVetoLimit;
    case tm_bset:
        return ( dv.getInt( i ) & mask ) != 0;
    case tm_bclr:
        return ( ~dv.getInt( i ) & mask ) != 0;
    default:
        break;
    }
    return false;
}

//======================================  Flush unwritten data.
TSeries
GateGen::flush( void )
{
    TSeries out( mWriteTime, mSampleTime, 0, double_nullptr );
    if ( inUse( ) && mWriteTime < mCurrentTime )
    {
        if ( !mTriggered )
        {
            extend_cheat( out, mCurrentTime, mIdle );
            mWriteTime = mCurrentTime;
        }
        else
        {
            if ( mCurrentTime > mStopTime )
            {
                extend_cheat( out, mStopTime, mActive );
                out.Append( TSeries( mStopTime, mSampleTime, *mTrailingEdge ) );
                mWriteTime = out.getEndTime( );
                if ( mWriteTime < mCurrentTime )
                {
                    extend_cheat( out, mCurrentTime, mIdle );
                }
                mTriggered = false;
            }
            else
            {
                extend_cheat( out, mCurrentTime, mActive );
                mWriteTime = mCurrentTime;
            }
        }
    }
    //cout << "GateGen: flushed " << out.getInterval() << " seconds of data."
    //	<< endl;
    return out;
}

//======================================  Reset the filter state to unused
void
GateGen::reset( void )
{
    mStartTime = Time( 0 );
    mCurrentTime = Time( 0 );
    mWriteTime = Time( 0 );
    mTriggered = false;
}

//======================================  Set the active signal value
void
GateGen::set_active( double Vactive )
{
    mActive = Vactive;
}

//======================================  Set the front-time parameter
void
GateGen::set_front_time( Interval ft )
{
    mFront = ft;
}

//======================================  Set the gate information.
void
GateGen::set_gate( void )
{
    if ( !mSampleRate )
        throw runtime_error( "GateGen: Sample rate is not valid" );
    if ( fmod( double( mInstep ) * mSampleRate, 1.0 ) != 0 )
    {
        cerr << "warning: Gated signal rate (" << mSampleRate
             << ") is not an integer multiple of the input rate ("
             << 1.0 / double( mInstep ) << ")." << endl;
    }
    mSampleTime = Interval( 1.0 / mSampleRate );

    //-----------------------------------  Calculate # transition time samples
    double samples = mTransit * mSampleRate;
    long   transitlen = long( samples + 0.5 );
    if ( transitlen != samples )
    {
        cerr << "warning: Gate transition time is not an even number of samples"
             << endl;
        mTransit = double( transitlen ) * mSampleTime;
        cerr << "Transition time reset to " << mTransit << endl;
    }

    //-----------------------------------  Calculate # pre-trigger time samples
    double fsamples = mFront * mSampleRate;
    long   frontlen = long( fsamples + 0.5 );
    if ( frontlen != fsamples )
    {
        cerr
            << "warning: Gate pre-trigger time is not an even number of samples"
            << endl;
        mFront = double( frontlen ) * mSampleTime;
        cerr << "Pre-trigger time reset to " << mFront << endl;
    }
    cout << "GenGate::set_gate: transit_len: " << transitlen
         << " front_len: " << frontlen << " window: " << mWindow << endl;

    //-----------------------------------  Check the width: -1 is replaced by
    //                                     excess front time + sample width
    if ( mMinWidth < Interval( 0 ) )
        mMinWidth = mFront - mTransit + mInstep;

    //-------------------------------------------------------------------------
    //  Generate leading (idle->active) and trailing (active->idle) edge of
    //  the window function
    //-------------------------------------------------------------------------
    DVectD* lead = new DVectD( frontlen );
    DVectD* trail = new DVectD( transitlen );
    mLeadingEdge.reset( lead );
    mTrailingEdge.reset( trail );

    //-----------------------------------  Generate tukey window
    if ( mWindow == "tukey" )
    {
        double a = mActive - mIdle;
        double b = mIdle;
        double k = M_PI / ( 2 * transitlen );
        for ( size_t i = 1; i <= transitlen; i++ )
        {
            double x = pow( sin( k * i ), 2 ) * a + b;
            ( *lead )[ i - 1 ] = x;
            ( *trail )[ transitlen - i ] = x;
        }
        for ( size_t i = transitlen; i < frontlen; i++ )
        {
            ( *lead )[ i ] = mActive;
        }
    }

    //-----------------------------------  Generate rectangular window
    else if ( mWindow == "rectangle" )
    {
        lead->replace_with_zeros( 0, frontlen, frontlen );
        ( *lead ) += mActive;
        trail->replace_with_zeros( 0, transitlen, transitlen );
        ( *trail ) += mActive;
    }

    //-----------------------------------  Invalid window string.
    else
    {
        throw runtime_error( string( "GateGen: Unknown window:" ) + mWindow );
    }
}

//======================================  Idle output value
void
GateGen::set_idle( double Vidle )
{
    mIdle = Vidle;
}

//======================================  Minimum time in active state.
void
GateGen::set_min_width( Interval wt )
{
    mMinWidth = wt;
}

//======================================  Set the output sample rate
void
GateGen::set_sample_rate( double rate )
{
    mSampleRate = rate;
}

//======================================  Set the selection criteria
void
GateGen::set_selection( const std::string& mode, double thresh )
{
    mTrigMode = test_mode_from_str( mode );
    mThreshold = thresh;
}

//======================================  Set the transition time.
void
GateGen::set_transit_time( Interval tt )
{
    mTransit = tt;
}

//======================================  Set the selection criteria
void
GateGen::set_veto( const std::string& mode, double thresh )
{
    mVetoMode = test_mode_from_str( mode );
    mVetoLimit = thresh;
}

//======================================  Set window finction name
void
GateGen::set_window( const std::string& win )
{
    mWindow = win;
}

//======================================  Find the first triggering sample
Time
GateGen::trigger_index( const TSeries& ts ) const
{
    const DVector& dv = *( ts.refDVect( ) );
    size_t         N = dv.size( );
    size_t         index = N;
    for ( size_t i = 0; i < N; i++ )
    {
        if ( eval_trigger( dv, i ) && !eval_veto( dv, i ) )
        {
            index = i;
            break;
        }
    }
    return ts.getBinT( index );
}

//=====================================  Convert string to test mode
GateGen::test_mode
GateGen::test_mode_from_str( const std::string& str )
{
    if ( str.empty( ) )
        return tm_null;
    if ( str == "<" )
        return tm_lt;
    if ( str == "<=" )
        return tm_le;
    if ( str == ">" )
        return tm_gt;
    if ( str == ">=" )
        return tm_ge;
    if ( str == "==" )
        return tm_eq;
    if ( str == "!=" )
        return tm_ne;
    if ( str == "&" )
        return tm_bset;
    if ( str == "&~" )
        return tm_bclr;
    return tm_null;
}

//=====================================  Convert test mode to string
std::string
GateGen::test_mode_to_str( test_mode tm )
{
    switch ( tm )
    {
    case tm_null:
        return "";
    case tm_lt:
        return "<";
    case tm_le:
        return "<=";
    case tm_gt:
        return ">";
    case tm_ge:
        return ">=";
    case tm_eq:
        return "==";
    case tm_ne:
        return "!=";
    case tm_bset:
        return "&";
    case tm_bclr:
        return "&~";
    }
    return "";
}
