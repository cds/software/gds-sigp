/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "GateVeto.hh"
#include "DVecType.hh"

using namespace std;

//======================================  Default constructor
GateVeto::GateVeto( void )
    : _integrationTime( 1.0 ), _maxAccum( 1.0 ), _padTime( 0 ),
      _sampleTime( 0.0 ), _triggerMode( tm_null ), _threshold( 1.0 ),
      _idle( 0 ), _active( 1.0 ), _unvetoed( false ), _integrateSamples( 0 ),
      _accumulateSamples( 0 ), _padSamples( 0 ), _accumulator( 0 ),
      _pad_accumulator( 0 ), _startTime( 0 ), _currentTime( 0 )
{
}

//======================================  Destructor
GateVeto::~GateVeto( void )
{
}

//======================================  Clone a GateVeto instance.
GateVeto*
GateVeto::clone( void ) const
{
    return new GateVeto( *this );
}

//======================================  Clone a GateVeto instance.
TSeries
GateVeto::apply( const TSeries& in )
{
    TSeries out( in.getStartTime( ), in.getTStep( ), 1, &_idle );
    out.extend( in.getEndTime( ) );
    if ( !inUse( ) )
    {
        _startTime = in.getStartTime( );
        _currentTime = _startTime;
        _sampleTime = in.getTStep( );
        setup( );
    }
    else
    {
        dataCheck( in );
    }

    const DVector& dv = *( in.refDVect( ) );
    DVectD&        dvd = dynamic_cast< DVectD& >( *out.refDVect( ) );
    size_t         N = in.getNSample( );
    for ( size_t i = 0; i < N; i++ )
    {
        unsigned int mask( _threshold );
        bool         rci = false;
        switch ( _triggerMode )
        {
        case tm_gt:
            rci = dv.getDouble( i ) > _threshold;
            break;
        case tm_ge:
            rci = dv.getDouble( i ) >= _threshold;
            break;
        case tm_lt:
            rci = dv.getDouble( i ) < _threshold;
            break;
        case tm_le:
            rci = dv.getDouble( i ) <= _threshold;
            break;
        case tm_eq:
            rci = dv.getDouble( i ) == _threshold;
            break;
        case tm_ne:
            rci = dv.getDouble( i ) != _threshold;
            break;
        case tm_bset:
            rci = ( dv.getInt( i ) & mask ) != 0;
            break;
        case tm_bclr:
            rci = ( ~dv.getInt( i ) & mask ) != 0;
            break;
        default:
            break;
        }

        //--------------------------------  Accumulate and store the trigger
        int save_last = _queue.back( );
        int save_first = _queue.front( );
        _accumulator -= save_first;
        _queue.pop( );
        if ( rci )
        {
            _queue.push( 1 );
            _accumulator += 1;
        }
        else
        {
            _queue.push( 0 );
        }

        //--------------------------------  test whether veto is active
        if ( _accumulator > _accumulateSamples && rci )
        {
            dvd[ i ] = _active;
            if ( _unvetoed )
            {
                _accumulator -= _queue.back( );
                _queue.back( ) = 0;
            }
        }
        else
        {
            dvd[ i ] = _idle;
        }
        if ( save_first && !_queue.front( ) )
        {
            if ( _pad_accumulator < _padSamples )
            {
                throw logic_error( "GateVeto: error tracking padding" );
            }
            _accumulator -= _padSamples;
            _pad_accumulator -= _padSamples;
        }
        if ( !save_last && _queue.back( ) )
        {
            _accumulator += _padSamples;
            _pad_accumulator += _padSamples;
        }
    }
    _currentTime = in.getEndTime( );
    return out;
}

//======================================  Clone a GateVeto instance.
void
GateVeto::dataCheck( const TSeries& in ) const
{
    if ( !inUse( ) )
        return;
    if ( in.getStartTime( ) != _currentTime )
    {
        throw runtime_error( "GateVeto: invalid series start time" );
    }
    if ( _sampleTime != in.getTStep( ) )
    {
        throw runtime_error( "GateVeto: invalid series sample rate" );
    }
}
//======================================  Default constructor
void
GateVeto::dump( std::ostream& out ) const
{
    out << "GateVeto persistent data:    " << endl;
    out << "  selection criterion:       " << test_mode_to_str( _triggerMode )
        << " " << _threshold << endl;
    out << "  idle / active values:      " << _idle << " / " << _active << endl;
    out << "  integration time (secs):   " << _integrationTime << endl;
    out << "  maximum cumulative time:   " << _maxAccum << endl;
    out << "  gate padding time:         " << _padTime << endl;
    if ( inUse( ) )
    {
        out << "  input tstep:          " << _sampleTime << endl;
        out << "  Start time:           " << _startTime << endl;
        out << "  Current time:         " << _currentTime << endl;
        out << "  accumulated samples:  " << _accumulator << endl;
        out << "  accumulated padding:  " << _pad_accumulator << endl;
    }
    else
    {
        out << "  -- Filter is not in use -- " << endl;
    }
}

//======================================  Clone a GateVeto instance.
void
GateVeto::reset( void )
{
    _startTime = Time( 0 );
    _currentTime = _startTime;
    while ( !_queue.empty( ) )
        _queue.pop( );
    _accumulator = 0;
    _pad_accumulator = 0;
}

//======================================  Set integration time.
void
GateVeto::set_active( double active )
{
    _active = active;
}

//======================================  Set integration time.
void
GateVeto::set_idle( double idle )
{
    _idle = idle;
}

//======================================  Set integration time.
void
GateVeto::set_integration_time( Interval it )
{
    _integrationTime = it;
}

//======================================  Set maximum accumulation time
void
GateVeto::set_max_accumulation( Interval mt )
{
    _maxAccum = mt;
}

//======================================  Set maximum accumulation time
void
GateVeto::set_pad_time( Interval pt )
{
    _padTime = pt;
}

//======================================  Set trigger ctriteria
void
GateVeto::set_selection( const std::string& oper, double thresh )
{
    _triggerMode = test_mode_from_str( oper );
    _threshold = thresh;
}

//======================================  Set trigger ctriteria
void
GateVeto::set_unvetoed( bool uv )
{
    _unvetoed = uv;
}

//======================================  Set up timing counts
void
GateVeto::setup( void )
{
    if ( _sampleTime <= Interval( 0 ) )
    {
        throw runtime_error( "GateVeto::setup: Invalid sample time." );
    }
    _accumulateSamples = size_t( _maxAccum / _sampleTime + 0.5 );
    if ( !_accumulateSamples )
        _accumulateSamples = 1;
    if ( _integrationTime > Interval( 0 ) )
    {
        _integrateSamples = size_t( _integrationTime / _sampleTime + 0.5 );
    }
    else
    {
        _integrateSamples = _accumulateSamples;
    }
    _padSamples = size_t( _padTime / _sampleTime + 0.5 );

    for ( size_t i = 0; i < _integrateSamples; i++ )
    {
        _queue.push( 0 );
    }
    _accumulator = 0;
    _pad_accumulator = 0;
}

//=====================================  Convert string to test mode
GateVeto::test_mode
GateVeto::test_mode_from_str( const std::string& str )
{
    if ( str.empty( ) )
        return tm_null;
    if ( str == "<" )
        return tm_lt;
    if ( str == "<=" )
        return tm_le;
    if ( str == ">" )
        return tm_gt;
    if ( str == ">=" )
        return tm_ge;
    if ( str == "==" )
        return tm_eq;
    if ( str == "!=" )
        return tm_ne;
    if ( str == "&" )
        return tm_bset;
    if ( str == "&~" )
        return tm_bclr;
    return tm_null;
}

//=====================================  Convert test mode to string
std::string
GateVeto::test_mode_to_str( test_mode tm )
{
    switch ( tm )
    {
    case tm_null:
        return "";
    case tm_lt:
        return "<";
    case tm_le:
        return "<=";
    case tm_gt:
        return ">";
    case tm_ge:
        return ">=";
    case tm_eq:
        return "==";
    case tm_ne:
        return "!=";
    case tm_bset:
        return "&";
    case tm_bclr:
        return "&~";
    }
    return "";
}
