/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef GATEGEN_HH
#define GATEGEN_HH
#include "Time.hh"
#include "Pipe.hh"
#include <string>
#include <memory>
#include <iosfwd>

class DVector;

/**  The GateGen class is used to generate a multiplicative gate signal. The gate
  *  signal transitions from the 'idle' state (by default 1.0) to the 'active' 
  *  (asserted) state (by default 0.0) when a trigger condition is met by the 
  *  input signal. The trigger condition may result from the input signal being
  *  greater or less than a threshold value. or when a bit defined by the 
  *  threshold value set in the input stream. The transition from the idle to 
  *  the value to the the active value and back are according to the specified 
  *  window (by default "tukey").
  *  
  *  \verbatim
      Timing diagram:
                                       _
      Trigger signal   _______________| |_____________
     
                       _______                  ______   __ 'idle'
      Gate                    \                /
                               \              /
                                \____________/           __ 'active'
      Time         front:    |<      >|
                 transit:    |<>|            |<>|
                   width:       |<          >|
     \endverbatim
  *
  *  \author John.zweizig <john.zweizig@ligo.org>
  *  \version $Id$
  */
class GateGen : public Pipe
{
public:
    /** enumerate test modes.
     */
    enum test_mode
    {
        tm_null, ///<  Always false
        tm_lt, ///<  "<"
        tm_le, ///<  "<="
        tm_gt, ///<  ">"
        tm_ge, ///<  ">="
        tm_eq, ///<  "=="
        tm_ne, ///<  "!="
        tm_bset, ///<  "&"
        tm_bclr ///<  "&~"
    };

public:
    /**  Default GateGen constructor.
     */
    GateGen( void );

    /**  Construct a GateGen filter and initialize the gate parameter.
      *  \brief Initializing GateGen constructor.
      *  \param srate   Sample rate of the output stream.
      *  \param transit Time taken for the transition from the idle state to
      *                 the active state and visa-versa.
      *  \param front   Time from the start of the signal transition to the 
      *                 onset of trigger condition. Note that the gate signal
      *                 precedes the trigger to allow it to be used to veto
      *                 the triggered transient.
      *  \param width   The time difference from the last sample meeting the 
      *                 trigger condition to the start of the transition from 
      *                 active to idle state.
      *  \param window  
      */
    GateGen( double             srate,
             Interval           transit = 0.25,
             Interval           front = 0.25,
             Interval           width = 0,
             const std::string& window = "tukey" );

    /**  Construct a GateGen instance with identical gate parameters, but not
      *  in use (i.e. the filter has been reset).
      *  \brief Copy constructor
      *  \param gg %GateGen instance to be copied.
      */
    GateGen( const GateGen& gg );

#ifndef __CINT__
    /**  GateGen move syntax constructor.
      *  \brief Move constructor
      *  \param g Instance to be moved.
      */
    GateGen( GateGen&& g ) = default;
#endif

    /**  Clean up and go away.
      *  \brief Destructor.
      */
    ~GateGen( void );

    /**  Generate a gate signal from the input time series. Note that neither 
      *  sample rate nor the time interval covered by the output series are 
      *  the same as those of the input series.
      *  \brief Generate a gate signal
      *  \param ts Trigger input time series
      *  \return Gate signal time series
      */
    TSeries apply( const TSeries& ts );

    /**  Clone a GateGen instance.
      *  \brief Clone this instance.
      *  \return Pointer to the new GateGen instance.
      */
    GateGen* clone( void ) const;

    /**  Check that the argument time series has correct start time and time
      *  step to be the next input to this filter.
      *  \brief Check that time series is valid.
      *  \param ts Time series to be checked for validity.
     */
    void dataCheck( const TSeries& ts ) const;

    /**  Print the current %GateGen parameters and state information to the 
      *  specified argument stream.
      *  \brief Dump the filter state.
      *  \param out Output stream to which the state information are written.
      */
    void dump( std::ostream& out ) const;

    /**  Evaluate the selection criterion.
     */
    bool eval_trigger( const DVector& dv, int i ) const;

    /**  Evaluate the selection criterion.
     */
    bool eval_veto( const DVector& dv, int i ) const;

    /**  Flush the output series until the current input time. Note that in 
      *  some cases the output series will already contain anticipated gate 
      *  transition samples up to or beyond the current time. In these cases,
      *  an empty series is returned.
      *  \brief Flush anticipated data assuming no further triggers.
      *  \return Gate signal series data
      */
    TSeries flush( void );

    /**  Get time stamp of next expected input sample. If the filter is 
      *  not in use, the current time is not defined.
      *  \brief Get the current time.
      *  \return Expected time of next sample for processing.
      */
    Time getCurrentTime( void ) const;

    /**  Get time stamp OF first input sample processed by this filter. If
      *  the filter is not in use, this method should return Time(0).
      *  \brief Processing start time.
      *  \return Processing start time.
      */
    Time getStartTime( void ) const;

    /**  Test whether filter is in use (i.e. whether the filter has been 
      *  applied to a time series.
      *  \brief Test whether filter is in use.
      *  \return true if the filter has been applied to data.
      */
    bool inUse( void ) const;

    /**  Reset the state parameters to their default (not in use) values 
      *  leaving the settable parameters unchanged.
      *  \brief Reset the filter
      */
    void reset( void );

    /**  Set the active state value. The result time series is set to the 
      *  active state value when within the triggered gate times.
      *  \brief Set the active state value.
      *  \param Vactive Active state value.
      */
    void set_active( double Vactive );

    /**  Set the time before the trigger of the start of idle->active 
      *  transition.
      *  \brief Set the delay from transition to trigger time.
      *  \param ft front time in seconds.
      */
    void set_front_time( Interval ft );

    /**  Set up the gate parameters. This method is called by the apply method 
      *  the first time it is called and after each reset. Parameters 
      *  initialized by set_gate include:
      *   
      *  * the calculated transition segments (up and down)
      *  * the default minimum gate width, if not specified.
      *  * check internal timing consistency
      *  
      *  \brief set gate parameters.
      */
    void set_gate( void );

    /**  Set the idle state value. The result time series is set to the idle 
      *  state value when outside of the triggered gate times.
      *  \brief  Set the idle state value.
      *  \param Vidle Idle state value.
      */
    void set_idle( double Vidle );

    /**  Set the minimum gate "fully active" time. By default the active time 
      *  is set to -1 and is overridden by the interval from the end of the
      *  transition to the end of the triggered input sample. The default value 
      *  is set when the gate parameters are set (see set_gate).
      *  \brief Minimum full active gate time.
      *  \param mt minimum time.
      */
    void set_min_width( Interval mt );

    /**  Set the output series sample rate.
      *  \brief set output sample rate.
      *  \param rate Sample rate.
      */
    void set_sample_rate( double rate );

    /**  Set the trigger selection criteria. Setting the selection criteria
      *  requires two parameters, an operator and a numeric threshold.
      *  <table>
      *  <tr><th>Operator</th><th>meaning</th></tr>
      *  <tr><td>\c "<" </td><td> true if \f$x(t) < threshold \f$ </td></tr>
      *  <tr><td>\c "<=" </td><td> true if \f$x(t) \le threshold \f$ </td></tr>
      *  <tr><td>\c ">" </td><td> true if \f$x(t) > threshold \f$ </td></tr>
      *  <tr><td>\c ">=" </td><td> true if \f$x(t) \ge threshold \f$ </td></tr>
      *  <tr><td>\c "==" </td><td> true if \f$x(t) = threshold \f$ </td></tr>
      *  <tr><td>\c "!=" </td><td> true if \f$x(t) \ne threshold \f$ </td></tr>
      *  <tr><td>\c "&" </td>
      *      <td>true if \f$int(x(t)) \, \& \, int(threshold) \ne 0\f$</td></tr>
      *  <tr><td>\c "&~" </td>
      *       <td>true if \f$\sim int(x(t)) \, \& \, int(threshold) \ne 0\f$</td></tr>
      *  </table>
      *  \brief set trigger condition
      *  \param oper   Operator string as specified in the table
      *  \param thresh threshold operand
      */
    void set_selection( const std::string& oper, double thresh );

    /**  State transition time from active -> idle and from idle -> active.
      *  \brief Set gate transition time
      *  \param tt Transition time.
      */
    void set_transit_time( Interval tt );

    /**  Set the veto selection criterion. Setting the veto criterion
      *  requires two parameters, an operator and a numeric threshold.
      *  <table>
      *  <tr><th>Operator</th><th>meaning</th></tr>
      *  <tr><td>\c "<" </td><td> true if \f$x(t) < threshold \f$ </td></tr>
      *  <tr><td>\c "<=" </td><td> true if \f$x(t) \le threshold \f$ </td></tr>
      *  <tr><td>\c ">" </td><td> true if \f$x(t) > threshold \f$ </td></tr>
      *  <tr><td>\c ">=" </td><td> true if \f$x(t) \ge threshold \f$ </td></tr>
      *  <tr><td>\c "==" </td><td> true if \f$x(t) = threshold \f$ </td></tr>
      *  <tr><td>\c "!=" </td><td> true if \f$x(t) \ne threshold \f$ </td></tr>
      *  <tr><td>\c "&" </td>
      *      <td>true if \f$int(x(t)) \, \& \, int(threshold) \ne 0\f$</td></tr>
      *  <tr><td>\c "&~" </td>
      *       <td>true if \f$\sim int(x(t)) \, \& \, int(threshold) \ne 0\f$</td></tr>
      *  </table>
      *  \brief Set the condition
      *  \param oper   Operator string as specified in the table
      *  \param thresh threshold operand
      */
    void set_veto( const std::string& oper, double thresh );

    /**  Set the window function used to transition from idle to active and 
      *  visa versa. Two windows are currently supported.
      *  * "tukey" transitions from off state idle (0) to on (1) as 
      *    \f$tukey(x) = sin^2(x)\f$ in the range \f$(0 < x \le \pi / 2)\f$.
      *  * "rectangle" window transition time is intrinsically zero. The window 
      *    is therefore on for the entire specified transition length in both 
      *    transition directions.
      *  \brief Set the transition window.
      *  \param win Window function name string.
      */
    void set_window( const std::string& win );

    /**  Trigger index searches through the time series for the first/next 
      *  sample satisfying the trigger condition.
      *  \brief Find first sample satisfying trigger condition.
      *  \param ts %Time series to be searched.
      *  \return Index of first sample to satisfy the trigger condition or the
      *          length of the time series.
      */
    Time trigger_index( const TSeries& ts ) const;

    static test_mode   test_mode_from_str( const std::string& str );
    static std::string test_mode_to_str( test_mode tm );

private:
    double      mSampleRate; ///< gate sample rate
    double      mThreshold; ///< signal threshold
    test_mode   mTrigMode; ///< trigger mode: "<", ">" or "&"
    double      mVetoLimit; ///< veto threshold
    test_mode   mVetoMode; ///< veto mode: "<", ">" or "&"
    std::string mWindow; ///< gate waveform
    double      mIdle; ///< Idle gate value
    double      mActive; ///< Active gate value
    Interval    mFront; ///< Time from start of transition to trigger.
    Interval    mTransit; ///< gate rise/fall time
    Interval    mMinWidth; ///< minimum gate width (excluding transition)
    std::unique_ptr< DVector > mLeadingEdge;
    std::unique_ptr< DVector > mTrailingEdge;
    Interval                   mInstep; ///< input series sample spacing.
    Interval                   mSampleTime; ///< output series sample spacing.
    Time                       mStartTime; ///< Time of first sample processed.
    Time mCurrentTime; ///< Next expected sample to process
    bool mTriggered; ///< trigger state at current time.
    Time mWriteTime; ///< next output sample to be written.
    Time mStopTime; ///< end of gate off time.
};

//======================================  Inline methods
inline Time
GateGen::getStartTime( void ) const
{
    return mStartTime;
}

inline Time
GateGen::getCurrentTime( void ) const
{
    return mCurrentTime;
}

inline bool
GateGen::inUse( void ) const
{
    return mCurrentTime != Time( 0 );
}

#endif // !defined(GATEGEN_HH)
