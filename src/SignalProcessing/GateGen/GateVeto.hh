/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef GATEVETO_HH
#define GATEVETO_HH
#include "Pipe.hh"
#include "Time.hh"
#include <queue>
#include <iosfwd>

/**  The GateVeto filter class generates a veto signal when a condition has 
  *  asserted for longer than a specified time within a sample interval.
  *
  *  \author John Zweizig <john.zweizig@ligo.org>
  *  \version 25 May 2019
  */
class GateVeto : public Pipe
{
public:
    /** enumerate test modes.
     */
    enum test_mode
    {
        tm_null, ///<  Always false
        tm_lt, ///<  "<"
        tm_le, ///<  "<="
        tm_gt, ///<  ">"
        tm_ge, ///<  ">="
        tm_eq, ///<  "=="
        tm_ne, ///<  "!="
        tm_bset, ///<  "&"
        tm_bclr ///<  "&~"
    };

public:
    /**  Default constructor.
    */
    GateVeto( void );

    /** Copy constructor.
    */
    GateVeto( const GateVeto& gv )
#ifndef __CINT__
        = default
#endif /* __CINT__ */
        ;

    /** Destroy a gate veto instance.
     */
    virtual ~GateVeto( void );

    /** Clone a gateVeto instance.
     */
    virtual GateVeto* clone( void ) const;

    /** Generate a gate veto based on the input time series.
    */
    virtual TSeries apply( const TSeries& in );

    /** Test that the time series is valid input for this instance.
    */
    virtual void dataCheck( const TSeries& in ) const;

    /**  Dump the gateVeto state information.
    */
    void dump( std::ostream& out ) const;

    /** Get the processing start time.
    */
    virtual Time getStartTime( void ) const;

    /** Get the time stamp of the next input data to be processed.
    */
    virtual Time getCurrentTime( void ) const;

    /**  Test whether the filter instance is in use (\e i.e. if it has been 
     *  applied to at least one input time series).
     */
    virtual bool inUse( void ) const;

    /** Reset the time series.
    */
    virtual void reset( void );

    /** set the active data value..
     */
    void set_active( double acive );

    /** set the idle data value.
     */
    void set_idle( double idle );

    /** set the integration time.
     */
    void set_integration_time( Interval it );

    /**  Set the maximum accumulated active time over an arbitrary span of 
     *  the integration time.
     */
    void set_max_accumulation( Interval mt );

    /**  Set the length of padding when a gate is actuated.
     */
    void set_pad_time( Interval mt );

    /**  Set the trigger selection criteria. Setting the selection criteria
      *  requires two parameters, an operator and a numeric threshold.
      *  <table>
      *  <tr><th>Operator</th><th>meaning</th></tr>
      *  <tr><td>\c "<" </td><td> true if \f$x(t) < threshold \f$ </td></tr>
      *  <tr><td>\c "<=" </td><td> true if \f$x(t) \le threshold \f$ </td></tr>
      *  <tr><td>\c ">" </td><td> true if \f$x(t) > threshold \f$ </td></tr>
      *  <tr><td>\c ">=" </td><td> true if \f$x(t) \ge threshold \f$ </td></tr>
      *  <tr><td>\c "==" </td><td> true if \f$x(t) = threshold \f$ </td></tr>
      *  <tr><td>\c "!=" </td><td> true if \f$x(t) \ne threshold \f$ </td></tr>
      *  <tr><td>\c "&" </td>
      *      <td>true if \f$int(x(t)) \, \& \, int(threshold) \ne 0\f$</td></tr>
      *  <tr><td>\c "&~" </td>
      *       <td>true if \f$\sim int(x(t)) \, \& \, int(threshold) \ne 0\f$</td></tr>
      *  </table>
      *  \brief set trigger condition
      *  \param oper   Operator string as specified in the table
      *  \param thresh threshold operand
      */
    void set_selection( const std::string& oper, double thresh );

    /**  Set unvetoed state which causes the trigger counts to be incremented 
     *  only if a veto is not generated.
     *  \brief Set unvetoed state.
     *  \param uv New unvetoed state.
     */
    void set_unvetoed( bool uv );

private:
    /** set all the timing counts and resize the timing queues as appropriate.
    */
    void               setup( void );
    static test_mode   test_mode_from_str( const std::string& str );
    static std::string test_mode_to_str( test_mode tm );

private:
    Interval          _integrationTime;
    Interval          _maxAccum;
    Interval          _padTime;
    Interval          _sampleTime;
    test_mode         _triggerMode;
    double            _threshold;
    double            _idle;
    double            _active;
    bool              _unvetoed;
    size_t            _integrateSamples;
    size_t            _accumulateSamples;
    size_t            _padSamples;
    std::queue< int > _queue;
    size_t            _accumulator;
    size_t _pad_accumulator; /// current padding sample count (safety)
    Time   _startTime;
    Time   _currentTime;
};

//====================================== inlince GateVeto methods
inline Time
GateVeto::getStartTime( void ) const
{
    return _startTime;
}

inline Time
GateVeto::getCurrentTime( void ) const
{
    return _currentTime;
}

inline bool
GateVeto::inUse( void ) const
{
    return _startTime != Time( 0 );
}

#endif // !defined(GATEVETO_HH)
