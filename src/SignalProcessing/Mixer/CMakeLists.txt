
add_library(Mixer OBJECT
    Mixer.cc
)

target_include_directories(Mixer PRIVATE
    ${CMAKE_BINARY_DIR}/src/config

    ${CMAKE_SOURCE_DIR}/src/SignalProcessing/FilterBase

    ${CMAKE_SOURCE_DIR}/src/Base/complex
    ${CMAKE_SOURCE_DIR}/src/Base/constant
    ${CMAKE_SOURCE_DIR}/src/Base/thread
    ${CMAKE_SOURCE_DIR}/src/Base/time

    ${CMAKE_SOURCE_DIR}/src/Containers/CWVec
    ${CMAKE_SOURCE_DIR}/src/Containers/DVector
    ${CMAKE_SOURCE_DIR}/src/Containers/FilterIO
    ${CMAKE_SOURCE_DIR}/src/Containers/TSeries
)

install(FILES
    Mixer.hh
    UnitTypes.hh
    DESTINATION include/gds-sigp
)
