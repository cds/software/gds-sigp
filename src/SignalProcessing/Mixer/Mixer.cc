//
// Mixer filter class implementation
//

// $Id$
// $Log$
// Revision 1.9  2007/04/18 06:37:20  jzweizig
// Explicit Time constructor
//
// Revision 1.8  2005/01/31 01:50:59  sigg
// Support for Windows with cygwin/gcc
//
// Revision 1.7  2004/12/22 02:11:31  sigg
// Fix for Sun Studio 9 compiler
//
// Revision 1.6  2003/08/12 19:00:48  jzweizig
// Fix current time counter
//
// Revision 1.5  2002/07/18 21:35:34  jzweizig
// Do phase calculations in double precision
//
// Revision 1.4  2002/02/12 09:35:11  sigg
// SUN CC/Solaris 8 fixes
//
// Revision 1.3  2001/12/14 20:45:54  jzweizig
// Add clone method
//
// Revision 1.2  2001/10/07 06:05:58  sigg
// fixed SUN C++ problems
//
// Revision 1.1  2001/10/01 22:56:37  jzweizig
// Original Version
//
// Revision 1.4  2000/10/18 18:14:33  jzweizig
// Resolve ambiguous promotions
//
// Revision 1.3  2000/06/22 00:31:41  jzweizig
// Remove throw clauses from all FilterBase and Pipe methods
//
// Revision 1.2  1999/11/30 02:20:18  jzweizig
// move constant.hh to Mixer.cc
//
// Revision 1.1  1999/10/27 23:34:03  jzweizig
// New mixer (heterodyne) class
//

#include <time.h>
#include <stdexcept>
#include <string>
#include <math.h>
#include "Mixer.hh"
#include "PConfig.h"
#include "TSeries.hh"
#include "DVecType.hh"
#include "Complex.hh"
#include "constant.hh"
#include "KeyChain.hh"
#include "Key.hh"

// ---------------------------------------- Default Constructor
Mixer::Mixer( ) : bInUse( false )
{
}

// ---------------------------------------- Copy Constructor
Mixer::Mixer( const Mixer& x )
    : bInUse( false ), fc( x.fc ), dt( x.dt ), phase( x.phase ), dphi( x.dphi ),
      tStart( Time( 0 ) ), tCurrent( Time( 0 ) )
{
}

// ---------------------------------------- Destructor

Mixer::~Mixer( )
{
}

// ---------------------------------------- Clone method
Mixer*
Mixer::clone( void ) const
{
    return new Mixer( *this );
}

// ---------------------------------------- apply ()
TSeries
Mixer::apply( const TSeries& in )
{

    if ( bInUse == false )
        initialize( in ); // Initialize on first use

    dataCheck( in ); // validate data

    // Extract the data to a complex vector

    unsigned long        nsamp( in.getNSample( ) );
    DVecType< fComplex > dv( nsamp );
    fComplex*            z = dv.refTData( );

    // Mix with local oscillator
    // for(;cond;) evaluates cond before the loop body; so,
    // this is safe against nsamp == 0
    // Note that there should be a way to find the number of samples after
    // which the series repeats. If this is an integer, only that number of
    // coefficients needs to be generated. one special case is if both the
    // hetrodyne and the sample frequencies are integers. Then the mixer will
    // repeat in a 1-second stride.

    double phi = phase;
    double delta_phi = dphi;
    for ( unsigned long k = 0; k < nsamp; ++k )
    {
#ifdef USE_SINCOS
        double sinphi, cosphi;
        sincos( phi, &sinphi, &cosphi );
        z[ k ] = fComplex( cosphi, sinphi );
#else
        z[ k ] = fComplex( cos( phi ), sin( phi ) );
#endif
        phi = fmod( phi + delta_phi, twopi ); // avoid over/under flows
    }
    phase = phi;
    dv *= *in.refDVect( );

    // Final clean up: Put the data back into a TSeries with same time,
    // sampling period as the input; mark the TSeries as heterodyned;
    // set the current time and return

    TSeries tsout( in.getStartTime( ), dt, dv );
    tsout.setF0( float( fc ) + in.getF0( ) );

    // Clean-up filter state

    tCurrent = in.getEndTime( );

    // We're done
    return tsout;
}

// ---------------------------------------- dataCheck ()

void
Mixer::dataCheck( const TSeries& in ) const
{
    // Check legitimacy of input time series:
    //     Continuity: Filter & TSeries in sync?
    //     Consistency: Filter rate same as TSeries rate?
    //     Other validity: TStep > 0?

    // Note: We can't assume filter is in use

    // Tests when filter in use
    if ( inUse( ) )
    {
        if ( in.getStartTime( ) != tCurrent )
            throw std::invalid_argument(
                "input start time != filter current time" );
        if ( in.getTStep( ) != dt )
            throw std::invalid_argument(
                "input, filter sample rates different" );
    }

    // Tests always
    if ( in.getTStep( ) <= Interval( 0.0 ) )
        throw std::out_of_range( "input TSeries sample rate < 0" );

    return;
};

// ---------------------------------------- Accessors

void
Mixer::reset( void )
{
    fc.reset( );
    phase.reset( );
    tStart.reset( );
    tCurrent.reset( );
    dt.reset( );

    bInUse = false;

    return;
}

const KeyChain&
Mixer::getInputKeys( void ) const
{
    throw std::runtime_error( "Unimplemented" );
}

const KeyChain&
Mixer::getOutputKeys( void ) const
{
    throw std::runtime_error( "Unimplemented" );
}

Time
Mixer::getStartTime( void ) const
{
    return tStart;
}

Time
Mixer::getCurrentTime( void ) const
{
    return tCurrent;
}

float
Mixer::getPhase( void ) const
{
    return phase;
}

void
Mixer::setPhase( const float& phi )
{
    if ( bInUse == true )
        throw std::logic_error( "filter in use" );

    phase = fmod( (double)phi, twopi ); // reduce to range

    return;
}

/** Set the local oscillator frequency in Hz. Throws logic_error if called
 *  while filter in use. 
 *  @memo Set the local oscillator frequency in Hz. 
 *  @param fc local oscillator frequency in Hz
 */
void
Mixer::setFcHz( const float& f )
{
    if ( bInUse == true )
        throw std::logic_error( "filter in use" );

    fc = f;
    fc = Hz;

    return;
}

/** Set the local oscillator frequency in Nyquist frequency units. 
 *  Throws out_of_range if oscillator frequency not in range (-1,1), 
 *  logic_error if called while filter in use. 
 *  @memo Sets the local oscillator frequency in Nyquist frequency units.
 *  @param fc   local oscillator frequency in Nyquist units
 */
void
Mixer::setFcNyquist( const float& f )
{
    if ( bInUse == true )
        throw std::logic_error( "filter in use" );
    fc = f;
    fc = Nyquist;

    return;
}

// ---------------------------------------- Inquiry

bool
Mixer::inUse( void ) const
{
    return bInUse;
}

bool
Mixer::getPhase( float& phi ) const NOEXCEPT
{
    bool status( true );
    try
    {
        phi = getPhase( );
    }
    catch ( std::exception& r )
    {
        status = false;
    }
    return status;
}

float
Mixer::getFcHz( void ) const
{
    float   f;
    FcUnits units( fc );
    if ( Hz == units )
        f = fc;
    else if ( Nyquist == units )
        f = float( fc ) / ( 2.0 * dt );
    else
        throw std::runtime_error( "Can't get here!!!" );

    return f;
}

bool
Mixer::getFcHz( float& ffc ) const NOEXCEPT
{
    bool rc = true;
    try
    {
        ffc = getFcHz( );
    }
    catch ( std::exception )
    {
        rc = false;
    }
    return rc;
}

float
Mixer::getFcNyquist( void ) const

{
    float   f;
    FcUnits units( fc );
    float   fcval( fc );

    if ( Hz == units )
        f = fcval / ( 2.0 * dt );
    else if ( Nyquist == units )
        f = fcval;
    else
        throw std::runtime_error( "Can't get here!!!" );

    return f;
}

bool
Mixer::getFcNyquist( float& ffc ) const NOEXCEPT
{
    bool rc = true;
    try
    {
        ffc = getFcNyquist( );
    }
    catch ( std::exception )
    {
        rc = false;
    }
    return rc;
}

// ---------------------------------------- Private Methods

void
Mixer::initialize( const TSeries& t )
{
    tStart = t.getStartTime( );
    tCurrent = tStart;
    dt = t.getTStep( );

    FcUnits units( fc );
    float   fcval( fc );

    if ( units == Hz )
        dphi = twopi * fcval * dt;
    else if ( units == Nyquist )
        dphi = twopi * fcval / 2;
    else
        throw std::out_of_range( "Illegal units" );

    if ( fabs( dphi ) > twopi / 2 )
        throw std::out_of_range( "Carrier frequency > Nyquist frequency" );

    bInUse = true;

    return;
}
