#ifndef Mixer_HH
#define Mixer_HH
/* 
 * $Id$
 * $Log$
 * Revision 1.3  2002/07/18 21:35:34  jzweizig
 * Do phase calculations in double precision
 *
 * Revision 1.2  2001/12/14 20:45:54  jzweizig
 * Add clone method
 *
 * Revision 1.1  2001/10/01 22:56:37  jzweizig
 * Original Version
 *
 * Revision 1.4  2000/07/07 01:52:00  jzweizig
 * Compile mixer, make it compatible with root
 *
 * Revision 1.3  2000/06/22 00:31:41  jzweizig
 * Remove throw clauses from all FilterBase and Pipe methods
 *
 * Revision 1.2  1999/11/30 02:20:27  jzweizig
 * move constant.hh to Mixer.cc
 *
 * Revision 1.1  1999/10/27 23:34:03  jzweizig
 * New mixer (heterodyne) class
 *
 */

#include "Pipe.hh"
#include "UnitTypes.hh"
#include "Interval.hh"
#include "Time.hh"

/** Mixer mixes a single input TSeries with a local oscillator,
 *  returning a single complex TSeries. 
 *  @memo Mixer API
 *  @author Lee Samuel Finn <LSF5@PSU.Edu>
 *  @version $Id$ 
 */
class Mixer : public Pipe
{
public:
    using Pipe::apply;
    using Pipe::dataCheck;

    // Lifecycle

    /** Default constructor produces uninitialized mixer object. 
   *  @memo Default constructor produces unitialized mixer object.
   */
    Mixer( void );

    /** Copy constructor produces mixer object identical to an existing object. 
   *  @memo Copy constructor.
   */
    Mixer( const Mixer& x );

    /** Default destructor
   *  @memo Default destructor.
   */
    ~Mixer( void );

    /** Clone method creates an identical mixer and returns a pointer.
   */
    Mixer* clone( void ) const;

    // Operations

    /**
   *  Mix input TSeries with a local oscillator exp(2*pi*I*fc*t)
   *  @memo Mix input TSeries with a local oscillator exp(2*pi*I*fc*t)
   *  @param in  input TSeries
   */
    TSeries apply( const TSeries& in );

    /** Validate filter input. The input TSeries
   *  is checked for continuity and consistency. Failure of either 
   *  continuity or consistency throws a runtime_error exception.
   *  @memo Validate filter input.
   *  @param in    TSeries to validate
   */
    void dataCheck( const TSeries& ) const;

    /** Reset filter to a pristine state: local oscillator frequency,
   *  start time, current time, interval, and phase are all undefined and 
   *  inUse() is false.
   *  @memo reset filter to a pristine state.
   */
    void reset( void );

    /** Return a KeyChain for input TSeries. Use these keys to 
   *  fill a FilterIO container with TSeries, optionally validates the 
   *  container and contents with dataCheck(), and act on the input with 
   *  apply(). 
   *  @memo Return a KeyChain with Keys for input TSeries.  Currently 
   *  unimplemented. 
   */
    virtual const KeyChain& getInputKeys( void ) const;

    /** Return a KeyChain for output TSeries. Use the keys on this chain
   *  to retrieve output TSeries from output FilterIO container. 
   *  @memo Return a KeyChain with Keys for output TSeries.  Currently 
   *  unimplemented. 
   */
    virtual const KeyChain& getOutputKeys( void ) const;

    // Accessors

    /** Get the moment of the first sample processed. Throws an exception 
   *  if the filter has not yet been used. Assumed to throw only 
   *  exceptions derived from standard c++ exception class. 
   *  @memo Get the moment of the first sample processed.
   */
    Time getStartTime( void ) const;

    /** Get the moment of the next expected sample. Throws an exception
   *  if the filter has not yet been used. Assumed to throw only 
   *  exceptions derived from standard c++ exception class. 
   *  @memo Get the moment of the next expected sample.
   */
    Time getCurrentTime( void ) const;

    /** Get the current phase. Throws logic_error exception if 
   *  undefined.
   *  @memo Get the current phase. 
   */
    float getPhase( void ) const;

    /** Get the current phase. Returns false if undefined.
   *  @memo Get the current phase. 
   *  @param phi  current filter phase
   */
    bool getPhase( float& phi ) const NOEXCEPT;

    /** Get the local oscillator frequency in units of Hz. 
   *  Throws logic_error exception if undefined.
   *  @memo Get the current phase. 
   */
    float getFcHz( void ) const;

    /** Get the local oscillator frequency in units of Hz. 
   *  Returns false if undefined. Root-safe.
   *  @memo Get the current phase. Root safe.
   */
    bool getFcHz( float& fc ) const NOEXCEPT;

    /** Get the local oscillator frequency in units of Nyquist frequency.
   *  Throws logic_error exception if undefined.
   *  @memo Get the current phase. 
   */

    float getFcNyquist( void ) const;

    /** Get the local oscillator frequency in units of Nyquist frequency.
   *  Returns false if undefined. Root-safe.
   *  @memo Get the current phase. Root-safe.
   */
    bool getFcNyquist( float& fc ) const NOEXCEPT;

    /** Set the initial phase. Throws logic_error exception if 
   *  filter in use. 
   *  @memo Set the initial phase. 
   *  @param phi  initial phase of local oscillator (radians)
   */
    void setPhase( const float& phi );

    /** Set the local oscillator frequency in Hz. Throws logic_error if called
   *  while filter in use. 
   *  @memo Set the local oscillator frequency in Hz. 
   *  @param f local oscillator frequency (Hz)
   */
    void setFcHz( const float& f );

    /** Set the local oscillator frequency in Nyquist frequency units. 
   *  Throws out_of_range if oscillator frequency not in range (-1,1), 
   *  logic_error if called while filter in use. 
   *  @memo Sets the local oscillator frequency in Nyquist frequency units.
   *  @param f   local oscillator frequency (Nyquist units)
   */
    void setFcNyquist( const float& f );

    // For root safety check if filter is in use
    // before setting filter characteristics

    /** Returns true if filter is in use.
   *  @memo Returns true if filter is in use.
   */
    bool inUse( void ) const;

protected:
private:
    // Data

    /** The bInUse flag signals whether the filter has acted on input 
   *  since being created or since the last reset */
    bool bInUse;

    /// PFloat objects are floats that except if they are used before set.
    typedef SetProtected< double > PFloat;

    /// PTime objects are Time objects that except if they are used before set.
    typedef SetProtected< Time > PTime;

    /** PInterval objects are Interval objects that except if they are 
   *  used before set.
   */
    typedef SetProtected< Interval > PInterval;

    /// Local oscillator units
    typedef enum
    { // units of oscillator frequency
        Hz,
        Nyquist
    } FcUnits;

    /** UFloat objects are floats with associated units. They except if they
   *  are used before their value or their units are set.
   */
    typedef wUnits< FcUnits, float > UFloat;

    /// Local oscillator frequency (with units)
    UFloat fc;

    /// The sampling interval of the time series being acted on.
    PInterval dt; // sampling period

    /** The current phase of the local oscillator (i.e., the phase 
   *  of the local oscillator as it waits for the next sample).
   */
    PFloat phase; // current phase (mod 2*pi)

    /// Radian advance of the local oscillator with each sample
    PFloat dphi; // derived from fc, dt, fcUnits

    /** The moment of time when the filter came into use (i.e., the time
   *  of the first sample acted on.  
   */
    PTime tStart; // start time

    /// The expected time of the next sample to be acted on.
    PTime tCurrent;

    // methods

    /** Initialize the first state on its first application after its 
   *  creation or most recent reset.
   */
    void initialize( const TSeries& );
};

#endif // Mixer_HH
