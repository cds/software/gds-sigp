#ifndef UnitTypes_HH
#define UnitTypes_HH

#ifndef __CINT__
#include <stdexcept>
#endif

/** Protect object of class T against use before it is set. 
 */
template < class T >
class SetProtected
{
public:
    /// Default constructor
    SetProtected( ) : setFlag( false )
    {
    }

    /// Construct with a value
    SetProtected( const T& x ) : setFlag( true ), val( x )
    {
    }

    /// Destruct
    ~SetProtected( )
    {
    }

#ifndef __CINT__
    /// Allow manipulation when value set
    operator T( void ) const;
#endif

    /// Assign a value
    SetProtected& operator=( const T& x );

    /// Clear the assigned flag
    void reset( void );

    /// check state
    bool isSet( void ) const;

private:
    bool setFlag;
    T    val;
};

#ifndef __CINT__
template < class T >
inline SetProtected< T >::operator T( ) const
{
    if ( !setFlag )
        throw std::range_error( "Value undefined" );
    return val;
}

template < class T >
inline SetProtected< T >&
SetProtected< T >::operator=( const T& x )
{
    setFlag = true;
    val = x;
    return *this;
}

template < class T >
inline bool
SetProtected< T >::isSet( void ) const
{
    return setFlag;
}

template < class T >
inline void
SetProtected< T >::reset( void )
{
    setFlag = false;
    return;
}

#endif

/**  Associate units U with type T. You can set value and units separately
  *  but you can't retrieve value unless units are also set. 
  */
template < class U, class T >
class wUnits
{
public:
    /// Default constructor
    wUnits( void )
    {
    }

    /// Construct with value, but no units
    wUnits( const T& x ) : val( x )
    {
    }

    /// Construct with units, but no value
    wUnits( const U& u ) : units( u )
    {
    }

    /// Construct with value and units
    wUnits( const U& u, const T& x ) : units( u ), val( x )
    {
    }

    /// Destruct
    ~wUnits( )
    {
    }

    /// Change units by assignment
    wUnits& operator=( const U& u );

    /// Change value by assignment
    wUnits& operator=( const T& x );

#ifndef __CINT__
    /// Allow manipulation of object as if it were its units
    operator U( void ) const;

    /** If units and value set, allow manipulation of object as if it 
   *  were its value
   */
    operator T( void ) const;
#endif

    /// Clear value and units
    void reset( void );

private:
    SetProtected< U > units;
    SetProtected< T > val;
};

#ifndef __CINT__

template < class U, class T >
inline void
wUnits< U, T >::reset( void )
{
    units.reset( );
    val.reset( );
}

template < class U, class T >
wUnits< U, T >::operator T( ) const
{
    if ( !units.isSet( ) )
        throw std::range_error( "Units not set" );
    return val;
}

template < class U, class T >
wUnits< U, T >::operator U( ) const
{
    return units;
}

template < class U, class T >
inline wUnits< U, T >&
wUnits< U, T >::operator=( const T& x )
{
    val = x;
    return *this;
}

template < class U, class T >
inline wUnits< U, T >&
wUnits< U, T >::operator=( const U& u )
{
    units = u;
    return *this;
}
#endif

#endif // UnitTypes_HH
