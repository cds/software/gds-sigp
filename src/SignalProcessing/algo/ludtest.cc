#include <time.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
//long double x = atan2l (1., 1.);
#include "lud.hh"

using namespace std;
using namespace diag;

typedef complex< double > TT;

int
main( )
{
    TT mm[ 12 ];
    TT aa[ 9 ];
    TT bb[ 12 ];

    TT** m = nrMatrix( mm, 3, 4 );
    TT** a = nrMatrix( aa, 3, 3 );
    TT** b = nrMatrix( bb, 3, 4 );

    a[ 1 ][ 1 ] = 1.0;
    a[ 1 ][ 2 ] = 2.0;
    a[ 1 ][ 3 ] = 3.0;
    a[ 2 ][ 1 ] = 6.0;
    a[ 2 ][ 2 ] = 5.0;
    a[ 2 ][ 3 ] = 4.0;
    a[ 3 ][ 1 ] = 3.0;
    a[ 3 ][ 2 ] = 3.0;
    a[ 3 ][ 3 ] = TT( 0.0, 1.0 );
    cout << "Matrix A = " << endl;
    for ( int i = 1; i <= 3; i++ )
    {
        for ( int j = 1; j <= 3; j++ )
        {
            cout << a[ i ][ j ] << "      ";
        }
        cout << endl;
    }

    inverse( a, 3 );

    cout << "Inverse of matrix A = " << endl;
    {
        for ( int i = 1; i <= 3; i++ )
        {
            for ( int j = 1; j <= 3; j++ )
            {
                cout << a[ i ][ j ] << "      ";
            }
            cout << endl;
        }
    }

    a[ 1 ][ 1 ] = 1.0;
    a[ 1 ][ 2 ] = 2.0;
    a[ 1 ][ 3 ] = 3.0;
    a[ 2 ][ 1 ] = 6.0;
    a[ 2 ][ 2 ] = 5.0;
    a[ 2 ][ 3 ] = 4.0;
    a[ 3 ][ 1 ] = 3.0;
    a[ 3 ][ 2 ] = 3.0;
    a[ 3 ][ 3 ] = TT( 0.0, 1.0 );
    cout << "Matrix A = " << endl;
    {
        for ( int i = 1; i <= 3; i++ )
        {
            for ( int j = 1; j <= 3; j++ )
            {
                cout << a[ i ][ j ] << "      ";
            }
            cout << endl;
        }
    }

    b[ 1 ][ 1 ] = 1.0;
    b[ 1 ][ 2 ] = 1.0;
    b[ 1 ][ 3 ] = 2.0;
    b[ 1 ][ 4 ] = 2.0;
    b[ 2 ][ 1 ] = 6.0;
    b[ 2 ][ 2 ] = 7.0;
    b[ 2 ][ 3 ] = 2.0;
    b[ 2 ][ 4 ] = 0.0;
    b[ 3 ][ 1 ] = 0.0;
    b[ 3 ][ 2 ] = 3.0;
    b[ 3 ][ 3 ] = TT( 0.0, 1.0 );
    b[ 3 ][ 4 ] = TT( 5.0, -1.0 );
    cout << "Matrix B = " << endl;
    {
        for ( int i = 1; i <= 3; i++ )
        {
            for ( int j = 1; j <= 4; j++ )
            {
                cout << b[ i ][ j ] << "      ";
            }
            cout << endl;
        }
    }

    imul( m, a, b, 3, 4 );

    cout << "Matrix M = A^(-1) B = " << endl;
    {
        for ( int i = 1; i <= 3; i++ )
        {
            for ( int j = 1; j <= 4; j++ )
            {
                cout << m[ i ][ j ] << "      ";
            }
            cout << endl;
        }
    }
    return 0;
}
