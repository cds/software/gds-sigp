/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: window				        		*/
/*                                                         		*/
/* Module Description: Windowing functions and constants.		*/
/*									*/
/* Module Arguments: none                				*/
/*									*/
/* Revision History:					   		*/
/* Rel   Date     Programmer   		Comments       	       		*/
/* 0.1   1/1/99   Edward Daw            Based on fftw fft library       */
/*                                                                      */
/* Documentation References: 						*/
/*	Man Pages: doc++ generated html					*/
/*	References: Numerical Recipes in C,                             */
/*                  http://theory.lcs.mit.edu/~fftw and docs therein    */
/*                  Blackman & Tukey: `The Measurement of Power Spectra'*/
/*                    Dover, p98 for definition of Hamming window       */
/*                  SRS785 manual for definitions of various windows    */
/*                                                         		*/
/* Author Information:							*/
/*	Name	Telephone    Fax          e-mail 			*/
/*	E. Daw  617-258-7697 617-253-7014 edaw@ligo.mit.edu		*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Unix						*/
/*	Compiler Used: Sun Sparcworks C compiler         		*/
/*	Runtime environment: UNIX    					*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	TBD			*/
/*			  Lint.			TBD			*/
/*			  ANSI			TBD			*/
/*			  POSIX			TBD			*/
/*									*/
/* Known Bugs, Limitations, Caveats:                       		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_WINDOW_H
#define _GDS_WINDOW_H

#ifdef __cplusplus
extern "C" {
#endif

/** @name Windowing Functions.

    This library generates data windows for FFT and sine detection
    algorithms.

    @memo Generates data windows.
    @author Edward Daw, Jan 99
   **********************************************************************/

/*@{*/

/** @name Constants and flags for windowing routines.
    This header defines common types of the gds algorithm library.

    @memo Constants and Flags.
    @author Edward Daw, Jan 99
    @version 0.1
   **********************************************************************/

/*@{*/

/** @name Flags to select data windowing  */

/*@{*/

/**Flag to specify Uniform windowing = 0 */
#define WINDOW_UNIFORM 0
/**Flag to specify Hanning windowing = 1 */
#define WINDOW_HANNING 1
/**Flag to specify Flattop windowing = 2 */
#define WINDOW_FLATTOP 2
/**Flag to specify Welch windowing = 3 */
#define WINDOW_WELCH 3
/**Flag to specify Bartlett windowing = 4 */
#define WINDOW_BARTLETT 4
/**Flag to specify BMH windowing = 5 */
#define WINDOW_BMH 5
/**Flag to specify Hamming windowing = 6 */
#define WINDOW_HAMMING 6

/*@}*/

/** @name Flag to select detrending  */

/*@{*/

/** Remove the DC partof the time series */
#define WINDOW_REMOVE_DC 256

/*@}*/

/*@}*/

/** @name Functions.
    Functions for calculating data windows.

    @author Edward Daw, Jan 99
   **********************************************************************/

/*@{*/

/**
This function generates a set of windowing coefficients for a data
set of a given length using one of various windowing functions.

  @param timeseries_length The number of real or complex components to
                           the time series.
  @param window An integer specifying which window. The windowing
                functions currently included are specified by setting
                this integer to WINDOW_<HANNING, WELCH, BARTLETT, UNIFORM,
                FLATTOP, BMH>. See Numerical Recipes section 13.4 and
                the manual for the SRS SR785 spectrum analyzer, page
                2-14 for definitions of these windowing functions.
  @param win_array A pointer to the array of window coefficients.
  @author Edward Daw, June 1999
 ************************************************************************/
void winCoeffGen( int timeseries_length, int window, double* win_array );

/**
This function multiplies a data set by a windowing function.
The function takes real or complex data of the same length
as the windowing function already created by win_coeff_gen().

  @param flag if equal to WINDOW_REMOVE_DC the mean of the time series
         will be removed before applying the window
  @param timeseries_length The number of real or complex components in
                           the time series
  @param data_type 1 = real data, 2 = complex data
  @param win_array the windowing function
  @param data A pointer to the unwindowed (input) data set
  @param wdata A pointer to the data set that has been multiplied by 
               the window function (output)
  @returns 0 if successful, -2 if data type not DATA_REAL or DATA_COMPLEX
  @author Edward Daw, June 1999
 ************************************************************************/
int windowData( int           flag,
                int           timeseries_length,
                int           data_type,
                const double* win_array,
                const double* data,
                double*       wdata );

/**
This function multiplies a data set by a windowing function.
The function takes real or complex data of the same length
as the windowing function already created by win_coeff_gen().

   @param flag if equal to WINDOW_REMOVE_DC the mean of the time series
           will be removed before applying the window
   @param timeseries_length The number of real or complex components in
       the time series
   @param data_type 1 = real data, 2 = complex data
  @param win_array the windowing function
  @param data A pointer to the unwindowed (input) data set
  @param wdata A pointer to the data set that has been multiplied by
  the window function (output)
      @returns 0 if successful, -2 if data type not DATA_REAL or DATA_COMPLEX
  @author Edward Daw, June 1999
************************************************************************/
int swindowData( int           flag,
                 int           timeseries_length,
                 int           data_type,
                 const double* win_array,
                 const float*  data,
                 double*       wdata );

/*@}*/

/*@}*/

#ifdef __cplusplus
}
#endif

#endif /* _GDS_WINDOW_H */
