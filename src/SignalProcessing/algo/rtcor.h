/************************************************************/
/*                                                          */
/* Module Name: rtcor                                       */
/*                                                          */
/* Module Description, iterative cross correlation          */
/* line subtraction                                         */
/*                                                          */
/* Ed Daw, 5th September 2008                               */
/* e.daw@shef.ac.uk                                         */
/* +44 114 222 4353                                         */
/*                                                          */
/* Comments: This header file should generate its own       */
/* documentation on typing doc++ < header file name >       */
/* See gds tree build instructions on how to download doc++ */
/*                                                          */
/*                                                          */
/************************************************************/

#ifndef _RTCOR_H
#define _RTCOR_H

#ifdef __cplusplus
extern "C" {
#endif

/** @name Iterative cross correlation algorithm library 
      @memo Code for real time cross correlation of two data streams.
      @author Ed Daw, 8th September 2008
  *********************************************************/

/*@{*/

/** @name rtcordata
      Parameter structure for storing data used by cross correlation.
      @memo A structure holding information used to apply iterative
            cross correlation.
      @author Ed Daw, 8th September 2008
  ***********************************************************/

struct rtcordataset
{
    /** Number of lags, equal to ncor for the 
      wrapped correlation. For the unwrapped cross correlation,
      this should be 2n+1 where n is a positive integer or zero.
      If n=0, the zero lag only is calculated. For n=1, lags
      -1, 0 and 1 are calculated, etc. */
    int nlags;
    /** Length of the vectors in the dot product calculated for
      each lag - only used by unwrapped correlation. */
    int ncor;
    /** Total size of y data array - only used by unwrapped correlation.
      The size of the wrapped y data array is equal to ncor in the
      case of the wrapped correlation. */
    int nbinsy;
    /** Pointers to timeseries data from channel x. */
    double* xdata;
    /** Pointers to timeseries data from channel y. */
    double* ydata;
    /** Storage for all calculated correlation lags. */
    double* corrmem;
    /** Counter for current location in x data. */
    int counter;
    /** Counter for current location in y data - only used by unwrapped 
     correlation. */
    int ycounter;
    /** Internal buffer for data between the end of the x array and the
      end of the y array - only used by unwrapped correlation. */
    double* xenddata;
    /** Counter for internal buffer for end of x array - only used in
      unwrapped correlation. */
    int xendcounter;
    /** Size of interanl buffer for end of x array - only used in
      unwrapped correlation. */
    int nxend;
};
typedef struct rtcordataset rtcordata;

/** @name Functions for wrapped cross correlation
      @memo The nth lag of a wrapped cross correlation is calculated by
            taking the data vectors from two data streams x and
            y and taking the dot product of x with the nth cyclic
            permutation of y. These functions facilitate this
	    operation.
  **************************************************/

/*@{*/

/** Initialize the wrapped cross correlation data buffers and
parameters. 

@param nlags The common number of samples in the x and y data
             vectors and also, since the dot product of each
             cyclic permutation is taken, the number of lags
             calculated.
@param initdatax The address on the stack of an array containing
the x data.
@param initdatay The address on the stack of an array containing
the y data.
@param pdata A pointer to an empty structure containing data to
be kept persistent between successive applications of the iteration
algorithm to new data points.
@return Zero.
@author Ed Daw, 5th September 2008
  ***************************************************************/
int rtcor_constructor( int        nlags,
                       double*    initdatax,
                       double*    initdatay,
                       rtcordata* pdata );

/** Destructor for wrapped cross correlation. Frees the stack.
@param pdata A pointer to the rtcordata structure preserving data
between iterations.
@return Zero. 
@author Ed Daw, 5th September 2008
  *****************************************************************/
int rtcor_destructor( rtcordata* pdata );

/** Iteration of the wrapped cross correlation on acquisition of
new x and y data samples.
@param xsample A new sample of x data.
@param ysample A new sample of y data.
@param pdata A pointer to a structure of type rtcordata. This
structure contains addresses of stack memory used to buffer data
which are freed by this function.
@return Zero.
@author Ed Daw, 5th September 2008
  *****************************************************************/
int rtcor_iterate( double xsample, double ysample, rtcordata* pdata );

/** Copy correlation results to a destination address.
@param destination Destination address for data. Should be sized
       to hold nlags doubles.
@param pdata  A pointer to a structure of type rtcordata. This
              structure contains addresses of stack memory used
              to buffer data, including correlation results, between
              iterations of the algorithm.
@return Zero.
@author Ed Daw, 5th September 2008
  *****************************************************************/
int rtcor_copy( double* destination, rtcordata* pdata );

/** Reference the pointer to the output data
   */
double* rtcor_refdata( rtcordata* pdata );

/** Dump internally held data to standard output. Intended for
debugging.
@param pdata A pointer to the structure of type rtcordata holding
             the data to be dumped. 
@return Zero.
@author Ed Daw, 5th September 2008
  ****************************************************************/
int rtcor_dump( rtcordata* pdata );

/** Copy the contents of an rtcordata structure, dynamically
allocating space on the stack for internal data buffers and 
copying the contents of the source data buffers to the new
space. This method works for structures used for wrapped cross
correlation.
@param ptarget A pointer to the target array
@param data The source structure
@return Zero.
@author Ed Daw, 19th November 2008
  ****************************************************************/
int rtcor_copystruct( rtcordata* ptarget, rtcordata data );

/*@}*/

/** @name Functions for unwrapped cross correlation 
      @memo The nth lag of an unwrapped cross correlation is
            the dot product of a sequence of data points from
            x with a sequence of data points from y timeshifted
            by n samples with respect to x.
  *****************************************************/

/*@{*/

/** Initialize the wrapped cross correlation data buffers and
parameters. 

@param nlags Number of lags to be calculated. Should be 
             2n+1, where n is a natural number or zero.
	     Lags calculated are symmetric about zero lag.
@param ncor Number of data bins in the dot product.
@param initdatax The address on the stack of an array containing
the x data to initialize the algorthm. x and y data should be the
same length, and that length should be nlags+ncor-1.
@param initdatay The address on the stack of an array containing
the y data to initialize the algorithm. See initdatax above.
@param pdata A pointer to an empty structure containing data to
be kept persistent between successive applications of the iteration
algorithm.
@return Zero.
@author Ed Daw, 5th September 2008
  ***************************************************************/
int urtcor_constructor( int        nlags,
                        int        ncor,
                        double*    initdatax,
                        double*    initdatay,
                        rtcordata* pdata );

/** Destructor for unwrapped cross correlation. Frees the stack.
@param pdata A pointer to the rtcordata structure preserving data
between iterations.
@return Zero. 
@author Ed Daw, 5th September 2008
  *****************************************************************/
int urtcor_destructor( rtcordata* pdata );

/** Iteration of the unwrapped cross correlation on acquisition of
new x and y data samples.
@param xsample A new sample of x data.
@param ysample A new sample of y data.
@param pdata A pointer to a structure of type rtcordata. This
structure contains addresses of stack memory used to buffer data
which are freed by this function.
@return Zero.
@author Ed Daw, 5th September 2008
  *****************************************************************/
int urtcor_iterate( double xsample, double ysample, rtcordata* pdata );

/** Copy correlation results to a destination address.
@param destination Destination address for data. Should be sized
       to hold nlags doubles.
@param pdata  A pointer to a structure of type rtcordata. This
              structure contains addresses of stack memory used
              to buffer data, including correlation results, between
              iterations of the algorithm.
@return Zero.
@author Ed Daw, 5th September 2008
  *****************************************************************/
int urtcor_copy( double* destination, rtcordata* pdata );

/** Dump internally held data to standard output. Intended for
debugging.
@param pdata A pointer to the structure of type rtcordata holding
             the data to be dumped. 
@return Zero.
@author Ed Daw, 5th September 2008
  ****************************************************************/
int urtcor_dump( rtcordata* pdata );

/** Copy the contents of an rtcordata structure, dynamically
allocating space on the stack for internal data buffers and 
copying the contents of the source data buffers to the new
space. This method works for structures used for unwrapped cross
correlation.
@param ptarget A pointer to the target array
@param data The source structure
@return Zero.
@author Ed Daw, 19th November 2008
  ****************************************************************/
int urtcor_copystruct( rtcordata* ptarget, rtcordata data );

/*@}*/

/*@}*/

#ifdef __cplusplus
}
#endif

#endif /* _RTCOR_H */
