/****************************************************************
 *								*
 *  Module Name: Upsample function				*
 *								*
 *  Procedure Description: up samples a time series by 		*
 *  inserting identical data points				*
 *								*	
 *  External Procedure Name: stepup				*
 *								*
 *  Procedure Arguments: flag for determining filter type;	*
 *	pointers to input and output vectors; number of		*
 *	input points; number of step ups;			*
 *								*
 *								*
 ****************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "gdsconst.h"
#include "upsample.h"
#include "gdssigproc.h"

int
stepup( int flag, const float x[], float y[], int n, int up_factor )
{
    int    i;
    int    j;
    float* yy = y;
    for ( i = 0; i < n; i++ )
    {
        for ( j = 0; j < up_factor; j++ )
        {
            *( yy++ ) = x[ i ];
        }
    }
    return 0;
}
