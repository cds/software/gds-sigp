/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: gdsSigProc						*/
/*                                                         		*/
/* Module Description: signal processing utility functions		*/
/*									*/
/*                                                         		*/
/* Module Arguments:					   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer   			Comments       		*/
/* 0.1   01/04/99 Peter Fritschel & Edward Daw  E.D. wrote sMixdown()   */
/* 0.2   5/25/99  PF				added some complex      */
/*						functions		*/
/*                                                         		*/
/* Documentation References: 						*/
/*	Man Pages: doc++ generated html					*/
/*	References:							*/
/*                                                         		*/
/* Author Information:							*/
/*	Name          Telephone    Fax          e-mail 			*/
/*	P Fritschel   617-253-8153 617-253-7014 pf@ligo.mit.edu		*/
/*	E Daw         617-258-7697 617-253-7014 edaw@ligo.mit.edu       */
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Unix						*/
/*	Compiler Used: Sun cc						*/
/*	Runtime environment: Solaris 					*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			TBD			*/
/*			  POSIX			TBD			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDSSIGPROC_H
#define _GDSSIGPROC_H

#ifdef __cplusplus
extern "C" {
#endif

/* Header File List: */
#include "gdstype.h"

/**@name Signal processing utility functions
   This module contains data types and low level signal processing
   functions

   @memo Signal processing.
   @author Written Jan 1999 by Ed Daw, Peter Fritschel
   @version 0.1
*********************************************************************/

/*@{*/

/** Vector dot product
       
    Calculates the dot product of two vectors (arrays); returns
    result as a double.


    @param x input vector 1
    @param y input vector 2
    @param npt number of points in x or y (must be the same)
    @return double the result x.y

    @memo Dot product of two vectors
    @author PF, Jan 1999
    @see 
**********************************************************************/
double sDotProd( const float x[], const float y[], int npt );

/** Vector dot product

    Calculates the dot product of two vectors (arrays); returns
    result as a double.


   @param x input vector 1
   @param y input vector 2
   @param npt number of points in x or y (must be the same)
       @return double the result x.y

   @memo Dot product of two vectors
   @author PF, Jan 1999
   @see
**********************************************************************/
double dDotProd( const double x[], const double y[], int npt );

/** Vector mean
       
    Calculates the mean of a vector (array) of data; returns the
    result in double precision.


    @param z input vector
    @param npt number of points in z
    @return double the result <z>

    @memo Mean of a vector
    @author PF, Jan 1999
    @see 
**********************************************************************/
dCmplx zMean( const dCmplx z[], int npt );
dCmplx szMean( const sCmplx x[], int npt );
double sMean( const float x[], int npt );
double dMean( const double x[], int npt );

/** Complex heterodyne function
       
    This function multiplies a time series of real or complex data
    by a complex exponential. To use with real data, call the function
    with NULL for the array of the imaginary parts in the time series.
    
\begin{verbatim}
Example:
int flag=1;
float xre[1024];   //the real part of the input data                         
float xim[1024];   //the imaginary part of the input data                    
float yre[1024];   //the real part of the heterodyned output                 
float yim[1024];   //the imaginary part of the heterodyned output            
int n = 1024;      //the number of points in the input data                  
double t;          //the time offset at the start of the input data          
double dt;         //the time interval between successive time series points 
double fmix;       //the heterodyne frequency                           

// call the function once to run the filter
sMixdown(flag, xre, xim, yre, yim, n, t, dt, fmix);
\end{verbatim}

    @param flag an integer flag, unused at present
    @param xre the real part of the input data                         
    @param xim the imaginary part of the input data                    
    @param yre real part of the heterodyned output                 
    @param yim imaginary part of the heterodyned output            
    @param n the number of points in the input data                  
    @param t time offset at the start of the input data          
    @param dt time interval between successive points
    @param fmix the heterodyne frequency                                
    @return void

    @memo Heterodyne a time series of real or imaginary data
    @author Edward Daw, November 1998
**********************************************************************/
void sMixdown( int         flag,
               const float xre[],
               const float xim[],
               float       yre[],
               float       yim[],
               int         n,
               double      t,
               double      dt,
               double      fmix );

/** Complex heterodyne function

    This function multiplies a time series of real or complex data
    by a complex exponential. To use with real data, call the function
    with NULL for the array of the imaginary parts in the time series.

\begin{verbatim}
Example:
int flag=1;
float xre[1024];   //the real part of the input data
float xim[1024];   //the imaginary part of the input data
float yre[1024];   //the real part of the heterodyned output
float yim[1024];   //the imaginary part of the heterodyned output
int n = 1024;      //the number of points in the input data
double t;          //the time offset at the start of the input data
double dt;         //the time interval between successive time series points
double fmix;       //the heterodyne frequency

   // call the function once to run the filter
   sMixdown(flag, xre, xim, yre, yim, n, t, dt, fmix);
   \end{verbatim}

   @param flag an integer flag, unused at present
    @param xre the real part of the input data
    @param xim the imaginary part of the input data
    @param yre real part of the heterodyned output
    @param yim imaginary part of the heterodyned output
    @param n the number of points in the input data
    @param t time offset at the start of the input data
    @param dt time interval between successive points
    @param fmix the heterodyne frequency
    @return void

    @memo Heterodyne a time series of real or imaginary data
    @author Edward Daw, November 1998
**********************************************************************/
void sdMixdown( int         flag,
                const float xre[],
                const float xim[],
                double      yre[],
                double      yim[],
                int         n,
                double      t,
                double      dt,
                double      fmix );

/** Complex heterodyne function

    This function multiplies a time series of real or complex data
    by a complex exponential. To use with real data, call the function
    with NULL for the array of the imaginary parts in the time series.

\begin{verbatim}
Example:
int flag=1;
    float xre[1024];   //the real part of the input data
    float xim[1024];   //the imaginary part of the input data
    float yre[1024];   //the real part of the heterodyned output
    float yim[1024];   //the imaginary part of the heterodyned output
    int n = 1024;      //the number of points in the input data
    double t;          //the time offset at the start of the input data
    double dt;         //the time interval between successive time series points
    double fmix;       //the heterodyne frequency

    // call the function once to run the filter
    sMixdown(flag, xre, xim, yre, yim, n, t, dt, fmix);
    \end{verbatim}

    @param flag an integer flag, unused at present
        @param xre the real part of the input data
        @param xim the imaginary part of the input data
        @param yre real part of the heterodyned output
        @param yim imaginary part of the heterodyned output
        @param n the number of points in the input data
        @param t time offset at the start of the input data
        @param dt time interval between successive points
        @param fmix the heterodyne frequency
        @return void

        @memo Heterodyne a time series of real or imaginary data
        @author Edward Daw, November 1998
        **********************************************************************/
        void dMixdown( int         flag,
                        const double xre[],
                        const double xim[],
                        double      yre[],
                        double      yim[],
                        int         n,
                        double      t,
                        double      dt,
                        double      fmix );

/** Complex multiply
       
    Returns the complex product of two complex numbers.

    @param z1 complex multiplicand
    @param z2 complex multiplicand
    @return dCmplx the product z1*z2

    @memo Complex multiplication
    @author PF, May 1999
    @see 
**********************************************************************/
dCmplx zMultply( dCmplx z1, dCmplx z2 );

/** Complex conjugate
       
    Returns the complex conjugate of a complex number.

    @param z number to be conjugated
    @return dCmplx the conjugate of z

    @memo Complex conjugation
    @author PF, May 1999
    @see 
**********************************************************************/
dCmplx zConj( dCmplx z );

/** Complex addition
       
    Returns the complex sum of two complex numbers.

    @param z1 complex number
    @param z2 complex number
    @return dCmplx the sum z1+z2

    @memo Complex addition
    @author PF, May 1999
    @see 
**********************************************************************/
dCmplx zAdd( dCmplx z1, dCmplx z2 );

/** Data copy routine.
       
    Copies a data array from source to destination. Additional
    parameters describe the start index in source and the 
    number of data points to be extracted. Furthermore, a binning
    can be specified, with either linear or logarithmic spacing.
    The destination array must hold at least len/bin data points.

    @param dest destination array
    @param src source array
    @param cmplx complex numbers?
    @param start start index in source
    @param len number of points to extract from source
    @param bin number of points to bin
    @param logbin logarithmic bin spacing?
    @return void

    @memo Data copy with selection and binning
    @author DS, May 2000
**********************************************************************/
void sDataCopy( float*       dest,
                const float* src,
                int          cmplx,
                int          start,
                int          len,
                int          bin,
                int          logbin );

/*@}*/

#ifdef __cplusplus
}
#endif

#endif /*_GDSSIGPROC_H */
