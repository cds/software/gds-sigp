/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: Random Number generator					*/
/*                                                         		*/
/* Procedure Description: generate random numbers			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
#include <math.h>
#include <string.h>
#ifdef LIGO_GDS
#include "tconv.h"
#else
#include <time.h>
#endif
#include "gdsrand.h"

static double rand_filter_calc( randBlock* rb, double x );

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: urand_r					*/
/*                                                         		*/
/* Procedure Description: reentrant uniform random number generator	*/
/* 									*/
/* Procedure Arguments: randBlock (seed etc...) and			*/
/*			distribution limits, lo and hi			*/
/*                                                         		*/
/* Procedure Returns: uniform random deviate				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
double
urand_r( randBlock* rb, double lo, double hi )
{

    int    j;
    long   k;
    double temp;

    /*--- check for initialize condition ---*/
    if ( rb->seed <= 0 )
    {
        unsigned long seed;
#ifdef LIGO_GDS
        seed = ( TAInow( ) / 1000 ) & 0xFFFFFFFF;
#else
        seed = time( NULL );
#endif
        rb->seed = ( rb->seed != 0 ) ? -rb->seed : seed;
        for ( j = RAND_NTAB + 7; j >= 0; --j )
        {
            k = (long)( rb->seed / RAND_IQ );
            rb->seed = RAND_IA * ( rb->seed - k * RAND_IQ ) - RAND_IR * k;
            if ( rb->seed < 0 )
                rb->seed += RAND_IM;
            if ( j < RAND_NTAB )
                rb->itab[ j ] = rb->seed;
        }
        rb->lastr = rb->itab[ 0 ];
    }

    /*--- normal operation ---*/
    k = (long)( rb->seed / RAND_IQ );
    rb->seed = RAND_IA * ( rb->seed - k * RAND_IQ ) - RAND_IR * k;
    if ( rb->seed < 0 )
        rb->seed += RAND_IM;
    j = rb->lastr / RAND_NDIV;
    rb->lastr = rb->itab[ j ];
    rb->itab[ j ] = rb->seed;
    if ( ( temp = RAND_AM * rb->lastr ) > RAND_RNMX )
        return RAND_RNMX * ( hi - lo ) + lo;
    return temp * ( hi - lo ) + lo;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: urand_r					*/
/*                                                         		*/
/* Procedure Description: reentrant uniform random number generator	*/
/* 			with bandlimit					*/
/* 									*/
/* Procedure Arguments: randBlock (seed etc...) and			*/
/*			distribution limits, lo and hi			*/
/*                      lower and upper frequency corner   		*/
/*                                                         		*/
/* Procedure Returns: uniform random deviate				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
double
urand_filter_r( randBlock* rb, double lo, double hi )
{
    double x; /* random value */

    /* get new random number */
    x = urand_r( rb, lo, hi );
    /* calculate filter */
    x = rand_filter_calc( rb, x );
    /* return */
    return x;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: urandv_r					*/
/*                                                         		*/
/* Procedure Description: reentrant uniform random vector generator	*/
/* 									*/
/* Procedure Arguments: randBlock (seed etc...) and			*/
/*			distribution limits, lo and hi			*/
/*			output buffer and size in samples		*/
/*                                                         		*/
/* Procedure Returns: GDS_ERROR						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int
urandv_r( randBlock* rb, double* buf, int size, double lo, double hi )
{
    return 0;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: urand_r					*/
/*                                                         		*/
/* Procedure Description: reentrant normal random number generator	*/
/* 									*/
/* Procedure Arguments: randBlock (seed etc...) and			*/
/*			distribution parameters, mean and sigma		*/
/*                                                         		*/
/* Procedure Returns: normal random deviate				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
double
nrand_r( randBlock* rb, double m, double sig )
{
    double fac, rsq, v1, v2;

    if ( rb->ncnt == 0 )
    { /* empty buffer */
        do
        { /* pick 2 numbers in the unit circle */
            v1 = urand_r( rb, -1.0, 1.0 );
            v2 = urand_r( rb, -1.0, 1.0 );
            rsq = v1 * v1 + v2 * v2;
        } while ( rsq >= 1.0 || rsq == 0.0 );

        fac = sqrt( -2.0 * log( rsq ) / rsq );
        rb->nextn = v1 * fac;
        rb->ncnt = 1;
        return v2 * fac * sig + m;
    }
    else
    { /* got one waiting */
        rb->ncnt = 0;
        return rb->nextn * sig + m;
    }
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: nrandv_r					*/
/*                                                         		*/
/* Procedure Description: reentrant normal random vector generator	*/
/* 									*/
/* Procedure Arguments: randBlock (seed etc...) and			*/
/*			distribution parameters, mean and sigma		*/
/*			output buffer and size in samples		*/
/*                                                         		*/
/* Procedure Returns: GDS_ERROR						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int
nrandv_r( randBlock* rb, double* buf, int size, double m, double sig )
{
    return 0;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: nrandBW_r					*/
/*                                                         		*/
/* Procedure Description: reentrant uniform random number generator	*/
/* 			with bandlimit					*/
/* 									*/
/* Procedure Arguments: randBlock (seed etc...) and			*/
/*			distribution parameters, mean and sigma		*/
/*                      lower and upper frequency corner   		*/
/*                                                         		*/
/* Procedure Returns: uniform random deviate				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
double
nrand_filter_r( randBlock* rb, double m, double s )
{
    double x; /* random value */

    /* get new random number */
    x = nrand_r( rb, m, s );
    /* calculate filter */
    x = rand_filter_calc( rb, x );
    /* return */
    return x;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: rand_filter					*/
/*                                                         		*/
/* Procedure Description: set filter for random number generator	*/
/* 									*/
/* Procedure Arguments: randBlock (seed etc...) and			*/
/*			type, order, 					*/
/*                      lower and upper frequency corner   		*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 otherwise			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int
rand_filter( randBlock* rb, int type, int order, double f1, double f2 )
{
    rb->filter_stages = 0;
    rb->filter_order = 0;
    memset( rb->filter_coeff, 0, sizeof( rb->filter_coeff ) );
    memset( rb->filter_old, 0, sizeof( rb->filter_old ) );
    if ( f2 < f1 )
    {
        double f = f1;
        f1 = f2;
        f2 = f;
    }
    switch ( type )
    {
    default: {
        break;
    }
    case 1: {
        if ( order > 8 )
            order = 8;
        if ( order < 0 )
            order = 0;
        if ( f1 <= 0 )
        {
            /* flat */
            if ( f2 >= 0.45 )
            {
            }
            /* low pass */
            else
            {
                rb->filter_stages = order / 2;
                rb->filter_order = 2;
            }
        }
        else
        {
            rb->filter_stages = order / 2;
            /* high pass */
            if ( f2 >= 0.45 )
            {
                rb->filter_order = 2;
            }
            /* band pass */
            else
            {
                rb->filter_order = 4;
            }
        }
        break;
    }
    }
    return 1;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: rand_filter_calc				*/
/*                                                         		*/
/* Procedure Description: set filter for random number generator	*/
/* 									*/
/* Procedure Arguments: randBlock (seed etc...) and			*/
/*			type, order, 					*/
/*                      lower and upper frequency corner   		*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 otherwise			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
static double
rand_filter_calc( randBlock* rb, double u )
{
    int    i;
    int    j;
    double y;

    for ( i = 0; i < rb->filter_stages; i++ )
    {
        y = rb->filter_coeff[ i ][ 0 ][ 0 ] * u;
        for ( j = 0; j < rb->filter_order; j++ )
        {
            y += rb->filter_coeff[ i ][ 0 ][ j + 1 ] *
                    rb->filter_old[ i ][ 0 ][ j ] +
                rb->filter_coeff[ i ][ 1 ][ j + 1 ] *
                    rb->filter_old[ i ][ 1 ][ j ];
        }
        memmove( rb->filter_old[ i ][ 0 ] + 1,
                 rb->filter_old[ i ][ 0 ],
                 rb->filter_order * sizeof( double ) );
        rb->filter_old[ i ][ 0 ][ 0 ] = u;
        memmove( rb->filter_old[ i ][ 1 ] + 1,
                 rb->filter_old[ i ][ 1 ],
                 rb->filter_order * sizeof( double ) );
        rb->filter_old[ i ][ 1 ][ 0 ] = y;
        u = y;
    }
    return u;
}
