#include <time.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <complex>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "sineanalyze.h"
#include "window.h"

#define NAVG 1

using namespace std;

int
main( int argc, char* argv[] )
{
    if ( argc < 5 )
    {
        cout << "usage: sdettest 'input file' f_low f_high f_num" << endl;
        return 0;
    }

    dCmplx sineAmp;
    int    npt = 0;
    int    j;
    double fs;
    double fsine;
    double dummy;
    double x[ 10000 ];
    dCmplx sineVals[ NAVG ];

    ifstream inp( argv[ 1 ] );
    if ( !inp )
    {
        cout << "invalid input file " << argv[ 1 ] << endl;
        return 1;
    }

    /* read sine wave  */
    while ( inp )
    {
        inp >> dummy >> x[ npt++ ];
    }
    double FL = atof( argv[ 2 ] );
    double FU = atof( argv[ 3 ] );
    int    NFREQ = atoi( argv[ 4 ] );

    /* calculate sine amplitude/phase coefficient */
    fs = 128.;
    for ( j = 0; j < NFREQ; ++j )
    {
        fsine = j * ( FU - FL ) / ( NFREQ - 1 ) + FL;
        sineAnalyze( 1,
                     WINDOW_HANNING,
                     x,
                     npt,
                     fs,
                     fsine,
                     NAVG,
                     0.0,
                     &sineAmp,
                     sineVals );
        cout << setw( 8 ) << fsine << setw( 12 ) << sineAmp.re << setw( 12 )
             << sineAmp.im << endl;
    }

    return 0;
}
