/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: preproc							*/
/*                                                         		*/
/* Module Description: preprocesses a time series			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "gdsconst.h"
#include "preproc.h"
#include "gdssigproc.h"
#include "upsample.h"
#include "decimate.h"

#define __ONESEC 1E9

preproc_t*
initPreproc( tainsec_t        start,
             tainsec_t        duration,
             double           inDatarate,
             double           outDatarate,
             double           fdown,
             tainsec_t        tpart,
             tainsec_t        dtpart,
             PartitionDataOut write,
             void*            user )
{
    preproc_t* pre; /* preprocess structure */
    double     rate; /* intermediate rate */
    int        dec; /* total decimation factor */
    int        sDelay; /* filter delay in samples */
    int        tDelay; /* total delay in samples */
    float      tmp[ 1 ]; /* tmp. float */

    /* check arguments */
    if ( ( start <= 0 ) || ( duration == 0 ) || ( inDatarate <= 0 ) ||
         ( outDatarate <= 0 ) || ( fdown < 0 ) || ( tpart <= 0 ) ||
         ( dtpart <= 0 ) || ( write == 0 ) )
    {
        return NULL;
    }

    /* allocate memory */
    pre = malloc( sizeof( preproc_t ) );
    if ( pre == NULL )
    {
        return NULL;
    }
    /* initialize */
    memset( pre, 0, sizeof( preproc_t ) );
    pre->start = start;
    pre->duration = duration;
    pre->datarate = inDatarate;
    pre->zoomstart = start;
    pre->zoomfreq = fdown;
    pre->tpart = tpart;
    pre->dtpart = dtpart;
    pre->write = write;
    pre->user = user;

    /* compute intermediate rates */
    if ( inDatarate == outDatarate )
    {
        pre->dt[ 0 ] = pre->dt[ 1 ] = pre->dt[ 2 ] = pre->dt[ 3 ] =
            1. / inDatarate;
        pre->upfactor = 1;
        pre->decimate1 = 1;
        pre->decimate2 = 1;
    }
    else if ( inDatarate < outDatarate )
    {
        pre->dt[ 0 ] = 1. / inDatarate;
        pre->dt[ 1 ] = pre->dt[ 2 ] = pre->dt[ 3 ] = 1. / outDatarate;
        pre->upfactor = (int)( outDatarate / inDatarate + 0.5 );
        pre->decimate1 = 1;
        pre->decimate2 = 1;
    }
    else
    {
        pre->dt[ 0 ] = pre->dt[ 1 ] = 1. / inDatarate;
        pre->upfactor = 1;
        if ( pre->zoomfreq > 0 )
        {
            pre->decimate1 = 1;
            rate = inDatarate;
            if ( ( rate > 4.000001 * pre->zoomfreq ) && ( rate > outDatarate ) )
            {
                pre->decimate1 *= 2;
                rate /= 2.;
            }
            pre->decimate2 = (int)( rate / outDatarate + 0.5 );
            pre->dt[ 2 ] = pre->dt[ 1 ] * pre->decimate1;
            pre->dt[ 3 ] = pre->dt[ 2 ] * pre->decimate2;
        }
        else
        {
            pre->dt[ 2 ] = pre->dt[ 3 ] = 1. / outDatarate;
            pre->decimate1 = (int)( inDatarate / outDatarate + 0.5 );
            pre->decimate2 = 1;
        }
    }

    /* compute filter delays */
    pre->decimationflag = 0;
    dec = pre->decimate1 * pre->decimate2;
    pre->decdelay =
        firphase( pre->decimationflag, dec ) / TWO_PI * pre->dt[ 1 ];
    sDelay = (int)( firphase( pre->decimationflag, dec ) / TWO_PI + 0.5 );
    tDelay = dec * ( ( sDelay + dec - 1 ) / dec );
    pre->delaytaps = tDelay - sDelay;
    pre->delayshift =
        (tainsec_t)( (double)tDelay * pre->dt[ 1 ] * __ONESEC + 0.5 );
    pre->delay1 =
        (tainsec_t)( ( firphase( pre->decimationflag, pre->decimate1 ) /
                           TWO_PI +
                       (double)pre->delaytaps ) *
                         pre->dt[ 1 ] * __ONESEC +
                     0.5 );

    /* allocate temporary input buffer */
    if ( dec >= 0 )
    {
        pre->buf = malloc( dec * sizeof( float ) );
    }

    /* allocate temp. memory for delay / decimation filter */
    timedelay( tmp, tmp, 0, pre->delaytaps, NULL, &pre->tmpdelay1 );
    decimate(
        pre->decimationflag, tmp, tmp, 0, pre->decimate1, NULL, &pre->tmpdec1 );
    decimate( pre->decimationflag,
              tmp,
              tmp,
              0,
              pre->decimate2,
              NULL,
              &pre->tmpdec2I );
    decimate( pre->decimationflag,
              tmp,
              tmp,
              0,
              pre->decimate2,
              NULL,
              &pre->tmpdec2Q );

    /* check if allocation failed */
    if ( ( pre->buf == NULL ) ||
         ( ( pre->delaytaps > 0 ) && ( pre->tmpdelay1 == NULL ) ) ||
         ( pre->tmpdec1 == NULL ) || ( pre->tmpdec2I == NULL ) ||
         ( pre->tmpdec2Q == NULL ) )
    {
        freePreproc( pre );
        return NULL;
    }

    return pre;
}

void
freePreproc( preproc_t* pre )
{
    float tmp[ 1 ]; /* tmp. float */

    /* release input buffer memory */
    if ( pre->buf != NULL )
    {
        free( pre->buf );
        pre->buf = NULL;
    }
    /* release temp memory for decimation filter */
    if ( pre->tmpdelay1 != NULL )
    {
        timedelay( tmp, tmp, 0, pre->delaytaps, pre->tmpdelay1, NULL );
        pre->tmpdelay1 = NULL;
    }
    if ( pre->tmpdec1 != NULL )
    {
        decimate( pre->decimationflag,
                  tmp,
                  tmp,
                  0,
                  pre->decimate1,
                  pre->tmpdec1,
                  NULL );
        pre->tmpdec1 = NULL;
    }
    if ( pre->tmpdec2I != NULL )
    {
        decimate( pre->decimationflag,
                  tmp,
                  tmp,
                  0,
                  pre->decimate2,
                  pre->tmpdec2I,
                  NULL );
        pre->tmpdec2I = NULL;
    }
    if ( pre->tmpdec2Q != NULL )
    {
        decimate( pre->decimationflag,
                  tmp,
                  tmp,
                  0,
                  pre->decimate2,
                  pre->tmpdec2Q,
                  NULL );
        pre->tmpdec2Q = NULL;
    }
    free( pre );
}

int
preprocess( preproc_t* pre, tainsec_t time, const float data[], int length )
{
    const float* x; /* temp pointer */
    float*       y; /* temp array */
    int          freex; /* do we have to free x? */
    int          dec; /* total decimation factor */
    int          missing; /* # of missing data points */
    int          ndata; /* number of decimated points */
    tainsec_t    t0; /* time of first data sample */
    tainsec_t    dt; /* length of data segment */
    int          complex; /* 1-real, 2-complex */
    int          part1; /* first partition */
    int          part2; /* last partition */
    int          i; /* partition index */
    int          partlen; /* length of partition in points */

    /* check arguments */
    if ( ( pre == NULL ) || ( time <= 0 ) || ( data == NULL ) ||
         ( length < 0 ) )
    {
        return -1;
    }
    if ( length == 0 )
    {
        return 0;
    }
    /* ignore anything which is before start - 2 * filter delay */
    if ( time + length * pre->dt[ 0 ] * __ONESEC <
         pre->start - 2. * pre->decdelay * __ONESEC )
    {
        return 0;
    }
    /* ignore anything after stop + 2 * filter delay */
    if ( time > pre->start + pre->duration + 2. * pre->decdelay * __ONESEC )
    {
        return 0;
    }
    /* check if we have and old buffer time */
    if ( pre->bufTime == 0 )
    {
        pre->bufTime = time;
        pre->bufSize = 0;
    }

    /* check if we have to pad zeros or throw away data */
    missing = (int)( ( ( pre->bufTime + pre->bufSize * pre->dt[ 0 ] * __ONESEC -
                         time ) /
                       ( pre->dt[ 0 ] * __ONESEC ) ) +
                     0.5 );
    if ( missing < 0 )
    {
        x = data + ( -missing );
        freex = 0;
        length += missing;
        if ( length <= 0 )
        {
            return 0;
        }
    }
    else if ( missing > 0 )
    {
        y = malloc( ( missing + length ) * sizeof( float ) );
        if ( y == 0 )
        {
            return -2;
        }
        memset( y, 0, missing * sizeof( float ) );
        memcpy( y + missing, data, length * sizeof( float ) );
        length += missing;
        freex = 1;
        x = y;
    }
    else
    {
        x = data;
        freex = 0;
    }

    /* check if we need to get the data from the input buffer */
    dec = pre->decimate1 * pre->decimate2;
    /* new data fits within buffer */
    if ( ( pre->bufSize + length <= dec ) && ( dec > 1 ) )
    {
        memcpy( pre->buf + pre->bufSize, x, length * sizeof( float ) );
        if ( freex )
        {
            free( (float*)x );
            freex = 0;
        }
        x = pre->buf;
        pre->bufSize += length;
        if ( pre->bufSize < dec )
        {
            return 0;
        }
        length = dec;
    }
    /* we need to shuffle data */
    else if ( pre->bufSize > 0 )
    {
        y = malloc( ( pre->bufSize + length ) * sizeof( float ) );
        if ( y == NULL )
        {
            if ( freex )
            {
                free( (float*)x );
            }
            return -2;
        }
        memcpy( y, pre->buf, pre->bufSize * sizeof( float ) );
        memcpy( y + pre->bufSize, x, length * sizeof( float ) );
        if ( freex )
        {
            free( (float*)x );
        }
        freex = 1;
        x = y;
        length += pre->bufSize;
    }

    /* check how many decimated points we have, set new buffer time */
    ndata = length / dec;
    t0 = pre->bufTime;
    pre->bufTime += (tainsec_t)( dec * ndata * pre->dt[ 0 ] * __ONESEC + 0.5 );
    /* prevent round-off errors from accumulating by
         rounding any bufTime which is closer than 1usec of a
         one second mark to this mark */
    if ( ( pre->bufTime + 1000 ) % 1000000000 < 2000 )
    {
        pre->bufTime = 1000000000 * ( ( pre->bufTime + 1000 ) / 1000000000 );
    }

    /* check if we have left over points */
    if ( ndata * dec == length )
    {
        pre->bufSize = 0;
    }
    else
    {
        /* copy them into the input buffer */
        pre->bufSize = length - ndata * dec;
        memcpy( pre->buf, x + ndata * dec, pre->bufSize * sizeof( float ) );
        length = ndata * dec;
    }

    /* finally we have the data stream ready: up sample first */
    if ( pre->upfactor > 1 )
    {
        y = malloc( length * pre->upfactor * sizeof( float ) );
        if ( y == NULL )
        {
            if ( freex )
            {
                free( (float*)x );
            }
            return -2;
        }
        stepup( 0, x, y, length, pre->upfactor );
        length *= pre->upfactor;
        if ( freex )
        {
            free( (float*)x );
        }
        freex = 1;
        x = y;
    }

    /* time delay */
    if ( pre->delaytaps > 0 )
    {
        y = malloc( length * sizeof( float ) );
        if ( y == NULL )
        {
            if ( freex )
            {
                free( (float*)x );
            }
            return -2;
        }
        timedelay(
            x, y, length, pre->delaytaps, pre->tmpdelay1, &pre->tmpdelay1 );
        if ( freex )
        {
            free( (float*)x );
        }
        freex = 1;
        x = y;
    }

    /* first decimation */
    if ( pre->decimate1 > 1 )
    {
        y = malloc( length / pre->decimate1 * sizeof( float ) );
        if ( y == NULL )
        {
            if ( freex )
            {
                free( (float*)x );
            }
            return -2;
        }
        decimate( pre->decimationflag,
                  x,
                  y,
                  length,
                  pre->decimate1,
                  pre->tmpdec1,
                  &pre->tmpdec1 );
        length /= pre->decimate1;
        if ( freex )
        {
            free( (float*)x );
        }
        freex = 1;
        x = y;
    }

    /* zoom (or down-conversion) stage */
    complex = 1;
    if ( pre->zoomfreq > 0 )
    {
        /* we are going complex! */
        complex = 2;
        y = malloc( 2 * length * sizeof( float ) );
        if ( y == NULL )
        {
            if ( freex )
            {
                free( (float*)x );
            }
            return -2;
        }
        sMixdown( 0,
                  x,
                  NULL,
                  y,
                  y + length,
                  length,
                  (double)( t0 - pre->zoomstart ) / __ONESEC,
                  pre->dt[ 2 ],
                  pre->zoomfreq );
        if ( freex )
        {
            free( (float*)x );
        }
        freex = 1;
        x = y;
    }

    /* second decimation stage */
    if ( pre->decimate2 > 0 )
    {
        int len2 = length / pre->decimate2;
        y = malloc( complex * len2 * sizeof( float ) );
        if ( y == NULL )
        {
            if ( freex )
            {
                free( (float*)x );
            }
            return -2;
        }
        decimate( pre->decimationflag,
                  x,
                  y,
                  length,
                  pre->decimate2,
                  pre->tmpdec2I,
                  &pre->tmpdec2I );
        if ( complex == 2 )
        {
            decimate( pre->decimationflag,
                      x + length,
                      y + len2,
                      length,
                      pre->decimate2,
                      pre->tmpdec2Q,
                      &pre->tmpdec2Q );
        }
        length = len2;
        if ( freex )
        {
            free( (float*)x );
        }
        freex = 1;
        x = y;
        /* reshuffle re/im parts */
        if ( complex == 2 )
        {
            y = malloc( complex * len2 * sizeof( float ) );
            if ( y == NULL )
            {
                if ( freex )
                {
                    free( (float*)x );
                }
                return -2;
            }
            for ( i = 0; i < len2; i++ )
            {
                y[ 2 * i ] = x[ i ];
                y[ 2 * i + 1 ] = x[ len2 + i ];
            }
            if ( freex )
            {
                free( (float*)x );
            }
            freex = 1;
            x = y;
        }
    }

    /* now check output partitions */
    t0 = t0 - pre->start - pre->delayshift;
    dt = (tainsec_t)( length * pre->dt[ 3 ] * __ONESEC + 0.5 );
    part1 = (int)( ( t0 - pre->tpart + pre->dtpart + 1000 ) / pre->dtpart );
    if ( part1 < 0 )
    {
        part1 = 0;
    }
    part2 = (int)( ( t0 + dt + 1000 ) / pre->dtpart );
    partlen = (int)( (double)pre->dtpart / __ONESEC / pre->dt[ 3 ] + 0.5 );
    for ( i = part1; i <= part2; i++ )
    {
        int          len = length;
        int          index;
        const float* xp = x;
        /* calculate index into partition */
        if ( t0 > pre->dtpart * i )
        {
            index = (int)( (double)( t0 - pre->dtpart * i ) /
                               ( pre->dt[ 3 ] * __ONESEC ) +
                           0.5 );
        }
        else
        {
            index = (int)( (double)( pre->dtpart * i - t0 ) /
                               ( pre->dt[ 3 ] * __ONESEC ) +
                           0.5 );
            index *= -1;
        }
        /* data start before partition: throw away beginning */
        if ( index < 0 )
        {
            xp = xp + ( -index );
            len -= ( -index );
            index = 0;
        }
        /* data is longer than partition: truncate */
        if ( index + len > partlen )
        {
            len = partlen - index;
        }
        if ( len <= 0 )
        {
            continue;
        }
        /* write partition data */
        pre->write( i, index, xp, len, pre->user );
    }

    if ( freex )
    {
        free( (float*)x );
    }
    return 0;
}
