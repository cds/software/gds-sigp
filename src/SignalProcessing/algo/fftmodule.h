/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: fftmodule			        		*/
/*                                                         		*/
/* Module Description: various functions related to fft analysis of the */
/*                     LIGO data - power spectral estimation, cross     */
/*                     spectral estimation, pseudo-random simulus.      */
/*									*/
/* Module Arguments: none                				*/
/*									*/
/* Revision History:					   		*/
/* Rel   Date     Programmer   		Comments       	       		*/
/* 0.1   1/1/99   Edward Daw            Based on fftw fft library       */
/*                                                                      */
/* Documentation References: 						*/
/*	Man Pages: doc++ generated html					*/
/*	References: Numerical Recipes in C,                             */
/*                  http://theory.lcs.mit.edu/~fftw and docs therein    */
/*                  Blackman & Tukey: `The Measurement of Power Spectra'*/
/*                    Dover, p98 for definition of Hamming window       */
/*                  SRS785 manual for definitions of various windows    */
/*                                                         		*/
/* Author Information:							*/
/*	Name	Telephone    Fax          e-mail 			*/
/*	E. Daw  617-258-7697 617-253-7014 edaw@ligo.mit.edu		*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Unix						*/
/*	Compiler Used: Sun Sparcworks C compiler         		*/
/*	Runtime environment: UNIX    					*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	TBD			*/
/*			  Lint.			TBD			*/
/*			  ANSI			TBD			*/
/*			  POSIX			TBD			*/
/*									*/
/* Known Bugs, Limitations, Caveats:                       		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

/** @name FFT algorithms  
    @memo Fast Fourier transformation.
    @author Edward Daw, Jan 99 */

/*@{*/

#ifndef _FFTMODULE_H
#define _FFTMODULE_H

#ifdef __cplusplus
extern "C" {
#endif

#ifdef FFTW3
#include "fftw3.h"
#else
#include <sfftw.h> /*single precision fft of complex time series*/
#include <srfftw.h> /*single precision fft of real time series*/
#endif

/** @name Constants and flags for FFT Routines.

@memo Constants and Flags
@author Edward Daw  */

/*@{*/

/** @name Flags to determine powerspectrum routine  */

/*@{*/

/**Flag to specify power spectrum output = 0 */
#define FFT_FUNC_POWERSPECT 0
/**Flag to specify fft output = 1 */
#define FFT_FUNC_FFT 1
/**Flag to specify cross spectrum output = 2 */
#define FFT_FUNC_CROSSCORR 2
/**Flag to specify correlation with pseudorandom simulus = 3 */
#define FFT_FUNC_PSRANDSIMULUS 3

/*@}*/

/** @name Flags to select output data format for powerspectrum function */

/*@{*/

/**Flag to specify power spectrum output. The units
     are amplitude rms per bin width. */
#define OUTPUT_AMPPERRTBIN 0
/**Flag to specify power spectral density output. The units
     are amplitude rms per root Hz. */
#define OUTPUT_PSD 1
/**Flag to specify complex output, real, imag pairs, freq ordered.
     The units are amplitude rms per root Hz. */
#define OUTPUT_COMPMINUSFTOF 2
/**Flag to specify complex output, same ordering as fftw output.
     For complex data the ordering is the same as
     Numerical recipes, fig 12.2.2. For real data the ordering
     is as follows: if there are n frequency bins labelled from 0 -> (n-1),
     and r,i represent the imaginary parts respectively, then the
     ordering is r0 r1 r2 ... r(n-1) i0 i(n-1) i(n-2) ... i1.
     This funny ordering is a consequence of using a shorter fft to exploit
     the symmetries of real input data. The units are amplitude rms
     per root Hz for both real and imaginary parts. This option is
     the fastest because no rearranging of the output data is required.
     Ordering is such that, when amplitudes from both positive and
     negative frequencies are combined, the overall amplitude of the sum
     in quadrature of real and imaginary parts is the rms spectral density
     of a sine wave in that frequency bin in amplitude per rtHz. */
#define OUTPUT_COMPNATIVE 3
/** this flag causes output to be written in numerical recipes
      format (figure 12.2.2) for both real and complex data.
      normalization as for OUTPUT_COMPMINUSFTOF */
#define OUTPUT_GDSFORMAT 4
/** outputs GDS format. For real timeseries data, if r(i) 
      represent the real(imaginary) parts
      of fourier components, the ordering of the fourier components
      at output is r0 i0 r1 i1 r2 i2 .... r(n/2-1) i(n/2-1). Here
      n/2-1 is the bin just below the nyquist frequienc bin. The 0th
      bin is the DC component and has an zero imaginary part i0=0.
      For complex timeseries data the output format is exactly as
      arranged in Numerical Recipes, fig 12.2.2. Normalization is
      as for OUTPUT_MINUSFTOF */

/*@}*/

/** @name Flags to select the mode of psGen. The mode flag is one
      of the following:
      \begin{verbatim}
      PS_INIT_PLAN - generates an fftw plan only, no fft is returned
      PS_TAKE_FFT - computes the FFT with the supplied plan & window
      PS_CLEAN_PLAN - destoyes the existing plan
      PS_INIT_WIN - initializes the window function only
      PS_INIT_ALL - generates a plan and initializes the window function
      PS_INIT_ALL_AND_TAKE_FFT - generates plan, initializes window
         function and computes the FFT
      \end{verbatim}
      It can be or'ed together with the folowing flags to select 
      further options:
      \begin{verbatim}
      PS_REMOVE_DC - removes the mean from the time series before
         computing the FFT
      \end{verbatim}

      The usage flag can be obtained by using the mask PS_USE_MASK,
      i.e., (p_mode & PS_USE_MASK).
    */

/*@{*/

/**Returns an fftw plan only, no fft generated */
#define PS_INIT_PLAN 0
/**Makes an fft with the supplied plan */
#define PS_TAKE_FFT 1
/**Destroys an existing plan */
#define PS_CLEAN_PLAN 2
/**Initialize a windowing function */
#define PS_INIT_WIN 3
/**Initialize a windowing function and generate a plan, but take no fft */
#define PS_INIT_ALL 4
/**Generate an fft having created a plan and a window function */
#define PS_INIT_ALL_AND_TAKE_FFT 5
/** Usage mask for above flags */
#define PS_USE_MASK 7
/** Flag to remove the DC term before taking the FFT */
#define PS_REMOVE_DC 8

/*@}*/

/*@}*/

/** @name FFT parameter type.
    Parameter structure for storing an FFT plan and window coefficients.

    @memo FFT parameter type
    @author Edward Daw, Jan 99
   **********************************************************************/

/*@{*/

/** FFT parameter structure. Used to store teh FFT plan and the 
    and the window function.
   **********************************************************************/
struct fftparam
{
#ifdef FFTW3
    /** pointer to a FFTW plan */
    fftw_plan real_plan;
    fftw_plan complex_plan;
#else
    /** pointer to a real-FFT plan */
    rfftw_plan* real_plan;
    /** pointer to a complex-FFT plan */
    fftw_plan* complex_plan;
#endif
    /** coefficients of the windowing function */
    double* window_coeff;
    /** temporay storage to hold the windowed time series */
    double* windowed_data;
};
typedef struct fftparam fftparam;

/*@}*/

/** @name Spectral analysis Functions.
    @memo Functions
    @author Edward Daw, Jan 99
   **********************************************************************/

/*@{*/

/** FFT function.
    This function generates an fft of a real or complex data set. The
    output can be either a power spectrum or the raw fft. The input data
    can be either a single array of floats for real input data or an array
    of pairs of floats for complex input data array.

    @param p_mode  For an explanation of possible values see above
		   Flags.
    @param pset A structure of type fftparam (see header fftmodule.h
                for definition). pset->windowed_data and 
	        pset->window_coeff should be allocated memory by the
	        calling function. pset->windowed_data should have
                2*timeseries_length*sizeof(float) length if the data is
	        complex, or timeseries_length*sizeof(float) if real.
	        pset->window_coeff should be of size 
                timeseries_length*sizeof(float). This structure can be
	        kept between multiple function calls if psGen will be
 	        called for the same sized data set multiple times for
	        the same data_type.
    @param timeseries_length The number of real or complex data points
                             in the time series. Should always be a 
		 	     power of two. Function will return an error
			     (-1) if it is not.
    @param data_type Either DATA_REAL (0) or DATA_COMPLEX (1). Refers
                     to the timeseries data input.
    @param data An array of real or complex input data.
    @param timestep The time interval (s) between successive data points.
    @param output_format One of 5 choices
                OUTPUT_AMPPERRTBIN (=0) a power spectrum with 
	 	normalization such that a sine wave input of
		amplitude 1 yields a peak of height 1/sqrt2.
		OUTPUT_PSD (=1) a power spectral density with
		units rms amplitude per rtHz.
		OUTPUT_COMPMINUSFTOF (=2) an fft output with the most
		negative negative frequency { (-Fn*(n/2-1 / n/2))
		where Fn is the nyquist frequency and n is the number
		of timeseries bins } first and the nyquist frequency
		last. Each bin has the real part first and the
		imaginary second. The normalization is such that
		the sum of contributions from -F and F yields
		a power spectral density in amplitude rms per rtHz.
                OUTPUT_COMPNATIVE (=3) returns data in the same format
		as used by fftw. The relevant manual pages are in:
		theory.lcs.mit.edu/~fftw/doc/fftw_3.html#SEC17.
		For complex input data, the format is the same
		as numerical recipes fig 12.2.2
		OUTPUT_GDSFORMAT (=4) .For real input data the format
		is r0 i0 r1 i1 ... r(n/2-1) i(n/2-1). The first
		bin is DC, so i0=0. The last bin is the frequency
		just below the nyquist frequency. For complex input
		data, the format is the same as numerical recipes
		figure 12.2.2 (DC first, then positive frequency
		components in ascending order, then the nyquist
		frequency, then from the most negative frequency 
		above (-) the nyquist frequency up to the bin just
		below DC. Each bin is 2 elements, real then imag.
    @param window Determines the window type. 
                 WINDOW_UNIFORM (=0) no windowing
		 WINDOW_HANNING (=1) from numerical recipes
		 WINDOW_FLATTOP (=2) from Stanford SR785 manual
		 WINDOW_WELCH   (=3) from Numerical Recipes
		 WINDOW_BARTLETT (=4) from Numerical Recipes
		 WINDOW_BMH (=5) from Stanford SR785 manual
		 WINDOW_HAMMING (=6) from Blackman and Tukey (see top 
                 of page)
    @param result Pointer to the result data. Always allocate
                 timeseries_length*2*sizeof(float) bytes to this pointer,
		 even if the output data set will be shorter than this.
    @returns -1 if error, 0 otherwise.
    @author Edward Daw, June 1999
   **********************************************************************/
int psGen( int          p_mode,
           fftparam*    pset,
           int          timeseries_length,
           int          data_type,
           const double* data,
           double        timestep,
           int          output_format,
           int          window,
           double*      result );

/** Rearranges output data.
    This function rearranges output data into one of various output
    formats, defined in Flags to select output format, see constants
    and flags section of this manual. Options are:
    \begin{verbatim}
    OUTPUT_AMPPERRTBIN (=0)
    OUTPUT_PSD (=1)
    OUTPUT_COMPMINUSFTOF (=2)
    OUTPUT_COMPNATIVE (=3)
    OUTPUT_COMPGDS (=4)
    \end{verbatim}
    See above explanation of psGen() for a description of the various
    formats.
  
    @param output_format which of the above output formats is required.
    @param data_type DATA_REAL or DATA_COMPLEX. Which type of input data
                   was used.
    @param timeseries_length Length of the time series data array.
    @param timestep The time interval between successive samples in seconds.
    @param result A pointer to the data array
    @returns -3 if you do not select a valid output format type,
              0 if successful.
    @author Edward Daw, 16th June 1999
   **********************************************************************/
int psDataPack( int     output_format,
                int     data_type,
                int     timeseries_length,
                float   timestep,
                double* result );

/** Checks if power of two.
    This function checks to see if an integer is a power of two.

    @param val Number to be checked
    @returns -1 if not a power of two, +1 if a power of two.
    @author Edward Daw, June 1999
   **********************************************************************/
int isitapoweroftwo( int val );

/** Cross Spectrum Function.
    This function computes the cross spectrum of two fourier series.
    
    @param data_length the number of bins in the fourier series
    @param data_x the first fourier series
    @param data_y the second fourier series
    @param cs_result the resulting cross spectrum
    @returns 0 if successful, -1 if error
    @author Edward Daw, June 1999
   **********************************************************************/
int crossSpect( int          data_length,
                const float* fdata_x,
                const float* fdata_y,
                float*       cs_result );

/** Cross Power Spectrum Function.
    This function computes the cross power spectrum of two fourier series.
    It gives the same result as the crossSpect functionh but with a different
    normalization, ie., the absolute value of a data point represents the
    true power at this frequency.
    
    @param data_length the number of bins in the fourier series
    @param data_type DATA_REAL, if the original time datas were unheterodyned,
                     DATA_COMPLEX if the time datas had been hetrodyned.
    @param data_x the first fourier series
    @param data_y the second fourier series
    @param cs_result the resulting cross power spectrum
    @returns 0 if successful, -1 if error
    @author Edward Daw, June 1999
   **********************************************************************/
int crossPower( int           data_length,
                int           data_type,
                const double* fdata_x,
                const double* fdata_y,
                double*       cs_result );

/** Power spectrum function.
    This function converts an fft of either real timeseries data or
    complex timeseries data into a power spectrum. It is assumed that
    the input data has been generated by psGen using the output format
    option OUTPUT_GDSFORMAT. The output array should be allocated
    (data_length/2)*sizeof(float) if the input data was real or
    data_length*sizeof(float) if the input data was complex.
    The resulting power spectrum is in amplitude rms per rtHz.

    @param data_length the number of complex bins in the fourier series
    @param data_type either DATA_REAL or DATA_COMPLEX. Refers to the
                     data type of the original timeseries. 
    @param input a pointer to the input data
    @param output a pointer to the output data
    @returns 0 if successful, -1 if error.
    @author Edward Daw, June 1999
   **********************************************************************/
int
fftToPs( int data_length, int data_type, const double* input, double* output );

/** Rotates the FFT output data array.
    This function rotates the two sided power spectrum 
    or fourier series generated by
    fftToPs if the timeseries data is of type DATA_COMPLEX so that the
    most negative -ve frequency (the nyquist frequency)
    is the first bin and the most positive (one below the nyquist frequency)
    frequency is the last bin. This rotation can be done for both
    power spectra and ffts themselves, depending on the value
    of the flag data_type. output should be allocated the same amount
    of memory space as the input array.

    @param data_length the number of bins in the power spectrum or fft.
    @param data_type DATA_REAL (a power spectrum) or DATA_COMPLEX (an fft)
    @param input a pointer to the input array
    @param output a pointer to the output array
    @returns 0 if successful, -1 if error
    @author Edward Daw, June 1999
   **********************************************************************/
int dataRotator( int           data_length,
                 int           data_type,
                 const double* input,
                 double*       output );

/** Coherence function.
    This function computes the coherence from the power spectra
    of two time series and the cross spectrum between them. The
    result array (data_length/2)*sizeof(float) for real (unheterodyned)
    input data and data_length*sizeof(float) for complex (heterodyned)
    data.

    @param data_length the number of frequency bins in the data set
    @param data_type DATA_REAL, if the original time datas were unheterodyned,
                     DATA_COMPLEX if the time datas had been hetrodyned.
    @param pxxdata the power spectrum of the first (x) data set. An array
                   of datalength floats for datalength bins of power
		   spectrum data for the first (x) data set.
    @param pyydata same as pxxdata for the second (y) data set.
    @param pxydata the cross spectrum between the two data sets. An array
                   of datalength pairs of floats for datalength complex
                   cross spectrum data points.
    @param result the coherence result
    @returns 0 if successful, -1 if error
    @author Edward Daw, July 1999
   **********************************************************************/
int coherence( int          data_length,
               int          data_type,
               const float* pxxdata,
               const float* pyydata,
               const float* pxydata,
               float*       result );

/** Coherence function.
    This function computes the coherence from the power spectra
    of two time series and the cross power spectrum between them. The
    result array (data_length/2)*sizeof(float) for real (unheterodyned)
    input data and data_length*sizeof(float) for complex (heterodyned)
    data.

    @param data_length the number of frequency bins in the data set
    @param pxxdata the power spectrum of the first (x) data set. An array
                   of datalength floats for datalength bins of power
		   spectrum data for the first (x) data set.
    @param pyydata same as pxxdata for the second (y) data set.
    @param pxydata the cross power spectrum between the two data sets. An array
                   of datalength pairs of floats for datalength complex
                   cross spectrum data points.
    @param result the coherence result
    @returns 0 if successful, -1 if error
    @author Edward Daw, July 1999
   **********************************************************************/
int coherenceCP( int           data_length,
                 const double* pxxdata,
                 const double* pyydata,
                 const double* pxydata,
                 double*       result );

/** Transfer function
 *
 * Compute the transfer function Txy given the cross spectral density of x and y (Cxy) and the amplitude
 * spectral density of x (Axx) using the formula
 *
 * Txy = Cxy / Pxx = Cxy / (Axx)^2
 *
 *
 * @param data_length number of frequency buckets in the transfer function
 * @param csd_in an array of complex values giving the cross spectral density, Cxy, for each bucket
 * @param a_xx_data_in an array of real values giving the amplitude spectral density, Axx, for the x channel
 * @param tf_out output array of complex values must be allocated by the caller to store data_length complex values
 * @return 0 if successful, -1 if error
 */
int transfer_function( int           data_length,
                       const double* csd_in,
                       const double* a_xx_data_in,
                       float*        tf_out );

/*@}*/

/*@}*/

#ifdef __cplusplus
}
#endif

#endif
