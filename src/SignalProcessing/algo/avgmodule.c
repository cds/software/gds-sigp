/* -*- mode: c++; c-basic-offset: 3; -*- */

/*
 * These routines perform averaging of real or complex float data
 * This code is preliminary; report all bugs to edaw@ligo.mit.edu
 * 
 * Edward Daw, 6/11/99
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "gdsconst.h"
#include "gdstype.h"
#include "avgmodule.h"

int
avg( const avg_specs* specs,
     int              number_of_sets,
     const float*     input_data,
     int*             number_so_far,
     float*           averaged_data )
{
    int i;
    int j;
    int avgtype; /*type of average - linear or exp, modulus or vector*/
    int datalength; /*number of elements in real or complex data array*/
    int datatype; /*DATA_REAL or DATA_COMPLEX*/
    int navg; /*number of averages, set to -1 for exponential*/
    const float*  rindata; /*pointer to real averaged input data*/
    const sCmplx* cindata; /*pointer to complex averaged input data*/
    float*        rdata; /*pointer to real averaged output data*/
    sCmplx*       cdata; /*pointer to complex averaged output data*/
    const float*  indatapointer; /*pointer to where to start averaging in the
   		  input data*/
    int           nsets; /*number of data sets added to the average at one go*/

    /*setup internal buffers for averaging parameters*/

    avgtype = specs->avg_type;
    datalength = specs->dataset_length;
    datatype = specs->data_type;
    navg = specs->number_of_averages;
    indatapointer = input_data;
    nsets = number_of_sets;

    if ( *number_so_far == 0 )
    {
        switch ( datatype )
        {
        case DATA_REAL: {
            memcpy( averaged_data, input_data, datalength * sizeof( float ) );
            if ( ( avgtype == AVG_LINEAR_SQUARE ) ||
                 ( avgtype == AVG_EXPON_SQUARE ) )
            {
                for ( j = 0; j < datalength; j++ )
                {
                    averaged_data[ j ] = (float)fabs( averaged_data[ j ] );
                }
            }
            indatapointer = input_data + datalength;
            break;
        }
        case DATA_COMPLEX: {
            switch ( avgtype )
            {
            case AVG_LINEAR_MODULUS:
            case AVG_EXPON_MODULUS:
            case AVG_LINEAR_SQUARE:
            case AVG_EXPON_SQUARE: {
                cindata = (const sCmplx*)input_data;
                rdata = (float*)averaged_data;
                for ( j = 0; j < datalength; ++j )
                {
                    double cre = cindata[ j ].re;
                    double cim = cindata[ j ].im;
                    rdata[ j ] = (float)sqrt( cre * cre + cim * cim );
                }
                break;
            }
            case AVG_LINEAR_VECTOR:
            case AVG_EXPON_VECTOR: {
                memcpy(
                    averaged_data, input_data, datalength * sizeof( sCmplx ) );
                break;
            }
            }
            indatapointer =
                (const float*)( ( (sCmplx*)input_data ) + datalength );
            break;
        }
        }
        ++( *number_so_far );
        --nsets;
        if ( nsets == 0 )
            return 0;
    }

    switch ( datatype )
    {
    case DATA_REAL: {
        switch ( avgtype )
        {
        case AVG_LINEAR_MODULUS:
        case AVG_LINEAR_VECTOR: {
            rindata = (const float*)indatapointer;
            rdata = (float*)averaged_data;
            for ( i = 0; i < nsets; ++i )
            {
                for ( j = 0; j < datalength; ++j )
                {
                    rdata[ j ] = (float)( ( (double)rdata[ j ] *
                                                (double)( *number_so_far ) +
                                            (double)rindata[ j ] ) /
                                          (double)( *number_so_far + 1 ) );
                }
                rindata += datalength;
                ++( *number_so_far );
            }
            return 0;
        }
        case AVG_EXPON_MODULUS:
        case AVG_EXPON_VECTOR: {
            rindata = (const float*)indatapointer;
            rdata = (float*)averaged_data;
            for ( i = 0; i < nsets; ++i )
            {
                for ( j = 0; j < datalength; ++j )
                {
                    rdata[ j ] =
                        ( rdata[ j ] * ( navg - 1 ) + rindata[ j ] ) / navg;
                }
                rindata += datalength;
                ++( *( number_so_far ) );
            }
            return 0;
        }
        case AVG_LINEAR_SQUARE: {
            rindata = (const float*)indatapointer;
            rdata = (float*)averaged_data;
            for ( i = 0; i < nsets; ++i )
            {
                double n = (double)*number_so_far;
                for ( j = 0; j < datalength; ++j )
                {
                    double rindj = rindata[ j ];
                    double rdj = rdata[ j ];
                    rdata[ j ] =
                        sqrt( ( rdj * rdj * n + rindj * rindj ) / ( n + 1 ) );
                }
                rindata += datalength;
                ++( *number_so_far );
            }
            return 0;
        }
        case AVG_EXPON_SQUARE: {
            rindata = (const float*)indatapointer;
            rdata = (float*)averaged_data;
            for ( i = 0; i < nsets; ++i )
            {
                for ( j = 0; j < datalength; ++j )
                {
                    double rindj = rindata[ j ];
                    double rdj = rdata[ j ];
                    rdata[ j ] = sqrt(
                        ( rdj * rdj * ( navg - 1 ) + rindj * rindj ) / navg );
                }
                rindata += datalength;
                ++( *( number_so_far ) );
            }
            return 0;
        }
        default: {
            return -1;
        }
        }
    }
    case DATA_COMPLEX: {
        switch ( avgtype )
        {
        case AVG_LINEAR_MODULUS: {
            cindata = (const sCmplx*)indatapointer;
            rdata = (float*)averaged_data;
            for ( i = 0; i < nsets; ++i )
            {
                double n = (double)*number_so_far;
                for ( j = 0; j < datalength; ++j )
                {
                    double cre = cindata->re;
                    double cim = cindata->im;
                    rdata[ j ] = ( (double)rdata[ j ] * n +
                                   sqrt( cre * cre + cim * cim ) ) /
                        ( n + 1.0 );
                    cindata++;
                }
                ++( *number_so_far );
            }
            return 0;
        }
        case AVG_EXPON_MODULUS: {
            cindata = (const sCmplx*)indatapointer;
            rdata = (float*)averaged_data;
            for ( i = 0; i < nsets; ++i )
            {
                for ( j = 0; j < datalength; ++j )
                {
                    double cre = cindata->re;
                    double cim = cindata->im;
                    rdata[ j ] = ( (double)rdata[ j ] * ( navg - 1 ) +
                                   sqrt( cre * cre + cim * cim ) ) /
                        navg;
                    cindata++;
                }
                ++( *number_so_far );
            }
            return 0;
        }
        case AVG_LINEAR_VECTOR: {
            cindata = (const sCmplx*)indatapointer;
            cdata = (sCmplx*)averaged_data;
            for ( i = 0; i < nsets; ++i )
            {
                for ( j = 0; j < datalength; ++j )
                {
                    ( cdata + j )->re =
                        ( ( ( cdata + j )->re ) * ( *number_so_far ) +
                          ( cindata + j )->re ) /
                        ( ( *number_so_far ) + 1 );
                    ( cdata + j )->im =
                        ( ( ( cdata + j )->im ) * ( *number_so_far ) +
                          ( cindata + j )->im ) /
                        ( ( *number_so_far ) + 1 );
                }
                cindata += datalength;
                ++( *number_so_far );
            }
            return 0;
        }
        case AVG_EXPON_VECTOR: {
            cindata = (const sCmplx*)indatapointer;
            cdata = (sCmplx*)averaged_data;
            for ( i = 0; i < nsets; ++i )
            {
                for ( j = 0; j < datalength; ++j )
                {
                    ( cdata + j )->re = ( ( ( cdata + j )->re ) * ( navg - 1 ) +
                                          ( cindata + j )->re ) /
                        navg;
                    ( cdata + j )->im = ( ( ( cdata + j )->im ) * ( navg - 1 ) +
                                          ( cindata + j )->im ) /
                        navg;
                }
                cindata += datalength;
                ++( *number_so_far );
            }
            return 0;
        }
        case AVG_LINEAR_SQUARE: {
            cindata = (const sCmplx*)indatapointer;
            rdata = (float*)averaged_data;
            for ( i = 0; i < nsets; ++i )
            {
                double n = (double)*number_so_far;
                for ( j = 0; j < datalength; ++j )
                {
                    //--------  Note that the original had
                    // rd[j] = sqrt(rdj^2*n + (crej^2 + cimj^2)/(n+1))
                    double rdj = rdata[ j ];
                    double crej = cindata->re;
                    double cimj = cindata->im;
                    rdata[ j ] =
                        sqrt( ( rdj * rdj * n + crej * crej + cimj * cimj ) /
                              ( n + 1 ) );
                    cindata++;
                }
                ++( *number_so_far );
            }
            return 0;
        }
        case AVG_EXPON_SQUARE: {
            cindata = (const sCmplx*)indatapointer;
            rdata = (float*)averaged_data;
            for ( i = 0; i < nsets; ++i )
            {
                for ( j = 0; j < datalength; ++j )
                {
                    double rdj = rdata[ j ];
                    double crej = cindata->re;
                    double cimj = cindata->im;
                    rdata[ j ] = sqrt( ( rdj * rdj * (double)( navg - 1 ) +
                                         crej * crej + cimj * cimj ) /
                                       (double)navg );
                    cindata++;
                }
                ++( *number_so_far );
            }
            return 0;
        }
        default: {
            return -1;
        }
        }
    }
    default: {
        return -1;
    }
    }
}

int
davg( const avg_specs* specs,
      int              number_of_sets,
      const double*    input_data,
      int*             number_so_far,
      double*          averaged_data )
{
    int i;
    int j;
    int avgtype; /*type of average - linear or exp, modulus or vector*/
    int datalength; /*number of elements in real or complex data array*/
    int datatype; /*DATA_REAL or DATA_COMPLEX*/
    int navg; /*number of averages, set to -1 for exponential*/
    const double* rindata; /*pointer to real averaged input data*/
    const dCmplx* cindata; /*pointer to complex averaged input data*/
    double*       rdata; /*pointer to real averaged output data*/
    dCmplx*       cdata; /*pointer to complex averaged output data*/
    const double* indatapointer; /*pointer to where to start averaging in the
                 input data*/
    int           nsets; /*number of data sets added to the average at one go*/

    /*setup internal buffers for averaging parameters*/

    avgtype = specs->avg_type;
    datalength = specs->dataset_length;
    datatype = specs->data_type;
    navg = specs->number_of_averages;
    indatapointer = input_data;
    nsets = number_of_sets;

    if ( *number_so_far == 0 )
    {
        switch ( datatype )
        {
        case DATA_REAL: {
            memcpy( averaged_data, input_data, datalength * sizeof( double ) );
            if ( ( avgtype == AVG_LINEAR_SQUARE ) ||
                 ( avgtype == AVG_EXPON_SQUARE ) )
            {
                for ( j = 0; j < datalength; j++ )
                {
                    averaged_data[ j ] = fabs( averaged_data[ j ] );
                }
            }
            indatapointer = input_data + datalength;
            break;
        }
        case DATA_COMPLEX: {
            switch ( avgtype )
            {
            case AVG_LINEAR_MODULUS:
            case AVG_EXPON_MODULUS:
            case AVG_LINEAR_SQUARE:
            case AVG_EXPON_SQUARE: {
                cindata = (const dCmplx*)input_data;
                rdata = averaged_data;
                for ( j = 0; j < datalength; ++j )
                {
                    double cre = cindata[ j ].re;
                    double cim = cindata[ j ].im;
                    rdata[ j ] = sqrt( cre * cre + cim * cim );
                }
                break;
            }
            case AVG_LINEAR_VECTOR:
            case AVG_EXPON_VECTOR: {
                memcpy(
                    averaged_data, input_data, datalength * sizeof( dCmplx ) );
                break;
            }
            }
            indatapointer =
                (const double*)( ( (dCmplx*)input_data ) + datalength );
            break;
        }
        }
        ++( *number_so_far );
        --nsets;
        if ( nsets == 0 )
            return 0;
    }

    switch ( datatype )
    {
    case DATA_REAL: {
        switch ( avgtype )
        {
        case AVG_LINEAR_MODULUS:
        case AVG_LINEAR_VECTOR: {
            rindata = indatapointer;
            rdata = averaged_data;
            for ( i = 0; i < nsets; ++i )
            {
                for ( j = 0; j < datalength; ++j )
                {
                    rdata[ j ] = ( ( rdata[ j ] * (double)( *number_so_far ) +
                                     rindata[ j ] ) /
                                   (double)( *number_so_far + 1 ) );
                }
                rindata += datalength;
                ++( *number_so_far );
            }
            return 0;
        }
        case AVG_EXPON_MODULUS:
        case AVG_EXPON_VECTOR: {
            rindata = indatapointer;
            rdata = averaged_data;
            for ( i = 0; i < nsets; ++i )
            {
                for ( j = 0; j < datalength; ++j )
                {
                    rdata[ j ] =
                        ( rdata[ j ] * ( navg - 1 ) + rindata[ j ] ) / navg;
                }
                rindata += datalength;
                ++( *( number_so_far ) );
            }
            return 0;
        }
        case AVG_LINEAR_SQUARE: {
            rindata = indatapointer;
            rdata = averaged_data;
            for ( i = 0; i < nsets; ++i )
            {
                double n = (double)*number_so_far;
                for ( j = 0; j < datalength; ++j )
                {
                    double rindj = rindata[ j ];
                    double rdj = rdata[ j ];
                    rdata[ j ] =
                        sqrt( ( rdj * rdj * n + rindj * rindj ) / ( n + 1 ) );
                }
                rindata += datalength;
                ++( *number_so_far );
            }
            return 0;
        }
        case AVG_EXPON_SQUARE: {
            rindata = indatapointer;
            rdata = averaged_data;
            for ( i = 0; i < nsets; ++i )
            {
                for ( j = 0; j < datalength; ++j )
                {
                    double rindj = rindata[ j ];
                    double rdj = rdata[ j ];
                    rdata[ j ] = sqrt(
                        ( rdj * rdj * ( navg - 1 ) + rindj * rindj ) / navg );
                }
                rindata += datalength;
                ++( *( number_so_far ) );
            }
            return 0;
        }
        default: {
            return -1;
        }
        }
    }
    case DATA_COMPLEX: {
        switch ( avgtype )
        {
        case AVG_LINEAR_MODULUS: {
            cindata = (const dCmplx*)indatapointer;
            rdata = averaged_data;
            for ( i = 0; i < nsets; ++i )
            {
                double n = (double)*number_so_far;
                for ( j = 0; j < datalength; ++j )
                {
                    double cre = cindata->re;
                    double cim = cindata->im;
                    rdata[ j ] =
                        ( rdata[ j ] * n + sqrt( cre * cre + cim * cim ) ) /
                        ( n + 1.0 );
                    cindata++;
                }
                ++( *number_so_far );
            }
            return 0;
        }
        case AVG_EXPON_MODULUS: {
            cindata = (const dCmplx*)indatapointer;
            rdata = averaged_data;
            for ( i = 0; i < nsets; ++i )
            {
                for ( j = 0; j < datalength; ++j )
                {
                    double cre = cindata->re;
                    double cim = cindata->im;
                    rdata[ j ] = ( rdata[ j ] * ( navg - 1 ) +
                                   sqrt( cre * cre + cim * cim ) ) /
                        navg;
                    cindata++;
                }
                ++( *number_so_far );
            }
            return 0;
        }
        case AVG_LINEAR_VECTOR: {
            cindata = (const dCmplx*)indatapointer;
            cdata = (dCmplx*)averaged_data;
            for ( i = 0; i < nsets; ++i )
            {
                for ( j = 0; j < datalength; ++j )
                {
                    ( cdata + j )->re =
                        ( ( ( cdata + j )->re ) * ( *number_so_far ) +
                          ( cindata + j )->re ) /
                        ( ( *number_so_far ) + 1 );
                    ( cdata + j )->im =
                        ( ( ( cdata + j )->im ) * ( *number_so_far ) +
                          ( cindata + j )->im ) /
                        ( ( *number_so_far ) + 1 );
                }
                cindata += datalength;
                ++( *number_so_far );
            }
            return 0;
        }
        case AVG_EXPON_VECTOR: {
            cindata = (const dCmplx*)indatapointer;
            cdata = (dCmplx*)averaged_data;
            for ( i = 0; i < nsets; ++i )
            {
                for ( j = 0; j < datalength; ++j )
                {
                    ( cdata + j )->re = ( ( ( cdata + j )->re ) * ( navg - 1 ) +
                                          ( cindata + j )->re ) /
                        navg;
                    ( cdata + j )->im = ( ( ( cdata + j )->im ) * ( navg - 1 ) +
                                          ( cindata + j )->im ) /
                        navg;
                }
                cindata += datalength;
                ++( *number_so_far );
            }
            return 0;
        }
        case AVG_LINEAR_SQUARE: {
            cindata = (const dCmplx*)indatapointer;
            rdata = averaged_data;
            for ( i = 0; i < nsets; ++i )
            {
                double n = (double)*number_so_far;
                for ( j = 0; j < datalength; ++j )
                {
                    //--------  Note that the original had
                    // rd[j] = sqrt(rdj^2*n + (crej^2 + cimj^2)/(n+1))
                    double rdj = rdata[ j ];
                    double crej = cindata->re;
                    double cimj = cindata->im;
                    rdata[ j ] =
                        sqrt( ( rdj * rdj * n + crej * crej + cimj * cimj ) /
                              ( n + 1 ) );
                    cindata++;
                }
                ++( *number_so_far );
            }
            return 0;
        }
        case AVG_EXPON_SQUARE: {
            cindata = (const dCmplx*)indatapointer;
            rdata = averaged_data;
            for ( i = 0; i < nsets; ++i )
            {
                for ( j = 0; j < datalength; ++j )
                {
                    double rdj = rdata[ j ];
                    double crej = cindata->re;
                    double cimj = cindata->im;
                    rdata[ j ] = sqrt( ( rdj * rdj * (double)( navg - 1 ) +
                                         crej * crej + cimj * cimj ) /
                                       (double)navg );
                    cindata++;
                }
                ++( *number_so_far );
            }
            return 0;
        }
        default: {
            return -1;
        }
        }
    }
    default: {
        return -1;
    }
    }
}