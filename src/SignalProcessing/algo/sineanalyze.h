/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: sineAnalyze						*/
/*                                                         		*/
/* Module Description: Calculates amplitude & phase of a sine wave	*/
/*		       from a time series, used for swept-sine analysis */
/*									*/
/*                                                         		*/
/* Module Arguments:					   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer   		Comments       	       		*/
/* 0.1  12/08/98 Peter Fritschel       	       				*/
/* 0.2	01/18/99     PF			added averaging capability	*/
/* 0.3  03/25/99     PF			changed normalization factor    */
/*					to give peak amplitude		*/
/* 0.4  05/11/99     PF			added phase offset argument     */
/*                                                         		*/
/* Documentation References: 						*/
/*	Man Pages: doc++ generated html					*/
/*	References:							*/
/*                                                         		*/
/* Author Information:							*/
/*	Name          Telephone    Fax          e-mail 			*/
/*	P Fritschel   617-253-8153 617-253-7014 pf@ligo.mit.edu		*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Unix						*/
/*	Compiler Used: Sun cc						*/
/*	Runtime environment: Solaris 					*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			TBD			*/
/*			  POSIX			TBD			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _SINEANALYZE_H
#define _SINEANALYZE_H

#ifdef __cplusplus
extern "C" {
#endif

/* Header File List: */
#include "gdssigproc.h"

/**@name Swept Sine Algorithms
   This module calculates the amplitude and phase of a sine wave
   in time series data, used to calculate frequency responses.

   @memo Swept Sine.
   @author Written Dec. 1998 by Peter Fritschel
   @version 0.1
   *********************************************************************/

/*@{*/

/** @name Constants and flags.
    * Constants and flags of the sine-analyze routine.

    @author PF, Dec 98
    @see 
************************************************************************/

/*@{*/

/** Number of leading coefficients in sineAnalyze. */
#define N_LEAD 24

/*@}*/

/** @name Functions.
    @author PF, Dec 98
    @see
*********************************************************************/

/*@{*/

/** This function calculates the complex amplitude of a fourier
    component in a time series. The data is first mixed-down - ie,
    it is muliplied by exp(2pi*i*fsine*t). The resulting real &
    imaginary parts are each low-pass filtered with an FIR filter,
    and numerically integrated over an integral number of cycles of
    fsine to extract the amplitude at fsine. Multiple measurements
    can be made on successive pieces of data by setting the
    number of averages, nAvg >1. The function returns both the
    averaged complex amplitude, and an array of amplitudes from
    each measurement; the latter would be used to calculate the
    coherence with another channel. The amplitude is normalized
    so that the magnitude is the peak amplitude of the sine-wave.
    To determine the number of input data points needed, the
    function sweptSineNpts should be used. The toff argument can be
    used to pass a time offset to the mixdown (heterodyne) function
    (which sineAnalyze calls). The flag argument is used to control a
    offset removal option: 0 for no action; 1 to remove the mean
    from the input time series. The data array must cover a minimum
    of one full sine cycle per required average plus the number of
    lead terms plus one, otherwise the function returns with an
    error of -2. The function will truncate the number of points
    for each average so that the last point just comes after the 
    zero crossing of the last cycle. Optionally, a window function
    can be applied to the down-converted time series to minimize
    leakage from close-by frequencies.

    @param flag offset removal option
    @param window type of window function to be used
    @param x input time series
    @param npt number of points in the input array
    @param fs sampling rate of input time series
    @param fsine frequency at which sine is calculated
    @param nAvg number of averages (# of independent measurements)
    @param toff phase offset for the mixdown function
    @param sine_amp complex amplitude of sine wave (return)
    @param sineVals array of individual amplitudes, nAvg total (return)
    @return GDS_ERROR

    @memo Calculates amplitude & phase of a sine wave in time series data
    @author PF, Dec 1998
    @see 
**********************************************************************/
int sineAnalyze( int         flag,
                 int         window,
                 const float x[],
                 int         npt,
                 double      fs,
                 double      fsine,
                 int         nAvg,
                 double      toff,
                 dCmplx*     sine_amp,
                 dCmplx      sineVals[] );

/**   Function to calculate trailing coefficients of integration algorithm  

      @param delta  the fraction of a sampling period covered by the last
                    integration span.
      @param trail_coeff[] array of calculated trailing coeffecients in
                           the integration algorithm.
      @return void


      @memo Calculates the trailing coefficients, given delta
      @author PF, Dec 1998

***********************************************************************/
void calcTrailcoeff( double delta, double trail_coeff[] );

/**  Function to calculate number of data points needed by the
     integration algorithm, per average

     @param t_integ integration time (covering an integral number of
            sine cycles), seconds
     @param fs sampling rate (Hz)
     @return int  number of points

     @memo Calculates number of points needed by sineAnalyze.c
     @author PF, Jan 1999
  *********************************************************************/
int sweptSineNpts( double t_integ, double fs );

/**  Function to calculate the coherence between two sets of
     data returned by sineAnalyze. The coherence is a number
     ranging from 0 to 1, indicating the fraction of the power
     (at the sine detection frequency) in the response channel
     is caused by the stimulus channel. This function would be
     called for each frequency point in a swept sine response.

     @param x1 array of sineVals returned by sineAnalyze (stimulus, eg)
     @param x2 array of sineVals returned by sineAnalyze (response, eg)
     @param int  number of averages (points in x1 & x2)
     @return the coherence, as a double

     @memo Calculates the coherence between two swept sine channels
     @author PF, Jan 1999
*********************************************************************/
double ssCoherence( const dCmplx x1[], const dCmplx x2[], int navg );

/*@}*/

/*@}*/

#ifdef __cplusplus
}
#endif

#endif /*_SINEANALYZE_H */
