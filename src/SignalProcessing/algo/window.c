/*
 * These modules perform data windowing. 
 *    Edward Daw, 10th June 1998
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "gdsconst.h"
#include "gdstype.h"
#include "gdssigproc.h"
#include "window.h"

void
winCoeffGen( int timeseries_length, int window, double* win_array )
{
    double weightnorm = 0; /*the sum of the squares of the window coeffts.*/
    double swn; /*the square root of weightnorm*/
    int    i = 0;

    switch ( window )
    {
    case WINDOW_HANNING:
        for ( i = 0; i < timeseries_length; ++i )
        {
            win_array[ i ] =
                0.5 * ( 1 - cos( 2 * PI * (double)i / timeseries_length ) );
            weightnorm += win_array[ i ] * win_array[ i ];
        }
        break;
    case WINDOW_WELCH:
        for ( i = 0; i < timeseries_length; ++i )
        {
            win_array[ i ] = 1.0 -
                pow( ( ( (double)i - ( timeseries_length / 2 ) ) /
                       ( timeseries_length / 2 ) ),
                     2 );
            weightnorm += win_array[ i ] * win_array[ i ];
        }
        break;
    case WINDOW_BARTLETT:
        for ( i = 0; i < timeseries_length; ++i )
        {
            win_array[ i ] = 1.0 -
                fabs( ( (double)i - ( timeseries_length / 2 ) ) /
                      ( timeseries_length / 2 ) );
            weightnorm += win_array[ i ] * win_array[ i ];
        }
        break;
    case WINDOW_UNIFORM:
        for ( i = 0; i < timeseries_length; ++i )
        {
            win_array[ i ] = 1.0;
            weightnorm += 1;
        }
        break;
    case WINDOW_FLATTOP:
        for ( i = 0; i < timeseries_length; ++i )
        {
            win_array[ i ] =
                ( 1.0 - 1.93 * cos( 2 * PI * (double)i / timeseries_length ) +
                  1.29 * cos( 4 * PI * (double)i / timeseries_length ) -
                  0.388 * cos( 6 * PI * (double)i / timeseries_length ) +
                  0.028 * cos( 8 * PI * (double)i / timeseries_length ) );
            weightnorm += win_array[ i ] * win_array[ i ];
        }
        break;
    case WINDOW_BMH:
        for ( i = 0; i < timeseries_length; ++i )
        {
            win_array[ i ] =
                ( 1.0 -
                  1.36109 * cos( 2 * PI * (double)i / timeseries_length ) +
                  0.39381 * cos( 4 * PI * (double)i / timeseries_length ) -
                  0.032557 * cos( 6 * PI * (double)i / timeseries_length ) );
            weightnorm += win_array[ i ] * win_array[ i ];
        }
        break;
    case WINDOW_HAMMING:
        for ( i = 0; i < timeseries_length; ++i )
        {
            win_array[ i ] =
                ( 0.54 - 0.46 * cos( 2 * PI * (double)i / timeseries_length ) );
            weightnorm += win_array[ i ] * win_array[ i ];
        }
        break;
    default: /*Uniform*/
        for ( i = 0; i < timeseries_length; ++i )
        {
            win_array[ i ] = 1;
            weightnorm += 1;
        }
    }

    /*normalize the windowed data to the sum of the window coefficients squared*/

    swn = sqrt( weightnorm / timeseries_length );
    for ( i = 0; i < timeseries_length; ++i )
        win_array[ i ] /= swn;
}

int
windowData( int           flag,
            int           timeseries_length,
            int           data_type,
            const double* win_array,
            const double* data,
            double*       wdata )
{
    int i;

    switch ( data_type )
    {
    case DATA_REAL: {
        /* get mean */
        double dc = 0.0;
        if ( ( flag & WINDOW_REMOVE_DC ) != 0 )
        {
            dc = dMean( data, timeseries_length );
        }
        /* window and dc removal */
        for ( i = 0; i < timeseries_length; ++i )
        {
            wdata[ i ] = ( data[ i ] - dc ) * win_array[ i ];
        }
        break;
    }
    case DATA_COMPLEX: {
        /* get mean */
        dCmplx dc;
        dc.re = 0.0;
        dc.im = 0.0;
        if ( ( flag & WINDOW_REMOVE_DC ) != 0 )
        {
            dc = zMean( (dCmplx*)data, timeseries_length );
        }
        /* window and dc removal */
        for ( i = 0; i < timeseries_length; ++i )
        {
            ( (dCmplx*)wdata + i )->re =
                ( ( ( (dCmplx*)data + i )->re - dc.re ) * win_array[ i ] );
            ( (dCmplx*)wdata + i )->im =
                ( ( ( (dCmplx*)data + i )->im - dc.im ) * win_array[ i ] );
        }
        break;
    }
    default:
        return -2;
    }
    return 0;
}

int
swindowData( int           flag,
             int           timeseries_length,
             int           data_type,
             const double* win_array,
             const float*  data,
             double*       wdata )
{
    int i;

    switch ( data_type )
    {
    case DATA_REAL: {
        /* get mean */
        double dc = 0.0;
        if ( ( flag & WINDOW_REMOVE_DC ) != 0 )
        {
            dc = sMean( data, timeseries_length );
        }
        /* window and dc removal */
        for ( i = 0; i < timeseries_length; ++i )
        {
            wdata[ i ] = ( data[ i ] - dc ) * win_array[ i ];
        }
        break;
    }
    case DATA_COMPLEX: {
        /* get mean */
        dCmplx dc;
        dc.re = 0.0;
        dc.im = 0.0;
        if ( ( flag & WINDOW_REMOVE_DC ) != 0 )
        {
            dc = szMean( (sCmplx*)data, timeseries_length );
        }
        /* window and dc removal */
        for ( i = 0; i < timeseries_length; ++i )
        {
            ( (dCmplx*)wdata + i )->re =
                ( ( ( (sCmplx*)data + i )->re - dc.re ) * win_array[ i ] );
            ( (dCmplx*)wdata + i )->im =
                ( ( ( (sCmplx*)data + i )->im - dc.im ) * win_array[ i ] );
        }
        break;
    }
    default:
        return -2;
    }
    return 0;
}
