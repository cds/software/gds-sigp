/* -*- mode: c++; c-basic-offset: 3; -*- */
/*
 * These modules perform ffts on real or complex input data. The output
 * is either a power spectrum or a complex fourier transform. Various
 * output formats are supported. Data must be a power of 2 in length
 * or an error is returned. Uses the fftw fourier transform algorithms
 * available free from http://theory.lcs.mit.edu/~fftw/. This code
 * is preliminary, report all bugs to edaw@ligo.mit.edu. Documentation
 * is on this web site under current research - gds - fft.
 *
 *    Edward Daw, 10th June 1998
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "gdsconst.h"
#include "window.h"
#include "fftmodule.h"

#ifdef FFTW3
typedef struct complex_re_im
{
    double re;
    double im;
} complex_re_im;

typedef struct complex_re_im_f
{
    float re;
    float im;
} complex_re_im_f;

#else
typedef fftw_complex complex_re_im;
#endif

/*
 * These static variables are used by the function powerspectrum(), not
 * the function psgen().
 */

/*
 * This power spectrum function does not make use of static variables to store
 * parameters and arrays between adjacent calls to the powerspectrum function.
 * Useful for applications where the subroutine may be called by many different
 * applications simultaneously, or applications where ffts are being 
 * generated for data sets of alternating or varying sizes.
 * 
 */

int
psGen( int          p_mode,
       fftparam*    pset,
       int          timeseries_length,
       int          data_type,
       const double* data,
       double        timestep,
       int          output_format,
       int          window,
       double*      result )
{
    int fft_error;

    /*initialization stuff if power spectrum mode is PS_INIT*/
    if ( ( ( p_mode & PS_USE_MASK ) == PS_INIT_PLAN ) ||
         ( ( p_mode & PS_USE_MASK ) == PS_INIT_ALL ) ||
         ( ( p_mode & PS_USE_MASK ) == PS_INIT_ALL_AND_TAKE_FFT ) )
    {
        /*check that timeseries_length is a power of two*/
        if ( isitapoweroftwo( timeseries_length ) != +1 )
            return -1;

        /*create plan for ffts of length timeseries_length*/
        switch ( data_type )
        {
        case DATA_REAL:
#ifdef FFTW3
            pset->real_plan = fftw_plan_dft_r2c_1d( timeseries_length,
                                                    (double*)data,
                                                    (fftw_complex*)result,
                                                    FFTW_MEASURE );
#else
            pset->real_plan = malloc( sizeof( rfftw_plan ) );
            *( pset->real_plan ) = rfftw_create_plan(
                timeseries_length,
                FFTW_REAL_TO_COMPLEX,
                ( FFTW_MEASURE | FFTW_USE_WISDOM | FFTW_THREADSAFE ) );
#endif
            break;
        case DATA_COMPLEX:
#ifdef FFTW3
            pset->complex_plan = fftw_plan_dft_1d( timeseries_length,
                                                   (fftw_complex*)data,
                                                   (fftw_complex*)result,
                                                   FFTW_FORWARD,
                                                   FFTW_MEASURE );
#else
            pset->complex_plan = malloc( sizeof( fftw_plan ) );
            *( pset->complex_plan ) = fftw_create_plan(
                timeseries_length,
                FFTW_FORWARD,
                ( FFTW_MEASURE | FFTW_USE_WISDOM | FFTW_THREADSAFE ) );
#endif
            break;
        }
        if ( ( p_mode & PS_USE_MASK ) == PS_INIT_PLAN )
        {
            return 0;
        }
    }

    /*create a window array. The argument window_data of psgen() should
    have sufficient memory allocated to it to hold timeseries_length
    doubles. Only do this step if p_mode requires generation of
    window coefficients*/

    if ( ( ( p_mode & PS_USE_MASK ) == PS_INIT_WIN ) ||
         ( ( p_mode & PS_USE_MASK ) == PS_INIT_ALL ) ||
         ( ( p_mode & PS_USE_MASK ) == PS_INIT_ALL_AND_TAKE_FFT ) )
    {
        winCoeffGen( timeseries_length, window, pset->window_coeff );
    }

    if ( ( ( p_mode & PS_USE_MASK ) == PS_INIT_PLAN ) ||
         ( ( p_mode & PS_USE_MASK ) == PS_INIT_WIN ) ||
         ( ( p_mode & PS_USE_MASK ) == PS_INIT_ALL ) )
    {
        return 0;
    }

    /*if p_mode PS_CLEAN_PLAN, destroy the existing plan then immediately
    return 0.*/

    if ( ( p_mode & PS_USE_MASK ) == PS_CLEAN_PLAN )
    {
        switch ( data_type )
        {
        case DATA_REAL:
#ifdef FFTW3
            fftw_destroy_plan( pset->real_plan );
#else
            rfftw_destroy_plan( *( pset->real_plan ) );
            free( pset->real_plan );
            pset->real_plan = 0;
#endif
            break;
        case DATA_COMPLEX:
#ifdef FFTW3
            fftw_destroy_plan( pset->complex_plan );
#else
            fftw_destroy_plan( *( pset->complex_plan ) );
            free( pset->complex_plan );
            pset->complex_plan = 0;
#endif
            break;
        default:
            return -1;
        }
        return 0;
    }

    /*if you haven't quit out by now, p_mode must be
    PS_TAKE_FFT, PS_INIT_ALL_AND_TAKE_FFT, in which
    case window the data and take an fft of it.*/

    /*window the input data. The array wdata must be allocated memory
    by the calling program*/

    windowData( ( ( p_mode & PS_REMOVE_DC ) != 0 ) ? WINDOW_REMOVE_DC : 0,
                 timeseries_length,
                 data_type,
                 pset->window_coeff,
                 data,
                 pset->windowed_data );

    /*do the fft*/
    switch ( data_type )
    {
    case DATA_REAL:
        /*real fft*/
#ifdef FFTW3
        fftw_execute_dft_r2c(
            pset->real_plan, pset->windowed_data, (fftw_complex*)result );
#else
        rfftw_one( *( pset->real_plan ), pset->windowed_data, (double*)result );
#endif
        break;
    case DATA_COMPLEX:
        /*complex fft*/
#ifdef FFTW3
        fftw_execute_dft( pset->complex_plan,
                          (fftw_complex*)pset->windowed_data,
                          (fftw_complex*)result );
#else
        fftw_one( *( pset->complex_plan ),
                  (fftw_complex*)( pset->windowed_data ),
                  (fftw_complex*)result );
#endif
        break;
    default:
        /*returns error if timeseries type is neither real nor complex.*/
        return -2;
    }

    /*pack the data for output*/
    if ( ( fft_error = psDataPack( output_format,
                                   data_type,
                                   timeseries_length,
                                   timestep,
                                   result ) ) != 0 )
    {
        return fft_error;
    }

    return 0;
}

int
psDataPack( int     output_format,
            int     data_type,
            int     timeseries_length,
            float   timestep,
            double* result )
{
    int            i;
    double         psnorm;
    complex_re_im* cbuf;

    int            n2 = timeseries_length / 2;
    double*        rdata = result;
    complex_re_im* rdatac = (complex_re_im*)result;
    switch ( output_format )
    {
    case OUTPUT_AMPPERRTBIN:
        switch ( data_type )
        {
        case DATA_REAL:
            /*  2 for addition of -ve frequencies, 1/ROOTTWO for rms */
            psnorm = ROOT_TWO * ( 2 / (double)timeseries_length );
#ifdef FFTW3
            /* fftw3 returns a single vector of n/2 + 1 complex numbers
		   */
            for ( i = 0; i < n2; ++i )
            {
                double xre, xim;
                int    i2 = 2 * i;
                xre = rdata[ i2 ];
                xim = rdata[ i2 + 1 ];
                rdata[ i ] = ( psnorm * sqrt( xre * xre + xim * xim ) );
            }
#else
            /*  note that this untangles the fftw2 half-complex format,
		   *  i.e. {r0, r1, ,,, r(n/2), i(n/2+1), .. i2, i1}
		   */
            rdata[ 0 ] = (float)( psnorm * fabs( rdata[ 0 ] ) );
            for ( i = 1; i < n2; ++i )
            {
                double xre, xim;
                xre = rdata[ i ];
                xim = rdata[ timeseries_length - i ];
                rdata[ i ] = (float)( psnorm * sqrt( xre * xre + xim * xim ) );
            }
#endif
            break;
        case DATA_COMPLEX:
            psnorm = ( 1 / ROOT_TWO ) * ( 2 / (double)timeseries_length );
            rdata[ 0 ] =
                ( psnorm *
                  sqrt( rdatac->re * rdatac->re + rdatac->im * rdatac->im ) );
            for ( i = 1; i < ( timeseries_length / 2 ); ++i )
            {
                double xre, xim, sum;
                xre = rdatac[ i ].re;
                xim = rdatac[ i ].im;
                sum = sqrt( xre * xre + xim * xim );
                xre = rdatac[ timeseries_length - i ].re;
                xim = rdatac[ timeseries_length - i ].im;
                sum += sqrt( xre * xre + xim * xim );
                rdata[ i ] = ( psnorm * sum );
            }
            break;
        default:
            return -2;
        }
        break;
    case OUTPUT_PSD:
        switch ( data_type )
        {
        case DATA_REAL:
            psnorm = ROOT_TWO * sqrt( timestep * timeseries_length ) *
                ( 2.0 / (double)timeseries_length );
#ifdef FFTW3
            /*fftw returns a single vector of n/2 + 1 complex numbers
		   */
            for ( i = 0; i < n2; ++i )
            {
                double xre, xim;
                int    i2 = 2 * i;
                xre = rdata[ i2 ];
                xim = rdata[ i2 + 1 ];
                rdata[ i ] = ( psnorm * sqrt( xre * xre + xim * xim ) );
            }

#else
            /*  note that this untangles the fftw2 half-complex format,
		   *  i.e. {r0, r1, ,,, r(n/2), i(n/2+1), .. i2, i1}
		   */
            rdata[ 0 ] = (float)( psnorm * fabs( rdata[ 0 ] ) );
            for ( i = 1; i < n2; ++i )
            {
                double xre, xim;
                xre = rdata[ i ];
                xim = rdata[ timeseries_length - i ];
                rdata[ i ] = (float)( psnorm * sqrt( xre * xre + xim * xim ) );
            }
#endif
            break;
        case DATA_COMPLEX:
            psnorm = ( 1 / ROOT_TWO ) * sqrt( timestep * timeseries_length ) *
                ( 2 / (float)timeseries_length );
            rdata[ 0 ] =
                ( psnorm *
                  sqrt( rdatac->re * rdatac->re + rdatac->im * rdatac->im ) );
            for ( i = 1; i < ( timeseries_length / 2 ); ++i )
            {
                double xre, xim, sum;
                xre = rdatac[ i ].re;
                xim = rdatac[ i ].im;
                sum = sqrt( xre * xre + xim * xim );
                xre = rdatac[ timeseries_length - i ].re;
                xim = rdatac[ timeseries_length - i ].im;
                sum += sqrt( xre * xre + xim * xim );
                rdata[ i ] = ( psnorm * sum );
            }
            break;
        }
        break;
    case OUTPUT_COMPMINUSFTOF: {
        switch ( data_type )
        {
        case DATA_REAL:
            /*normalize as power spectral density*/
            psnorm = ( 1 / ROOT_TWO ) * sqrt( timestep * timeseries_length ) *
                ( 2 / (double)timeseries_length );

            /*allocate arrays pointing to real output of rfftw, complex output*/
            /*once -ve frequency components have been inserted*/
            cbuf = calloc( timeseries_length, sizeof( fftw_complex ) );

            /*zero frequency component is in bin timeseries_length/2 - 1*/
            ( cbuf + timeseries_length / 2 - 1 )->re = ( psnorm * rdata[ 0 ] );
            ( cbuf + timeseries_length / 2 - 1 )->im = 0;

            /*nyquist frequency component is in bin timeseries_length - 1*/
            ( cbuf + timeseries_length - 1 )->re =
                ( psnorm * rdata[ timeseries_length / 2 ] );
            ( cbuf + timeseries_length - 1 )->im = 0;

            /*fill in rest of bins using symmetry of positive and negative
                  components*/
            for ( i = 1; i < timeseries_length / 2; ++i )
            {
                ( cbuf + timeseries_length / 2 - 1 + i )->re =
                    ( psnorm * rdata[ i ] );
                ( cbuf + timeseries_length / 2 - 1 - i )->re =
                    ( psnorm * rdata[ i ] );
                ( cbuf + timeseries_length / 2 - 1 + i )->im =
                    ( psnorm * rdata[ timeseries_length - i ] );
                ( cbuf + timeseries_length / 2 - 1 - i )->im =
                    ( -psnorm * rdata[ timeseries_length - i ] );
            }
            memcpy( (void*)result,
                    (void*)cbuf,
                    timeseries_length * sizeof( fftw_complex ) );
            free( cbuf );
            break;
        case DATA_COMPLEX:
            /*assign memory to a swap space*/
            cbuf = calloc( timeseries_length / 2, sizeof( fftw_complex ) );

            /*copy complex bins starting at timeseries_length/2+1 
                  and of length timeseries_length/2-1 from results array to swap 
                  space*/
            memcpy( (void*)cbuf,
                    (void*)( rdatac + ( timeseries_length / 2 ) + 1 ),
                    ( ( timeseries_length / 2 ) - 1 ) *
                        sizeof( fftw_complex ) );

            /*copy complex bins starting at 0 and of length timeseries_length/2+1
                  to destination timeseries_length/2-1 within results array*/
            memcpy( (void*)( rdatac + ( timeseries_length / 2 ) - 1 ),
                    (void*)rdatac,
                    ( timeseries_length / 2 + 1 ) * sizeof( fftw_complex ) );

            /*copy from swap space data of length timeseries_length/2-1 to
                  beginning of results_array*/
            memcpy( (void*)rdatac,
                    (void*)cbuf,
                    ( ( timeseries_length / 2 ) - 1 ) *
                        sizeof( fftw_complex ) );

            free( cbuf );

            /*normalize results data as power spectral density*/
            psnorm = ( 1 / ROOT_TWO ) * sqrt( timestep * timeseries_length ) *
                ( 2 / (float)timeseries_length );
            for ( i = 0; i < ( timeseries_length ); ++i )
            {
                ( rdatac + i )->re *= psnorm;
                ( rdatac + i )->im *= psnorm;
            }
            break;
        }
        break;
    case OUTPUT_COMPNATIVE:
        switch ( data_type )
        {
        case DATA_REAL:
            psnorm = ( 1 / ROOT_TWO ) * sqrt( timestep * timeseries_length ) *
                ( 2 / (double)timeseries_length );
            for ( i = 0; i < ( timeseries_length ); ++i )
            {
                rdata[ i ] *= psnorm;
            }
            break;
        case DATA_COMPLEX:
            psnorm = ( 1 / ROOT_TWO ) * sqrt( timestep * timeseries_length ) *
                ( 2 / (double)timeseries_length );
            for ( i = 0; i < ( timeseries_length ); ++i )
            {
                ( rdatac + i )->re *= psnorm;
                ( rdatac + i )->im *= psnorm;
            }
            break;
        }
        break;
    }
    case OUTPUT_GDSFORMAT: {
        switch ( data_type )
        {
        case DATA_REAL: {
            psnorm = sqrt( timestep / (double)timeseries_length );
#ifdef FFTW3
            /* Normalize fftw3 data */
            for ( i = 0; i < n2; ++i )
            {
                rdatac[ i ].re *= psnorm;
                rdatac[ i ].im *= psnorm;
            }
#else
            /* Untangle and normalize fftw2 data */
            cbuf = calloc( n2, sizeof( fftw_complex ) );
            if ( cbuf == NULL )
            {
                return -5;
            }
            cbuf[ 0 ].re = (float)( psnorm * rdata[ 0 ] );
            cbuf[ 0 ].im = 0;
            for ( i = 1; i < n2; ++i )
            {
                cbuf[ i ].re = (float)( psnorm * rdata[ i ] );
                cbuf[ i ].im =
                    (float)( psnorm * rdata[ timeseries_length - i ] );
            }
            memcpy( result, cbuf, timeseries_length * sizeof( float ) );
            free( cbuf );
#endif
            break;
        }
        case DATA_COMPLEX: {
            psnorm = sqrt( timestep / (double)timeseries_length );
            for ( i = 0; i < timeseries_length; ++i )
            {
                rdatac[ i ].re *= psnorm;
                rdatac[ i ].im *= psnorm;
            }
            break;
        }
        default:
            return -3;
        }
        break;
    }
    default:
        return -3;
    }

    return 0;
}

int
isitapoweroftwo( int val )
{
    /* Original code
      double twomant;
      int twoexp;
   
      twomant = frexp((double)val, &twoexp);
      if(twomant != 0.5)
         return -1;
      else
         return +1;
     */
    if ( ( val & ( val - 1 ) ) == 0 )
        return 1;
    return -1;
}

int
crossSpect( int          data_length,
            const float* fdata_x,
            const float* fdata_y,
            float*       cs_result )
{
    int   i;
    float re1;
    float im1;
    float re2;
    float im2;

    for ( i = 0; i < data_length; ++i )
    {
        re1 = fdata_x[ 2 * i ];
        im1 = fdata_x[ 2 * i + 1 ];
        re2 = fdata_y[ 2 * i ];
        im2 = fdata_y[ 2 * i + 1 ];
        cs_result[ 2 * i ] = re1 * re2 + im1 * im2;
        cs_result[ 2 * i + 1 ] = im1 * re2 - re1 * im2;
    }

    return 0;
}

int
crossPower( int           data_length,
            int           data_type,
            const double* fdata_x,
            const double* fdata_y,
            double*       cs_result )
{
    int    i;
    double re1;
    double im1;
    double re2;
    double im2;

    switch ( data_type )
    {
    case DATA_REAL:
        cs_result[ 0 ] = fdata_x[ 0 ] * fdata_y[ 0 ];
        cs_result[ 1 ] = 0;
        break;
    case DATA_COMPLEX:
        re1 = fdata_x[ 0 ];
        im1 = fdata_x[ 1 ];
        re2 = fdata_y[ 0 ];
        im2 = fdata_y[ 1 ];
        cs_result[ 0 ] = 2. * ( re1 * re2 + im1 * im2 );
        cs_result[ 1 ] = 2. * ( im1 * re2 - re1 * im2 );
        break;
    default:
        return -1;
    }
    for ( i = 1; i < data_length; ++i )
    {
        re1 = fdata_x[ 2 * i ];
        im1 = fdata_x[ 2 * i + 1 ];
        re2 = fdata_y[ 2 * i ];
        im2 = fdata_y[ 2 * i + 1 ];
        cs_result[ 2 * i ] = 2. * ( re1 * re2 + im1 * im2 );
        cs_result[ 2 * i + 1 ] = 2. * ( im1 * re2 - re1 * im2 );
    }

    return 0;
}

int
fftToPs( int data_length, int data_type, const double* input, double* output )
{
    int    i;
    double norm = 0;
    int    resultlength = 0;

    switch ( data_type )
    {
    case DATA_REAL:
        norm = ROOT_TWO;
        resultlength = data_length;
        break;
    case DATA_COMPLEX:
        norm = ROOT_TWO;
        resultlength = data_length;
        break;
    default:
        return -1;
    }
    if ( data_type == DATA_REAL )
    {
        output[ 0 ] = fabs( input[ 0 ] );
    }
    if ( data_type == DATA_COMPLEX )
    {
        output[ 0 ] =
            ( norm *
              sqrt( input[ 0 ] * input[ 0 ] + input[ 1 ] * input[ 1 ] ) );
    }
    for ( i = 1; i < resultlength; ++i )
    {
        double xre, xim;
        xre = input[ 2 * i ];
        xim = input[ 2 * i + 1 ];
        output[ i ] = ( norm * sqrt( xre * xre + xim * xim ) );
    }
    return 0;
}

int
dataRotator( int           data_length,
             int           data_type,
             const double* input,
             double*       output )
{
    switch ( data_type )
    {
    case DATA_REAL:
        memcpy( (void*)output,
                (void*)( input + data_length / 2 ),
                ( data_length / 2 ) * sizeof( double ) );
        memcpy( (void*)( output + data_length / 2 ),
                (void*)input,
                ( data_length / 2 ) * sizeof( double ) );
        break;
    case DATA_COMPLEX:
        memcpy( (void*)output,
                (void*)( input + data_length ),
                ( data_length ) * sizeof( double ) );
        memcpy( (void*)( output + data_length ),
                (void*)input,
                ( data_length ) * sizeof( double ) );
        break;
    }
    return 0;
}

int
coherence( int          data_length,
           int          data_type,
           const float* pxxdata,
           const float* pyydata,
           const float* pxydata,
           float*       result )
{
    int    i; /* index */
    double denom; /* denominator */

    switch ( data_type )
    {
    case DATA_REAL: {
        /* point at f=0 */
        denom = pxxdata[ 0 ] * pxxdata[ 0 ] * pyydata[ 0 ] * pyydata[ 0 ];
        if ( denom > 0 )
        {
            result[ 0 ] = (float)( ( pxydata[ 0 ] * pxydata[ 0 ] +
                                     pxydata[ 1 ] * pxydata[ 1 ] ) /
                                   denom );
        }
        else
        {
            result[ 0 ] = 0.0;
        }
        /* rest of the points */
        for ( i = 1; i < data_length; i++ )
        {
            denom = pxxdata[ i ] * pxxdata[ i ] * pyydata[ i ] * pyydata[ i ];
            if ( denom > 0 )
            {
                result[ i ] =
                    (float)( 4.0 *
                             ( pxydata[ 2 * i ] * pxydata[ 2 * i ] +
                               pxydata[ 2 * i + 1 ] * pxydata[ 2 * i + 1 ] ) /
                             denom );
            }
            else
            {
                result[ i ] = 0.0;
            }
        }
        break;
    }
    case DATA_COMPLEX: {
        for ( i = 0; i < data_length; i++ )
        {
            denom = pxxdata[ i ] * pxxdata[ i ] * pyydata[ i ] * pyydata[ i ];
            if ( denom > 0 )
            {
                result[ i ] =
                    (float)( 4.0 *
                             ( pxydata[ 2 * i ] * pxydata[ 2 * i ] +
                               pxydata[ 2 * i + 1 ] * pxydata[ 2 * i + 1 ] ) /
                             denom );
            }
            else
            {
                result[ i ] = 0.0;
            }
        }
        break;
    }
    default: {
        return -1;
    }
    }
    return 0;
}

int
coherenceCP( int           data_length,
             const double* pxxdata,
             const double* pyydata,
             const double* pxydata,
             double*       result )
{
    int    i; /* index */
    double denom; /* denominator */

    for ( i = 0; i < data_length; i++ )
    {
        denom = pxxdata[ i ] * pxxdata[ i ] * pyydata[ i ] * pyydata[ i ];
        if ( denom > 0 )
        {
            result[ i ] =
                (float)( ( pxydata[ 2 * i ] * pxydata[ 2 * i ] +
                           pxydata[ 2 * i + 1 ] * pxydata[ 2 * i + 1 ] ) /
                         denom );
        }
        else
        {
            result[ i ] = 0.0;
        }
    }
    return 0;
}

int
transfer_function( int           data_length,
                   const double* csd_in,
                   const double* pxx_amp_data_in,
                   float*        tf_out )
{

    complex_re_im*   csd = (complex_re_im*)csd_in;
    complex_re_im_f* tf = (complex_re_im_f*)tf_out;

    for ( int i = 0; i < data_length; ++i )
    {
        double denom = pxx_amp_data_in[ i ];
        denom *= denom;
        if ( denom != 0 )
        {
            tf[ i ].re = (float)( csd[ i ].re / denom );
            tf[ i ].im = (float)( csd[ i ].im / denom );
        }
        else
        {
            tf[ i ].re = 0.0f;
            tf[ i ].im = 0.0f;
        }
    }

    return 0;
}