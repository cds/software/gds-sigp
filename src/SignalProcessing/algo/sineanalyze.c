
/****************************************************************
 *								*
 *  Module Name: Sine wave complex amplitude detection	       	*
 *								*
 *  Procedure Description: Calculates amplitude & phase of a    *
 *      sine wave from a time series, used for swept-sine       *
 *      analysis.					        *
 *								*	
 *  External Procedure Name: sineAnalyze			*
 *								*
 *  Procedure Arguments: flag for offset removal option;	*
 *	pointer to input vector; number of input points;  	*
 *	sampling rate; sine wave frequency; pointer to  	*
 *	result - complex amplitude of sine wave          	*
 *								*
 *								*
 *								*
 ****************************************************************/

#include <math.h>
#include <stdlib.h>
#include "gdsconst.h"
#include "gdstype.h"
#include "sineanalyze.h"
#include "window.h"
#include "gdssigproc.h"

static const double lead_coeff[ N_LEAD ] = {
    4.024070e-5, -2.394330e-4, 2.310359e-3, 8.793926e-3, 2.067860e-2,
    4.296640e-2, 7.783272e-2,  1.268987e-1, 1.905449e-1, 2.677029e-1,
    3.558301e-1, 4.511144e-1,  5.488856e-1, 6.441699e-1, 7.322971e-1,
    8.094551e-1, 8.731013e-1,  9.221673e-1, 9.570336e-1, 9.793214e-1,
    9.912061e-1, 9.976896e-1,  1.000239,    9.999598e-1
};

static const double trail_pwr_coeff[ N_LEAD + 1 ][ 7 ] = {
    { 9.999598e-1,
      0.0,
      1.316968e-4,
      -7.316491e-5,
      -5.487368e-5,
      4.389895e-5,
      -7.316491e-6 },
    { 1.000239,
      0.0,
      -1.118827e-3,
      1.060560e-3,
      -1.374325e-4,
      -1.095488e-4,
      2.557462e-5 },
    { 9.976896e-1,
      5.267874e-3,
      -2.441319e-3,
      -6.659546e-4,
      2.919819e-4,
      1.385624e-4,
      -4.135187e-5 },
    { 9.912061e-1,
      7.925644e-3,
      -2.162353e-3,
      1.192605e-3,
      -4.107389e-4,
      -1.016219e-4,
      4.003073e-5 },
    { 9.793214e-1,
      1.672283e-2,
      -5.491618e-3,
      4.096692e-4,
      2.410381e-4,
      2.362852e-5,
      -2.087507e-5 },
    { 9.570336e-1,
      2.821431e-2,
      -6.281264e-3,
      3.534452e-4,
      3.758990e-7,
      -3.606196e-6,
      4.539119e-6 },
    { 9.221673e-1,
      4.179041e-2,
      -7.150691e-3,
      1.781502e-4,
      5.128335e-5,
      -2.672663e-6,
      -1.555889e-7 },
    { 8.731013e-1,
      5.640868e-2,
      -7.351238e-3,
      -5.160398e-5,
      6.155837e-5,
      -1.135560e-6,
      -2.561838e-7 },
    { 8.094551e-1,
      7.070597e-2,
      -6.818525e-3,
      -3.046459e-4,
      6.296106e-5,
      7.474227e-7,
      -3.138304e-7 },
    { 7.322971e-1,
      8.318285e-2,
      -5.539011e-3,
      -5.427347e-4,
      5.451991e-5,
      2.627674e-6,
      -3.133752e-7 },
    { 6.441699e-1,
      9.242961e-2,
      -3.615770e-3,
      -7.276560e-4,
      3.714149e-5,
      4.139464e-6,
      -2.519651e-7 },
    { 5.488856e-1,
      9.735183e-2,
      -1.257139e-3,
      -8.286703e-4,
      1.350299e-5,
      4.980618e-6,
      -1.401923e-7 },
    { 4.511144e-1,
      9.735183e-2,
      1.255457e-3,
      -8.286703e-4,
      -1.245154e-5,
      4.980618e-6,
      0.e-20 },
    { 3.558301e-1,
      9.242961e-2,
      3.614429e-3,
      -7.276560e-4,
      -3.630319e-5,
      4.139464e-6,
      1.401923e-7 },
    { 2.677029e-1,
      8.318285e-2,
      5.538274e-3,
      -5.427347e-4,
      -5.405933e-5,
      2.627674e-6,
      2.519651e-7 },
    { 1.905449e-1,
      7.070597e-2,
      6.818520e-3,
      -3.046459e-4,
      -6.295765e-5,
      7.474227e-7,
      3.133752e-7 },
    { 1.268987e-1,
      5.640868e-2,
      7.351930e-3,
      -5.160398e-5,
      -6.199072e-5,
      -1.135560e-6,
      3.138304e-7 },
    { 7.783272e-2,
      4.179041e-2,
      7.151898e-3,
      1.781502e-4,
      -5.203781e-5,
      -2.672663e-6,
      2.561838e-7 },
    { 4.296640e-2,
      2.821431e-2,
      6.337600e-3,
      3.534452e-4,
      -3.558621e-5,
      -3.606196e-6,
      1.555889e-7 },
    { 2.067860e-2,
      1.672283e-2,
      5.186648e-3,
      4.096692e-4,
      -5.043170e-5,
      2.362852e-5,
      -4.539119e-6 },
    { 8.793926e-3,
      7.925644e-3,
      2.893223e-3,
      1.192605e-3,
      -4.605463e-5,
      -1.016219e-4,
      2.087507e-5 },
    { 2.310359e-3,
      5.267874e-3,
      1.464728e-3,
      -6.659546e-4,
      3.183875e-4,
      1.385624e-4,
      -4.003073e-5 },
    { -2.394330e-4,
      0.,
      1.921945e-3,
      1.060560e-3,
      -3.645162e-4,
      -1.095488e-4,
      4.135187e-5 },
    { 4.024070e-5,
      0.,
      -5.263901e-4,
      -7.316491e-5,
      3.015570e-4,
      4.389895e-5,
      -2.557462e-5 },
    { 0.0, 0.0, 8.779789e-5, 0.0, -5.487368e-5, 0., 7.316491e-6 }
};

int
sineAnalyze( int         flag,
             int         window,
             const float x[],
             int         npt,
             double      fs,
             double      fsine,
             int         nAvg,
             double      toff,
             dCmplx*     sineAmp,
             dCmplx      sineVals[] )
{
    int          i; /* data index */
    int          j; /* average index */
    int          nptMeas; /* # of points/avrg */
    int          nptInt; /* pts used for integral */
    float*       data; /* tmp. data arary */
    const float* dataPtr; /* ptr to data */
    double*      yre; /* re of down-converted */
    double*      yim; /* im of down-converted */
    double*      temp_yre; /* ptr to re down-conv. */
    double*      temp_yim; /* ptr to im down-conv. */
    double*      win; /* window array */
    double       wnorm; /* window normalization */
    double       trail_coeff[ N_LEAD + 1 ]; /* trail coeff. */
    double       avgx; /* mean of data */
    double       lead_re; /* lead term sum (re) */
    double       lead_im; /* lead term sum (im) */
    double       mid_re; /* mid term sum (re) */
    double       mid_im; /* mid term sum (im) */
    double       trail_re; /* trail term sum (re) */
    double       trail_im; /* trail term sum (im) */
    double       fraction; /* fraction  of last sample */
    double       delta; /* interpolation delta */
    double       nsampInteg; /* data normalization */
    double       ncycle; /* number of cycles */

    if ( npt <= N_LEAD )
    {
        return -1;
    }
    /* remove dc term if required */
    switch ( flag )
    {
    /* no offset removal, do nothing */
    case ( 0 ): {
        dataPtr = x;
        data = NULL;
        break;
    }
    /* remove the mean from the data */
    case ( 1 ): {
        avgx = sMean( x, npt );
        data = malloc( npt * sizeof( float ) );
        if ( data == NULL )
        {
            return -1;
        }
        for ( i = 0; i < npt; ++i )
            data[ i ] = x[ i ] - avgx;
        dataPtr = data;
        break;
    }
    /* illegal argument */
    default: {
        return -1;
    }
    }

    /* down-convert data */
    yre = malloc( 2 * npt * sizeof( double ) );
    if ( yre == NULL )
    {
        free( data );
        return -1;
    }
    yim = yre + npt;
    sdMixdown( 0, dataPtr, NULL, yre, yim, npt, toff, 1 / fs, fsine );

    /* nptMeas: no. of points per measurement		      */
    /* nptInt: no. of points included in the integration    */
    /* delta: the fraction of a sampling period covered     */
    /*        by the last integration span.                 */
    /* nsampInteg: the number of samples (including         */
    /*        fractional part) spanning the integration     */
    /* 	(an integral multiple, ncycle, of the sine    */
    /* 	period); normalization factor for the result  */

    fsine = fabs( fsine );
    if ( fsine == 0 )
    { /* cover f=0, DS */
        nptMeas = ( npt - 1 - N_LEAD ) / nAvg + N_LEAD;
        nptInt = nptMeas;
        if ( nptInt < 2 * N_LEAD )
        {
            return -2;
        }
        fraction = 0;
        delta = 1;
        nsampInteg = nptMeas;
    }

    else
    {
        /* nptMeas = npt/nAvg; */
        nptMeas = ( npt - 1 - N_LEAD ) / nAvg + N_LEAD; /* DS */
        modf( ( nptMeas - N_LEAD ) * ( fsine / fs ), &ncycle );
        if ( ncycle < 1 )
        {
            return -2;
        }
        nptInt = sweptSineNpts( ncycle / fsine, fs );
        if ( nptInt < 2 * N_LEAD )
        {
            return -2;
        }
        fraction = modf( ( nptInt - N_LEAD ) * ( fsine / fs ), &ncycle );
        delta = 1 - ( ( fraction ) * ( fs / fsine ) );
        nsampInteg = ncycle * ( fs / fsine );
    }

    /* trailing coeffcients */
    calcTrailcoeff( delta, trail_coeff );
    /*printf ("npt=%i N_LEAD=%i nAvg=%i fsine=%g fs=%g ncycle=%f\n", 
             npt, N_LEAD, nAvg, fsine, fs, ncycle);
      printf ("nptInt=%i nptMeas=%d fraction=%g delta=%g nsampInteg=%g\n", 
             nptInt, nptMeas, fraction, delta, nsampInteg);*/

    /* generate window function if necessary */
    if ( window == WINDOW_UNIFORM )
    {
        win = NULL;
        wnorm = nsampInteg; /* norm of uniform window */
    }
    else
    {
        win = malloc( nptInt * sizeof( double ) );
        if ( win == NULL )
        {
            free( data );
            free( yre );
            return -1;
        }
        winCoeffGen( nptInt, window, win );
        /* calculate the window normalization */
        wnorm = dDotProd( win, lead_coeff, N_LEAD ) +
            dDotProd( win + nptInt - N_LEAD - 1, trail_coeff, N_LEAD + 1 );
        for ( i = N_LEAD; i < ( nptInt - ( N_LEAD + 1 ) ); ++i )
        {
            wnorm += win[ i ];
        }
    }
    /* printf ("windowing done wnorm = %g\n", wnorm);*/
    /* perform integration nAvg times */
    for ( j = 0; j < nAvg; ++j )
    {
        /* printf ("do average %i\n", j); */
        /* temp_yre = yre + (j * nptMeas);
            temp_yim = yim + (j * nptMeas); */
        temp_yre = yre + ( j * ( nptMeas - N_LEAD ) ); /* DS */
        temp_yim = yim + ( j * ( nptMeas - N_LEAD ) ); /* DS */

        /* windowing the time series */
        if ( window != WINDOW_UNIFORM )
        {
            if ( ( windowData( 0, nptInt, DATA_REAL, win, temp_yre, temp_yre ) <
                   0 ) ||
                 ( windowData( 0, nptInt, DATA_REAL, win, temp_yim, temp_yim ) <
                   0 ) )
            {
                free( win );
                free( yre );
                free( data );
                return -1;
            }
        }

        /* leading terms */
        lead_re = dDotProd( temp_yre, lead_coeff, N_LEAD );
        lead_im = dDotProd( temp_yim, lead_coeff, N_LEAD );

        /* mid terms */
        for ( mid_re = mid_im = 0, i = N_LEAD; i < ( nptInt - ( N_LEAD + 1 ) );
              ++i )
        {
            mid_re += temp_yre[ i ];
            mid_im += temp_yim[ i ];
        }

        /* trailing terms */
        trail_re = dDotProd(
            ( temp_yre + nptInt - N_LEAD - 1 ), trail_coeff, N_LEAD + 1 );
        trail_im = dDotProd(
            ( temp_yim + nptInt - N_LEAD - 1 ), trail_coeff, N_LEAD + 1 );

        /* add it all up and normalize; */
        sineVals[ j ].re = ( lead_re + mid_re + trail_re ) / wnorm;
        sineVals[ j ].im = ( lead_im + mid_im + trail_im ) / wnorm;
        /* factor of 2 for peak sine amplitude	*/
        if ( fsine > 0 )
        {
            sineVals[ j ].re *= 2.0;
            sineVals[ j ].im *= 2.0;
        }
        /* printf ("average %i done\n", j);*/
    }

    free( win );
    free( yre );
    free( data );
    if ( nAvg > 1 )
        *sineAmp = zMean( sineVals, nAvg );
    else
    {
        sineAmp->re = sineVals[ 0 ].re;
        sineAmp->im = sineVals[ 0 ].im;
    }
    /* printf ("averaging done\n");*/
    return 0;
}

void
calcTrailcoeff( double delta, double trail_coeff[] )
{
    double pwrs_of_delta[ 7 ] = { 1.0 };
    int    j;

    /* fill in pwrs_of_delta, from 0 to 6	*/
    pwrs_of_delta[ 1 ] = delta;
    for ( j = 2; j < 7; ++j )
    {
        pwrs_of_delta[ j ] = pow( delta, (double)j );
    }
    /* printf ("delta=%g\n", delta);*/
    for ( j = 0; j < ( N_LEAD + 1 ); ++j )
    {
        trail_coeff[ j ] =
            dDotProd( pwrs_of_delta, &trail_pwr_coeff[ j ][ 0 ], 7 );
        /*printf ("trail[%i] = %g\n", j, trail_coeff[j]);*/
    }
}

int
sweptSineNpts( double t_integ, double fs )
{
    int nptMeas;

    nptMeas = (int)( N_LEAD + 1 + floor( t_integ * fs ) );
    return nptMeas; /* no. of points per measurement  */
}

double
ssCoherence( const dCmplx x1[], const dCmplx x2[], int navg )
{
    double coherence;
    dCmplx x2conj;
    dCmplx x12star;
    dCmplx sumz = { 0.0 };
    double x1sq = 0.0;
    double x2sq = 0.0;
    int    i;

    /* the loop computes the factors: num = <x1*conj(x2)>;  */
    /* den1 = <|x1|^2>; den2 = <|x2|^2> (dividing by navg   */
    /* is omitted, since it cancels among the factors)      */
    for ( i = 0; i < navg; ++i )
    {
        x2conj = zConj( x2[ i ] );
        x12star = zMultply( x1[ i ], x2conj );
        sumz = zAdd( sumz, x12star );
        x1sq += x1[ i ].re * x1[ i ].re + x1[ i ].im * x1[ i ].im;
        x2sq += x2[ i ].re * x2[ i ].re + x2[ i ].im * x2[ i ].im;
    }

    /* the coherence is now: |num|^2/(den1*den2)  */
    if ( x1sq * x2sq > 1E-300 )
    {
        coherence = ( sumz.re * sumz.re + sumz.im * sumz.im ) / ( x1sq * x2sq );
    }
    else
    {
        coherence = 0;
    }
    return coherence;
}
