/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: avgmodule			        		*/
/*                                                         		*/
/* Module Description: averageing functions for LIGO data analysis      */
/*                     - performs a vector (average real and imaginary  */
/*                     parts) or scalar (average amplitudes) average,   */
/*		       with exponential or linear weighting.            */
/*                                                                      */
/* Module Arguments: none                				*/
/*									*/
/* Revision History:					   		*/
/* Rel   Date     Programmer   		Comments       	       		*/
/* 0.1   1/11/99  Edward Daw            written for GDS diagnostics     */
/*                                                                      */
/* Documentation References: 						*/
/*	Man Pages: doc++ generated html					*/
/*	References: Numerical Recipes in C,                             */
/*                                                         		*/
/* Author Information:							*/
/*	Name	Telephone    Fax          e-mail 			*/
/*	E. Daw  617-258-7697 617-253-7014 edaw@ligo.mit.edu		*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Unix						*/
/*	Compiler Used: Sun Sparcworks C compiler         		*/
/*	Runtime environment: UNIX    					*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	TBD			*/
/*			  Lint.			TBD			*/
/*			  ANSI			TBD			*/
/*			  POSIX			TBD			*/
/*									*/
/* Known Bugs, Limitations, Caveats:                       		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

/** @name Averaging algorithms 
    @memo Averaging.
    @author Edward Daw, Jan 99 */

/*@{*/

#ifndef _AVGMODULE_H
#define _AVGMODULE_H

#ifdef __cplusplus
extern "C" {
#endif

/** @name Constants and flags for Averaging Routines.

      @memo Constants and Flags
      @author Edward Daw  */

/*@{*/

/**Flag to select a linear average of the amplitude of the input data */
#define AVG_LINEAR_MODULUS 0
/**Flag to select an exponential average of the amplitude of input data */
#define AVG_EXPON_MODULUS 1
/**Flag to select a linear average of the input data vectors */
#define AVG_LINEAR_VECTOR 2
/**Flag to select an exponential average of the input data vectors */
#define AVG_EXPON_VECTOR 3
/**Flag to select a square sum average of the input data vectors */
#define AVG_LINEAR_SQUARE 4
/**Flag to select a square sum average of the input data vectors using
     an exponental weight */
#define AVG_EXPON_SQUARE 5

/** Data structure describing teh averaging */
struct avg_specs
{
    /** average type; see defines */
    int avg_type;
    /** length of data set in points */
    int dataset_length;
    /** data type: DATA_REAL or DATA_COMPLEX */
    int data_type;
    /** number of averages to do */
    int number_of_averages;
};
typedef struct avg_specs avg_specs;

/*@}*/

/**@name Functions for Averaging.

@memo Functions
@author Edward Daw */

/*@{*/

/**
  This function averages real or complex data arrays. Any length
  of array is supported. Memory space for the data arrays must be
  allocated by the calling routine. Input and output data is
  all of type float. For complex input / output, data must be
  arranged as re part, im part, re part, im part.. until the end
  of the dataset is reached. The structure avg_complex can help
  with this. To use the function, the first time you run it, set
  *number_so_far to zero. The structure average_details should contain
  parameters indicating the type of average and the input data format.
  if the output data or the input data is complex, space should be
  allocated for dataset_length * sizeof(avg_complex) at the relevant
  pointer. Below are definitions of the different average types.
  AVG_LINEAR_MODULUS. If there are n real / complex elements in each
                      input array, the output for each element is the
		      linear average of the amplitudes of the input elements.
		      For complex input data, the amplitude is the sum
		      in quadrature of the real and complex parts.
  AVG_EXPON_MODULUS. Same as above, but use an exponential average as defined
                     in 4) below.
  AVG_LINEAR_VECTOR. For complex input data only. Find an n element array
                     of complex numbers. For the real parts, the elements
                     are the linear average of the real parts of the input
                     data. For the imaginary parts, the output elements are
		     the linear average of the imaginary parts of the input
		     data.
  AVG_EXPON_VECTOR. Same as above, but take the exponential average as 
                    defined in 4) below.
  AVG_LINEAR_SQUARE. Calulates the root-mean-square rather than a 
                     straight average. For complex input data calculates
		     the rms of the modulus.
  AVG_EXPON_SQUARE. Same as above, but take the exponential average as 
                    defined in 4) below.

  @param average_details A structure of type avg_specs. It has 4 
                         elements, all integer -
			 1) avg_type , one of AVG_LINEAR_MODULUS
                         AVG_EXPON_MODULUS, AVG_LINEAR_VECTOR and
			 AVG_EXPON_VECTOR.
                         2) dataset_length, the number of elements
                         in the input and output data arrays.
                         3) data_type, DATA_REAL or DATA_COMPLEX.
                         4) number_of_averages. The number used
                         in the formula for exponential averages.
			 If number_of_averages = N, the previously
                         averaged data is dminus, the newly computed
			 set of averaged data is dplus, and the data
			 to be added to the average is dnew,  then the formula
			 for dplus is dplus = ((n-1)dminus + dnew)/n
  @param number_of_sets in AVG_ACQUIRE mode more than one data set can
                        be averaged with existing data. This parameter
                        determines how many sets you are adding in
                        a single call to avg. To use this feature, the
			input_data should consist of the many sets of
			data to be simultaneously averaged placed end-on-end
			in a single floating point array. 
  @param number_so_far The number of data sets added so far. Increments
                       by one each time new data is added to the stack.
  @param input_data A pointer to the input data array
  @param averaged_data A pointer to the array containing the 
                       results of averaging.
  @returns 0 if successful, -ve if error.
  @author Edward Daw, 11th June 1999.
************************************************************/
int avg( const avg_specs* average_details,
         int              number_of_sets,
         const float*     input_data,
         int*             number_so_far,
         float*           averaged_data );

/*@}*/

/**@name Functions for Averaging.

 @memo Functions
 @author Edward Daw */

/**
   This function averages real or complex data arrays. Any length
   of array is supported. Memory space for the data arrays must be
   allocated by the calling routine. Input and output data is
   all of type float. For complex input / output, data must be
   arranged as re part, im part, re part, im part.. until the end
   of the dataset is reached. The structure avg_complex can help
   with this. To use the function, the first time you run it, set
   *number_so_far to zero. The structure average_details should contain
   parameters indicating the type of average and the input data format.
   if the output data or the input data is complex, space should be
   allocated for dataset_length * sizeof(avg_complex) at the relevant
   pointer. Below are definitions of the different average types.
   AVG_LINEAR_MODULUS. If there are n real / complex elements in each
                       input array, the output for each element is the
                       linear average of the amplitudes of the input elements.
                       For complex input data, the amplitude is the sum
                       in quadrature of the real and complex parts.
   AVG_EXPON_MODULUS. Same as above, but use an exponential average as defined
                      in 4) below.
   AVG_LINEAR_VECTOR. For complex input data only. Find an n element array
                      of complex numbers. For the real parts, the elements
                      are the linear average of the real parts of the input
                      data. For the imaginary parts, the output elements are
                      the linear average of the imaginary parts of the input
                      data.
   AVG_EXPON_VECTOR. Same as above, but take the exponential average as
                     defined in 4) below.
   AVG_LINEAR_SQUARE. Calulates the root-mean-square rather than a
                      straight average. For complex input data calculates
                      the rms of the modulus.
   AVG_EXPON_SQUARE. Same as above, but take the exponential average as
                     defined in 4) below.

   @param average_details A structure of type avg_specs. It has 4
                          elements, all integer -
                          1) avg_type , one of AVG_LINEAR_MODULUS
                          AVG_EXPON_MODULUS, AVG_LINEAR_VECTOR and
                          AVG_EXPON_VECTOR.
                          2) dataset_length, the number of elements
                          in the input and output data arrays.
                          3) data_type, DATA_REAL or DATA_COMPLEX.
                          4) number_of_averages. The number used
                          in the formula for exponential averages.
                          If number_of_averages = N, the previously
                          averaged data is dminus, the newly computed
                          set of averaged data is dplus, and the data
                          to be added to the average is dnew,  then the formula
                          for dplus is dplus = ((n-1)dminus + dnew)/n
   @param number_of_sets in AVG_ACQUIRE mode more than one data set can
                         be averaged with existing data. This parameter
                         determines how many sets you are adding in
                         a single call to avg. To use this feature, the
                         input_data should consist of the many sets of
                         data to be simultaneously averaged placed end-on-end
                         in a single floating point array.
   @param number_so_far The number of data sets added so far. Increments
                        by one each time new data is added to the stack.
   @param input_data A pointer to the input data array
   @param averaged_data A pointer to the array containing the
                        results of averaging.
   @returns 0 if successful, -ve if error.
   @author Edward Daw, 11th June 1999.
 ************************************************************/
int davg( const avg_specs* specs,
          int              number_of_sets,
          const double*    input_data,
          int*             number_so_far,
          double*          averaged_data );

/*@}*/

/*@}*/

#ifdef __cplusplus
}
#endif

#endif
