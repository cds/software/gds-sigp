/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: lud.h							*/
/*                                                         		*/
/* Module Description: LU decomposition					*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 28Nov98  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: stdtest.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C++ 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			OK			*/
/*			  POSIX			OK			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS NW17-37				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_LUD_H
#define _GDS_LUD_H

/* Header File List: */
#include <complex>

namespace diag
{

    /** @name LU Decomposition
    These function templates implement the LU decomposition from 
    Numerical Receipies in C. The routines use the vector/matrix
    convention therein.
   
    @memo Lower/upper triangular matrix decomposition.
    @author Written October 1999 by Daniel Sigg
    @see Numerical Receipies in C
    @version 0.1
 ************************************************************************/

    /*@{*/

    /** This function returns a pointer array representing a matrix
    in the format used in Numerical Receipes in C. Careful, the 
    array indices start with 1. When done, the returned pointer
    array has to be freed with nrMatrixFree.

    @memo Matrix type of Numerical Receipes
    @param mat array containing the matrix data
    @param n number of columns
    @param m number of rows
    @return numerical receipes matrix object
    @author DS, Oct 1999
 ************************************************************************/
    template < class T >
    T** nrMatrix( T* mat, int n, int m );

    /** This function frees a Numerical Receipes matrix type.

    @memo Frees a Numerical Receipes matrix type
    @param a matrix
    @return void
    @author DS, Oct 1999
 ************************************************************************/
    template < class T >
    void nrMatrixFree( T** a );

} // namespace diag
namespace std
{
    /** Overloads fabs for complex numbers.

    @param a complex number
    @return absolute value
    @author DS, Oct 1999
 ************************************************************************/
    template < class T >
    inline T fabs( const std::complex< T >& a );
} // namespace std

namespace diag
{

    /** This function calculates the LU decomposition. The input matrix
    is overwritten by the output matrix.
   
    @memo LU decomposition
    @param a matrix
    @param n column/row number
    @param indx list of row permutations (return)
    @param d output +/-1 depending on even/odd row permuations
    @return 0 if successful, <0 otherwise
    @see Numerical Receipies in C
    @author DS, Oct 1999
 ************************************************************************/
    template < class T >
    int ludcmp( T* const* a, int n, int* indx, double* d );

    /** This function performs the LU back substitition. The input vector
    is overwritten by the result vector.
   
    @memo Back subsitution
    @param a LU matrix
    @param n column/row number
    @param indx list of row permutations
    @param b input/output vector
    @return 0 if successful, <0 otherwise
    @see Numerical Receipies in C
    @author DS, Oct 1999
 ************************************************************************/
    template < class T >
    int lubksb( const T* const* a, int n, const int* indx, T* b );

    /** Computes the inverse matrix. The input matrix is overwritten by
    the inverted output matrix.
   
    @memo Matrix inversion
    @param a matrix
    @param n column/row number
    @return 0 if successful, <0 otherwise
    @see Numerical Receipies in C
    @author DS, Oct 1999
 ************************************************************************/
    template < class T >
    int inverse( T* const* a, int n );

    /** Computes the product C = A^(-1) B. Both input matrices are preserved;
    the output matrix is return through the first argument. The matrix
    A must be of dimension n x n; whereas the matrix b can be of 
    dimension n x m.
   
    @memo Matrix inversion and multiplication
    @param c C matrix (return)
    @param a A matrix
    @param b B matrix
    @param n column number of A/row number of A, B and C
    @param m column number of B and C
    @return 0 if successful, <0 otherwise
    @see Numerical Receipies in C
    @author DS, Oct 1999
 ************************************************************************/
    template < class T >
    int imul( T* const* c, const T* const* a, const T* const* b, int n, int m );

    /*@}*/

} // namespace diag
#include "lud.cc"

#endif // _GDS_LUD_H
