#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "decimate.h"

int
main( void )
{
    float  x[ 262144 ], ytemp[ 512 ], y[ 131072 ], *p;
    int    nread, nwrite, i, j;
    float* temp;
    FILE * ifp, *ofp;

    decimate( 1, x, y, 0, 1, NULL, &temp ); /* initialize */

    ifp = fopen( "noise_256kfl.bin", "rb" );
    ofp = fopen( "dtest.bin", "wb" );

    nread = fread( x, 4, 262144, ifp );
    printf( "number of points read: %d\n", nread );

    for ( i = 0; i < 256; ++i )
    {
        p = x + i * 1024;
        decimate( 1, p, ytemp, 1024, 1, temp, &temp );
        for ( j = 0; j < 512; ++j )
            y[ j + i * 512 ] = ytemp[ j ];
    }

    nwrite = fwrite( y, 4, 131072, ofp );
    printf( "number of points written: %d\n", nwrite );

    decimate( 1, x, y, 0, 1, temp, NULL );
    return 0;
}
