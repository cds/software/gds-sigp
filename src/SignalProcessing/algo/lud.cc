/* -*- mode: c++; c-basic-offset: 3; -*- */

#include <cmath>
#include <complex>
#include <memory>

const double _LUD_TINY = 1.0e-20;

//   abs(float) and abs(double) were not part of the pre-c++98 standard.
//   This unites the abs/fabs ambiguity for early compilers.
//
// #if __cplusplus < 199711L
//
// namespace std {
//  template <class T>
//  inline T abs(T a)
//  {
//    return std::fabs(a);
//  }
// }
//
// #endif

namespace diag
{

    template < class T >
    T**
    nrMatrix( T* mat, int n, int m )
    {
        if ( mat == 0 )
        {
            return 0;
        }
        T** a = new ( std::nothrow ) T*[ n + 1 ];
        if ( a == 0 )
        {
            return a;
        }
        for ( int k = 1; k <= n; k++ )
        {
            a[ k ] = mat + ( k - 1 ) * m - 1;
        }
        return a;
    }

    template < class T >
    void
    nrMatrixFree( T** a )
    {
        delete[] a;
    }

    template < class T >
    int
    ludcmp( T* const* a, int n, int* indx, double* d )
    {
        int     i, imax = 1, j, k;
        T       dum, sum;
        double  big, temp;
        double* vv;

        vv = new ( std::nothrow ) double[ n + 1 ];
        if ( vv == 0 )
        {
            return -1;
        }
        *d = 1.0;
        for ( i = 1; i <= n; i++ )
        {
            big = 0.0;
            for ( j = 1; j <= n; j++ )
                if ( ( temp = abs( a[ i ][ j ] ) ) > big )
                    big = temp;
            if ( big == 0.0 )
            {
                delete[] vv;
                return -2; /* singular */
            }
            vv[ i ] = 1.0 / big;
        }
        for ( j = 1; j <= n; j++ )
        {
            for ( i = 1; i < j; i++ )
            {
                sum = a[ i ][ j ];
                for ( k = 1; k < i; k++ )
                    sum -= a[ i ][ k ] * a[ k ][ j ];
                a[ i ][ j ] = sum;
            }
            big = 0.0;
            for ( i = j; i <= n; i++ )
            {
                sum = a[ i ][ j ];
                for ( k = 1; k < j; k++ )
                    sum -= a[ i ][ k ] * a[ k ][ j ];
                a[ i ][ j ] = sum;
                if ( ( temp = vv[ i ] * abs( sum ) ) >= big )
                {
                    big = temp;
                    imax = i;
                }
            }
            if ( j != imax )
            {
                for ( k = 1; k <= n; k++ )
                {
                    dum = a[ imax ][ k ];
                    a[ imax ][ k ] = a[ j ][ k ];
                    a[ j ][ k ] = dum;
                }
                *d = -( *d );
                vv[ imax ] = vv[ j ];
            }
            indx[ j ] = imax;
            if ( a[ j ][ j ] == 0.0 )
                a[ j ][ j ] = _LUD_TINY;
            if ( j != n )
            {
                dum = 1.0 / ( a[ j ][ j ] );
                for ( i = j + 1; i <= n; i++ )
                    a[ i ][ j ] *= dum;
            }
        }
        delete[] vv;
        return 0;
    }

    template < class T >
    int
    lubksb( const T* const* a, int n, const int* indx, T* b )
    {
        int i, ii = 0, ip, j;
        T   sum;

        for ( i = 1; i <= n; i++ )
        {
            ip = indx[ i ];
            sum = b[ ip ];
            b[ ip ] = b[ i ];
            if ( ii )
                for ( j = ii; j <= i - 1; j++ )
                    sum -= a[ i ][ j ] * b[ j ];
            else if ( sum != 0.0 )
                ii = i;
            b[ i ] = sum;
        }
        for ( i = n; i >= 1; i-- )
        {
            sum = b[ i ];
            for ( j = i + 1; j <= n; j++ )
                sum -= a[ i ][ j ] * b[ j ];
            b[ i ] = sum / a[ i ][ i ];
        }
        return 0;
    }

    template < class T >
    int
    inverse( T* const* a, int n )
    {
        int* indx = new ( std::nothrow ) int[ n + 1 ];
        T*   col = new ( std::nothrow ) T[ n + 1 ];
        T*   iamat = new ( std::nothrow ) T[ n * n ];
        T**  ia = nrMatrix( iamat, n, n );

        if ( ( indx == 0 ) || ( col == 0 ) || ( iamat == 0 ) || ( ia == 0 ) )
        {
            delete[] indx;
            delete[] col;
            delete[] iamat;
            nrMatrixFree( ia );
            return -1;
        }
        double d;
        int    ret = ludcmp( a, n, indx, &d );
        if ( ret < 0 )
        {
            delete[] indx;
            delete[] col;
            delete[] iamat;
            nrMatrixFree( ia );
            return ret;
        }
        for ( int j = 1; j <= n; j++ )
        {
            for ( int i = 1; i <= n; i++ )
            {
                col[ i ] = 0.0;
            }
            col[ j ] = 1.0;
            if ( lubksb( a, n, indx, col ) < 0 )
            {
                delete[] indx;
                delete[] col;
                delete[] iamat;
                nrMatrixFree( ia );
                return ret;
            }
            {
                for ( int i = 1; i <= n; i++ )
                {
                    ia[ i ][ j ] = col[ i ];
                }
            }
        }
        {
            for ( int j = 1; j <= n; j++ )
            {
                for ( int i = 1; i <= n; i++ )
                {
                    a[ i ][ j ] = ia[ i ][ j ];
                }
            }
        }
        delete[] indx;
        delete[] col;
        delete[] iamat;
        nrMatrixFree( ia );
        return 0;
    }

    template < class T >
    int
    imul( T* const* c, const T* const* a, const T* const* b, int n, int m )
    {
        int* indx = new ( std::nothrow ) int[ n + 1 ];
        T*   col = new ( std::nothrow ) T[ n + 1 ];
        T*   ludat = new ( std::nothrow ) T[ n * n ];
        T**  lu = nrMatrix( ludat, n, n );
        int  ret = 0;

        if ( ( indx == 0 ) || ( col == 0 ) || ( ludat == 0 ) || ( lu == 0 ) )
        {
            ret = -1;
        }
        if ( ret >= 0 )
        {
            double d;
            for ( int j = 1; j <= n; j++ )
            {
                for ( int i = 1; i <= n; i++ )
                {
                    lu[ i ][ j ] = a[ i ][ j ];
                }
            }
            ret = ludcmp( lu, n, indx, &d );
        }
        if ( ret >= 0 )
        {
            for ( int j = 1; j <= m; j++ )
            {
                for ( int i = 1; i <= n; i++ )
                {
                    col[ i ] = b[ i ][ j ];
                }
                if ( lubksb( lu, n, indx, col ) < 0 )
                {
                    ret = -3;
                    break;
                }
                {
                    for ( int i = 1; i <= n; i++ )
                    {
                        c[ i ][ j ] = col[ i ];
                    }
                }
            }
        }

        delete[] indx;
        delete[] col;
        delete[] lu;
        delete[] ludat;
        return ret;
    }

} // namespace diag
