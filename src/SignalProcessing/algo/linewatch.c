/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: linewatch						*/
/*                                                         		*/
/* Procedure Description: line amplitude and phase monitoring, line 	*/
/*                        subtraction                                   */
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "linewatch.h"
#include "gdsconst.h"

unsigned int
linewatch_gettsbuffersize( double sampling_rate, double responsetime )
{
    /* second nearest integer power of two above the response time */
    int two_power;
    /* number of seconds of data stored */
    int duration_secs;

    /* The response time is half the length of the stored timeseries. */
    /* Find nearest integer power of two set response time and double it*/
    /* For example, if the response time was set to 36 seconds, the FFT */
    /* length would be twice 64, or 128 seconds */
    frexp( responsetime, &two_power );
    duration_secs = (int)pow( (double)2, (double)( ++two_power ) );

    /* Return the length required of the data timeseries buffer */
    return (unsigned int)ceil( sampling_rate ) * duration_secs;
}

int
linewatch_constructor( linedata* ldata,
                       double    freq,
                       double    width,
                       double    sampling_rate,
                       double    responsetime,
                       double*   tseries_buffer )
{
    /* second nearest integer power of two above the response time */
    int two_power;
    /* number of seconds of data stored */
    int duration_secs;
    /* any fractional part of the sampling rate in Hz; nonzero not supported */
    double srateint;
    /* resolution bandwidth achievable using stored timeseries data */
    double resbw;
    /* ratio of line frequency to resolution bandwidth */
    double resbwmult;
    /* nearest integer multiple of resolution bandwidth to the line frequency */
    int nearestresbwmult;
    /* integer of first and last frequency bin to be monitored */
    int firstbin;
    int lastbin;
    /* counter for looping over phase shifts */
    int pscount;

    /* The response time is half the length of the stored timeseries. */
    /* Find nearest integer power of two set response time and double it*/
    /* For example, if the response time was set to 36 seconds, the FFT */
    /* length would be twice 64, or 128 seconds */
    frexp( responsetime, &two_power );
    duration_secs = (int)pow( (double)2, (double)( ++two_power ) );
    ldata->datasize = (int)ceil( sampling_rate ) * duration_secs;

    /* Read in pointer to timeseries buffer. Memory at this pointer */
    /* should be allocated outside the structure so that several */
    /* linewatch structures can share the same timeseries buffer. */
    ldata->tseries = tseries_buffer;

    /* check the sampling rate is an integer */
    if ( modf( sampling_rate, &srateint ) != 0 )
    {
        printf( "ERROR: non integer sampling rate in Hz not supported." );
        return -1;
    }
    /* frequency interval between successive nondegenerate frequency bins */
    resbw = sampling_rate / ldata->datasize;
    /* enter sampling rate into linetracker data structure */
    ldata->srate = srateint;
    /* get nearest integer multiple of resbw to the line frequency */
    resbwmult = freq / resbw;
    if ( resbwmult - floor( resbwmult ) < 0.5 )
    {
        nearestresbwmult = floor( resbwmult );
    }
    else
    {
        nearestresbwmult = ceil( resbwmult );
    }
    /* get number of bins within the peak width */
    ldata->no_of_frequencies = (int)floor( width / resbw );
    /* get multiples of the sampling rate corresponding to the first and */
    /* last bins to be monitored                                         */
    if ( ( ldata->no_of_frequencies % 2 ) == 0 )
    {
        if ( ( resbwmult - nearestresbwmult ) >= 0 )
        {
            lastbin = nearestresbwmult + ldata->no_of_frequencies / 2;
            firstbin = nearestresbwmult - ldata->no_of_frequencies / 2 + 1;
        }
        else
        {
            lastbin = nearestresbwmult + ldata->no_of_frequencies / 2 - 1;
            firstbin = nearestresbwmult - ldata->no_of_frequencies / 2;
        }
    }
    else
    {
        firstbin = nearestresbwmult - ( ldata->no_of_frequencies - 1 ) / 2;
        lastbin = nearestresbwmult + ( ldata->no_of_frequencies - 1 ) / 2;
    }
    /* allocate memory for phases */
    ldata->cosphase = calloc( ldata->no_of_frequencies, sizeof( double ) );
    ldata->sinphase = calloc( ldata->no_of_frequencies, sizeof( double ) );
    /* Allocate memory for fourier coefficients and set them to zero. */
    /* Calloc does the setting to zero by default. */
    ldata->real = calloc( ldata->no_of_frequencies, sizeof( double ) );
    ldata->imag = calloc( ldata->no_of_frequencies, sizeof( double ) );
    /* Set values of phase shifts */
    for ( pscount = firstbin; pscount < firstbin + ldata->no_of_frequencies;
          pscount++ )
    {
        ldata->cosphase[ pscount - firstbin ] =
            cos( 2 * (double)PI * (double)pscount / (double)ldata->datasize );
        ldata->sinphase[ pscount - firstbin ] =
            sin( 2 * (double)PI * (double)pscount / (double)ldata->datasize );
    }
    /* Calculate bin offset from current bin to correction bin. */
    ldata->correctionoffset =
        (unsigned int)floor( (double)ldata->datasize / 2 );

    /* Allocate memory for cos and sin of phase shifts to correction bin */
    ldata->coslag = calloc( ldata->no_of_frequencies, sizeof( double ) );
    ldata->sinlag = calloc( ldata->no_of_frequencies, sizeof( double ) );

    /* Calculate phase shifts corresponding to phase correction */
    for ( pscount = firstbin; pscount < firstbin + ldata->no_of_frequencies;
          pscount++ )
    {
        ldata->coslag[ pscount - firstbin ] =
            cos( 2 * (double)PI * ( (double)ldata->correctionoffset - 1 ) *
                 (double)pscount / ( (double)ldata->datasize ) );
        ldata->sinlag[ pscount - firstbin ] =
            sin( 2 * (double)PI * ( (double)ldata->correctionoffset - 1 ) *
                 (double)pscount / ( (double)ldata->datasize ) );
    }

    /* set counter to zero */
    ldata->dcounter = 0;

    /* set line monitor set bit to true */
    ldata->initialized = TRUE;
    /* flag set to indicate that the timeseries data buffer */
    /* is full set to FALSE */
    ldata->bufferfull = FALSE;

    /* debug information */
    printf( "Number of frequencies is %u.\n", ldata->no_of_frequencies );
    printf( "Frequency %.2f Hz.\n", freq );
    printf( "Resbw %.4e Hz.\n", resbw );
    printf( "Nearest integer bin %d.\n", nearestresbwmult );
    printf( "Size of FFT is %d.\n", ldata->datasize );
    printf( "Correction offset is %u bins.\n", ldata->correctionoffset );

    return 0;
}

int
linewatch_destructor( linedata* ldata )
{
    free( ldata->cosphase );
    free( ldata->sinphase );
    free( ldata->coslag );
    free( ldata->sinlag );
    free( ldata->real );
    free( ldata->imag );
    ldata->initialized = FALSE;
    ldata->bufferfull = FALSE;
    return 0;
};

double
linewatch_increment( linedata* ldata,
                     double    timesample,
                     double*   rp,
                     double*   ip,
                     double**  ptsbin )
{
    /* counter for Fourier bins */
    int bincount;
    /* ringbuffer bin where data is subtracted */
    unsigned int correctionbin;
    /* actual phase corrected line sum to be subtracted */
    double phasedcorrection;
    /* sum of real parts of Fourier coefficients */
    double rpsum;

    /* loop over Fourier coefficients to be evolved */
    rpsum = 0;
    for ( bincount = 0; bincount < ldata->no_of_frequencies; bincount++ )
    {
        /* calculate evolved Fourier coefficients */
        *rp = ( ldata->cosphase[ bincount ] ) *
                ( ldata->real[ bincount ] + timesample -
                  ( ldata->tseries[ ldata->dcounter ] ) ) -
            ( ldata->sinphase[ bincount ] ) * ( ldata->imag[ bincount ] );
        *ip = ( ldata->sinphase[ bincount ] ) *
                ( ldata->real[ bincount ] + timesample -
                  ( ldata->tseries[ ldata->dcounter ] ) ) +
            ( ldata->cosphase[ bincount ] ) * ( ldata->imag[ bincount ] );
        /* update the space for the evolved Fourier coefficients */
        /* in the linewatch structure */
        ldata->real[ bincount ] = ( *rp );
        ldata->imag[ bincount ] = ( *ip );
        /* printf("%e,%e\n",(*rp),(*ip)); */
        /* Calculate correction to be applied at the subtraction bin, evolving */
        /* the phase of the correction over the number of samples gap between */
        /* the current timeseries bin and the correction bin */
        rpsum += ( ldata->coslag[ bincount ] * ( *rp ) -
                   ldata->sinlag[ bincount ] * ( *ip ) );
    }

    /* Calculate bin where correction will be made */
    correctionbin =
        ( ldata->dcounter + ldata->correctionoffset ) % ( ldata->datasize );
    /* Set the pointer to the bin for the correction in the function argument*/
    *ptsbin = ldata->tseries + correctionbin;

    /* Add current timesample to tseries buffer */
    /* The following line was removed to an external function because */
    /* writing to the timeseries effects other line monitors in the array */
    /* that use the timeseries at the same pointer. External function */
    /* is linewatch_nextsample. */
    /* ldata->tseries[ldata->dcounter]=timesample; */

    /* Increment counter, resetting to zero when it reaches the end */
    /* of the timeseries buffer. Removed to an external function so that */
    /* arrays of line monitors may operate using the same ring buffer */
    /* for the time series. External function is linewatch_nextsample.*/
    /* ldata->dcounter=(ldata->dcounter+1)%(ldata->datasize); */

    /* The returned value is that which should be subtracted from */
    /* the timeseries to remove the line. */
    phasedcorrection = 2 * rpsum / ( ldata->datasize );
    return phasedcorrection;
}

void
linewatch_nextsample( linedata* ldata, double timesample )
{
    /* update timeseries ring buffer with latest timesample */
    ldata->tseries[ ldata->dcounter ] = timesample;
    /* increment counter pointing to current sample in ring buffer */
    ldata->dcounter = ( ldata->dcounter + 1 ) % ( ldata->datasize );
    return;
}

void
linewatch_duplicate( double*   pnewts,
                     linedata* new_ldata,
                     linedata* existing_ldata )
{
    unsigned int fc;
    new_ldata->datasize = existing_ldata->datasize;
    new_ldata->tseries = pnewts;
    new_ldata->srate = existing_ldata->srate;
    new_ldata->no_of_frequencies = existing_ldata->no_of_frequencies;
    for ( fc = 0; fc < existing_ldata->no_of_frequencies; ++fc )
    {
        new_ldata->cosphase[ fc ] = existing_ldata->cosphase[ fc ];
        new_ldata->sinphase[ fc ] = existing_ldata->sinphase[ fc ];
        new_ldata->coslag[ fc ] = existing_ldata->coslag[ fc ];
        new_ldata->sinlag[ fc ] = existing_ldata->sinlag[ fc ];
    }
    new_ldata->dcounter = 0;
    new_ldata->correctionoffset = existing_ldata->correctionoffset;
    new_ldata->initialized = existing_ldata->initialized;
    new_ldata->bufferfull = existing_ldata->bufferfull;
    return;
}
