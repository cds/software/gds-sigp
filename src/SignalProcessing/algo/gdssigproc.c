/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name:	gdssigproc						*/
/*                                                         		*/
/* Procedure Description: signal processing utility functions 	       	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "gdsconst.h"
#include "gdssigproc.h"

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: sDotProd					*/
/*                                                         		*/
/* Procedure Description: calculates the dot product of two single  	*/
/*			  precision vectors				*/
/* 									*/
/* Procedure Arguments: pointers to 2 input vectors; number of points	*/
/*			in vectors (vectors must be same length)	*/
/*                                                         		*/
/* Procedure Returns: the result x.y , double precision	      		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
double
sDotProd( const float x[], const float y[], int npt )
{

    double dotproduct = 0.0;
    int    i;

    for ( i = 0; i < npt; ++i )
    {
        dotproduct += x[ i ] * y[ i ];
    }
    return dotproduct;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: sDotProd					*/
/*                                                         		*/
/* Procedure Description: calculates the dot product of two single  	*/
/*			  precision vectors				*/
/* 									*/
/* Procedure Arguments: pointers to 2 input vectors; number of points	*/
/*			in vectors (vectors must be same length)	*/
/*                                                         		*/
/* Procedure Returns: the result x.y , double precision	      		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
double
dDotProd( const double x[], const double y[], int npt )
{

    double dotproduct = 0.0;
    int    i;

    for ( i = 0; i < npt; ++i )
    {
        dotproduct += x[ i ] * y[ i ];
    }
    return dotproduct;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: sMixdown      				*/
/*                                                         		*/
/* Procedure Description: Multiplies a vector by a complex sine wave	*/
/*		     of user defined frequency.			        */
/* 									*/
/* Procedure Arguments:	pointers to real & imaginary parts of the 	*/
/*		     input and output vectors; input flag (unused); 	*/
/*		     number of input points; starting time offset (sec);*/
/*		     time interval between points (sec);                */
/*		     heterodyne frequency (Hz)				*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
void
sMixdown( int         flag,
          const float xre[],
          const float xim[],
          float       yre[],
          float       yim[],
          int         n,
          double      t,
          double      dt,
          double      fmix )
{
    int    i; /* a counter */
    double phasestep; /* phase interval between steps in time series*/
    /* see Ed Daw lab book 2 page 95. */
    double phaseoffs; /* the phase at the start of the time series */
    /* relative to some arbitrary zero */
    float reexp; /* temporary store for real part of exponential */
    float imexp; /* temporary store for imaginary part of exp */

    phasestep = -2 * PI * fmix * dt; /* - sign is chosen to be consistent */
    phaseoffs = -2 * PI * fmix * t; /* with FFT definition; see Ed Daw   */
    /* lab book 2 p 113 */

    /*heterodyne routine for real input data*/
    if ( xim == NULL )
    {
        for ( i = 0; i < n; ++i )
        {
            yre[ i ] = xre[ i ] * (float)cos( phaseoffs + phasestep * i );
            yim[ i ] = xre[ i ] * (float)sin( phaseoffs + phasestep * i );
        }
    }

    /*heterodyne algorithm for complex input data*/
    else
    {
        for ( i = 0; i < n; ++i )
        {
            reexp = (float)cos( phaseoffs + phasestep * i );
            imexp = (float)sin( phaseoffs + phasestep * i );
            yre[ i ] = xre[ i ] * reexp - xim[ i ] * imexp;
            yim[ i ] = xre[ i ] * imexp + xim[ i ] * reexp;
        }
    }
    return;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: sdMixdown      				*/
/*                                                         		*/
/* Procedure Description: Multiplies a vector by a complex sine wave	*/
/*		     of user defined frequency.			        */
/* 									*/
/* Procedure Arguments:	pointers to real & imaginary parts of the 	*/
/*		     input and output vectors; input flag (unused); 	*/
/*		     number of input points; starting time offset (sec);*/
/*		     time interval between points (sec);                */
/*		     heterodyne frequency (Hz)				*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
void
sdMixdown( int         flag,
           const float xre[],
           const float xim[],
           double      yre[],
           double      yim[],
           int         n,
           double      t,
           double      dt,
           double      fmix )
{
    int    i; /* a counter */
    double phasestep; /* phase interval between steps in time series*/
    /* see Ed Daw lab book 2 page 95. */
    double phaseoffs; /* the phase at the start of the time series */
    /* relative to some arbitrary zero */
    double reexp; /* temporary store for real part of exponential */
    double imexp; /* temporary store for imaginary part of exp */

    phasestep = -2 * PI * fmix * dt; /* - sign is chosen to be consistent */
    phaseoffs = -2 * PI * fmix * t; /* with FFT definition; see Ed Daw   */
    /* lab book 2 p 113 */

    /*heterodyne routine for real input data*/
    if ( xim == NULL )
    {
        for ( i = 0; i < n; ++i )
        {
            yre[ i ] = xre[ i ] * cos( phaseoffs + phasestep * i );
            yim[ i ] = xre[ i ] * sin( phaseoffs + phasestep * i );
        }
    }

    /*heterodyne algorithm for complex input data*/
    else
    {
        for ( i = 0; i < n; ++i )
        {
            reexp = cos( phaseoffs + phasestep * i );
            imexp = sin( phaseoffs + phasestep * i );
            yre[ i ] = xre[ i ] * reexp - xim[ i ] * imexp;
            yim[ i ] = xre[ i ] * imexp + xim[ i ] * reexp;
        }
    }
    return;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: dMixdown      				*/
/*                                                         		*/
/* Procedure Description: Multiplies a vector by a complex sine wave	*/
/*		     of user defined frequency.			        */
/* 									*/
/* Procedure Arguments:	pointers to real & imaginary parts of the 	*/
/*		     input and output vectors; input flag (unused); 	*/
/*		     number of input points; starting time offset (sec);*/
/*		     time interval between points (sec);                */
/*		     heterodyne frequency (Hz)				*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
void
dMixdown( int         flag,
          const double xre[],
          const double xim[],
          double       yre[],
          double       yim[],
          int         n,
          double      t,
          double      dt,
          double      fmix )
{
    int    i; /* a counter */
    double phasestep; /* phase interval between steps in time series*/
    /* see Ed Daw lab book 2 page 95. */
    double phaseoffs; /* the phase at the start of the time series */
    /* relative to some arbitrary zero */
    double reexp; /* temporary store for real part of exponential */
    double imexp; /* temporary store for imaginary part of exp */
    
    phasestep = -2 * PI * fmix * dt; /* - sign is chosen to be consistent */
    phaseoffs = -2 * PI * fmix * t; /* with FFT definition; see Ed Daw   */
    /* lab book 2 p 113 */

    /*heterodyne routine for real input data*/
    if ( xim == NULL )
    {
        for ( i = 0; i < n; ++i )
        {
            yre[ i ] = xre[ i ] * (double)cos( phaseoffs + phasestep * i );
            yim[ i ] = xre[ i ] * (double)sin( phaseoffs + phasestep * i );
        }
    }

    /*heterodyne algorithm for complex input data*/
    else
    {
        for ( i = 0; i < n; ++i )
        {
            reexp = (double)cos( phaseoffs + phasestep * i );
            imexp = (double)sin( phaseoffs + phasestep * i );
            yre[ i ] = xre[ i ] * reexp - xim[ i ] * imexp;
            yim[ i ] = xre[ i ] * imexp + xim[ i ] * reexp;
        }
    }
    return;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: zMean	      				*/
/*                                                         		*/
/* Procedure Description: Calculates the mean of an array of double	*/
/*		          complex data points.			        */
/* 									*/
/* Procedure Arguments:	pointer to the data array, of type dCmplx; 	*/
/*		     number of points in array			  	*/
/*                                                         		*/
/* Procedure Returns: the mean, as a type dCmplx		      	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
dCmplx
zMean( const dCmplx z[], int npt )
{
    int    j;
    dCmplx mean = { 0 };

    for ( j = 0; j < npt; ++j )
    {
        mean.re += z[ j ].re;
        mean.im += z[ j ].im;
    }
    mean.re /= npt;
    mean.im /= npt;

    return mean;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: szMean	      				*/
/*                                                         		*/
/* Procedure Description: Calculates the mean of an array of double	*/
/*		          complex data points.			        */
/* 									*/
/* Procedure Arguments:	pointer to the data array, of type sCmplx; 	*/
/*		     number of points in array			  	*/
/*                                                         		*/
/* Procedure Returns: the mean, as a type dCmplx		      	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
dCmplx
szMean( const sCmplx z[], int npt )
{
    int    j;
    dCmplx mean = { 0 };

    for ( j = 0; j < npt; ++j )
    {
        mean.re += z[ j ].re;
        mean.im += z[ j ].im;
    }
    mean.re /= npt;
    mean.im /= npt;

    return mean;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: sMean	      				*/
/*                                                         		*/
/* Procedure Description: Calculates the mean of an array of floats	*/
/* 									*/
/* Procedure Arguments:	pointer to the data array;		 	*/
/*		     number of points in array			  	*/
/*                                                         		*/
/* Procedure Returns: the mean, as a type double		      	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
double
sMean( const float x[], int npt )
{
    int    j;
    double mean = 0.0;

    for ( j = 0; j < npt; ++j )
    {
        mean += x[ j ];
    }
    mean /= npt;
    return mean;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: sMean	      				*/
/*                                                         		*/
/* Procedure Description: Calculates the mean of an array of floats	*/
/* 									*/
/* Procedure Arguments:	pointer to the data array;		 	*/
/*		     number of points in array			  	*/
/*                                                         		*/
/* Procedure Returns: the mean, as a type double		      	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
double
dMean( const double x[], int npt )
{
    int    j;
    double mean = 0.0;

    for ( j = 0; j < npt; ++j )
    {
        mean += x[ j ];
    }
    mean /= npt;
    return mean;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: zMultply	      				*/
/*                                                         		*/
/* Procedure Description: Calculates the product of two double complex	*/
/*			  numbers					*/
/* 									*/
/* Procedure Arguments:	complex multiplicand 1;	complex multiplicand 2	*/
/*                                                         		*/
/* Procedure Returns: the product, as a type dCmplx		      	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
dCmplx
zMultply( dCmplx z1, dCmplx z2 )
{
    dCmplx z;
    z.re = z1.re * z2.re - z1.im * z2.im;
    z.im = z1.im * z2.re + z1.re * z2.im;
    return z;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: zConj	      				*/
/*                                                         		*/
/* Procedure Description: Calculates the conjugate of a complex	number  */
/* 									*/
/* Procedure Arguments:	complex number, type dCmplx			*/
/*                                                         		*/
/* Procedure Returns: the conjugate, as a type dCmplx		      	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
dCmplx
zConj( dCmplx z )
{
    dCmplx zstar;
    zstar.re = z.re;
    zstar.im = -z.im;
    return zstar;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: zAdd	      				*/
/*                                                         		*/
/* Procedure Description: Adds two complex numbers			*/
/* 									*/
/* Procedure Arguments:	complex numbers 1 & 2				*/
/*                                                         		*/
/* Procedure Returns: the sum, as a type dCmplx			      	*/
/*                                                     	    		*/
/*----------------------------------------------------------------------*/
dCmplx
zAdd( dCmplx z1, dCmplx z2 )
{
    dCmplx zsum;
    zsum.re = z1.re + z2.re;
    zsum.im = z1.im + z2.im;
    return zsum;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: sDataCopy	      				*/
/*                                                         		*/
/* Procedure Description: Copies a data array				*/
/* 									*/
/* Procedure Arguments:	destination, source, cmoplex?, start, length,	*/
/*                      bin, logbin spacing             		*/
/*                                                         		*/
/* Procedure Returns: void					      	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
void
sDataCopy( float*       dest,
           const float* src,
           int          cmplx,
           int          start,
           int          len,
           int          bin,
           int          logbin )
{
    int max; /* max pts. in destination */
    int i; /* index into dest */
    int j; /* index over bin */

    if ( ( dest == NULL ) || ( src == NULL ) )
    {
        return;
    }

    if ( start < 0 )
        start = 0;
    if ( bin < 1 )
        bin = 1;
    if ( len < 0 )
        len = 0;
    max = len / bin;
    if ( max <= 0 )
    {
        return;
    }

    /* calculate start address */
    src += start;
    if ( cmplx != 0 )
    {
        src += start;
    }
    /* optimization for bining of 1 */
    if ( bin == 1 )
    {
        memcpy( dest, src, ( cmplx == 0 ? 1 : 2 ) * max * sizeof( float ) );
        return;
    }

    /* linear bining */
    if ( ( logbin == 0 ) || ( len <= 1 ) )
    {
        for ( i = 0; i < max; i++ )
        {
            if ( cmplx == 0 )
            {
                dest[ i ] = src[ bin * i ];
            }
            else
            {
                dest[ 2 * i ] = src[ 2 * ( bin * i ) ];
                dest[ 2 * i + 1 ] = src[ 2 * ( bin * i ) + 1 ];
            }
            for ( j = 1; j < bin; j++ )
            {
                if ( cmplx == 0 )
                {
                    dest[ i ] += src[ bin * i + j ];
                }
                else
                {
                    dest[ 2 * i ] += src[ 2 * ( bin * i + j ) ];
                    dest[ 2 * i + 1 ] += src[ 2 * ( bin * i + j ) + 1 ];
                }
            }
            if ( cmplx == 0 )
            {
                dest[ i ] /= bin;
            }
            else
            {
                dest[ 2 * i ] /= bin;
                dest[ 2 * i + 1 ] /= bin;
            }
        }
    }

    /* logarithmic bining */
    /* space points equally on a logarithmic scale
         use linear interpolation to for points which fall
         in between. */
    else
    {
        double dx;
        double x1;
        double x2;
        double y[ 2 ];
        int    j1;
        int    j2;
        dx = exp( log( len ) * bin / len );
        x1 = 1.;
        for ( i = 0; i < max; i++ )
        {
            j1 = (int)floor( x1 );
            x2 = x1 * dx;
            j2 = (int)ceil( x2 );
            if ( j2 > len )
                j2 = len;
            if ( cmplx == 0 )
            {
                y[ 0 ] = src[ j1 - 1 ] / 2. -
                    ( x1 - j1 ) *
                        ( src[ j1 - 1 ] +
                          ( x1 - j1 ) * ( src[ j1 ] - src[ j1 - 1 ] ) / 2. );
                y[ 1 ] = 0;
            }
            else
            {
                y[ 0 ] = src[ 2 * ( j1 - 1 ) ] / 2. -
                    ( x1 - j1 ) *
                        ( src[ 2 * ( j1 - 1 ) ] +
                          ( x1 - j1 ) *
                              ( src[ 2 * j1 ] - src[ 2 * ( j1 - 1 ) ] ) / 2. );
                y[ 1 ] = src[ 2 * ( j1 - 1 ) + 1 ] / 2. -
                    ( x1 - j1 ) *
                        ( src[ 2 * ( j1 - 1 ) + 1 ] +
                          ( x1 - j1 ) *
                              ( src[ 2 * j1 + 1 ] -
                                src[ 2 * ( j1 - 1 ) + 1 ] ) /
                              2. );
            }
            for ( j = j1 + 1; j < j2; j++ )
            {
                if ( cmplx == 0 )
                {
                    y[ 0 ] += src[ j - 1 ];
                }
                else
                {
                    y[ 0 ] += src[ 2 * ( j - 1 ) ];
                    y[ 1 ] += src[ 2 * ( j - 1 ) + 1 ];
                }
            }
            if ( cmplx == 0 )
            {
                y[ 0 ] += src[ j2 - 1 ] / 2. -
                    ( j2 - x2 ) *
                        ( src[ j2 - 1 ] -
                          ( j2 - x2 ) * ( src[ j2 - 1 ] - src[ j2 - 2 ] ) /
                              2. );
                dest[ i ] = (float)( y[ 0 ] / ( x2 - x1 ) );
            }
            else
            {
                y[ 0 ] += src[ 2 * ( j2 - 1 ) ] / 2. -
                    ( j2 - x2 ) *
                        ( src[ 2 * ( j2 - 1 ) ] -
                          ( j2 - x2 ) *
                              ( src[ 2 * ( j2 - 1 ) ] -
                                src[ 2 * ( j2 - 2 ) ] ) /
                              2. );
                y[ 1 ] += src[ 2 * ( j2 - 1 ) + 1 ] / 2. -
                    ( j2 - x2 ) *
                        ( src[ 2 * ( j2 - 1 ) + 1 ] -
                          ( j2 - x2 ) *
                              ( src[ 2 * ( j2 - 1 ) + 1 ] -
                                src[ 2 * ( j2 - 2 ) + 1 ] ) /
                              2. );
                dest[ 2 * i ] = (float)( y[ 0 ] / ( x2 - x1 ) );
                dest[ 2 * i + 1 ] = (float)( y[ 1 ] / ( x2 - x1 ) );
            }
            x1 = x2;
        }
    }
}
