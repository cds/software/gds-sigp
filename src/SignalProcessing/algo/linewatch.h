/************************************************************/
/*                                                          */
/* Module Name: linewatch                                   */
/*                                                          */
/* Module Description: line amplitude and phase monitoring, */
/* line subtraction                                         */
/*                                                          */
/* Ed Daw, 12th October 2007                                */
/* e.daw@shef.ac.uk                                         */
/* +44 114 222 4353                                         */
/*                                                          */
/* Comments: This header file should generate its own       */
/* documentation on typing doc++ < header file name >       */
/* See gds tree build instructions on how to download doc++ */
/*                                                          */
/* Further comments: A paper summarizing the operation of   */
/* this algorithm is available from arXiv:0808.2573 [gr-qc] */
/*                                                          */
/************************************************************/

#ifndef _LINEWATCH_H
#define _LINEWATCH_H

#ifdef __cplusplus
extern "C" {
#endif

/** @name Line watching library overview    
      @memo These algorithms allow for the tracking of the amplitude and phase
            of a single Fourier coefficient of a time series. The theory behind
            this code is described at \URL{http://arxiv.org/pdf/0808.2573}. 
      @author Ed Daw, 12th October 2007
  *************************************************************************/

/*@{*/

/** @name Constants and flags for linewatch routines */

/*@{*/

/** integer 1 defined as boolean TRUE */
#define TRUE 1
/** integer 0 defined as boolean FALSE */
#define FALSE 0

/*@}*/

/** @name Linewatch parameter type
      Parameter structure for storing data used by line subtraction.
      
      @memo A structure holding information about the line monitor
            passed between successive iterations.
      @author Ed Daw, 12th October 2007
  ******************************************************************/

/*@{*/

/** Linewatch parameter structure. Used to store various data needed
      for monitoring the line parameters and synthesizing a single Fourier
      component of the subtraction waveform.
  ************************************************************************/

struct linedataset
{
    /** The number of elements in the timeseries buffer array. 
    Equal to the length of the FFT to obtain the same Fourier
    coefficients */
    unsigned int datasize;
    /** Pointer to the stored timeseries */
    double* tseries;
    /** sampling rate in Hz */
    int srate;
    /** The number of Fourier components to be monitored */
    unsigned int no_of_frequencies;
    /** An array of phase shifts, one for each frequency. 
	this is the phase shift per timesample of a sinewave
	at the frequency of this Fourier component, in radians.*/
    double* cosphase;
    double* sinphase;
    /** A counter that increments by one for each time sample
	written to the tseries array. Software will reset this
	counter to zero once it reaches the value datasize */
    unsigned int dcounter;
    /** Real and imaginary parts of the most recent Fourier
	coefficient for each frequency. */
    double* real;
    double* imag;
    /** Bin offset from current bin to bin where correction applied. 
	This is basically half the timeseries length, but be careful where
	the timeseries length is odd. */
    unsigned int correctionoffset;
    /** Real and imaginary parts of phase correction for offset from
	bin 0 of FFT to bin where correction is applied */
    double* coslag;
    double* sinlag;
    /** Flag set to TRUE when all fields are initialized */
    unsigned int initialized;
    /** Flag set by user to indicate that the number of iterations
	has equalled or exceed the length of the timeseries data buffer*/
    unsigned int bufferfull;
};
typedef struct linedataset linedata;

/*@}*/

/** @name Linewatch functions
      @author Ed Daw, 12th October 2007
  *********************************************************************/

/*@{*/

/** Calculate the number of entries in the timeseries buffer necessary to 
      monitor the line with the requested response time and sampling rate. 
      The code maintains a ring buffer of previous samples from the data 
      stream. To allow the memory for this buffer to be allocated by the 
      code calling the line watching library, this function returns the 
      number of elements this buffer must be sized to hold.

      @param sampling_rate The data sampling rate in Hz
      @param responsetime The timescale for responses of the line 
                          monitor to changes in line parameters.
      @return The number of doubles that the buffer will be required to hold.
      @author Ed Daw, October 2007
  ***************************************************************************/
unsigned int linewatch_gettsbuffersize( double sampling_rate,
                                        double responsetime );

/** Initialize the line monitor. Though this code is written in C, the
   way it is used is C++ like in the sense that a function must be called
   to initalize the structure containing the data passed between iterations
   of the line monitor. This C function is the constructor that initializes
   this structure.  

   @param ldata A pointer to the parameter structure of type linedata
                associated with the line monitor.
   @param freq The estimated central frequency of the line, in Hz
   @param width The estimated full width of the line above the surrounding
                noise floor, in Hz.
   @param sampling_rate The data sampling rate, in Hz
   @param responsetime The timescale for response of the monitor to changes
                       in the amplitude and phase of the line.
   @param tseries_buffer A pointer to a memory segment sized to hold the 
                         number of doubles returned by linewatch_gettsbuffersize.
   @return Zero.
   @author Ed Daw, October 2007
***************************************************************************/
int linewatch_constructor( linedata* ldata,
                           double    freq,
                           double    width,
                           double    sampling_rate,
                           double    responsetime,
                           double*   tseries_buffer );

/** Clear the line monitor after final use - equivalent to a C++
      destructor for the line monitor.

      @param ldata - a pointer to a parameter structure of type linedata
                     associated with the line monitor.
      @return Zero.
      @author Ed Daw, October 2007
  **************************************************************************/
int linewatch_destructor( linedata* ldata );

/** Increment monitor by one timesample. 

  @param ldata A pointer to the paramter structure of type linedata
               associated with the line monitor.
  @param timesample The new timesample from the time series
  @param rp Address of the calculated Fourier coefficent, real part.
  @param ip Address of the calculated Fourier coefficent, imaginary part.
  @param ptsbin A pointer to the memory address of the timesample from
                the stored timeseries from which the correction should be
                subtracted.
  @return The correction to be subtracted from the timesample referred
          to by the address at ptsbin.
  @author Ed Daw, October 2007
  ***************************************************************************/
double linewatch_increment( linedata* ldata,
                            double    timesample,
                            double*   rp,
                            double*   ip,
                            double**  ptsbin );

/** Update timeseries buffer. Use after linewatch_increment
      to overwrite the contents of the timeseries buffer with the 
      latest time sample. This is an separate function because
      where there is an array of lines monitors, each line monitor 
      may be using the same ring buffer. In this case each line monitor
      separately will need to use the previous data stored in the ring
      buffer, so the ring buffer should not be overwritten with the new
      data point until all the line monitors have been incremented.
      
      @param ldata A pointer to the parameter structure of type
                   linedata associated with the line monitor.
      @param timesample The latest timesample.
      @author Ed Daw, October 2007
  **************************************************************************/
void linewatch_nextsample( linedata* ldata, double timesample );
/** Create a new linewatch structure for the same lines as an existing one.
      @param new_ldata A pointer to the new line data
      @param existing_ldata A pointer to the existing line data
      @author Ed Daw, September 2008
  *************************************************************************/
void linewatch_duplicate( double*   pnewts,
                          linedata* new_ldata,
                          linedata* existing_ldata );

/*@}*/

/*@}*/

#ifdef __cplusplus
}
#endif

#endif /* _LINEWATCH_H */
