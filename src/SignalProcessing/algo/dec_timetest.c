#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "tconv.h"
#include "decimate.h"

int
main( void )
{
    float     x[ 262144 ], y[ 131072 ];
    int       nread, nwrite;
    float*    temp;
    FILE *    ifp, *ofp;
    tainsec_t start;

    decimate( 1, x, y, 0, 1, NULL, &temp ); /* initialize */

    ifp = fopen( "noise_256kfl.bin", "rb" );
    ofp = fopen( "dtimetest.bin", "wb" );

    nread = fread( x, 4, 262144, ifp );
    printf( "number of points read: %d\n", nread );
    start = TAInow( );
    decimate( 1, x, y, 262144, 1, temp, &temp );
    printf( "Time elapsed for decimate = %g ms\n",
            ( TAInow( ) - start ) / 1E6 );

    nwrite = fwrite( y, 4, 131072, ofp );
    printf( "number of points written: %d\n", nwrite );

    decimate( 1, x, y, 0, 1, temp, NULL ); /* cleanup */
    return 0;
}

#if 0
   void decimateF (int flag, float x[], float y[], int n, int dec,
                  void* prev, void** next)
   {
   
      float *filt_coeff = NULL, sum, *data, *fold_data;
      int    filt_length = 0, data_length, filt_ord, i, j, k, npt_y;
      float firls1[11] = {2.225549e-3, -3.914217e-3, 6.226653e-3,
      -9.330331e-3, 1.347478e-2, -1.907462e-2,
      2.690526e-2, -3.863524e-2, 5.863624e-2,
      -1.030298e-1, 3.172755e-1};
   
      switch (flag) {		/*determine which filter to use*/
         case(1):
            filt_coeff = firls1;
            filt_length = 11;
            break;
      }
   
      npt_y = (n/(int) ldexp(1, dec));
   
   /* filter order; also equals the no. of data points needed in
    * the temporary data arrays to bridge over more than one
    * decimation call
    */
   
      filt_ord = 4*filt_length - 2;
   
   /* set up temporary data pointer on first function call:
    * initialize prev with filt_ord elements
    */
   
      if (prev == NULL)
         prev = calloc(filt_ord, sizeof(float));
   
   
   /* make a new data array with the previous filt_ord data
    *  points first, and the x data points next
    */
   
      data_length = filt_ord + n;
   
      data = calloc(data_length, sizeof(float));
      fold_data = calloc(filt_length + 1, sizeof(float));
   
      for (i = 0; i < filt_ord; ++i)   
         data[i] = (*((float*)prev + i));	      
      for (i = filt_ord; i < data_length; ++i)    
         data[i] = x[i - filt_ord];    
   
   
   /* filter algorithm; half-band, linear phase FIR filter;	*/
   /* taking advantage of the symmetry of the filter		*/
   /* coefficients, the appropriate data pairs are first added	*/
   /* to make the fold_data array, which is then multiplied by  */
   /* the filter coefficients					*/
   
      for (k = 0; k < dec; ++k)
      {
         for	 (i = 0; i < n/2; ++i)
         {	
            for (j = 0; j < filt_length; ++j)
               fold_data[j] = data[2*(i+j)] + data[filt_ord + 2*(i-j)];
         
            fold_data[filt_length] = data[filt_ord/2 + 2*i];
         
            sum = 0;
            for (j = 0; j < filt_length; ++j)
               sum += fold_data[j] * (*(filt_coeff +j));
         
            sum += fold_data[filt_length]/2;
            data[i] = sum;
         }
      }
   
      if (next == NULL)
         free(prev);
      else
      /* for next function call, set up array of last
       * filt_ord points of x, and point to it
       */
      {
         for (i = 0; i < filt_ord; ++i)
            *((float*)prev + i) = (*(x + (n - filt_ord + i)));
         *next = prev;
      }
   
      for (i = 0; i < npt_y; ++i)	/* copy the output values into y */
         y[i] = data[i];
   
      free(data);
      free(fold_data);
   
   }
#endif
