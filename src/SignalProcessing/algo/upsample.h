/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: upsample						*/
/*                                                         		*/
/* Module Description: Up samples a time series		       		*/
/*									*/
/*                                                         		*/
/* Module Arguments:					   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer   		Comments       	       		*/
/* 0.1   12/26/99 Daniel Sigg       	linear interpolation only	*/
/*                                                         		*/
/* Documentation References: 						*/
/*	Man Pages: doc++ generated html					*/
/*	References:							*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8336  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Unix						*/
/*	Compiler Used: Sun cc						*/
/*	Runtime environment: Solaris 					*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			TBD			*/
/*			  POSIX			TBD			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _UPSAMPLE_H
#define _UPSAMPLE_H

#ifdef __cplusplus
extern "C" {
#endif

/* Header File List: */

/**@name Up-sampling Algorithms
   This module is used to up sample time series data. Currently only
   step interpolation is supported.

   @memo Up sampling.
   @author Written Dec. 1999 by Daniel Sigg
   @version 1.0; DS; December 1999
*********************************************************************/

/*@{*/

/** @name Functions.
    @author DS, Dec 99
    @see Upsampling routiunes
*********************************************************************/

/*@{*/

/** The stepup function adds data points to a time series by 
    repeatedly inserting the previous sampled value in between
    original samples. i.e. zero-order-hold.

    @param flag unused for now, set to 0
    @param x input time series
    @param y output time series (return)
    @param n number of point in the input array
    @param up_factor up sampling factor
    @return 0 if successful, <0 otherwise

    @memo Up sampling of time series data
**********************************************************************/
int stepup( int flag, const float x[], float y[], int n, int up_factor );

/*@}*/

/*@}*/

#ifdef __cplusplus
}
#endif

#endif /*_UPSAMPLE_H */
