#!/usr/bin/env python3
from scipy.signal import firls, remez
import numpy as np
import sys
import matplotlib.pyplot as plot
from typing import Dict

def print_array(name: str, array: np.array) -> None:
    sys.stdout.write(f"static const double {name}[{array.size}] = \n")
    sys.stdout.write("{\t")
    for i in range(array.size):
        if(i > 0):
            sys.stdout.write(",")
            if(i % 3) == 0:
                sys.stdout.write("\n\t")
            else:
                sys.stdout.write(" ")
        sys.stdout.write("%0.20e" % array[i])
    sys.stdout.write("};\n")

def generate_firls_filter(order: int) -> np.array:
    assert order % 2 == 0, "order must be even"
    assert order % 4 != 0, "order must not be a multiple of 4"
    raw_fir = firls(order + 1, [0,0.45,0.55,1],[1,1,0,0])
    return raw_fir[0:order//2:2]

def generate_mclellan_parks_filter(order: int) -> np.array:
    """
    Generate a mclellan-parks filter using the method found here:

    https://tomverbeure.github.io/2020/12/15/Half-Band-Filters-A-Workhorse-of-Decimation-Filters.html

    This might be better, since the old method had pretty large values in the odd taps, which are supposed to be zero.

    :param order:
    :return:
    """
    assert order % 2 == 0, "order must be even"
    assert order % 4 != 0, "order must not be a multiple of 4"
    return remez(order//2 + 1, [0, 0.90, 1, 1], [1, 0], fs=2)[:(order//2+1)//2] / 2

def generate_mclellan_parks_filter_old(order: int) -> np.array:
    """
    Generate a mclellan-parks filter the old way. that is by taking every other tap.

    An examination of the full filter shows the taps that should be zero aren't even close to zero.

    :param order:
    :return:
    """
    assert order % 2 == 0, "order must be even"
    assert order % 4 != 0, "order must not be a multiple of 4"
    raw_fir = remez(order + 1, [0, 0.45, 0.55, 1], [1, 0], fs=2)
    return raw_fir[0:order//2:2]


def print_c_code(filts: Dict[str, np.array]) -> None:
    for name, filt in filts.items():
        print_array(name, filt)


def graph_filter(ax, filt, label: str):
    f = np.arange(start=0, stop=0.50, step=0.001)
    #print(f)
    result = np.array([0+0j]*f.size)
    arc_step = np.exp(-2*np.pi*f*(0+1j))
    #print(arc_step)
    for raw_i,h in enumerate(filt):
        i = raw_i*2
        k = filt.size * 4 - i - 2
        step = h * (arc_step ** i + arc_step ** k)
        #print(f"step {raw_i}, {i}, {k}")
        #print(step)
        result += step
    midstep = 0.5 * (arc_step ** (filt.size*2-1))
    #print("midstep")
    #print(midstep)
    result += midstep
    #print("result")
    #print(result)
    mag = np.abs(result)
    ax.plot(f, mag, label=label)
    print(filt)


def generate_filters() -> Dict[str, np.array]:
    return {
        "firls1": generate_firls_filter(42),
        "firPM1": generate_mclellan_parks_filter_old(42),
        "firls2": generate_firls_filter(22),
        "firls3": generate_firls_filter(82),
    }



def compare_pm_filters():
    filts = generate_filters()

    fig = plot.figure()
    ax = fig.add_subplot(2,1,1)
    ax.set_yscale('log')
    ax.grid(True)


    graph_filter(ax, filts['firPM1'], 'firPM1 new')
    graph_filter(ax, generate_mclellan_parks_filter_old(42), 'firPM1 old')

    ax.legend()

    plot.show()


def graph_all_filters():
    filts = generate_filters()
    fig = plot.figure()
    ax = fig.add_subplot(2,1,1)
    ax.set_yscale('log')
    ax.grid(True)
    for name, filt in filts.items():
        graph_filter(ax, filt, name)

    ax.legend()

    plot.show()


def print_all_c_code():
    filts = generate_filters()
    print_c_code(filts)


if __name__ == "__main__":
    print_all_c_code()