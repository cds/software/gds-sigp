/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: preproc							*/
/*                                                         		*/
/* Module Description: Preprocessing function		       		*/
/*		       includes: data collection, up sampling, 		*/
/*		       decimation, down-conversion, decimation,		*/
/*		       time shift, partitioning				*/
/*                                                         		*/
/* Module Arguments:					   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer   		Comments       	       		*/
/* 0.1   12/26/99 Daniel Sigg       	linear interpolation only	*/
/*                                                         		*/
/* Documentation References: 						*/
/*	Man Pages: doc++ generated html					*/
/*	References:							*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8336  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Unix						*/
/*	Compiler Used: Sun cc						*/
/*	Runtime environment: Solaris 					*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			TBD			*/
/*			  POSIX			TBD			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _PREPROC_H
#define _PREPROC_H

#ifdef __cplusplus
extern "C" {
#endif

/* Header File List: */

/**@name Data Preprocessing Algorithms
   This module is used to proprocess a time series. It includes
   the following stages: data collection, up-sampling, time delay,
   filter/decimation, down-conversion, filter/decimation, time shift
   and partitioning.

   @memo Preprocessing.
   @author Written Dec. 1999 by Daniel Sigg
   @version 1.0; DS; December 1999
  *********************************************************************/

/*@{*/

/** @name Types.
    @author DS, Dec 99
    @see Preprocessing data types
  *********************************************************************/

/*@{*/

#ifndef _TAINSEC_T
#define _TAINSEC_T
#include <inttypes.h>
/*typedef long long tainsec_t;*/
typedef int64_t       tainsec_t;
typedef unsigned long taisec_t;
#endif

/** Callback routine for output data. This function is called by the
    preprocessing routine whenever new data is available, 
    This structure is used as a parameter to the preprocess function.
    @param part partition number (starts at zero)
    @param index index into partition array
    @param x data array
    @param len length of data array
    @param user user data
    @return void
  *********************************************************************/
typedef void ( *PartitionDataOut )(
    int part, int index, const float x[], int len, void* user );

/** Preprocessing paramater structure. This structure should not be
    accessed directly.
    This structure is used as a parameter to the preprocess function.
  *********************************************************************/
struct preproc_t
{
    /** start time of preprocessing (nsec) */
    tainsec_t start;
    /** duration of preprocessing (nsec): -1 for infinite */
    tainsec_t duration;
    /** input data rate (Hz) */
    double datarate;
    /** spacing of data points (sec): original time series, after 
          up-sampling stage, after first decimation and after
          second decimation */
    double dt[ 4 ];
    /** up sampling factor */
    int upfactor;
    /** flag for decimation filter */
    int decimationflag;
    /** first decimation factor */
    int decimate1;
    /** second decimation factor */
    int decimate2;
    /** time zero for down-conversion (nsec) */
    tainsec_t zoomstart;
    /** frequency of down-conversion (Hz) */
    double zoomfreq;
    /** decimation delay (in sec) */
    double decdelay;
    /** number of taps in the delay filter */
    int delaytaps;
    /** delay shift (in nsec) */
    tainsec_t delayshift;
    /** accumulated delay after first decimation stage (in nsec) */
    tainsec_t delay1;
    /** duration of output partitions */
    tainsec_t tpart;
    /** time spacing of output partitions */
    tainsec_t dtpart;
    /** data output callback function */
    PartitionDataOut write;
    /** buffer for holding data segments which are too small */
    float* buf;
    /** buffer time */
    tainsec_t bufTime;
    /** size of data in buffer */
    int bufSize;
    /** temporary storage for delay filter */
    float* tmpdelay1;
    /** temporary storage for 1st decimation filter */
    float* tmpdec1;
    /** temporary storage for 2nd decimation filter, I phase */
    float* tmpdec2I;
    /** temporary storage for 2nd decimation filter, Q phase */
    float* tmpdec2Q;
    /** user data pointer */
    void* user;
};
typedef struct preproc_t preproc_t;

/*@}*/

/** @name Functions.
    @author DS, Dec 99
    @see Preprocessing routiunes
  *********************************************************************/

/*@{*/

/** This function initializes the preprocessing parameters.
       
    @memo initialize preprocessing parameters
    @param start start time (GPS nsec)
    @param duration duration (nsec)
    @param inDatarate input data rate (Hz)
    @param outDatarate output data rate (Hz)
    @param fdown down-conversion frequency
    @param tpart length of output partitions (nsec)
    @param dtpart time spacing of output partitions (nsec)
    @param tpart length of output partitions (nsec)
    @param write callback function for output data
    @param user user data for callback
    @return preprocessing structure if successful, NULL otherwise
  **********************************************************************/
preproc_t* initPreproc( tainsec_t        start,
                        tainsec_t        duration,
                        double           inDatarate,
                        double           outDatarate,
                        double           fdown,
                        tainsec_t        tpart,
                        tainsec_t        dtpart,
                        PartitionDataOut write,
                        void*            user );

/** This function frees the preprocessing parameter structure.
       
    @memo Free preprocessing parameters
    @param pre preprocessing structure
    @return void
  **********************************************************************/
void freePreproc( preproc_t* pre );

/** This function preprocesses a time series.
    Currently only data rates which are a power of 2 are supported.
    Missing data points are filled in with zeros.
    @memo preprocessing function
    @param pre preprocessing structure
    @param time time of first data point
    @param data input data array
    @param length length of input data array
    @return 0 if successful, <0 otherwise
  **********************************************************************/
int
preprocess( preproc_t* pre, tainsec_t time, const float data[], int length );

/*@}*/

/*@}*/

#ifdef __cplusplus
}
#endif

#endif /*_PREPROC_H */
