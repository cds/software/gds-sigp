/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: algorithm						*/
/*                                                         		*/
/* Module Description: Inlcude file for gds algorith libraries		*/
/*									*/
/*                                                         		*/
/* Module Arguments:					   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date    Programmer   Comments			   		*/
/* 0.1	 17Jun99 Daniel Sigg  separate header/object			*/
/*                                                         		*/
/* Documentation References: 						*/
/*	Man Pages: doc++ generated html					*/
/*	References:							*/
/*                                                         		*/
/* Author Information:							*/
/*   Name         Telephone       Fax             e-mail 		*/
/*   Daniel Sigg  (509) 372 8132  (509) 372 8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Unix,	vxWorks/Baja47				*/
/*	Compiler Used: Sun cc, gcc					*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			TBD			*/
/*			  POSIX			OK			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_ALGORITHM_H
#define _GDS_ALGORITHM_H

#ifdef __cplusplus
extern "C" {
#endif

/* Header File List: */
#include "gdstype.h"
#include "gdsconst.h"
#include "gdssigproc.h"
#include "gdsrand.h"
#include "decimate.h"
#include "upsample.h"
#include "preproc.h"
#include "avgmodule.h"
#include "window.h"
#include "fftmodule.h"
#include "sineanalyze.h"

/** @name Algorithm Libraries

    This header includes the complete set of gds algorithm libraries.
    \begin{verbatim}
    #include "gdstype.h"	Common defines and types
    #include "gdsconst.h"	Numerical and physical constants
    #include "gdssigproc.h"	Basic signal processing routines
    #include "gdsrand.h"	Random number generator
    #include "decimate.h"	Filter/decimation routines
    #include "upsample.h"	Up sampling routines
    #include "preproc.h"	Preprocessing routines
    #include "avgmodule.h"	Averaging routines
    #include "window.h"		Data windowing algorithms
    #include "fftmodule.h"	FFT related algorithms
    #include "sineanalyze.h"	Sine detection routines
    \end{verbatim}

    @memo Main algorithm library.
    @author DS, June 1999
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /*_GDS_ALGORITHM_H */
