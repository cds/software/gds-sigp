/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: gdstype				        		*/
/*                                                         		*/
/* Module Description: Common types and defines of the algorithm lib.	*/
/*									*/
/* Module Arguments: none                				*/
/*									*/
/* Revision History:					   		*/
/* Rel   Date     Programmer   		Comments       	       		*/
/* 0.1   17June99 Daniel Sigg            				*/
/*                                                                      */
/* Documentation References: 						*/
/*	Man Pages: doc++ generated html					*/
/*	References: 							*/
/*                                                         		*/
/* Author Information:							*/
/*	Name	Telephone    Fax          e-mail 			*/
/*	E. Daw  617-258-7697 617-253-7014 edaw@ligo.mit.edu		*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Unix						*/
/*	Compiler Used: Sun Sparcworks C compiler         		*/
/*	Runtime environment: UNIX    					*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	TBD			*/
/*			  Lint.			TBD			*/
/*			  ANSI			TBD			*/
/*			  POSIX			TBD			*/
/*									*/
/* Known Bugs, Limitations, Caveats:                       		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_TYPE_H
#define _GDS_TYPE_H

#ifdef __cplusplus
extern "C" {
#endif

/**@name Common types of the algorithm library
   This header defines common types of the gds algorithm library.

   @memo Common types and defines.
   @author Written June 1999 by Daniel Sigg
   @version 0.1
  ***********************************************************************/

/*@{*/

/** @name Complex data types.
    Complex data types of the signal processing functions.

    @author PF, Jan 99
    @see 
   **********************************************************************/

/*@{*/

/** Single precision complex number.
    Can be cast into a complex<float> in C++.
   **********************************************************************/
struct sCmplx
{
    /** Real part */
    float re;
    /** Imaginary part */
    float im;
};
typedef struct sCmplx sCmplx;

/** Double precision complex number.
    Can be cast into a complex<double> in C++.
   **********************************************************************/
struct dCmplx
{
    /** Real part */
    double re;
    /** Imaginary part */
    double im;
};
typedef struct dCmplx dCmplx;

/*@}*/

/*@}*/

#ifdef __cplusplus
}
#endif

#endif /* _GDS_TYPE_H */
