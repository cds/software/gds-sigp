/****************************************************************
 *								*
 *  Module Name: Decimate-by-2 function				*
 *								*
 *  Procedure Description: decimates an input data stream	*
 *	by a factor of 2, using a half-band FIR filter to	*
 *	perform anti-aliasing 					*
 *								*	
 *  External Procedure Name: decimate				*
 *								*
 *  Procedure Arguments: flag for determining filter type;	*
 *	pointers to input and output vectors; number of		*
 *	input points; number of serial decimation stages;	*
 *	pointer to data from previous block; pointer to 	*
 *	data for next block					*
 *								*
 *								*
 ****************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "gdsconst.h"
#include "decimate.h"
#include "gdssigproc.h"


// created by 'generate_decimation_filters.py'
// these are double precisions filters that match closely to the original single precision filters.
static const double firls1[11] =
    {       2.22554904691202315448e-03, -3.91421695770125931818e-03, 6.22665357252368163732e-03,
      -9.33033087251471798806e-03, 1.34747780128723956133e-02, -1.90746224371436054468e-02,
      2.69052629568931472859e-02, -3.86352438908939069262e-02, 5.86362435989722860508e-02,
      -1.03029758712481767091e-01, 3.17275548508645810486e-01};
static const double firPM1[11] =
    {       5.70494360354520824857e-03, -5.29224585348962543729e-03, 7.67297283803256187362e-03,
      -1.08395826732333080272e-02, 1.49376797801889695666e-02, -2.04747250344783672527e-02,
      2.81300262651430661720e-02, -3.96782627672148946463e-02, 5.93996840026398825740e-02,
      -1.03522026124540519887e-01, 3.17427832894289385646e-01};
static const double firls2[6] =
    {       -1.35343139967720339084e-02, 2.19369141779353904465e-02, -3.44831974238690719403e-02,
      5.55089938517470057877e-02, -1.01086644479057921608e-01, 3.16616495440901646319e-01};
static const double firls3[21] =
    {       8.54931013620596449076e-05, -1.88228899893453917270e-04, 3.48320897889317746161e-04,
      -5.83580993296865433405e-04, 9.14678779430507914866e-04, -1.36536883869076012057e-03,
      1.96287916744361350119e-03, -2.73860067225829232479e-03, 3.72930179810362916193e-03,
      -4.97923747947023615651e-03, 6.54378649582754330638e-03, -8.49576317005643934233e-03,
      1.09366003936409023067e-02, -1.40169123164776126406e-02, 1.79764583465630185810e-02,
      -2.32280702457226748137e-02, 3.05537341895249989210e-02, -4.16368574367429467031e-02,
      6.08716290111040794764e-02, -1.04408588098128848887e-01, 3.17741531439199031972e-01};

/// These are the old single precision versions of the filters for reference only.
//// strange SUN C++ bug: do not define const or static
//float firls1[ 11 ] = { 2.225549e-3, -3.914217e-3, 6.226653e-3, -9.330331e-3,
//                       1.347478e-2, -1.907462e-2, 2.690526e-2, -3.863524e-2,
//                       5.863624e-2, -1.030298e-1, 3.172755e-1 };
//float firPM1[ 11 ] = { 5.704943e-3, -5.292245e-3, 7.672972e-3, -1.083958e-2,
//                       1.493768e-2, -2.047472e-2, 2.813003e-2, -3.967826e-2,
//                       5.939968e-2, -1.035220e-1, 3.174278e-1 };
//float firls2[ 6 ] = { -1.353431e-2, 2.193691e-2,  -3.448320e-2,
//                      5.550899e-2,  -1.010866e-1, 3.166165e-1 };
//float firls3[ 21 ] = { 8.549310e-5, -1.882289e-4, 3.483209e-4, -5.835810e-4,
//                       9.146788e-4, -1.365369e-3, 1.962879e-3, -2.738601e-3,
//                       3.729302e-3, -4.979237e-3, 6.543786e-3, -8.495763e-3,
//                       1.093660e-2, -1.401691e-2, 1.797646e-2, -2.322807e-2,
//                       3.055373e-2, -4.163686e-2, 6.087163e-2, -1.044086e-1,
//                       3.177415e-1 };

int
decimate( int         flag,
          const double x[],
          double       y[],
          int         n,
          int         dec_factor,
          double*      prev,
          double**     next )
{
    double       mantissa;
    const double* filt_coeff;
    double        sum;
    double*       dataPtr;
    int          filt_length;
    int          tempnpt;
    int          npt;
    int          filt_ord;
    int          i;
    int          j;
    int          k;
    int          npt_y;
    int          ndec;

    switch ( flag )
    { /*determine which filter to use*/
    case 2: {
        filt_coeff = firPM1;
        filt_length = 11;
        break;
    }
    case 3: {
        filt_coeff = firls2;
        filt_length = 6;
        break;
    }
    case 4: {
        filt_coeff = firls3;
        filt_length = 21;
        break;
    }
    case 1:
    default: {
        filt_coeff = firls1;
        filt_length = 11;
        break;
    }
    }

    /*  check that dec_factor is a power of 2; return error if not */
    if ( ( mantissa = frexp( (double)dec_factor, &ndec ) ) != 0.5 )
        return ( -1 );
    --ndec; /* this makes 2^(ndec) = dec_factor   */
    npt_y = ( n / dec_factor ); /* # pts in the output array   */
    filt_ord = 4 * filt_length - 2; /* filter order  */
    tempnpt = ndec * filt_ord; /* #pts needed in temp array to */
    /* bridge over multiple calls   */

    /* set up temporary data pointer on first function call: */
    /* initialize prev with tempnpt elements                 */

    if ( prev == NULL )
    {
        prev = calloc( tempnpt, sizeof( double ) );
        memset( prev, 0, tempnpt * sizeof( double ) );
    }

    /* make a new data array with the previous tempnpt data      */
    /* points first, and the x data points next; then copy last  */
    /* tempnpt points to prev, for next call                     */

    dataPtr = malloc( ( n + tempnpt ) * sizeof( double ) );
    memcpy( dataPtr, prev, sizeof( double ) * tempnpt );
    memcpy( ( dataPtr + tempnpt ), x, sizeof( double ) * n );

    /* filter algorithm; half-band, linear phase FIR filter;	*/
    /* taking advantage of the symmetry of the filter		*/
    /* coefficients, the appropriate data pairs are added	*/
    /* together, then multiplied by the filter coefficients	*/

    /*   printf("data shuffling done\n"); */
    npt = n;
    dataPtr += tempnpt;
    for ( k = 1; k <= ndec; ++k )
    {
        dataPtr -= filt_ord; /* start at data from previous call  */
        if ( next != NULL )
        {
            memcpy( ( prev + tempnpt - k * filt_ord ),
                    ( dataPtr + npt ),
                    sizeof( double ) * filt_ord );
        }
        npt /= 2; /* decrease #pts by factor of 2 at each stage */
        for ( i = 0; i < npt; ++i )
        {
            sum = 0.0;
            for ( j = 0; j < filt_length; j++ )
            {
                sum += filt_coeff[ j ] *
                    ( dataPtr[ 2 * ( i + j ) ] +
                      dataPtr[ filt_ord + 2 * ( i - j ) ] );
            }
            dataPtr[ i ] =
                (double)( sum + dataPtr[ filt_ord / 2 + 2 * i ] / 2.0 );
        }
    }

    /* copy the output values into y */
    memcpy( y, dataPtr, sizeof( double ) * npt_y );

    if ( next == NULL )
        free( prev ); /*  cleanup  */
    else
        *next = prev;
    free( dataPtr );
    return 0;
}

double
firphase( int flag, int dec_factor )
{

    double phasefactor;
    int    filt_length;
    int    filt_ord;

    switch ( flag )
    { /*determine which filter to use*/
    case ( 1 ):
        filt_length = 11;
        break;
    case ( 2 ):
        filt_length = 11;
        break;
    case ( 3 ):
        filt_length = 6;
        break;
    case ( 4 ):
        filt_length = 21;
        break;
    default:
        filt_length = 11;
        break;
    }

    filt_ord = 4 * filt_length - 2; /* filter order  */
    phasefactor = PI * ( dec_factor - 1 ) * filt_ord;
    return phasefactor;
}

int
decimationFilterName( int flag, char* type, int size )
{
    char buf[ 256 ];

    switch ( flag )
    {
    case 2: {
        sprintf( buf,
                 "FIR (equiripple): order=%i fR=%f "
                 "pass. ripple=%f-%f dB stopband attn.=%i-%i dB",
                 42,
                 0.9,
                 0.05,
                 0.05,
                 43,
                 43 );
        break;
    }
    case 3: {
        sprintf( buf,
                 "FIR (least-squares): order=%i fR=%f "
                 "pass. ripple=%f-%f dB stopband attn.=%i-%i dB",
                 22,
                 0.9,
                 0.1,
                 0.8,
                 30,
                 40 );
        break;
    }
    case 4: {
        sprintf( buf,
                 "FIR (least-squares): order=%i fR=%f "
                 "pass. ripple=%f-%f dB stopband attn.=%i-%i dB",
                 82,
                 0.9,
                 0.0006,
                 0.01,
                 60,
                 90 );
        break;
    }
    case 1:
    default: {
        sprintf( buf,
                 "FIR (least-squares): order=%i fR=%f "
                 "pass. ripple=%f-%f dB stopband attn.=%i-%i dB",
                 42,
                 0.9,
                 0.02,
                 0.1,
                 40,
                 56 );
        break;
    }
    }
    strncpy( type, buf, size );
    return 0;
}

int
timedelay(
    const double x[], double y[], int n, int delay, double* prev, double** next )
{
    int size; /* copy size */

    /* check array arguments */
    if ( ( n > 0 ) && ( ( x == NULL ) || ( y == NULL ) ) )
    {
        return -1;
    }

    /* handle special cases */
    if ( delay < 0 )
    {
        /* error */
    }
    else if ( delay == 0 )
    {
        if ( ( n > 0 ) && ( x != y ) )
        {
            memcpy( y, x, n * sizeof( double ) );
        }
    }
    else
    { /* delay > 0 */

        /* set up temporary data pointer on first function call */
        if ( prev == NULL )
        {
            prev = calloc( 2 * delay, sizeof( double ) );
            if ( prev == NULL )
            {
                return -1;
            }
            memset( prev, 0, 2 * delay * sizeof( double ) );
        }

        /* copy things around */
        if ( n > 0 )
        {
            size = ( n < delay ) ? n : delay;
            /* remove last size samples from input */
            memcpy( prev + delay, x + n - size, size * sizeof( double ) );
            /* copy rest of input to back of output */
            if ( n > delay )
            {
                memmove( y + size, x, ( n - size ) * sizeof( double ) );
            }
            /* copy front of delay buffer to front of output */
            memcpy( y, prev, size * sizeof( double ) );
            /* move rest of data in delay buffer upfront */
            memmove( prev, prev + size, delay * sizeof( double ) );
        }
    }

    /* cleanup */
    if ( next == NULL )
    {
        free( prev );
    }
    else
    {
        *next = prev;
    }
    return 0;
}
