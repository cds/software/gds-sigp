/* implement wrapped iterative correlation */
/* Ed Daw, 8th June 2008                   */
/* added unwrapped iterative correlation   */
/* Ed Daw, 2nd September 2008              */

#include <rtcor.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>

int
rtcor_constructor( int        n_lags,
                   double*    initdatax,
                   double*    initdatay,
                   rtcordata* pdata )
{
    int    nbytes;
    int    corrcount;
    int    lagcount;
    double corraccum;

    /* allocate memory to internal data arrays */
    pdata->nlags = n_lags;
    pdata->xdata = calloc( n_lags, sizeof( double ) );
    pdata->ydata = calloc( n_lags, sizeof( double ) );
    /* set initial contents of x and y data arrays */
    nbytes = n_lags * sizeof( double );
    memcpy( (void*)pdata->xdata, (void*)initdatax, nbytes );
    memcpy( (void*)pdata->ydata, (void*)initdatay, nbytes );
    /* allocate memory for correlation lag data */
    pdata->corrmem = calloc( n_lags, sizeof( double ) );
    /* perform initial correlation in time domain */
    for ( lagcount = 0; lagcount < n_lags; ++lagcount )
    {
        corraccum = 0;
        for ( corrcount = 0; corrcount < n_lags; ++corrcount )
        {
            corraccum += initdatax[ corrcount ] *
                initdatay[ ( corrcount + lagcount ) % n_lags ];
        }
        pdata->corrmem[ lagcount ] = corraccum;
    }
    /* set counter to zero */
    pdata->counter = 0;
    return 0;
}

int
rtcor_destructor( rtcordata* pdata )
{
    free( pdata->xdata );
    free( pdata->ydata );
    free( pdata->corrmem );
    return 0;
}

int
rtcor_iterate( double xsample, double ysample, rtcordata* pdata )
{
    int i;
    for ( i = 0; i < pdata->nlags; ++i )
    {
        double xend;
        double oldcorr;
        if ( i == 0 )
        {
            xend = xsample;
        }
        else
        {
            xend = pdata->xdata[ ( ( pdata->counter ) + ( pdata->nlags ) - i ) %
                                 ( pdata->nlags ) ];
        }
        oldcorr = pdata->corrmem[ i ];
        pdata->corrmem[ i ] = oldcorr +
            ( xsample - ( pdata->xdata[ pdata->counter ] ) ) *
                pdata->ydata[ ( ( pdata->counter ) + i ) % ( pdata->nlags ) ] +
            ( ysample - ( pdata->ydata[ pdata->counter ] ) ) * xend;
    }
    pdata->xdata[ pdata->counter ] = xsample;
    pdata->ydata[ pdata->counter ] = ysample;
    pdata->counter = ( pdata->counter + 1 ) % ( pdata->nlags );
    return 0;
}

int
rtcor_copy( double* destination, rtcordata* pdata )
{
    unsigned int nbytes;
    nbytes = pdata->nlags * sizeof( double );
    memcpy( (void*)destination, (void*)pdata->corrmem, nbytes );
    return 0;
}

double*
rtcor_refdata( rtcordata* pdata )
{
    return pdata->corrmem;
}

int
rtcor_dump( rtcordata* pdata )
{
    int bc;
    printf( "counter set to %d.\n", pdata->counter );
    printf( "x buffer:\n" );
    for ( bc = 0; bc < pdata->nlags; ++bc )
    {
        printf( "%d\t%f\n", bc, pdata->xdata[ bc ] );
    }
    printf( "y buffer:\n" );
    for ( bc = 0; bc < pdata->nlags; ++bc )
    {
        printf( "%d\t%f\n", bc, pdata->ydata[ bc ] );
    }
    printf( "correlation buffer:\n" );
    for ( bc = 0; bc < pdata->nlags; ++bc )
    {
        printf( "%d\t%f\n", bc, pdata->corrmem[ bc ] );
    }
    return 0;
}

int
urtcor_constructor( int        n_cor,
                    int        n_lags,
                    double*    initdatax,
                    double*    initdatay,
                    rtcordata* pdata )
{
    int    nbytesx;
    int    nbytesxend;
    int    nbytesy;
    int    n_end;
    int    corrcount;
    int    lagcount;
    double corraccum;

    /* calculate number of data points between end of x data and end of y */
    /* data */
    n_end = (int)( ( n_lags - 1 ) / 2 );
    /* allocate memory to internal data arrays*/
    pdata->nlags = n_lags;
    pdata->ncor = n_cor;
    pdata->nbinsy = n_cor + n_lags - 1;
    pdata->xdata = calloc( n_cor, sizeof( double ) );
    pdata->ydata = calloc( n_cor + n_lags - 1, sizeof( double ) );
    pdata->xenddata = calloc( n_end, sizeof( double ) );
    /* set initial contents of x and y data arrays, noting that the y */
    /* data array is longer than the x one by n_lags-1 */
    nbytesx = n_cor * sizeof( double );
    nbytesxend = n_end * sizeof( double );
    nbytesy = ( n_cor + n_lags - 1 ) * sizeof( double );
    memcpy( (void*)pdata->xdata, (void*)( initdatax + n_end ), nbytesx );
    memcpy( (void*)pdata->ydata, (void*)initdatay, nbytesy );
    memcpy( (void*)pdata->xenddata,
            (void*)( initdatax + n_end + n_cor ),
            nbytesxend );
    /* allocate memory for correlation lag data */
    pdata->corrmem = calloc( n_lags, sizeof( double ) );
    /* perform initial correlation in the time domain */
    for ( lagcount = 0; lagcount < n_lags; ++lagcount )
    {
        corraccum = 0;
        for ( corrcount = 0; corrcount < n_cor; ++corrcount )
        {
            corraccum += ( pdata->xdata[ corrcount ] *
                           pdata->ydata[ corrcount + lagcount ] );
        }
        pdata->corrmem[ lagcount ] = corraccum;
    }
    /* set counters to zero */
    pdata->counter = 0;
    pdata->ycounter = 0;
    pdata->xendcounter = 0;
    pdata->nxend = n_end;
    return 0;
}

int
urtcor_destructor( rtcordata* pdata )
{
    free( pdata->xenddata );
    return rtcor_destructor( pdata );
}

int
urtcor_iterate( double xsample, double ysample, rtcordata* pdata )
{
    int    i;
    double xsamplefeed;
    double ysamplefeed;
    double oldcorr;
    /* apply iteration algorithm for all lags except for the rightmost lag in y */
    xsamplefeed = pdata->xenddata[ pdata->xendcounter ];
    for ( i = 0; i < pdata->nlags - 1; ++i )
    {
        oldcorr = pdata->corrmem[ i ];
        ysamplefeed = pdata->ydata[ ( ( pdata->ycounter ) + i + pdata->ncor ) %
                                    ( pdata->nbinsy ) ];
        pdata->corrmem[ i ] = oldcorr + xsamplefeed * ysamplefeed -
            pdata->xdata[ pdata->counter ] *
                pdata->ydata[ ( ( pdata->ycounter ) + i ) % ( pdata->nbinsy ) ];
    }
    /* the rightmost lag in y that introduces the new y sample */
    oldcorr = pdata->corrmem[ pdata->nlags - 1 ];
    ysamplefeed = ysample;
    pdata->corrmem[ pdata->nlags - 1 ] = oldcorr + xsamplefeed * ysamplefeed -
        pdata->xdata[ pdata->counter ] *
            pdata->ydata[ ( ( pdata->ycounter ) + ( pdata->nlags - 1 ) ) %
                          ( pdata->nbinsy ) ];
    /* update the sample buffers for the x, xend and y data buffers */
    pdata->xdata[ pdata->counter ] = xsamplefeed;
    pdata->xenddata[ pdata->xendcounter ] = xsample;
    pdata->ydata[ pdata->ycounter ] = ysample;
    /* update the counters for the x, xend and y data buffers */
    pdata->counter = ( pdata->counter + 1 ) % ( pdata->ncor );
    pdata->ycounter = ( pdata->ycounter + 1 ) % ( pdata->nbinsy );
    pdata->xendcounter = ( pdata->xendcounter + 1 ) % ( pdata->nxend );
    return 0;
}

int
urtcor_copy( double* destination, rtcordata* pdata )
{
    unsigned int nbytes;
    nbytes = pdata->nlags * sizeof( double );
    memcpy( (void*)destination, (void*)pdata->corrmem, nbytes );
    return 0;
}

int
urtcor_dump( rtcordata* pdata )
{
    int bc;
    printf( "x counter set to %d.\n", pdata->counter );
    printf( "x buffer:\n" );
    for ( bc = 0; bc < pdata->ncor; ++bc )
    {
        printf( "%d\t%f\n", bc, pdata->xdata[ bc ] );
    }
    printf( "x end buffer:\n" );
    for ( bc = 0; bc < pdata->nxend; ++bc )
    {
        printf( "%d\t%f\n", bc, pdata->xenddata[ bc ] );
    }
    printf( "y buffer:\n" );
    for ( bc = 0; bc < pdata->nbinsy; ++bc )
    {
        printf( "%d\t%f\n", bc, pdata->ydata[ bc ] );
    }
    printf( "correlation buffer:\n" );
    for ( bc = 0; bc < pdata->nlags; ++bc )
    {
        printf( "%d\t%f\n", bc, pdata->corrmem[ bc ] );
    }

    return 0;
}

int
rtcor_copystruct( rtcordata* ptarget, rtcordata data )
{
    ptarget->nlags = data.nlags;
    ptarget->xdata = calloc( data.nlags, sizeof( double ) );
    ptarget->ydata = calloc( data.nlags, sizeof( double ) );
    ptarget->corrmem = calloc( data.nlags, sizeof( double ) );
    memcpy( (void*)ptarget->xdata,
            (void*)data.xdata,
            data.nlags * sizeof( double ) );
    memcpy( (void*)ptarget->ydata,
            (void*)data.ydata,
            data.nlags * sizeof( double ) );
    memcpy( (void*)ptarget->corrmem,
            (void*)data.corrmem,
            data.nlags * sizeof( double ) );
    ptarget->counter = data.counter;
    return 0;
}

int
urtcor_copystruct( rtcordata* ptarget, rtcordata data )
{
    int n_end;
    ptarget->nlags = data.nlags;
    ptarget->ncor = data.ncor;
    ptarget->nbinsy = data.nbinsy;
    ptarget->xdata = calloc( data.nlags, sizeof( double ) );
    ptarget->ydata = calloc( data.nbinsy, sizeof( double ) );
    n_end = (int)( ( data.nlags - 1 ) / 2 );
    ptarget->xenddata = calloc( n_end, sizeof( double ) );
    memcpy( (void*)ptarget->xdata,
            (void*)data.xdata,
            data.nlags * sizeof( double ) );
    memcpy( (void*)ptarget->ydata,
            (void*)data.ydata,
            data.nbinsy * sizeof( double ) );
    memcpy( (void*)ptarget->xenddata,
            (void*)data.xenddata,
            n_end * sizeof( double ) );
    ptarget->corrmem = calloc( data.nlags, sizeof( double ) );
    memcpy( (void*)ptarget->corrmem,
            (void*)data.corrmem,
            data.nlags * sizeof( double ) );
    ptarget->counter = data.counter;
    ptarget->ycounter = data.ycounter;
    ptarget->xendcounter = data.xendcounter;
    ptarget->nxend = n_end;
    return 0;
}
