#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "gdsconst.h"
#include "sineanalyze.h"

#define NAVG 2
#define NFREQ 100
#define FL 3.8
#define FU 3.91

int
main( void )
{

    dCmplx sineAmp;
    int    npt;
    int    i;
    int    j;
    double fs;
    double fsine;
    double t_integ;
    float* x;
    dCmplx sineVals[ NAVG ];
    FILE*  ofp;

    ofp = fopen( "sstest.txt", "w" );

    /* make a sine wave  */
    fs = 16384.;
    for ( j = 0; j < NFREQ; ++j )
    {
        fsine = pow( 10.0, j * ( FU - FL ) / ( NFREQ - 1 ) + FL );
        fsine = j / 1.01010101;
        t_integ =
            fsine > 1.0 ? 100 / fsine : 100; /* 100 cycles of the sine wave */
        npt = NAVG * sweptSineNpts( t_integ, fs );

        printf( "number of points = %d\n", npt );
        x = malloc( npt * sizeof( float ) );

        for ( i = 0; i < npt; ++i )
        {
            x[ i ] = (float)( sin( i * 2 * PI * fsine / fs ) );
        }

        sineAnalyze( 0, 0, x, npt, fs, fsine, NAVG, 0.0, &sineAmp, sineVals );
        free( x );
        fprintf( ofp, "%g\t%g\t%g\n", fsine, sineAmp.re, sineAmp.im );
    }

    printf( "Real = %g\n", sineAmp.re );
    printf( "Imaginary = %g\n", sineAmp.im );

    return 0;
}
