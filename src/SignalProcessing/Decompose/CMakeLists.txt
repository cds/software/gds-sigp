
add_library(Decompose OBJECT
    Decompose.cc
)

target_include_directories(Decompose PRIVATE
    ${CMAKE_SOURCE_DIR}/src/SignalProcessing/FilterBase
    ${CMAKE_SOURCE_DIR}/src/SignalProcessing/Window

    ${CMAKE_SOURCE_DIR}/src/Base/complex
    ${CMAKE_SOURCE_DIR}/src/Base/misc
    ${CMAKE_SOURCE_DIR}/src/Base/thread
    ${CMAKE_SOURCE_DIR}/src/Base/time

    ${CMAKE_SOURCE_DIR}/src/Containers
    ${CMAKE_SOURCE_DIR}/src/Containers/CWVec
    ${CMAKE_SOURCE_DIR}/src/Containers/DVector
    ${CMAKE_SOURCE_DIR}/src/Containers/FilterIO
    ${CMAKE_SOURCE_DIR}/src/Containers/fSeries
    ${CMAKE_SOURCE_DIR}/src/Containers/TSeries

    ${CMAKE_SOURCE_DIR}/src/Math/ltmatrix
)

install(FILES
    Decompose.hh
    DESTINATION include/gds-sigp
)
