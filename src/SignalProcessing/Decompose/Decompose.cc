/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "Decompose.hh"
#include "dv_function.hh"
#include "fSeries/DFT.hh"
#include "fSeries/PSD.hh"
#include "LTMatrix.hh"
#include "Tukey.hh"
#include "Hanning.hh"
#include "lcl_array.hh"
#include <list>

using namespace std;
using namespace containers;

//======================================  Default constructor.
Decompose::Decompose( const KeyChain& chain )
    : _in_chain( chain ), _out_chain( chain ), _out_IO( _out_chain )
{
    size_t N = chain.size( );
    _target_window.set( Tukey( 0.1 ) );
    _chanfilt.reserve( N - 1 );
    KeyChain::const_iterator aux0 = chain.begin( );
    aux0++;
    for ( KeyChain::const_iterator i = aux0; i != chain.end( ); i++ )
    {
        _chanfilt.push_back( i->describe( ) );
    }
}

//======================================  Destructor.
Decompose::~Decompose( )
{
}

//======================================  Clone a filter.
Decompose*
Decompose::clone( void ) const
{
    return new Decompose( *this );
}

//======================================  Apply the filter
FilterIO&
Decompose::apply( const FilterIO& in )
{
    ;
}

//======================================  Clone a filter.
void
Decompose::apply( const io_vect& in, io_vect& out )
{
    size_t N = in.size( );
    Time   vStart = in[ 0 ].getStartTime( ) - _seglen;
    double target_maxf = 0.5 / double( in[ 0 ].getTStep( ) );
    out.resize( N );
    out[ 0 ] = in[ 0 ];
    for ( size_t i = 1; i < N; i++ )
    {
        chanfilt& chan( _chanfilt[ i - 1 ] );
        if ( chan._history.empty( ) )
        {
            DVectD* dvd = new DVectD( chan._order );
            dvd->replace_with_zeros( 0, chan._order, chan._order );
            chan._history.setData( vStart, in[ i ].getTStep( ), dvd );
        }
        if ( chan._history.Append( in[ i ] ) )
            throw runtime_error( "Decompose: Time error" );
        Interval chan_dt = chan._history.getTStep( );

        //--------------------------------  Get the input dft
        DFT     data( chan._history );
        DVectD* cofs = new DVectD( chan._fir_coefs );
        cofs->Extend( chan._history.getNSample( ) );
        TSeries cof_ts( vStart, chan_dt, cofs );
        DFT     cof_dft( cof_ts );
        data *= cof_dft;
        data.extend( target_maxf );
        out[ i ] = data.iFFT( );
        out[ 0 ] -= out[ i ];
    }
}

void
Decompose::dataCheck( const FilterIO& in ) const
{
    return;
}

void
Decompose::reset( void )
{
    return;
}

//======================================  Hann weighted average DFT bins
DFT
average_dft_bins( const containers::DFT& dft, double dF )
{
    const DVector& indv = dft.refDVect( );
    size_t         nAvg = size_t( dF / dft.getFStep( ) + 0.5 );
    size_t         lAvg = 2 * nAvg;
    size_t         nSeg = dft.getHighFreq( ) / dF;
    Hanning        w( lAvg );
    DVectW         dvw( nSeg );
    dvw[ 0 ] = dComplex( 0 );
    size_t inx0 = 0;
    for ( size_t i = 1; i < nSeg - 1; i++ )
    {
        unique_ptr< DVector > segdv( indv.Extract( inx0, lAvg ) );
        inx0 += nAvg;
        *segdv *= w.refDVect( );
        dvw[ i ] = segdv->CSum( 0, lAvg ) / lAvg;
    }
    dvw[ nSeg - 1 ] = 0.0;
    DFT out;
    static_cast< fSeries& >( out ) =
        fSeries( 0, dF, dft.getStartTime( ), dft.getDt( ), dvw );
    return out;
}

//======================================  Calculate transfer functions
void
Decompose::train( const io_vect& in, Interval seg_len )
{
    size_t N = in.size( );
    if ( N < 2 )
        throw runtime_error( "Decompose needs at least two inputs" );
    const TSeries& target( in[ 0 ] );

    size_t  nAux = N - 1;
    io_vect resampled( nAux );
    _par_vect.resize( nAux );

    //-----------------------------------  Get target parameters
    Interval tLength = target.getInterval( );
    Time     t0 = target.getStartTime( );
    size_t   nSeg = size_t( tLength / seg_len );
    if ( tLength != seg_len * double( nSeg ) )
    {
        throw logic_error(
            "Decompose: training length not a multiple of segment" );
    }

    //-----------------------------------  Get target window.
    containers::DFT target_dft( target );
    double          target_maxf = target_dft.getHighFreq( );

    //-----------------------------------  Loop over aux channels, calculate tf
    size_t              mtx_el = 0;
    lcl_array< double > covcoefs( N * ( N - 1 ) / 2 );
    for ( size_t iaux = 1; iaux < N; iaux++ )
    {
        chanfilt&      chan( _chanfilt[ iaux - 1 ] );
        const TSeries& aux_ts( in[ iaux ] );
        if ( aux_ts.getStartTime( ) != t0 || aux_ts.getInterval( ) != tLength )
        {
            throw runtime_error(
                "Decompose: Aux channel times do not match target" );
        }
        if ( double( aux_ts.getTStep( ) ) * 2.0 * target_maxf > 1.1 )
        {
            DFT aux_resample_dft( in[ iaux ] );
            aux_resample_dft.extend( target_maxf );
            resampled.push_back( aux_resample_dft.iFFT( ) );
        }
        else
        {
            resampled.push_back( in[ iaux ] );
        }

        if ( chan._window.null( ) )
        {
            chan._window.set( Tukey( 0.1, aux_ts.getNSample( ) ) );
        }

        DFT aux_dft( chan._window( aux_ts ) );
        aux_dft.extend( target_maxf );
        aux_dft.iFFT( resampled[ iaux - 1 ] );
        double aux_maxf = aux_dft.getHighFreq( );
        PSD    aux_psd( aux_dft );
        aux_dft.refDVect( ).Conjugate( );
        aux_dft *= target_dft.extract( 0, aux_maxf );
        aux_dft /= aux_psd;

        //--------------------------------  Rebin transfer function
        DFT tf = average_dft_bins( aux_dft, 1.0 / double( seg_len ) );
        tf.Dump( cout );
        tf.extend( aux_maxf );
        chan._fir_coefs = *( tf.iFFT( ).refDVect( ) );
        chan._order = chan._fir_coefs.size( );

        //--------------------------------  Build covariance matrix
        _par_vect[ iaux - 1 ] = resampled[ iaux - 1 ] * target;
        for ( size_t j = 0; j < iaux; j++ )
        {
            covcoefs[ mtx_el++ ] = resampled[ iaux - 1 ] * resampled[ j ];
        }
    }

    //-----------------------------------  Solve the linear combination
    lcl_array< double > vec( N - 1 );
    LTMatrix            covmtx( N - 1, covcoefs, LTMatrix::kSymmetric );
    cout << "Covariance matrix:" << endl;
    covmtx.dump( cout );
    covmtx.solve( &_par_vect.at( 0 ), vec );
    cout << "Linear parameters:";
    for ( size_t i = 0; i < nAux; i++ )
    {
        cout << "  " << vec[ i ] << endl;
    }
    cout << endl;

    //-----------------------------------  Set up the filters.
    for ( size_t iaux = 0; iaux < nAux; iaux++ )
    {
        chanfilt& chan( _chanfilt[ iaux ] );
        chan._fir_coefs *= vec[ iaux ];
    }
    _seglen = seg_len;
}
