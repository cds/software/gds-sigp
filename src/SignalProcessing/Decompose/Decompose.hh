/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef DECOMPOSE_HH
#define DECOMPOSE_HH

#include "autopipe.hh"
#include "DVecType.hh"
#include "fSeries/DFT.hh"
#include "FilterBase.hh"
#include "FilterMap.hh"
#include "KeyChain.hh"
#include <string>
#include <vector>

/**  Decompose is a linear decomposition filter. The input to the filter is a 
  *  target time series (in element 0) and an arbitrary number of component 
  *  time series the output contains the target with the identified components
  *  subtracted and each of the components normalized to their contribution
  *  to the target.
  *  \brief Linear decomposition of a signal.
  *  \author John Zweizig <john.zweizig@ligo.org>
  *  \version $id$
  */

class Decompose : public FilterBase
{
public:
    typedef std::vector< TSeries > io_vect;

public:
    //====================================  Default constructor
    Decompose( const KeyChain& keys );
    virtual ~Decompose( );
    Decompose*      clone( void ) const;
    FilterIO&       apply( const FilterIO& in );
    void            apply( const io_vect& in, io_vect& out );
    void            dataCheck( const FilterIO& in ) const;
    Time            getCurrentTime( void ) const;
    const KeyChain& getInputKeys( void ) const;
    const KeyChain& getOutputKeys( void ) const;
    Time            getStartTime( void ) const;
    bool            inUse( void ) const;
    void            reset( void );
    void            train( const io_vect& in, Interval seg_len );

private:
    struct chanfilt
    {
        chanfilt( const std::string& name );
        std::string _name;
        DVectD      _fir_coefs;
        auto_pipe   _window;
        TSeries     _history;
        size_t      _order;
    };
    typedef std::vector< chanfilt > chan_vect;

private:
    KeyChain              _in_chain;
    KeyChain              _out_chain;
    auto_pipe             _target_window;
    chan_vect             _chanfilt;
    std::vector< double > _par_vect;
    FilterMap             _out_IO;
    Interval              _seglen;
    double                _sampleRate;
    Time                  _startTime;
    Time                  _currentTime;
};

//======================================  inline methods.
inline const KeyChain&
Decompose::getInputKeys( void ) const
{
    return _in_chain;
}

inline const KeyChain&
Decompose::getOutputKeys( void ) const
{
    return _out_chain;
}

inline Time
Decompose::getCurrentTime( void ) const
{
    return _currentTime;
}

inline Time
Decompose::getStartTime( void ) const
{
    return _startTime;
}

inline bool
Decompose::inUse( void ) const
{
    return _startTime != Time( 0 );
}

inline Decompose::chanfilt::chanfilt( const std::string& name ) : _name( name )
{
}

#endif // !defined(DECOMP_HH)
