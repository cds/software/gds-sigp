//
//    fast fourier transforms.
//
#include "Complex.hh"

typedef float    FLOAT;
typedef double   FLINT; // internal floating point results
typedef fComplex COMPLEX;

/** Structure used to save pointers to fft plans.
 */
struct fftplan
{
    /** Structure containing the coefficient arrays for various fft operations.
   */
    struct data_struct
    {
        /** Construct and initializa a plan entry.
     */
        data_struct( ) : WSave( 0 ), NAlloc( 0 ), LastN( 0 )
        {
        }
        FLOAT*       WSave; ///< save the plan coefficients.
        unsigned int NAlloc; ///< number of allocated words
        unsigned int LastN; ///< Number of word used for last plan
    };
    data_struct c; ///< Structure for complex transforms
    data_struct r; ///< structure for real trnsforms.
};

/** Initialize forward or backward complex dft coefficients.
 *  /param n number of input data words.
 *  /param wsave data array containing coefficients for use in fft.
 */
void cffti( int n, FLOAT* wsave );

/** Forward dft a complex array in place.
 *  /param n number of input data words.
 *  /param r input/output data array.
 *  /param wsave data array containing coefficients for use in fft.
 */
void cfftf( int n, FLOAT* r, FLOAT* wsave );

/** Backward dft a complex array in place.
 *  /param n number of input data words.
 *  /param r input/output data array.
 *  /param wsave data array containing coefficients for use in fft.
 */
void cfftb( int n, FLOAT* r, FLOAT* wsave );

/** Dump out the fft coefficient storage.
 *  /param n number of input data words.
 *  /param wsave data array containing coefficients for use in fft.
 */
void cfftp( int n, FLOAT* wsave );

/** Initialize coefficients for a real dft.
 *  /param n number of input data words.
 *  /param wsave data array containing data to be processed in place.
 */
void rffti( int n, FLOAT* wsave );

/** Forward dft a real array in place.
 *  /param n number of input data words.
 *  /param x input real data array.
 *  /param y output complex data array.
 *  /param wsave data array containing coefficients for use in fft.
 */
void rfftf( int n, const FLOAT* x, COMPLEX* y, FLOAT* wsave );
//void rfftb(int n, const FLOAT* x, COMPLEX* y, FLOAT* wsave);

// static const FLOAT tpi = 6.28318530717959;
