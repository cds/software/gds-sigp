/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef WRAP_FFT_HH
#define WRAP_FFT_HH

#include "gmutex.hh"
#include <string>
#include <complex>
#include <map>

/**  The %plan_pointer class is a package independent base class for pointers 
  *  to previously calculated plans.
  */
class plan_pointer
{
public:
    /**  Default null constructor.
      *  \brief Constructor.
      */
    plan_pointer( void );

    /**  Virtual destructor.
      *  \brief Destructor.
      */
    virtual ~plan_pointer( void );

    /**  Clone a plan.
      *  \brief Clone a plan
      *  \return Cloned plan pointer address.
      */
    virtual plan_pointer* clone( void ) = 0;

    /**  Delete the plan and destroy the plan.
      *  \brief Kill the plan and pointer.
      */
    virtual void kill( void );

    /**  Return the plan address and destroy the pointer.
      *  \brief Release plan from pointer.
      *  \return Plan address. 
      */
    virtual void* release( void );

    /**  Replace any existing plan with a new plan.
      *  \brief Replace the plan.
      *  \param plan New plan address.
      */
    virtual void reset( void* plan );
};

/** Define the width of the field needed to insert the fft_dirtype.
  */
#define FFT_DIRTYPE_FIELD_WIDTH 3

/**  The wrapfft class provides a standard interface for various fft
  *  packages and maintains the plans that are to be used by the package.
  *  \brief fft package wrapper.
  *  \author J. Zweizig
  *  \version 1.0: Last modified April 10, 2008
  */
class wrapfft
{
public:
    /** Enumerate the fft data types and directions that will be implemented.
      */
    enum fft_dirtype
    {
        fft_float_fcomplex = 0, ///< Forward real float transform
        fft_fcomplex_float = 1, ///< Reverse real float transform
        fft_fcomplex_forward = 2, ///< Forward complex float transform
        fft_fcomplex_backward = 3, ///< Reverse complex float transform
        fft_double_dcomplex = 4, ///< Forward real double transform
        fft_dcomplex_double = 5, ///< Reverse real double transform
        fft_dcomplex_forward = 6, ///< Forward complex double transform
        fft_dcomplex_backward = 7 ///< Reverse complex double transform
    };

    /**  Plan id data type.
      */
    typedef long plan_id;

    /** Enumerate reordering types.
      */
    enum reorder_type
    {
        reorder_dft_to_freq, ///< reorder dft to f-domain
        reorder_freq_to_dft ///< reorder dft to f-domain
    };

public:
    /**  The default constructor is all that's needed.
      *  \brief Default constructor.
      */
    wrapfft( void );

    /**  Virtual destructor to shut down packages.
      *  \brief Default destructor.
      */
    virtual ~wrapfft( void );

    /**  Optional entry to fetch saved plans from a specified file. This
      *  method defaults to a null routine, and must be overridden if 
      *  an appropriate function is available.
      *  \brief Fetch precalculated plans
      *  \param file  File name.
      *  \return True on success.
      */
    virtual bool fetch_plans( const std::string& file );

    /**  Select an fft length that will lead to a fast transform.
      *  \brief Chose an fft length that will be fast
      *  \param N Starting length.
      *  \return recommended length.
      */
    virtual long pick_length( long N ) const;

    /**  Reorder a complex series to DFT or to frequency ordering. The 
      *  source and destination addresses may be equal or disjoint.
      *  \brief Reorder DFT data
      *  \param in  Pointer to input (source) series.
      *  \param out Pointer to output (destination) storage.
      *  \param N   Length.
      *  \param dir direction.
      */
    virtual void reorder( const std::complex< float >* in,
                          std::complex< float >*       out,
                          long                         N,
                          reorder_type                 dir ) const;

    /**  Reorder a complex series to DFT or to frequency ordering.The 
      *  source and destination addresses may be equal or disjoint.
      *  \brief Reorder DFT data
      *  \param in  Pointer to input (source) series.
      *  \param out Pointer to output (destination) storage.
      *  \param N   Length.
      *  \param dir direction.
      */
    virtual void reorder( const std::complex< double >* in,
                          std::complex< double >*       out,
                          long                          N,
                          reorder_type                  dir ) const;

    /**  Transform N real data points to a single-sided complex dft.
      *  The input vector must contain N float elements and the output
      *  vector must accommodate N+1 complex elements.
      *  \brief DFT of real data.
      *  \param N   Length of input series.
      *  \param in  Pointer to real (float) series.
      *  \param out Pointer to storage for N+1 element complex series.
      */
    virtual void
    transform( long N, const float* in, std::complex< float >* out );

    /**  Perform an inverse DFT from a single-sided complex series to
      *  a real series.
      *  The input vector contains N/2+1 complex elements and the output
      *  vector must accommodate N float elements.
      *  \brief Inverse DFT of single-sided data.
      *  \param N   Length of real (output) series.
      *  \param in  Pointer to an N/2+1 element complex series.
      *  \param out Pointer to storage for an N element (float) series.
      */
    virtual void
    transform( long N, const std::complex< float >* in, float* out );

    /**  Perform a forward or backward DFT of N complex data points. The
      *  resulting series overwrites the input data. The length is set 
      *  -N to indicate an backward transform.
      *  The input/output vector must contain N complex elements.
      *  \brief DFT of real data.
      *  \param N   Length of input series.
      *  \param out Pointer to storage for N element complex series.
      */
    virtual void transform( long N, std::complex< float >* out );

    /**  Transform N real data points to a single-sided complex dft.
      *  The input vector must contain N double elements and the output
      *  vector must accommodate N/2+1 complex elements.
      *  \brief DFT of real data.
      *  \param N   Length of input series.
      *  \param in  Pointer to real (float) series.
      *  \param out Pointer to storage for N/2+1 element complex series.
      */
    virtual void
    transform( long N, const double* in, std::complex< double >* out );

    /**  Perform an inverse DFT from a single-sided complex series to
      *  a real series.
      *  The input vector contains N/2+1 complex elements and the output
      *  vector must accommodate N float elements.
      *  \brief Inverse DFT of single-sided data.
      *  \param N   Length of real (output) series.
      *  \param in  Pointer to an N/2+1 element complex series.
      *  \param out Pointer to storage for an N element (float) series.
      */
    virtual void
    transform( long N, const std::complex< double >* in, double* out );

    /**  Perform a forward or backward DFT of N complex data points. The
      *  resulting series overwrites the input data. The length is set 
      *  -N to indicate an backward transform.
      *  The input/output vector must contain N complex elements.
      *  \brief DFT of real data.
      *  \param N   Length of input series.
      *  \param out Pointer to storage for N element complex series.
      */
    virtual void transform( long N, std::complex< double >* out );

protected:
    /**  Find a previously generated and saved plan. Return a pointer 
      *  to the plan.
      *  \brief Fetch precalculated plans
      *  \param id Transform identifier.
      *  \return Pointer to a previously generated plan.
      */
    virtual plan_pointer* find_plan( plan_id id );

    /**  Generate a plan_id for the specified transformation type.
      *  \brief Get the appropriate plan_id.
      *  \param type  Direction, fft type and data type.
      *  \param N     Number of elements in series.
      *  \return An ID of the specified plan.
      */
    virtual plan_id get_plan_id( fft_dirtype type, long N );

    /**  Make a plan to perform the specified transformation.
      *  \brief Make a plan.
      *  \param id Identifier for the plan to be generated.
      *  \return Pointer to the generated plan.
      */
    virtual plan_pointer* make_plan( plan_id id );

    /**  Store a plan for the specified transformation type.
      *  \brief Store a plan for the specified plan_id.
      *  \param pid  ID for the specified plan.
      *  \param p    Reference to a plan pointer.
      *  \return An ID of the specified plan.
      */
    virtual void set_plan( plan_id pid, plan_pointer& p );

private:
    typedef std::map< plan_id, plan_pointer* > plan_map;
    typedef plan_map::value_type               plan_node;
    typedef plan_map::iterator                 plan_iter;
    typedef plan_map::const_iterator           const_plan_iter;

private:
    thread::readwritelock fft_mux;
    plan_map              fft_plan_map;
};

#endif // WRAP_FFT_HH
