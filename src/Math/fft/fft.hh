#ifndef FFT_HH
#define FFT_HH

#include "Complex.hh"

/// FFT support flags
enum fft_support_type
{
    kFFTW,
    kFFTDouble
};

/**  Returns information about the FFT implementation.
  *  Supported flags are:
  *    kFFTW: true if FFTW3 is used
  *    kFFTDouble: true if double precision FFTs are available
  *  @brief FFT Implementation information.
  *  @param supp Support flag.
  *  @return true if feature indicated by flag is supported
  */
bool infoFFT( fft_support_type supp );

/**  Returns a guess at an optimal fft length >= the specified length.
  *  @brief Optimal FFT length selection.
  *  @param N minimum DFT length.
  *  @return Optimal number
  */
long wfft_pick_length( long N );

/**  Read a specified file containing predefined fft plans. For fftw the 
  *  specified wisdom file is read.
  *  @brief Read fft plan file
  *  @param planfile Plan file path.
  *  @return True if plan file (fftw wisdom) read successfully.
  */
bool wfft_read_plans( const char* planfile );

/**  Reorder the output data from DFT ordering to Frequency ordering and 
  *  visa-versa.
  *  @brief Reorder DFT output
  *  @param x data to be reordered.
  *  @param y destination storage.
  *  @param N minimum DFT length.
  *  @param to_dft DFT to frequency if false.
  */
void wfft_reorder( const fComplex x[], fComplex y[], long N, bool to_dft );

/**  Reorder the output data from DFT ordering to Frequency ordering and 
  *  visa-versa.
  *  @brief Reorder DFT output
  *  @param x data to be reordered.
  *  @param y destination storage.
  *  @param N minimum DFT length.
  *  @param to_dft DFT to frequency if false.
  */
void wfft_reorder( const dComplex x[], dComplex y[], long N, bool to_dft );

/**  Calculate a fast Fourier transform of a single precision complex series. 
  *  The transformed data overwrite the untransformed data. The output series 
  *  store in the usual order, \e e.g. 
  *  \verbatim 0, dF, ..., F_Ny-dF, -F_Ny, ..., -dF. \endverbatim
  *  @brief In-place Fourier Transform of a complex series.
  *  @param x Complex series to be transformed.
  *  @param N Number of elements in the series. N may be any number > 1.
  *  @param Dir Select between transform (1) and inverse transform (0).
  */
void wfft( fComplex x[], unsigned int N, int Dir );

/**  Calculate a fast Fourier transform of a double precision complex series.
  *  The transformed data overwrite the untransformed data. The output series 
  *  store in the usual order, \e e.g. 
  *  \verbatim 0, dF, ..., F_Ny-dF, -F_Ny, ..., -dF. \endverbatim
  *  @brief In-place Fourier Transform of a complex series.
  *  @param x Complex series to be transformed.
  *  @param N Number of elements in the series. N may be any number > 1.
  *  @param Dir Select between transform (1) and inverse transform (0).
  */
void wfft( dComplex x[], unsigned int N, int Dir );

/**  Calculate a fast Fourier Transform of a real series. The output
  *  data are stored as a single-sided series from 0 to F_NY, inclusive. 
  *  The output array has N/2+1 elements and may overlap the input array.
  *  @brief Fourier Transform of a real series.
  *  @param x Real time series to be Fourier transformed.
  *  @param y Complex result of the transformation.
  *  @param N Number of elements in the input series. N may be any even number.
  */
void wfft( const float x[], fComplex y[], unsigned int N );

/**  Calculate a fast Fourier Transform of a real series. The output
  *  data are stored as a single-sided series from 0 to F_NY, inclusive. 
  *  The output array has N/2+1 elements and may overlap the input array.
  *  @brief Fourier Transform of a real series.
  *  @param x Real time series to be Fourier transformed.
  *  @param y Complex result of the transformation.
  *  @param N Number of elements in the input series. N may be any even number.
  */
void wfft( const double x[], dComplex y[], unsigned int N );

/**  Calculate an inverse Fourier Transform of a single sided series. The 
  *  output data are stored as a float time series. The output data may 
  *  overlap the input array.
  *  @brief Inverse Fourier transform of a single-sided series.
  *  @param x Complex time series to be Fourier transformed.
  *  @param y Real result of the transformation.
  *  @param N Number of elements in the output series. N may be any even number.
  */
void wfft( const fComplex y[], float x[], unsigned int N );

/**  Calculate an inverse Fourier Transform of a single sided series. The 
  *  output data are stored as a float time series. The output data may 
  *  overlap the input array.
  *  @brief Inverse Fourier transform of a single-sided series.
  *  @param x Complex time series to be Fourier transformed.
  *  @param y Real result of the transformation.
  *  @param N Number of elements in the output series. N may be any even number.
  */
void wfft( const dComplex y[], double x[], unsigned int N );

#endif // FFT_HH
