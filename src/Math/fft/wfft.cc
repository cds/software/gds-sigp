/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "fft.hh"
#include <pthread.h>

bool
infoFFT( fft_support_type supp )
{
    switch ( supp )
    {
    case kFFTW:
#ifdef FFTW3
        return true;
#else
        return false;
#endif
    case kFFTDouble:
#ifdef FFT_DOUBLE
        return true;
#else
        return false;
#endif
    default:
        return false;
    }
}

#ifdef FFTW3
//==========================================================================
//=                                                                        =
//=                                 Use FFTW3                              =
//=                                                                        =
//==========================================================================
#include "wrapfftw.hh"

wrapfft&
fftw_mod( void )
{
    static wrapfftw mod_internal;
    return mod_internal;
}

//======================================  Complex fft
void
wfft( fComplex x[], unsigned int N, int direc )
{
    long len = N;
    if ( !direc )
        len = -N;
    fftw_mod( ).transform( len,
                           reinterpret_cast< std::complex< float >* >( x ) );
}

//======================================  Real fft
void
wfft( const float x[], fComplex y[], unsigned int N )
{
    std::complex< float >* fy = reinterpret_cast< std::complex< float >* >( y );
    fftw_mod( ).transform( N, x, fy );
}

//======================================  Real inverse fft
void
wfft( const fComplex y[], float x[], unsigned int N )
{
    const std::complex< float >* fy =
        reinterpret_cast< const std::complex< float >* >( y );
    fftw_mod( ).transform( N, fy, x );
}

//======================================  pick a transform length
long
wfft_pick_length( long N )
{
    return fftw_mod( ).pick_length( N );
}

//======================================  pick a transform length
bool
wfft_read_plans( const char* path )
{
    return fftw_mod( ).fetch_plans( path );
}

//======================================  pick a transform length
void
wfft_reorder( const fComplex* in, fComplex* out, long N, bool to_dft )
{
    wrapfft::reorder_type dir;
    if ( to_dft )
        dir = wrapfft::reorder_freq_to_dft;
    else
        dir = wrapfft::reorder_dft_to_freq;
    fftw_mod( ).reorder( reinterpret_cast< const std::complex< float >* >( in ),
                         reinterpret_cast< std::complex< float >* >( out ),
                         N,
                         dir );
}

//======================================  pick a transform length
void
wfft_reorder( const dComplex* in, dComplex* out, long N, bool to_dft )
{
    wrapfft::reorder_type dir;
    if ( to_dft )
        dir = wrapfft::reorder_freq_to_dft;
    else
        dir = wrapfft::reorder_dft_to_freq;
    fftw_mod( ).reorder(
        reinterpret_cast< const std::complex< double >* >( in ),
        reinterpret_cast< std::complex< double >* >( out ),
        N,
        dir );
}

#ifdef FFT_DOUBLE
//======================================  Complex fft (double)
void
wfft( dComplex x[], unsigned int N, int direc )
{
    std::complex< double >* fx =
        reinterpret_cast< std::complex< double >* >( x );
    long len = N;
    if ( !direc )
        len = -len;
    fftw_mod( ).transform( len, fx );
}

//======================================  Real fft (double)
void
wfft( const double x[], dComplex y[], unsigned int N )
{
    std::complex< double >* fy =
        reinterpret_cast< std::complex< double >* >( y );
    fftw_mod( ).transform( N, x, fy );
}

//======================================  Real inverse fft (double)
void
wfft( const dComplex y[], double x[], unsigned int N )
{
    const std::complex< double >* fy =
        reinterpret_cast< const std::complex< double >* >( y );
    fftw_mod( ).transform( N, fy, x );
}
#endif

#else
//==========================================================================
//=                                                                        =
//=                                 Use FFTPAK                             =
//=                                                                        =
//==========================================================================

#include "fftpak.hh"

//======================================  FFT plan
static fftplan plan;

//======================================  Complex fft
void
wfft( fComplex x[], unsigned int N, int direc )
{
    if ( N != plan.c.LastN )
    {
        if ( plan.c.NAlloc < N )
        {
            if ( plan.c.WSave )
                delete[] plan.c.WSave;
            plan.c.WSave = new FLOAT[ 4 * N + 15 ];
            plan.c.NAlloc = N;
        }
        cffti( N, plan.c.WSave );
        plan.c.LastN = N;
    }
    if ( direc > 0 )
    {
        cfftf( N, (FLOAT*)x, plan.c.WSave );
    }
    else
    {
        cfftb( N, (FLOAT*)x, plan.c.WSave );
    }
}

//======================================  Real fft
void
wfft( const float x[], fComplex y[], unsigned int N )
{
    if ( N != plan.r.LastN )
    {
        if ( plan.r.NAlloc < N )
        {
            if ( plan.r.WSave )
                delete[] plan.r.WSave;
            plan.r.WSave = new FLOAT[ 2 * N + 15 ];
            plan.r.NAlloc = N;
        }
        rffti( N, plan.r.WSave );
        plan.r.LastN = N;
    }
    rfftf( N, x, y, plan.r.WSave );
}

//======================================  Real inverse fft
void
wfft( const fComplex y[], float x[], unsigned int N )
{
    printf( "Inverse FFT not supported\n" );
}

#endif

#ifndef FFT_DOUBLE
void
wfft( dComplex x[], unsigned int N, int direc )
{
    printf( "Double precision FFTs not supported\n" );
}

void
wfft( const double x[], dComplex y[], unsigned int N )
{
    printf( "Double precision FFTs not supported\n" );
}

void
wfft( const dComplex y[], double x[], unsigned int N )
{
    printf( "Double precision FFTs not supported\n" );
}
#endif
