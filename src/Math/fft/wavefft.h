#ifndef _WAVEFFT_H
#define _WAVEFFT_H

/********************************************************/
/* Wavelet Analysis Tool                                */
/* file wavefft.h                                       */
/* Author: Sergey Klimenko                              */
/* Date:   July 20, 2000                                */
/********************************************************/

void wavefft( double a[], double b[], int ntot, int n, int nspan, int isn );
#endif
