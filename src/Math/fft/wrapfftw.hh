/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef WRAP_FFTW_HH
#define WRAP_FFTW_HH

#include "wrapfft.hh"
#include "fftw3.h"

/**  fftw_plan_pointer is an implementation of the plan_pointer API that
  *  wraps FFTW plans.
  *  \brief FFTW plan pointer class.
  *  \author John Zweizig (john.zweizig@ligo.org)
  *  \version $Id$
  */
class fftw_plan_pointer : public plan_pointer
{
public:
    /**  Construct a plan wrapper from an FFTW plan pointer and the transform
      *  parameters.
      *  \brief Construct an fftw plan pointer.
      *  \param plan Pointer to an FFTW plan.
      *  \param typ  Transform type.
      */
    fftw_plan_pointer( void*                plan = 0,
                       wrapfft::fft_dirtype typ = wrapfft::fft_float_fcomplex );

    /**  Destroy a wrapped FFTW plan.
      *  \brief Destructor.
      */
    ~fftw_plan_pointer( void );

    /**  Clone a wrapped FFTW plan.
      *  \brief Clone a plan.
      *  \return Pointer to the cloned plan.
      */
    fftw_plan_pointer* clone( void );

    /**  Delete the wrapped FFTW plan.
      *  \brief delete the plan.
      */
    void kill( void );

    /**  Transfer the FFTW plan pointer by marking this wrapper invalid
      *  and returning the raw plan pointer.
      *  \brief Transfer plan pointer.
      *  \return Pointer to the FFTW plan.
      */
    void* release( void );

    /**  Replace the plan pointer and kill the old plan.
      *  \brief Replace a plan.
      *  \param plan New FFTW plan.
      */
    void reset( void* plan );

    /**  Get a single precision plan pointer for FFTW.
      *  \brief Get plan pointer.
      *  \return Plan pointer for use by FFTW.
      */
    fftwf_plan get_fftwf_plan( void );

    /**  Get a double precision plan pointer for FFTW.
      *  \brief Get plan pointer.
      *  \return Plan pointer for use by FFTW.
      */
    fftw_plan get_fftwd_plan( void );

private:
    wrapfft::fft_dirtype _type;
    void*                _plan;
};

/**  The wrapfftw class implements the standard interface for the fftw3.
  *  package.
  *  \brief fftw3 package wrapper.
  *  \author J. Zweizig
  *  \version $Id$
  */
class wrapfftw : public wrapfft
{
public:
    /**  The default constructor is all that's needed.
      *  \brief Default constructor.
      */
    wrapfftw( void );

    /**  Virtual destructor to shut down packages.
      *  \brief Default destructor.
      */
    ~wrapfftw( void );

    /**  This entry instructs FFTW  to load the contents of the specified 
      *  wisdom file into its memory. If the argument file path is blank 
      *  (empty string) the function instructs FFTW to use the system 
      *  default wisdom file. 
      *  \brief Fetch FFTW wisdom data.
      *  \param file  File path or empty to use the system wisdom file.
      *  \return True on success.
      */
    bool fetch_plans( const std::string& file );

    /**  Select an fft length that will lead to a fast transform. The 
      *  selected length is the smallest product of small prime numbers 
      *  (\e i.e. 2, 3, 5) greater than or equal to the specified length. 
      *  \brief Chose an fft length that will be fast
      *  \param N Starting length.
      *  \return Recommended length.
      */
    long pick_length( long N ) const;

    /**  Transform N real data points to a single-sided complex dft.
      *  The input vector must contain N float elements and the output
      *  vector must accommodate N+1 complex elements.
      *  \brief DFT of real data.
      *  \param N   Length of input series.
      *  \param in  Pointer to real (float) series.
      *  \param out Pointer to storage for N+1 element complex series.
      */
    void transform( long N, const float* in, std::complex< float >* out );

    /**  Perform an inverse DFT from a single-sided complex series to
      *  a real series.
      *  The input vector contains N/2+1 complex elements and the output
      *  vector must accommodate N float elements.
      *  \brief Inverse DFT of single-sided data.
      *  \param N   Length of real (output) series.
      *  \param in  Pointer to an N/2+1 element complex series.
      *  \param out Pointer to storage for an N element (float) series.
      */
    void transform( long N, const std::complex< float >* in, float* out );

    /**  Perform a forward or backward DFT of N complex data points. The
      *  resulting series overwrites the input data. The length is set 
      *  to -N to indicate an backward transform.
      *  The input/output vector must contain N complex elements.
      *  \brief DFT of real data.
      *  \param N   Length of input series.
      *  \param out Pointer to storage for N element complex series.
      */
    void transform( long N, std::complex< float >* out );

    /**  Transform N real data points to a single-sided complex dft.
      *  The input vector must contain N double elements and the output
      *  vector must accommodate N/2+1 complex elements.
      *  \brief DFT of real data.
      *  \param N   Length of input series.
      *  \param in  Pointer to real (float) series.
      *  \param out Pointer to storage for N/2+1 element complex series.
      */
    void transform( long N, const double* in, std::complex< double >* out );

    /**  Perform an inverse DFT from a single-sided complex series to
      *  a real series.
      *  The input vector contains N/2+1 complex elements and the output
      *  vector must accommodate N float elements.
      *  \brief Inverse DFT of single-sided data.
      *  \param N   Length of real (output) series.
      *  \param in  Pointer to an N/2+1 element complex series.
      *  \param out Pointer to storage for an N element (float) series.
      */
    void transform( long N, const std::complex< double >* in, double* out );

    /**  Perform a forward or backward DFT of N complex data points. The
      *  resulting series overwrites the input data. The length is set 
      *  -N to indicate an backward transform.
      *  The input/output vector must contain N complex elements.
      *  \brief DFT of real data.
      *  \param N   Length of input series.
      *  \param out Pointer to storage for N element complex series.
      */
    void transform( long N, std::complex< double >* out );

protected:
    /**  Return a pointer to a specified precalculated FFTW plan.
      *  \brief Fetch precalculated plans
      *  \param id  Identifier of the requested plan. 
      *  \return Plan pointer address.
      */
    fftw_plan_pointer* find_plan( plan_id id );

private:
    thread::readwritelock mux;
};

inline fftwf_plan
fftw_plan_pointer::get_fftwf_plan( void )
{
    return reinterpret_cast< fftwf_plan >( _plan );
}

inline fftw_plan
fftw_plan_pointer::get_fftwd_plan( void )
{
    return reinterpret_cast< fftw_plan >( _plan );
}

#endif // WRAP_FFT_HH
