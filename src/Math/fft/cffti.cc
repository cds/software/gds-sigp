#include "fftpak.hh"
#include "PConfig.h"
#include <iostream>
#include "constant.hh"

static void cffti1( int n, FLOAT* wa, int* ifac );

void
cffti( int n, FLOAT* wsave )
{
    if ( n != 1 )
        cffti1( n, wsave + 2 * n, (int*)( wsave + 4 * n ) );
}

void
cfftp( int n, FLOAT* wsave )
{
    int* ifac = (int*)( wsave + 4 * n );
    if ( ifac[ 0 ] != n )
    {
        std::cout << "Wrong N set in FFT temp storage" << std::endl;
    }
    else
    {
        std::cout << "FFT data area set for N=" << *ifac++ << std::endl;
        std::cout << "Prime factors: ";
        int nf = *ifac++;
        for ( int i = 0; i < nf; i++ )
            std::cout << *ifac++ << " ";
        std::cout << std::endl;
    }
}

static const int ntryh[ 4 ] = { 3, 4, 2, 5 };

static void
cffti1( int n, FLOAT* wa, int* ifac )
{
    int nl = n;
    int nf = 0;
    int j = 0, ntry = 3;
    while ( nl != 1 )
    {
        int nq, nr = 1;
        while ( nr != 0 )
        {
            nq = nl / ntry;
            nr = nl - ntry * nq;
            if ( nr )
            {
                j++;
                if ( j < 4 )
                    ntry = ntryh[ j ];
                else
                    ntry += 2;
            }
        }
        nf++;
        ifac[ nf + 1 ] = ntry;
        nl = nq;
        if ( ntry == 2 && nf != 1 )
        {
            for ( int i = 1; i < nf; i++ )
            {
                int ib = nf - i;
                ifac[ ib + 2 ] = ifac[ ib + 1 ];
            }
            ifac[ 2 ] = 2;
        }
    }
    ifac[ 0 ] = n;
    ifac[ 1 ] = nf;

    //--------------------------------------  Fill in the angle table.
    double argh = twopi / double( n );
    int    i = 0;
    int    l1 = 1;
    for ( int k1 = 0; k1 < nf; k1++ )
    {
        int ip = ifac[ k1 + 2 ];
        int ld = 0;
        int l2 = l1 * ip;
        int ido = n / l2;
        for ( int j = 1; j < ip; j++ )
        {
            int i1 = i;
            wa[ i ] = 1.0;
            wa[ i + 1 ] = 0.0;
            ld += l1;
            double fi = 0.0;
            double argld = double( ld ) * argh;
            for ( int ii = 0; ii < ido; ii++ )
            {
                i += 2;
                fi += 1.0;
                double arg = fi * argld;
#ifdef USE_SINCOS
                sincosf( arg, wa + i + 1, wa + i );
#else
                wa[ i ] = cos( arg );
                wa[ i + 1 ] = sin( arg );
#endif // defined(USE_SINCOS)
            }
            if ( ip >= 5 )
            {
                wa[ i1 ] = wa[ i ];
                wa[ i1 + 1 ] = wa[ i + 1 ];
            }
        }
        l1 = l2;
    }
}
