#include "fftpak.hh"
#include <string.h>

//--------------------------------------  Internal functions
static void cfftb1( int n, FLOAT* a, FLOAT* ch, FLOAT* wa, int* ifac );
static void passb( int*   nac,
                   int    ido,
                   int    ip,
                   int    l1,
                   FLOAT* cc,
                   FLOAT* c1,
                   FLOAT* c2,
                   FLOAT* ch,
                   FLOAT* ch2,
                   FLOAT* wa );
static void passb2( int ido, int l1, FLOAT* cc, FLOAT* ch, FLOAT* wa1 );
static void
passb3( int ido, int l1, FLOAT* cc, FLOAT* ch, FLOAT* wa1, FLOAT* wa2 );
static void passb4(
    int ido, int l1, FLOAT* cc, FLOAT* ch, FLOAT* wa1, FLOAT* wa2, FLOAT* wa3 );
static void passb5( int    ido,
                    int    l1,
                    FLOAT* cc,
                    FLOAT* ch,
                    FLOAT* wa1,
                    FLOAT* wa2,
                    FLOAT* wa3,
                    FLOAT* wa4 );

//--------------------------------------  External entry point
void
cfftb( int n, FLOAT* c, FLOAT* wsave )
{
    if ( n != 1 )
        cfftb1( n, c, wsave, wsave + 2 * n, (int*)( wsave + 4 * n ) );
}

//--------------------------------------  Main function.
static void
cfftb1( int n, FLOAT* c, FLOAT* ch, FLOAT* wa, int* ifac )
{
    if ( n != ifac[ 0 ] )
        return;
    int  nf = ifac[ 1 ];
    bool na = false;
    int  l1 = 1;
    int  iw = 0;
    for ( int k1 = 0; k1 < nf; k1++ )
    {
        int ip = ifac[ k1 + 2 ];
        int l2 = ip * l1;
        int ido = n / l2;
        int idot = 2 * ido;
        int nac;
        int ix2 = iw + idot;
        int ix3 = ix2 + idot;
        int ix4 = ix3 + idot;

        switch ( ip )
        {
        case 4:
            if ( !na )
                passb4( idot, l1, c, ch, wa + iw, wa + ix2, wa + ix3 );
            else
                passb4( idot, l1, ch, c, wa + iw, wa + ix2, wa + ix3 );
            na = !na;
            break;

        case 2:
            if ( !na )
                passb2( idot, l1, c, ch, wa + iw );
            else
                passb2( idot, l1, ch, c, wa + iw );
            na = !na;
            break;

        case 3:
            if ( !na )
                passb3( idot, l1, c, ch, wa + iw, wa + ix2 );
            else
                passb3( idot, l1, ch, c, wa + iw, wa + ix2 );
            na = !na;
            break;

        case 1: // case 1(5) to use passb(pass5) for ip = 5
            if ( !na )
                passb5(
                    idot, l1, c, ch, wa + iw, wa + ix2, wa + ix3, wa + ix4 );
            else
                passb5(
                    idot, l1, ch, c, wa + iw, wa + ix2, wa + ix3, wa + ix4 );
            na = !na;
            break;

        case 5: // case 5(1) to use passb(pass5) for ip = 5;

        default:
            if ( !na )
                passb( &nac, idot, ip, l1, c, c, c, ch, ch, wa + iw );
            else
                passb( &nac, idot, ip, l1, ch, ch, ch, c, c, wa + iw );
            if ( nac != 0 )
                na = !na;
        }

        l1 = l2;
        iw += ( ip - 1 ) * idot;
    }

    if ( na )
    {
        // int n2 = 2*n;
        // for (int i=0 ; i<n2 ; i++) c[i] = ch[i];
        memcpy( c, ch, 2 * n * sizeof( FLOAT ) );
    }
}

static void
passb( int*   nac,
       int    ido,
       int    ip,
       int    l1,
       FLOAT* cc,
       FLOAT* c1,
       FLOAT* c2,
       FLOAT* ch,
       FLOAT* ch2,
       FLOAT* wa )
{
    int idl1 = ido * l1;
    int idot = ido / 2;
    int ipph = ( ip + 1 ) / 2;
    int idp = ip * ido;
    if ( ido >= l1 )
    {
        FLOAT* pCH1kj = ch + idl1;
        for ( int j = 1; j < ipph; j++ )
        {
            int    jc = ip - j;
            FLOAT* pCH1kjc = ch + jc * idl1;
            FLOAT* pCC1jk = cc + j * ido;
            FLOAT* pCC1jck = cc + jc * ido;
            for ( int k = 0; k < l1; k++ )
            {
                for ( int i = 0; i < ido; i++ )
                {
                    pCH1kj[ i ] = pCC1jk[ i ] + pCC1jck[ i ];
                    pCH1kjc[ i ] = pCC1jk[ i ] - pCC1jck[ i ];
                }
                pCH1kj += ido;
                pCH1kjc += ido;
                pCC1jk += idp;
                pCC1jck += idp;
            }
        }
        FLOAT* pCH1k1 = ch;
        FLOAT* pCC11k = cc;
        for ( int k = 0; k < l1; k++ )
        {
            for ( int i = 0; i < ido; i++ )
            {
                pCH1k1[ i ] = pCC11k[ i ];
            }
            pCH1k1 += ido;
            pCC11k += idp;
        }
    }
    else
    {
        for ( int j = 1; j < ipph; j++ )
        {
            int jc = ip - j;
            for ( int i = 0; i < ido; i++ )
            {
                FLOAT* pCH1kj = ch + j * idl1;
                FLOAT* pCH1kjc = ch + jc * idl1;
                FLOAT* pCC1jk = cc + j * ido;
                FLOAT* pCC1jck = cc + jc * ido;
                for ( int k = 0; k < l1; k++ )
                {
                    pCH1kj[ i ] = pCC1jk[ i ] + pCC1jck[ i ];
                    pCH1kjc[ i ] = pCC1jk[ i ] - pCC1jck[ i ];
                    pCH1kj += ido;
                    pCH1kjc += ido;
                    pCC1jk += idp;
                    pCC1jck += idp;
                }
            }
        }
        for ( int i = 0; i < ido; i++ )
        {
            FLOAT* pCHik1 = ch + i;
            FLOAT* pCCi1k = cc + i;
            for ( int k = 0; k < l1; k++ )
            {
                *pCHik1 = *pCCi1k;
                pCHik1 += ido;
                pCCi1k += idp;
            }
        }
    }

    int    idl = 0;
    int    inc = 0;
    FLOAT* C21l = c2 + idl1;
    FLOAT* C21lc = c2 + ( ip - 1 ) * idl1;
    for ( int l = 1; l < ipph; l++ )
    {
        for ( int ik = 0; ik < idl1; ik++ )
        {
            C21l[ ik ] = ch2[ ik ] + wa[ idl ] * ch2[ ik + idl1 ];
            C21lc[ ik ] = wa[ idl + 1 ] * ch2[ ik + idl1 * ( ip - 1 ) ];
        }
        int idlj = idl;
        inc += ido;
        FLOAT* CH21j = ch2 + 2 * idl1;
        FLOAT* CH21jc = ch2 + ( ip - 2 ) * idl1;
        for ( int j = 2; j < ipph; j++ )
        {
            idlj += inc;
            if ( idlj >= idp )
                idlj -= idp;
            FLINT war = wa[ idlj ];
            FLINT wai = wa[ idlj + 1 ];
            for ( int ik = 0; ik < idl1; ik++ )
            {
                C21l[ ik ] += war * CH21j[ ik ];
                C21lc[ ik ] += wai * CH21jc[ ik ];
            }
            CH21j += idl1;
            CH21jc -= idl1;
        }
        C21l += idl1;
        C21lc -= idl1;
        idl += ido;
    }

    FLOAT* CH2ikj = ch2 + idl1;
    for ( int j = 1; j < ipph; j++ )
    {
        for ( int ik = 0; ik < idl1; ik++ )
        {
            ch2[ ik ] += *CH2ikj++;
        }
    }
    FLOAT* CH21j = ch2 + idl1;
    FLOAT* C21j = c2 + idl1;
    FLOAT* CH21jc = ch2 + ( ip - 1 ) * idl1;
    FLOAT* C21jc = c2 + ( ip - 1 ) * idl1;
    for ( int j = 1; j < ipph; j++ )
    {
        for ( int ik = 0; ik < idl1; ik += 2 )
        {
            CH21j[ ik ] = C21j[ ik ] - C21jc[ ik + 1 ];
            CH21jc[ ik ] = C21j[ ik ] + C21jc[ ik + 1 ];
            CH21j[ ik + 1 ] = C21j[ ik + 1 ] + C21jc[ ik ];
            CH21jc[ ik + 1 ] = C21j[ ik + 1 ] - C21jc[ ik ];
        }
        CH21j += idl1;
        C21j += idl1;
        CH21jc -= idl1;
        C21jc -= idl1;
    }
    if ( ido == 2 )
    {
        *nac = 1;
    }
    else
    {
        *nac = 0;
        for ( int ik = 0; ik < idl1; ik++ )
        {
            c2[ ik ] = ch2[ ik ];
        }
        FLOAT* C11kj = c1 + idl1;
        FLOAT* CH1kj = ch + idl1;
        for ( int j = 1; j < ip; j++ )
        {
            for ( int k = 0; k < l1; k++ )
            {
                C11kj[ 0 ] = CH1kj[ 0 ];
                C11kj[ 1 ] = CH1kj[ 1 ];
                C11kj += ido;
                CH1kj += ido;
            }
        }
        if ( idot <= l1 )
        {
            int idij = 0;
            for ( int j = 1; j < ip; j++ )
            {
                idij += 2;
                for ( int i = 3; i < ido; i += 2 )
                {
                    FLOAT* pC11kj = c1 + idl1 * j;
                    FLOAT* pCH1kj = ch + idl1 * j;
                    for ( int k = 0; k < l1; k++ )
                    {
                        pC11kj[ i - 1 ] = wa[ idij ] * pCH1kj[ i - 1 ] -
                            wa[ idij + 1 ] * pCH1kj[ i ];
                        pC11kj[ i ] = wa[ idij ] * pCH1kj[ i ] +
                            wa[ idij + 1 ] * pCH1kj[ i - 1 ];
                        pC11kj += ido;
                        pCH1kj += ido;
                    }
                    idij += 2;
                }
            }
        }
        else
        {
            FLOAT* C11kj = c1 + idl1;
            FLOAT* CH1kj = ch + idl1;
            FLOAT* WA1j = wa;
            for ( int j = 1; j < ip; j++ )
            {
                for ( int k = 0; k < l1; k++ )
                {
                    for ( int i = 2; i < ido; i += 2 )
                    {
                        C11kj[ i ] = WA1j[ i ] * CH1kj[ i ] -
                            WA1j[ i + 1 ] * CH1kj[ i + 1 ];
                        C11kj[ i + i ] = WA1j[ i ] * CH1kj[ i + 1 ] +
                            WA1j[ i + 1 ] * CH1kj[ i ];
                    }
                    C11kj += ido;
                    CH1kj += ido;
                }
                WA1j += ido;
            }
        }
    }
}

static void
passb2( int ido, int l1, FLOAT* cc, FLOAT* ch, FLOAT* wa1 )
{
    int ldo = l1 * ido;
    if ( ido == 2 )
    {
        FLOAT* pCH1k1 = ch;
        FLOAT* pCC11k = cc;
        for ( int k = 0; k < l1; k++ )
        {
            pCH1k1[ 0 ] = pCC11k[ 0 ] + pCC11k[ 2 ];
            pCH1k1[ ldo ] = pCC11k[ 0 ] - pCC11k[ 2 ];
            pCH1k1[ 1 ] = pCC11k[ 1 ] + pCC11k[ 3 ];
            pCH1k1[ ldo + 1 ] = pCC11k[ 1 ] - pCC11k[ 3 ];
            pCH1k1 += 2;
            pCC11k += 4;
        }
    }
    else
    {
        FLOAT* pCH1k1 = ch;
        FLOAT* pCC11k = cc;
        for ( int k = 0; k < l1; k++ )
        {
            for ( int i = 1; i < ido; i += 2 )
            {
                pCH1k1[ i - 1 ] = pCC11k[ i - 1 ] + pCC11k[ i - 1 + ido ];
                FLINT tr2 = pCC11k[ i - 1 ] - pCC11k[ i - 1 + ido ];
                pCH1k1[ i ] = pCC11k[ i ] + pCC11k[ i + ido ];
                FLINT ti2 = pCC11k[ i ] - pCC11k[ i + ido ];
                pCH1k1[ i + ldo ] = wa1[ i - 1 ] * ti2 + wa1[ i ] * tr2;
                pCH1k1[ i - 1 + ldo ] = wa1[ i - 1 ] * tr2 - wa1[ i ] * ti2;
            }
            pCH1k1 += ido;
            pCC11k += ido + ido;
        }
    }
}
static void
passb3( int ido, int l1, FLOAT* cc, FLOAT* ch, FLOAT* wa1, FLOAT* wa2 )
{
    static FLINT taur = -0.5, taui = 0.866025403784439;
    int          ldo = l1 * ido;
    if ( ido == 2 )
    {
        FLOAT* pCC11k = cc;
        FLOAT* pCH1k1 = ch;
        for ( int k = 0; k < l1; k++ )
        {
            FLINT tr2 = pCC11k[ 2 ] + pCC11k[ 4 ];
            FLINT cr2 = pCC11k[ 0 ] + taur * tr2;
            pCH1k1[ 0 ] = pCC11k[ 0 ] + tr2;
            FLINT ti2 = pCC11k[ 3 ] + pCC11k[ 5 ];
            FLINT ci2 = pCC11k[ 1 ] + taur * ti2;
            pCH1k1[ 1 ] = pCC11k[ 1 ] + ti2;
            FLINT cr3 = taui * ( pCC11k[ 2 ] - pCC11k[ 4 ] );
            FLINT ci3 = taui * ( pCC11k[ 3 ] - pCC11k[ 5 ] );
            pCH1k1[ ldo ] = cr2 - ci3;
            pCH1k1[ 2 * ldo ] = cr2 + ci3;
            pCH1k1[ 1 + ldo ] = ci2 + cr3;
            pCH1k1[ 1 + 2 * ldo ] = ci2 - cr3;
            pCC11k += 6;
            pCH1k1 += 2;
        }
    }
    else
    {
        FLOAT* pCC11k = cc;
        FLOAT* pCH1k1 = ch;
        for ( int k = 0; k < l1; k++ )
        {
            for ( int i = 1; i < ido; i += 2 )
            {
                FLINT tr2 = pCC11k[ i - 1 + ido ] + pCC11k[ i - 1 + 2 * ido ];
                FLINT cr2 = pCC11k[ i - 1 ] + taur * tr2;
                pCH1k1[ i - 1 ] = pCC11k[ i - 1 ] + tr2;
                FLINT ti2 = pCC11k[ i + ido ] + pCC11k[ i + 2 * ido ];
                FLINT ci2 = pCC11k[ i ] + taur * ti2;
                pCH1k1[ i ] = pCC11k[ i ] + ti2;
                FLINT cr3 = taui *
                    ( pCC11k[ i - 1 + ido ] - pCC11k[ i - 1 + 2 * ido ] );
                FLINT ci3 =
                    taui * ( pCC11k[ i + ido ] - pCC11k[ i + 2 * ido ] );
                FLINT dr2 = cr2 - ci3;
                FLINT dr3 = cr2 + ci3;
                FLINT di2 = ci2 + cr3;
                FLINT di3 = ci2 - cr3;
                pCH1k1[ i + ldo ] = wa1[ i - 1 ] * di2 + wa1[ i ] * dr2;
                pCH1k1[ i - 1 + ldo ] = wa1[ i - 1 ] * dr2 - wa1[ i ] * di2;
                pCH1k1[ i + 2 * ldo ] = wa2[ i - 1 ] * di3 + wa2[ i ] * dr3;
                pCH1k1[ i - 1 + 2 * ldo ] = wa2[ i - 1 ] * dr3 - wa2[ i ] * di3;
            }
            pCH1k1 += ido;
            pCC11k += 3 * ido;
        }
    }
}
static void
passb4(
    int ido, int l1, FLOAT* cc, FLOAT* ch, FLOAT* wa1, FLOAT* wa2, FLOAT* wa3 )
{
    if ( ido == 2 )
    {
        FLOAT* pCC11k = cc;
        FLOAT* pCH1k1 = ch;
        for ( int k = 0; k < l1; k++ )
        {
            FLINT ti1 = pCC11k[ 1 ] - pCC11k[ 5 ];
            FLINT ti2 = pCC11k[ 1 ] + pCC11k[ 5 ];
            FLINT tr4 = pCC11k[ 7 ] - pCC11k[ 3 ];
            FLINT ti3 = pCC11k[ 3 ] + pCC11k[ 7 ];
            FLINT tr1 = pCC11k[ 0 ] - pCC11k[ 4 ];
            FLINT tr2 = pCC11k[ 0 ] + pCC11k[ 4 ];
            FLINT ti4 = pCC11k[ 2 ] - pCC11k[ 6 ];
            FLINT tr3 = pCC11k[ 2 ] + pCC11k[ 6 ];
            pCH1k1[ 0 ] = tr2 + tr3;
            pCH1k1[ 4 * l1 ] = tr2 - tr3;
            pCH1k1[ 1 ] = ti2 + ti3;
            pCH1k1[ 1 + 4 * l1 ] = ti2 - ti3;
            pCH1k1[ l1 * 2 ] = tr1 + tr4;
            pCH1k1[ 6 * l1 ] = tr1 - tr4;
            pCH1k1[ 1 + 2 * l1 ] = ti1 + ti4;
            pCH1k1[ 1 + 6 * l1 ] = ti1 - ti4;
            pCH1k1 += 2;
            pCC11k += 8;
        }
    }
    else
    {
        FLOAT* pCC11k = cc;
        FLOAT* pCH1k1 = ch;
        int    ldo = ido * l1;
        for ( int k = 0; k < l1; k++ )
        {
            for ( int i = 1; i < ido; i += 2 )
            {
                FLINT ti1 = pCC11k[ i ] - pCC11k[ i + 2 * ido ];
                FLINT ti2 = pCC11k[ i ] + pCC11k[ i + 2 * ido ];
                FLINT ti3 = pCC11k[ i + ido ] + pCC11k[ i + 3 * ido ];
                FLINT tr4 = pCC11k[ i + 3 * ido ] - pCC11k[ i + ido ];

                FLINT tr1 = pCC11k[ i - 1 ] - pCC11k[ i - 1 + 2 * ido ];
                FLINT tr2 = pCC11k[ i - 1 ] + pCC11k[ i - 1 + 2 * ido ];
                FLINT ti4 = pCC11k[ i - 1 + ido ] - pCC11k[ i - 1 + 3 * ido ];
                FLINT tr3 = pCC11k[ i - 1 + ido ] + pCC11k[ i - 1 + 3 * ido ];
                pCH1k1[ i - 1 ] = tr2 + tr3;
                FLINT cr3 = tr2 - tr3;
                pCH1k1[ i ] = ti2 + ti3;
                FLINT ci3 = ti2 - ti3;
                FLINT cr2 = tr1 + tr4;
                FLINT cr4 = tr1 - tr4;
                FLINT ci2 = ti1 + ti4;
                FLINT ci4 = ti1 - ti4;
                pCH1k1[ i - 1 + ldo ] = wa1[ i - 1 ] * cr2 - wa1[ i ] * ci2;
                pCH1k1[ i + ldo ] = wa1[ i - 1 ] * ci2 + wa1[ i ] * cr2;
                pCH1k1[ i - 1 + 2 * ldo ] = wa2[ i - 1 ] * cr3 - wa2[ i ] * ci3;
                pCH1k1[ i + 2 * ldo ] = wa2[ i - 1 ] * ci3 + wa2[ i ] * cr3;
                pCH1k1[ i - 1 + 3 * ldo ] = wa3[ i - 1 ] * cr4 - wa3[ i ] * ci4;
                pCH1k1[ i + 3 * ldo ] = wa3[ i - 1 ] * ci4 + wa3[ i ] * cr4;
            }
            pCH1k1 += ido;
            pCC11k += 4 * ido;
        }
    }
}
static void
passb5( int    ido,
        int    l1,
        FLOAT* cc,
        FLOAT* ch,
        FLOAT* wa1,
        FLOAT* wa2,
        FLOAT* wa3,
        FLOAT* wa4 )
{
    static FLINT tr11 = 0.309016994374947;
    static FLINT ti11 = 0.951056516295154;
    static FLINT tr12 = -.809016994374947;
    static FLINT ti12 = 0.587785252292473;
    int          ldo = l1 * ido;
    if ( ido == 2 )
    {
        FLOAT* pCC11k = cc;
        FLOAT* pCH1k1 = ch;
        for ( int k = 0; k < l1; k++ )
        {
            FLINT ti5 = pCC11k[ 3 ] - pCC11k[ 9 ];
            FLINT ti2 = pCC11k[ 3 ] + pCC11k[ 9 ];
            FLINT ti4 = pCC11k[ 5 ] - pCC11k[ 7 ];
            FLINT ti3 = pCC11k[ 5 ] + pCC11k[ 7 ];
            FLINT tr5 = pCC11k[ 2 ] - pCC11k[ 8 ];
            FLINT tr2 = pCC11k[ 2 ] + pCC11k[ 8 ];
            FLINT tr4 = pCC11k[ 4 ] - pCC11k[ 6 ];
            FLINT tr3 = pCC11k[ 4 ] + pCC11k[ 6 ];
            pCH1k1[ 0 ] = pCC11k[ 0 ] + tr2 + tr3;
            pCH1k1[ 1 ] = pCC11k[ 1 ] + ti2 + ti3;
            FLINT cr2 = pCC11k[ 0 ] + tr11 * tr2 + tr12 * tr3;
            FLINT ci2 = pCC11k[ 1 ] + tr11 * ti2 + tr12 * ti3;
            FLINT cr3 = pCC11k[ 0 ] + tr12 * tr2 + tr11 * tr3;
            FLINT ci3 = pCC11k[ 1 ] + tr12 * ti2 + tr11 * ti3;
            FLINT cr5 = ti11 * tr5 + ti12 * tr4;
            FLINT ci5 = ti11 * ti5 + ti12 * ti4;
            FLINT cr4 = ti12 * tr5 - ti11 * tr4;
            FLINT ci4 = ti12 * ti5 - ti11 * ti4;
            pCH1k1[ ldo ] = cr2 - ci5;
            pCH1k1[ 4 * ldo ] = cr2 + ci5;
            pCH1k1[ 1 + ldo ] = ci2 + cr5;
            pCH1k1[ 1 + 2 * ldo ] = ci3 + cr4;
            pCH1k1[ 2 * ldo ] = cr3 - ci4;
            pCH1k1[ 3 * ldo ] = cr3 + ci4;
            pCH1k1[ 1 + 3 * ldo ] = ci3 - cr4;
            pCH1k1[ 1 + 4 * ldo ] = ci2 - cr5;
            pCH1k1 += 2;
            pCC11k += 10;
        }
    }
    else
    {
        FLOAT* pCC11k = cc;
        FLOAT* pCH1k1 = ch;
        for ( int k = 0; k < l1; k++ )
        {
            for ( int i = 1; i < ido; i += 2 )
            {
                FLINT ti5 = pCC11k[ i + ido ] - pCC11k[ i + 4 * ido ];
                FLINT ti2 = pCC11k[ i + ido ] + pCC11k[ i + 4 * ido ];
                FLINT ti4 = pCC11k[ i + 2 * ido ] - pCC11k[ i + 3 * ido ];
                FLINT ti3 = pCC11k[ i + 2 * ido ] + pCC11k[ i + 3 * ido ];
                FLINT tr5 = pCC11k[ i - 1 + ido ] - pCC11k[ i - 1 + 4 * ido ];
                FLINT tr2 = pCC11k[ i - 1 + ido ] + pCC11k[ i - 1 + 4 * ido ];
                FLINT tr4 =
                    pCC11k[ i - 1 + 2 * ido ] - pCC11k[ i - 1 + 3 * ido ];
                FLINT tr3 =
                    pCC11k[ i - 1 + 2 * ido ] + pCC11k[ i - 1 + 3 * ido ];
                pCH1k1[ i - 1 ] = pCC11k[ i - 1 ] + tr2 + tr3;
                pCH1k1[ i ] = pCC11k[ i ] + ti2 + ti3;
                FLINT cr2 = pCC11k[ i - 1 ] + tr11 * tr2 + tr12 * tr3;
                FLINT ci2 = pCC11k[ i ] + tr11 * ti2 + tr12 * ti3;
                FLINT cr3 = pCC11k[ i - 1 ] + tr12 * tr2 + tr11 * tr3;
                FLINT ci3 = pCC11k[ i ] + tr12 * ti2 + tr11 * ti3;
                FLINT cr5 = ti11 * tr5 + ti12 * tr4;
                FLINT ci5 = ti11 * ti5 + ti12 * ti4;
                FLINT cr4 = ti12 * tr5 - ti11 * tr4;
                FLINT ci4 = ti12 * ti5 - ti11 * ti4;
                FLINT dr3 = cr3 - ci4;
                FLINT dr4 = cr3 + ci4;
                FLINT di3 = ci3 + cr4;
                FLINT di4 = ci3 - cr4;
                FLINT dr5 = cr2 + ci5;
                FLINT dr2 = cr2 - ci5;
                FLINT di5 = ci2 - cr5;
                FLINT di2 = ci2 + cr5;
                pCH1k1[ i - 1 + ldo ] = wa1[ i - 1 ] * dr2 - wa1[ i ] * di2;
                pCH1k1[ i + ldo ] = wa1[ i - 1 ] * di2 + wa1[ i ] * dr2;
                pCH1k1[ i - 1 + 2 * ldo ] = wa2[ i - 1 ] * dr3 - wa2[ i ] * di3;
                pCH1k1[ i + 2 * ldo ] = wa2[ i - 1 ] * di3 + wa2[ i ] * dr3;
                pCH1k1[ i - 1 + 3 * ldo ] = wa3[ i - 1 ] * dr4 - wa3[ i ] * di4;
                pCH1k1[ i + 3 * ldo ] = wa3[ i - 1 ] * di4 + wa3[ i ] * dr4;
                pCH1k1[ i - 1 + 4 * ldo ] = wa4[ i - 1 ] * dr5 - wa4[ i ] * di5;
                pCH1k1[ i + 4 * ldo ] = wa4[ i - 1 ] * di5 + wa4[ i ] * dr5;
            }
            pCH1k1 += ido;
            pCC11k += 5 * ido;
        }
    }
}
