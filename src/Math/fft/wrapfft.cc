/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "wrapfft.hh"
#include <stdexcept>
#include <cstring>

using namespace std;
using ::thread::semlock;

//======================================  Constructor
wrapfft::wrapfft( void )
{
}

//======================================  Destructor
wrapfft::~wrapfft( void )
{
    semlock lock( fft_mux, semlock::wrlock );
    for ( plan_iter i = fft_plan_map.begin( ); i != fft_plan_map.end( ); ++i )
    {
        delete i->second;
        i->second = 0;
    }
}

//======================================  Fetch a plan pointer.
bool
wrapfft::fetch_plans( const std::string& file )
{
    return false;
}

//======================================  Fetch a plan pointer.
plan_pointer*
wrapfft::find_plan( plan_id pid )
{
    semlock         lock( fft_mux, semlock::rdlock );
    const_plan_iter iter = fft_plan_map.find( pid );
    if ( iter == fft_plan_map.end( ) )
        return 0;
    return iter->second;
}

//======================================  Get plan ID
wrapfft::plan_id
wrapfft::get_plan_id( fft_dirtype type, long N )
{
    return ( N << FFT_DIRTYPE_FIELD_WIDTH ) + long( type );
}

//======================================  Fetch a plan pointer.
plan_pointer*
wrapfft::make_plan( plan_id )
{
    return 0;
}

//======================================  Chose an optimal length > N
long
wrapfft::pick_length( long N ) const
{
    return N;
}

//======================================  Inline reorder template.
template < class T >
inline void
reorder_template( const T* in, T* out, long N, wrapfft::reorder_type dir )
{
    long nPos = ( N + 1 ) >> 1;
    long nNeg = N - nPos;

    //----------------------------------  Copy reordered data.
    if ( in != out )
    {
        switch ( dir )
        {
        case wrapfft::reorder_dft_to_freq:
            memcpy( out, in + nPos, nNeg * sizeof( T ) );
            memcpy( out + nNeg, in, nPos * sizeof( T ) );
            break;
        case wrapfft::reorder_freq_to_dft:
            memcpy( out, in + nNeg, nPos * sizeof( T ) );
            memcpy( out + nPos, in, nNeg * sizeof( T ) );
            break;
        }

        //----------------------------------  Reorder even length series in-place.
    }
    else if ( nPos == nNeg )
    {
        T* pP = out + nPos;
        T* pN = out;
        for ( long i = 0; i < nPos; ++i )
        {
            T t = *pN;
            *pN++ = *pP;
            *pP++ = t;
        }

        //----------------------------------  Reorder odd length series in-place.
    }
    else
    {
        T* pP; // positive f's in output (f) ordering
        T* pN;
        T  t;
        switch ( dir )
        {
        case wrapfft::reorder_dft_to_freq:
            pP = out + nPos; // positive f's in output (f) ordering
            pN = out;
            t = *pN; // Save the DC component
            for ( long i = 0; i < nNeg; ++i )
            {
                *pN++ = *pP;
                *pP++ = *pN;
            }
            out[ nNeg ] = t;
            break;

        case wrapfft::reorder_freq_to_dft:
            pP = out + nNeg; // end of positive f's in dft ordering
            pN = out + N - 1;
            t = *pP; // Save the DC component
            for ( long i = 0; i < nNeg; ++i )
            {
                *pP-- = *pN;
                *pN-- = *pP;
            }
            out[ 0 ] = t;
            break;
        }
    }
}

//======================================  Reorder float data.
void
wrapfft::reorder( const std::complex< float >* in,
                  std::complex< float >*       out,
                  long                         N,
                  reorder_type                 dir ) const
{
    reorder_template( in, out, N, dir );
}

//======================================  Reorder double data.
void
wrapfft::reorder( const std::complex< double >* in,
                  std::complex< double >*       out,
                  long                          N,
                  reorder_type                  dir ) const
{
    reorder_template( in, out, N, dir );
}

//======================================  Set the plan for the specified ID.
void
wrapfft::set_plan( plan_id pid, plan_pointer& p )
{
    semlock   lock( fft_mux, semlock::wrlock );
    plan_iter iter = fft_plan_map.find( pid );
    if ( iter != fft_plan_map.end( ) )
    {
        delete iter->second;
        iter->second = p.clone( );
    }
    else
    {
        fft_plan_map.insert( plan_node( pid, p.clone( ) ) );
    }
}

//======================================  Double precision complex transform
void
wrapfft::transform( long N, std::complex< double >* out )
{
    throw std::runtime_error( "wrapfft: Complex double DFT not implemented" );
}

//======================================  Double precision real transform
void
wrapfft::transform( long N, const double* in, std::complex< double >* out )
{
    throw std::runtime_error( "wrapfft: Double real DFT not implemented" );
}

//======================================  Double precision inverse transform
void
wrapfft::transform( long N, const std::complex< double >* in, double* out )
{
    throw std::runtime_error( "wrapfft: Double inverse DFT not implemented" );
}

//======================================  Single precision complex transform
void
wrapfft::transform( long N, std::complex< float >* out )
{
    throw std::runtime_error( "wrapfft: Complex double DFT not implemented" );
}

//======================================  Single precision real transform
void
wrapfft::transform( long N, const float* in, std::complex< float >* out )
{
    throw std::runtime_error( "wrapfft: Float real DFT not implemented" );
}

//======================================  Single precision inverse transform
void
wrapfft::transform( long N, const std::complex< float >* in, float* out )
{
    throw std::runtime_error( "wrapfft: Float inverse DFT not implemented" );
}

plan_pointer::plan_pointer( void )
{
}

plan_pointer::~plan_pointer( void )
{
    kill( );
}

void
plan_pointer::kill( void )
{
}

void*
plan_pointer::release( void )
{
    return 0;
}

void
plan_pointer::reset( void* )
{
}
