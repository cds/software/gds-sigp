/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifdef FFTW3
#include "wrapfftw.hh"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <errno.h>

using namespace std;
using ::thread::semlock;

//======================================  Constructor
wrapfftw::wrapfftw( void )
{
    const char* wf = getenv( "FFTW_WISDOM" );
    if ( wf && *wf )
        fetch_plans( wf );
}

//======================================  Destructor
wrapfftw::~wrapfftw( void )
{
}

//======================================  Implement stream_char function
extern "C" {
int
get_stream_char( void* is )
{
    return reinterpret_cast< std::istream* >( is )->get( );
}
}

//======================================  Fetch a plan pointer.
bool
wrapfftw::fetch_plans( const std::string& file )
{
    int rc = 0;

    //----------------------------------  Import system wisdom if path is empty
    if ( file.empty( ) )
    {
        rc = fftw_import_system_wisdom( );
        cout << "import_system_wisdom() = " << rc << endl;
    }

    //----------------------------------  Import wisdom from path.
    else
    {
#ifdef IMPORT_WISDOM_FROM_FILE
        errno = 0;
        FILE* f = fopen( file.c_str( ), "r" );
        if ( f != NULL )
        {
            rc = fftw_import_wisdom_from_file( f );
            cout << "import_wisdom_from_file(\"" << file << "\") = " << rc
                 << endl;
            if ( !rc )
                perror( "failed to read fftw wisdom file" );
            fclose( f );
        }
        else
        {
            perror( "failed to open fftw wisdom file" );
        }
#else
        ifstream f( file.c_str( ) );
        if ( !f.is_open( ) )
        {
            perror( "failed to open fftw wisdom file" );
            cerr << "Wisdom file: " << file << endl;
            return true;
        }
        errno = 0;
        rc = fftw_import_wisdom( &get_stream_char, &f );
        if ( !rc && errno )
            perror( "failed to read fftw wisdom file" );
        cout << "import_wisdom(\"" << file << "\") = " << rc << endl;
#endif
    }
    return rc != 0;
}

//======================================  Find the specified plan
fftw_plan_pointer*
wrapfftw::find_plan( plan_id id )
{
    return dynamic_cast< fftw_plan_pointer* >( wrapfft::find_plan( id ) );
}

//======================================  reduce length
inline long
reduce_len( long r0, long N, long p, long q )
{
    long r = r0;
    while ( ( r % q ) == 0 && r * p / q >= N )
        r = r * p / q;
    return r;
}

//======================================  Pick an efficient DFT length
long
wrapfftw::pick_length( long N ) const
{

    //----------------------------------  Find the nearest power of 2
    long r = 1;
    while ( r < N )
        r *= 2;
    if ( r == N )
        return r;

    //----------------------------------  See if we can make it closer
    r = reduce_len( r, N, 3, 4 );
    r = reduce_len( r, N, 5, 6 );
    r = reduce_len( r, N, 15, 16 );
    return r;
}

//======================================  Double precision complex transform
void
wrapfftw::transform( long N, std::complex< double >* out )
{
#ifdef FFT_DOUBLE
    fftw_complex* dout = (fftw_complex*)out;
    //----------------------------------  Get the plan id
    plan_id id;
    if ( N < 0 )
        id = get_plan_id( fft_dcomplex_backward, -N );
    else
        id = get_plan_id( fft_dcomplex_forward, N );

    //----------------------------------  Get a plan pointer
    fftw_plan_pointer* pp = find_plan( id );

    //----------------------------------  Use an existing plan
    fftw_plan p;
    if ( pp )
    {
        p = pp->get_fftwd_plan( );

        //----------------------------------  Generate a plan.
    }
    else if ( N < 0 )
    {
        semlock wlock( mux, semlock::wrlock );
        p = fftw_plan_dft_1d( -N, dout, dout, FFTW_BACKWARD, FFTW_ESTIMATE );
        fftw_plan_pointer pptr( p, fft_dcomplex_backward );
        set_plan( id, pptr );
    }
    else
    {
        semlock wlock( mux, semlock::wrlock );
        p = fftw_plan_dft_1d( N, dout, dout, FFTW_FORWARD, FFTW_ESTIMATE );
        fftw_plan_pointer pptr( p, fft_dcomplex_forward );
        set_plan( id, pptr );
    }

    //----------------------------------  Execute the plan
    semlock rlock( mux, semlock::rdlock );
    fftw_execute_dft( p, dout, dout );
#else
    throw std::runtime_error( "Double precision fft not available" );
#endif
}

//======================================  Double precision real transform
void
wrapfftw::transform( long N, const double* in, std::complex< double >* out )
{
#ifdef FFT_DOUBLE
    fftw_complex*      dout = (fftw_complex*)out;
    plan_id            id = get_plan_id( fft_double_dcomplex, N );
    fftw_plan_pointer* pp = find_plan( id );
    fftw_plan          p;
    if ( pp )
    {
        p = pp->get_fftwd_plan( );
    }
    else
    {
        semlock wlock( mux, semlock::wrlock );
        p = fftw_plan_dft_r2c_1d( N, (double*)in, dout, FFTW_ESTIMATE );
        fftw_plan_pointer pptr( p, fft_double_dcomplex );
        set_plan( id, pptr );
    }
    semlock rlock( mux, semlock::rdlock );
    fftw_execute_dft_r2c( p, (double*)in, dout );
#else
    throw std::runtime_error( "Double precision fft not available" );
#endif
}

//======================================  Double precision inverse transform
void
wrapfftw::transform( long N, const std::complex< double >* in, double* out )
{
#ifdef FFT_DOUBLE
    fftw_complex*      din = (fftw_complex*)in;
    plan_id            id = get_plan_id( fft_dcomplex_double, N );
    fftw_plan_pointer* pp = find_plan( id );
    fftw_plan          p;
    if ( pp )
    {
        p = pp->get_fftwd_plan( );
    }
    else
    {
        semlock wlock( mux, semlock::wrlock );
        p = fftw_plan_dft_c2r_1d( N, din, out, FFTW_ESTIMATE );
        fftw_plan_pointer pptr( p, fft_dcomplex_double );
        set_plan( id, pptr );
    }
    semlock rlock( mux, semlock::rdlock );
    fftw_execute_dft_c2r( p, din, out );
#else
    throw std::runtime_error( "Double precision fft not available" );
#endif
}

//======================================  Single precision complex transform
void
wrapfftw::transform( long N, std::complex< float >* out )
{
    fftwf_complex* fout = (fftwf_complex*)out;

    //----------------------------------  Get the plan id
    plan_id id;
    if ( N < 0 )
        id = get_plan_id( fft_fcomplex_backward, -N );
    else
        id = get_plan_id( fft_fcomplex_forward, N );

    //----------------------------------  Get a plan pointer
    fftw_plan_pointer* pp = find_plan( id );

    //----------------------------------  Use an existing plan
    fftwf_plan p;
    if ( pp )
    {
        p = pp->get_fftwf_plan( );

        //----------------------------------  Generate a plan.
    }
    else if ( N < 0 )
    {
        semlock wlock( mux, semlock::wrlock );
        p = fftwf_plan_dft_1d( -N, fout, fout, FFTW_BACKWARD, FFTW_ESTIMATE );
        fftw_plan_pointer pptr( p, fft_fcomplex_backward );
        set_plan( id, pptr );
    }
    else
    {
        semlock wlock( mux, semlock::wrlock );
        p = fftwf_plan_dft_1d( N, fout, fout, FFTW_FORWARD, FFTW_ESTIMATE );
        fftw_plan_pointer pptr( p, fft_fcomplex_forward );
        set_plan( id, pptr );
    }

    //----------------------------------  Execute the plan
    semlock rlock( mux, semlock::rdlock );
    fftwf_execute_dft( p, fout, fout );
}

//======================================  Single precision real transform
void
wrapfftw::transform( long N, const float* in, std::complex< float >* out )
{
    fftwf_complex* fout = (fftwf_complex*)out;

    plan_id            id = get_plan_id( fft_float_fcomplex, N );
    fftw_plan_pointer* pp = find_plan( id );
    fftwf_plan         p;
    if ( pp )
    {
        p = pp->get_fftwf_plan( );
    }
    else
    {
        semlock wlock( mux, semlock::wrlock );
        p = fftwf_plan_dft_r2c_1d( N, (float*)in, fout, FFTW_ESTIMATE );
        fftw_plan_pointer pptr( p, fft_float_fcomplex );
        set_plan( id, pptr );
    }
    semlock rlock( mux, semlock::rdlock );
    fftwf_execute_dft_r2c( p, (float*)in, fout );
}

//======================================  Single precision inverse transform
void
wrapfftw::transform( long N, const std::complex< float >* in, float* out )
{
    fftwf_complex*     fin = (fftwf_complex*)in;
    plan_id            id = get_plan_id( fft_fcomplex_float, N );
    fftw_plan_pointer* pp = find_plan( id );
    fftwf_plan         p;
    if ( pp )
    {
        p = pp->get_fftwf_plan( );
    }
    else
    {
        semlock wlock( mux, semlock::wrlock );
        p = fftwf_plan_dft_c2r_1d( N, fin, out, FFTW_ESTIMATE );
        fftw_plan_pointer pptr( p, fft_fcomplex_float );
        set_plan( id, pptr );
    }
    semlock rlock( mux, semlock::rdlock );
    fftwf_execute_dft_c2r( p, fin, out );
}

//======================================  fftw plan pointer constructor
fftw_plan_pointer::fftw_plan_pointer( void* plan, wrapfft::fft_dirtype dirt )
    : _type( dirt ), _plan( plan )
{
}

//======================================  fftw plan pointer destructor
fftw_plan_pointer::~fftw_plan_pointer( void )
{
    kill( );
}

//======================================  Clone an fftw plan pointer
fftw_plan_pointer*
fftw_plan_pointer::clone( void )
{
    return new fftw_plan_pointer( release( ), _type );
}

//======================================  Kill an fftw plan
void
fftw_plan_pointer::kill( void )
{
    if ( !_plan )
        return;
    switch ( _type )
    {
    case wrapfft::fft_float_fcomplex:
    case wrapfft::fft_fcomplex_float:
    case wrapfft::fft_fcomplex_forward:
    case wrapfft::fft_fcomplex_backward:
        fftwf_destroy_plan( get_fftwf_plan( ) );
        break;
    case wrapfft::fft_double_dcomplex:
    case wrapfft::fft_dcomplex_double:
    case wrapfft::fft_dcomplex_forward:
    case wrapfft::fft_dcomplex_backward:
#ifdef FFT_DOUBLE
        fftw_destroy_plan( get_fftwd_plan( ) );
#endif
        break;
    }
    _plan = 0;
}

//======================================  Get the plan address
void*
fftw_plan_pointer::release( void )
{
    void* r = _plan;
    _plan = 0;
    return r;
}

//======================================  Reset plan address
void
fftw_plan_pointer::reset( void* p )
{
    if ( _plan )
        kill( );
    _plan = p;
}
#endif
