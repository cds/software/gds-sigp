//
//    real fft functions
//
//
#include "fftpak.hh"
#include "constant.hh"
#include "PConfig.h"
#include <math.h>
#include <string.h>

void
rffti( int N, FLOAT* ws )
{
    cffti( N / 2, ws );
}

//--------------------------------------  Real fourrier transform
void
rfftf( int n, const FLOAT* r, COMPLEX* c, FLOAT* ws )
{
    int novr2 = n / 2;
    if ( r != reinterpret_cast< const FLOAT* >( c ) )
        memcpy( c, r, n * sizeof( FLOAT ) );
    cfftf( novr2, reinterpret_cast< FLOAT* >( c ), ws );
    int    center = ( novr2 + 1 ) / 2;
    double dphi = twopi / n;
    double arg = 0;
#ifdef USE_SINCOS
    double sarg( 0 ), carg( 0 );
#endif
    for ( int k = 1; k < center; k++ )
    {
        int   j = novr2 - k;
        FLOAT rsum = c[ k ].real( ) + c[ j ].real( );
        FLOAT rdif = c[ k ].real( ) - c[ j ].real( );
        FLOAT isum = c[ k ].imag( ) + c[ j ].imag( );
        FLOAT idif = c[ k ].imag( ) - c[ j ].imag( );
        arg += dphi;

#ifdef USE_SINCOS
        sincos( arg, &sarg, &carg );
#else
        double sarg = sin( arg );
        double carg = cos( arg );
#endif
        c[ k ] = COMPLEX( rsum + carg * isum - sarg * rdif,
                          idif - sarg * isum - carg * rdif );
        c[ k ] *= 0.5;
        c[ j ] = c[ k ] - COMPLEX( carg * isum - sarg * rdif, idif );
    }
    COMPLEX c0 = c[ 0 ];
    c[ 0 ] = COMPLEX( c0.real( ) + c0.imag( ) );
    c[ novr2 ] = COMPLEX( c0.real( ) - c0.imag( ) );
    if ( !( novr2 & 1 ) )
        c[ center ] = ~c[ center ];
}
