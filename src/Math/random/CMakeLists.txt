
add_library(random OBJECT
    rndm.cc
)

target_include_directories(random PRIVATE
    ${CMAKE_SOURCE_DIR}/src/Base/constant
)

install(FILES
    rndm.hh
    DESTINATION include/gds-sigp
)
