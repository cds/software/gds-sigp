/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef RANDNUM_HH
#define RANDNUM_HH
#include <string>

/**  Generate a single random number in the the range 0-1.
 */
double Rndm( void );

/**  Set the random number seed.
 */
void RndmSeed( unsigned long seed );

/**  Generate two gaussianly distributed random numbers.
 */
void   Rannor( double& a, double& b );
double Rannor( void );

/**  Generate a poisson number.
 */
long PoissonRndm( double x );

/**  Save the random number generator state.
 */
void SaveRndmState( const std::string& file );

/**  Restore the random number generator state.
 */
void RestoreRndmState( const std::string& file );

#endif // RANDNUM_HH
