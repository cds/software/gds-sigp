/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "SquareWave.hh"
#include "Time.hh"
#include "constant.hh"
#include <cmath>

#define fMod( x, y ) ( ( x ) - (y)*floor( ( x ) / ( y ) ) )

//-------------------------------------  normalized phase.
inline double
normPhase( double phi )
{
    if ( ( phi >= 0 ) && ( phi < twopi ) )
    {
        return phi;
    }
    else
    {
        return fMod( phi, twopi );
    }
}

//-------------------------------------  SquareWave Constructor.
SquareWave::SquareWave(
    double Freq, double ampl, double phi0, Interval dT, const Time& t0 )
    : Chirp( t0, t0 + dT, t0 ), mPhic( phi0 ), mAmpl( ampl )
{
    mOmega = twopi * Freq;
}

//-------------------------------------  Frequency domain template bin.
Chirp::complex_type
SquareWave::Fspace( double Freq, double dF ) const
{
    complex_type r( 0 );
    double       f = mOmega / twopi;
    if ( f <= 0 )
    {
        return r;
    }
    int n = (int)( 2 * Freq / f + 1.5 );
    if ( n < 10 )
        n = 10;
    for ( int i = 0; i < n; ++i )
    {
        double x = -pow( ( Freq - double( 2 * i + 1 ) * f ) / dF, 2 );
        r += mAmpl * exp( complex_type( x, mPhic ) ) / double( 2 * i + 1 );
    }
    return 4 / pi * r;
}

//-------------------------------------  Time domain template bin.
double
SquareWave::Tspace( const Time& t0 ) const
{
    if ( normPhase( phi( t0 ) ) < pi )
    {
        return Ampl( t0 );
    }
    else
    {
        return -Ampl( t0 );
    }
}

//-------------------------------------  Waveform Frequency.
double
SquareWave::freq( const Time& t ) const
{
    return mOmega / twopi;
}

//-------------------------------------  Waveform Phase angle.
double
SquareWave::phi( const Time& t ) const
{
    return mOmega * double( t - mTc ) - mPhic;
}

//-------------------------------------  Amplitude.
double
SquareWave::Ampl( const Time& t ) const
{
    return mAmpl;
}

void
SquareWave::setAmp( double amp )
{
    mAmpl = amp;
}
