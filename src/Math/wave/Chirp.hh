#ifndef CHIRP_HH
#define CHIRP_HH
//
//    Class to define astrophysics templates
//
#include "Complex.hh"
#include "Time.hh"

/**  The chirp class defines a waveform function API that may be used to 
  *  generate templates.
  *  \brief Base class of astrophysical template functions.
  */
class Chirp
{
public:
    typedef dComplex complex_type; ///< complex data type use by chirp

    /** Construct a virtual %Chirp base class instance and initialize the 
   *  three time parameters.
   *  \brief chirp class construct
   *  \param tStart Start time of generated signal.
   *  \param tEnd   End time of generated signal.
   *  \param tCrit  Signal dependent critical time.
   */
    Chirp( const Time& tStart = Time( 0 ),
           const Time& tEnd = Time( 0 ),
           const Time& tCrit = Time( 0 ) );

    /**  The destructor is defined to be virtual in case the derived 
    *  template classes need to perform cleanup or ststistical processing.
    *  \brief Chirp destructor.
    */
    virtual ~Chirp( void )
    {
    }

    /**  Interface to a function which will return the frequency domain 
    *  representation of the astrophysical waveform.
    *  \brief Template representation in the frequency domain.
    *  \param Freq Frequency at which signal is to be sampled
    *  \param dF   Frequency step
    *  \returns complex f-domain coefficient of signal.
    */
    virtual complex_type Fspace( double Freq, double dF = 0 ) const = 0;

    /**  Interface to a function which will return the time domain 
    *  representation of the astrophysical waveform.
    *  \brief Template representation in the time domain.
    *  \param t0 %Time at which signal is to be sampled.
    *  \returns Signal amplitude at the specified time.
    */
    virtual double Tspace( const Time& t0 ) const = 0;

    /**  Return a time domain representation of the astrophysical waveform
    *  heterodyned by the specified frequency.
    *  \brief Template representation in the time domain.
    *  \param t0 %Time at which signal is to be sampled.
    *  \param f0 heterodyne frequency
    *  \returns Complex amplitude of heterodyned signal at the specified time.
    */
    complex_type Tspace( const Time& t0, double f0 ) const;

    //-----------------------------------  Accessors
    /** The getT0 method returns the earliest time for which the signal 
   *  waveform is defined.
   *  \brief Get the starting time.
   *  \returns Signal start time
   */
    virtual Time getT0( void ) const;

    /**  Returns the time at which the phase is defined. This is typically the 
    *  statr time, but in the case of an inspiral waveform it is the 
    *  coalescence time.
    *  \brief Get the critical time.
    *  \returns Critical time.
    */
    virtual Time getTc( void ) const;

    /**  GetTEnd returns the latest time for which the waveform is defined.
    *  \brief Get the end time.
    *  \returns End time.
    */
    virtual Time getTEnd( void ) const;

    /** TvsF(f) returns the absolute (GPS) time at which the in-spiral 
    *  frequency is exactly 'f'. This is defined only for monotonically 
    *  changing frequency signals.
    *  \brief Time versus Frequency.
    *  \param f Frequency argument
    *  \returns %Time at which signal crosses the specified frequency.
    */
    virtual Time TvsF( double f ) const;

    /** freq(t) returns the waveform frequency at Absolute time t. This 
    *  method returns zero or fNy/2 if the frequency is not a single-valued 
    *  function of time.
    *  \brief Frequency at specified time
    *  \param t %Time to be interrogated
    *  \returns Frequency at the specified time.
    */
    virtual double freq( const Time& t ) const;

    /**  phi(t) returns the phase angle at absolute time t. This 
    *  method returns zero if the phase is not a single-valued 
    *  function of time.
    *  \brief Phase at specified time
    *  \param t %Time to be interrogated
    *  \returns Phase at the specified time.
    */
    virtual double phi( const Time& t ) const;

    /** Ampl(t) returns the wave-form amplitude at a specific time.
    *  \brief Amplitude at specified time
    *  \param t %Time to be interrogated
    *  \returns Amplitude at the specified time.
    */
    virtual double Ampl( const Time& t ) const;

protected:
    Time mT0; ///< Start time of generated signal
    Time mTl; ///< End time of generated signal
    Time mTc; ///< Critical time of generated signal
};

//======================================  Chirp class inline methods
inline Time
Chirp::getT0( void ) const
{
    return mT0;
}

inline Time
Chirp::getTEnd( void ) const
{
    return mTl;
}

inline Time
Chirp::getTc( void ) const
{
    return mTc;
}

#endif //   CHIRP_HH
