/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "Sine.hh"
#include "Time.hh"
#include "constant.hh"
#include <cmath>

//-------------------------------------  Sine Constructor.
Sine::Sine( double Freq, double ampl, double phi0, Interval dT, const Time& t0 )
    : Chirp( t0, t0 + dT, t0 ), mPhic( phi0 ), mAmpl( ampl )
{
    mOmega = twopi * Freq;
}

//-------------------------------------  Frequency domain template bin.
Chirp::complex_type
Sine::Fspace( double Freq, double dF ) const
{
    complex_type r( -pow( ( Freq - mOmega / twopi ) / dF, 2 ), mPhic );
    return exp( r );
}

//-------------------------------------  Time domain template bin.
double
Sine::Tspace( const Time& t0 ) const
{
    return Ampl( t0 ) * sin( phi( t0 ) );
}

//-------------------------------------  Waveform Frequency.
double
Sine::freq( const Time& t ) const
{
    return mOmega / twopi;
}

//-------------------------------------  Waveform Phase angle.
double
Sine::phi( const Time& t ) const
{
    return mOmega * double( t - mTc ) + mPhic;
}

//-------------------------------------  Amplitude.
double
Sine::Ampl( const Time& t ) const
{
    return mAmpl;
}

void
Sine::setAmp( double amp )
{
    mAmpl = amp;
}
