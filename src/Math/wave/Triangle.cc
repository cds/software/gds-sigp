/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "Triangle.hh"
#include "Time.hh"
#include "constant.hh"
#include <cmath>

#define fMod( x, y ) ( ( x ) - (y)*floor( ( x ) / ( y ) ) )

//-------------------------------------  normalized phase.
inline double
normPhase( double phi )
{
    if ( ( phi >= 0 ) && ( phi < twopi ) )
    {
        return phi;
    }
    else
    {
        return fMod( phi, twopi );
    }
}

//-------------------------------------  Triangle Constructor.
Triangle::Triangle(
    double Freq, double ampl, double phi0, Interval dT, const Time& t0 )
    : Chirp( t0, t0 + dT, t0 ), mPhic( phi0 ), mAmpl( ampl )
{
    mOmega = twopi * Freq;
}

//-------------------------------------  Frequency domain template bin.
Chirp::complex_type
Triangle::Fspace( double Freq, double dF ) const
{
    complex_type r( 0 );
    double       f = mOmega / twopi;
    if ( f <= 0 )
    {
        return r;
    }
    int n = (int)( 2 * Freq / f + 1.5 );
    if ( n < 10 )
        n = 10;
    for ( int i = 0; i < n; ++i )
    {
        double x = -pow( ( Freq - double( 2 * i + 1 ) * f ) / dF, 2 );
        r += mAmpl * exp( complex_type( x, mPhic + i * pi ) ) /
            pow( double( 2 * i + 1 ), 2 );
    }
    return 8. / ( pi * pi ) * r;
}

//-------------------------------------  Time domain template bin.
double
Triangle::Tspace( const Time& t0 ) const
{
    double x;
    if ( ( x = normPhase( phi( t0 ) ) ) < pi )
    {
        return Ampl( t0 ) * ( x * 2. / pi - 1. );
    }
    else
    {
        return Ampl( t0 ) * ( 3. - x * 2. / pi );
    }
}

//-------------------------------------  Waveform Frequency.
double
Triangle::freq( const Time& t ) const
{
    return mOmega / twopi;
}

//-------------------------------------  Waveform Phase angle.
double
Triangle::phi( const Time& t ) const
{
    return mOmega * double( t - mTc ) - mPhic;
}

//-------------------------------------  Amplitude.
double
Triangle::Ampl( const Time& t ) const
{
    return mAmpl;
}

void
Triangle::setAmp( double amp )
{
    mAmpl = amp;
}
