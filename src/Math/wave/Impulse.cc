/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "Impulse.hh"
#include "Time.hh"
#include "constant.hh"
#include <cmath>

//-------------------------------------  Impulse Constructor.
Impulse::Impulse(
    Interval dur, double ampl, Interval delay, Interval dT, const Time& t0 )
    : Chirp( t0, t0 + dT, t0 ), mAmpl( ampl ), mDuration( dur ), mDelay( delay )
{
}

//-------------------------------------  Frequency domain template bin.
Chirp::complex_type
Impulse::Fspace( double Freq, double dF ) const
{
    return complex_type( 0, 0 );
}

//-------------------------------------  Time domain template bin.
double
Impulse::Tspace( const Time& t0 ) const
{
    return Ampl( t0 );
}

//-------------------------------------  Amplitude.
double
Impulse::Ampl( const Time& t ) const
{
    return ( ( ( t - mT0 ) >= mDelay ) && ( ( t - mT0 ) < mDelay + mDuration ) )
        ? mAmpl
        : 0;
}

void
Impulse::setAmp( double amp )
{
    mAmpl = amp;
}

//-------------------------------------  Duration.
Interval
Impulse::Duration( void ) const
{
    return mDuration;
}

void
Impulse::setDuration( Interval dur )
{
    mDuration = dur;
}

//-------------------------------------  Delay.
Interval
Impulse::Delay( void ) const
{
    return mDelay;
}

void
Impulse::setDelay( Interval delay )
{
    mDelay = delay;
}
