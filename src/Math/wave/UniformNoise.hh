/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef UniformNoise_HH
#define UniformNoise_HH
#include "Chirp.hh"
#include "Interval.hh"

/**  UniformNoise is a template generator based on the Chirp class. It 
 *  generates uniform noise of unit amplitude.
 *  \brief Uniform noise template generator.
 */
class UniformNoise : public Chirp
{
public:
    /** Generate a uniform noise template. The template extends from T0 
    *  for a time dT. and has a flat time domain distribution from 0-1.
    *  \brief UniformNoise Constructor. 
    *  \param fNy Nyquist frequency (half of sample rate)
    *  \param dT  Template length in seconds.
    *  \param t0  Start time.
    */
    explicit UniformNoise( double      fNy,
                           Interval    dT = Interval( 60.0 ),
                           const Time& t0 = Time( 0 ) );

    /**  UniformNoise destructor.
    */
    ~UniformNoise( void )
    {
    }

    /**  Interface to a function which will return the frequency domain 
    *  representation of the astrophysical waveform.
    *  \brief Template representation in the frequency domain.
    *  \param Freq Frequency at which signal is to be sampled
    *  \param dF   Frequency step
    *  \returns complex f-domain coefficient of signal.
    */
    complex_type Fspace( double Freq, double dF = 0 ) const;

    /**  Interface to a function which will return the time domain 
    *  representation of the astrophysical waveform.
    *  \brief Template representation in the time domain.
    *  \param t0 %Time at which signal is to be sampled.
    *  \returns Signal amplitude at the specified time.
    */
    double Tspace( const Time& t0 ) const;

    /** freq(t) returns the waveform frequency at Absolute time t. This 
    *  method returns zero or fNy/2 if the frequency is not a single-valued 
    *  function of time.
    *  \brief Frequency at specified time
    *  \param t %Time to be interrogated
    *  \returns Frequency at the specified time.
    */
    virtual double freq( const Time& t ) const;

    /**  phi(t) returns the phase angle at absolute time t. This 
    *  method returns zero if the phase is not a single-valued 
    *  function of time.
    *  \brief Phase at specified time
    *  \param t %Time to be interrogated
    *  \returns Phase at the specified time.
    */
    double phi( const Time& t ) const;

    /** Ampl(t) returns the wave-form amplitude at a specific time.
    *  \brief Amplitude at specified time
    *  \param t %Time to be interrogated
    *  \returns Amplitude at the specified time.
    */
    double Ampl( const Time& t ) const;

private:
    /**  This is the Nyquist frequency.
    *  @memo Nyquist frequency.
    */
    double mFny;
};

#endif //  UniformNoise_HH
