/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef GaussNoise_HH
#define GaussNoise_HH
#include "Chirp.hh"
#include "Interval.hh"

/**  Gaussian noise template generator.
 *  GaussNoise is a template generator based on the Chirp class. It 
 *  generates gaussian noise of unit amplitude.
 */
class GaussNoise : public Chirp
{
public:
    /** Generate a gaussian noise template. The template extends from T0 
    *  for a time dT. and has a flat frequency spectrum from 0-fNy.
    *  \brief GaussNoise Constructor. 
    *  \param fNy Nyquist frequency of the gaussian noise waveform.
    *  \param dT  Duration of the gaussian noise waveform.
    *  \param t0  Start time of the gaussian noise waveform.
    */
    explicit GaussNoise( double      fNy,
                         Interval    dT = Interval( 60.0 ),
                         const Time& t0 = Time( 0 ) );

    /** Destroy a GaussNoise instance.
    * \brief destructor.
    */
    ~GaussNoise( void )
    {
    }

    /**  Interface to a function which will return the frequency domain 
    *  representation of the astrophysical waveform.
    *  \brief Template representation in the frequency domain.
    *  \param Freq Frequency at which signal is to be sampled
    *  \param dF   Frequency step
    *  \returns complex f-domain coefficient of signal.
    */
    complex_type Fspace( double Freq, double dF = 0 ) const;

    /**  Interface to a function which will return the time domain 
    *  representation of the astrophysical waveform.
    *  \brief Template representation in the time domain.
    *  \param t0 %Time at which signal is to be sampled.
    *  \returns Signal amplitude at the specified time.
    */
    double Tspace( const Time& t0 ) const;

    /** freq(t) returns the waveform frequency at Absolute time t. This 
    *  method returns zero or fNy/2 if the frequency is not a single-valued 
    *  function of time.
    *  \brief Frequency at specified time
    *  \param t %Time to be interrogated
    *  \returns Frequency at the specified time.
    */
    double freq( const Time& t ) const;

private:
    /**  This is the Nyquist frequency.
    *  @memo Nyquist frequency.
    */
    double mFny;
};

#endif //  GaussNoise_HH
