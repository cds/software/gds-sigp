/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef IMPULSE_HH
#define IMPULSE_HH
#include "Chirp.hh"
#include "Interval.hh"

/** Impulse is a template generator based on the Chirp class. It generates 
 *  an impulse.
 *  \brief Impulse template generator.
 */
class Impulse : public Chirp
{
public:
    /** Generate a inpulse of duration \a dur, amplitude A and delay d.
    *  The template extends from T0 for a time dT.
    *  \brief Impulse Constructor.
    *  \param dur   %Impulse duration
    *  \param ampl  %Impulse amplitude
    *  \param delay Delay from start of generated dignal to transient
    *  \param dT    Length of the generated signal
    *  \param t0    nominal start time of signal. 
    */
    explicit Impulse( Interval    dur,
                      double      ampl = 1.0,
                      Interval    delay = Interval( 0 ),
                      Interval    dT = Interval( 60.0 ),
                      const Time& t0 = Time( 0 ) );

    /** Destroy the %Impulse instance.
    *  \brief Destructor.
    */
    ~Impulse( void )
    {
    }

    /**  Interface to a function which will return the frequency domain 
    *  representation of the astrophysical waveform.
    *  \brief Template representation in the frequency domain.
    *  \param Freq Frequency at which signal is to be sampled
    *  \param dF   Frequency step
    *  \returns complex f-domain coefficient of signal.
    */
    complex_type Fspace( double Freq, double dF = 0 ) const;

    /**  Interface to a function which will return the time domain 
    *  representation of the astrophysical waveform.
    *  \brief Template representation in the time domain.
    *  \param t0 %Time at which signal is to be sampled.
    *  \returns Signal amplitude at the specified time.
    */
    double Tspace( const Time& t0 ) const;

    /** Ampl(t) returns the wave-form amplitude at a specific time.
    *  \brief Amplitude at specified time
    *  \param t %Time to be interrogated
    *  \returns Amplitude at the specified time.
    */
    double Ampl( const Time& t ) const;

    /**  Set the function amplitude.
    *  \brief set the amplitude.
    *  \param amp Amplitude of the impulse.
    */
    void setAmp( double amp );

    /**  Duration returns the impulse duration.
    *  \brief impulse duration.
    *  \returns impulse duration in seconds.
    */
    Interval Duration( void ) const;

    /** Set the impulse duration
    *  \brief Set the duration.
    *  \param dur Width of impulse in seconds.
    */
    void setDuration( Interval dur );

    /** Delay returns the impulse delay.
    *  \brief Get the delay
    *  \returns Delay time from t0 to the start of the impulse.
    */
    Interval Delay( void ) const;

    /**  Set the impulse delay
    *  \brief Set the delay.
    *  \param delay Time from the template start time (t0) to the start 
    *               of the impulse.
    */
    void setDelay( Interval delay );

private:
    //---------------------------  External Parameters
    /**  Impulse amplitude.
    */
    double mAmpl;

    /**  Impulse duration.
    */
    Interval mDuration;

    /**  Impulse delay.
    */
    Interval mDelay;
};

#endif //  IMPULSE_HH
