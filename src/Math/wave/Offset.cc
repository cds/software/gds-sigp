/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "Offset.hh"
#include "Time.hh"
#include "constant.hh"
#include <cmath>

//-------------------------------------  Offset Constructor.
Offset::Offset( double offset, Interval dT, const Time& t0 )
    : Chirp( t0, t0 + dT, t0 ), mAmpl( offset )
{
}

//-------------------------------------  Frequency domain template bin.
Chirp::complex_type
Offset::Fspace( double Freq, double dF ) const
{
    complex_type r( 0, 0 );
    if ( ( Freq <= 0 ) && ( Freq + dF > 0 ) )
    {
        r = complex_type( mAmpl );
    }
    return r;
}

//-------------------------------------  Time domain template bin.
double
Offset::Tspace( const Time& t0 ) const
{
    return Ampl( t0 );
}

//-------------------------------------  Amplitude.
double
Offset::Ampl( const Time& t ) const
{
    return mAmpl;
}

void
Offset::setAmp( double amp )
{
    mAmpl = amp;
}
