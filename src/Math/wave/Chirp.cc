/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "Chirp.hh"
#include "Interval.hh"
#include <iostream>
#include <cmath>

//======================================  Chirp constructor.
Chirp::Chirp( const Time& tStart, const Time& tEnd, const Time& tCrit )
    : mT0( tStart ), mTl( tEnd ), mTc( tCrit )
{
}

//======================================  Return heterodyned F(t)
Chirp::complex_type
Chirp::Tspace( const Time& t, double f0 ) const
{
    complex_type r;
    double       dT = ( t - getTc( ) );
    double       Xt = Tspace( t );
    r = std::polar( 1.0, f0 * dT );
    r *= Xt;
    return r;
}

//======================================  Return heterodyned F(t)
Time
Chirp::TvsF( double f ) const
{
    return mT0;
}

//======================================  Return frequency(t)
double
Chirp::freq( const Time& t ) const
{
    return 0;
}

//======================================  Return phase(t)
double
Chirp::phi( const Time& t ) const
{
    return 0;
}

//======================================  Return amplitude(t)
double
Chirp::Ampl( const Time& t ) const
{
    return 1.0;
}
