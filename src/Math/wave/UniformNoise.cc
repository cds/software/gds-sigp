/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "UniformNoise.hh"
#include "Time.hh"
#include "constant.hh"
#include "rndm.hh"
#include <cmath>

//-------------------------------------  UniformNoise Constructor.
UniformNoise::UniformNoise( double Freq, Interval dT, const Time& t0 )
    : Chirp( t0, t0 + dT, t0 ), mFny( Freq )
{
}

//-------------------------------------  Frequency domain template bin.
Chirp::complex_type
UniformNoise::Fspace( double Freq, double dF ) const
{
    double rn = twopi * Rndm( );
    return complex_type( ::cos( rn ), ::sin( rn ) );
}

//-------------------------------------  Time domain template bin.
double
UniformNoise::Tspace( const Time& t0 ) const
{
    return Ampl( t0 ) * ( 2.0 * Rndm( ) - 1.0 );
}

//-------------------------------------  Waveform Frequency.
double
UniformNoise::freq( const Time& t ) const
{
    return mFny / 2.0;
}

//-------------------------------------  Waveform Phase angle.
double
UniformNoise::phi( const Time& t ) const
{
    return 0.0;
}

//-------------------------------------  Amplitude.
double
UniformNoise::Ampl( const Time& t ) const
{
    return 1.0;
}
