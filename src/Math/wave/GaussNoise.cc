/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "GaussNoise.hh"
#include "Time.hh"
#include "constant.hh"
#include "rndm.hh"
#include <cmath>

//======================================  GaussNoise Constructor.
GaussNoise::GaussNoise( double Freq, Interval dT, const Time& t0 )
    : Chirp( t0, t0 + dT, t0 ), mFny( Freq )
{
}

//======================================  Frequency domain template bin.
Chirp::complex_type
GaussNoise::Fspace( double Freq, double dF ) const
{
    double rn = twopi * Rndm( );
    return complex_type( ::cos( rn ), ::sin( rn ) );
}

//======================================  Time domain template bin.
double
GaussNoise::Tspace( const Time& t0 ) const
{
    return Ampl( t0 ) * Rannor( );
}

//======================================  Waveform Frequency.
double
GaussNoise::freq( const Time& t ) const
{
    return mFny / 2.0;
}
