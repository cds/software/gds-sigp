
add_library(wave OBJECT
    Chirp.cc
    GaussNoise.cc
    Impulse.cc
    Inspiral.cc
    Offset.cc
    Ramp.cc
    Sine.cc
    SquareWave.cc
    Triangle.cc
    UniformNoise.cc
)

target_include_directories(wave PRIVATE
    ${CMAKE_SOURCE_DIR}/src/Math/random

    ${CMAKE_SOURCE_DIR}/src/Base/complex
    ${CMAKE_SOURCE_DIR}/src/Base/constant
    ${CMAKE_SOURCE_DIR}/src/Base/time
)

install(FILES
    Chirp.hh
    GaussNoise.hh
    Impulse.hh
    Inspiral.hh
    Offset.hh
    Ramp.hh
    Sine.hh
    SquareWave.hh
    Triangle.hh
    UniformNoise.hh
    DESTINATION include/gds-sigp
)
