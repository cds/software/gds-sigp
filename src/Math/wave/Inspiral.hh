/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef INSPIRAL_HH
#define INSPIRAL_HH
#include "Chirp.hh"
#include "Interval.hh"

/**  Newtonian binary in-spiral template generator.
  *  Inspiral is a template generator based on the Chirp class. It
  *  generates binary in-spiral wave-forms. So far, only the zeroeth 
  *  order (Newtonian) approximation is used.
  */
class Inspiral : public Chirp
{
public:
    /**  Generate an in-spiral waveform template for two objects of masses 
    *  'Mass1' and 'Mass2'. 'dT' is the time interval to be considered. 
    *  If not specified dT defaults to 60s. 't0' if specified is the 
    *  absolute critical time of the inspiral and the template extends 
    *  between t0-dT and t0. If t0 is not specified, the template extends 
    *  from 0->dT.
    *  \brief %Inspiral Constructor. 
    *  \param Mass1 Mass of lighter object
    *  \param Mass2 Mass of heavier object
    *  \param dT    duration of waveform
    *  \param t0    start time of waveform.
    */
    Inspiral( double      Mass1,
              double      Mass2,
              Interval    dT = Interval( 60.0 ),
              const Time& t0 = Time( 0, 0 ) );

    /**  Inspiral destructor.
    */
    ~Inspiral( void )
    {
    }

    /**  Interface to a function which will return the frequency domain 
    *  representation of the astrophysical waveform.
    *  \brief Template representation in the frequency domain.
    *  \param Freq Frequency at which signal is to be sampled
    *  \param dF   Frequency step
    *  \returns complex f-domain coefficient of signal.
    */
    complex_type Fspace( double Freq, double dF = 0 ) const;

    /**  Interface to a function which will return the time domain 
    *  representation of the astrophysical waveform.
    *  \brief Template representation in the time domain.
    *  \param t0 %Time at which signal is to be sampled.
    *  \returns Signal amplitude at the specified time.
    */
    double Tspace( const Time& t0 ) const;

    /** TvsF(f) returns the absolute (GPS) time at which the in-spiral 
    *  frequency is exactly 'f'. This is defined only for monotonically 
    *  changing frequency signals.
    *  \brief Time versus Frequency.
    *  \param f Frequency argument
    *  \returns %Time at which signal crosses the specified frequency.
    */
    Time TvsF( double f ) const;

    /** freq(t) returns the waveform frequency at Absolute time t. This 
    *  method returns zero or fNy/2 if the frequency is not a single-valued 
    *  function of time.
    *  \brief Frequency at specified time
    *  \param t %Time to be interrogated
    *  \returns Frequency at the specified time.
    */
    double freq( const Time& t ) const;

    /**  phi(t) returns the phase angle at absolute time t. This 
    *  method returns zero if the phase is not a single-valued 
    *  function of time.
    *  \brief Phase at specified time
    *  \param t %Time to be interrogated
    *  \returns Phase at the specified time.
    */
    double phi( const Time& t ) const;

    /** Ampl(t) returns the wave-form amplitude at a specific time.
    *  \brief Amplitude at specified time
    *  \param t %Time to be interrogated
    *  \returns Amplitude at the specified time.
    */
    double Ampl( const Time& t ) const;

private:
    //---------------------------  External Parameters
    /**  Mass of first object.
    *  The mass of the lighter object in solar mass units.
    */
    double mMass1;

    /**  Mass of second object.
    *  The mass of the heavier object in solar mass units.
    */
    double mMass2;

    /**  Phase offset.
    *  This is the phase at Tc.
    */
    double mPhic;

    //---------------------------  Useful internal function.
    /**  Time at which the frequency is maximized.
    *  TMax searches over the interval Tmin - Tmax for a a maximum in 
    *  freq(t). It returns the Time at which the maximum is found.
    */
    Time TMax( const Time& tmin, const Time& tmax ) const;

    //---------------------------  Derived constants.
    /**  Total mass.
    *  Internal variable calculated from Mass1 + Mass2.
    */
    double mMTot;

    /**  Reduced mass/Total.
    *  Internal variable calculated from Mass1*Mass2/(MTot*MTot).
    */
    double mEta;
};

#endif //  INSPIRAL_HH
