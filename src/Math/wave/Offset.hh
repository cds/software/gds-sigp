/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef OFFSET_HH
#define OFFSET_HH
#include "Chirp.hh"
#include "Interval.hh"

/**  Offset is a template generator based on the Chirp class. It generates 
 *  a fixed offset.
 *  \brief Fixed offset template generator.
 */
class Offset : public Chirp
{
public:
    /** Generate a constant signal for the length of the template.
    *  The template extends from T0 for a time dT.
    *  \brief Offset Constructor.
    *  \param offset Constsnt amplitude of signal.
    *  \param dT  Duration of the constant waveform.
    *  \param t0  Start time of the constant waveform.
    */
    explicit Offset( double      offset,
                     Interval    dT = Interval( 60.0 ),
                     const Time& t0 = Time( 0 ) );

    /** Destroy an %Offset instance.
    *  \brief %Offset destructor.
    */
    ~Offset( void )
    {
    }

    /**  Interface to a function which will return the frequency domain 
    *  representation of the astrophysical waveform.
    *  \brief Template representation in the frequency domain.
    *  \param Freq Frequency at which signal is to be sampled
    *  \param dF   Frequency step
    *  \returns complex f-domain coefficient of signal.
    */
    complex_type Fspace( double Freq, double dF = 0 ) const;

    /**  Interface to a function which will return the time domain 
    *  representation of the astrophysical waveform.
    *  \brief Template representation in the time domain.
    *  \param t0 %Time at which signal is to be sampled.
    *  \returns Signal amplitude at the specified time.
    */
    double Tspace( const Time& t0 ) const;

    /** Ampl(t) returns the wave-form amplitude at a specific time.
    *  \brief Amplitude at specified time
    *  \param t %Time to be interrogated
    *  \returns Amplitude at the specified time.
    */
    double Ampl( const Time& t ) const;

    /** Set the fnunction amplitude.
    *  \brief Set the amplitude.
    *  \param amp Offest amplitude 
    */
    void setAmp( double amp );

private:
    //---------------------------  External Parameters
    /**  Offset amplitude.
    */
    double mAmpl;
};

#endif //  OFFSET_HH
