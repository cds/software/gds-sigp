/* -*- mode: c++; c-basic-offset: 3; -*- */
//
//    Inspiral template generation.
//
#include <cmath>
#include "Inspiral.hh"
#include "Interval.hh"
#include "constant.hh"

//==================================  Gravitational constant.
static const double To = 4.925491e-6;

//----------------------------------  Simple newtonian constructor
Inspiral::Inspiral( double Mass1, double Mass2, Interval dT, const Time& t0 )
{
    mMass1 = Mass1;
    mMass2 = Mass2;
    if ( !t0 )
    {
        mT0 = t0;
        mTc = t0 + dT;
    }
    else
    {
        mTc = t0;
        mT0 = mTc - dT;
    }
    mMTot = mMass1 + mMass2;
    mEta = mMass1 * mMass2 / ( mMTot * mMTot );
    mPhic = 0.0;
    mTl = TMax( mT0, mTc );
}

//----------------------------------  Return a frequency representation.
Chirp::complex_type
Inspiral::Fspace( double Freq, double dF ) const
{
    Time         t = TvsF( Freq );
    double       dphi = phi( t ) - twopi * Freq * double( t - mT0 );
    double       A = Ampl( t );
    complex_type r;
    r = std::polar( A, dphi );
    return r;
}

//----------------------------------  Return a time representation.
double
Inspiral::Tspace( const Time& t0 ) const
{
    double hc = Ampl( t0 ) * cos( 2.0 * phi( t0 ) );
    return hc;
}

//----------------------------------  Return time at which frequency
//
//    TvsF() performs a binary search for a time that corresponds to the
//    argument frequency. TvsF assumes that the frequency is a monotonically
//    increasing function of T. The binary search is terminated and a linear
//    interpolation is used if the time interval decreases below 1us or if
//    the absolute frequency decreases below 1mHz.
//
Time
Inspiral::TvsF( double F ) const
{
    Time   Tlo = mT0;
    Time   Thi = mTl;
    double Flo = freq( Tlo );
    double Fhi = freq( Thi );
    if ( Flo >= Fhi )
        return Tlo;
    else if ( F <= Flo )
        return Tlo;
    else if ( F >= Fhi )
        return Thi;

    Time   Tx;
    double Fx, dF;
    while ( 1 )
    {
        Tx = Tlo + 0.5 * ( Thi - Tlo ); // Note: an interpolative estimate
        //       could be used here
        Fx = freq( Tx );
        if ( F > Fx )
        {
            Tlo = Tx;
            Flo = Fx;
            dF = F - Fx;
        }
        else
        {
            Thi = Tx;
            Fhi = Fx;
            dF = Fx - F;
        }
        if ( ( dF <= 0.001 ) || ( ( Thi - Tlo ) <= Interval( 0.000001 ) ) )
            break;
    }
    return Tlo + ( F - Flo ) / ( Fhi - Flo ) * ( Thi - Tlo );
}

//----------------------------------  Return time at which frequency is max.
//
//    TvsF() performs a binary search for a time that corresponds to the
//    maximum frequency
//
Time
Inspiral::TMax( const Time& Tmin, const Time& Tmax ) const
{
    int i;

    Time     t = Tmin;
    Interval dt = ( Tmax - Tmin ) / 10.0;

    while ( dt > Interval( 0.00000001 ) )
    {
        double Fprev = freq( t );
        for ( i = 0; i < 10; i++ )
        {
            t += dt;
            double F = freq( t );
            if ( F < Fprev )
                break;
            Fprev = F;
        }
        t -= dt;
        if ( i )
            t -= dt;
        dt /= 5;
    }
    return t + dt;
}

//----------------------------------  Return the template amplitude
double
Inspiral::Ampl( const Time& t ) const
{
    double A = twopi * To * mMTot * freq( t );
    return 2.0 * mEta * mMTot * pow( A, 0.666666666666666666 );
}

//----------------------------------  Return a time representation.
double
Inspiral::freq( const Time& t ) const
{
    double Theta = -mEta / ( 5 * To * mMTot ) * ( t - mTc );
    double T1 = pow( Theta, -.125 );
    double T2 = T1 * T1;
    double T3 = T2 * T1;
    double f = T3 / ( 16.0 * pi * To * mMTot ) *
        ( 1.0 +
          T2 *
              ( 743.0 / 2688.0 + mEta * 11.0 / 32.0 - T1 * 0.30 * pi +
                T2 *
                    ( 1855099.0 / 14450688.0 + mEta * 56975.0 / 258048.0 +
                      mEta * mEta * 371.0 / 2048.0 ) ) );
    return f;
}

//----------------------------------  Return a time representation.
double
Inspiral::phi( const Time& t ) const
{
    double Theta = -mEta / ( 5.0 * To * mMTot ) * ( t - mTc );
    double T1 = pow( Theta, 0.125 );
    double T2 = T1 * T1;
    double T4 = T2 * T2;
    double p = mPhic -
        ( T1 / mEta ) *
            ( T4 + T2 * ( 3715.0 / 8064.0 + mEta * 55.0 / 96.0 ) -
              T1 * 0.75 * pi + 9275495.0 / 14450688.0 +
              mEta * 284875.0 / 258048.0 + mEta * mEta * 1855.0 / 2048.0 );
    return p;
}
