/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "Ramp.hh"
#include "Time.hh"
#include "constant.hh"
#include <cmath>

#define fMod( x, y ) ( ( x ) - (y)*floor( ( x ) / ( y ) ) )

//-------------------------------------  normalized phase.
inline double
normPhase( double phi )
{
    if ( ( phi >= 0 ) && ( phi < twopi ) )
    {
        return phi;
    }
    else
    {
        return fMod( phi, twopi );
    }
}

//-------------------------------------  Ramp Constructor.
Ramp::Ramp( double Freq, double ampl, double phi0, Interval dT, const Time& t0 )
    : Chirp( t0, t0 + dT, t0 ), mPhic( phi0 ), mAmpl( ampl )
{
    mOmega = twopi * Freq;
}

//-------------------------------------  Frequency domain template bin.
Chirp::complex_type
Ramp::Fspace( double Freq, double dF ) const
{
    complex_type r( 0 );
    double       f = mOmega / twopi;
    if ( f <= 0 )
    {
        return r;
    }
    int n = int( 2 * Freq / f + 1.5 );
    if ( n < 10 )
        n = 10;
    for ( int i = 0; i < 2 * n + 1; ++i )
    {
        double x = -pow( ( Freq - double( i ) * f ) / dF, 2 );
        r += mAmpl * exp( complex_type( x, mPhic + i * pi ) ) / i;
    }
    return 2. / pi * r;
}

//-------------------------------------  Time domain template bin.
double
Ramp::Tspace( const Time& t0 ) const
{
    return Ampl( t0 ) * normPhase( phi( t0 ) ) / twopi;
}

//-------------------------------------  Waveform Frequency.
double
Ramp::freq( const Time& t ) const
{
    return mOmega / twopi;
}

//-------------------------------------  Waveform Phase angle.
double
Ramp::phi( const Time& t ) const
{
    return mOmega * double( t - mTc ) - mPhic;
}

//-------------------------------------  Amplitude.
double
Ramp::Ampl( const Time& t ) const
{
    return mAmpl;
}

void
Ramp::setAmp( double amp )
{
    mAmpl = amp;
}
