/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef RAMP_HH
#define RAMP_HH
#include "Chirp.hh"
#include "Interval.hh"

/**  Ramp template generator.
 *  Ramp is a template generator based on the Chirp class. It generates 
 *  a ramp waveform.
 */
class Ramp : public Chirp
{
public:
    /** Generate a ramp template of frequency \a f, amplitude \a ampl and 
    *  starting phase \a phi0. A ramp increase linearly from 0 to \a ampl and
    *  then drops back to zero. This is repeated \a f times per second. The 
    *  template extends from \a t0 for a time \a dT.
    *  \brief Ramp Constructor.
    *  \param f ramp frequency
    *  \param ampl ramp amplitude
    *  \param phi0 ramp phase parameter - phase at t0.
    *  \param dT    Length of the generated signal
    *  \param t0    nominal start time of signal. 
    */
    explicit Ramp( double      f,
                   double      ampl = 1.0,
                   double      phi0 = 0.0,
                   Interval    dT = Interval( 60.0 ),
                   const Time& t0 = Time( 0 ) );

    /**  Ramp destructor.
    */
    ~Ramp( void )
    {
    }

    /**  Interface to a function which will return the frequency domain 
    *  representation of the astrophysical waveform.
    *  \brief Template representation in the frequency domain.
    *  \param Freq Frequency at which signal is to be sampled
    *  \param dF   Frequency step
    *  \returns complex f-domain coefficient of signal.
    */
    complex_type Fspace( double Freq, double dF = 0 ) const;

    /**  Interface to a function which will return the time domain 
    *  representation of the astrophysical waveform.
    *  \brief Template representation in the time domain.
    *  \param t0 %Time at which signal is to be sampled.
    *  \returns Signal amplitude at the specified time.
    */
    double Tspace( const Time& t0 ) const;

    /** freq(t) returns the waveform frequency at Absolute time t. This 
    *  method returns zero or fNy/2 if the frequency is not a single-valued 
    *  function of time.
    *  \brief Frequency at specified time
    *  \param t %Time to be interrogated
    *  \returns Frequency at the specified time.
    */
    double freq( const Time& t ) const;

    /**  phi(t) returns the phase angle at absolute time t. This 
    *  method returns zero if the phase is not a single-valued 
    *  function of time.
    *  \brief Phase at specified time
    *  \param t %Time to be interrogated
    *  \returns Phase at the specified time.
    */
    double phi( const Time& t ) const;

    /** Ampl(t) returns the wave-form amplitude at a specific time.
    *  \brief Amplitude at specified time
    *  \param t %Time to be interrogated
    *  \returns Amplitude at the specified time.
    */
    double Ampl( const Time& t ) const;

    /** Set the function amplitude.
    *  \brief Set the amplitude.
    *  \param amp Amplitude
    */
    void setAmp( double amp );

private:
    //---------------------------  External Parameters
    /**  Frequency.
    *  The sinusoidal frequency.
    */
    double mOmega;

    /**  Phase offset.
    *  This is the phase at Tc.
    */
    double mPhic;

    /**  Ramp amplitude.
    */
    double mAmpl;
};

#endif //  RAMP_HH
