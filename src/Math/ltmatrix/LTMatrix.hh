/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef LTMATRIX_HH
#define LTMATRIX_HH

#include <iosfwd>

/**  The %LTMatrix is used to represent and manipulate real Triangle- and 
  *  Symmetric matrices, \e i.e. square matrices with at most \c N(N+1)/2 
  *  independent elements. These can be:
  *   - Triangle matrices (elements above the diagonal are zero)
  *   - Symmetric matrices (\c A[i,j] is equal to \c A[j,i] )
  *   - Upper Triangle transposed to the lower triangle.
  *
  *  The matrix elements are stored in row-major order \e i.e. \c A[0,0], 
  *  \c A[1,0], \c [A[1,1], \c ..., \c [N-1,0], \c ..., \c A[N-1,N-1].
  *
  *  This can be used torepresent numerous symmetric matrix forms, including
  *  * Hankel matrices: square matrix where all elements on evey ascending 
  *           skew diagonal is constant, \e i.e. A[i,j]\ =\ A[i-k,\ j+k]. 
  *           Hankel matrices have 2N-1 independent parameters and can be 
  *           set by the hankel() method.
  *  * Toeplitz matrices: Square matrices with all elements of each diagonal
  *           descending form left to right are  equal, \e i.e. 
  *           \c T[i,j]\ =\ T[i+k,\ j+k]. Symmetric Toeplitz matrices have N 
  *           independent parameters and can be represented as a triangle 
  *           matrix and set by the toeplitz() method. 
  *  * Hermitian matrices are symmetric matrices. 
  *
  *  The inverse of a Hermitian matrix may be found using the cholesky 
  *  decomposition.
  *
  *  \author John G. Zweizig
  *  \version $Id$
  */
class LTMatrix
{
public:
    /** Enumerate matrix types
     */
    enum matrix_type
    {
        kUnknown, ///< Empty or unknown type
        kSymmetric, ///< Symmetric matrix
        kTriangle, ///< Lower triangle
        kTranspose ///< Upper triangle.
    };

    /// Length data type.
    typedef unsigned long size_type;

public:
    /**  Construct an uninitialized LTMatrix instance. If the dimension argument
      *  \a N is non-zero, storage will be allocated for the specified matrix
      *  size, but the data will remain uninitialized.
      *  \brief Default constructor.
      *  \param N Dimension of the matrix.
      */
    LTMatrix( size_type N = 0 );

    /**  Construct an array and initialize it with the specified data.
      *  \brief Data constructor.
      *  \param N Matrix dimension
      *  \param data Initialization data (N*(N+1)/2 data words).
      *  \param mt Matrix type
      */
    LTMatrix( size_type N, const double* data, matrix_type mt = kUnknown );

    /**  Construct an exact copy of the argument matrix.
      *  \brief Copy constructor.
      *  \param mtrx %LTMatrix to be copied.
      */
    LTMatrix( const LTMatrix& mtrx );

    /**  Destroy an %LTMatrix and release any allocated storage.
      */
    virtual ~LTMatrix( void );

    /**  Assign the contents a specified %LTMatrix to this instance.
      *  \brief Assignment operator
      *  \param mtrx Matrix to be copied.
      *  \return Reference to this instance.
      */
    LTMatrix& operator=( const LTMatrix& mtrx );

    /**  Calculate the sum of this instance and the specified %LTMatrix 
      *   argument.
      *  \brief Sum of two LT matrices.
      *  \param mtrx Matrix to be added to this instance.
      *  \return Reference to this instance.
      */
    LTMatrix& operator+=( const LTMatrix& mtrx );

    /**  Get the specified data element. The index argument specifies the 
      *  position of the element in compressed storage, \e i.e. the index
      *  for \c A[i,j] is \f$ inx = i ( i + 1) / 2 + j \f$.
      *  \brief Get an element
      *  \param i Index of desired element in compress array.
      *  \return Element value.
      */
    double operator[]( size_type i ) const;

    /**  Get reference to the specified data element. The index argument 
      *  specifies the position of the element in compressed storage, \e i.e. 
      *  the index for \c A[i,j] is \f$ inx = i ( i + 1) / 2 + j \f$.
      *  \brief Reference an element
      *  \param i Index of desired element in compress array.
      *  \return Writable reference to the specified element.
      */
    double& operator[]( size_type i );

    /**  Perform a Cholesky decomposition of a positive-definite symmetric 
      *  matrix, A. The returned value is a lower triangle matrix, L, that 
      *  satisfies the equation \f$ A = L L^T \f$.
      *  \brief Cholesky decomposition.
      *  \return The Cholesky matrix, L.
      */
    LTMatrix cholesky( ) const;

    /**  Return the dimension of the matrix.
      *  \brief Dimension.
      *  \return Matrix dimension.
      */
    size_type dim( void ) const;

    /**  Dump the matrix as a lower triangle to the specified output stream.
      *  \brief Dump a lower triangle matrix.
      *  \param out Output stream.
      */
    void dump( std::ostream& out ) const;

    /**  Create a symmetric Hankel matrix from a column and a row. A Hankel
      *  matrix of dimension N, is one in which all elements of each cross-
      *  diagonal are equal, or H[i,j-1]=H[i-1,j], for all i,j<N. The matrix 
      *  thus has 2N-1 independent element which are contained in the first 
      *  column and the last row.
      *  \brief Construct a Hankel matrix
      *  \param N      Number of elements in each row, column.
      *  \param column Elements of the first column
      *  \param row    Elements of the last row.
      */
    void hankel( size_type N, const double column[], const double row[] );

    /**  Multiply a vector by a lower-triangle or symmetric matrix. The length
      *  of the input vector must be the same as the dimension of the LTMatrix.
      *  \brief Vector multiply
      *  \param in     Vector to be multiplied by the matrix.
      *  \param result Vector result of multiplication.
      */
    void mpyvec( const double in[], double result[] ) const;

    /**  Number of data elements, \e i.e. \c N(N+1)/2.
      *  \brief Number of data elements.
      *  \return Number of data elements.
      */
    size_type nData( void ) const;

    /**  Print the matrix [row,column] coordinate for the specified index .
      *  \brief Print row, column numbers.
      *  \param out Output stream.
      *  \param inx Index into array.
      *  \return Output stream reference.
     */
    std::ostream& prt_coord( std::ostream& out, size_type inx ) const;

    /**  Set the matrix dimension.
      * \brief set the dimension.
      * \param N matrix dimension.
      */
    void set_size( size_type N );

    /**  Set the matrix type as enumerated by matrix type.
      *  \brief set the matrix type.
      *  \param mt New matrix type.
      */
    void set_type( enum matrix_type mt );

    /**  Solve the set of linear equations defined by the current 
      *  matrix, A. \e i.e. solve for x in A x = b, where A is the current 
      *  matrix, b is the input vector \a vec and x is the returned vector 
      *  \a coefs.
      *  The matrix may be either a lower-triangle matrix or a symmetric 
      *  matrix, in which case, the matrix is factored using cholesky 
      *  decomposition and then solved recursively.
      *  \brief Solve linear equations.
      *  \param vec   Vector (b) of N elements in A x = b.
      *  \param coefs solution vector (x) of N elements in A x = b.
      */
    void solve( const double vec[], double coefs[] );

    /**  Solve the set of linear equations defined by the current 
      *  Upper triangular matrix, A. \e i.e. solve for x in A x = b, 
      *  where A is the current matrix, b is the input vector \a vec and x 
      *  is the returned vector, \a coefs.
      *  solveTranspose is called by solve to solve the transpose matrix after
      *  performing a choleski depcomposition.
      *  \brief Solve linear equations.
      *  \param vec   Vector (b) of N elements in A x = b.
      *  \param coefs solution vector (x) of N elements in A x = b.
      */
    void solveTranspose( const double vec[], double coefs[] );

    /**  Create a symmetric Toeplitz matrix from a single column of elements.
      *  For a symmetric Toeplitz matrix, the first column and the first row 
      *  are identical.
      *  \brief Construct a Toeplitz matrix.
      *  \param N Matrix dimension
      *  \param column First column (and row).
      */
    void toeplitz( size_type N, const double column[] );

private:
    size_type   mDim;
    matrix_type mType;
    double*     mData;
};

//======================================  Inline methods.
inline LTMatrix::size_type
LTMatrix::dim( void ) const
{
    return mDim;
}

inline LTMatrix::size_type
LTMatrix::nData( void ) const
{
    return mDim * ( mDim + 1 ) / 2;
}

inline double
LTMatrix::operator[]( size_type i ) const
{
    return mData[ i ];
}

inline double&
LTMatrix::operator[]( size_type i )
{
    return mData[ i ];
}

#endif // !defined(LTMATRIX_HH)
