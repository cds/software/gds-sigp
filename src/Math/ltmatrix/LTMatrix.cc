/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "LTMatrix.hh"
#include "lcl_array.hh"
#include "gds_veclib.hh"
#include <iostream>
#include <cstring>
#include <cmath>
#include <stdexcept>

using namespace std;

// #define DEBUG_SOLVE 1

#define INLINE_VDOT 1

//======================================  Default constructor
LTMatrix::LTMatrix( size_type N ) : mDim( 0 ), mType( kUnknown ), mData( 0 )
{
    set_size( N );
}

//======================================  Data constructor
LTMatrix::LTMatrix( size_type N, const double* data, matrix_type mt )
    : mDim( 0 ), mType( mt ), mData( 0 )
{
    set_size( N );
    if ( N && data )
        memcpy( mData, data, nData( ) * sizeof( double ) );
}

//======================================  Copy constructor
LTMatrix::LTMatrix( const LTMatrix& mtrx )
    : mDim( 0 ), mType( kUnknown ), mData( 0 )
{
    *this = mtrx;
}

//======================================  Destructor
LTMatrix::~LTMatrix( void )
{
    set_size( 0 );
}

//======================================  Assignment operator
LTMatrix&
LTMatrix::operator=( const LTMatrix& mtrx )
{
    if ( mData != mtrx.mData )
    {
        if ( mData )
            set_size( 0 );
        set_size( mtrx.mDim );
        memcpy( mData, mtrx.mData, nData( ) * sizeof( double ) );
        mType = mtrx.mType;
    }
    return *this;
}

//======================================  Addition operator
LTMatrix&
LTMatrix::operator+=( const LTMatrix& mtrx )
{
    if ( mDim != mtrx.mDim )
    {
        throw runtime_error( "LTMatrix: Can't add unequal dimension matrices" );
    }
    if ( mType != mtrx.mType )
    {
        throw runtime_error( "LTMatrix: Can't add different type matrices" );
    }
    size_t N = nData( );
    for ( size_t i = 0; i < N; ++i )
        mData[ i ] += mtrx.mData[ i ];
    return *this;
}

//======================================  Cholesky decomposition of sym matrix
LTMatrix
LTMatrix::cholesky( ) const
{
    //----------------------------------  Check this is a compressed symtx.
    if ( mType != kSymmetric )
    {
        throw runtime_error( "LTMatrix::cholesky: Not a symmetric matrix" );
    }

    //----------------------------------  Create the output matrix.
    LTMatrix out( mDim );
    out.set_type( kTriangle );

    //----------------------------------  Fill the matrix
    size_type inxij = 0; // index of a[i,j]
    for ( size_type i = 0; i < mDim; i++ )
    {
        double    sumxx = 0;
        size_type inxi0 = inxij;
        size_type inxjk = 0;
        for ( size_type j = 0; j < i; j++ )
        {
#ifdef INLINE_VDOT
            double Lij = mData[ inxij ] -
                vdot( &( out[ inxjk ] ), &( out[ inxi0 ] ), j );
            inxjk += j;
#else
            double Lij = mData[ inxij ];
            for ( size_type k = 0; k < j; ++k )
            {
                Lij -= out[ inxjk++ ] * out[ inxi0 + k ];
            }
#endif
            Lij /= out[ inxjk++ ];
            out[ inxij++ ] = Lij;
            sumxx += Lij * Lij;
        }
        sumxx = mData[ inxij ] - sumxx;
        if ( sumxx < 0 )
        {
            throw runtime_error(
                "LTMatrix::cholesky: Matrix not positive definite" );
        }
        out[ inxij++ ] = sqrt( sumxx );
    }
    return out;
}

//======================================  Dump data
void
LTMatrix::dump( std::ostream& out ) const
{
    out << "LT Matrix, dim = " << mDim << endl;
    size_type inxij = 0;
    for ( size_type i = 0; i < mDim; i++ )
    {
        for ( size_type j = 0; j < i; j++ )
            out << mData[ inxij++ ] << "   ";
        out << mData[ inxij++ ] << endl;
    }
}

//======================================  Build a hankel matrix from column+row
void
LTMatrix::hankel( size_type N, const double column[], const double row[] )
{
    set_size( N );
    size_type inxij = 0;
    for ( size_type i = 0; i < N; i++ )
    {
        size_t endCpy = 2 * i + 1;
        size_t nMax = endCpy;
        if ( nMax > N )
            nMax = N;
        for ( size_t j = i; j < nMax; ++j )
            mData[ inxij++ ] = column[ j ];
        for ( size_t j = N; j < endCpy; ++j )
            mData[ inxij++ ] = row[ j - N + 1 ];
    }
    mType = kSymmetric;
}

//======================================  Multiply a vector by a matrix
void
LTMatrix::mpyvec( const double in[], double result[] ) const
{
    switch ( mType )
    {
    case kSymmetric: {
        for ( size_type i = 0; i < mDim; i++ )
        {
            double    sum = 0;
            size_type inxij = i * ( i + 1 ) / 2;
            for ( size_type j = 0; j < i; j++ )
            {
                sum += in[ j ] * mData[ inxij++ ];
            }
            for ( size_type j = i; j < mDim; j++ )
            {
                sum += in[ j ] * mData[ inxij++ ];
                inxij += j;
            }
            result[ i ] = sum;
        }
        break;
    }
    case kTriangle: {
        result[ 0 ] = in[ 0 ] * mData[ 0 ];
        size_type inxij = 1;
        for ( size_type i = 1; i < mDim; i++ )
        {
            result[ i ] = vdot( in, mData + inxij, i + 1 );
            inxij += i + 1;
        }
        break;
    }
    default:
        throw runtime_error( "LTMatrix::mpyvec: Invalid matrix type." );
    }
}

//======================================  Print coordinates
std::ostream&
LTMatrix::prt_coord( std::ostream& out, size_type inx ) const
{
    size_type k = 0;
    for ( size_type i = 0; i < mDim; i++ )
    {
        size_type l = k + i + 1;
        if ( inx < l )
        {
            return out << "[" << i << "," << inx - k << "]";
        }
        k = l;
    }
    return out << "[***]";
}

//======================================  Set the matrix size
void
LTMatrix::set_size( size_type N )
{
    if ( mData )
    {
        delete[] mData;
        mData = 0;
    }
    mDim = N;
    if ( mDim )
        mData = new double[ nData( ) ];
}

//======================================  Set the matrix type
void
LTMatrix::set_type( matrix_type mt )
{
    mType = mt;
}

//======================================  Solve a set of linear equations
void
LTMatrix::solve( const double vec[], double coefs[] )
{
    switch ( mType )
    {
    case kSymmetric: {
        //--------------------------  Calculate a cholesky decomposition
        LTMatrix cd( cholesky( ) );

        //--------------------------  Solve the lower triangle matrix
        lcl_array< double > t( mDim );
        cd.solve( vec, t );

        //--------------------------  Solve the transpose matrix
        cd.solveTranspose( t, coefs );
    }
    break;
    case kTriangle: {
#ifdef DEBUG_SOLVE
        cout << "Enter solve (triangle matrix): AX = Y. dim = " << mDim << endl;
#endif
        size_type inxij = 0;
        for ( size_type i = 0; i < mDim; ++i )
        {
            double xi = vec[ i ];
#ifdef DEBUG_SOLVE
            cout << "X[" << i << "] = (Y[" << i << "]";
#endif
            for ( size_type j = 0; j < i; j++ )
            {
#ifdef DEBUG_SOLVE
                cout << " - X[" << j << "] * A";
                prt_coord( cout, inxij );
#endif
                xi -= mData[ inxij++ ] * coefs[ j ];
            }
#ifdef DEBUG_SOLVE
            cout << ") / A";
            prt_coord( cout, inxij ) << endl;
#endif
            coefs[ i ] = xi / mData[ inxij++ ];
        }
    }
    break;
    default:
        throw runtime_error( "LTMatrix::solve: Invalid matrix type." );
    }
}

//======================================  Solve a set of linear equations
void
LTMatrix::solveTranspose( const double vec[], double coefs[] )
{
    if ( mType == kTriangle )
    {
#ifdef DEBUG_SOLVE
        cout << "Enter solveTranspose: AX = Y. dim = " << mDim << endl;
#endif
        for ( size_type j = mDim; j; )
        {
            double    xj = vec[ --j ];
            size_type inxij = mDim * ( mDim - 1 ) / 2 + j;
#ifdef DEBUG_SOLVE
            cout << "X[" << j << "] = (Y[" << j << "]";
#endif
            for ( size_type i = mDim - 1; i > j; --i )
            {
                xj -= mData[ inxij ] * coefs[ i ];
#ifdef DEBUG_SOLVE
                cout << " - X[" << i << "] * A";
                prt_coord( cout, inxij );
#endif
                inxij -= i;
            }
            coefs[ j ] = xj / mData[ inxij ];
#ifdef DEBUG_SOLVE
            cout << ") / A";
            prt_coord( cout, inxij ) << endl;
#endif
        }
    }

    //===================================  Only solve transposed mtx fot LTs
    else
    {
        throw runtime_error( "LTMatrix::solveTranspose: Invalid matrix type." );
    }
}

//====================================== Generate a toeplitx matrix
void
LTMatrix::toeplitz( size_type N, const double col[] )
{
    size_type inxij = 0;
    for ( size_type i = 0; i < N; ++i )
    {
        for ( size_type j = i + 1; j != 0; )
            mData[ inxij++ ] = col[ --j ];
    }
    mType = kSymmetric;
}
