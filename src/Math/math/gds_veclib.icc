#ifndef GEN_VECTOR_LIBRARY2_ICC
#define GEN_VECTOR_LIBRARY2_ICC
#include "gen_vect.hh"

//======================================  Decide on generic vs. x86
extern gen_vect global_gen_vect;

//======================================  Add vectors
inline void 
vadd3(const double* a, const double* b, double* c, std::size_t n) {
   global_gen_vect.add(c, a, b, n);
}

inline void 
vadd(const double* a, double* b, std::size_t n) {
   global_gen_vect.add(b, a, n);
}

//======================================  Convert float->int vector
inline void 
vcvtfi(const float* a, int* b, std::size_t n) {
   global_gen_vect.cvt(b, a, n);
}

//======================================  Convert int->float vector
inline void 
vcvtif(const int* a, float* b, std::size_t n) {
   global_gen_vect.cvt(b, a, n);
}

//======================================  Inner product.
inline vec_result
vdot(const double* a, const double* b, std::size_t n) {
   return global_gen_vect.dot(a, b, n);
}

//======================================  Multiply vectors
inline void 
vmul3(const double* a, const double* b, double* c, std::size_t n) {
   global_gen_vect.mul(c, a, b, n);
}

inline void 
vmul(const double* a, double* c, std::size_t n) {
   global_gen_vect.mul(c, a, n);
}

inline void 
vmul(const dComplex* a, dComplex* c, std::size_t n) {
   global_gen_vect.mul(c, a, n);
}

//======================================  Multiply vectors
inline void 
vmuladd(const double* a, const double* b, double* c, std::size_t n) {
   global_gen_vect.muladd(c, a, b, n);
}

//======================================  Multiply vectors
inline void 
vmuladd(const double* a, double  b, double* c, std::size_t n) {
   global_gen_vect.muladd(c, a, b, n);
}

//======================================  Scale a vector.
inline void 
vscale(double* a, double b, std::size_t n) {
   global_gen_vect.mul(a, b, n);
}

//======================================  Sum of vector entries.
inline vec_result 
vsum(const double* a, std::size_t n) {
   return global_gen_vect.sum(a, n);
}

//======================================  Calculate complex mod Squared.
inline void 
vcmodsq(const dComplex* a, double* b, std::size_t n) {
   global_gen_vect.cmodsq(b, reinterpret_cast<const double*>(a), n);
}

inline void 
vcmuld(const double* a, dComplex* b, std::size_t n) {
   global_gen_vect.muld(b, a, n);
}

#endif // !defined(gen_vector_library_hh)
