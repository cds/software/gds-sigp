#ifndef GDS_VECTOR_LIBRARY_HH
#define GDS_VECTOR_LIBRARY_HH
#include "Complex.hh"
#include <cstddef>

//======================================  Useful data types
typedef double vec_result;

//======================================  Add vectors
void vadd3( const double* a, const double* b, double* c, std::size_t n );
void vadd( const double* a, double* c, std::size_t n );

//======================================  Convert float->int vector
void vcvtfi( const float* a, int* b, std::size_t n );

//======================================  Convert int->float vector
void vcvtif( const int* a, float* b, std::size_t n );

//======================================  Inner product.
vec_result vdot( const double* a, const double* b, std::size_t n );

//======================================  Multiply vectors
void vmul3( const double* a, const double* b, double* c, std::size_t n );
void vmul( const double* a, double* c, std::size_t n );
void vmul( const dComplex* a, dComplex* c, std::size_t n );

//======================================  Multiply and add vectors
void vmuladd( const double* a, const double* b, double* c, std::size_t n );
void vmuladd( const double* a, double b, double* c, std::size_t n );

//======================================  Scale a vector.
void vscale( double* a, double b, std::size_t n );

//======================================  Sum of vector entries.
vec_result vsum( const double* a, std::size_t n );

//======================================  Calculate complex mod Squared.
void vcmodsq( const dComplex* a, double* b, std::size_t n );
void vcmuld( const double* a, dComplex* b, std::size_t n );

/**  Include the implementation of all the functions.
 */
#include "gds_veclib.icc"

#endif // !defined(gds_vector_library_hh)
