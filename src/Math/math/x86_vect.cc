/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "gen_vect.hh"
#include <iostream>
#include <cstdlib>
#include "gds_math_arch.hh"

using namespace std;

gen_vect global_gen_vect;

//======================================  Constructor - find the capabilities
#define cpuid( func, ax, bx, cx, dx )                                          \
    __asm__ __volatile__( "cpuid"                                              \
                          : "=a"( ax ), "=b"( bx ), "=c"( cx ), "=d"( dx )     \
                          : "a"( func ) );

inline const char*
blurb( const char* s, bool t )
{
    if ( t )
        return s;
    return "";
}

//======================================  Constructor - find the capabilities
gen_vect::gen_vect( void )
{
    bool simd_status = ( getenv( "GDS_SIMD_STATUS" ) != NULL );

    int eax, ebx, ecx, edx;
    cpuid( 1, eax, ebx, ecx, edx );

    bool t_sse2 = ( edx & 0x04000000 );
    bool t_ssse3 = ( ecx & 0x00000200 );
    bool t_sse4_1 = ( ecx & 0x00080000 );
    bool t_sse4_2 = ( ecx & 0x00100000 );
    bool t_avx = ( ecx & 0x10000000 );
    if ( t_avx && ( ecx & 0x08000000 ) == 0 )
    {
        if ( simd_status )
            cerr << "avx not supported by O/S" << endl;
        t_avx = false;
    }

    if ( simd_status )
    {
        cerr << "simd features supported: " << blurb( "sse2", t_sse2 ) << " "
             << blurb( "ssse3", t_ssse3 ) << " " << blurb( "sse4.1", t_sse4_1 )
             << " " << blurb( "sse4.2", t_sse4_2 ) << " "
             << blurb( "avx", t_avx ) << endl;
    }

    if ( t_avx )
    {
#ifdef __AVX__
        set_sse_id( avx );
#else
        if ( simd_status )
            cerr << "vect_math not built for avx" << endl;
        set_sse_id( sse4_2 );
#endif
    }
    else if ( t_sse4_2 )
    {
        set_sse_id( sse4_2 );
    }
    else if ( t_sse4_1 )
    {
        set_sse_id( sse4_1 );
    }
    else if ( t_ssse3 )
    {
        set_sse_id( ssse3 );
    }
    else if ( t_sse2 )
    {
        set_sse_id( sse2 );
    }
    else
    {
        set_sse_id( nosse );
    }
}

//=======================================  Force a specific cpu implementation.
void
gen_vect::set_sse_id( cpu_sse capable )
{
    _sseID = capable;
    if ( getenv( "GDS_SIMD_STATUS" ) != NULL )
    {
        cerr << "SSE type set to: " << sse_id_name( _sseID ) << endl;
    }
}

//=======================================  Force a specific cpu implementation.
const char*
gen_vect::sse_id_name( cpu_sse capable )
{
    switch ( capable )
    {
    case nosse:
        return "nosse";
    case sse2:
        return "sse2";
    case ssse3:
        return "ssse3";
    case sse4_1:
        return "sse4_1";
    case sse4_2:
        return "sse4_2";
    case avx:
        return "avx";
    }
    return "unknown";
}

//=========================================================================
//
//             Add vectors
//
//=========================================================================

//======================================  add(double out[], double in[], N)
template <>
void
gen_vect::add( double out[], const double in[], size_t N ) const
{
    switch ( _sseID )
    {
    case avx:
    case sse2:
    case ssse3:
    case sse4_1:
    case sse4_2:
        //--------------------------------  Check 16-byte relative alignment
        if ( align16_rel( out, in ) )
        {

            //-----------------------------  Single operation if absolute
            if ( !align16_abs( out ) )
            {
                *out++ += *in++;
                N--;
            }

            //-----------------------------  Use 2instructions per.
            size_t nLoop = N >> 1;
            if ( nLoop )
            {
                const vecd128* av = reinterpret_cast< const vecd128* >( in );
                vecd128*       cv = reinterpret_cast< vecd128* >( out );
                for ( size_t i = 0; i < nLoop; ++i )
                {
                    vecd128 c = cv[ i ];
                    c += av[ i ];
                    cv[ i ] = c;
                }
            }
            if ( ( N & 1 ) != 0 )
                out[ N - 1 ] += in[ N - 1 ];
            break;
        }
    default:
        while ( N-- )
            *( out++ ) += *( in++ );
    }
}

//======================================  add(double out[], double in1[],
//                                            double in2[], N)
template <>
void
gen_vect::add( double       out[],
               const double in1[],
               const double in2[],
               size_t       N ) const
{
    switch ( _sseID )
    {
    case avx:
    case sse4_2:
        //------------------------------------  Test for unaligned output
        if ( !align16_rel( in1, out ) && align16_rel( in1, in2 ) )
        {

            //------------------------------  One double add if phase wrong
            if ( !align16_abs( in1 ) )
            {
                *out++ = *in1++ + *in2++;
                N--;
            }

            //----------------------------------  Loop sse2 add, unaligned store
            if ( N > 1 )
            {
                size_t         nLoop = N >> 1;
                const vecd128* av = reinterpret_cast< const vecd128* >( in1 );
                const vecd128* bv = reinterpret_cast< const vecd128* >( in2 );
                for ( size_t i = 0; i < nLoop; ++i )
                {
                    vecd128 tmp = av[ i ];
                    tmp += bv[ i ];
                    asm( "movdqu %1, %0" : "=m"( out[0] ) : "x"( tmp ) : );
                    out += 2;
                }
                nLoop = N & ~1;
                in1 += nLoop;
                in2 += nLoop;
                N &= 1;
            }
        }

    case sse2:
    case ssse3:
    case sse4_1:
        //--------------------------------  Test 3-fold alignment
        if ( align16_rel( out, in1, in2 ) )
        {

            //-----------------------------  One double add if phase wrong
            if ( !align16_abs( out ) )
            {
                *out++ = *in1++ + *in2++;
                N--;
            }

            //-----------------------------  Loop over sse2 add
            if ( N > 1 )
            {
                size_t         nLoop = N >> 1;
                const vecd128* av = reinterpret_cast< const vecd128* >( in1 );
                const vecd128* bv = reinterpret_cast< const vecd128* >( in2 );
                vecd128*       cv = reinterpret_cast< vecd128* >( out );
                for ( size_t i = 0; i < nLoop; ++i )
                    cv[ i ] = av[ i ] + bv[ i ];
                in1 += N & ~1;
                in2 += N & ~1;
                out += N & ~1;
                N &= 1;
            }
        }

        //--------------------------------  Test 2-fold alignment
        else if ( align16_rel( in1, in2 ) )
        {

            //-----------------------------  One double add if phase wrong
            if ( !align16_abs( in1 ) )
            {
                *out++ = *in1++ + *in2++;
                N--;
            }

            //-----------------------------  Loop over sse2 add
            if ( N > 1 )
            {
                size_t         nLoop = N >> 1;
                vd2map         sumu;
                const vecd128* av = reinterpret_cast< const vecd128* >( in1 );
                const vecd128* bv = reinterpret_cast< const vecd128* >( in2 );
                for ( size_t i = 0; i < nLoop; ++i )
                {
                    vecd128 tmp = av[ i ];
                    tmp += bv[ i ];
                    sumu.v = tmp;
                    *out++ = sumu.d[ 0 ];
                    *out++ = sumu.d[ 1 ];
                }
                in1 += N & ~1;
                in2 += N & ~1;
                N &= 1;
            }
        }

    default:
        //--------------------------------  tight loop...
        while ( N-- )
            *( out++ ) = *( in1++ ) + *( in2++ );
    }
}

template <>
void
gen_vect::add( double out[], double in, size_t N ) const
{
    switch ( _sseID )
    {
    case avx:
        //--------------------------------  Align for avx instructions
        while ( N && !align32_abs( out ) )
        {
            *out++ *= in;
            N--;
        }

        //--------------------------------  Multiply 4 at a time
        if ( N > 3 )
        {
            vecd256* av = reinterpret_cast< vecd256* >( out );
            vecd256  bv = { in, in, in, in };
            while ( N > 3 )
            {
                vecd256 c = *av;
                c *= bv;
                N -= 4;
                *av++ = c;
            }
            out = reinterpret_cast< double* >( av );
        }

    case sse2:
    case ssse3:
    case sse4_1:
    case sse4_2:
        //--------------------------------  Test sse alignment
        if ( N && !align16_abs( out ) )
        {
            *out++ += in;
            N--;
        }

        if ( N > 1 )
        {
            size_t   nLoop = N >> 1;
            vecd128* av = reinterpret_cast< vecd128* >( out );
            SSE_REG_LOAD_DUP( bv, in );
            while ( nLoop-- )
                *av++ += bv;
            out = reinterpret_cast< double* >( av );
            N &= 1;
        }

    default:
        while ( N-- )
        {
            *out++ += in;
        }
    }
}

//======================================  Complex mod-squared
template <>
void
gen_vect::cmodsq( double out[], const double in[], size_t n ) const
{
    switch ( _sseID )
    {
    case avx:
    case ssse3:
    case sse4_1:
    case sse4_2:
        if ( align16_abs( in ) )
        {
            const vecd128* av = reinterpret_cast< const vecd128* >( in );
            //  Multply two in a shot
            while ( n > 1 )
            {
                vecd128 prod0 = *av++;
                vecd128 prod1 = *av++;
                prod0 *= prod0;
                prod1 *= prod1;
                asm( "haddpd %2, %1;" // does this really || add and pack.
                     "movdqu %1, %0"
                     : "=m"( out[0] )
                     : "x"( prod0 ), "x"( prod1 )
                     : );
                out += 2;
                n -= 2;
            }
            //------------------------------- Calculate the remaining modsq
            if ( n )
            {
                const double* in = reinterpret_cast< const double* >( av );
                *out = in[ 0 ] * in[ 0 ] + in[ 1 ] * in[ 1 ];
            }
        }

        //----------------------------------  Same trick for unaligned data
        else
        {
            while ( n > 1 )
            {
                asm( "lddqu %1, %%xmm0;"
                     "mulpd %%xmm0, %%xmm0;"
                     "lddqu %2, %%xmm1;"
                     "mulpd %%xmm1, %%xmm1;"
                     "haddpd %%xmm1, %%xmm0;"
                     "movdqu %%xmm0, %0"
                     : "=m"( out[0] )
                     : "g"( in[0] ), "g"( in[2] )
                     : "xmm0", "xmm1" );
                in += 4;
                out += 2;
                n -= 2;
            }
            if ( n )
            {
                *out = in[ 0 ] * in[ 0 ] + in[ 1 ] * in[ 1 ];
            }
        }
        break;

        //-----------------------------------  vcmodsq using sse2.
    case sse2:
        if ( align16_abs( in ) )
        {
            const vecd128* av = reinterpret_cast< const vecd128* >( in );
            for ( size_t i = 0; i < n; i++ )
            {
                vecd128 prod = *av++;
                prod *= prod;
                asm( "pshufd $78, %1, %%xmm1;"
                     //"addpd  %1, %%xmm1;"
                     "addsd  %1, %%xmm1;"
                     "movsd  %%xmm1, %0"
                     : "=m"( out[0] )
                     : "x"( prod )
                     : "xmm1" );
                out++;
            }
        }

        //---------------------------------  Phased evaluation.
        //   imaginary and real ports of successive complex numbers are
        //   calculated with a single 2-operand SSE2 vector.
        else
        {
            vecd128 span;
            asm( "movsd %1, %0;"
                 "mulsd %0, %0"
                 : "=x"( span )
                 : "m"( in[0] )
                 : );

            const vecd128* av = reinterpret_cast< const vecd128* >( ++in );
            n--;
            for ( size_t i = 0; i < n; i++ )
            {
                vecd128 prod = *av++;
                prod *= prod;
                asm( "addsd  %2, %1;"
                     "movsd  %1, %0;"
                     "pshufd $78, %2, %1;"
                     : "=m"( out[0] ), "=x"( span )
                     : "x"( prod ), "x"( span )
                     : );
                out++;
            }

            in = reinterpret_cast< const double* >( av );
            asm( "movsd %1, %%xmm0;"
                 "mulsd %%xmm0, %%xmm0;"
                 "addsd %2, %%xmm0;"
                 "movsd %%xmm0, %0"
                 : "=m"( out[0] )
                 : "m"( in[0] ), "x"( span )
                 : "xmm0" );
        }
        break;

    default:
        while ( n-- )
        {
            *out++ = in[ 0 ] * in[ 0 ] + in[ 1 ] * in[ 1 ];
            in += 2;
        }
    }
}

//======================================  Vector conversion
//                                        cvt(float out[], int in[], N)
template <>
void
gen_vect::cvt( float out[], const int in[], size_t N ) const
{
    switch ( _sseID )
    {
    case avx:
    case sse2:
    case ssse3:
    case sse4_1:
    case sse4_2:

        //--------------------------------  Check alignment for sse2.
        if ( align16_rel( in, out ) )
        {
            while ( N && !align16_abs( in ) )
            {
                *out++ = float( *in++ );
                N--;
            }

            //-----------------------------  Convert 4 at a shot
            const veci128* av = reinterpret_cast< const veci128* >( in );
            vecf128*       bv = reinterpret_cast< vecf128* >( out );
            for ( size_t nquad = ( N >> 2 ); nquad; --nquad )
            {
                asm( "cvtdq2ps %1, %%xmm0;"
                     "movdqa   %%xmm0, %0"
                     : "=m"( bv[0] )
                     : "m"( av[0] )
                     : "xmm0" );
                av++;
                bv++;
            }

            //-----------------------------  Leave argument values for final
            N &= 3;
            in = reinterpret_cast< const int* >( av );
            out = reinterpret_cast< float* >( bv );
        }
    default:
        while ( N-- )
        {
            *out++ = float( *in++ );
        }
    }
}

//======================================  Vector conversion
//                                        cvt(int out[], const float in[], N)
template <>
void
gen_vect::cvt( int out[], const float in[], size_t N ) const
{
    switch ( _sseID )
    {
    case avx:
    case sse2:
    case ssse3:
    case sse4_1:
    case sse4_2:

        //--------------------------------  Check alignment for sse2.
        if ( align16_rel( in, out ) )
        {
            while ( N && !align16_abs( in ) )
            {
                *out++ = int( *in++ );
                N--;
            }

            //-----------------------------  Convert 4 at a shot
            const vecf128* av = reinterpret_cast< const vecf128* >( in );
            veci128*       bv = reinterpret_cast< veci128* >( out );
            for ( size_t nquad = ( N >> 2 ); nquad; --nquad )
            {
                asm( "cvtps2dq %1, %%xmm0;"
                     "movdqa   %%xmm0, %0"
                     : "=m"( bv[0] )
                     : "m"( av[0] )
                     : "xmm0" );
                av++;
                bv++;
            }

            //-----------------------------  Leave argument values for final
            N &= 3;
            in = reinterpret_cast< const float* >( av );
            out = reinterpret_cast< int* >( bv );
        }
    default:
        while ( N-- )
        {
            *out++ = int( *in++ );
        }
    }
}

//======================================  Vector divide functions
template <>
void
gen_vect::div( double out[], const double* in, size_t N ) const
{
    switch ( _sseID )
    {
    case avx:

    case sse2:
    case ssse3:
    case sse4_1:
    case sse4_2:
        //--------------------------------  Check 16-byte relative alignment
        if ( align16_rel( out, in ) )
        {

            //-----------------------------  Single operation if absolute
            if ( !align16_abs( out ) )
            {
                *out++ /= *in++;
                N--;
            }

            //-----------------------------  Use 2instructions per.
            size_t nLoop = N >> 1;
            if ( nLoop )
            {
                const vecd128* av = reinterpret_cast< const vecd128* >( in );
                vecd128*       cv = reinterpret_cast< vecd128* >( out );
                for ( size_t i = 0; i < nLoop; ++i )
                {
                    vecd128 c = cv[ i ];
                    c /= av[ i ];
                    cv[ i ] = c;
                }
            }
            if ( ( N & 1 ) != 0 )
                out[ N - 1 ] /= in[ N - 1 ];
            return;
        }
    default:
        while ( N-- )
            *( out++ ) /= *( in++ );
    }
}

template <>
void
gen_vect::div( double        out[],
               const double* in1,
               const double* in2,
               size_t        N ) const
{
    switch ( _sseID )
    {
    case avx:
    case sse4_2:
        //------------------------------------  Test for unaligned output
        if ( !align16_rel( in1, out ) && align16_rel( in1, in2 ) )
        {

            //------------------------------  One double add if phase wrong
            if ( !align16_abs( in1 ) )
            {
                *out++ = *in1++ / *in2++;
                N--;
            }

            //----------------------------------  Loop sse2 add, unaligned store
            size_t nLoop = N >> 1;
            if ( nLoop )
            {
                const vecd128* av = reinterpret_cast< const vecd128* >( in1 );
                const vecd128* bv = reinterpret_cast< const vecd128* >( in2 );
                for ( size_t i = 0; i < nLoop; ++i )
                {
                    vecd128 tmp = av[ i ];
                    tmp /= bv[ i ];
                    asm( "movdqu %1, %0" : "=m"( out[0] ) : "x"( tmp ) : );
                    out += 2;
                }
            }

            //----------------------------------  Last element.
            if ( ( N & 1 ) != 0 )
                *out = in1[ N - 1 ] / in2[ N - 1 ];
            return;
        }

    case sse2:
    case ssse3:
    case sse4_1:
        //--------------------------------  Test 3-fold alignment
        if ( align16_rel( out, in1, in2 ) )
        {

            //-----------------------------  One double add if phase wrong
            if ( !align16_abs( out ) )
            {
                *out++ = *in1++ / *in2++;
                N--;
            }

            //-----------------------------  Loop over sse2 add
            if ( N > 1 )
            {
                size_t         nLoop = N >> 1;
                const vecd128* av = reinterpret_cast< const vecd128* >( in1 );
                const vecd128* bv = reinterpret_cast< const vecd128* >( in2 );
                vecd128*       cv = reinterpret_cast< vecd128* >( out );
                for ( size_t i = 0; i < nLoop; ++i )
                    cv[ i ] = av[ i ] / bv[ i ];
                in1 += N & ~1;
                in2 += N & ~1;
                out += N & ~1;
                N &= 1;
            }
        }

        //--------------------------------  Test 2-fold alignment
        else if ( align16_rel( in1, in2 ) )
        {

            //-----------------------------  One double add if phase wrong
            if ( !align16_abs( in1 ) )
            {
                *out++ = *in1++ / *in2++;
                N--;
            }

            //-----------------------------  Loop over sse2 add
            if ( N > 1 )
            {
                size_t         nLoop = N >> 1;
                vd2map         sumu;
                const vecd128* av = reinterpret_cast< const vecd128* >( in1 );
                const vecd128* bv = reinterpret_cast< const vecd128* >( in2 );
                for ( size_t i = 0; i < nLoop; ++i )
                {
                    vecd128 tmp = av[ i ];
                    tmp /= bv[ i ];
                    sumu.v = tmp;
                    *out++ = sumu.d[ 0 ];
                    *out++ = sumu.d[ 1 ];
                }
                in1 += N & ~1;
                in2 += N & ~1;
                N &= 1;
            }
        }

    default:
        //--------------------------------  tight loop...
        while ( N-- )
            *( out++ ) = *( in1++ ) / *( in2++ );
    }
}

template <>
void
gen_vect::div( double out[], double in, size_t N ) const
{
    switch ( _sseID )
    {
    case avx:
        //--------------------------------  Align for avx instructions
        while ( N && !align32_abs( out ) )
        {
            *out++ /= in;
            N--;
        }

        //--------------------------------  Multiply 4 at a time
        if ( N > 3 )
        {
            vecd256* av = reinterpret_cast< vecd256* >( out );
            vecd256  bv = { in, in, in, in };
            while ( N > 3 )
            {
                vecd256 c = *av;
                c /= bv;
                N -= 4;
                *av++ = c;
            }
            out = reinterpret_cast< double* >( av );
        }

    case sse2:
    case ssse3:
    case sse4_1:
    case sse4_2:
        //--------------------------------  Test sse alignment
        if ( N && !align16_abs( out ) )
        {
            *out++ /= in;
            N--;
        }

        if ( N > 1 )
        {
            size_t   nLoop = N >> 1;
            vecd128* av = reinterpret_cast< vecd128* >( out );
            SSE_REG_LOAD_DUP( bv, in );
            while ( nLoop-- )
                *av++ /= bv;
            out = reinterpret_cast< double* >( av );
            N &= 1;
        }

    default:
        while ( N-- )
        {
            *out++ /= in;
        }
    }
}

//=========================================================================
//
//    Inner product (dot) functions
//
//=========================================================================
template <>
gen_vect::math_type
gen_vect::dot( const double in1[], const double in2[], size_t N ) const
{
    math_type sum = 0.0;
    switch ( _sseID )
    {
    case avx:
    case sse4_2:
    case sse4_1:
    case ssse3:
    case sse2:
        //----------------------------------  Check alignment for sse2.
        if ( align16_rel( in1, in2 ) )
        {

            //------------------------------  Calculate 1 word if unaligned.
            if ( !align16_abs( in1 ) )
            {
                sum = ( *in1++ ) * ( *in2++ );
                N--;
            }

            //------------------------------  Loop over pairs with sse2
            if ( N > 1 )
            {
                size_t         nLoop = N >> 1;
                const vecd128* av = reinterpret_cast< const vecd128* >( in1 );
                const vecd128* bv = reinterpret_cast< const vecd128* >( in2 );
                vecd128        sumv = *av * *bv;
                for ( size_t i = 1; i < nLoop; ++i )
                    sumv += av[ i ] * bv[ i ];
                //--------------------------  sse2 architecture.
                if ( _sseID == sse2 )
                {
                    vd2map sumu;
                    sumu.v = sumv;
                    sum += sumu.d[ 0 ] + sumu.d[ 1 ];
                }

                //--------------------------  ssse3, sse4.1 architectures
                else
                {
                    double dvec;
                    asm( "haddpd %1, %1;"
                         "movsd %1, %0"
                         : "=m"( dvec )
                         : "x"( sumv ) );
                    sum += dvec;
                }
                nLoop = N & ~1;
                in1 += nLoop;
                in2 += nLoop;
                N &= 1;
            }
        }

        //----------------------------------  Use a single loop if unaligned.
        else if ( N > 1 )
        {
            size_t         nVec = N >> 1;
            const vecd128* av;
            const double*  bv;
            if ( !align16_abs( in2 ) )
            {
                av = reinterpret_cast< const vecd128* >( in1 );
                bv = in2;
            }
            else
            {
                av = reinterpret_cast< const vecd128* >( in2 );
                bv = in1;
            }

            vecd128 sumv = { 0.0, 0.0 };
            vecd128 temp;
            if ( _sseID == sse2 )
            {
                for ( size_t i = 0; i < nVec; i++ )
                {
                    asm( "movupd %1, %0" : "=x"( temp ) : "m"( bv[0] ) );
                    bv += 2;
                    temp *= av[ i ];
                    sumv += temp;
                }
                vd2map sumu;
                sumu.v = sumv;
                sum += sumu.d[ 0 ] + sumu.d[ 1 ];
            }
            else
            {
                for ( size_t i = 0; i < nVec; i++ )
                {
                    asm( "lddqu %1, %0" : "=x"( temp ) : "m"( bv[0] ) );
                    bv += 2;
                    temp *= av[ i ];
                    sumv += temp;
                }
                double dvec;
                asm( "haddpd %1, %1;"
                     "movsd %1, %0"
                     : "=m"( dvec )
                     : "x"( sumv ) );
                sum += dvec;
            }
            in1 += ( N & ~1 );
            in2 += ( N & ~1 );
            N &= 1;
        }

    default:
        while ( N-- )
            sum += *( in1++ ) * *( in2++ );
    }
    return sum;
}

//=========================================================================
//
//    Multiplication functions
//
//=========================================================================

//======================================  Two-operand double vector multiply
template <>
void
gen_vect::mul( double out[], const double* in, size_t N ) const
{
    switch ( _sseID )
    {
    case avx:
    case sse2:
    case ssse3:
    case sse4_1:
    case sse4_2:
        //--------------------------------  Check 16-byte relative alignment
        if ( align16_rel( out, in ) )
        {

            //-----------------------------  Single operation if absolute
            if ( !align16_abs( out ) )
            {
                *out++ *= *in++;
                N--;
            }

            //-----------------------------  Use 2instructions per.
            size_t nLoop = N >> 1;
            if ( nLoop )
            {
                const vecd128* av = reinterpret_cast< const vecd128* >( in );
                vecd128*       cv = reinterpret_cast< vecd128* >( out );
                for ( size_t i = 0; i < nLoop; ++i )
                {
                    vecd128 c = cv[ i ];
                    c *= av[ i ];
                    cv[ i ] = c;
                }
            }
            if ( ( N & 1 ) != 0 )
                out[ N - 1 ] *= in[ N - 1 ];
            return;
        }
    default:
        while ( --N )
            *( out++ ) *= *( in++ );
    }
}

#ifndef __AVX__
//======================================  Two-operand complex multiply
inline void
dcplx_mul( vecd128& out, const dComplex& in )
{
    const double* b = reinterpret_cast< const double* >( &in );
    asm( "movapd %0, %%xmm1;" // xmm1 = {ar, ai}
         "shufpd $1, %%xmm1, %%xmm1;" // xmm1 = {ai, ar}
         "movddup %1, %%xmm2;" // xmm2 = {br, br}
         "mulpd %%xmm2, %0;" // mla  = {ar*br, ai*br}
         "movddup %2, %%xmm2;" // xmm2 = {bi, bi}
         "mulpd %%xmm2, %%xmm1;" // xmm1 = {ai*bi, ar*bi}
         "addsubpd %%xmm1, %0;" // mla  = {ar*br-ai*bi, ai*br+ar*bi}
         : "+x"( out )
         : "m"( b[0] ), "m"( b[1] )
         : "xmm1", "xmm2" );
}

#else // defined(__AVX__)
//======================================  Perform 2 simultaneous complex
//                                        multiplications using avx
inline void
dcplx_mul2( vecd256& out, const vecd256& in )
{
    asm(
        "vmovapd %0, %%ymm0;" // ymm0 = {a1r, a1i, a2r, a2i}
        "vshufpd $5, %%ymm0, %%ymm0, %%ymm1;" // ymm1 = {a1i, a1r, a2i, a2r}
        "vmovapd %1, %%ymm0;" // ymm0 = {b1r, b1i, b2r, b2i}
        "vshufpd $0, %%ymm0, %%ymm0, %%ymm2;" // ymm2 = {b1r, b1r, b2r, b2r}
        "vmulpd %0, %%ymm2, %0;" // oreg = {a1r*b1r, a1i*b1r, a2r*b2r, a2i*b2r}
        "vshufpd $15, %%ymm0, %%ymm0, %%ymm2;" // ymm2 = {b1i, b1i, b2i, b2i}
        "vmulpd %%ymm2, %%ymm1, %%ymm1;" // ymm1 = {a1i*b1i, a1r*b1i, a2i*b2i, a2r*b2i}
        "vaddsubpd %%ymm1, %0, %0;" // oreg  = {ar*br-ai*bi, ai*br+ar*bi}
#if __GNUC__ == 4
        : "+x"( out )
        : "m"( in )
        : "xmm0", "xmm1", "xmm2"
#else
        : "+v"( out )
        : "m"( in )
        : "ymm0", "ymm1", "ymm2"
#endif
    );
}
#endif

//======================================  ssse3 complex multiply
template <>
void
gen_vect::mul( dComplex out[], const dComplex in[], size_t N ) const
{
    switch ( _sseID )
    {

#ifdef __AVX__
    case avx:
        if ( align32_rel( in, out ) && align16_abs( out ) )
        {
            //cerr << "avx aligned complex multiply, N= " << N << endl;
            if ( !align32_abs( in, out ) )
            {
                *( out++ ) *= *( in++ );
                N--;
            }

            //-----------------------------  Pair-wise multiply
            const vecd256* av = reinterpret_cast< const vecd256* >( in );
            vecd256*       bv = reinterpret_cast< vecd256* >( out );
            for ( ; N > 1; N -= 2 )
            {
                dcplx_mul2( *bv++, *av++ );
            }

            if ( N )
            {
                out = reinterpret_cast< dComplex* >( bv );
                in = reinterpret_cast< const dComplex* >( av );
                *( out++ ) *= *( in++ );
                N--;
            }
            return;
        }

#else // !defined(__AVX__)                                                     \
    //--------------------------------  Skylark and later nodes have a         \
    //                                  per-iteration upper register save.     \
    //                                  This makes use of sse3 instructions    \
    //                                  inefficient when compiled with AVX
    case ssse3:
    case sse4_1:
    case sse4_2:
        if ( align16_abs( out ) )
        {
            //cerr << "sse3 aligned complex multiply, N= " << N << endl;
            vecd128* av = reinterpret_cast< vecd128* >( out );
            for ( ; N; N-- )
            {
                dcplx_mul( *av++, *in++ );
            }
            return;
        }
#endif

    default:
        //--------------------------------  Use normal stuff
        //cerr << "unaligned complex multiply, N= " << N << endl;
        while ( N-- )
        {
            *out++ *= *in++;
        }
    }
}

//======================================  Three-operand double vector multiply
template <>
void
gen_vect::mul( double       out[],
               const double in1[],
               const double in2[],
               size_t       N ) const
{
    switch ( _sseID )
    {
    case avx:
    case sse4_2:
        //------------------------------------  Test for unaligned output
        if ( !align16_rel( in1, out ) && align16_rel( in1, in2 ) )
        {

            //------------------------------  One double add if phase wrong
            if ( !align16_abs( in1 ) )
            {
                *out++ = *in1++ * *in2++;
                N--;
            }

            //----------------------------------  Loop sse2 add, unaligned store
            if ( N > 1 )
            {
                size_t         nLoop = N >> 1;
                const vecd128* av = reinterpret_cast< const vecd128* >( in1 );
                const vecd128* bv = reinterpret_cast< const vecd128* >( in2 );
                for ( size_t i = 0; i < nLoop; ++i )
                {
                    vecd128 tmp = av[ i ];
                    tmp *= bv[ i ];
                    asm( "movdqu %1, %0" : "=m"( out[0] ) : "x"( tmp ) : );
                    out += 2;
                }
                nLoop = N & ~1;
                in1 += nLoop;
                in2 += nLoop;
                N &= 1;
            }
        }

    case sse2:
    case ssse3:
    case sse4_1:
        //--------------------------------  Test 3-fold alignment
        if ( align16_rel( out, in1, in2 ) )
        {

            //-----------------------------  One double add if phase wrong
            if ( !align16_abs( out ) )
            {
                *out++ = *in1++ * *in2++;
                N--;
            }

            //-----------------------------  Loop over sse2 add
            if ( N > 1 )
            {
                size_t         nLoop = N >> 1;
                const vecd128* av = reinterpret_cast< const vecd128* >( in1 );
                const vecd128* bv = reinterpret_cast< const vecd128* >( in2 );
                vecd128*       cv = reinterpret_cast< vecd128* >( out );
                for ( size_t i = 0; i < nLoop; ++i )
                    cv[ i ] = av[ i ] * bv[ i ];
                nLoop <<= 1;
                in1 += nLoop;
                in2 += nLoop;
                out += nLoop;
                N &= 1;
            }
        }

        //--------------------------------  Test 2-fold alignment
        else if ( align16_rel( in1, in2 ) )
        {

            //-----------------------------  One double add if phase wrong
            if ( !align16_abs( in1 ) )
            {
                *out++ = *in1++ * *in2++;
                N--;
            }

            //-----------------------------  Loop over sse2 add
            if ( N > 1 )
            {
                size_t         nLoop = N >> 1;
                vd2map         sumu;
                const vecd128* av = reinterpret_cast< const vecd128* >( in1 );
                const vecd128* bv = reinterpret_cast< const vecd128* >( in2 );
                for ( size_t i = 0; i < nLoop; ++i )
                {
                    vecd128 tmp = av[ i ];
                    tmp *= bv[ i ];
                    sumu.v = tmp;
                    *out++ = sumu.d[ 0 ];
                    *out++ = sumu.d[ 1 ];
                }
                nLoop <<= 1;
                in1 += nLoop;
                in2 += nLoop;
                N &= 1;
            }
        }

    default:
        //--------------------------------  tight loop...
        while ( N-- )
            *( out++ ) = *( in1++ ) * *( in2++ );
    }
}

//======================================  Three-operand double vector multiply
template <>
void
gen_vect::muladd( double       out[],
                  const double in1[],
                  const double in2[],
                  size_t       N ) const
{
    switch ( _sseID )
    {
    case avx:
        //------------------------------------  Test for avx aligned output
        if ( align32_rel( in1, in2, out ) )
        {

            //------------------------------  One double add if phase wrong
            while ( N && !align32_abs( out ) )
            {
                *out++ += *in1++ * *in2++;
                N--;
            }

            //----------------------------------  Loop sse2 add, unaligned store
            if ( N > 3 )
            {
                const vecd256* av = reinterpret_cast< const vecd256* >( in1 );
                const vecd256* bv = reinterpret_cast< const vecd256* >( in2 );
                vecd256*       cv = reinterpret_cast< vecd256* >( out );
                for ( ; N > 3; N -= 4 )
                {
                    *( cv++ ) += *( av++ ) * *( bv++ );
                }
                if ( N )
                {
                    in1 = reinterpret_cast< const double* >( av );
                    in2 = reinterpret_cast< const double* >( bv );
                    out = reinterpret_cast< double* >( cv );
                }
            }
        }

    case sse2:
    case ssse3:
    case sse4_1:
    case sse4_2:
        //--------------------------------  Test 3-fold alignment
        if ( align16_rel( out, in1, in2 ) )
        {

            //-----------------------------  One double add if phase wrong
            if ( !align16_abs( out ) )
            {
                *out++ = *in1++ * *in2++;
                N--;
            }

            //-----------------------------  Loop over sse2 add
            if ( N > 1 )
            {
                size_t         nLoop = N >> 1;
                const vecd128* av = reinterpret_cast< const vecd128* >( in1 );
                const vecd128* bv = reinterpret_cast< const vecd128* >( in2 );
                vecd128*       cv = reinterpret_cast< vecd128* >( out );
                for ( ; N > 1; N -= 2 )
                    *( cv++ ) += *( av++ ) * *( bv++ );
                if ( N )
                {
                    in1 = reinterpret_cast< const double* >( av );
                    in2 = reinterpret_cast< const double* >( bv );
                    out = reinterpret_cast< double* >( cv );
                }
            }
        }

    default:
        //--------------------------------  tight loop...
        for ( ; N; N-- )
            *( out++ ) += *( in1++ ) * *( in2++ );
    }
}

//======================================  Scaler multiply and add (vector)
template <>
void
gen_vect::muladd( double out[], const double in1[], double in2, size_t N ) const
{
    switch ( _sseID )
    {
    case avx:
        //------------------------------------  Test for avx aligned output
        if ( align32_rel( in1, out ) )
        {

            //------------------------------  One double add if phase wrong
            while ( N && !align32_abs( out ) )
            {
                *out++ += *in1++ * in2;
                N--;
            }

            //----------------------------------  Loop sse2 add, unaligned store
            if ( N > 3 )
            {
                vecd256        bv = { in2, in2, in2, in2 };
                const vecd256* av = reinterpret_cast< const vecd256* >( in1 );
                vecd256*       cv = reinterpret_cast< vecd256* >( out );
                for ( ; N > 3; N -= 4 )
                {
                    *( cv++ ) += *( av++ ) * bv;
                }
                if ( N )
                {
                    in1 = reinterpret_cast< const double* >( av );
                    out = reinterpret_cast< double* >( cv );
                }
            }
        }

    case sse2:
    case ssse3:
    case sse4_1:
    case sse4_2:
        //--------------------------------  Test 3-fold alignment
        if ( align16_rel( out, in1 ) )
        {

            //-----------------------------  One double add if phase wrong
            if ( !align16_abs( out ) )
            {
                *out++ = *in1++ * in2;
                N--;
            }

            //-----------------------------  Loop over sse2 add
            if ( N > 1 )
            {
                vecd128        bv = { in2, in2 };
                const vecd128* av = reinterpret_cast< const vecd128* >( in1 );
                vecd128*       cv = reinterpret_cast< vecd128* >( out );
                for ( ; N > 1; N -= 2 )
                    *( cv++ ) += *( av++ ) * bv;
                if ( N )
                {
                    in1 = reinterpret_cast< const double* >( av );
                    out = reinterpret_cast< double* >( cv );
                }
            }
        }

    default:
        //--------------------------------  tight loop...
        for ( ; N; N-- )
            *( out++ ) += *( in1++ ) * in2;
    }
}

template <>
void
gen_vect::mul( double out[], double in, size_t N ) const
{
    switch ( _sseID )
    {
    case avx:
        //-----------------------------  Align for avx instructions
        while ( N && !align32_abs( out ) )
        {
            *out++ *= in;
            N--;
        }

        //-----------------------------  Multiply 4 at a time
        if ( N > 3 )
        {
            vecd256* av = reinterpret_cast< vecd256* >( out );
            vecd256  bv = { in, in, in, in };
            while ( N > 3 )
            {
                *av++ *= bv;
                N -= 4;
            }
            out = reinterpret_cast< double* >( av );
        }

    case sse2:
    case ssse3:
    case sse4_1:
    case sse4_2:
        //--------------------------------  Test sse alignment
        if ( N && !align16_abs( out ) )
        {
            *out++ *= in;
            N--;
        }

        if ( N > 1 )
        {
            size_t   nLoop = N >> 1;
            vecd128* av = reinterpret_cast< vecd128* >( out );
            SSE_REG_LOAD_DUP( bv, in );
            while ( nLoop-- )
                *av++ *= bv;
            out = reinterpret_cast< double* >( av );
        }

        if ( ( N & 1 ) != 0 )
            *out++ *= in;
        return;

    default:
        while ( N-- )
        {
            *out++ *= in;
        }
    }
}

//======================================  Multiply dcomplex by double vector
template <>
void
gen_vect::muld( dComplex out[], const double in[], size_t N ) const
{

    switch ( _sseID )
    {
    case avx:
    case sse2:
    case ssse3:
    case sse4_1:
    case sse4_2:
        //--------------------------------  Test sse alignment
        if ( align16_abs( out ) )
        {
            vecd128* av = reinterpret_cast< vecd128* >( out );
            while ( N-- )
            {
                SSE_REG_LOAD_DUP( bv, in[ 0 ] );
                in++;
                *av++ *= bv;
            }
            return;
        }
        else
        {
            cerr << "complex data misaligned: a = " << hex << long( out ) << dec
                 << endl;
        }
    default:
        while ( N-- )
        {
            *( out++ ) *= *( in++ );
        }
    }
}

//=========================================================================
//
//    Scale vector functions
//
//=========================================================================

//======================================  Scale a vector
template <>
void
gen_vect::scale( double out[], math_type in, size_t N ) const
{
    mul( out, in, N );
}

//======================================  Scale a vector
template <>
void
gen_vect::scale( dComplex out[], math_type in, size_t N ) const
{
    mul( reinterpret_cast< double* >( out ), in, 2 * N );
}

//=========================================================================
//
//    Subtraction functions
//
//=========================================================================

//======================================  Two-operand vector subtraction
template <>
void
gen_vect::sub( double out[], const double in[], size_t N ) const
{
    switch ( _sseID )
    {
    case avx:
    case sse2:
    case ssse3:
    case sse4_1:
    case sse4_2:
        //--------------------------------  Check 16-byte relative alignment
        if ( align16_rel( out, in ) )
        {

            //-----------------------------  Single operation if absolute
            if ( !align16_abs( out ) )
            {
                *out++ -= *in++;
                N--;
            }

            //-----------------------------  Use 2instructions per.
            size_t nLoop = N >> 1;
            if ( nLoop )
            {
                const vecd128* av = reinterpret_cast< const vecd128* >( in );
                vecd128*       cv = reinterpret_cast< vecd128* >( out );
                for ( size_t i = 0; i < nLoop; ++i )
                {
                    vecd128 c = cv[ i ];
                    c -= av[ i ];
                    cv[ i ] = c;
                }
            }
            if ( ( N & 1 ) != 0 )
                out[ N - 1 ] += in[ N - 1 ];
            return;
        }
    default:
        while ( --N )
            *( out++ ) -= *( in++ );
    }
}

//======================================  Three-operand vector subtract
template <>
void
gen_vect::sub( double       out[],
               const double in1[],
               const double in2[],
               size_t       N ) const
{
    switch ( _sseID )
    {
    case avx:
    case sse4_2:
        //------------------------------------  Test for unaligned output
        if ( !align16_rel( in1, out ) && align16_rel( in1, in2 ) )
        {

            //------------------------------  One double add if phase wrong
            if ( !align16_abs( in1 ) )
            {
                *out++ = *( in1++ ) - *( in2++ );
                N--;
            }

            //----------------------------------  Loop sse2 add, unaligned store
            size_t nLoop = N >> 1;
            if ( nLoop )
            {
                const vecd128* av = reinterpret_cast< const vecd128* >( in1 );
                const vecd128* bv = reinterpret_cast< const vecd128* >( in2 );
                for ( size_t i = 0; i < nLoop; ++i )
                {
                    vecd128 tmp = av[ i ];
                    tmp -= bv[ i ];
                    asm( "movdqu %1, %0" : "=m"( out[0] ) : "x"( tmp ) : );
                    out += 2;
                }
            }

            //----------------------------------  Last element.
            if ( ( N & 1 ) != 0 )
                *out = in1[ N - 1 ] - in2[ N - 1 ];
            return;
        }

    case sse2:
    case ssse3:
    case sse4_1:
        //--------------------------------  Test 3-fold alignment
        if ( align16_rel( out, in1, in2 ) )
        {

            //-----------------------------  One double add if phase wrong
            if ( !align16_abs( out ) )
            {
                *out++ = *in1++ - *in2++;
                N--;
            }

            //-----------------------------  Loop over sse2 add
            if ( N > 1 )
            {
                size_t         nLoop = N >> 1;
                const vecd128* av = reinterpret_cast< const vecd128* >( in1 );
                const vecd128* bv = reinterpret_cast< const vecd128* >( in2 );
                vecd128*       cv = reinterpret_cast< vecd128* >( out );
                for ( size_t i = 0; i < nLoop; ++i )
                    cv[ i ] = av[ i ] - bv[ i ];
                nLoop = N & ~1;
                in1 += nLoop;
                in2 += nLoop;
                out += nLoop;
                N &= 1;
            }
        }

        //--------------------------------  Test 2-fold alignment
        else if ( align16_rel( in1, in2 ) )
        {

            //-----------------------------  One double add if phase wrong
            if ( !align16_abs( in1 ) )
            {
                *out++ = *in1++ - *in2++;
                N--;
            }

            //-----------------------------  Loop over sse2 add
            if ( N > 1 )
            {
                size_t         nLoop = N >> 1;
                vd2map         sumu;
                const vecd128* av = reinterpret_cast< const vecd128* >( in1 );
                const vecd128* bv = reinterpret_cast< const vecd128* >( in2 );
                for ( size_t i = 0; i < nLoop; ++i )
                {
                    vecd128 tmp = av[ i ];
                    tmp -= bv[ i ];
                    sumu.v = tmp;
                    *out++ = sumu.d[ 0 ];
                    *out++ = sumu.d[ 1 ];
                }
                nLoop = N & ~1;
                in1 += nLoop;
                in2 += nLoop;
                N &= 1;
            }
        }

    default:
        //--------------------------------  tight loop over what's left...
        while ( N-- )
            *( out++ ) = *( in1++ ) - *( in2++ );
    }
}

//======================================  Subtract a constant.
template <>
void
gen_vect::sub( double out[], double in, size_t N ) const
{
    add( out, -in, N );
}

//======================================  Sum elements of a vector
template <>
double
gen_vect::sum( const double in[], size_t n ) const
{
    math_type out = 0;
    size_t    nLeft = n;
    switch ( _sseID )
    {

        //----------------------------------- Use avx instructionsif available.
    case avx:
        while ( nLeft && !align32_abs( in ) )
        {
            out = *in++;
            nLeft--;
        }

        if ( nLeft > 3 )
        {
            const vecd256* av = reinterpret_cast< const vecd256* >( in );
            vecd256        sv = { 0, 0, 0, 0 };
            while ( nLeft > 3 )
            {
                sv += *av++;
                nLeft -= 4;
            }
#if 0
	 double dvec;
	 asm("haddpd %1, %%ymm1;"
	     "haddpd %%ymm1, %%ymm1;"
	     "movsd %%ymm1, %0"
	     : "=m" (dvec)
	     : "x" (sv)
	     : "ymm1"
	     );
	 out += dvec;
#else
            vd4map sumu;
            sumu.v = sv;
            out += sumu.d[ 0 ] + sumu.d[ 1 ] + sumu.d[ 2 ] + sumu.d[ 3 ];
#endif
            in = reinterpret_cast< const double* >( av );
        }

        if ( nLeft )
        {
            while ( nLeft-- )
                out += *in++;
        }
        break;

        //----------------------------------  Use the sse2 instructions.
    case sse2:
    case ssse3:
    case sse4_1:
    case sse4_2:
        if ( !align16_abs( in ) )
        {
            out = *in++;
            nLeft--;
        }

        if ( nLeft > 1 )
        {
            size_t         nLoop = nLeft >> 1;
            const vecd128* av = reinterpret_cast< const vecd128* >( in );
            vecd128        sv = { 0, 0 };
            while ( nLoop-- )
                sv += *av++;
            vd2map sumu;
            sumu.v = sv;
            out += sumu.d[ 0 ] + sumu.d[ 1 ];
        }

        if ( ( nLeft & 1 ) != 0 )
            out += in[ nLeft - 1 ];
        break;

        //======================================  Scaler vsum version.
    default:
        if ( n )
        {
            out = in[ 0 ];
            for ( size_t i = 1; i < n; ++i )
                out += in[ i ];
        }
    }
    return out;
}
