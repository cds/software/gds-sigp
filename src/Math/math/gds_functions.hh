/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef GDS_FUNCTIONS_HH
#define GDS_FUNCTIONS_HH

/**  Greatest common denominator of two integers.
 */
long gcd( long a, long b );

/**  Greatest common denominator
 */
double gcd( double a, double b );

/**  Round a floating point number \a x up to the nearest integer multiple 
  *  of \a m that is not less than \a x. Zero is returned if either argument
  *  is less than or equal to zero.
  *  \brief Round a number up to and integer multiple of a specified value.
  *  \param x Number to be rounded.
  *  \param m Multiplier to be rounded to.
  *  \return Nearest multiple of \a m not less than \a x.
  */
double roundup( double x, double m );

/**  Round an integer \a x up to the nearest integer multiple 
  *  of \a m that is not less than \a x. The function is defined to be zero 
  *  if \a x is less than or equal to zero and \a x if m is less than or 
  *  equal to zero.
  *  \brief Round a number up to and integer multiple of a specified value.
  *  \param x Number to be rounded.
  *  \param m Multiplier to be rounded to.
  *  \return Nearest multiple of \a m not less than \a x.
  */
long iroundup( long x, long m );

/**  Approximate a real number (x) by a ratio of two integers. The function 
  *  replaces tw long integers with the numerator (p) and denominator (q) 
  *  of the nearest rational number found and returns the difference between
  *  the requeste number (x) and the ratio (p/q). If a negative number is 
  *  specified, the p value will be negative. 
  *  \brief Approximate a real value as a ratio of two integers.
  *  \param x The number to be approximated.
  *  \param p The rational numerator.
  *  \param q The rational denominator.
  *  \param tol The requred approximation tolerance.
  *  \returns The differnce between the requested value and the ratio.
  */
double rat( double x, long& p, long& q, double tol = 0.0001 );

#include "gds_functions.icc"

#endif // !defined(GDS_FUNCTIONS_HH)
