#ifndef GDS_MATH_ARCH_HH
#define GDS_MATH_ARCH_HH

/*  Test the compiler/platform architecture to see if it supports 
 *  vectorized calculations using the SSE2-4 and/or avx instruction
 *  sets.
 */

//==================================  Useful data types
#define SSE_VEC_LENGTH 16
typedef double vecd128 __attribute__( ( vector_size( SSE_VEC_LENGTH ) ) );
typedef float  vecf128 __attribute__( ( vector_size( SSE_VEC_LENGTH ) ) );
typedef int    veci128 __attribute__( ( vector_size( SSE_VEC_LENGTH ) ) );
/*---- Mask to test pointer alignment  */
#define VEC128_ADMASK ( sizeof( vecd128 ) - 1 )
/** Union to map two doubles onto a vecd128 (sse2 xreg) data word
 */
union vd2map
{
    vecd128 v; ///< sse double vector data type
    double  d[ 2 ]; ///< double array
};

#define AVX_VEC_LENGTH 32
typedef double vecd256 __attribute__( ( vector_size( AVX_VEC_LENGTH ) ) );
typedef float  vecf256 __attribute__( ( vector_size( AVX_VEC_LENGTH ) ) );

/*---- Mask to test pointer alignment  */
#define VEC256_ADMASK ( sizeof( vecd256 ) - 1 )
/** Union to map four doubles onto a vecd256 (avx) data word
 */
union vd4map
{
    vecd256 v; ///< svx double vector datatypw
    double  d[ 4 ]; ///< double array
};

/* ------------------  Alignment tests  --------------------------*/

#define AVX_REG_LOAD_DUP( r, x ) vecd256 r = { x, x, x, x }
// #define SSE_REG_LOAD_DUP(r,x) vecd128 r = {x, x}
#define SSE_REG_LOAD_DUP( r, x )                                               \
    vecd128 r;                                                                 \
    asm( "movddup %1, %0" : "=x"( r ) : "m"( x ) : )

//======================================  Alignment tests
//
// ---  Test alignment of single operand on 8-byte boundary
inline bool
align8_abs( const void* a )
{
    return ( long( a ) & 7 ) == 0;
}
//
//       Test abosolute alignment realtive to a  16-byte boundary for:
//  ---  One argument
inline bool
align16_abs( const void* a )
{
    return ( long( a ) & 15 ) == 0;
}
//  ---  Two operands
inline bool
align16_abs( const void* a, const void* b )
{
    return ( ( long( a ) | long( b ) ) & 15 ) == 0;
}
//
//      Test relative alignment to 16-byte boundaries for:
//  ---  Two operands
inline bool
align16_rel( const void* a, const void* b )
{
    return ( ( long( a ) ^ long( b ) ) & 15 ) == 0;
}
//  ---  Three operands
inline bool
align16_rel( const void* a, const void* b, const void* c )
{
    return ( ( ( long( a ) ^ long( b ) ) | ( long( a ) ^ long( c ) ) ) & 15 ) ==
        0;
}
//
//      Test absolute alignment on a 32-byte boundary for:
// ---  One argument
inline bool
align32_abs( const void* a )
{
    return ( long( a ) & 31 ) == 0;
}
// ---  Two operands
inline bool
align32_abs( const void* a, const void* b )
{
    return ( ( long( a ) | long( b ) ) & 31 ) == 0;
}
//
//      Test relative alignment to a 32-byte boundary for
//  --- Two operands
inline bool
align32_rel( const void* a, const void* b )
{
    return ( ( long( a ) ^ long( b ) ) & 31 ) == 0;
}
//  ---  Three operands
inline bool
align32_rel( const void* a, const void* b, const void* c )
{
    return ( ( ( long( a ) ^ long( b ) ) | ( long( a ) ^ long( c ) ) ) & 31 ) ==
        0;
}

#endif // !defined(gds_math_arch_hh)
