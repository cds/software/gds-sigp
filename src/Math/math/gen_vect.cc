/* -*- mode: c++; c-basic-offset: 3; -*- */
#include <iostream>
#include "gds_math_arch.hh"

//=====================================  Define the gen_vect class
//
// This source file contains the specializations for optimized code generation.
// to avoid inhibiting the default template expansion we will drop the
// lines that tell the compiler it is specialized elsewhere.
#define GDS_VECT_INHIBIT_SPECIALIZATION 1

#include "gen_vect.hh"

using namespace std;

gen_vect global_gen_vect;

//======================================  Constructor - find the capabilities
gen_vect::gen_vect( void )
{
}

//=============================================================================
//
//   Expand all the template we promised to specialize. Presumably this will be
//   done at a higher than normal optimization level so that we can save time
//   by calling these pre-compiled functions.
//
//=============================================================================
template void gen_vect::add( double out[], const double in[], size_t N ) const;

template void gen_vect::add( double       out[],
                             const double in1[],
                             const double in2[],
                             size_t       N ) const;

template void gen_vect::add( double out[], double in, size_t N ) const;

template void
gen_vect::cmodsq( double out[], const double in[], size_t N ) const;

template void gen_vect::cvt( float out[], const int in[], size_t N ) const;

template void gen_vect::cvt( int out[], const float in[], size_t N ) const;

template void gen_vect::div( double out[], const double in[], size_t N ) const;

template void gen_vect::div( double       out[],
                             const double in1[],
                             const double in2[],
                             size_t       N ) const;

template void gen_vect::div( double out[], double in, size_t N ) const;

template gen_vect::math_type
gen_vect::dot( const double in1[], const double in2[], size_t N ) const;

template void gen_vect::mul( double out[], const double in[], size_t N ) const;

template void
gen_vect::mul( dComplex out[], const dComplex in[], size_t N ) const;

template void gen_vect::mul( double       out[],
                             const double in1[],
                             const double in2[],
                             size_t       N ) const;

template void gen_vect::mul( double out[], double in, size_t N ) const;

template void
gen_vect::muld( dComplex out[], const double in[], size_t N ) const;

template void gen_vect::muladd( double       out[],
                                const double in1[],
                                const double in2[],
                                size_t       N ) const;

template void gen_vect::muladd( double       out[],
                                const double in1[],
                                double       in2,
                                size_t       N ) const;

template void gen_vect::scale( double out[], math_type in, size_t N ) const;

template void gen_vect::scale( dComplex out[], math_type in, size_t N ) const;

template void gen_vect::sub( double out[], const double in[], size_t N ) const;

template void gen_vect::sub( double       out[],
                             const double in1[],
                             const double in2[],
                             size_t       N ) const;

template void gen_vect::sub( double out[], double in, size_t N ) const;

template double gen_vect::sum( const double in[], size_t N ) const;
