/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef GEN_VECT_HH
#define GEN_VECT_HH
#include <cstddef>

/** The vec_math class performs a bunch of vector operations optimized 
 *  depending on the capabilities of the target machine. It defines an api
 *  used to optimize the gds_veclib suite of vector functions.
 */
class gen_vect
{
public:
    /** The cpu_sse enumerator indicates the highest level instruction set 
    *  available on the machine it is running on.
    */
    enum cpu_sse
    {
        nosse, ///< no vector capabilities.
        sse2, ///< sse2   capabilities (basic 128b vector operations)
        ssse3, ///< ssse3  capabilities (sse2 + addsub)
        sse4_1, ///< sse4.1 capabilities (ssse3 + ?)
        sse4_2, ///< sse4.2 capabilities (sse4.1 + ?)
        avx ///< avx    capabilities (256b vector operations)
    };

    typedef double math_type; ///< data type used for all vectors

public:
    /** Generic vector math constructor. The constructor is used to determine 
    *  processor capabilities at run-time.
    */
    gen_vect( void );

    /** Generic vector math instance destructor.
    *  \brief Destructor.
    */
    virtual ~gen_vect( void )
    {
    }

    /** Translate a cpu implementation to a name string.
    *  \param capable CPU capability level.
    *  \returns character array name of instruction set.
    */
    const char* sse_id_name( cpu_sse capable );

    /** Force use of a specific cpu instruction set.
    *  \param capable CPU capability level/instruction set.
    */
    void set_sse_id( cpu_sse capable );

    /** Add a vector element-by-element to another vector.
    *  \brief 2-operand addition.
    *  \param out First operand vector replace with result
    *  \param in  Second operand vector
    *  \param N   Length of the vectors.
    */
    template < typename T >
    void add( T out[], const T in[], std::size_t N ) const;

    /**  Add two vectors producing a third
    *  \brief 3-operand addition.
    *  \param out Output vector replace with result
    *  \param in1 First operand vector
    *  \param in2 Second operand vector
    *  \param N   Length of the vectors.
    */
    template < typename T >
    void add( T out[], const T in1[], const T in2[], std::size_t N ) const;

    /**  Add a constant to a vector
    *  \brief add a constant to a vector.
    *  \param out First operand vector replace with result
    *  \param in  Constant operand.
    *  \param N   Length of the vectors.
    */
    template < typename T >
    void add( T out[], T in, std::size_t N ) const;

    /**  Type conversion template
    *  \brief Convert types
    *  \param out R-type output vector resulting from conversion.
    *  \param in  T-type input vector to be converted
    *  \param N   Length of the vectors.
    */
    template < typename R, typename T >
    void cvt( R out[], const T in[], std::size_t N ) const;

    /**  Divide two vectors
    *  \brief 2-operand division.
    *  \param out Dividend operator vector replaced by Quotient (result) vector
    *  \param in  Divisor operand vector
    *  \param N   Length of the vectors.
    */
    template < typename T >
    void div( T out[], const T in[], std::size_t N ) const;

    /**  Divide two vectors producing a third
    *  \brief 3-operand division.
    *  \param out Quotient (result) vector
    *  \param in1 Dividend operand vector
    *  \param in2 Divisor operand vector
    *  \param N   Length of the vectors.
    */
    template < typename T >
    void div( T out[], const T in1[], const T in2[], std::size_t N ) const;

    /** Divide a vector by a constant. Note that for float/double vectors this 
    *  operation is equivalent to (faster) multiplication by 1/constant and 
    *  is implemented as such by the default templates.
    *  \brief divide a vector by a constant.
    *  \param out First operand vector replace with result
    *  \param in  Constant operand.
    *  \param N   Length of the vectors.
    */
    template < typename T >
    void div( T out[], T in, std::size_t N ) const;

    /** Sum of element-by element multiplication of two vectors.
    *  \brief Inner (dot) product
    *  \param in1 First operand vector
    *  \param in2 Second operand vector
    *  \param N   Length of the vectors.
    *  \returns sum of element by element product of two vectors.
    */
    template < typename T >
    math_type dot( const T in1[], const T in2[], std::size_t N ) const;

    /**  Multiply two vectors
    *  \brief 2-operand product
    *  \param out First operand vector replaced by product
    *  \param in  Second operand vector
    *  \param N   Length of the vectors.
    */
    template < typename T >
    void mul( T out[], const T in[], std::size_t N ) const;

    /** Multiply two vectors producing a third
    *  \brief 3-operand multiplication.
    *  \param out Output vector replaced by result
    *  \param in1 First operand vector
    *  \param in2 Second operand vector
    *  \param N   Length of the vectors.
    */
    template < typename T >
    void mul( T out[], const T in1[], const T in2[], std::size_t N ) const;

    /**  Multiply a vector by a scalar of the same type.
    *  \brief Multiply a vector by a scalar constant.
    *  \param out First operand vector replace with result
    *  \param in  Constant operand.
    *  \param N   Length of the vectors.
    */
    template < typename T >
    void mul( T out[], T in, std::size_t N ) const;

    /**  Multiply a vector of arbitrary type by a double vector
    *  \brief Multiply vectors of different types
    *  \param out First operand type T vector replaced with result
    *  \param in  Second operand math_type (double) vector
    *  \param N   Length of the vectors.
    */
    template < typename T >
    void muld( T out[], const double in[], std::size_t N ) const;

    /** Multiply two vectors and add the result to a third vector
    *  \brief vector multiply and add operation.
    *  \param out Vector to which the product of the other two operands is added.
    *  \param in1 First input operand vector
    *  \param in2 Second input operand vector
    *  \param N   Length of the vectors.
    */
    template < typename T >
    void muladd( T out[], const T in1[], const T in2[], std::size_t N ) const;

    /** Multiply a vector by a scalar and add the result to a third vector
    *  \brief vectpr multiply by scalar and add operation.
    *  \param out Vector to which the product of the other two operands is added.
    *  \param in1 Input operand vector
    *  \param in2 Scalar multiplier.
    *  \param N   Length of the vectors.
    */
    template < typename T >
    void muladd( T out[], const T in1[], T in2, std::size_t N ) const;

    /** Scale a vector by a double constant.
    *  \brief Multiply a vector by a math_type (double) scalar constant.
    *  \param out Operand vector replace with result
    *  \param in  Constant operand.
    *  \param N   Length of the vectors.
    */
    template < typename T >
    void scale( T out[], math_type in, std::size_t N ) const;

    /**  Subtract the second operand vector from the first operand vector.
    *  \brief 2-operand vector subtraction.
    *  \param out First operand vector replaced with result
    *  \param in  Second operand vector subtracted from first.
    *  \param N   Length of the vectors.
    */
    template < typename T >
    void sub( T out[], const T in[], std::size_t N ) const;

    /**  Subtract two vectors producing a third
    *  \brief 3-operand vector subtraction.
    *  \param out Output vector replace with result
    *  \param in1 First operand vector
    *  \param in2 Second operand vector subtracted from first.
    *  \param N   Length of the vectors.
    */
    template < typename T >
    void sub( T out[], const T in1[], const T in2[], std::size_t N ) const;

    /**  Subtract a constant from a vector
    *  \brief Subtract a scalar constant from a vector.
    *  \param out First operand vector replace with result
    *  \param in  Constant operand.
    *  \param N   Length of the vectors.
    */
    template < typename T >
    void sub( T out[], T in, std::size_t N ) const;

    /**  Sum the elements of a vector.
    *  \brief Sum a vector
    *  \param in1 constant input vector.
    *  \param N  Number of vector elements to be summed.
    *  \returns sum of the first N elements in the vector
    */
    template < typename T >
    T sum( const T in1[], std::size_t N ) const;

    /** Modulo square of a complex vector.
    *  \brief Calculate the magnitude squared of elements of a complex vector.
    *  \param out Output double vector.
    *  \param in  Complex vector.
    *  \param N   Length of the vectors.
    */
    template < typename T >
    void cmodsq( double out[], const T in[], std::size_t N ) const;

private:
    cpu_sse _sseID;
};

#include "gen_vect.icc"

#endif // !defined(VEC_MATH_HH)
