#ifndef GDS_GCD_HH
#define GDS_GCD_HH

// the contents of this header file have been merged into the gds_functions.hh
// header file with the implementations included.

#include "gds_functions.hh"

#endif // !defined(GDS_GCD_HH)
